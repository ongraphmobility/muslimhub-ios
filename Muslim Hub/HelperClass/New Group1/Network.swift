//
//  Network.swift
//  Tend
//
//  Created by Sakshi Singh on 14/04/18.
//  Copyright © 2018 Sakshi. All rights reserved.
//

import Foundation
import Alamofire
import KVNProgress



typealias ServiceResponse = (Any?, Error?) -> Void

class Network: NSObject {
    
    
    
    //Live Url
    static let shared: Network = Network(baseURL:URL.init(string:
        
        "https://be.muslimhubapp.com")!)
   //"https://betest.muslimhubapp.com/"
    let baseURL: URL
    var headers: HTTPHeaders
    let decoder = JSONDecoder()
    var LoginToken:String!
    
    private func convertToData(_ value:Any) -> Data {
        if let str =  value as? String {
            return str.data(using: String.Encoding.utf8)!
        }else if let str =  value as? NSNumber {
            return str.stringValue.data(using: String.Encoding.utf8)!
        } else if let jsonData = try? JSONSerialization.data(withJSONObject: value, options: .prettyPrinted) {
            return jsonData
        } else {
            return Data()
        }
    }
    
    // Initialization
    private init(baseURL: URL) {
        self.baseURL = baseURL
        
        if let loginToken = UserDefaults.standard.value(forKey: "Login_Token") as? String{
            self.LoginToken = loginToken
        }
        if  self.LoginToken != nil {
            self.headers = [
                //                "Accept": "application/json",
                "Content-Type": "application/x-www-form-urlencoded",
                "Authorization": ("Token " + ( self.LoginToken))
            ]
        }else {
            self.headers = [
                //                "Accept": "application/json",
                "Content-Type": "application/x-www-form-urlencoded",
                // "Authorization": AppDelegate.instance.AppAdminUser?.uniqueId ?? ""
            ]
        }
    }
    
    //MARK:Delete Class Api---------------------------------------------------
    
    //AS
    func deleteOrgClassListItem(id: String, completion:@escaping (_ result:EidSalahCommonModel?) -> Void) {
        if let loginToken = UserDefaults.standard.value(forKey: "Login_Token") as? String{
            self.LoginToken = loginToken
        }
        self.headers = [
            "Content-Type": "application/json",
            "Authorization": ("Token " + ( self.LoginToken))
        ]
        
        let url = "/api/v1/operations/classes/?id=" + id
        Alamofire.request(URL(string: url, relativeTo: baseURL)!, method: .delete, parameters: nil , encoding: URLEncoding.default, headers: headers).response { (resposne) in
            do {
                let data = try self.decoder.decode(EidSalahCommonModel.self, from: resposne.data ?? Data())
                print("Success")
                if data.success == true{
                    completion(data)
                }
                KVNProgress.dismiss()
            } catch let error {
                KVNProgress.dismiss()
                self.handleError(data: resposne, error: error)
            }
        }
    }
    //MARK:Delete Api---------------------------------------------------
    
    //mark:Delete special Prayer
    func deleteSpecialPrayerFroIqamah(id:Int,completion:@escaping (_ result:EidSalahCommonModel?) -> Void) {
        
        if let loginToken = UserDefaults.standard.value(forKey: "Login_Token") as? String{
            self.LoginToken = loginToken
        }
        
        self.headers = [
            "Content-Type": "application/json",
            "Authorization": ("Token " + (self.LoginToken))
        ]
        
        let url = "/api/v1/iqama/special-prayers/?id=\(id)"
        
        Alamofire.request(URL(string: url, relativeTo: baseURL)!, method: .delete, parameters: nil , encoding: JSONEncoding.default, headers: headers).response { (response) in
            self.printRequest(response)
            do {
                let data = try self.decoder.decode(EidSalahCommonModel.self, from: response.data ?? Data())
                print("Success")
                
                //  if data.success == true{
                completion(data)
                //}
                KVNProgress.dismiss()
            } catch let error {
                KVNProgress.dismiss()
                self.handleError(data: response, error: error)
            }
        }
    }
    
    //mark:Delete special Prayer
    func deleteJummaIqamah(id:Int,completion:@escaping (_ result:EidSalahCommonModel?) -> Void) {
        
        if let loginToken = UserDefaults.standard.value(forKey: "Login_Token") as? String{
            self.LoginToken = loginToken
        }
        
        self.headers = [
            "Content-Type": "application/json",
            "Authorization": ("Token " + (self.LoginToken))
        ]
        
        let url = "/api/v1/iqama/jumma/?id=\(id)"
        
        Alamofire.request(URL(string: url, relativeTo: baseURL)!, method: .delete, parameters: nil , encoding: JSONEncoding.default, headers: headers).response { (response) in
            self.printRequest(response)
            do {
                let data = try self.decoder.decode(EidSalahCommonModel.self, from: response.data ?? Data())
                print("Success")
                
                //  if data.success == true{
                completion(data)
                //}
                KVNProgress.dismiss()
            } catch let error {
                KVNProgress.dismiss()
                self.handleError(data: response, error: error)
            }
        }
    }
    //mark:Delete Iqama
    func deleteIqamah(id:Int,completion:@escaping (_ result:EidSalahCommonModel?) -> Void) {
        
        if let loginToken = UserDefaults.standard.value(forKey: "Login_Token") as? String{
            self.LoginToken = loginToken
        }
        
        self.headers = [
            "Content-Type": "application/json",
            "Authorization": ("Token " + (self.LoginToken))
        ]
        
        let url = "/api/v1/iqama/?id=\(id)"
        
        Alamofire.request(URL(string: url, relativeTo: baseURL)!, method: .delete, parameters: nil , encoding: JSONEncoding.default, headers: headers).response { (response) in
            self.printRequest(response)
            do {
                let data = try self.decoder.decode(EidSalahCommonModel.self, from: response.data ?? Data())
                print("Success")
                
                //  if data.success == true{
                completion(data)
                //}
                KVNProgress.dismiss()
            } catch let error {
                KVNProgress.dismiss()
                self.handleError(data: response, error: error)
            }
        }
    }
    
    //delete org Announcement
    func deleteOrgAnnouncementListItem(id: Int, orgId: Int, completion:@escaping (_ result:EidSalahCommonModel?) -> Void) {
        if let loginToken = UserDefaults.standard.value(forKey: "Login_Token") as? String{
            self.LoginToken = loginToken
        }
        self.headers = [
            "Content-Type": "application/json",
            "Authorization": ("Token " + ( self.LoginToken))
        ]
        
        let para: Parameters =
            ["org" : orgId,
             "id"  : id
        ]
        
        let url = baseURL.description + "/api/v1/operations/announcement/"
        
        Alamofire.request(URL(string: url)!, method: .delete, parameters: para, encoding: URLEncoding.default, headers: headers).response { (response) in
            print(response)
            do {
                let data = try self.decoder.decode(EidSalahCommonModel.self, from: response.data ?? Data())
                print("Success")
                KVNProgress.dismiss()
                //                if data.success == true{
                completion(data)
                //                }else{
                //                }
            } catch let error {
                self.handleError(data: response, error: error)
            }
        }
    }
    
    func deleteOrgEventListItem(id: String, completion:@escaping (_ result:EidSalahCommonModel?) -> Void) {
        if let loginToken = UserDefaults.standard.value(forKey: "Login_Token") as? String{
            self.LoginToken = loginToken
        }
        self.headers = [
            "Content-Type": "application/json",
            "Authorization": ("Token " + ( self.LoginToken))
        ]
        
        let url = "/api/v1/operations/events/?id=" + id
        Alamofire.request(URL(string: url, relativeTo: baseURL)!, method: .delete, parameters: nil , encoding: URLEncoding.default, headers: headers).response { (resposne) in
            do {
                let data = try self.decoder.decode(EidSalahCommonModel.self, from: resposne.data ?? Data())
                print("Success")
                if data.success == true{
                    completion(data)
                }
                KVNProgress.dismiss()
            } catch let error {
                self.handleError(data: resposne, error: error)
            }
        }
    }
    
    func deleteOrgActivityListItem(id: String, completion:@escaping (_ result:EidSalahCommonModel?) -> Void) {
        if let loginToken = UserDefaults.standard.value(forKey: "Login_Token") as? String{
            self.LoginToken = loginToken
        }
        self.headers = [
            "Content-Type": "application/json",
            "Authorization": ("Token " + ( self.LoginToken))
        ]
        
        let url = "/api/v1/operations/activities/?id=" + id
        Alamofire.request(URL(string: url, relativeTo: baseURL)!, method: .delete, parameters: nil , encoding: URLEncoding.default, headers: headers).response { (resposne) in
            do {
                let data = try self.decoder.decode(EidSalahCommonModel.self, from: resposne.data ?? Data())
                print("Success")
                if data.success == true{
                    completion(data)
                }
                KVNProgress.dismiss()
            } catch let error {
                self.handleError(data: resposne, error: error)
            }
        }
    }
    func deleteOrgDoantionListItem(id: String, completion:@escaping (_ result:EidSalahCommonModel?) -> Void) {
        if let loginToken = UserDefaults.standard.value(forKey: "Login_Token") as? String{
            self.LoginToken = loginToken
        }
        self.headers = [
            "Content-Type": "application/json",
            "Authorization": ("Token " + ( self.LoginToken))
        ]
        
        let url = "/api/v1/operations/donations/?id=" + id
        Alamofire.request(URL(string: url, relativeTo: baseURL)!, method: .delete, parameters: nil , encoding: URLEncoding.default, headers: headers).response { (resposne) in
            do {
                let data = try self.decoder.decode(EidSalahCommonModel.self, from: resposne.data ?? Data())
                print("Success")
                if data.success == true{
                    completion(data)
                }
                KVNProgress.dismiss()
            } catch let error {
                self.handleError(data: resposne, error: error)
            }
        }
    }
    //end
    
    
    func delateEidSalahListItem(id: String, completion:@escaping (_ result:EidSalahCommonModel?) -> Void) {
        if let loginToken = UserDefaults.standard.value(forKey: "Login_Token") as? String{
            self.LoginToken = loginToken
        }
        self.headers = [
            "Content-Type": "application/json",
            "Authorization": ("Token " + ( self.LoginToken))
        ]
        
        let url = "/api/v1/org/eidsalah/?id=" + id
        Alamofire.request(URL(string: url, relativeTo: baseURL)!, method: .delete, parameters: nil , encoding: URLEncoding.default, headers: headers).response { (resposne) in
            
            self.printRequest(resposne)
            
            do {
                let data = try self.decoder.decode(EidSalahCommonModel.self, from: resposne.data ?? Data())
                print("Success")
                if data.success == true{
                    completion(data)
                }
                KVNProgress.dismiss()
            } catch let error {
                self.handleError(data: resposne, error: error)
            }
        }
        
    }
    
    
    //MARK:Post Api-----------------------------------------
    
    
    //mark: - Add Jumma
    func addJummaForIqamah(requestDict: [String: Any],editFrom:Bool, completion:@escaping (_ result:EidSalahCommonModel?) -> Void) {
        
        if let loginToken = UserDefaults.standard.value(forKey: "Login_Token") as? String{
            self.LoginToken = loginToken
        }
        
        self.headers = [
            "Content-Type": "application/json",
            "Authorization": ("Token " + (self.LoginToken))
        ]
        
        if editFrom == true{
            
            Alamofire.request(URL(string: "/api/v1/iqama/jumma/", relativeTo: baseURL)!, method: .put, parameters: requestDict , encoding: JSONEncoding.default, headers: headers).response { (response) in
                self.printRequest(response)
                do {
                    let data = try self.decoder.decode(EidSalahCommonModel.self, from: response.data ?? Data())
                    print("Success")
                    // if data.success == true{
                    completion(data)
                    //  }
                    KVNProgress.dismiss()
                } catch let error {
                    KVNProgress.dismiss()
                    self.handleError(data: response, error: error)
                }
            }
        }else{
            
            Alamofire.request(URL(string: "/api/v1/iqama/jumma/", relativeTo: baseURL)!, method: .post, parameters: requestDict , encoding: JSONEncoding.default, headers: headers).response { (response) in
                self.printRequest(response)
                do {
                    let data = try self.decoder.decode(EidSalahCommonModel.self, from: response.data ?? Data())
                    print("Success")
                    
                    completion(data)
                    
                    KVNProgress.dismiss()
                } catch let error {
                    KVNProgress.dismiss()
                    self.handleError(data: response, error: error)
                }
            }
        }
        
    }
    //mark: - Add IQAMA
    func addIqamaForIqamah(requestDict: [String: Any],isEdit:Bool, completion:@escaping (_ result:EidSalahCommonModel?) -> Void) {
        
        if let loginToken = UserDefaults.standard.value(forKey: "Login_Token") as? String{
            self.LoginToken = loginToken
        }
        
        self.headers = [
            "Content-Type": "application/json",
            "Authorization": ("Token " + (self.LoginToken))
        ]
        
        
        if isEdit == true{
            Alamofire.request(URL(string: "/api/v1/iqama/", relativeTo: baseURL)!, method: .put, parameters: requestDict , encoding: JSONEncoding.default, headers: headers).response { (response) in
                self.printRequest(response)
                do {
                    let data = try self.decoder.decode(EidSalahCommonModel.self, from: response.data ?? Data())
                    print("Success")
                    
                    completion(data)
                    
                    KVNProgress.dismiss()
                } catch let error {
                    KVNProgress.dismiss()
                    self.handleError(data: response, error: error)
                }
            }
        }else{
            Alamofire.request(URL(string: "/api/v1/iqama/", relativeTo: baseURL)!, method: .post, parameters: requestDict , encoding: JSONEncoding.default, headers: headers).response { (response) in
                self.printRequest(response)
                do {
                    let data = try self.decoder.decode(EidSalahCommonModel.self, from: response.data ?? Data())
                    print("Success")
                    
                    completion(data)
                    
                    KVNProgress.dismiss()
                } catch let error {
                    KVNProgress.dismiss()
                    self.handleError(data: response, error: error)
                }
            }
        }
        
        
        
    }
    
    
    //mark: - Add Special Prayer
    func addSpecialPrayerFroIqamah(requestDict: [String: Any],editSpecialPrayer: Bool, completion:@escaping (_ result:EidSalahCommonModel?) -> Void) {
        
        if let loginToken = UserDefaults.standard.value(forKey: "Login_Token") as? String{
            self.LoginToken = loginToken
        }
        
        self.headers = [
            "Content-Type": "application/json",
            "Authorization": ("Token " + (self.LoginToken))
        ]
        if editSpecialPrayer == true{
            Alamofire.request(URL(string: "/api/v1/iqama/special-prayers/", relativeTo: baseURL)!, method: .put, parameters: requestDict , encoding: JSONEncoding.default, headers: headers).response { (response) in
                self.printRequest(response)
                do {
                    let data = try self.decoder.decode(EidSalahCommonModel.self, from: response.data ?? Data())
                    print("Success")
                    
                    //  if data.success == true{
                    completion(data)
                    //}
                    KVNProgress.dismiss()
                } catch let error {
                    KVNProgress.dismiss()
                    self.handleError(data: response, error: error)
                }
            }
        }else{
            
            
            Alamofire.request(URL(string: "/api/v1/iqama/special-prayers/", relativeTo: baseURL)!, method: .post, parameters: requestDict , encoding: JSONEncoding.default, headers: headers).response { (response) in
                self.printRequest(response)
                do {
                    let data = try self.decoder.decode(EidSalahCommonModel.self, from: response.data ?? Data())
                    print("Success")
                    
                    //  if data.success == true{
                    completion(data)
                    //}
                    KVNProgress.dismiss()
                } catch let error {
                    KVNProgress.dismiss()
                    self.handleError(data: response, error: error)
                }
            }
        }
    }
    
    
    func addReport(id:Int, completion:@escaping (_ result:EidSalahCommonModel?) -> Void) {
        
        if let loginToken = UserDefaults.standard.value(forKey: "Login_Token") as? String{
            self.LoginToken = loginToken
        }
        
        self.headers = [
            "Content-Type": "application/json",
            
        ]
        let url =  "/api/v1/iqama/report/?id=" + "\(id)"
        
        Alamofire.request(URL(string: url, relativeTo: baseURL)!, method: .post, parameters: nil , encoding: JSONEncoding.default, headers: headers).response { (response) in
            self.printRequest(response)
            do {
                let data = try self.decoder.decode(EidSalahCommonModel.self, from: response.data ?? Data())
                print("Success")
                
                //  if data.success == true{
                completion(data)
                //}
                KVNProgress.dismiss()
            } catch let error {
                KVNProgress.dismiss()
                self.handleError(data: response, error: error)
            }
        }
        
    }
    
    //MARK:- Add Manage Users-------------
    func addManageUsers(org_id:Int,email:String,first_name:String,last_name:String,password:String,mobile_number:String, completion:@escaping (_ result:LoginModel?) -> Void) {
        
        if let loginToken = UserDefaults.standard.value(forKey: "Login_Token") as? String{
            self.LoginToken = loginToken
        }
        self.headers = [
            //                "Accept": "application/json",
            "Content-Type": "application/json",
            "Authorization": ("Token " + ( self.LoginToken))
        ]
        
        
        
        var url = "https://be.muslimhubapp.com/" + "api/v1/manage/users/?org_id=" + "\(org_id)"
        
        url = (url  + "&type=manual")
        
        
        let para: Parameters =
            ["email": email,"first_name":first_name,"last_name":last_name,"mobile_number":mobile_number]
        
        
        Alamofire.request(URL(string: url, relativeTo: baseURL)!, method: .post, parameters:para , encoding: JSONEncoding.default, headers: headers).response { (resposne) in
            
            self.printRequest(resposne)
            
            do {
                let data = try self.decoder.decode(LoginModel.self, from: resposne.data ?? Data())
                print("Success")
                
                completion(data)
                KVNProgress.dismiss()
            } catch let error {
                KVNProgress.dismiss()
                self.handleError(data: resposne, error: error)
            }
        }
        
    }
    //MARK:upload Xls File
    func uploadWithAlamofire(org_id:Int,filePath:Data,completion:@escaping (_ result:XlsModel?) -> Void)  {
        
        
        if let loginToken = UserDefaults.standard.value(forKey: "Login_Token") as? String{
            self.LoginToken = loginToken
        }
        let headers: HTTPHeaders = [
            "Authorization":  ("Token " + ( self.LoginToken)),
            
            "content-type" : "application/json"
        ]
        
        
        
        
        var urll = "https://be.muslimhubapp.com/" + "api/v1/manage/users/?org_id=" + "\(org_id)"
        
        urll = (urll  + "&type=auto")
        
        
        print("xlx url",urll)
        Alamofire.upload(multipartFormData: {
            multipartFormData in
            multipartFormData.append(filePath, withName: "excel_file",fileName: "xlsFile_\(self.generateRandomDigits(10)).xls", mimeType: "application/vnd.ms-excel")
        },
                         to: urll, method: .post, headers: headers) { (result) in
                            
                            switch result {
                            case .success(let upload, _, _):
                                
                                upload.uploadProgress(closure: { (progress) in
                                    
                                    print("Upload Progress: \(progress.fractionCompleted)")
                                })
                                
                                upload.response { response in
                                    print(response)
                                    if let data = response.data, let utf8Text = String(data: data, encoding: .utf8) {
                                        
                                        do {
                                            let data = try self.decoder.decode(XlsModel.self, from: response.data ?? Data())
                                            print("Success")
                                            
                                            
                                            completion(data)
                                            KVNProgress.dismiss()
                                            
                                            
                                        } catch let error {
                                            KVNProgress.dismiss()
                                            self.handleError(data: response, error: error)
                                        }
                                        
                                        
                                        
                                        print("Data: \(utf8Text)")
                                        //                        if data.success == true{
                                        
                                        //}
                                    }
                                    print(response.request!.value as Any)
                                }
                                
                            case .failure(let encodingError):
                                print(encodingError)
                            }
        }
    }
    
    //mark: Add Eid Salah APi
    func addEidSalah(latitude:Double, longitude:Double, address: String, city: String, date: String, salah_time: String, held: Int, attendees: Int, takbir_time: String, parking: Int, khutbah_lang: Int, phone_num: String, info: String, org: Int, completion:@escaping (_ result:EidSalahCommonModel?) -> Void) {
        
        if let loginToken = UserDefaults.standard.value(forKey: "Login_Token") as? String{
            self.LoginToken = loginToken
        }
        let para: Parameters =
            ["address"          : address,
             "city"             : city,
             "date"             : date,
             "salah_time"       : salah_time,
             "takbir_time"      : takbir_time,
             "held"             : held,
             "attendees"        : attendees,
             "parking"          : parking,
             "latitude"         : latitude,
             "longitude"        : longitude,
             "khutbah_lang"     : khutbah_lang,
             "phone_num"        : phone_num,
             "info"             : info ,
             "org"              : org
        ]
        
        self.headers = [
            "Content-Type": "application/json",
            "Authorization": ("Token " + (self.LoginToken))
        ]
        Alamofire.request(URL(string: "/api/v1/org/eidsalah/", relativeTo: baseURL)!, method: .post, parameters:para , encoding: JSONEncoding.default, headers: headers).response { (resposne) in
            
            self.printRequest(resposne)
            
            do {
                let data = try self.decoder.decode(EidSalahCommonModel.self, from: resposne.data ?? Data())
                print("Success")
                
                if data.success == true{
                    completion(data)
                }
                
                KVNProgress.dismiss()
                
                
            } catch let error {
                KVNProgress.dismiss()
                self.handleError(data: resposne, error: error)
            }
        }
        
    }
    
    func requestWith(imageData: Data?, key:String,parameters: [String : Any], completion:@escaping (_ result:OrganizationDetailModel?) -> Void) {
        if let loginToken = UserDefaults.standard.value(forKey: "Login_Token") as? String{
            self.LoginToken = loginToken
        }
        let url = "https://be.muslimhubapp.com/api/v1/org/update/" /* your API url */
        
        let headers: HTTPHeaders = [
            "Authorization": ("Token " + (self.LoginToken)),
            "Content-type": "application/json"
        ]
        
        Alamofire.upload(multipartFormData: { (multipartFormData) in
            
            
            if let data = imageData{
                let keyName = key
                let uuid = UUID().uuidString
                
                let imageName = ("image1" + uuid + ".png")
                multipartFormData.append(data, withName: keyName, fileName: imageName, mimeType: "image/png")
                
            }
            for (key, value) in parameters {
                multipartFormData.append("\(value)".data(using: String.Encoding.utf8)!, withName: key as String)
            }
            
        }, to: url, method: .put, headers: headers) { (result) in
            
            
            switch result{
            case .success(let upload, _, _):
                upload.response { response in
                    print("Request: \(response.request)")
                    print("Response: \(response.response)")
                    print("Error: \(response.error)")
                    
                    if let data = response.data, let utf8Text = String(data: data, encoding: .utf8) {
                        
                        do {
                            let data = try self.decoder.decode(OrganizationDetailModel.self, from: response.data ?? Data())
                            print("Success")
                            
                            if data.success == true{
                                completion(data)
                            }
                            
                            KVNProgress.dismiss()
                            
                            
                        } catch let error {
                            KVNProgress.dismiss()
                            self.handleError(data: response, error: error)
                        }
                        print("Data: \(utf8Text)")
                        
                    }
                    
                }
            case .failure(let error):
                print("Error in upload: \(error.localizedDescription)")
                //onError?(error)
            }
            
            
        }
    }
    
    //ADD ANNOUNCEMENT FOR A ORGANISATION
    func addAnnouncementFromOrgMultiple(imageData: [Data]?,image1:String,image2:String,image3:String,image4:String,image5:String,image6:String,parameters: [String : Any], completion:@escaping (_ result:OrganizationDetailModel?) -> Void) {
        
        let url = "https://be.muslimhubapp.com/api/v1/operations/announcement/" /* your API url */
        
        if let loginToken = UserDefaults.standard.value(forKey: "Login_Token") as? String{
            self.LoginToken = loginToken
        }
        let headers: HTTPHeaders = [
            "Authorization":  ("Token " + ( self.LoginToken)),
            "Content-type":  "multipart/form-data"
            
        ]
        
        Alamofire.upload(multipartFormData: { (multipartFormData) in
            for (key, value) in parameters {
                multipartFormData.append("\(value)".data(using: String.Encoding.utf8)!, withName: key as String)
            }
            if imageData != nil{
                
                let count = imageData!.count
                for i in 1..<count+1{
                    multipartFormData.append(imageData![i-1], withName:"image\(i)", fileName: "image_\(self.generateRandomDigits(10)).jpeg", mimeType: "image/jpeg")
                    
                }
            }
            
        }, usingThreshold: UInt64.init(), to: url, method: .post, headers: headers) { (result) in
            
            
            switch result{
            case .success(let upload, _, _):
                upload.response { response in
                    //                    print("Request: \(response.request)")
                    //                    print("Response: \(response.response)")
                    //                    print("Error: \(response.error)")
                    //
                    if let data = response.data, let utf8Text = String(data: data, encoding: .utf8) {
                        
                        do {
                            let data = try self.decoder.decode(OrganizationDetailModel.self, from: response.data ?? Data())
                            print("Success")
                            
                            
                            completion(data)
                            KVNProgress.dismiss()
                            
                            
                        } catch let error {
                            KVNProgress.dismiss()
                            self.handleError(data: response, error: error)
                        }
                        
                        
                        
                        print("Data: \(utf8Text)")
                        //                        if data.success == true{
                        
                        //}
                    }
                    
                }
            case .failure(let error):
                print("Error in upload: \(error.localizedDescription)")
                //onError?(error)
                KVNProgress.dismiss()
            }
        }
    }
    
    //mark:Add Anouncement Images
    func requestWithMultiple(imageData: [Data]?,image1:String,image2:String,image3:String,image4:String,image5:String,image6:String,parameters: [String : Any], completion:@escaping (_ result:OrganizationDetailModel?) -> Void) {
        
        let url = "https://be.muslimhubapp.com/api/v1/info/announcements/change/" /* your API url */
        if let loginToken = UserDefaults.standard.value(forKey: "Login_Token") as? String{
            self.LoginToken = loginToken
        }
        let headers: HTTPHeaders = [
            "Authorization":  ("Token " + ( self.LoginToken)),
            "Content-type":  "multipart/form-data"
            
        ]
        
        Alamofire.upload(multipartFormData: { (multipartFormData) in
            for (key, value) in parameters {
                multipartFormData.append("\(value)".data(using: String.Encoding.utf8)!, withName: key as String)
            }
            if imageData != nil{
                
                let count = imageData!.count
                for i in 1..<count+1{
                    multipartFormData.append(imageData![i-1], withName:"image\(i)", fileName: "image_\(self.generateRandomDigits(10)).jpeg", mimeType: "image/jpeg")
                    
                }
            }
            
        }, usingThreshold: UInt64.init(), to: url, method: .post, headers: headers) { (result) in
            
            
            switch result{
            case .success(let upload, _, _):
                upload.response { response in
                    //                    print("Request: \(response.request)")
                    //                    print("Response: \(response.response)")
                    //                    print("Error: \(response.error)")
                    //
                    if let data = response.data, let utf8Text = String(data: data, encoding: .utf8) {
                        
                        do {
                            let data = try self.decoder.decode(OrganizationDetailModel.self, from: response.data ?? Data())
                            print("Success")
                            
                            
                            completion(data)
                            KVNProgress.dismiss()
                            
                            
                        } catch let error {
                            KVNProgress.dismiss()
                            self.handleError(data: response, error: error)
                        }
                        
                        
                        
                        print("Data: \(utf8Text)")
                        //                        if data.success == true{
                        
                        //}
                    }
                    
                }
            case .failure(let error):
                print("Error in upload: \(error.localizedDescription)")
                //onError?(error)
                KVNProgress.dismiss()
            }
        }
    }
    
    
    //marK:Add Event With multiple Images
    func requestWithMultipleForEvent(imageData: [Data]?,image1:String,image2:String,image3:String,image4:String,image5:String,image6:String,parameters: [String : Any], completion:@escaping (_ result:OrganizationDetailModel?) -> Void) {
        
        let url = "https://be.muslimhubapp.com/api/v1/operations/events/" /* your API url */
        if let loginToken = UserDefaults.standard.value(forKey: "Login_Token") as? String{
            self.LoginToken = loginToken
        }
        let headers: HTTPHeaders = [
            "Authorization":  ("Token " + ( self.LoginToken)),
            "Content-type":  "application/json"
        ]
        
        Alamofire.upload(multipartFormData: { (multipartFormData) in
            for (key, value) in parameters {
                multipartFormData.append("\(value)".data(using: String.Encoding.utf8)!, withName: key as String)
            }
            if imageData != nil{
                
                let count = imageData!.count
                for i in 1..<count+1{
                    multipartFormData.append(imageData![i-1], withName:"image\(i)", fileName: "image_\(self.generateRandomDigits(10)).jpeg", mimeType: "image/jpeg")
                    
                }
            }
            
        }, usingThreshold: UInt64.init(), to: url, method: .post, headers: headers) { (result) in
            
            
            switch result{
            case .success(let upload, _, _):
                upload.response { response in
                    print("Request: \(String(describing: response.request))")
                    print("Response: \(String(describing: response.response))")
                    print("Error: \(String(describing: response.error))")
                    
                    if let data = response.data, let utf8Text = String(data: data, encoding: .utf8) {
                        
                        do {
                            let data = try self.decoder.decode(OrganizationDetailModel.self, from: response.data ?? Data())
                            print("Success")
                            
                            
                            completion(data)
                            KVNProgress.dismiss()
                            
                            
                        } catch let error {
                            KVNProgress.dismiss()
                            self.handleError(data: response, error: error)
                        }
                        print("Data: \(utf8Text)")
                        
                    }
                    
                }
            case .failure(let error):
                print("Error in upload: \(error.localizedDescription)")
                //onError?(error)
                KVNProgress.dismiss()
            }
        }
    }
    //marK:Add Activities With multiple Images
    func requestWithMultipleForActivites(imageData: [Data]?,image1:String,image2:String,image3:String,image4:String,image5:String,image6:String,parameters: [String : Any], completion:@escaping (_ result:OrganizationDetailModel?) -> Void) {
        
        let url = "https://be.muslimhubapp.com/api/v1/operations/activities/" /* your API url */
        if let loginToken = UserDefaults.standard.value(forKey: "Login_Token") as? String{
            self.LoginToken = loginToken
        }
        let headers: HTTPHeaders = [
            "Authorization":  ("Token " + ( self.LoginToken)),
            "Content-type":  "application/json"
        ]
        
        Alamofire.upload(multipartFormData: { (multipartFormData) in
            for (key, value) in parameters {
                multipartFormData.append("\(value)".data(using: String.Encoding.utf8)!, withName: key as String)
            }
            if imageData != nil{
                
                let count = imageData!.count
                for i in 1..<count+1{
                    multipartFormData.append(imageData![i-1], withName:"image\(i)", fileName: "image_\(self.generateRandomDigits(10)).jpeg", mimeType: "image/jpeg")
                    
                }
            }
            
        }, usingThreshold: UInt64.init(), to: url, method: .post, headers: headers) { (result) in
            
            
            switch result{
            case .success(let upload, _, _):
                upload.response { response in
                    print("Request: \(String(describing: response.request))")
                    print("Response: \(String(describing: response.response))")
                    print("Error: \(String(describing: response.error))")
                    
                    if let data = response.data, let utf8Text = String(data: data, encoding: .utf8) {
                        
                        do {
                            let data = try self.decoder.decode(OrganizationDetailModel.self, from: response.data ?? Data())
                            print("Success")
                            
                            
                            completion(data)
                            KVNProgress.dismiss()
                            
                            
                        } catch let error {
                            KVNProgress.dismiss()
                            self.handleError(data: response, error: error)
                        }
                        print("Data: \(utf8Text)")
                        
                    }
                    
                }
            case .failure(let error):
                print("Error in upload: \(error.localizedDescription)")
                //onError?(error)
                KVNProgress.dismiss()
            }
        }
    }
    
    //marK:Add Classes With multiple Images
    func requestWithMultipleForClasses(imageData: [Data]?,image1:String,image2:String,image3:String,image4:String,image5:String,image6:String,parameters: [String : Any], completion:@escaping (_ result:OrganizationDetailModel?) -> Void) {
        
        let url = "https://be.muslimhubapp.com/api/v1/operations/classes/" /* your API url */
        if let loginToken = UserDefaults.standard.value(forKey: "Login_Token") as? String{
            self.LoginToken = loginToken
        }
        let headers: HTTPHeaders = [
            "Authorization":  ("Token " + ( self.LoginToken)),
            "Content-type":  "application/json"
        ]
        
        Alamofire.upload(multipartFormData: { (multipartFormData) in
            for (key, value) in parameters {
                multipartFormData.append("\(value)".data(using: String.Encoding.utf8)!, withName: key as String)
            }
            if imageData != nil{
                
                let count = imageData!.count
                for i in 1..<count+1{
                    multipartFormData.append(imageData![i-1], withName:"image\(i)", fileName: "image_\(self.generateRandomDigits(10)).jpeg", mimeType: "image/jpeg")
                    
                }
            }
            
        }, usingThreshold: UInt64.init(), to: url, method: .post, headers: headers) { (result) in
            
            
            switch result{
            case .success(let upload, _, _):
                upload.response { response in
                    print("Request: \(String(describing: response.request))")
                    print("Response: \(String(describing: response.response))")
                    print("Error: \(String(describing: response.error))")
                    
                    if let data = response.data, let utf8Text = String(data: data, encoding: .utf8) {
                        
                        do {
                            let data = try self.decoder.decode(OrganizationDetailModel.self, from: response.data ?? Data())
                            print("Success")
                            
                            
                            completion(data)
                            KVNProgress.dismiss()
                            
                            
                        } catch let error {
                            KVNProgress.dismiss()
                            self.handleError(data: response, error: error)
                        }
                        print("Data: \(utf8Text)")
                        
                    }
                    
                }
            case .failure(let error):
                print("Error in upload: \(error.localizedDescription)")
                //onError?(error)
                KVNProgress.dismiss()
            }
        }
        
    }
    
    
    func requestWithMultipleForDonation(imageData: [Data]?,image1:String,image2:String,image3:String,image4:String,image5:String,image6:String,parameters: [String : Any], completion:@escaping (_ result:OrganizationDetailModel?) -> Void) {
        
        let url = "https://be.muslimhubapp.com/api/v1/operations/donations/" /* your API url */
        if let loginToken = UserDefaults.standard.value(forKey: "Login_Token") as? String{
            self.LoginToken = loginToken
        }
        let headers: HTTPHeaders = [
            "Authorization":  ("Token " + ( self.LoginToken)),
            //  "Content-type": "multipart/form-data"
            "Content-Type": "application/json"
        ]
        
        Alamofire.upload(multipartFormData: { (multipartFormData) in
            
            if imageData != nil{
                let count = imageData!.count
                for i in 1..<count+1{
                    multipartFormData.append(imageData![i-1], withName:"image\(i)", fileName: "image_\(self.generateRandomDigits(10)).jpeg", mimeType: "image/jpeg")
                    
                }
            }
            
            for (key, value) in parameters {
                multipartFormData.append(self.convertToData(value), withName: key)
            }
            
        }, usingThreshold: UInt64.init(), to: url, method: .post, headers: headers) { (result) in
            
            
            switch result{
            case .success(let upload, _, _):
                upload.response { response in
                    print("Request: \(String(describing: response.request))")
                    print("Response: \(String(describing: response.response))")
                    print("Error: \(String(describing: response.error))")
                    
                    if let data = response.data, let utf8Text = String(data: data, encoding: .utf8) {
                        
                        do {
                            let data = try self.decoder.decode(OrganizationDetailModel.self, from: response.data ?? Data())
                            print("Success")
                            
                            
                            completion(data)
                            KVNProgress.dismiss()
                            
                            
                        } catch let error {
                            KVNProgress.dismiss()
                            self.handleError(data: response, error: error)
                        }
                        print("Data: \(utf8Text)")
                        
                    }
                    
                }
            case .failure(let error):
                print("Error in upload: \(error.localizedDescription)")
                //onError?(error)
                KVNProgress.dismiss()
            }
        }
        
        
    }
    
    func loginUser(email: String,password: String,device_id:String,device_token:String,phone_type:Int, completion:@escaping (_ result:LoginModel?) -> Void) {
        
        
        let para: Parameters =
            ["email": email,"password":password,"device_id":device_id,"device_token":device_token,"phone_type":phone_type]
        self.headers = [
            "Content-Type": "application/json"
        ]
        Alamofire.request(URL(string: "/api/v1/auth/login/", relativeTo: baseURL)!, method: .post, parameters:para , encoding: JSONEncoding.default, headers: headers).response { (resposne) in
            
            self.printRequest(resposne)
            
            do {
                
                let data = try self.decoder.decode(LoginModel.self, from: resposne.data ?? Data())
                print("Success")
                KVNProgress.dismiss()
                
                completion(data)
            } catch let error {
                KVNProgress.dismiss()
                self.handleError(data: resposne, error: error)
            }
        }
        
    }
    
    func deviceIdRegister(device_id:String,device_token:String,phone_type:Int,completion:@escaping (_ result:LoginModel?) -> Void) {
        self.headers = [
            "Content-Type": "application/json"
        ]
        let para = ["device_id":device_id,"device_token":device_token,"phone_type":phone_type] as [String : Any]
        Alamofire.request(URL(string: "/api/v1/auth/device/registration/", relativeTo: baseURL)!, method: .post, parameters:para , encoding: JSONEncoding.default, headers: headers).response { (resposne) in
            
            self.printRequest(resposne)
            
            do {
                let data = try self.decoder.decode(LoginModel.self, from: resposne.data ?? Data())
                print("Success")
                
                if data.success == true{
                    completion(data)
                }
                
                
                
                
            } catch let error {
                
                self.handleError(data: resposne, error: error)
            }
        }
        
    }
    func createUserLogin(first_name: String,last_name: String,email: String,mobile_number:String, completion:@escaping (_ result:LoginModel?) -> Void) {
        
        var token = String()
        var url = String()
        let para: Parameters =
            ["first_name": first_name,"last_name":last_name,"email":email,"mobile_number":mobile_number]
        
        if let deviceId = UserDefaults.standard.value(forKeyPath: "deviceToken") as? String{ //2.  if user is Anonymous:
            token = "device_id=" + deviceId
        }
        
        self.headers = [
            //                "Accept": "application/json",
            "Content-Type": "application/json",
            
        ]
        
        url = "/api/v1/auth/device/profile/?" + token
        Alamofire.request(URL(string:url , relativeTo: baseURL)!, method: .post, parameters:para , encoding: JSONEncoding.default, headers: headers).response { (resposne) in
            KVNProgress.dismiss()
            self.printRequest(resposne)
            
            do {
                
                let data = try self.decoder.decode(LoginModel.self, from: resposne.data ?? Data())
                print("Success")
                
                KVNProgress.dismiss()
                completion(data)
                
                
                
            } catch let error {
                KVNProgress.dismiss()
                self.handleError(data: resposne, error: error)
            }
        }
        
    }
    func editCreateUserLogin(first_name: String,last_name: String,email: String,mobile_number:String, completion:@escaping (_ result:LoginModel?) -> Void) {
        
        var token = String()
        var url = String()
        let para: Parameters =
            ["first_name": first_name,"last_name":last_name,"email":email,"mobile_number":mobile_number]
        self.headers = [
                  //                "Accept": "application/json",
                  "Content-Type": "application/json",
                  
              ]
              
        if let deviceId = UserDefaults.standard.value(forKeyPath: "deviceToken") as? String{ //2.  if user is Anonymous:
            token = "device_id=" + deviceId
        }
        url = "/api/v1/auth/device/profile/?" + token
        Alamofire.request(URL(string:url , relativeTo: baseURL)!, method: .put, parameters:para , encoding: JSONEncoding.default, headers: headers).response { (resposne) in
            KVNProgress.dismiss()
            self.printRequest(resposne)
            
            do {
                
                let data = try self.decoder.decode(LoginModel.self, from: resposne.data ?? Data())
                print("Success")
                
                KVNProgress.dismiss()
                completion(data)
                
                
                
            } catch let error {
                KVNProgress.dismiss()
                self.handleError(data: resposne, error: error)
            }
        }
        
    }
    
    func otpVerification(otp: Int, completion:@escaping (_ result:LoginModel?) -> Void) {
        
        var token = String()
        var url = String()
        let para: Parameters =
            ["otp":otp ]
        
        if let deviceId = UserDefaults.standard.value(forKeyPath: "deviceToken") as? String{ //2.  if user is Anonymous:
            token = "device_id=" + deviceId
        }
        url = "/api/v1/auth/device/otp-verify/?" + token
        Alamofire.request(URL(string:url , relativeTo: baseURL)!, method: .post, parameters:para , encoding: JSONEncoding.default, headers: nil).response { (resposne) in
            KVNProgress.dismiss()
            self.printRequest(resposne)
            
            do {
                
                let data = try self.decoder.decode(LoginModel.self, from: resposne.data ?? Data())
                print("Success")
                
                KVNProgress.dismiss()
                completion(data)
                
                
                
            } catch let error {
                KVNProgress.dismiss()
                self.handleError(data: resposne, error: error)
            }
        }
        
    }
    func changePassword(old_password: String,new_password1: String,new_password2: String, completion:@escaping (_ result:LoginModel?) -> Void) {
        
        let para: Parameters =
            ["old_password": old_password,"new_password1":new_password1,"new_password2":new_password2]
        if let loginToken = UserDefaults.standard.value(forKey: "Login_Token") as? String{
            
            self.LoginToken = loginToken
            self.headers = [
                //    "Accept": "application/json",
                "Content-Type": "application/json",
                "Authorization": ("Token " + ( self.LoginToken))
            ]
        }
        Alamofire.request(URL(string: "/api/v1/auth/password/change/", relativeTo: baseURL)!, method: .post, parameters:para , encoding: JSONEncoding.default, headers: headers).response { (resposne) in
            KVNProgress.dismiss()
            self.printRequest(resposne)
            
            do {
                
                let data = try self.decoder.decode(LoginModel.self, from: resposne.data ?? Data())
                print("Success")
                
                KVNProgress.dismiss()
                completion(data)
                
                
                
            } catch let error {
                KVNProgress.dismiss()
                self.handleError(data: resposne, error: error)
            }
        }
        
    }
    
    func forgotPassword(email: String, completion:@escaping (_ result:LoginModel?) -> Void) {
        
        let para: Parameters =
            ["email": email]
        self.headers = [
            
            "Content-Type": "application/json",
            
        ]
        
        Alamofire.request(URL(string: "/api/v1/auth/password/reset/", relativeTo: baseURL)!, method: .post, parameters:para , encoding: JSONEncoding.default, headers: headers).response { (resposne) in
            
            self.printRequest(resposne)
            
            do {
                let data = try self.decoder.decode(LoginModel.self, from: resposne.data ?? Data())
                print("Success")
                  completion(data)
               
                 
                
                
            } catch let error {
                KVNProgress.dismiss()
                self.handleError(data: resposne, error: error)
            }
        }
        
    }
    
    //Mark:- PoliApi-----------------------------------
    func poliPaymentApi(LinkType: String,Amount: String,CurrencyCode: String,MerchantData: String,MerchantReference: String,ConfirmationEmail: String,AllowCustomerReference: String,ViaEmail: String,RecipientName: String,LinkExpiry:String,RecipientEmail:String,auth_Code:String,merchant_Code:String, completion:@escaping (_ result:String?) -> Void) {
        
        let para: Parameters =
            ["LinkType": LinkType,"Amount":Amount,"CurrencyCode":CurrencyCode,"MerchantData":MerchantData,"MerchantReference":MerchantReference,"ConfirmationEmail":ConfirmationEmail,"AllowCustomerReference":AllowCustomerReference,"ViaEmail":ViaEmail,"RecipientName":RecipientName,"LinkExpiry":LinkExpiry,"RecipientEmail":RecipientEmail]
        if let loginToken = UserDefaults.standard.value(forKey: "Login_Token") as? String{
            
            self.LoginToken = loginToken
            
            let code = merchant_Code + ":" + auth_Code
            let base64Encoded = code.base64Encoded()
            
            self.headers = [
                
                "Content-Type": "application/json",
                "Authorization": "Basic " + base64Encoded!
            ]
        }else{
            let code = merchant_Code + ":" + auth_Code
            let base64Encoded = code.base64Encoded()
            
            self.headers = [
                
                "Content-Type": "application/json",
                "Authorization": "Basic " + base64Encoded!
            ]
            
        }
      
    
        Alamofire.request(URL(string: "https://poliapi.apac.paywithpoli.com/api/POLiLink/Create", relativeTo: nil)!, method: .post, parameters:para , encoding: JSONEncoding.default, headers: headers).responseJSON { (response) in
          
            //            self.printRequest(resposne)
               // KVNProgress.dismiss()
            let value = response.result.value
            
            if let responseDict = value as? [String:Any] {
             
                let msg = MHHelper.shared.getMessage(from: responseDict)
                AKAlertController.alert(msg )
            } else {
               
                if let data = response.data {
                    let str = String(decoding: data, as: UTF8.self)
                    completion(str)
                }
                
            }
            
            
            
            
        }
        
    }
    //Mark: RegisterOrg Api----------------------
    func registerOrg(user_email: String,user_mobile_number: String,user_first_name: String,user_last_name:String,password1: String,password2: String,name: String,abbreviation:String,org_type:String,description:String,address:String,latitude:Double,longitude:Double,reg_number:String,org_contact_name:String,org_contact:String,org_email:String,org_url:String,comments:String,donation_allowed:Bool,poli_pay_id:String,is_private:Bool,services:[Int],org_approved:String,tax_deductable:Bool,merchant_code:String,auth_code:String, completion:@escaping (_ result:RegisterOrgModel?) -> Void) {
        
        let para: Parameters =
            ["user_email": user_email,
             "user_mobile_number": user_mobile_number,
             "user_first_name": user_first_name,
             "user_last_name": user_last_name,
             "password1": password1,
             "password2": password2,
             "name": name,
             "abbreviation": abbreviation,
             "org_type": org_type,
             "description": description,
             "address": address,
             "latitude": latitude,
             "longitude": longitude,
             "reg_number": reg_number,
             "org_contact_name": org_contact_name,
             "org_contact": org_contact ,
             "org_email": org_email,
             "org_url": org_url,
             "comments": comments,
             "donation_allowed": donation_allowed,
             "poli_pay_id": poli_pay_id,
             "is_private": is_private,
             "services": services,
             "org_approved": org_approved,
             "tax_deductable":tax_deductable,
             "auth_code":auth_code,
             "merchant_code":merchant_code
        ]
        if let loginToken = UserDefaults.standard.value(forKey: "Login_Token") as? String{
            self.LoginToken = loginToken
        }
        self.headers = [
            //                "Accept": "application/json",
            "Content-Type": "application/json",
            "Authorization":  ("Token " + ( self.LoginToken))
        ]
        Alamofire.request(URL(string: "/api/v1/org/onboard/", relativeTo: baseURL)!, method: .post, parameters:para , encoding: JSONEncoding.default, headers: headers).response { (resposne) in
            
            self.printRequest(resposne)
            
            do {
                let data = try self.decoder.decode(RegisterOrgModel.self, from: resposne.data ?? Data())
                print("Success")
                
                //                if data.success == true{
                //
                //
                //                }
                completion(data)
                KVNProgress.dismiss()
                
                
                
                
            } catch let error {
                KVNProgress.dismiss()
                self.handleError(data: resposne, error: error)
            }
        }
        
    }
    //mark: RequestOrg Api----------------------
    func requestOrg(requester_name: String,requester_contact: String,org_contact_name:String,org_contact: String,org_email: String,name: String,abbreviation:String,org_type:String,description:String,address:String,latitude:Double,longitude:Double,reg_number:String,org_url:String,comments:String,board_member:String,is_prayer_available:String, completion:@escaping (_ result:LoginModel?) -> Void) {
        
        let para: Parameters =
            ["requester_name": requester_name,
             "requester_contact": requester_contact,
             "org_contact_name": org_contact_name,
             "name": name,
             "abbreviation": abbreviation,
             "org_type": org_type,
             "description": description,
             "address": address,
             "latitude": latitude,
             "longitude": longitude,
             "reg_number": reg_number,
             "org_contact": org_contact ,
             "org_email": org_email,
             "org_url": org_url,
             "comments": comments,
             "board_member": board_member,
             "is_prayer_available": is_prayer_available,
        ]
        if let loginToken = UserDefaults.standard.value(forKey: "Login_Token") as? String{
            self.LoginToken = loginToken
            self.headers = [
                //                "Accept": "application/json",
                "Content-Type": "application/json",
                "Authorization":  ("Token " + (self.LoginToken))
            ]
            Alamofire.request(URL(string: "/api/v1/org/onboard/request/", relativeTo: baseURL)!, method: .post, parameters:para , encoding: JSONEncoding.default, headers: headers).response { (resposne) in
                
                self.printRequest(resposne)
                
                do {
                    let data = try self.decoder.decode(LoginModel.self, from: resposne.data ?? Data())
                    print("Success")
                    
                    completion(data)
                    KVNProgress.dismiss()
                    
                    
                    
                    
                } catch let error {
                    KVNProgress.dismiss()
                    self.handleError(data: resposne, error: error)
                }
            }
            
            
        }else{
            //            self.headers = [
            //                //                "Accept": "application/json",
            //                "Content-Type": "application/json",
            //
            //            ]
            //
            Alamofire.request(URL(string: "/api/v1/org/onboard/request/", relativeTo: baseURL)!, method: .post, parameters:para , encoding: URLEncoding.default, headers: nil).response { (resposne) in
                
                self.printRequest(resposne)
                
                do {
                    let data = try self.decoder.decode(LoginModel.self, from: resposne.data ?? Data())
                    print("Success")
                    
                    completion(data)
                    KVNProgress.dismiss()
                    
                    
                    
                    
                } catch let error {
                    KVNProgress.dismiss()
                    self.handleError(data: resposne, error: error)
                }
            }
            
            
        }
        
        
    }
    func orgPriority(token:String,device_id:String,org_id: Int,priority_index: Int, completion:@escaping (_ result:LoginModel?) -> Void) {
        
        if let loginToken = UserDefaults.standard.value(forKey: "Login_Token") as? String{
            self.LoginToken = loginToken
        }
        self.headers = [
            //    "Accept": "application/json",
            "Content-Type": "application/json",
            
        ]
        
        let para: Parameters =
            ["token":token,"device_id":device_id,"org_id": org_id,"priority_index":priority_index]
        
        Alamofire.request(URL(string: "/api/v1/org/priority/", relativeTo: baseURL)!, method: .post, parameters:para , encoding: JSONEncoding.default, headers: headers).response { (resposne) in
            
            self.printRequest(resposne)
            
            do {
                
                let data = try self.decoder.decode(LoginModel.self, from: resposne.data ?? Data())
                print("Success")
                KVNProgress.dismiss()
                
                completion(data)
            } catch let error {
                KVNProgress.dismiss()
                self.handleError(data: resposne, error: error)
            }
        }
        
    }
    func editAdminDetails(email:String,first_name:String,last_name:String,password:String,mobile_number_verified:Bool,privileges:[String:Any],mobile_number:String, completion:@escaping (_ result:EditAdminListModel?) -> Void) {
        
        if let loginToken = UserDefaults.standard.value(forKey: "Login_Token") as? String{
            self.LoginToken = loginToken
        }
        self.headers = [
            //                "Accept": "application/json",
            "Content-Type": "application/json",
            "Authorization": ("Token " + ( self.LoginToken))
        ]
        
        var url = String()
        if let addAdminId_id = UserDefaults.standard.value(forKeyPath: "addAdminId") as? String{
            
            url = "/api/v1/auth/update/?id=" + addAdminId_id
        }
        
        let para: Parameters =
            ["email": email,"first_name":first_name,"last_name":last_name,"password":password,"mobile_number_verified":mobile_number_verified,"privileges":privileges,"mobile_number":mobile_number]
        
        Alamofire.request(URL(string: url, relativeTo: baseURL)!, method: .post, parameters:para , encoding: JSONEncoding.default, headers: headers).response { (resposne) in
            
            self.printRequest(resposne)
            
            do {
                let data = try self.decoder.decode(EditAdminListModel.self, from: resposne.data ?? Data())
                print("Success")
                
                completion(data)
                
                
                KVNProgress.dismiss()
            } catch let error {
                KVNProgress.dismiss()
                self.handleError(data: resposne, error: error)
            }
        }
        
    }
    //marK:ProfileUpdate Api
    func ProfileUpdate(email:String,first_name:String,last_name:String,mobile_number_verified:Bool,mobile_number:String, completion:@escaping (_ result:LoginModel?) -> Void) {
        
        if let loginToken = UserDefaults.standard.value(forKey: "Login_Token") as? String{
            self.LoginToken = loginToken
        }
        self.headers = [
            //                "Accept": "application/json",
            "Content-Type": "application/json",
            "Authorization": ("Token " + ( self.LoginToken))
        ]
        
        var url = String()
        if let UserIdLogin = UserDefaults.standard.value(forKey: "LoginUserId") as? Int{
            let userId = String(UserIdLogin)
            url = "/api/v1/auth/update/?id=" + userId
        }
        
        let para: Parameters =
            ["email": email,"first_name":first_name,"last_name":last_name,"mobile_number_verified":mobile_number_verified,"mobile_number":mobile_number]
        
        Alamofire.request(URL(string: url, relativeTo: baseURL)!, method: .post, parameters:para , encoding: JSONEncoding.default, headers: headers).response { (resposne) in
            
            self.printRequest(resposne)
            
            do {
                let data = try self.decoder.decode(LoginModel.self, from: resposne.data ?? Data())
                print("Success")
                
                completion(data)
                
                
                KVNProgress.dismiss()
            } catch let error {
                KVNProgress.dismiss()
                self.handleError(data: resposne, error: error)
            }
        }
        
    }
    func addAnnouncement(title: String,description: String,image1:String,image2:String,image3:String,image4:String,image5:String,image6:String, completion:@escaping (_ result:LoginModel?) -> Void) {
        if let loginToken = UserDefaults.standard.value(forKey: "Login_Token") as? String{
            self.LoginToken = loginToken
        }
        self.headers = [
            //                "Accept": "application/json",
            "Content-Type": "application/json",
            "Authorization":  ("Token " + ( self.LoginToken))
        ]
        
        let para: Parameters =
            ["title": title,"description":description,"image1":image1,"image2":image2,"image3":image3,"image4":image4,"image5":image5,"image6":image6]
        Alamofire.request(URL(string: "/api/v1/info/announcements/change/", relativeTo: baseURL)!, method: .post, parameters:para , encoding: JSONEncoding.default, headers: headers).response { (resposne) in
            
            self.printRequest(resposne)
            
            do {
                let data = try self.decoder.decode(LoginModel.self, from: resposne.data ?? Data())
                print("Success")
                
                if data.success == true{
                    completion(data)
                }
                
                KVNProgress.dismiss()
                
                
            } catch let error {
                KVNProgress.dismiss()
                self.handleError(data: resposne, error: error)
            }
        }
        
    }
    
    //mark:Logout Api-------------------------
    func logOut(token:String,device_id:String, completion:@escaping (_ result:LoginModel?) -> Void) {
        self.headers = [
            
            "Content-Type":"application/json",
            
        ]
        let para: Parameters =
            ["token": token,"device_id":device_id]
        Alamofire.request(URL(string: "/api/v1/auth/logout/", relativeTo: baseURL)!, method: .post, parameters:para , encoding: JSONEncoding.default, headers: headers).response { (resposne) in
            
            self.printRequest(resposne)
            
            do {
                let data = try self.decoder.decode(LoginModel.self, from: resposne.data ?? Data())
                print("Success")
                if data.success == true{
                    completion(data)
                }
                KVNProgress.dismiss()
                
            } catch let error {
                KVNProgress.dismiss()
                self.handleError(data: resposne, error: error)
            }
        }
        
    }
    func deletOrgApi(id:Int,completion:@escaping (_ result:LoginModel?) -> Void) {
        
        let para = ["id":id]
        self.headers = [
            //                "Accept": "application/json",
            "Content-Type": "application/json",
            "Authorization":  ("Token " + ( self.LoginToken))
        ]
        Alamofire.request(URL(string:"/api/v1/org/delete/", relativeTo: baseURL)!, method: .post, parameters:para , encoding: JSONEncoding.default, headers: headers).response { (resposne) in
            
            self.printRequest(resposne)
            
            do {
                let data = try self.decoder.decode(LoginModel.self, from: resposne.data ?? Data())
                print("Success")
                
                if data.success == true{
                    completion(data)
                }
                
                
                
                
            } catch let error {
                self.handleError(data: resposne, error: error)
            }
        }
        
    }
    
    
    func goingCountActivity(activity: Int,men: Int,women: Int,children: Int,name:String, completion:@escaping (_ result:LoginModel?) -> Void) {
        
        var token: String = ""
        
        if let loginToken = UserDefaults.standard.value(forKey: "Login_Token") as? String{
            self.LoginToken = loginToken
            token = "token=" + self.LoginToken
        }else{
            if let deviceId = UserDefaults.standard.value(forKeyPath: "deviceToken") as? String{ //2.  if user is Anonymous:
                token = "device_id=" + deviceId
            }
            
        }
        let para: Parameters =
            ["activity": activity,"men":men,"women":women,"children":children,"name":name]
        
        self.headers = [
            "Content-Type": "application/json"
        ]
        
        let url = "/api/v1/operations/activity-count/?" + token
        
        Alamofire.request(URL(string: url, relativeTo: baseURL)!, method: .post, parameters:para , encoding: JSONEncoding.default, headers: headers).response { (resposne) in
            
            self.printRequest(resposne)
            
            do {
                
                let data = try self.decoder.decode(LoginModel.self, from: resposne.data ?? Data())
                print("Success")
                KVNProgress.dismiss()
                
                completion(data)
            } catch let error {
                KVNProgress.dismiss()
                self.handleError(data: resposne, error: error)
            }
        }
        
    }
    func goingCountEvent(event: Int,men: Int,women: Int,children: Int,name: String, completion:@escaping (_ result:LoginModel?) -> Void) {
        
        var token: String = ""
        
        if let loginToken = UserDefaults.standard.value(forKey: "Login_Token") as? String{
            self.LoginToken = loginToken
            token = "token=" + self.LoginToken
        }else{
            if let deviceId = UserDefaults.standard.value(forKeyPath: "deviceToken") as? String{ //2.  if user is Anonymous:
                token = "device_id=" + deviceId
            }
            
        }
        let para: Parameters =
            ["event": event,"men":men,"women":women,"children":children,"name":name]
        
        self.headers = [
            "Content-Type": "application/json"
        ]
        
        let url = "/api/v1/operations/event-count/?" + token
        
        Alamofire.request(URL(string: url, relativeTo: baseURL)!, method: .post, parameters:para , encoding: JSONEncoding.default, headers: headers).response { (resposne) in
            
            self.printRequest(resposne)
            
            do {
                
                let data = try self.decoder.decode(LoginModel.self, from: resposne.data ?? Data())
                print("Success")
                KVNProgress.dismiss()
                
                completion(data)
            } catch let error {
                KVNProgress.dismiss()
                self.handleError(data: resposne, error: error)
            }
        }
        
    }
    func feedBackAPi(subject: String,email: String,message: String, completion:@escaping (_ result:LoginModel?) -> Void) {
        
        //  var token: String = ""
        
        //        if let loginToken = UserDefaults.standard.value(forKey: "Login_Token") as? String{
        //            self.LoginToken = loginToken
        //            token = "token=" + self.LoginToken
        //        }else{
        //            if let deviceId = UserDefaults.standard.value(forKeyPath: "deviceToken") as? String{ //2.  if user is Anonymous:
        //                token = "device_id=" + deviceId
        //            }
        //
        //        }
        let para: Parameters =
            ["subject": subject,"email":email,"message":message]
        
        self.headers = [
            "Content-Type": "application/json"
        ]
        
        let url = "/api/v1/feedback/" // + token
        
        Alamofire.request(URL(string: url, relativeTo: baseURL)!, method: .post, parameters:para , encoding: JSONEncoding.default, headers: headers).response { (resposne) in
            
            self.printRequest(resposne)
            
            do {
                
                let data = try self.decoder.decode(LoginModel.self, from: resposne.data ?? Data())
                print("Success")
                KVNProgress.dismiss()
                
                completion(data)
            } catch let error {
                KVNProgress.dismiss()
                self.handleError(data: resposne, error: error)
            }
        }
        
    }
    func deletManageUserApi(user_id:Int,orgId:Int,completion:@escaping (_ result:LoginModel?) -> Void) {
        
        var url = String()
        
        url = "/api/v1/manage/users/?id=" + ("\(user_id)") + "&org_id=" + ("\(orgId)")
        Alamofire.request(URL(string: url, relativeTo: baseURL)!, method: .delete, parameters:nil , encoding: URLEncoding.default, headers: headers).response { (resposne) in
            
            self.printRequest(resposne)
            
            do {
                let data = try self.decoder.decode(LoginModel.self, from: resposne.data ?? Data())
                print("Success")
                completion(data)
                
                
                
                
                
            } catch let error {
                self.handleError(data: resposne, error: error)
            }
        }
        
    }
    func deletAdminApi(user_id:Int,org_id:Int,completion:@escaping (_ result:LoginModel?) -> Void) {
        
        var url = String()
        let para: Parameters =
            ["user_id": user_id,"org_id":org_id]
        self.headers = [
            "Content-Type": "application/json"
        ]
        url = "/api/v1/auth/delete/?id="
        Alamofire.request(URL(string: url, relativeTo: baseURL)!, method: .post, parameters: para, encoding: JSONEncoding.default, headers: headers).response { (resposne) in
            
            self.printRequest(resposne)
            
            do {
                let data = try self.decoder.decode(LoginModel.self, from: resposne.data ?? Data())
                print("Success")
                
                if data.success == true{
                    completion(data)
                }
                
                
                
                
            } catch let error {
                self.handleError(data: resposne, error: error)
            }
        }
        
    }
    func deletAnnouncementApi(completion:@escaping (_ result:LoginModel?) -> Void) {
        
        var url = String()
        if let admin_id = UserDefaults.standard.value(forKeyPath: "announcement_id") as? String{
            url = "/api/v1/info/announcements/change/?id=" + admin_id
        }
        self.headers = [
            //                "Accept": "application/json",
            "Content-Type": "application/json",
            "Authorization":  ("Token " + ( self.LoginToken))
        ]
        Alamofire.request(URL(string: url, relativeTo: baseURL)!, method: .delete, parameters:nil , encoding: URLEncoding.default, headers: headers).response { (resposne) in
            
            self.printRequest(resposne)
            
            do {
                let data = try self.decoder.decode(LoginModel.self, from: resposne.data ?? Data())
                print("Success")
                
                
                completion(data)
                
                
                KVNProgress.dismiss()
                
                
            } catch let error {
                KVNProgress.dismiss()
                
                self.handleError(data: resposne, error: error)
            }
        }
        
    }
    //mark:Add admin api----------------
    func AddAdminApi(email:String,first_name:String,last_name:String,password:String,mobile_number_verified:Bool,privileges:[String:Any],mobile_number:String, completion:@escaping (_ result:LoginModel?) -> Void) {
        
        if let loginToken = UserDefaults.standard.value(forKey: "Login_Token") as? String{
            self.LoginToken = loginToken
        }
        self.headers = [
            //                "Accept": "application/json",
            "Content-Type": "application/json",
            "Authorization":  ("Token " + ( self.LoginToken))
        ]
        
        var url = String()
        if let org_id = UserDefaults.standard.value(forKeyPath: "Org_id") as? String{
            
            url = "/api/v1/auth/create/?org_id="  + org_id
        }
        
        let para: Parameters =
            ["email": email,"first_name":first_name,"last_name":last_name,"password":password,"mobile_number_verified":mobile_number_verified,"privileges":privileges,"mobile_number":mobile_number]
        
        Alamofire.request(URL(string: url, relativeTo: baseURL)!, method: .post, parameters:para , encoding: JSONEncoding.default, headers: headers).response { (resposne) in
            
            self.printRequest(resposne)
            
            do {
                let data = try self.decoder.decode(LoginModel.self, from: resposne.data ?? Data())
                print("Success")
                //  if data.success == true{
                completion(data)
                // }
                KVNProgress.dismiss()
                
            } catch let error {
                self.handleError(data: resposne, error: error)
            }
        }
        
    }
    //MARK:Get Api-------------------------------------------------------
    func getCreateUser(completion:@escaping (_ result:LoginModel?) -> Void) {
        
        var token = String()
        var url = String()
        
        if let deviceId = UserDefaults.standard.value(forKeyPath: "deviceToken") as? String{ //2.  if user is Anonymous:
            token = "device_id=" + deviceId
        }
        self.headers = [
            "Content-Type": "application/json",
            
        ]
        
        url = "/api/v1/auth/device/profile/?" + token
        Alamofire.request(URL(string:url , relativeTo: baseURL)!, method: .get, parameters:nil , encoding: URLEncoding.default, headers: headers).response { (resposne) in
            KVNProgress.dismiss()
            self.printRequest(resposne)
            
            do {
                
                let data = try self.decoder.decode(LoginModel.self, from: resposne.data ?? Data())
                print("Success")
                
                KVNProgress.dismiss()
                completion(data)
                
                
                
            } catch let error {
                KVNProgress.dismiss()
                //self.handleError(data: resposne, error: error)
            }
        }
        
    }
    
    func getPrivilegesData(completion:@escaping (_ result:LoginModel?) -> Void) {
           
          
           var url = String()
           
          if let loginToken = UserDefaults.standard.value(forKey: "Login_Token") as? String{
              self.LoginToken = loginToken
            self.headers = [
                                 "Content-Type": "application/json",
                                 "Authorization":  ("Token " + ( self.LoginToken))
                             ]
                             
          }
     
           url = "/api/v1/auth/user-detail/"
           Alamofire.request(URL(string:url , relativeTo: baseURL)!, method: .get, parameters:nil , encoding: URLEncoding.default, headers: headers).response { (resposne) in
               KVNProgress.dismiss()
               self.printRequest(resposne)
               
               do {
                   
                   let data = try self.decoder.decode(LoginModel.self, from: resposne.data ?? Data())
                   print("Success")
                   
                   KVNProgress.dismiss()
                   completion(data)
                   
                   
                   
               } catch let error {
                   KVNProgress.dismiss()
                   self.handleError(data: resposne, error: error)
               }
           }
           
       }
    
    func getResendOtp(completion:@escaping (_ result:LoginModel?) -> Void) {
        
        var token = String()
        var url = String()
        
        if let deviceId = UserDefaults.standard.value(forKeyPath: "deviceToken") as? String{ //2.  if user is Anonymous:
            token = "device_id=" + deviceId
        }
        url = "/api/v1/auth/device/otp-verify/?" + token
        Alamofire.request(URL(string:url , relativeTo: baseURL)!, method: .get, parameters:nil , encoding: URLEncoding.default, headers: headers).response { (resposne) in
            KVNProgress.dismiss()
            self.printRequest(resposne)
            
            do {
                
                let data = try self.decoder.decode(LoginModel.self, from: resposne.data ?? Data())
                print("Success")
                
                KVNProgress.dismiss()
                completion(data)
                
                
                
            } catch let error {
                KVNProgress.dismiss()
                self.handleError(data: resposne, error: error)
            }
        }
        
    }
    //mark:- Iqama get lists api-------------------
    func IQAMAListsApi(id:Int,completion:@escaping (_ result:IQAMALists?) -> Void) {
        var url = String()
        
        url = "/api/v1/iqama/?org=\(id)&date=" + AppDelegate.instance.currentDate
        Alamofire.request(URL(string: url, relativeTo: baseURL) ?? "", method: .get, parameters: nil , encoding: URLEncoding.default, headers: headers).responseJSON(completionHandler: { (responseObj) in
            KVNProgress.dismiss()
            switch responseObj.result {
            case .success(_):
                do {
                    let data = try self.decoder.decode(IQAMALists.self, from: responseObj.data ?? Data())
                    print("Success")
                    
                    completion(data)
                    
                } catch let error {
                    print("ERROR:\(error.localizedDescription)")
                    //                    self.handleError(data: resposne, error: error)
                }
            case .failure(let error):
                print("Error:\(error.localizedDescription)")
            }
        })
        
        
        //        { (resposne) in
        //            self.printRequest(resposne)
        //        KVNProgress.dismiss()
        //            do {
        //                let data = try self.decoder.decode(IQAMALists.self, from: resposne.data ?? Data())
        //                print("Success")
        //
        //                completion(data)
        //
        //            } catch let error {
        //
        //                self.handleError(data: resposne, error: error)
        //            }
        //        }
    }
    
    func aroundIqamaListApi(completion:@escaping (_ result:AroundMeIQAMALists?) -> Void) {
        AppDelegate.instance.checkLocationPermission { (currentLat, currentLong) in
               AppDelegate.instance.current_date()
            
            var url = String()
            if let loginToken = UserDefaults.standard.value(forKey: "Login_Token") as? String{
                print(loginToken)
                let lat = "/api/v1/iqama/nearme/?latitude=" + "\(String(describing: currentLat.rounded(toPlaces: 6)))"
                let long = lat + "&longitude=" + "\(String(describing: currentLong.rounded(toPlaces: 6)))"
                  //  url =  long + "&token=" + loginToken + "&date=" + "7-02-2020" + "&time=" + "14:05"
              url =  long + "&token=" + loginToken + "&date=" + AppDelegate.instance.currentDate + "&time=" + AppDelegate.instance.currentTime
            }else{
                   AppDelegate.instance.current_date()
                if let deviceId = UserDefaults.standard.value(forKeyPath: "deviceToken") as? String{ //2.  if user is Anonymous:
                    let lat = "/api/v1/iqama/nearme/?latitude=" + "\(String(describing: currentLat.rounded(toPlaces: 6)))"
                    let long = lat + "&longitude=" + "\(String(describing: currentLong.rounded(toPlaces: 6)))"
                    
                url =  long + "&device_id=" + deviceId + "&date=" + AppDelegate.instance.currentDate + "&time=" + AppDelegate.instance.currentTime
                  //  url =  long + "&device_id=" + deviceId + "&date=" + "7-02-2020" + "&time=" + "14:05"
                }
                
                
            }
            
            self.headers = [
                "Content-Type": "application/json",
                
            ]
            
            
            Alamofire.request(URL(string: url, relativeTo: self.baseURL) ?? "", method: .get, parameters: nil , encoding: URLEncoding.default, headers: self.headers).response { (resposne) in
                self.printRequest(resposne)
                do {
                    let data = try self.decoder.decode(AroundMeIQAMALists.self, from: resposne.data ?? Data())
                    print("Success")
                    
                    completion(data)
                    
                    KVNProgress.dismiss()
                } catch let error {
                    KVNProgress.dismiss()
                    self.handleError(data: resposne, error: error)
                }
            }
            
        }
        
        
        
    }
    //mark:- rsvp get api--------------------------
    func getrsvp(types:String,id:Int,completion:@escaping (_ result:LoginModel?) -> Void) {
        
        var token: String = ""
        var url = String()
        if let loginToken = UserDefaults.standard.value(forKey: "Login_Token") as? String{
            print(loginToken)
            token = loginToken
        }else{
            if let deviceId = UserDefaults.standard.value(forKeyPath: "deviceToken") as? String{ //2.  if user is Anonymous:
                token = deviceId
            }
        }
        self.headers = [
            "Content-Type": "application/json",
            "Authorization": ("Token " + token)
        ]
        
        if types == "event"{
            url = "/api/v1/operations/rsvp/?rsvp=T&e_id=" + "\(id)"
        }else{
            url = "/api/v1/operations/rsvp/?rsvp=T&a_id=" + "\(id)"
        }
        
        
        
        Alamofire.request(URL(string: url, relativeTo: baseURL) ?? "", method: .get, parameters: nil , encoding: URLEncoding.default, headers: headers).response { (resposne) in
            self.printRequest(resposne)
            do {
                let data = try self.decoder.decode(LoginModel.self, from: resposne.data ?? Data())
                print("Success")
                
                completion(data)
                
                KVNProgress.dismiss()
            } catch let error {
                KVNProgress.dismiss()
                self.handleError(data: resposne, error: error)
            }
        }
        
        
    }
    //MARK:- Going Count Get Api-----------
    func getGoingCount(types:String,id:Int,completion:@escaping (_ result:GoingCountModel?) -> Void) {
        
        var token: String = ""
        var url = String()
        if let loginToken = UserDefaults.standard.value(forKey: "Login_Token") as? String{
            token = (loginToken)
            self.headers = [
                "Content-Type": "application/json",
                "Authorization": ("Token " + token)
            ]
            
            if types == "event"{
                url = "/api/v1/operations/event-count/?token=" + token + "&event=" + "\(id)"
            }else{
                url = "/api/v1/operations/activity-count/?token=" + token + "&activity=" + "\(id)"
            }
            
        }else{
            if let deviceId = UserDefaults.standard.value(forKeyPath: "deviceToken") as? String{ //2.  if user is Anonymous:
                token = deviceId
            }
            self.headers = [
                "Content-Type": "application/json"
            ]
            
            if types == "event"{
                url = "/api/v1/operations/event-count/?device_id=" + token + "&event=" + "\(id)"
            }else{
                url = "/api/v1/operations/activity-count/?device_id=" + token + "&activity" + "\(id)"
            }
            
        }
        
        Alamofire.request(URL(string: url, relativeTo: baseURL) ?? "", method: .get, parameters: nil , encoding: URLEncoding.default, headers: headers).response { (resposne) in
            self.printRequest(resposne)
            do {
                let data = try self.decoder.decode(GoingData.self, from: resposne.data ?? Data())
                print("Success")
                
                completion(data.data)
                
                KVNProgress.dismiss()
            } catch let error {
                KVNProgress.dismiss()
                self.handleError(data: resposne, error: error)
            }
        }
    }
    func getListOfHomeFeed(keyAll:String,completion:@escaping (_ result:[HomeFeedData]?) -> Void) {
        
        var url = String()
        if keyAll == "Fav"{
            var token: String = ""
            if let loginToken = UserDefaults.standard.value(forKey: "Login_Token") as? String{
                token = (loginToken)
                url = "/api/v1/home/?token=" + token + "&fav=T"
            }else{
                if let deviceId = UserDefaults.standard.value(forKeyPath: "deviceToken") as? String{
                    token = deviceId
                    url = "/api/v1/home/?device_id=" + token + "&fav=T"
                }
            }
            
            
        }else{
            var token: String = ""
            if let loginToken = UserDefaults.standard.value(forKey: "Login_Token") as? String{
                token = (loginToken)
                url = "/api/v1/home/?token=" + token
            }else{
                if let deviceId = UserDefaults.standard.value(forKeyPath: "deviceToken") as? String{
                    token = deviceId
                    url = "/api/v1/home/?device_id=" + token
                }
            }
            //  url = "/api/v1/home/"
        }
        
        self.headers = [
            "Content-Type": "application/json"
            
        ]
        
        
        Alamofire.request(URL(string: url, relativeTo: baseURL)!, method: .get, parameters: nil , encoding: URLEncoding.default, headers: headers).response { (resposne) in
            self.printRequest(resposne)
            do {
                let data = try self.decoder.decode(HomeFeedModel.self, from: resposne.data ?? Data())
                print("Success")
                
                completion(data.data)
                
                KVNProgress.dismiss()
            } catch let error {
                KVNProgress.dismiss()
               //   self.handleError(data: resposne, error: error)
            }
        }
    }
    
    
    func getListOfNewFeed(orgId:Int,completion:@escaping (_ result:[HomeFeedData]?,_ iqamaResult:IQAMAListModel?) -> Void) {
        
        
        self.headers = [
            "Content-Type": "application/x-www-form-urlencoded"
            
        ]
        var url = String()
        let id = "\(orgId)"
        var token: String = ""
        if let loginToken = UserDefaults.standard.value(forKey: "Login_Token") as? String{
            token = (loginToken)
            url = "/api/v1/news-feed/?org_id=" + id + "&date=" + AppDelegate.instance.currentDate + "&token=" + token
        }else{
            if let deviceId = UserDefaults.standard.value(forKeyPath: "deviceToken") as? String{
                token = deviceId
                url = "/api/v1/news-feed/?org_id=" + id + "&date=" + AppDelegate.instance.currentDate + "&device_id=" + token
            }
        }
        
        //   url = "/api/v1/news-feed/?org_id=" + id + "&date=" + AppDelegate.instance.currentDate
        
        
        Alamofire.request(URL(string: url, relativeTo: baseURL) ?? "", method: .get, parameters: nil , encoding: URLEncoding.default, headers: headers).response { (resposne) in
            self.printRequest(resposne)
            do {
                let data = try self.decoder.decode(HomeFeedModel.self, from: resposne.data ?? Data())
                
                
                
              
                
                completion(data.data,data.iq_ju)
                
                KVNProgress.dismiss()
            } catch let error {
                KVNProgress.dismiss()
                self.handleError(data: resposne, error: error)
            }
        }
    }
    //mark:- Get serach List APi-----------------
    
    func getListOfSearchOrganization(searchKey:String,isfav: String, completion:@escaping (_ result:[OrganizationDataLists]?) -> Void) {
        var url = String()
        var token: String = ""
        
        let escapedAddress = searchKey.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed) ?? ""
               
        
        if let loginToken = UserDefaults.standard.value(forKey: "Login_Token") as? String{
            token = (loginToken)
            self.headers = [
                "Content-Type": "application/json",
                "Authorization": ("Token " + token)
            ]
            
            if isfav == "true"{
                url = "/api/v1/org/full-search/?token=" + token + "&keyword=\(escapedAddress)" + "&fav_only=true"
            }else{
                url = "/api/v1/org/full-search/?token=" + token + "&keyword=\(escapedAddress)"
            }
        }else{
            if let deviceId = UserDefaults.standard.value(forKeyPath: "deviceToken") as? String{ //2.  if user is Anonymous:
                token = deviceId
            }
            self.headers = [
                "Content-Type": "application/json"
            ]
            
            if isfav == "true"{
                url = "/api/v1/org/full-search/?device_id=" + token + "&keyword=\(escapedAddress)" + "&fav_only=true"
            }else{
                url = "/api/v1/org/full-search/?device_id=" + token + "&keyword=\(escapedAddress)"
            }
        }
        
        
        
        
        
        Alamofire.request(URL(string: url, relativeTo: baseURL) ?? "", method: .get, parameters: nil , encoding: URLEncoding.default, headers: headers).response { (resposne) in
            self.printRequest(resposne)
            do {
                let data = try self.decoder.decode(OrganizationListModel.self, from: resposne.data ?? Data())
                print("Success")
                
                completion(data.data)
                
                KVNProgress.dismiss()
            } catch let error {
                KVNProgress.dismiss()
                self.handleError(data: resposne, error: error)
            }
        }
    }
    func getListOfSearch_NewsFeed_Organization(searchKey:String,isfav: String,orgId: Int, completion:@escaping (_ result:[HomeFeedData]?) -> Void) {
        var url = String()
        var token: String = ""
        
        
        let escapedAddress = searchKey.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed) ?? ""
               
        
        if let loginToken = UserDefaults.standard.value(forKey: "Login_Token") as? String{
            token = (loginToken)
            self.headers = [
                "Content-Type": "application/json",
                "Authorization": ("Token " + token)
            ]
            //            http://demo2.ongraph.com:5000/api/v1/news-feed/?org_id=23&date=27-09-2019&device_id=1a046dd4e02b1ce4&search=test
            //
            
            
            url = "/api/v1/news-feed/?" + "org_id=\(orgId)" + "&date=\(AppDelegate.instance.currentDate)&" + "token=" + token + "&search=\(escapedAddress)"
            
            
        }else{
            if let deviceId = UserDefaults.standard.value(forKeyPath: "deviceToken") as? String{ //2.  if user is Anonymous:
                token = deviceId
            }
            self.headers = [
                "Content-Type": "application/json"
            ]
            
            url = "/api/v1/news-feed/?" + "org_id=\(orgId)" + "&date=\(AppDelegate.instance.currentDate)&" + "device_id=" + token + "&search=\(escapedAddress)"
            
            
            
        }
        Alamofire.request(URL(string: url, relativeTo: baseURL) ?? "", method: .get, parameters: nil , encoding: URLEncoding.default, headers: headers).response { (resposne) in
            self.printRequest(resposne)
            do {
                let data = try self.decoder.decode(HomeFeedModel.self, from: resposne.data ?? Data())
                print("Success")
                
                completion(data.data)
                
                KVNProgress.dismiss()
            } catch let error {
                KVNProgress.dismiss()
                self.handleError(data: resposne, error: error)
            }
        }
    }
    func getListOfSearch_Home_Organization(searchKey:String,isfav: String, completion:@escaping (_ result:[HomeFeedData]?) -> Void) {
        var url = String()
        var token: String = ""
        
       
        let escapedAddress = searchKey.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
       
       
        if let loginToken = UserDefaults.standard.value(forKey: "Login_Token") as? String{
            token = (loginToken)
            self.headers = [
                "Content-Type": "application/json",
                "Authorization": ("Token " + token)
            ]
            
            if isfav == "true"{
                url = "/api/v1/home/?token=" + token + "&search=\(escapedAddress ?? "")" + "&fav_only=true"
                
            }else{
                url = "/api/v1/home/?token=" + token + "&search=\(escapedAddress ?? "")" //+ "fav_only=false"
                
            }
            
        }else{
            if let deviceId = UserDefaults.standard.value(forKeyPath: "deviceToken") as? String{ //2.  if user is Anonymous:
                token = deviceId
            }
            self.headers = [
                "Content-Type": "application/json"
            ]
            
            if isfav == "true"{
                url =  "/api/v1/home/?device_id=" + token + "&search=\(escapedAddress ?? "")" + "&fav_only=true"
                
            }else{
                url =  "/api/v1/home/?device_id=" + token + "&search=\(escapedAddress ?? "")" //+ "fav_only=false"
                
            }
            
            
            
        }
        Alamofire.request(URL(string: url, relativeTo: baseURL) ?? "", method: .get, parameters: nil , encoding: URLEncoding.default, headers: headers).response { (resposne) in
            self.printRequest(resposne)
            do {
                let data = try self.decoder.decode(HomeFeedModel.self, from: resposne.data ?? Data())
                print("Success")
                
                completion(data.data)
                
                KVNProgress.dismiss()
            } catch let error {
                KVNProgress.dismiss()
                self.handleError(data: resposne, error: error)
            }
        }
    }
    func getListOfSearchAroundMe(searchKey:String,lat:Double,long:Double, completion:@escaping (_ result:GetClassModel?) -> Void) {
        var url = String()
        var token: String = ""
        
        let escapedAddress = searchKey.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed) ?? ""
              
        if let loginToken = UserDefaults.standard.value(forKey: "Login_Token") as? String{
            token = loginToken
            url = "/api/v1/operations/class/search/?keyword=" + escapedAddress + "&latitude=\(lat)" + "&longitude=\(long)" + "&token=" + token
            
            
        }else{
            if let deviceId = UserDefaults.standard.value(forKeyPath: "deviceToken") as? String{ //2.  if user is Anonymous:
                token = deviceId
                
                url = "/api/v1/operations/class/search/?keyword=" + escapedAddress + "&latitude=\(lat)" + "&longitude=\(long)" + "&device_id=" + token
            }
        }
        
        
        
        Alamofire.request(URL(string: url, relativeTo: baseURL) ?? "", method: .get, parameters: nil , encoding: URLEncoding.default, headers: headers).response { (resposne) in
            self.printRequest(resposne)
            do {
                let data = try self.decoder.decode(GetClassModel.self, from: resposne.data ?? Data())
                print("Success")
                
                completion(data)
                
                KVNProgress.dismiss()
            } catch let error {
                KVNProgress.dismiss()
                self.handleError(data: resposne, error: error)
            }
        }
    }
    func getListOfSearchEidSalaha(searchKey:String, completion:@escaping (_ result:EidSalahList??) -> Void) {
        var url = String()
        let escapedAddress = searchKey.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed) ?? ""
               
        var token: String = ""
        if let loginToken = UserDefaults.standard.value(forKey: "Login_Token") as? String{
            token = (loginToken)
            self.headers = [
                "Content-Type": "application/json",
                "Authorization": ("Token " + token)
            ]
            url = "/api/v1/org/eidsalah/?keyword=" + "\(escapedAddress)" + "&token=" + token
        }else{
            if let deviceId = UserDefaults.standard.value(forKeyPath: "deviceToken") as? String{ //2.  if user is Anonymous:
                token = deviceId
            }
            self.headers = [
                "Content-Type": "application/json"
            ]
            url = "/api/v1/org/eidsalah/?keyword=" + "\(escapedAddress)" + "&device_id=" + token
        }
        
        Alamofire.request(URL(string: url, relativeTo: baseURL) ?? "", method: .get, parameters: nil , encoding: URLEncoding.default, headers: headers).response { (resposne) in
            self.printRequest(resposne)
            do {
                let data = try self.decoder.decode(EidSalahList.self, from: resposne.data ?? Data())
                print("Success")
                
                completion(data)
                
                KVNProgress.dismiss()
            } catch let error {
                KVNProgress.dismiss()
                self.handleError(data: resposne, error: error)
            }
        }
    }
    //mark: - Get List of Event PAi
    func getListOfEventInOrganization(orgId: Int, completion:@escaping (_ result:OrgranizationEventListModel?) -> Void) {
        
        var token: String = ""
        var url = String()
        if let loginToken = UserDefaults.standard.value(forKey: "Login_Token") as? String{
            token = (loginToken)
            self.headers = [
                "Content-Type": "application/json",
                "Authorization": ("Token " + token)
            ]
        }else{
            
            self.headers = [
                "Content-Type": "application/json"
            ]
            
        }
        
        if let loginToken = UserDefaults.standard.value(forKey: "Login_Token") as? String{
            token = (loginToken)
            url = "/api/v1/operations/events/?org_id=" + "\(orgId)" + "&token=" + token
            
        }else{
            if let deviceId = UserDefaults.standard.value(forKeyPath: "deviceToken") as? String{
                token = deviceId
                url = "/api/v1/operations/events/?org_id=" + "\(orgId)" + "&device_id=" + token
                
                
            }
        }
        
        
        //let url = "/api/v1/operations/events/?org_id=" + "\(orgId)"
        Alamofire.request(URL(string: url, relativeTo: baseURL)!, method: .get, parameters: nil , encoding: URLEncoding.default, headers: headers).response { (resposne) in
            self.printRequest(resposne)
            do {
                let data = try self.decoder.decode(OrgranizationEventListModel.self, from: resposne.data ?? Data())
                print("Success")
                if data.success == true{
                    completion(data)
                }
                KVNProgress.dismiss()
            } catch let error {
                KVNProgress.dismiss()
                self.handleError(data: resposne, error: error)
            }
        }
    }
    func getListOfDonation(completion:@escaping (_ result:DonationListsModel?) -> Void) {
        
        var token: String = ""
        var url = String()
        if let loginToken = UserDefaults.standard.value(forKey: "Login_Token") as? String{
            token = (loginToken)
            self.headers = [
                "Content-Type": "application/json",
                "Authorization": ("Token " + token)
            ]
        }else{
            
            self.headers = [
                "Content-Type": "application/json"
            ]
            
        }
        
        
        if let loginToken = UserDefaults.standard.value(forKey: "Login_Token") as? String{
            token = (loginToken)
            url = "/api/v1/operations/donations/" + "?token=" + token
            
        }else{
            if let deviceId = UserDefaults.standard.value(forKeyPath: "deviceToken") as? String{
                token = deviceId
                url = "/api/v1/operations/donations/" + "?device_id=" + token
                
                
            }
        }
        
        
        //let url = "/api/v1/operations/events/?org_id=" + "\(orgId)"
        Alamofire.request(URL(string: url, relativeTo: baseURL)!, method: .get, parameters: nil , encoding: URLEncoding.default, headers: headers).response { (resposne) in
            self.printRequest(resposne)
            do {
                let data = try self.decoder.decode(DonationListsModel.self, from: resposne.data ?? Data())
                print("Success")
                if data.success == true{
                    completion(data)
                }
                KVNProgress.dismiss()
            } catch let error {
                KVNProgress.dismiss()
                self.handleError(data: resposne, error: error)
            }
        }
    }
    //Mmark:Export Email
    func getpdfExport(orgId: Int,type:String, completion:@escaping (_ result:LoginModel?) -> Void) {
        
        var token: String = ""
        if let loginToken = UserDefaults.standard.value(forKey: "Login_Token") as? String{
            token = (loginToken)
            self.headers = [
                "Content-Type": "application/json",
                "Authorization": ("Token " + token)
            ]
        }else{
            
            self.headers = [
                "Content-Type": "application/json"
            ]
            
        }
        
        
        
        
        let url = "/api/v1/manage/users/?org_id=" + "\(orgId)" + "&type=" + type
        Alamofire.request(URL(string: url, relativeTo: baseURL)!, method: .get, parameters: nil , encoding: URLEncoding.default, headers: headers).response { (resposne) in
            self.printRequest(resposne)
            do {
                let data = try self.decoder.decode(LoginModel.self, from: resposne.data ?? Data())
                print("Success")
                if data.success == true{
                    completion(data)
                }
                KVNProgress.dismiss()
            } catch let error {
                KVNProgress.dismiss()
                self.handleError(data: resposne, error: error)
            }
        }
    }
    
    
    func getFlag(orgId: Int, completion:@escaping (_ result:FlgModel?) -> Void) {
        
        var token: String = ""
        if let loginToken = UserDefaults.standard.value(forKey: "Login_Token") as? String{
            token = (loginToken)
        }else{
            if let deviceId = UserDefaults.standard.value(forKeyPath: "deviceToken") as? String{ //2.  if user is Anonymous:
                token = deviceId
            }
        }
        
        self.headers = [
            "Content-Type": "application/json",
            "Authorization": ("Token " + token)
        ]
        
        let url = "/api/v1/org/data-flag/?org_id=" + "\(orgId)"
        Alamofire.request(URL(string: url, relativeTo: baseURL)!, method: .get, parameters: nil , encoding: URLEncoding.default, headers: nil).response { (resposne) in
            self.printRequest(resposne)
            do {
                let data = try self.decoder.decode(FlgModel.self, from: resposne.data ?? Data())
                print("Success")
                
                completion(data)
                
                KVNProgress.dismiss()
            } catch let error {
                KVNProgress.dismiss()
                self.handleError(data: resposne, error: error)
            }
        }
    }
    
    //HG
    //mark: - Get List of About Us PAi
    func getListOfAboutUs(orgId: Int, completion:@escaping (_ result:AboutUsModelOrg?) -> Void) {
        
        var token: String = ""
        if let loginToken = UserDefaults.standard.value(forKey: "Login_Token") as? String{
            token = (loginToken)
            self.headers = [
                "Content-Type": "application/json",
                "Authorization": ("Token " + token)
            ]
        }else{
            
            self.headers = [
                "Content-Type": "application/json",
                
            ]
        }
        
        let url = "/api/v1/org/about/?id=" + "\(orgId)"
        Alamofire.request(URL(string: url, relativeTo: baseURL)!, method: .get, parameters: nil , encoding: URLEncoding.default, headers: headers).response { (resposne) in
            self.printRequest(resposne)
            do {
                let data = try self.decoder.decode(AboutUsModelOrg.self, from: resposne.data ?? Data())
                print("Success")
                if data.success == true{
                    completion(data)
                }
                KVNProgress.dismiss()
            } catch let error {
                KVNProgress.dismiss()
                self.handleError(data: resposne, error: error)
            }
        }
    }
    func getListOfActivitiesInOrganization(orgId: Int, completion:@escaping (_ result:OrgranizationEventListModel?) -> Void) {
        
        var token: String = ""
        var url: String = ""
        if let loginToken = UserDefaults.standard.value(forKey: "Login_Token") as? String{
            token = (loginToken)
            self.headers = [
                "Content-Type": "application/json",
                "Authorization": ("Token " + token)
            ]
        }else{
            
            self.headers = [
                "Content-Type": "application/json",
                
            ]
        }
        
        
        if let loginToken = UserDefaults.standard.value(forKey: "Login_Token") as? String{
            token = (loginToken)
            url = "/api/v1/operations/activities/?org_id=" + "\(orgId)" + "&token=" + token
            
        }else{
            if let deviceId = UserDefaults.standard.value(forKeyPath: "deviceToken") as? String{
                token = deviceId
                url = "/api/v1/operations/activities/?org_id=" + "\(orgId)" + "&device_id=" + token
                
                
            }
        }
        
        
        // let url = "/api/v1/operations/activities/?org_id=" + "\(orgId)"
        Alamofire.request(URL(string: url, relativeTo: baseURL)!, method: .get, parameters: nil , encoding: URLEncoding.default, headers: headers).response { (resposne) in
            self.printRequest(resposne)
            do {
                let data = try self.decoder.decode(OrgranizationEventListModel.self, from: resposne.data ?? Data())
                print("Success")
                
                completion(data)
                
                KVNProgress.dismiss()
            } catch let error {
                KVNProgress.dismiss()
                self.handleError(data: resposne, error: error)
            }
        }
    }
    
    //end
    
    //mark: - Search Organization Api
    func searchOrganization(searchString:String, completion:@escaping (_ result:ListOfOrganizationModel?) -> Void) {
        var url = String()
        var token: String = ""
        
         let escapedAddress = searchString.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed) ?? ""
        
        if let loginToken = UserDefaults.standard.value(forKey: "Login_Token") as? String{
            
            token = (loginToken)
            
            url = "/api/v1/org/search/?keyword=" + escapedAddress //+ "?token=" + token
        }else{
            if let deviceId = UserDefaults.standard.value(forKeyPath: "deviceToken") as? String{ //2.  if user is Anonymous:
                token = deviceId
                
                url = "/api/v1/org/search/?keyword=" + escapedAddress //+ "?device_id=" + token
            }
        }
        
        
        self.headers = [
            "Content-Type": "application/json",
            "Authorization": ("Token " + token)
        ]
        
        
        Alamofire.request(URL(string: url, relativeTo: baseURL)!, method: .get, parameters: nil , encoding: URLEncoding.default, headers: headers).response { (resposne) in
            
            self.printRequest(resposne)
            
            do {
                let data = try self.decoder.decode(ListOfOrganizationModel.self, from: resposne.data ?? Data())
                print("Success")
                
                if data.success == true{
                    completion(data)
                }
                
                
                
                
                
            } catch let error {
                self.handleError(data: resposne, error: error)
            }
        }
        
    }
    
    //mark: - Get Eid Salah List
    func getEidSalahList(completion:@escaping (_ result:EidSalahList?) -> Void) {
        
        self.headers = [
            "Content-Type": "application/json",
            // "Authorization": ("Token " + (AppDelegate.instance.AppAdminUser?.Token)!)
        ]
        var url = String()
        if let loginToken = UserDefaults.standard.value(forKey: "Login_Token") as? String{
            
            url =  "/api/v1/org/eidsalah/" + "?token=" + loginToken
        }else{
            if let deviceId = UserDefaults.standard.value(forKeyPath: "deviceToken") as? String{ //2.  if user is Anonymous:
                url =  "/api/v1/org/eidsalah/" + "?device_id=" + deviceId
            }
            
        }
        
        Alamofire.request(URL(string: url, relativeTo: baseURL)!, method: .get, parameters: nil , encoding: URLEncoding.default, headers: headers).response { (resposne) in
            
            self.printRequest(resposne)
            
            do {
                let data = try self.decoder.decode(EidSalahList.self, from: resposne.data ?? Data())
                print("Success")
                if data.success == true{
                    completion(data)
                }
            } catch let error {
                self.handleError(data: resposne, error: error)
            }
        }
        
    }
    
    
    func favUnFav(org_id:Int,is_fav:String, completion:@escaping (_ result:LoginModel?) -> Void) {
        if let loginToken = UserDefaults.standard.value(forKey: "Login_Token") as? String{
            
            self.LoginToken = loginToken
        }
        self.headers = [
            "Content-Type": "application/x-www-form-urlencoded",
            // "Authorization": ("Token " + (AppDelegate.instance.AppAdminUser?.Token)!)
        ]
        
        let para: Parameters =
            ["is_fav": is_fav,"org_id":org_id]
        
        let baseUrl = "/api/v1/org/favourite/"
        var url = String()
        if let loginToken = UserDefaults.standard.value(forKey: "Login_Token") as? String{
            self.LoginToken = loginToken
            url = baseUrl + "?token=" + self.LoginToken
        }else{
            if let deviceId = UserDefaults.standard.value(forKeyPath: "deviceToken") as? String{ //2.  if user is Anonymous:
                url = baseUrl + "?device_id=" + deviceId
            }
            
        }
        
        Alamofire.request(URL(string: url, relativeTo: baseURL)!, method: .get, parameters:para , encoding: URLEncoding.default, headers: headers).response { (resposne) in
            
            self.printRequest(resposne)
            
            do {
                let data = try self.decoder.decode(LoginModel.self, from: resposne.data ?? Data())
                print("Success")
                if data.success == true{
                    completion(data)
                }
                
                
            } catch let error {
                self.handleError(data: resposne, error: error)
            }
        }
        
    }
    
    func getClassesDataLists(orgId:Int,completion:@escaping (_ result:GetClassModel?) -> Void) {
        self.headers = [
            
            "Content-Type": "application/x-www-form-urlencoded",
            
        ]
        if let loginToken = UserDefaults.standard.value(forKey: "Login_Token") as? String{
            
            self.LoginToken = loginToken
        }
        //mark:Url for two condition 1. If user is logged in 2.  if user is Anonymous:
        
        let baseUrl = "/api/v1/operations/classes/"
        var url = String()
        if self.LoginToken != nil{
            url =  baseUrl + "?org_id=" + "\(orgId)"
            //url = url + "?token=" + (self.LoginToken)
        }else{
            if let deviceId = UserDefaults.standard.value(forKeyPath: "deviceToken") as? String{ //2.  if user is Anonymous:
                url = baseUrl  + "?org_id=" + "\(orgId)" //+ "?device_id=" + deviceId
            }
            
        }
        
        Alamofire.request(URL(string: url, relativeTo: baseURL)!, method: .get, parameters:nil , encoding: URLEncoding.default, headers: headers).response { (resposne) in
            
            self.printRequest(resposne)
            
            do {
                let data = try self.decoder.decode(GetClassModel.self, from: resposne.data ?? Data())
                print("Success")
                if data.success == true{
                    completion(data)
                }
                KVNProgress.dismiss()
                
            } catch let error {
                KVNProgress.dismiss()
                self.handleError(data: resposne, error: error)
            }
        }
        
    }
    
    func getClassesAroundMeLists(lat:Double,long:Double,completion:@escaping (_ result:GetClassModel?) -> Void) {
        self.headers = [
            
            "Content-Type": "application/x-www-form-urlencoded",
            
        ]
        
        var token = ""
        if let loginToken = UserDefaults.standard.value(forKey: "Login_Token") as? String{
            self.LoginToken = loginToken
            token = "&token=" + self.LoginToken
        }else{
            if let deviceId = UserDefaults.standard.value(forKeyPath: "deviceToken") as? String{ //2.  if user is Anonymous:
                token = "&device_id=" + deviceId
            }
            
        }
        
        
        //mark:Url for two condition 1. If user is logged in 2.  if user is Anonymous:
        
        let baseUrl = "/api/v1/operations/classes-nearme/?latitude="
        var url = String()
        url =  baseUrl + "\(lat)" + "&longitude=" +  "\(long)" + token
        
        Alamofire.request(URL(string: url, relativeTo: baseURL)!, method: .get, parameters:nil , encoding: URLEncoding.default, headers: headers).response { (resposne) in
            
            self.printRequest(resposne)
            
            do {
                let data = try self.decoder.decode(GetClassModel.self, from: resposne.data ?? Data())
                print("Success")
                if data.success == true{
                    completion(data)
                }
                KVNProgress.dismiss()
                
            } catch let error {
                KVNProgress.dismiss()
                self.handleError(data: resposne, error: error)
            }
        }
        
    }
    func organizationLists(completion:@escaping (_ result:[OrganizationDataLists]?) -> Void) {
        self.headers = [
            
            "Content-Type": "application/x-www-form-urlencoded",
            
        ]
        
        
        if let loginToken = UserDefaults.standard.value(forKey: "Login_Token") as? String{
            
            self.LoginToken = loginToken
        }
        //mark:Url for two condition 1. If user is logged in 2.  if user is Anonymous:
        self.headers = [
            //                "Accept": "application/json",
            "Content-Type": "application/json",
            
        ]
        
        let baseUrl = "/api/v1/org/organizations/"
        var url = String()
        var token = String()
        if let loginToken = UserDefaults.standard.value(forKey: "Login_Token") as? String{
            self.LoginToken = loginToken
            token = "?token=" + self.LoginToken
        }else{
            if let deviceId = UserDefaults.standard.value(forKeyPath: "deviceToken") as? String{ //2.  if user is Anonymous:
                token = "?device_id=" + deviceId
            }
            
        }
        url = baseUrl + token
        
        
        
        Alamofire.request(URL(string: url, relativeTo: baseURL)!, method: .get, parameters:nil , encoding: URLEncoding.default, headers: headers).response { (resposne) in
            
            self.printRequest(resposne)
            
            do {
                let data = try self.decoder.decode(OrganizationListModel.self, from: resposne.data ?? Data())
                print("Success")
                if data.success == true{
                    completion(data.data)
                }
                KVNProgress.dismiss()
                
            } catch let error {
                KVNProgress.dismiss()
                self.handleError(data: resposne, error: error)
            }
        }
        
    }
    func donationLists(keyAll:String,completion:@escaping (_ result:DonationListsModel?) -> Void) {
        
        
        if let loginToken = UserDefaults.standard.value(forKey: "Login_Token") as? String{
            
            self.LoginToken = loginToken
        }
        //mark:Url for two condition 1. If user is logged in 2.  if user is Anonymous:
        
        let baseUrl = "/api/v1/operations/donations/"
        var url = String()
        var token = String()
        
        if keyAll == "FAV"{
            
            if let loginToken = UserDefaults.standard.value(forKey: "Login_Token") as? String{
                token = (loginToken)
                url = "/api/v1/operations/donations/?token=" + token + "&fav=T"
            }else{
                if let deviceId = UserDefaults.standard.value(forKeyPath: "deviceToken") as? String{
                    token = deviceId
                    url = "/api/v1/operations/donations/?device_id=" + token + "&fav=T"
                }
            }
            
            
        }else{
            var token: String = ""
            if let loginToken = UserDefaults.standard.value(forKey: "Login_Token") as? String{
                token = (loginToken)
                url = "/api/v1/operations/donations/?token=" + token
            }else{
                if let deviceId = UserDefaults.standard.value(forKeyPath: "deviceToken") as? String{
                    token = deviceId
                    url = "/api/v1/operations/donations/?device_id=" + token
                }
            }
            
        }
        
        
        
        
        Alamofire.request(URL(string: url, relativeTo: baseURL)!, method: .get, parameters:nil , encoding: URLEncoding.default, headers: headers).response { (resposne) in
            
            self.printRequest(resposne)
            
            do {
                let data = try self.decoder.decode(DonationListsModel.self, from: resposne.data ?? Data())
                print("Success")
                
                completion(data)
                
                KVNProgress.dismiss()
                
            } catch let error {
                KVNProgress.dismiss()
                self.handleError(data: resposne, error: error)
            }
        }
        
    }
    func ManageorganizationUsersLists(org_id: Int, completion:@escaping (_ result:[OrganizationDataLists]?) -> Void) {
        
        if let loginToken = UserDefaults.standard.value(forKey: "Login_Token") as? String{
            
            self.LoginToken = loginToken
        }
        self.headers = [
            
            "Content-Type": "application/x-www-form-urlencoded",
            "Authorization": ("Token " + self.LoginToken)
        ]
        //mark:Url for two condition 1. If user is logged in 2.  if user is Anonymous:
        
        let baseUrl = "api/v1/manage/users/?org_id="
        var url = String()
        url = (baseUrl + "\(org_id)")
        url = url + "&type=list"
        
        
        Alamofire.request(URL(string: url, relativeTo: baseURL)!, method: .get, parameters:nil , encoding: URLEncoding.default, headers: headers).response { (resposne) in
            
            self.printRequest(resposne)
            
            do {
                let data = try self.decoder.decode(OrganizationListModel.self, from: resposne.data ?? Data())
                print("Success")
                if data.success == true{
                    completion(data.data)
                }
                KVNProgress.dismiss()
                
            } catch let error {
                KVNProgress.dismiss()
                self.handleError(data: resposne, error: error)
            }
        }
        
    }
    
    
    
    func ManageorganizationLists(adminType:String,completion:@escaping (_ result:[OrganizationDataLists]?) -> Void) {
        self.headers = [
            
            "Content-Type": "application/x-www-form-urlencoded",
            
        ]
        if let loginToken = UserDefaults.standard.value(forKey: "Login_Token") as? String{
            
            self.LoginToken = loginToken
        }
        //mark:Url for two condition 1. If user is logged in 2.  if user is Anonymous:
        
        let baseUrl = "/api/v1/org/organizations/"
        var url = String()
        if self.LoginToken != nil{
            
            url = baseUrl + "?token=" + (self.LoginToken)
            
            if adminType == "false"{
                url = url + "&admin_page=" + "false"
            }else{
                url = url + "&admin_page=" + "true"
            }
            
            //        if let superAdmin = UserDefaults.standard.value(forKey: "isSuperAdmin") as? Bool{
            //
            //            if superAdmin == false{
            //
            //               url = url + "&admin_page=" + "true"
            //            }else{
            //               url = baseUrl + "?token=" + (self.LoginToken)
            //            }
            //
            //        }
        }
        else{
            if let deviceId = UserDefaults.standard.value(forKeyPath: "deviceToken") as? String{ //2.  if user is Anonymous:
                url = baseUrl + "?device_id=" + deviceId
            }
            
        }
        
        
        
        Alamofire.request(URL(string: url, relativeTo: baseURL)!, method: .get, parameters:nil , encoding: URLEncoding.default, headers: headers).response { (resposne) in
            
            self.printRequest(resposne)
            
            do {
                let data = try self.decoder.decode(OrganizationListModel.self, from: resposne.data ?? Data())
                print("Success")
                if data.success == true{
                    completion(data.data)
                }
                KVNProgress.dismiss()
                
            } catch let error {
                KVNProgress.dismiss()
                self.handleError(data: resposne, error: error)
            }
        }
        
    }
    
    func favOnlyLists(fav_only:Bool,completion:@escaping (_ result:[OrganizationDataLists]?) -> Void) {
        self.headers = [
            
            "Content-Type": "application/x-www-form-urlencoded",
            
        ]
        let para:Parameters = ["fav_only":fav_only]
        //mark:Url for two condition 1. If user is logged in 2.  if user is Anonymous:
        
        let baseUrl = "/api/v1/org/organizations/"
        var url = String()
        if let loginToken = UserDefaults.standard.value(forKey: "Login_Token") as? String{
            LoginToken = loginToken
            url = baseUrl + "?token=" + (LoginToken) + "&"
        }else{
            if let deviceId = UserDefaults.standard.value(forKeyPath: "deviceToken") as? String{ //2.  if user is Anonymous:
                url = baseUrl + "?device_id=" + deviceId + "&"
            }
            
        }
        
        Alamofire.request(URL(string: url, relativeTo: baseURL)!, method: .get, parameters:para , encoding: URLEncoding.default, headers: headers).response { (resposne) in
            
            self.printRequest(resposne)
            
            do {
                let data = try self.decoder.decode(OrganizationListModel.self, from: resposne.data ?? Data())
                print("Success")
                if data.success == true {
                    completion(data.data)
                }
                KVNProgress.dismiss()
                
            } catch let error {
                KVNProgress.dismiss()
                // self.handleError(data: resposne, error: error)
                
            }
        }
        
    }
    
    
    func announcementLists(completion:@escaping (_ result:[AnnouncementData]?) -> Void) {
        self.headers = [
            
            "Content-Type": "application/x-www-form-urlencoded",
            
        ]
        
        //mark:Url for two condition 1. If user is logged in 2.  if user is Anonymous:
        
        _ = "/api/v1/info/announcements/"
        
        let url = "/api/v1/info/announcements/"
        
        //        if AppDelegate.instance.AppAdminUser?.Token != nil{ //1. If user is logged in
        //
        //            url = baseUrl + "?token=" + (AppDelegate.instance.AppAdminUser?.Token)!
        //        }else{
        //            if let deviceId = UserDefaults.standard.value(forKeyPath: "deviceToken") as? String{ //2.  if user is Anonymous:
        //                url = baseUrl + "?device_id=" + deviceId
        //            }
        //
        //        }
        
        Alamofire.request(URL(string: url, relativeTo: baseURL)!, method: .get, parameters:nil , encoding: URLEncoding.default, headers: headers).response { (resposne) in
            
            self.printRequest(resposne)
            
            do {
                let data = try self.decoder.decode(AnnouncmentLists.self, from: resposne.data ?? Data())
                print("Success")
                if data.success == true{
                    completion(data.data)
                }
                KVNProgress.dismiss()
                
            } catch let error {
                KVNProgress.dismiss()
                self.handleError(data: resposne, error: error)
            }
        }
        
    }
    //mark: Oragnization Near me api------------------------
    func organizationListsNearMe(latitude:Double,longitude:Double, completion:@escaping (_ result:[OrganizationDataLists]?) -> Void) {
        self.headers = [
            
            "Content-Type": "application/json",
            
        ]
        
        let baseUrl = "/api/v1/org/nearme/"
        var url = String()
        if let loginToken = UserDefaults.standard.value(forKey: "Login_Token") as? String{
            url = baseUrl + "?token=" + (self.LoginToken)
        }else{
            if let deviceId = UserDefaults.standard.value(forKeyPath: "deviceToken") as? String{ //2.  if user is Anonymous:
                url = baseUrl + "?device_id=" + deviceId
            }
            
        }
        
        let para: Parameters =
            ["latitude": latitude,"longitude": longitude]
        Alamofire.request(URL(string: url, relativeTo: baseURL)!, method: .get, parameters:para , encoding: URLEncoding.default, headers: headers).response { (resposne) in
            
            self.printRequest(resposne)
            
            do {
                let data = try self.decoder.decode(OrganizationListModel.self, from: resposne.data ?? Data())
                print("Success")
                if data.success == true{
                    completion(data.data)
                }
                
                
            } catch let error {
                //KVNProgress.dismiss()
                self.handleError(data: resposne, error: error)
            }
        }
        
    }
    
    //mark: Oragnization detail api------------------------
    func organizationDetails(id:Int, completion:@escaping (_ result:OrgDetailsData?) -> Void) {
        
        let para: Parameters =
            ["id": id]
        let baseUrl = "/api/v1/org/detail/"
        var url = String()
        if let loginToken = UserDefaults.standard.value(forKey: "Login_Token") as? String{
            self.LoginToken = loginToken
            url = baseUrl + "?token=" + (self.LoginToken)
         
        }else{
            if let deviceId = UserDefaults.standard.value(forKeyPath: "deviceToken") as? String{ //2.  if user is Anonymous:
                url = baseUrl + "?device_id=" + deviceId
            }
            
        }
        
        
        Alamofire.request(URL(string: url, relativeTo: baseURL)!, method: .get, parameters:para , encoding: URLEncoding.default, headers: nil).response { (resposne) in
            
            self.printRequest(resposne)
            
            do {
                let data = try self.decoder.decode(OrganizationDetailModel.self, from: resposne.data ?? Data())
                print("Success")
                if data.success == true{
                    completion(data.data)
                }
                KVNProgress.dismiss()
                
            } catch let error {
                KVNProgress.dismiss()
                self.handleError(data: resposne, error: error)
            }
        }
        
    }
    
    //marK:About Us Api---------------------------------
    func aboutUs(completion:@escaping (_ result:AboutData?) -> Void) {
        
        
        
        if let loginToken = UserDefaults.standard.value(forKey: "Login_Token") as? String{
            self.LoginToken = loginToken
            self.headers = [
                //                "Accept": "application/json",
                "Content-Type": "application/x-www-form-urlencoded",
                "Authorization": ("Token " + ( self.LoginToken))
            ]
        }
        
        Alamofire.request(URL(string: "/api/v1/info/aboutus/", relativeTo: baseURL)!, method: .get, parameters:nil , encoding: URLEncoding.default, headers: headers).response { (resposne) in
            
            self.printRequest(resposne)
            
            do {
                let data = try self.decoder.decode(AboutUsModel.self, from: resposne.data ?? Data())
                print("Success")
                if data.success == true{
                    completion(data.data)
                }
                KVNProgress.dismiss()
                
            } catch let error {
                KVNProgress.dismiss()
                self.handleError(data: resposne, error: error)
            }
        }
        
    }
    
    func addServices(completion:@escaping (_ result:[AddServiceData]?) -> Void) {
        
        Alamofire.request(URL(string: "api/v1/org/services/", relativeTo: baseURL)!, method: .get, parameters:nil , encoding: URLEncoding.default, headers: nil).response { (resposne) in
            
            self.printRequest(resposne)
            
            do {
                let data = try self.decoder.decode(AddServicesModel.self, from: resposne.data ?? Data())
                print("Success")
                if data.success == true{
                    completion(data.data)
                }
                KVNProgress.dismiss()
                
            } catch let error {
                KVNProgress.dismiss()
                self.handleError(data: resposne, error: error)
            }
        }
        
    }
    
    //mark:AdminList Api---------------------------------
    func AdminLists(completion:@escaping (_ result:[AdminListSData]?) -> Void) {
        
        var url = String()
        if let org_id = UserDefaults.standard.value(forKeyPath: "Org_id") as? String{
            
            url = "/api/v1/auth/organization/users/?org_id=" + org_id
        }
        Alamofire.request(URL(string: url, relativeTo: baseURL)!, method: .get, parameters:nil , encoding: URLEncoding.default, headers: headers).response { (resposne) in
            
            self.printRequest(resposne)
            
            do {
                let data = try self.decoder.decode(AdminListModel.self, from: resposne.data ?? Data())
                print("Success")
                if data.success == true{
                    completion(data.users)
                }
                KVNProgress.dismiss()
                
            } catch let error {
                KVNProgress.dismiss()
                self.handleError(data: resposne, error: error)
            }
        }
        
    }
    
    
    //MARK:Put Api-------------------------------------------------------
    
    
    //MARK:- Add Manage Users-------------
    func editManageUsers(org_id:Int,email:String,first_name:String,last_name:String,id:Int,mobile_number:String, completion:@escaping (_ result:LoginModel?) -> Void) {
        
        if let loginToken = UserDefaults.standard.value(forKey: "Login_Token") as? String{
            self.LoginToken = loginToken
        }
        self.headers = [
            //                "Accept": "application/json",
            "Content-Type": "application/json",
            "Authorization": ("Token " + ( self.LoginToken))
        ]
        
        
        
        
        
        
        let para: Parameters =
            ["email": email,"first_name":first_name,"last_name":last_name,"id":id,"mobile_number":mobile_number]
        
        Alamofire.request(URL(string: "/api/v1/manage/users/", relativeTo: baseURL)!, method: .put, parameters:para , encoding: JSONEncoding.default, headers: headers).response { (resposne) in
            
            self.printRequest(resposne)
            
            do {
                let data = try self.decoder.decode(LoginModel.self, from: resposne.data ?? Data())
                print("Success")
                // if data.success == true{
                completion(data)
                // }
                
                KVNProgress.dismiss()
            } catch let error {
                KVNProgress.dismiss()
                self.handleError(data: resposne, error: error)
            }
        }
        
    }
    
    func editactivitesOfAboutUs(id: Int,description: String,president: String,gs: String,imam: String,latitude:Double,longitude: Double,address: String,org_email: String,org_contact: String,pre_up_iq: Bool, completion:@escaping (_ result:AboutUsModelOrg?) -> Void) {
        
        var token: String = ""
        if let loginToken = UserDefaults.standard.value(forKey: "Login_Token") as? String{
            token = (loginToken)
        }else{
            if let deviceId = UserDefaults.standard.value(forKeyPath: "deviceToken") as? String{ //2.  if user is Anonymous:
                token = deviceId
            }
        }
        let para: Parameters =
            ["id": id,"description":description,"president":president,"gs":gs,"imam":imam,"latitude":latitude,"longitude":longitude,"address":address,"org_email":org_email,"org_contact":org_contact,"pre_up_iq":pre_up_iq]
        self.headers = [
            "Content-Type": "application/x-www-form-urlencoded",
            "Authorization": ("Token " + token)
        ]
        let url = "/api/v1/org/about/"
        //let url = "/api/v1/org/about/?id=" + "\(orgId)"
        Alamofire.request(URL(string: url, relativeTo: baseURL)!, method: .put, parameters: para , encoding: URLEncoding.default, headers: headers).response { (resposne) in
            self.printRequest(resposne)
            do {
                let data = try self.decoder.decode(AboutUsModelOrg.self, from: resposne.data ?? Data())
                print("Success")
                // if data.success == true{
                completion(data)
                //}
                KVNProgress.dismiss()
            } catch let error {
                KVNProgress.dismiss()
                self.handleError(data: resposne, error: error)
            }
        }
    }
    
    
    func editForEvent(parameters: [String : Any], completion:@escaping (_ result:OrgranizationEventListModel?) -> Void) {
        
        if let loginToken = UserDefaults.standard.value(forKey: "Login_Token") as? String{
            self.LoginToken = loginToken
        }
        let headers: HTTPHeaders = [
            "Authorization":  ("Token " + ( self.LoginToken)),
            "Content-type":  "application/x-www-form-urlencoded"
        ]
        Alamofire.request(URL(string: "/api/v1/operations/events/", relativeTo: baseURL)!, method: .put, parameters:parameters , encoding: URLEncoding.default, headers: headers).response { (resposne) in
            
            self.printRequest(resposne)
            
            do {
                let data = try self.decoder.decode(OrgranizationEventListModel.self, from: resposne.data ?? Data())
                print("Success")
                
                
                completion(data)
                KVNProgress.dismiss()
                
                
            } catch let error {
                KVNProgress.dismiss()
                self.handleError(data: resposne, error: error)
            }
        }
        
    }
    func editForDonation(parameters: [String : Any], completion:@escaping (_ result:OrgranizationEventListModel?) -> Void) {
        
        if let loginToken = UserDefaults.standard.value(forKey: "Login_Token") as? String{
            self.LoginToken = loginToken
        }
        let headers: HTTPHeaders = [
            "Authorization":  ("Token " + ( self.LoginToken)),
            "Content-type":  "application/json"
        ]
        Alamofire.request(URL(string: "/api/v1/operations/donations/", relativeTo: baseURL)!, method: .put, parameters:parameters , encoding: JSONEncoding.default, headers: headers).response { (resposne) in
            
            self.printRequest(resposne)
            
            do {
                let data = try self.decoder.decode(OrgranizationEventListModel.self, from: resposne.data ?? Data())
                print("Success")
                
                
                completion(data)
                KVNProgress.dismiss()
                
                
            } catch let error {
                KVNProgress.dismiss()
                self.handleError(data: resposne, error: error)
            }
        }
        
    }
    func editForActivites(parameters: [String : Any], completion:@escaping (_ result:OrgranizationEventListModel?) -> Void) {
        
        if let loginToken = UserDefaults.standard.value(forKey: "Login_Token") as? String{
            self.LoginToken = loginToken
        }
        let headers: HTTPHeaders = [
            "Authorization":  ("Token " + ( self.LoginToken)),
            "Content-type":  "application/x-www-form-urlencoded"
        ]
        Alamofire.request(URL(string: "/api/v1/operations/activities/", relativeTo: baseURL)!, method: .put, parameters:parameters , encoding: URLEncoding.default, headers: headers).response { (resposne) in
            
            self.printRequest(resposne)
            
            do {
                let data = try self.decoder.decode(OrgranizationEventListModel.self, from: resposne.data ?? Data())
                print("Success")
                
                
                completion(data)
                KVNProgress.dismiss()
                
                
            } catch let error {
                KVNProgress.dismiss()
                self.handleError(data: resposne, error: error)
            }
        }
        
    }
    
    func editForClasses(parameters: [String : Any], completion:@escaping (_ result:GetClassModel?) -> Void) {
        
        if let loginToken = UserDefaults.standard.value(forKey: "Login_Token") as? String{
            self.LoginToken = loginToken
        }
        let headers: HTTPHeaders = [
            "Authorization":  ("Token " + ( self.LoginToken)),
            "Content-type":  "application/x-www-form-urlencoded"
        ]
        Alamofire.request(URL(string: "/api/v1/operations/classes/", relativeTo: baseURL)!, method: .put, parameters:parameters , encoding: URLEncoding.default, headers: headers).response { (resposne) in
            
            self.printRequest(resposne)
            
            do {
                let data = try self.decoder.decode(GetClassModel.self, from: resposne.data ?? Data())
                print("Success")
                
                
                completion(data)
                KVNProgress.dismiss()
                
                
            } catch let error {
                KVNProgress.dismiss()
                self.handleError(data: resposne, error: error)
            }
        }
        
    }
    
    func editAnnouncementApi(imageData: [Data]?,image1:String,image2:String,image3:String,image4:String,image5:String,image6:String,parameters: [String : Any], completion:@escaping (_ result:OrgDetailsData?) -> Void) {
        
        let url = "https://be.muslimhubapp.com/api/v1/info/announcements/change/" /* your API url */
        if let loginToken = UserDefaults.standard.value(forKey: "Login_Token") as? String{
            self.LoginToken = loginToken
        }
        let headers: HTTPHeaders = [
            "Authorization": ("Token " + ( self.LoginToken)),
            "Content-type": "multipart/form-data"
        ]
        
        Alamofire.upload(multipartFormData: { (multipartFormData) in
            for (key, value) in parameters {
                multipartFormData.append("\(value)".data(using: String.Encoding.utf8)!, withName: key as String)
            }
            if imageData != nil{
                
                let count = imageData!.count
                for i in 0..<count{
                    multipartFormData.append(imageData![i], withName:"image\(i)", fileName: "image\(i).jpeg", mimeType: "image/jpeg")
                    
                }
            }
            
        }, usingThreshold: UInt64.init(), to: url, method: .put, headers: headers) { (result) in
            
            
            switch result{
            case .success(let upload, _, _):
                
                upload.response { response in
                    //      print("Request: \(response.request)")
                    // print("Response: \(response.response)")
                    // print("Error: \(response.error)")
                    
                    if let data = response.data, let utf8Text = String(data: data, encoding: .utf8) {
                        print("Data: \(utf8Text)")
                    }
                }
                
            case .failure(let error):
                print("Error in upload: \(error.localizedDescription)")
                //onError?(error)
            }
        }
    }
    //mark: Edit Eid Salah APi
    func editEidSalah(latitude: Double, longitude: Double, address: String, city: String, date: String, salah_time: String, held: Int, attendees: Int, takbir_time: String, parking: Int, khutbah_lang: Int, phone_num: String, info: String, org: Int, added_by: String, completion:@escaping (_ result:EidSalahCommonModel?) -> Void) {
        
        let para: Parameters =
            ["address"          : address,
             "city"             : city,
             "date"             : date,
             "salah_time"       : salah_time,
             "takbir_time"      : takbir_time,
             "held"             : held,
             "attendees"        : attendees,
             "parking"          : parking,
             "latitude"         : latitude,
             "longitude"        : longitude,
             "khutbah_lang"     : khutbah_lang,
             "phone_num"        : phone_num,
             "info"             : info ,
             "added_by"         : added_by
        ]
        if let loginToken = UserDefaults.standard.value(forKey: "Login_Token") as? String{
            self.LoginToken = loginToken
        }
        let url = "api/v1/org/eidsalah/?id=" + "\(org)"
        self.headers = [
            "Content-Type": "application/json",
            "Authorization": ("Token " + ( self.LoginToken))
        ]
        Alamofire.request(URL(string: url, relativeTo: baseURL)!, method: .put, parameters:para , encoding: JSONEncoding.default, headers: headers).response { (resposne) in
            
            self.printRequest(resposne)
            
            do {
                let data = try self.decoder.decode(EidSalahCommonModel.self, from: resposne.data ?? Data())
                print("Success")
                
                
                completion(data)
                
                
                KVNProgress.dismiss()
                
                
            } catch let error {
                KVNProgress.dismiss()
                self.handleError(data: resposne, error: error)
            }
        }
        
    }
    
    func addAnnouncementFromOrg(id: Int, title: String, description: String, image1: String, image2: String, image3: String, image4: String, image5: String, image6: String, completion:@escaping (_ result:LoginModel?) -> Void) {
        if let loginToken = UserDefaults.standard.value(forKey: "Login_Token") as? String{
            self.LoginToken = loginToken
        }
        self.headers = [
            //                "Accept": "application/json",
            "Content-Type": "application/x-www-form-urlencoded",
            "Authorization":  ("Token " + ( self.LoginToken))
        ]
        let para: Parameters =
            ["id": id, "title": title,"description":description,"image1":image1,"image2":image2,"image3":image3,"image4":image4,"image5":image5,"image6":image6]
        
        Alamofire.request(URL(string: "/api/v1/operations/announcement/", relativeTo: baseURL)!, method: .put, parameters:para , encoding: URLEncoding.default, headers: headers).response { (resposne) in
            self.printRequest(resposne)
            
            do {
                let data = try self.decoder.decode(LoginModel.self, from: resposne.data ?? Data())
                print("Success")
                
                
                completion(data)
                KVNProgress.dismiss()
                
                
            } catch let error {
                KVNProgress.dismiss()
                self.handleError(data: resposne, error: error)
            }
        }
        
    }
    
    
    func editAnnouncement(urlPath: String, ordId: Int? = 0 , id: Int,title: String,description: String,image1:String,image2:String,image3:String,image4:String,image5:String,image6:String, completion:@escaping (_ result:LoginModel?) -> Void) {
        if let loginToken = UserDefaults.standard.value(forKey: "Login_Token") as? String{
            self.LoginToken = loginToken
        }
        self.headers = [
            //                "Accept": "application/json",
            "Content-Type": "application/x-www-form-urlencoded",
            "Authorization":  ("Token " + ( self.LoginToken))
        ]
        var para: Parameters!
        if ordId != 0{
            para = ["id": id,
                    "org": ordId ?? 0,
                    "title": title,
                    "description":description,
                    "image1":image1,
                    "image2":image2,
                    "image3":image3,
                    "image4":image4,
                    "image5":image5,
                    "image6":image6
            ]
        }else{
            para =
                ["id": id, "title": title,"description":description,"image1":image1,"image2":image2,"image3":image3,"image4":image4,"image5":image5,"image6":image6]
        }
        
        
        
        Alamofire.request(URL(string: urlPath, relativeTo: baseURL)!, method: .put, parameters:para , encoding: URLEncoding.default, headers: headers).response { (resposne) in
            
            self.printRequest(resposne)
            
            do {
                let data = try self.decoder.decode(LoginModel.self, from: resposne.data ?? Data())
                print("Success")
                
                
                completion(data)
                KVNProgress.dismiss()
                
                
            } catch let error {
                KVNProgress.dismiss()
                self.handleError(data: resposne, error: error)
            }
        }
        
    }
    func aboutUSEdit(id:Int,text:String, completion:@escaping (_ result:AboutUsEditModel?) -> Void) {
        
        let para: Parameters =
            ["id": id,"text":text]
        
        
        let baseUrl = "/api/v1/info/aboutus/"
        var url = String()
        if let loginToken = UserDefaults.standard.value(forKey: "Login_Token") as? String{
            url = baseUrl //+ "?token=" + (self.LoginToken)
            self.headers = [
                //                "Accept": "application/json",
                "Content-Type": "application/x-www-form-urlencoded",
                "Authorization":  ("Token " + ( self.LoginToken))
            ]
        }else{
            if let deviceId = UserDefaults.standard.value(forKeyPath: "deviceToken") as? String{ //2.  if user is Anonymous:
                url = baseUrl //+ "?device_id=" + deviceId
            }
            
        }
        
        
        Alamofire.request(URL(string: url, relativeTo: baseURL)!, method: .put, parameters:para , encoding: URLEncoding.default, headers: headers).response { (resposne) in
            
            self.printRequest(resposne)
            
            do {
                let data = try self.decoder.decode(AboutUsEditModel.self, from: resposne.data ?? Data())
                print("Success")
                if data.success == true{
                    completion(data)
                }
                
                KVNProgress.dismiss()
            } catch let error {
                KVNProgress.dismiss()
                self.handleError(data: resposne, error: error)
            }
        }
        
    }
    //mark: EditOrg APi
    func orgEdit(id: Int,image: String,name:String,logo: String,abbreviation: String,org_type: String,description:String,address:String,latitude:Double,longitude:Double,reg_number:String,admin:[Any],org_contact_name:String,org_contact:String,org_email:String,org_url:String,comments:String,donation_allowed:Bool,tax_deductable:Bool,poli_pay_id:String,is_private:Bool,services:[Int],auth_code:String,merchant_code:String, completion:@escaping (_ result:OrganizationDetailModel?) -> Void) {
        
        let para: Parameters =
            ["id":id,
             "image":image,
             "logo":logo,
             "name": name,
             "abbreviation": abbreviation,
             "org_type": org_type,
             "description": description,
             "address": address,
             "latitude": latitude,
             "longitude": longitude,
             "reg_number": reg_number,
             "org_contact_name": org_contact_name,
             "org_contact": org_contact ,
             "org_email": org_email,
             "org_url": org_url,
             "comments": comments,
             "donation_allowed": donation_allowed,
             "poli_pay_id": poli_pay_id,
             "is_private": is_private,
             "services": services,
             "tax_deductable":tax_deductable,
             "auth_code":auth_code,
             "merchant_code":merchant_code
        ]
        if let loginToken = UserDefaults.standard.value(forKey: "Login_Token") as? String{
            self.LoginToken = loginToken
        }
        self.headers = [
            //                "Accept": "application/json",
            "Content-Type": "application/json",
            "Authorization":  ("Token " + ( self.LoginToken))
        ]
        Alamofire.request(URL(string: "/api/v1/org/update/", relativeTo: baseURL)!, method: .put, parameters:para , encoding: JSONEncoding.default, headers: headers).response { (resposne) in
            
            self.printRequest(resposne)
            
            do {
                let data = try self.decoder.decode(OrganizationDetailModel.self, from: resposne.data ?? Data())
                print("Success")
                
                // if data.success == true{
                completion(data)
                
                //                }else
                //                {
                //                    if KVNProgress.isVisible(){
                //                        KVNProgress.dismiss()
                //                    }
                //                  AKAlertController.alert("", message: data.message ?? "Something went worng")
                //                }
                
                
                
            } catch let error {
                KVNProgress.dismiss()
                self.handleError(data: resposne, error: error)
            }
        }
        
    }
    func AdminEditOnlyServices(id: Int,services:[Int], completion:@escaping (_ result:OrganizationDetailModel?) -> Void) {
        
        let para: Parameters =
            ["id":id,
             "services": services
        ]
        if let loginToken = UserDefaults.standard.value(forKey: "Login_Token") as? String{
            self.LoginToken = loginToken
        }
        self.headers = [
            //                "Accept": "application/json",
            "Content-Type": "application/json",
            "Authorization":  ("Token " + ( self.LoginToken))
        ]
        Alamofire.request(URL(string: "/api/v1/org/update/", relativeTo: baseURL)!, method: .put, parameters:para , encoding: JSONEncoding.default, headers: headers).response { (resposne) in
            
            self.printRequest(resposne)
            
            do {
                let data = try self.decoder.decode(OrganizationDetailModel.self, from: resposne.data ?? Data())
                print("Success")
                
                if data.success == true{
                    completion(data)
                    
                }else
                {
                    if KVNProgress.isVisible(){
                        KVNProgress.dismiss()
                    }
                    AKAlertController.alert("", message: data.message ?? "Something went worng")
                }
                
                
                
            } catch let error {
                KVNProgress.dismiss()
                self.handleError(data: resposne, error: error)
            }
        }
        
    }
    func AdminEdit(id: Int,name:String,abbreviation: String, completion:@escaping (_ result:OrganizationDetailModel?) -> Void) {
        
        let para: Parameters =
            ["id":id,
             "name": name,
             "abbreviation": abbreviation
        ]
        if let loginToken = UserDefaults.standard.value(forKey: "Login_Token") as? String{
            self.LoginToken = loginToken
        }
        self.headers = [
            //                "Accept": "application/json",
            "Content-Type": "application/json",
            "Authorization":  ("Token " + ( self.LoginToken))
        ]
        Alamofire.request(URL(string: "/api/v1/org/update/", relativeTo: baseURL)!, method: .put, parameters:para , encoding: JSONEncoding.default, headers: headers).response { (resposne) in
            
            self.printRequest(resposne)
            
            do {
                let data = try self.decoder.decode(OrganizationDetailModel.self, from: resposne.data ?? Data())
                print("Success")
                
                if data.success == true{
                    completion(data)
                    
                }else
                {
                    if KVNProgress.isVisible(){
                        KVNProgress.dismiss()
                    }
                    AKAlertController.alert("", message: data.message ?? "Something went worng")
                }
                
                
                
            } catch let error {
                KVNProgress.dismiss()
                self.handleError(data: resposne, error: error)
            }
        }
        
    }
    func generateRandomDigits(_ digitNumber: Int) -> String {
        var number = ""
        for i in 0..<digitNumber {
            var randomNumber = arc4random_uniform(10)
            while randomNumber == 0 && i == 0 {
                randomNumber = arc4random_uniform(10)
            }
            number += "\(randomNumber)"
        }
        return number
    }
    
}



extension Network {
    // to hadle error of api on response
    private func handleError(data: DefaultDataResponse?, error:Error?){
        if data?.response == nil {
            KVNProgress.dismiss()
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                AKAlertController.actionSheet("", message: "Internet connection failed.", sourceView: UIApplication.shared.keyWindow?.rootViewController?.presentedViewController?.view as Any, buttons: ["Try Again"], color: nil, tapBlock: nil)
            }
            return
        }
        guard let errorV = try? self.decoder.decode(ErrorModel.self, from: data?.data ?? Data()) else {
            KVNProgress.dismiss()
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                //  print(error)
                
                AKAlertController.alert("", message: error?.localizedDescription ?? "")
                return
            }
            return
        }
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            guard errorV.code != 200 else { return }
            
            AKAlertController.alert("", message: errorV.message)
            return
        }
    }
    
    // To print request
    private func printRequest(_ response: DefaultDataResponse) {
        
        let decryptParams = (try? JSONSerialization.jsonObject(with: response.request?.httpBody ?? Data(), options: []))
        
        //  let decryptParam = try? Network.decryptApiResponse(responseData: (decryptParams?["data"] as? String ?? "").data(using: .utf8) ?? Data())
        
        printDebug(
            """
            \n
            API Request:-
            --------------
            URL         : \(response.request?.url?.absoluteString ?? "")
            Headers     : \(response.request?.allHTTPHeaderFields ?? [:])
            HTTP Method : \(response.request?.httpMethod ?? "")
            Parameters  : \(String(bytes: response.request?.httpBody ?? Data(), encoding: .utf8) ?? "")
            RequestStatusCode: \(response.response?.statusCode ?? 0)
            
            Response    : \(String(bytes: response.data ?? Data(), encoding: .utf8) ?? "")
            \n\n
            """
        )
        
        do {
            let decodedJson = try JSONSerialization.jsonObject(with: response.data ?? Data(), options: [])
            if let json = decodedJson as? Dictionary<String, Any>, (json["code"] as? Int) == 401 {
                //                AppUserDefaults.removeAllValues()
                //               AppDelegate.instance.setRootViewController()
            }
        } catch _ {
        }
    }
    
}

func printDebug<T>(_ obj: T) {
    //MARK:- Comment Line to disable all logs
    print(obj)
}

func jsonString(from object:Any) -> String? {
    guard let data = try? JSONSerialization.data(withJSONObject: object, options: []) else {
        return nil
    }
    return String(data: data, encoding: String.Encoding.utf8)
}




class MyError: NSError {
    var customDescription: String = ""
    init() {
        super.init(domain: "MyError", code: 100, userInfo: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
extension Data {
    
    init<T>(fromArray values: [T]) {
        var values = values
        self.init(buffer: UnsafeBufferPointer(start: &values, count: values.count))
    }
    
    func toArray<T>(type: T.Type) -> [T] {
        return self.withUnsafeBytes {
            [T](UnsafeBufferPointer(start: $0, count: self.count/MemoryLayout<T>.stride))
        }
    }
}

// MARK: - Mixed string utils and helpers

extension String {
    func base64Encoded() -> String? {
        return data(using: .utf8)?.base64EncodedString()
    }
    
    func base64Decoded() -> String? {
        guard let data = Data(base64Encoded: self) else { return nil }
        return String(data: data, encoding: .utf8)
    }
}
