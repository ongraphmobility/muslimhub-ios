//
//  Global.swift
//  Muslim Hub
//
//  Created by Ongraph on 28/06/19.
//  Copyright © 2019 OnGraph. All rights reserved.
//

import Foundation
import UIKit

class Global {
class var shared : Global {
    
    struct Static {
        static let instance : Global = Global()
    }
    return Static.instance
    
}
    var defaults = UserDefaults.standard
    var priviligesData: [PriviligesData] {
        get{
            
            if let data = defaults.value(forKey:defaultsKeys.kOrg_Permission) as? Data {
                let object = (try? PropertyListDecoder().decode(Array<PriviligesData>.self, from: data)) ?? []
                  return object
            }
            else{
                   return []
              }
//            let data = defaults.object(forKey: defaultsKeys.kOrg_Permission) as! NSData
//            if let object = NSKeyedUnarchiver.unarchiveObject(with: data as Data) as? [PriviligesData]{
//                return object
//            }
//            else{
//                return []
//            }
          
        }
        set (orgPermission){
            defaults.set(try? PropertyListEncoder().encode(orgPermission), forKey: defaultsKeys.kOrg_Permission)
//            let encodedData: Data = NSKeyedArchiver.archivedData(withRootObject: orgPermission)
//            defaults.set(encodedData, forKey: defaultsKeys.kOrg_Permission)
            defaults.synchronize()
        }
    }
}

struct defaultsKeys
{
    static let kOrg_Permission = "Org_Permission"
}
