//
//  Extensions.swift
//  BSC
//
//  Created by Sakshi Singh on 02/10/17.
//  Copyright © 2017 Sakshi Singh. All rights reserved.
//

import UIKit
import MapKit

let GOOGLE_API_KEY = "AIzaSyC13lZI9i2FSLjwGDrWn0XcSgW5SxzUtDM"

extension UIView {
    
    @IBInspectable var corners: CGFloat {
        get {
            return layer.cornerRadius
        }
        set {
            layer.cornerRadius = newValue
            layer.masksToBounds = newValue > 0
        }
    }
    
    @IBInspectable var borderColor: UIColor {
        get {
            guard let color = layer.borderColor else {
                return UIColor.clear
            }
            return UIColor(cgColor: color)
        }
        set {
            layer.borderColor = newValue.cgColor
            layer.masksToBounds = true
        }
    }
    
    @IBInspectable var borderWidth: CGFloat {
        get {
            return layer.borderWidth
        }
        set {
            layer.borderWidth = newValue
            layer.masksToBounds = newValue > 0
        }
    }
    
    
    @IBInspectable var maxLength: Int {
        get {
            return 10
        }
        set {
            
        }
    }
    
    
    
}
extension String {
    func validateEmail() -> Bool {
        let emailRegex = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,6}"
        return NSPredicate(format: "SELF MATCHES %@", emailRegex).evaluate(with: self)
    }
    
    var isValidURL: Bool {
        let detector = try! NSDataDetector(types: NSTextCheckingResult.CheckingType.link.rawValue)
        if let match = detector.firstMatch(in: self, options: [], range: NSRange(location: 0, length: self.utf16.count)) {
            // it is a link, if the match covers the whole string
            return match.range.length == self.utf16.count
        } else {
            return false
        }
    }
    
}

extension UIView {
    class func viewFromNib<T : UIView>() -> T {
        return Bundle.main.loadNibNamed(String(describing: T.self), owner: nil, options: nil)![0] as! T
    }
    class func loadNib() -> UINib {
        return UINib(nibName: String(describing: self), bundle: Bundle.main)
    }
    
    
    func addDashedBorder(strokeColor: UIColor, lineWidth: CGFloat) {
        backgroundColor = .clear
        
        let shapeLayer = CAShapeLayer()
        shapeLayer.name = "DashedTopLine"
        shapeLayer.bounds = bounds
        shapeLayer.position = CGPoint(x: frame.width / 2, y: frame.height / 2)
        shapeLayer.fillColor = UIColor.clear.cgColor
        shapeLayer.strokeColor = strokeColor.cgColor
        shapeLayer.lineWidth = lineWidth
        shapeLayer.lineJoin = CAShapeLayerLineJoin.round
        shapeLayer.lineDashPattern = [4, 4]
        
        let path = CGMutablePath()
        path.move(to: CGPoint.zero)
        path.addLine(to: CGPoint(x: frame.width, y: 0))
        shapeLayer.path = path
        
        layer.addSublayer(shapeLayer)
    }
    
    
    
    
    
}

extension UIView {
    
    var x: CGFloat {
        get {
            return self.frame.origin.x
        } set {
            self.frame.origin.x = newValue
        }
    }
    
    var y: CGFloat {
        get {
            return self.frame.origin.y
        } set {
            self.frame.origin.y = newValue
        }
    }
    
    var midX: CGFloat {
        return self.frame.midX
    }
    
    var maxX: CGFloat {
        return self.frame.maxX
    }
    
    var midY: CGFloat {
        return self.frame.midY
    }
    
    var maxY: CGFloat {
        return self.frame.maxY
    }
    
    var width: CGFloat {
        get {
            return self.frame.size.width
        } set {
            self.frame.size.width = newValue
        }
    }
    
    var height: CGFloat {
        get {
            return self.frame.size.height
        } set {
            self.frame.size.height = newValue
        }
    }
    
    var size: CGSize {
        get {
            return self.frame.size
        } set {
            self.frame.size = newValue
        }
    }
    
}

extension UIScreen {
    
    static var width: CGFloat {
        return UIScreen.main.bounds.size.width
    }
    
    static var height: CGFloat {
        return UIScreen.main.bounds.size.height
    }
    
    static var size: CGSize {
        return UIScreen.main.bounds.size
    }
}

extension UILabel {
    
    func manageBlendedLayers() {
        self.isOpaque = true
        self.backgroundColor = UIColor.white
    }
    
}

extension UIColor {
    
    /**
     Creates a UIColor object for the given rgb value which can be specified
     as HTML hex color value. For example:
     
     let color = UIColor(rgb: 0x8046A2)
     let colorWithAlpha = UIColor(rgb: 0x8046A2, alpha: 0.5)
     
     - parameter rgb: color value as Int. To be specified as hex literal like 0xff00ff
     - parameter alpha: alpha optional alpha value (default 1.0)
     */
    
    convenience init(rgb: Int, alpha: CGFloat = 1.0) {
        let r = CGFloat((rgb & 0xff0000) >> 16) / 255
        let g = CGFloat((rgb & 0x00ff00) >>  8) / 255
        let b = CGFloat((rgb & 0x0000ff)      ) / 255
        
        self.init(red: r, green: g, blue: b, alpha: alpha)
    }
    
    convenience init(red: Int, green: Int, blue: Int) {
        let r = CGFloat(red) / 255
        let g = CGFloat(green) / 255
        let b = CGFloat(blue) / 255
        
        self.init(red: r, green: g, blue: b, alpha: 1.0)
    }
    
    convenience public init(hex: Int, alpha: CGFloat = 1.0) {
        let red = CGFloat((hex & 0xFF0000) >> 16) / 255.0
        let green = CGFloat((hex & 0xFF00) >> 8) / 255.0
        let blue = CGFloat((hex & 0xFF)) / 255.0
        self.init(red: red, green: green, blue: blue, alpha: alpha)
    }
    
    func as1ptImage() -> UIImage {
        UIGraphicsBeginImageContext(CGSize(width: 1, height: 1))
        setFill()
        UIGraphicsGetCurrentContext()?.fill(CGRect(x: 0, y: 0, width: 1, height: 1))
        let image = UIGraphicsGetImageFromCurrentImageContext() ?? UIImage()
        UIGraphicsEndImageContext()
        return image
    }
    
    
}

@IBDesignable class RoundedButton: UIButton
{
    override func layoutSubviews() {
        super.layoutSubviews()
        
        updateCornerRadius()
    }
    
    @IBInspectable var rounded: Bool = false {
        didSet {
            updateCornerRadius()
        }
    }
    
    func updateCornerRadius() {
        layer.cornerRadius = rounded ? frame.size.height / 2 : 0
    }
}



extension CGPoint {
    static var Center: CGPoint {
        return CGPoint(x: UIScreen.main.bounds.maxX/2, y: UIScreen.main.bounds.maxY/2)
    }
}

extension UIViewController
{
    
    
    
    func showSingleAlertMessage(message : String, subMsg : String, sender : UIViewController,completion: @escaping (_ result: Bool)->()){
        
        
        
        let alert: UIAlertController = UIAlertController(title: message, message:subMsg, preferredStyle: .alert)
        
        var myMutableString = NSMutableAttributedString()
        myMutableString = NSMutableAttributedString(string: message as String, attributes: [NSAttributedString.Key.font:UIFont(name: "Avenir Medium", size: 18.0)!])
        var myMutableStringSubMsg = NSMutableAttributedString()
        myMutableStringSubMsg = NSMutableAttributedString(string: subMsg as String, attributes: [NSAttributedString.Key.font:UIFont(name: "Avenir Medium", size: 18.0)!])
        alert.view.tintColor = UIColor.init(hex: 0x0DC9E4)
        alert.setValue(myMutableString, forKey: "attributedTitle")
        alert.setValue(myMutableStringSubMsg, forKey: "attributedMessage")
        
        let yesAction: UIAlertAction = UIAlertAction(title: "OK", style: .default) { action -> Void in
            //Code for action here
            completion(true)
        }
        alert.addAction(yesAction)
        sender.present(alert, animated: true, completion: nil)
        
    }
    func showTwoBtnsAlertMsg(message : String, subMsg : String, sender : UIViewController,completion: @escaping (_ result: Bool)->()){
        
        let alert: UIAlertController = UIAlertController(title: message, message:subMsg, preferredStyle: .alert)
        
        var myMutableString = NSMutableAttributedString()
        myMutableString = NSMutableAttributedString(string: message as String, attributes: [NSAttributedString.Key.font:UIFont(name: "Avenir Medium", size: 18.0)!])
        alert.view.tintColor = UIColor.init(hex: 0x0DC9E4)
        alert.setValue(myMutableString, forKey: "attributedTitle")
        //   alert.setValue(myMutableString, forKey: "attributedMessage")
        //Create and add first option action
        let action: UIAlertAction = UIAlertAction(title: "Cancel", style: .default) { action -> Void in
            //Code for action here
            completion(false)
        }
        let yesAction: UIAlertAction = UIAlertAction(title: "OK", style: .default) { action -> Void in
            //Code for action here
            completion(true)
        }
        alert.addAction(action)
        alert.addAction(yesAction)
        
        sender.present(alert, animated: true, completion: nil)
        
    }
    
    //NavigationBar Header
    func headernavigation() {
        
        //    navigationController?.setNavigationBarHidden(false, animated: true)
        
        let attributes = [NSAttributedString.Key.font : UIFont(name: "UniversalDoomsday", size: 22)!, NSAttributedString.Key.foregroundColor : UIColor.white]
        self.navigationController?.navigationBar.titleTextAttributes = attributes
        let navBackgroundImage:UIImage! = UIImage(named: "header_iPhone")
        let navimg = navBackgroundImage.resizeImageWith(newSize: CGSize(width: UIScreen.main.bounds.size.width, height: navBackgroundImage.size.height))
        self.navigationController?.navigationBar.setBackgroundImage(navimg, for: .default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        
        self.navigationController?.navigationBar.isTranslucent = false
    }
    
    func convertToString(date: Date, dateformat formatType: String) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = formatType // Your New Date format as per requirement change it own
       
        let newDate: String = dateFormatter.string(from: date) // pass Date here
        print(newDate) // New formatted Date string
        
        return newDate
    }
    
}

extension UIImage{
    
    func resizeImageWith(newSize: CGSize) -> UIImage {
        
        let horizontalRatio = newSize.width / size.width
        let verticalRatio = newSize.height / size.height
        
        let ratio = max(horizontalRatio, verticalRatio)
        let newSize = CGSize(width: size.width * ratio, height: size.height * ratio)
        UIGraphicsBeginImageContextWithOptions(newSize, true, 0)
        draw(in: CGRect(origin: CGPoint(x: 0, y: 0), size: newSize))
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return newImage!
    }
}
extension UIView{
    
    func tableViewCell() -> UITableViewCell? {
        var tableViewcell : UIView? = self
        while(tableViewcell != nil)
        {
            if tableViewcell! is UITableViewCell {
                break
            }
            tableViewcell = tableViewcell!.superview
        }
        return tableViewcell as? UITableViewCell
    }
    
    
    
    func tableViewIndexPath(_ tableView: UITableView) -> IndexPath? {
        if let cell = self.tableViewCell() {
            return tableView.indexPath(for: cell)
        }
        else {
            return nil
        }
    }
    
    func collectionviewCell() -> UICollectionViewCell? {
        var collectionviewCell : UIView? = self
        while(collectionviewCell != nil)
        {
            if collectionviewCell! is UICollectionViewCell {
                break
            }
            collectionviewCell = collectionviewCell!.superview
        }
        return collectionviewCell as? UICollectionViewCell
    }
    
    func collectionViewIndexPath(_ collectionView: UICollectionView) -> IndexPath? {
        if let cell = self.collectionviewCell() {
            return collectionView.indexPath(for: cell)
        }
        else {
            return nil
        }
    }
    
    
    func txtCornerRadius(_ radius: CGFloat) {
        self.layer.cornerRadius = radius
        self.clipsToBounds = true
    }
    
    
    func buttonShowdow() {
        
        self.layer.shadowOpacity = 1
        self.layer.shadowRadius = 9
        self.layer.shadowColor = UIColor(red: 34/255, green: 156/255, blue: 237/255, alpha: 1.0).cgColor
        self.layer.masksToBounds = false
    }
    
    
}

extension UIViewController{
    
    
    
    func secondsToHoursMinutesSeconds (seconds : Int) -> (Int, Int, Int) {
        return (seconds / 3600, (seconds % 3600) / 60, (seconds % 3600) % 60)
    }
    func convertTimeStampIntoDate(_ timeInMs: Double) -> String
    {
        
        let date = Date(timeIntervalSince1970: timeInMs)
        
        let newDateFormatterForWholeApp = DateFormatter()
        
        newDateFormatterForWholeApp.dateFormat = "HH:mm a"
        let enUSPOSIXLocale = Locale(identifier: "en")
        
        
        newDateFormatterForWholeApp.locale = enUSPOSIXLocale
        
        newDateFormatterForWholeApp.timeZone = TimeZone(identifier: "Europe/London")
        
        let dateStr = newDateFormatterForWholeApp.string(from: date)
        return dateStr
    }
    
    
    
    func json(from object:Any) -> String? {
        guard let data = try? JSONSerialization.data(withJSONObject: object, options: []) else {
            return nil
        }
        return String(data: data, encoding: String.Encoding.utf8)
    }
    
    
}
extension Double {
    /// Rounds the double to decimal places value
    func rounded(toPlaces places:Int) -> Double {
        let divisor = pow(10.0, Double(places))
        return (self * divisor).rounded() / divisor
    }
}
extension UINavigationController {
    
    func backToViewController(vc: Any) {
        // iterate to find the type of vc
        for element in viewControllers as Array {
            if "\(type(of: element)).Type" == "\(type(of: vc))" {
                self.popToViewController(element, animated: true)
                break
            }
        }
    }
    
}


extension Date {
    
    func string(with format: String) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = format
        return dateFormatter.string(from: self)
    }
    
    
    func yearsFrom(_ date:Date) -> Int{
        return (Calendar.current as NSCalendar).components(.year, from: date, to: self, options: []).year!
    }
    func monthsFrom(_ date:Date) -> Int{
        return (Calendar.current as NSCalendar).components(.month, from: date, to: self, options: []).month!
    }
    func weeksFrom(_ date:Date) -> Int{
        return (Calendar.current as NSCalendar).components(.weekOfYear, from: date, to: self, options: []).weekOfYear!
    }
    func daysFrom(_ date:Date) -> Int{
        return (Calendar.current as NSCalendar).components(.day, from: date, to: self, options: []).day!
    }
    func hoursFrom(_ date:Date) -> Int{
        return (Calendar.current as NSCalendar).components(.hour, from: date, to: self, options: []).hour!
    }
    func minutesFrom(_ date:Date) -> Int{
        return (Calendar.current as NSCalendar).components(.minute, from: date, to: self, options: []).minute!
    }
    func secondsFrom(_ date:Date) -> Int{
        return (Calendar.current as NSCalendar).components(.second, from: date, to: self, options: []).second!
    }
    func offsetFrom(_ date:Date) -> String {
        if yearsFrom(date)   > 0 { return "\(yearsFrom(date)) \(KYEARS)"   }
        if monthsFrom(date)  > 0 { return "\(monthsFrom(date)) \(KMONTHS)"  }
        if weeksFrom(date)   > 0 { return "\(weeksFrom(date)) \(KWEEKS)"   }
        if daysFrom(date)    > 0 { return "\(daysFrom(date)) \(KDAYS)"    }
        if hoursFrom(date)   > 0 { return "\(hoursFrom(date)) \(KHOURS)"   }
        if minutesFrom(date) > 0 { return "\(minutesFrom(date)) \(KMINUTES)" }
        if secondsFrom(date) > 0 { return "\(secondsFrom(date)) \(KSECONDS)" }
        return ""
    }
    
    
    
}
extension Date {
    
    func getDateFromTimeStamp(date : Date) -> String {
        //        let date = NSDate(timeIntervalSince1970: Double(date)!)
        let dayTimePeriodFormatter = DateFormatter()
        dayTimePeriodFormatter.dateFormat = "EE,MMM dd yyyy"
        dayTimePeriodFormatter.locale = Locale(identifier: "en_US_POSIX")
        let dateString = dayTimePeriodFormatter.string(from: date)
        return dateString
    }
    func timeAgoDisplay(dateCreated : Date,timeStamp: Int) -> String {
        
        
        let calendar = Calendar.current
        let minuteAgo = calendar.date(byAdding: .minute, value: -1, to: Date())!
        let hourAgo = calendar.date(byAdding: .hour, value: -1, to: Date())!
        let dayAgo = calendar.date(byAdding: .day, value: -1, to: Date())!
        let weekAgo = calendar.date(byAdding: .day, value: -7, to: Date())!
        let diff2 = Calendar.current.dateComponents([.hour], from: dateCreated, to: Date()).hour ?? 0
        print(diff2)
        
        if minuteAgo < dateCreated {
            let diff = Calendar.current.dateComponents([.second], from: dateCreated, to: Date()).second ?? 0
            return "just now"
            // return "\(diff) sec ago"
        } else if hourAgo < dateCreated {
            let diff = Calendar.current.dateComponents([.minute], from: dateCreated, to: Date()).minute ?? 0
            return "\(diff) min ago"
        } else if dayAgo < dateCreated {
            let diff = Calendar.current.dateComponents([.hour], from: dateCreated, to: Date()).hour ?? 0
            return "\(diff) hrs ago"
        }
            //        else if dayAgo == dateCreated {
            //            return "yesterday"
            //        }
        else if weekAgo < dateCreated {
            if diff2 > 24 && diff2 < 49{
                return "Yesterday"
            }
            else{
                let dateStr = self.getDateFromTimeStamp(date: dateCreated)
                return dateStr
            }
            
            
            
            
        }
        else{
            let dateStr = self.getDateFromTimeStamp(date: dateCreated)
            return dateStr
            
        }
    }
}

//MARK:For Date------------------
let KTIMEFORMATE = "MM-dd-yyyy HH:mm" //            "createdAt":"12/17/2018 13:11"
let dateFormatterForWholeApp = DateFormatter()
let KLOCALE = "en_US_POSIX"
let KTIMEZONE = "UTC"
let KYEARS = "years"
let KMONTHS = "months"
let KWEEKS = "weeks"
let KDAYS = "days"
let KMINUTES = "minutes"
let KSECONDS = "seconds"
let KHOURS = "hours"

extension UIApplication {
    class var statusBarBackgroundColor: UIColor? {
        get {
            return (shared.value(forKey: "statusBar") as? UIView)?.backgroundColor
        } set {
            (shared.value(forKey: "statusBar") as? UIView)?.backgroundColor = newValue
        }
    }
}
extension UITableViewCell{
    func locationCell(lat:Double,long:Double,title:String){
        
        
        AKAlertController.actionSheet("", message: "Choose a map to view a location",sourceView: (Any).self, buttons: ["Open In Apple Map","Open In Google Map","Cancel"],color:nil, tapBlock: { (_, _, index) in
            if index == 0{
                
                
                let coordinate = CLLocationCoordinate2DMake(lat,long)
                let mapItem = MKMapItem(placemark: MKPlacemark(coordinate: coordinate, addressDictionary:nil))
                let regionDistance:CLLocationDistance = 1000
                let regionSpan = MKCoordinateRegion(center: coordinate, latitudinalMeters: regionDistance, longitudinalMeters: regionDistance)
                
                let options = [
                    MKLaunchOptionsMapCenterKey: NSValue(mkCoordinate: regionSpan.center),
                    MKLaunchOptionsMapSpanKey: NSValue(mkCoordinateSpan: regionSpan.span)
                ]
                mapItem.name = title
                mapItem.openInMaps(launchOptions: options)
                
                
                
            }else if index == 1{
                if (UIApplication.shared.canOpenURL(URL(string:"comgooglemaps://")!)) {
                    UIApplication.shared.openURL(NSURL(string: "comgooglemaps://?saddr=&daddr=\(lat)),\(long))&directionsmode=driving")! as URL)
                } else {
                    UIApplication.shared.openURL(NSURL(string:
                        "https://maps.google.com/?q=@\(lat)),\(long))")! as URL)
                }
                
                
            }else if index == 2{
                return
            }
            
        })
        
        
        
    }
    
    
}
extension UIViewController {
    
    
    func location(lat:Double,long:Double,title:String) {
        
        
        AKAlertController.actionSheet("", message: "Choose a map to view a location",sourceView: (Any).self, buttons: ["Open In Apple Map","Open In Google Map","Cancel"],color:nil, tapBlock: { (_, _, index) in
            if index == 0{
                
                
                let coordinate = CLLocationCoordinate2DMake(lat,long)
                let mapItem = MKMapItem(placemark: MKPlacemark(coordinate: coordinate, addressDictionary:nil))
                let regionDistance:CLLocationDistance = 1000
                let regionSpan = MKCoordinateRegion(center: coordinate, latitudinalMeters: regionDistance, longitudinalMeters: regionDistance)
                
                let options = [
                    MKLaunchOptionsMapCenterKey: NSValue(mkCoordinate: regionSpan.center),
                    MKLaunchOptionsMapSpanKey: NSValue(mkCoordinateSpan: regionSpan.span)
                ]
                mapItem.name = title
                mapItem.openInMaps(launchOptions: options)
            }else if index == 1 {
                if (UIApplication.shared.canOpenURL(URL(string:"comgooglemaps://")!)) {
                    let urllll = "comgooglemaps://?q=\(title)&center=\(lat),\(long)&zoom=14&views=traffic"
                    let escapedAddress = urllll.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
                    UIApplication.shared.open(NSURL(string: escapedAddress ?? "")! as URL)
                } else {
                    UIApplication.shared.open(NSURL(string:
                        "https://maps.google.com/?q=@\(lat),\(long)")! as URL)
                }
                
                
            } else if index == 2{
                return
            }
            
        })
        
        
        
    }
    
    func timeConverter(_ value: String) -> String
    {
        var returnValue: String! = ""
        dateFormatterForWholeApp.dateFormat = KTIMEFORMATE
        dateFormatterForWholeApp.locale = Locale(identifier: KLOCALE)
        dateFormatterForWholeApp.timeZone = TimeZone(abbreviation: KTIMEZONE)
        let date1 = dateFormatterForWholeApp.date(from: value)
        
        if date1 != nil
        {
            dateFormatterForWholeApp.dateFormat = KTIMEFORMATE
            dateFormatterForWholeApp.timeZone = TimeZone.autoupdatingCurrent
            let stringNewDate = dateFormatterForWholeApp.string(from: date1!)
            let date = dateFormatterForWholeApp.date(from: stringNewDate)
            
            let stringValue = Date().offsetFrom(date!)
            let intValue = (Date().offsetFrom(date!) as NSString).integerValue
            
            if stringValue.contains(KDAYS)
            {
                if intValue > 5
                {
                    returnValue = "few days ago"
                }
                else if intValue == 1
                {
                    returnValue = "Yesterday"
                }
                else
                {
                    returnValue = "\(intValue) days ago"
                }
            }
            else if stringValue.contains(KSECONDS) || stringValue == ""
            {
                returnValue = "just now"
            }
            else if stringValue.contains(KMONTHS) || stringValue.contains(KYEARS) || stringValue.contains(KWEEKS)
            {
                returnValue = "few days ago"
            }
            else if stringValue.contains(KMINUTES)
            {
                if intValue == 1
                {
                    returnValue = "a min ago"
                }
                else
                {
                    returnValue = "\(intValue) min ago"
                }
            }
            else if stringValue.contains(KHOURS)
            {
                if intValue == 1
                {
                    returnValue = "an hour ago"
                }
                else
                {
                    returnValue = "\(intValue) hours ago"
                }
            }
            else
            {
                returnValue = "few days ago"
            }
        }
        return returnValue
    }
    
    func hideKeyboardWhenTappedAround() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UIViewController.dismissKeyboard))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }
    
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
}

extension UILabel {
    
    func addTrailing(with trailingText: String, moreText: String, moreTextFont: UIFont, moreTextColor: UIColor) {
        let readMoreText: String = trailingText + moreText
        
        let lengthForVisibleString: Int = self.vissibleTextLength
        let mutableString: String = self.text!
        let trimmedString: String? = (mutableString as NSString).replacingCharacters(in: NSRange(location: lengthForVisibleString, length: ((self.text?.count)! - lengthForVisibleString)), with: "")
        let readMoreLength: Int = (readMoreText.count)
        let trimmedForReadMore: String = (trimmedString! as NSString).replacingCharacters(in: NSRange(location: ((trimmedString?.count ?? 0) - readMoreLength), length: readMoreLength), with: "") + trailingText
        let answerAttributed = NSMutableAttributedString(string: trimmedForReadMore, attributes: [NSAttributedString.Key.font: self.font])
        let readMoreAttributed = NSMutableAttributedString(string: moreText, attributes: [NSAttributedString.Key.font: moreTextFont, NSAttributedString.Key.foregroundColor: moreTextColor])
        answerAttributed.append(readMoreAttributed)
        self.attributedText = answerAttributed
    }
    
    var vissibleTextLength: Int {
        let font: UIFont = self.font
        let mode: NSLineBreakMode = self.lineBreakMode
        let labelWidth: CGFloat = self.frame.size.width
        let labelHeight: CGFloat = self.frame.size.height
        let sizeConstraint = CGSize(width: labelWidth, height: CGFloat.greatestFiniteMagnitude)
        
        let attributes: [AnyHashable: Any] = [NSAttributedString.Key.font: font]
        let attributedText = NSAttributedString(string: self.text!, attributes: attributes as? [NSAttributedString.Key : Any])
        let boundingRect: CGRect = attributedText.boundingRect(with: sizeConstraint, options: .usesLineFragmentOrigin, context: nil)
        
        if boundingRect.size.height > labelHeight {
            var index: Int = 0
            var prev: Int = 0
            let characterSet = CharacterSet.whitespacesAndNewlines
            repeat {
                prev = index
                if mode == NSLineBreakMode.byCharWrapping {
                    index += 1
                } else {
                    index = (self.text! as NSString).rangeOfCharacter(from: characterSet, options: [], range: NSRange(location: index + 1, length: self.text!.count - index - 1)).location
                }
            } while index != NSNotFound && index < self.text!.count && (self.text! as NSString).substring(to: index).boundingRect(with: sizeConstraint, options: .usesLineFragmentOrigin, attributes: attributes as? [NSAttributedString.Key : Any], context: nil).size.height <= labelHeight
            return prev
        }
        return self.text!.count
    }
}
extension UITabBar {
    
    static let height: CGFloat = 60.0
    
    override open func sizeThatFits(_ size: CGSize) -> CGSize {
        guard let window = UIApplication.shared.keyWindow else {
            return super.sizeThatFits(size)
        }
        var sizeThatFits = super.sizeThatFits(size)
        if #available(iOS 11.0, *) {
            sizeThatFits.height = UITabBar.height + window.safeAreaInsets.bottom
        } else {
            sizeThatFits.height = UITabBar.height
        }
        return sizeThatFits
    }
}
extension UIButton {
    func underlineMyText() {
        guard let text = self.titleLabel?.text else { return }
        
        let attributedString = NSMutableAttributedString(string: text)
        attributedString.addAttribute(NSAttributedString.Key.underlineStyle, value: NSUnderlineStyle.single.rawValue, range: NSRange(location: 0, length: text.count))
        
        self.setAttributedTitle(attributedString, for: .normal)
    }
}
extension UIView {
    
    func roundCorners(_ corners: UIRectCorner, radius: CGFloat) {
        let path = UIBezierPath(roundedRect: self.bounds, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
        let mask = CAShapeLayer()
        mask.path = path.cgPath
        self.layer.mask = mask
    }
    
    
    
}
extension Date {
    
    static func today() -> Date {
        return Date()
    }
    
    func next(_ weekday: Weekday, considerToday: Bool = false) -> Date {
        return get(.Next,
                   weekday,
                   considerToday: considerToday)
    }
    
    func previous(_ weekday: Weekday, considerToday: Bool = false) -> Date {
        return get(.Previous,
                   weekday,
                   considerToday: considerToday)
    }
    
    func get(_ direction: SearchDirection,
             _ weekDay: Weekday,
             considerToday consider: Bool = false) -> Date {
        
        let dayName = weekDay.rawValue
        
        let weekdaysName = getWeekDaysInEnglish().map { $0.lowercased() }
        
        assert(weekdaysName.contains(dayName), "weekday symbol should be in form \(weekdaysName)")
        
        let searchWeekdayIndex = weekdaysName.index(of: dayName)! + 1
        
        let calendar = Calendar(identifier: .gregorian)
        
        if consider && calendar.component(.weekday, from: self) == searchWeekdayIndex {
            return self
        }
        
        var nextDateComponent = DateComponents()
        nextDateComponent.weekday = searchWeekdayIndex
        
        
        let date = calendar.nextDate(after: self,
                                     matching: nextDateComponent,
                                     matchingPolicy: .nextTime,
                                     direction: direction.calendarSearchDirection)
        
        return date!
    }
    
}

// MARK: Helper methods
extension Date {
    func getWeekDaysInEnglish() -> [String] {
        var calendar = Calendar(identifier: .gregorian)
        calendar.locale = Locale(identifier: "en_US_POSIX")
        return calendar.weekdaySymbols
    }
    
    enum Weekday: String {
        case monday, tuesday, wednesday, thursday, friday, saturday, sunday
    }
    
    enum SearchDirection {
        case Next
        case Previous
        
        var calendarSearchDirection: Calendar.SearchDirection {
            switch self {
            case .Next:
                return .forward
            case .Previous:
                return .backward
            }
        }
    }
}
//extension UITextField {
//    @IBInspectable var placeholderColor: UIColor {
//        get {
//            return attributedPlaceholder?.attribute(.foregroundColor, at: 0, effectiveRange: nil) as? UIColor ?? .clear
//        }
//        set {
//            guard let attributedPlaceholder = attributedPlaceholder else { return }
//            let attributes: [NSAttributedString.Key: UIColor] = [.foregroundColor: newValue]
//            self.attributedPlaceholder = NSAttributedString(string: attributedPlaceholder.string, attributes: attributes)
//        }
//    }
//}


