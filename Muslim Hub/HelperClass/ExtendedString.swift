//
//  ExtendedString.swift
//  Muslim Hub
//
//  Created by Ongraph on 07/10/19.
//  Copyright © 2019 OnGraph. All rights reserved.
//

import Foundation

extension String {
    static func getString(with msg:Any?) -> String {
        guard let str = msg as? String  else {
            guard let str = msg as? NSNumber  else {
                return ""
            }
            return str.stringValue
        }
        return str
    }
}


final class MHHelper  {
    static let shared = MHHelper()
    private init() {}
    
    func getMessage(from dic: [String:Any])->String {
        guard let msg = dic["Message"] as? String else {
            guard let msg = dic["message"] as? String else {
                guard let msg = dic["Error"] as? String else {
                    guard let msg = dic["error"] as? String else {
                        guard let msg = dic["msg"] as? String else {
                           return ""
                        }
                        return msg
                    }
                    return msg
                }
                return msg
            }
            return msg
        }
        return msg
    }
    
    
    func convertToDate(from strDate: String, currentFormat: String)-> Date {
        let dateFormatter = DateFormatter.init()
        dateFormatter.dateFormat = currentFormat
        let date = dateFormatter.date(from: strDate)
        return date ?? Date()
    }
    
    
    func convertToStrinDate(date: Date, desiredFormat : String = "dd-MM-yyyy")->String {
        let dateFormatter = DateFormatter.init()
        dateFormatter.dateFormat = desiredFormat
        let dateStr = dateFormatter.string(from: date)
        return dateStr
    }
    
    func convertToDateFrom(date: Date, desiredFormat : String = "dd-MM-yyyy")-> Date {
        let strDate = self.convertToStrinDate(date: date, desiredFormat: desiredFormat)
        let myDate = self.convertToDate(from: strDate, currentFormat: desiredFormat)
        return myDate
    }
    
}
