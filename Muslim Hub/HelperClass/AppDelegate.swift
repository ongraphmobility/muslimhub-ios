//
//  AppDelegate.swift
//  Muslim Hub
//
//  Created by Sakshi Singh on 01/04/19.
//  Copyright © 2019 OnGraph. All rights reserved.
//

import UIKit
import CoreData
import SlideMenuControllerSwift
import IQKeyboardManagerSwift
import UserNotifications
import GooglePlaces
import GoogleMaps
import Firebase
import Branch
import FirebaseMessaging
import AWSSNS

typealias  LocationCompletion = ((_ lat :Double, _ lon: Double)-> ())


@UIApplicationMain
 class AppDelegate: UIResponder, UIApplicationDelegate,UNUserNotificationCenterDelegate {
  //  let SNSPlatformApplicationArn = "arn:aws:sns:ap-southeast-2:673977550873:app/APNS_SANDBOX/muslimapns"
         
    var window: UIWindow?
    var leftMenu: MenuViewController!
    var AppAdminUser: LoginUser?
    var deviceTokenString = ""
    var currentDate = ""
    var isLogout = Bool()
    var locationManager = CLLocationManager()
   
    var currentLat:Double!
    var currentLong:Double!
    var currentTime:String!
    var notification_OrgId = [Int]()
    var locCompletion : LocationCompletion?
    var string_PhoneNumber = [String]()
       var string_URL = [String]()
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        
      
       ///
         IQKeyboardManager.shared.previousNextDisplayMode = .alwaysHide
        if #available(iOS 13.0, *) {
           
        } else {
            let statusBar = UIApplication.shared.value(forKeyPath: "statusBarWindow.statusBar") as? UIView
            statusBar?.backgroundColor = UIColor.init(hex: 0x05BDB9)
        }
       
//           let credentialsProvider = AWSCognitoCredentialsProvider(regionType:.APSoutheast2,
//               identityPoolId:"ap-southeast-2:db990eb4-a2ed-479f-a66e-83ac0ceeb40e")
//
//     let configuration = AWSServiceConfiguration(region:.APSoutheast2,         credentialsProvider:credentialsProvider)
//
//  AWSServiceManager.default().defaultServiceConfiguration = configuration
//
        
        let deviceToken = UIDevice.current.identifierForVendor?.uuidString
            //?? "9A758C70-E19A-4731-83E1-ABF64F3314E1"
        UserDefaults.standard.set(deviceToken, forKey: "deviceToken")
       
        UserDefaults.standard.synchronize()
        
        FirebaseApp.configure()
        //marK:notification method---------------------
//        Messaging.messaging().delegate = self
//        Messaging.messaging().isAutoInitEnabled = true
//        UNUserNotificationCenter.current().delegate = self
        DispatchQueue.main.async {
            self.registerForPushNotification()
        }
        if let remoteNotification = launchOptions?[UIApplication.LaunchOptionsKey.remoteNotification] as? NSDictionary {
                 // there is a notification...do stuff...
                 print("dinFInishLaunchingWithOption().. calling didREceiveRemoteNotification")
                 
                 self.application(application, didReceiveRemoteNotification: remoteNotification as [NSObject : AnyObject])
             }
       
        if let curent_font = UserDefaults.standard.value(forKey: "FontSize") as? String{
             UserDefaults.standard.set(curent_font, forKey: "FontSize")
        }else{
              UserDefaults.standard.set("Medium", forKey: "FontSize")
        }
       
            //let UUID = NSUUID().uuidString
      self.sideMenu()
      //   self.deviceIdRegisterAPi()
        
        self.startUpdatingLocation()
        self.current_date()
        
 

        
        
        IQKeyboardManager.shared.enable = true
       
        
        //mark:google place api----------
        
        GMSPlacesClient.provideAPIKey("AIzaSyC13lZI9i2FSLjwGDrWn0XcSgW5SxzUtDM")
        GMSServices.provideAPIKey("AIzaSyC13lZI9i2FSLjwGDrWn0XcSgW5SxzUtDM")
      
      //Branch.setUseTestBranchKey(true)
        
        
        // listener for Branch Deep Link data
        Branch.getInstance().initSession(launchOptions: launchOptions) { (params, error) in
                // do stuff with deep link data (nav to page, display content, etc)
                print(params as? [String: AnyObject] ?? {})
                
            
                
                let orgId =  params?["data"] as? String
                let orgType = params?["type"] as? String
                let typeId = params?["typeId"] as? String
                let MH_Announcement = params?["MH_Announcement"] as? String
                // latest
                let sessionParams = Branch.getInstance().getLatestReferringParams()
                _ = Branch.getInstance().getFirstReferringParams()
                
                let value_Clicked = sessionParams?["+clicked_branch_link"] as? Int
                if value_Clicked == 1 {
                    
                    if MH_Announcement == "true"{
                       self.sideMenu()
                    }else{
                        self.branchIoNavigation(orgId: Int(orgId!) ?? 0, orgType: orgType ?? "",brachio:value_Clicked ?? 0,typeId: Int(typeId!) ?? 0)
                    }
                  
                }

            }
        
     //   FIXME: delete in distribution
//        let tempDeviceToken = "884acab218c8c0535a0ee80f11233188dc5cdcc4e1d2eaa55847714bde586e7c"
//        UserDefaults.standard.set(tempDeviceToken, forKey: "device_Id")
//        self.deviceIdRegisterAPi()
//
        sleep(3)
        
        if let language = UserDefaults.standard.value(forKey: Constant.LANGUAGE) as? String{

            if language == "ar" {
                UIView.appearance().semanticContentAttribute = .forceLeftToRight
               
            }else{
                UIView.appearance().semanticContentAttribute = .forceLeftToRight
            }
        }

        return true
    }
    
    // Respond to URI scheme links
    func application(_ application: UIApplication, open url: URL, sourceApplication: String?, annotation: Any) -> Bool {
        // pass the url to the handle deep link call
        let branchHandled = Branch.getInstance().application(application,
                                                             open: url,
                                                             sourceApplication: sourceApplication,
                                                             annotation: annotation
        )
        if (!branchHandled) {
            // If not handled by Branch, do other deep link routing for the Facebook SDK, Pinterest SDK, etc
        }
        
        // do other deep link routing for the Facebook SDK, Pinterest SDK, etc
        return true
    }
    
    // Respond to Universal Links
    func application(_ application: UIApplication, continue userActivity: NSUserActivity, restorationHandler: @escaping ([Any]?) -> Void) -> Bool {
        // pass the url to the handle deep link call
        Branch.getInstance().continue(userActivity)
        
        return true
    }
    func application(_ app: UIApplication, open url: URL, options: [UIApplication.OpenURLOptionsKey : Any] = [:]) -> Bool {
        //  return Twitter.sharedInstance().application(app, open: url, options: options)
        
        print("URL Scheme",url.scheme!)
        
        Branch.getInstance().application(app, open: url, options: options)
        return true
    }
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any],
                     fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        
        print("dddd", userInfo)
        
        completionHandler(UIBackgroundFetchResult.newData)
        
        Branch.getInstance().handlePushNotification(userInfo)
    }
    
    func application(_ application: UIApplication,
                     continue userActivity: NSUserActivity,
                     restorationHandler: @escaping ([UIUserActivityRestoring]?) -> Void) -> Bool{
        
        return true
    }
    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }
    
    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
        
    }
    
    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
        
    }
    
    func applicationDidBecomeActive(_ application: UIApplication) {
        if let curent_font = UserDefaults.standard.value(forKey: "FontSize") as? String{
            UserDefaults.standard.set(curent_font, forKey: "FontSize")
        }else{
            UserDefaults.standard.set("Medium", forKey: "FontSize")
        }
        
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
        
    }
    
    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
        // Saves changes in the application's managed object context before the application terminates.
        //        self.saveContext()
    }
    
    // MARK: - Core Data stack
    
    lazy var persistentContainer: NSPersistentContainer = {
        /*
         The persistent container for the application. This implementation
         creates and returns a container, having loaded the store for the
         application to it. This property is optional since there are legitimate
         error conditions that could cause the creation of the store to fail.
         */
        let container = NSPersistentContainer(name: "Muslim_Hub")
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                
                /*
                 Typical reasons for an error here include:
                 * The parent directory does not exist, cannot be created, or disallows writing.
                 * The persistent store is not accessible, due to permissions or data protection when the device is locked.
                 * The device is out of space.
                 * The store could not be migrated to the current model version.
                 Check the error message to determine what the actual problem was.
                 */
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()
    
    // MARK: - Core Data Saving support
    
    func saveContext () {
        let context = persistentContainer.viewContext
        if context.hasChanges {
            do {
                try context.save()
            } catch {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                let nserror = error as NSError
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
    }
    
    func sideMenu(){
        
        SlideMenuOptions.hideStatusBar = false
        SlideMenuOptions.leftViewWidth = UIScreen.main.bounds.size.width - 70

        let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
        let mainViewController = storyboard.instantiateViewController(withIdentifier: "TabVC") as! TabVC
        let leftViewController = storyboard.instantiateViewController(withIdentifier: "MenuViewController") as! MenuViewController

        let slideMenuController = SlideMenuController(mainViewController: mainViewController, leftMenuViewController: leftViewController)

        self.window?.rootViewController = slideMenuController
        self.window?.makeKeyAndVisible()
    }
    
    func branchIoNavigation(orgId:Int,orgType:String,brachio:Int,typeId:Int){
        
        SlideMenuOptions.hideStatusBar = false
        SlideMenuOptions.leftViewWidth = UIScreen.main.bounds.size.width - 70
        let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
     
        
        let viewController = mainStoryboard.instantiateViewController(withIdentifier: "ActivitiesViewController") as! ActivitiesViewController
        viewController.orgId = orgId
        viewController.brachio = brachio
        viewController.brachTypeId = typeId
       if orgType == "Events"{
            viewController.selectedIndexActivites = 1
        }else if orgType == "Classes"{
            viewController.selectedIndexActivites = 2
        }else if orgType == "Activities"{
            viewController.selectedIndexActivites = 4
        }else if orgType == "Announcements"{
            viewController.selectedIndexActivites = 0
        }else if orgType == "News Feed"{
            viewController.selectedIndexActivites = 0
        }else if orgType == "News Feed"{
            viewController.selectedIndexActivites = 0
        }
        
        UIApplication.shared.keyWindow?.rootViewController = viewController
        
    }
    
}



extension AppDelegate {
    
    static var instance: AppDelegate {
        return UIApplication.shared.delegate as! AppDelegate
    }
//  func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
//
//        deviceTokenString = deviceToken.hexString()
//        NSLog("Device Token:- \(deviceTokenString)")
//        UserDefaults.standard.set(deviceTokenString, forKey: "deviceToken")
//    }
//
 
    func deviceIdRegisterAPi(){
        if let deviceId = UserDefaults.standard.value(forKeyPath: "deviceToken") as? String{
             let device_token = UserDefaults.standard.value(forKey: "device_Id") as! String  //
                //UserDefaults.standard.value(forKey: "device_Id") as! String
            Network.shared.deviceIdRegister(device_id: deviceId,device_token:device_token,phone_type:1) { (result) in
                
                guard result != nil else {
                    
                    return
                }
                
                if result?.success == true {
                    
                }else{
                    
                }
            }
        }
    }
    
    func current_date(){
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd-MM-yyyy"
        let dateFormatter1 = DateFormatter()
        dateFormatter1.dateFormat = "dd-MM-yyyy"
        currentDate = dateFormatter1.string(from:Date())
        print(currentDate)
        
        
        let timedateFormatter1 = DateFormatter()
        timedateFormatter1.dateFormat = "HH:mm"
        currentTime = timedateFormatter1.string(from: Date())
        
        print(currentTime)
        
        
    }
    
}

extension Data {
    func hexString() -> String {
        return self.reduce("") { string, byte in
            string + String(format: "%02X", byte)
        }
    }
}

//Mark:current Location---------------------------------------
extension AppDelegate :  CLLocationManagerDelegate
{
    
    
    func checkLocationPermission(completion : LocationCompletion?) {
        self.locCompletion = completion
        if CLLocationManager.locationServicesEnabled() {
             switch CLLocationManager.authorizationStatus() {
                case .notDetermined, .restricted, .denied:
                    print("No access")
                    //                    self.startUpdatingLocation()
                    
                    let alertController = UIAlertController(title: "Alert", message: "Please go to Settings and turn on the location permission", preferredStyle: .alert)
                    let settingsAction = UIAlertAction(title: "Settings", style: .default) { (_) -> Void in
                        guard let settingsUrl = URL(string: UIApplication.openSettingsURLString) else {
                            return
                        }
                        if UIApplication.shared.canOpenURL(settingsUrl) {
                            UIApplication.shared.open(settingsUrl, completionHandler: { (success) in })
                        }
                    }
                    let cancelAction = UIAlertAction(title: "Cancel", style: .default, handler: nil)
                    alertController.addAction(cancelAction)
                    alertController.addAction(settingsAction)
                    self.window?.rootViewController?.present(alertController, animated: true, completion: nil)
             case .authorizedAlways, .authorizedWhenInUse:
               /// self.locCompletion?(28.5414,77.3970 )
                    self.locCompletion?(currentLat, currentLong)
                }
            } else {
                print("Location services are not enabled")
        }
    }
    
    func startUpdatingLocation(){
        
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestAlwaysAuthorization()
        locationManager.requestWhenInUseAuthorization()
        
        locationManager.startUpdatingLocation()
    }
    func geocode(latitude: Double, longitude: Double, completion: @escaping (CLPlacemark?, Error?) -> ())  {
        CLGeocoder().reverseGeocodeLocation(CLLocation(latitude: latitude, longitude: longitude)) { completion($0?.first, $1) }
    }
    
    // Below Mehtod will print error if not able to update location.
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print("Error Location:\(error.localizedDescription)")
    }
    
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        //Access the last object from locations to get perfect current location
        if let location = locations.last {
            
            let myLocation = CLLocationCoordinate2DMake(location.coordinate.latitude,location.coordinate.longitude)
            let location = locations.last! as CLLocation
            
            /* you can use these values*/
            self.currentLat = location.coordinate.latitude
            self.currentLong = location.coordinate.longitude
            self.locCompletion?(self.currentLat,self.currentLong)
            geocode(latitude: myLocation.latitude, longitude: myLocation.longitude) { placemark, error in
                guard let placemark = placemark, error == nil else { return }
                // you should always update your UI in the main thread
                DispatchQueue.main.async {
                    //  update UI here
                    print("address1:", placemark.thoroughfare ?? "")
                    print("address2:", placemark.subThoroughfare ?? "")
                    print("city:",     placemark.locality ?? "")
                    print("state:",    placemark.administrativeArea ?? "")
                    print("zip code:", placemark.postalCode ?? "")
                    print("country:",  placemark.country ?? "")
                }
            }
        }
        manager.stopUpdatingLocation()
        
    }
    
    
}
//MARK:ALL Notification Methods------------------------------------------------------
extension AppDelegate:MessagingDelegate{
    
//    func applicationDidEnterBackground(_ application: UIApplication) {
//
//      //  Messaging.messaging().disconnect()
//        print("Disconnected from FCM.")
//    }
//
    // [START receive_message]
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any]) {
        // If you are receiving a notification message while your app is in the background,
        // this callback will not be fired till the user taps on the notification launching the application.
        // TODO: Handle data of notification
    }
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        print("Unable to register for remote notifications: \(error.localizedDescription)")
    }
    
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        // some other way of handling notification
        completionHandler([.alert, .sound])
        
        let userinfo =  notification.request.content.userInfo
       
        let responseDict = NSMutableDictionary(dictionary: userinfo)
        
        let data = responseDict["aps"] as? [String:Any]
              
      
        if data!["data"] != nil{
            self.notification_OrgId.append(data!["data"] as! Int)
        }
        
    }
    
    
    
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        deviceTokenString = deviceToken.map { String(format: "%02.2hhx", $0) }.joined()
     
         UserDefaults.standard.set(deviceTokenString, forKey: "device_Id")
        UserDefaults.standard.synchronize()
        self.deviceIdRegisterAPi()
        
//           // Create a platform endpoint. In this case,  the endpoint is a
//           // device endpoint ARN
//            let sns = AWSSNS.default()
//            let request = AWSSNSCreatePlatformEndpointInput()
//            request?.token = deviceTokenString
//        request?.platformApplicationArn = SNSPlatformApplicationArn
//            sns.createPlatformEndpoint(request!).continueWith(executor: AWSExecutor.mainThread(), block: { (task: AWSTask!) -> AnyObject? in
//                if task.error != nil {
//                    print("Error: \(String(describing: task.error))")
//                } else {
//                    let createEndpointResponse = task.result! as AWSSNSCreateEndpointResponse
//
//                    if let endpointArnForSNS = createEndpointResponse.endpointArn {
//                        print("endpointArn: \(endpointArnForSNS)")
//                        UserDefaults.standard.set(endpointArnForSNS, forKey: "endpointArnForSNS")
//                    }
//                }
//                return nil
//            })
          
        }
    
    
    func registerForPushNotification() {
        if #available(iOS 10.0, *) {
            let center = UNUserNotificationCenter.current()
            center.delegate = self
            center.requestAuthorization(options:[.badge, .alert, .sound]) { (granted, error) in
                // Enable or disable features based on authorization.
                if granted == true {
                    debugPrint("Allow")
                    OperationQueue.main.addOperation {
                        UIApplication.shared.registerForRemoteNotifications()
                    }
                }
                else{
                    debugPrint("Don't Allow")
                }
            }
        } else {
            // Fallback on earlier versions
        }
    }
    func messaging(_ messaging: Messaging, didReceive remoteMessage: MessagingRemoteMessage) {
        print("Received data message: \(remoteMessage.appData)")
    }

    
    func extractTokenFromData(deviceToken:Data) -> String {
        let token = deviceToken.reduce("", {$0 + String(format: "%02X", $1)})
        return token.uppercased();
    }
    
    func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String) {
        print("Firebase registration token: \(fcmToken)")
        UserDefaults.standard.set(fcmToken, forKey: "fcmtoken")
    }

    
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        
        debugPrint("Tapped in notification")
        let userInfo = response.notification.request.content.userInfo
        handleNotification(userInfo, application: UIApplication.shared)
        
         print(response.notification.request.content.userInfo)
         let responseDict = NSMutableDictionary(dictionary: userInfo)
        
        let data = responseDict["aps"] as? [String:Any]
        
        if data!["data"] != nil{
            if self.notification_OrgId.contains(data!["data"] as! Int){
                            
                            self.notification_OrgId.removeAll{$0 == data!["data"] as! Int}
                        }
        }
        
        let MH_Announcement = data!["MH_Announcement"] as! String
        if MH_Announcement == "true"{
            self.sideMenu()
        }else{
            branchIoNavigation(orgId:data!["data"] as! Int,orgType:data!["type"] as! String,brachio:1,typeId:data!["typeId"] as! Int)
            
        }
             
    }
    
 
    
    //MARK: HANDLE NOTIFICATION
    func handleNotification(_ userInfo: [AnyHashable: Any],application:UIApplication)
    {
        UIApplication.shared.applicationIconBadgeNumber = 0
        
        if UIApplication.shared.applicationIconBadgeNumber > 0 {
            
            UIApplication.shared.applicationIconBadgeNumber = UIApplication.shared.applicationIconBadgeNumber - 1
        }
      
    }
}
