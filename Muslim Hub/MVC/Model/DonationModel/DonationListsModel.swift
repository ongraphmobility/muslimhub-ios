//
//  DonationListsModel.swift
//  Muslim Hub
//
//  Created by Ongraph on 07/10/19.
//  Copyright © 2019 OnGraph. All rights reserved.
//

import Foundation
// MARK: - DonationListsModel
struct DonationListsModel: Codable {
    let success: Bool?
    let data: [DonationData]?
}

// MARK: - Datum
struct DonationData: Codable {
    let id: Int?
    let images: [String]?
    let donationLogo, startDate, endDate, poliPayID: String?
    let title, datumDescription: String?
    let taxDeductable: Bool?
    let phoneNumber, email: String?
    let donationType, paymentGateway, amountType: Int?
    let preDIFAmountList: [PreDIFAmountList]?
    let website: String?
    var selectedImage_Array: [Bool]?
    let addedAt, org: Int?
    let org_abbr,org_name:String?
  let merchant_code,auth_code: String?
    enum CodingKeys: String, CodingKey {
        case id, images,org_abbr,org_name
        case donationLogo = "donation_logo"
        case startDate = "start_date"
        case endDate = "end_date"
        case poliPayID = "poli_pay_id"
        case title
        case datumDescription = "description"
        case taxDeductable = "tax_deductable"
        case phoneNumber = "phone_number"
        case email
        case donationType = "donation_type"
        case paymentGateway = "payment_gateway"
        case amountType = "amount_type"
        case preDIFAmountList = "pre_dif_amount_list"
        case website
        case addedAt = "added_at"
        case org
        case selectedImage_Array,merchant_code,auth_code
    }
}

// MARK: - PreDIFAmountList
struct PreDIFAmountList: Codable {
    let amount: Double?
    let preDIFAmountListDescription: String?

    enum CodingKeys: String, CodingKey {
        case amount
        case preDIFAmountListDescription = "description"
    }
}
struct PoliModel: Codable {
    let success: Bool?
    let data: String?
    let Message: String?
    let message: String
}
