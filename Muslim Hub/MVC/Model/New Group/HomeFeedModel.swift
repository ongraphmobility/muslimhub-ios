//
//  HomeFeedModel.swift
//  Muslim Hub
//
//  Created by Ongraph on 11/06/19.
//  Copyright © 2019 OnGraph. All rights reserved.
//


import Foundation

// MARK: - Welcome
struct HomeFeedModel: Codable {
    let success: Bool?
    let data: [HomeFeedData]?
    let iq_ju:IQAMAListModel?
    let message:String?
}

// MARK: - Datum
struct HomeFeedData: Codable {
    let id: Int?
    let org: Int?
    let message: String?
    let org_type:String?
    let title, datumDescription: String
    let latitude, longitude: Double?
    let address, startDate, endDate, startTime: String?
    let donationType, paymentGateway, amountType: Int?
    let endTime: String?
    let repeatingDay: Int?
    let addedAt: Int
    let images: [String]
    let goingCount: Int?
    let activityLogo: String?
    let men, women, children: Int?
    let category: String
    let classInputType: Int?
    let selectClass: Int?
    let userInput: String?
    let attendeesType: Int?
    let ageGroup, teacherName,org_name,org_abbr: String?
    let free: Int?
    let classesLogo, eventLogo,logo,donation_logo: String?
    var selectedImage_Array: [Bool]?
    var isMoreTapped: Bool?
    var going:Bool?
    var is_private:Bool?
    var pre_dif_amount_list: [PreDIFAmountList]?
    let website: String?
    let merchant_code,auth_code: String?
    let phone_number: String?
     let tax_deductable: Bool?
   let email: String?
    enum CodingKeys: String, CodingKey {
        case id, org, title,going,org_type,merchant_code,auth_code,email,phone_number,message
        case datumDescription = "description"
        case latitude, longitude, address
        case tax_deductable
        case startDate = "start_date"
        case endDate = "end_date"
        case startTime = "start_time"
        case endTime = "end_time"
        case repeatingDay = "repeating_day"
        case addedAt = "added_at"
        case images
        case goingCount = "going_count"
        case activityLogo = "activity_logo"
        case men, women, children, category, org_abbr, org_name
        case classInputType = "class_input_type"
        case selectClass = "select_class"
        case userInput = "user_input"
        case attendeesType = "attendees_type"
        case ageGroup = "age_group"
        case teacherName = "teacher_name"
        case free
        case classesLogo = "classes_logo"
        case eventLogo = "event_logo"
        case donation_logo
        case selectedImage_Array
        case isMoreTapped
        case logo
        case is_private
        case donationType = "donation_type"
        case paymentGateway = "payment_gateway"
        case amountType = "amount_type"
        case pre_dif_amount_list
        case website
 
    }
}



