//
//  ListOfOrganizationModel.swift
//  Muslim Hub
//
//  Created by Ongraph on 5/13/19.
//  Copyright © 2019 OnGraph. All rights reserved.
//

import Foundation

struct ListOfOrganizationModel: Codable {
    let success: Bool
    let data: [OrganizaionList]
}

struct OrganizaionList: Codable {
    let id: Int
    let abbreviation, name: String
}

struct EidSalahCommonModel: Codable {
    let success: Bool
    let message: String
}
