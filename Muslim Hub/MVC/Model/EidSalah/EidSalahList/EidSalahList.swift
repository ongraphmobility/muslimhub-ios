//
//  EidSalahList.swift
//  Muslim Hub
//
//  Created by Ongraph on 5/14/19.
//  Copyright © 2019 OnGraph. All rights reserved.
//

import Foundation

struct EidSalahList: Codable {
    let success: Bool
    let data: [EidSalahListData]
}

struct EidSalahListData: Codable {
    let id, org: Int
    let orgName, orgAbb: String
    let latitude, longitude: Double
    let date, salahTime, address, takbirTime, city: String
    let held, attendees, parking, khutbahLang: Int
    let phoneNum: String
    let info: String
    let addedBy: String
    let logo:String?
    var distance:Int?
    
    enum CodingKeys: String, CodingKey {
        case id, org
        case orgName = "org_name"
        case orgAbb = "org_abb"
        case latitude, longitude, address, date, city
        case salahTime = "salah_time"
        case takbirTime = "takbir_time"
        case held, attendees, parking
        case khutbahLang = "khutbah_lang"
        case phoneNum = "phone_num"
        case info
        case addedBy = "added_by"
        case logo
        case distance
    }
}
