/* 
Copyright (c) 2019 Swift Models Generated from JSON powered by http://www.json4swift.com

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

For support, please feel free to contact me at https://www.linkedin.com/in/syedabsar

*/

import Foundation
struct GetClassData : Codable   {
	var id : Int?
	var org : Int?
    var org_name: String?
	var title : String?
	var description : String?
	var latitude : Double?
	var longitude : Double?
	var address : String?
	var class_input_type : Int?
	var select_class : Int?
	var user_input : String?
	var start_date : String?
	var end_date : String?
	var start_time : String?
	var end_time : String?
	var attendees_type : Int?
	var age_group : String?
	var repeating_day : Int?
	var teacher_name : String?
	var free : Int?
	var added_at : Int?
	var images : [String]?
    var selectedImage_Array: [Bool]?
    var isMoreTapped: Bool?
	var classes_logo : String?

	enum CodingKeys: String, CodingKey {

        case org_name
		case id = "id"
		case org = "org"
		case title = "title"
		case description = "description"
		case latitude = "latitude"
		case longitude = "longitude"
		case address = "address"
		case class_input_type = "class_input_type"
		case select_class = "select_class"
		case user_input = "user_input"
		case start_date = "start_date"
		case end_date = "end_date"
		case start_time = "start_time"
		case end_time = "end_time"
		case attendees_type = "attendees_type"
		case age_group = "age_group"
		case repeating_day = "repeating_day"
		case teacher_name = "teacher_name"
		case free = "free"
		case added_at = "added_at"
		case images = "images"
		case classes_logo = "classes_logo"
        case selectedImage_Array
        case isMoreTapped

	}

//    init(from decoder: Decoder) throws {
//        let values = try decoder.container(keyedBy: CodingKeys.self)
//        id = try values.decodeIfPresent(Int.self, forKey: .id)
//        org = try values.decodeIfPresent(Int.self, forKey: .org)
//        title = try values.decodeIfPresent(String.self, forKey: .title)
//        description = try values.decodeIfPresent(String.self, forKey: .description)
//        latitude = try values.decodeIfPresent(Double.self, forKey: .latitude)
//        longitude = try values.decodeIfPresent(Double.self, forKey: .longitude)
//        address = try values.decodeIfPresent(String.self, forKey: .address)
//        class_input_type = try values.decodeIfPresent(Int.self, forKey: .class_input_type)
//        select_class = try values.decodeIfPresent(Int.self, forKey: .select_class)
//        user_input = try values.decodeIfPresent(String.self, forKey: .user_input)
//        start_date = try values.decodeIfPresent(String.self, forKey: .start_date)
//        end_date = try values.decodeIfPresent(String.self, forKey: .end_date)
//        start_time = try values.decodeIfPresent(String.self, forKey: .start_time)
//        end_time = try values.decodeIfPresent(String.self, forKey: .end_time)
//        attendees_type = try values.decodeIfPresent(Int.self, forKey: .attendees_type)
//        age_group = try values.decodeIfPresent(String.self, forKey: .age_group)
//        repeating_day = try values.decodeIfPresent(Int.self, forKey: .repeating_day)
//        teacher_name = try values.decodeIfPresent(String.self, forKey: .teacher_name)
//        free = try values.decodeIfPresent(Int.self, forKey: .free)
//        added_at = try values.decodeIfPresent(Int.self, forKey: .added_at)
//        images = try values.decodeIfPresent([String].self, forKey: .images)
//        classes_logo = try values.decodeIfPresent(String.self, forKey: .classes_logo)
//    }

}




extension GetClassData : Equatable {
    static func == (lhs: Self, rhs: Self) -> Bool {
        return (lhs.id ?? 0) == (rhs.id ?? 0)
    }
}
