//
//  IQAMAListsModel.swift
//  Muslim Hub
//
//  Created by Ongraph on 16/07/19.
//  Copyright © 2019 OnGraph. All rights reserved.
//


import Foundation

// MARK: - IQAMALists
struct IQAMALists: Codable {
    let success: Bool?
    var data: IQAMAListModel?
    
    enum CodingKeys: String, CodingKey {
         case success
        case data = "iq_ju"
    }
}

// MARK: - DataClass
struct IQAMAListModel: Codable {
    var iqama: [Iqama]?
    var jumma: [Jumma]?
    var specialPrayers: [SpecialPrayer]?
    
    enum CodingKeys: String, CodingKey {
        case iqama, jumma
        case specialPrayers = "special_prayers"
    }
}
// MARK: - Iqama
struct Iqama: Codable {
    let id: Int?
    let prayerCode, swIsPrayed, date, time: String?
    let notes: String?
    let org: Int?
    let calc_type: String?
    let calc_method: Int?
    let offset: Int?
    
    enum CodingKeys: String, CodingKey {
        case id
        case prayerCode = "prayer_code"
        case swIsPrayed = "sw_is_prayed"
        case date, time, notes, org, calc_type,calc_method,offset
    }
}

// MARK: - Jumma
struct Jumma: Codable {
    let id: Int?
    let date, time: String?
    let latitude, longitude: Double?
    let type: Int?
    let address, notes: String?
    let org: Int?
}

// MARK: - SpecialPrayer
struct SpecialPrayer: Codable {
    let id: Int?
      let latitude, longitude: Double?
    let name, address: String?
    let specialPrayerDescription: String?
    let org: Int?
    let date, time: String?
    
    enum CodingKeys: String, CodingKey {
        case id, name, latitude, longitude, address
        case specialPrayerDescription = "description"
        case org
        case date, time
    }
}
