//
//  AddServicesModel.swift
//  Muslim Hub
//
//  Created by Sakshi Singh on 08/05/19.
//  Copyright © 2019 OnGraph. All rights reserved.
//


import Foundation

struct AddServicesModel: Codable {
    let success: Bool?
    let data: [AddServiceData]?
}

struct AddServiceData: Codable {
    let id: Int?
    let serviceName: String?
    let icon: String?
    
    enum CodingKeys: String, CodingKey {
        case id
        case serviceName = "service_name"
        case icon
    }
}
