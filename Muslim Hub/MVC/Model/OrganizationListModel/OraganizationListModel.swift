//
//  OraganizationListModel.swift
//  Muslim Hub
//
//  Created by Sakshi Singh on 06/05/19.
//  Copyright © 2019 OnGraph. All rights reserved.
//


import Foundation

struct OrganizationListModel: Codable {
    let success: Bool?
    let data: [OrganizationDataLists]?
}

struct OrganizationDataLists: Codable {
    let id: Int?
    let name, abbreviation, address: String?
    let latitude: Double?
    let longitude: Double?
    var is_fav:Bool?
    let logo:String?
    let email:String?
    let mobile_number:String?
    let is_private:Bool?
    let first_name:String?
    let last_name:String?
    let org_type:String?
}
struct FlgModel: Codable {
    let data: [String]?
    let success: Bool?
    
}
