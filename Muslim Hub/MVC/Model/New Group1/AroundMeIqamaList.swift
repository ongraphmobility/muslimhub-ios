//
//  AroundMeIqamaList.swift
//  Muslim Hub
//
//  Created by Ongraph on 05/08/19.
//  Copyright © 2019 OnGraph. All rights reserved.
//
import Foundation

// MARK: - AroundMeIQAMALists
struct AroundMeIQAMALists: Codable {
    let success: Bool?
    var data: [AroundIaqma]?
}

// MARK: - Datum
struct AroundIaqma: Codable {
    let id: Int?
    let name, abbreviation, address: String?
    let latitude, longitude: Double?
    let city: String?
    let iqamaID: Int?
    var is_fav: Bool?
    let org,type: Int?
    let prayerCode, swIsPrayed, prayer_type, date, time: String?
    let notes, calcType: String?
    let time_left,time_ago:String?
    let distance:Double?
    let logo:String?
    let past:Bool?
    enum CodingKeys: String, CodingKey {
        case id, name, abbreviation, address, latitude, longitude, city,past, prayer_type
        case iqamaID = "iqama_id"
        case is_fav 
        case org,type
        case prayerCode = "prayer_code"
        case swIsPrayed = "sw_is_prayed"
        case date, time, notes
        case calcType = "calc_type"
        case time_left,distance,logo,time_ago
    }
}
