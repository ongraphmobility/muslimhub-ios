//
//  AnnouncementListModel.swift
//  Muslim Hub
//
//  Created by Rohit on 13/05/19.
//  Copyright © 2019 OnGraph. All rights reserved.
//

import Foundation

struct AnnouncmentLists: Codable {
    let success: Bool?
    var data: [AnnouncementData]?
}

struct AnnouncementData: Codable {
    var id: Int?
    var title, description: String?
    var addedAt: Int?
    var images: [String]?
    var selectedImage_Array: [Bool]?
    
    enum CodingKeys: String, CodingKey {
        case id, title, description
        case addedAt = "added_at"
        case images
        case selectedImage_Array
    }
}
