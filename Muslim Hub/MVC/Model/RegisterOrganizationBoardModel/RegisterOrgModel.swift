//
//  RegisterOrgModel.swift
//  Muslim Hub
//
//  Created by Sakshi Singh on 01/05/19.
//  Copyright © 2019 OnGraph. All rights reserved.
//

import Foundation

struct RegisterOrgModel: Codable {
    let success: Bool?
    let message: String?
    let data: RegisterData?
}

struct RegisterData: Codable {
    let userID: Int?
    let userFirstName, userLastName, userEmail, userMobileNumber: String?
    let name: String?
    let admin: String?
    let abbreviation, orgType, description, address: String?
    let latitude, longitude: Double?
    let regNumber, orgContactName, orgContact, orgEmail: String?
    let orgURL: String?
    let comments: String?
    let donationAllowed: Bool?
    let poliPayID: String?
    let isPrivate: Bool?
    let services: [Int]?
    let image:String?
    let logo:String?
    let tax_deductable:Bool?
    
    enum CodingKeys: String, CodingKey {
        case userID = "user_id"
        case userFirstName = "user_first_name"
        case userLastName = "user_last_name"
        case userEmail = "user_email"
        case userMobileNumber = "user_mobile_number"
        case name, admin, abbreviation
        case orgType = "org_type"
        case description, address, latitude, longitude
        case regNumber = "reg_number"
        case orgContactName = "org_contact_name"
        case orgContact = "org_contact"
        case orgEmail = "org_email"
        case orgURL = "org_url"
        case comments
        case donationAllowed = "donation_allowed"
        case poliPayID = "poli_pay_id"
        case isPrivate = "is_private"
        case services
        case image
        case logo
        case tax_deductable
    }
}
