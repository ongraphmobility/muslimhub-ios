//
//  AddimageAnnouncementCollectionViewCell.swift
//  Muslim Hub
//
//  Created by Sakshi Singh on 14/05/19.
//  Copyright © 2019 OnGraph. All rights reserved.
//

import UIKit

class AddimageAnnouncementCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var img_View:UIImageView!
    @IBOutlet weak var btn_Delete:UIButton!
}
