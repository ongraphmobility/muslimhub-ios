//
//  AddAdminListModel.swift
//  Muslim Hub
//
//  Created by Sakshi Singh on 10/05/19.
//  Copyright © 2019 OnGraph. All rights reserved.
//

import Foundation

struct AdminListModel: Codable {
    let users: [AdminListSData]?
    let success: Bool?
}

struct AdminListSData: Codable {
    let id: Int?
    let isSuperuser, isStaff, isActive: Bool?
    let email, firstName, lastName, mobileNumber: String?
    let mobileNumberVerified: Bool?
    var privileges: Privilege?
   // let favOrg: [String]?
    
    enum CodingKeys: String, CodingKey {
        case id
        case isSuperuser = "is_superuser"
        case isStaff = "is_staff"
        case isActive = "is_active"
        case email
        case firstName = "first_name"
        case lastName = "last_name"
        case mobileNumber = "mobile_number"
        case mobileNumberVerified = "mobile_number_verified"
        case privileges
       // case favOrg = "fav_org"
    }
}

struct Privilege: Codable {
    let orgID : Int?
    let orgPerm: String?
    
    enum CodingKeys: String, CodingKey {
        case orgID = "org_id"
        case orgPerm = "org_perm"
    }
}


struct EditAdminListModel: Codable {
    let user: EditAdminListSData?
    let success: Bool?
    let message:String?
}
struct EditAdminListSData: Codable {
    let id: Int?
    let isSuperuser, isStaff, isActive: Bool?
    let email, firstName, lastName, mobileNumber: String?
    let mobileNumberVerified: Bool?
    let privileges: [PriviligesData]?
    // let favOrg: [String]?
    
    enum CodingKeys: String, CodingKey {
        case id
        case isSuperuser = "is_superuser"
        case isStaff = "is_staff"
        case isActive = "is_active"
        case email
        case firstName = "first_name"
        case lastName = "last_name"
        case mobileNumber = "mobile_number"
        case mobileNumberVerified = "mobile_number_verified"
        case privileges
        // case favOrg = "fav_org"
    }
}
