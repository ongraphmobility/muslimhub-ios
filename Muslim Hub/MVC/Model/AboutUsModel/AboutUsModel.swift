//
//  AboutUsModel.swift
//  Muslim Hub
//
//  Created by Sakshi Singh on 07/05/19.
//  Copyright © 2019 OnGraph. All rights reserved.
//


import Foundation

struct AboutUsModel: Codable {
    let success: Bool?
    let data: AboutData?
}

struct AboutData: Codable {
    let id: Int?
    let text: String?
}
struct AboutUsEditModel: Codable {
    let success: Bool?
    let data: String?
}
