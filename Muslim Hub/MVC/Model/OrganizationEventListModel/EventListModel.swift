//
//  EventListModel.swift
//  Muslim Hub
//
//  Created by Himanshu Goyal on 30/05/19.
//  Copyright © 2019 OnGraph. All rights reserved.
//

//HG
import Foundation

struct OrgranizationEventListModel : Codable {
    let success : Bool?
    let data : [EventData]?
    let message : String?
    
    enum CodingKeys: String, CodingKey {
        
        case success = "success"
        case data = "data"
        case message
    }
    
//    init(from decoder: Decoder) throws {
//        let values = try decoder.container(keyedBy: CodingKeys.self)
//        success = try values.decodeIfPresent(Bool.self, forKey: .success)
//        data = try values.decodeIfPresent([EventData].self, forKey: .data)
//    }
    
}

struct EventData : Codable {
    var id : Int?
    var org : Int?
    var title : String?
    var description : String?
    var latitude : Double?
    var longitude : Double?
    var address : String?
    var start_date : String?
    var end_date : String?
    var start_time : String?
    var end_time : String?
    var attendees_type : Int?
    var added_at : Int?
    var images : [String]?
    var going_count : Int?
    var event_logo : String?
    var activity_logo: String?
    var selectedImage_Array: [Bool]?
    var isMoreTapped: Bool?
    var repeating_day:Int?
    var men,women,children: Int?
    var going: Bool?
    enum CodingKeys: String, CodingKey {
        
        case id = "id"
        case org = "org"
        case title = "title"
        case description = "description"
        case latitude = "latitude"
        case longitude = "longitude"
        case address = "address"
        case start_date = "start_date"
        case end_date = "end_date"
        case start_time = "start_time"
        case end_time = "end_time"
        case attendees_type = "attendees_type"
        case added_at = "added_at"
        case images = "images"
        case going_count = "going_count"
        case event_logo = "event_logo"
        case activity_logo = "activity_logo"
        case selectedImage_Array
        case isMoreTapped
        case repeating_day
        case men,women,children,going
    }
    
//    init(from decoder: Decoder) throws {
//        let values = try decoder.container(keyedBy: CodingKeys.self)
//        id = try values.decodeIfPresent(Int.self, forKey: .id)
//        org = try values.decodeIfPresent(Int.self, forKey: .org)
//        title = try values.decodeIfPresent(String.self, forKey: .title)
//        description = try values.decodeIfPresent(String.self, forKey: .description)
//        latitude = try values.decodeIfPresent(Double.self, forKey: .latitude)
//        longitude = try values.decodeIfPresent(Double.self, forKey: .longitude)
//        address = try values.decodeIfPresent(String.self, forKey: .address)
//        start_date = try values.decodeIfPresent(String.self, forKey: .start_date)
//        end_date = try values.decodeIfPresent(String.self, forKey: .end_date)
//        start_time = try values.decodeIfPresent(String.self, forKey: .start_time)
//        end_time = try values.decodeIfPresent(String.self, forKey: .end_time)
//        attendees_type = try values.decodeIfPresent(Int.self, forKey: .attendees_type)
//        added_at = try values.decodeIfPresent(Int.self, forKey: .added_at)
//        images = try values.decodeIfPresent([String].self, forKey: .images)
//        going_count = try values.decodeIfPresent(Int.self, forKey: .going_count)
//        event_logo = try values.decodeIfPresent(String.self, forKey: .event_logo)
//        activity_logo = try values.decodeIfPresent(String.self, forKey: .activity_logo)
//            repeating_day = try values.decodeIfPresent(Int.self, forKey: .repeating_day)
//    }
    
}
