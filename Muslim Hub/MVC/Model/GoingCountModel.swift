//
//  GoingCountModel.swift
//  Muslim Hub
//
//  Created by Ongraph on 08/07/19.
//  Copyright © 2019 OnGraph. All rights reserved.
//

import Foundation


// MARK: - Welcome
struct GoingData:Codable {
    
    let data: GoingCountModel?
}
struct GoingCountModel: Codable {
  
    let name: String?
    let men: Int?
    let women: Int?
    let children:Int?
}


