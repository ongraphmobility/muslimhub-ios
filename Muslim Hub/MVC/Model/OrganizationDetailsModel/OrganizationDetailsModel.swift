//
//  OrganizationDetailsModel.swift
//  Muslim Hub
//
//  Created by Sakshi Singh on 06/05/19.
//  Copyright © 2019 OnGraph. All rights reserved.
//


import Foundation

struct OrganizationDetailModel: Codable {
    let success: Bool?
    let data: OrgDetailsData?
    let message:String?
}

struct OrgDetailsData: Codable {
    var id: Int?
    let admin: [String]?
    let name, abbreviation, orgType,merchant_code, auth_code, description: String?
    let address, regNumber: String?
    let latitude, longitude: Double?
    let orgContactName, orgContact, orgEmail: String?
    let orgURL: String?
    let comments: String?
    let donationAllowed, taxDeductable: Bool?
    let poliPayID: String?
    let isPrivate: Bool?
    let services: [Int]?
    let favMemberCount: Int?
    let orgApproved: String?
    let logo:String?
    let image:String?
    var is_fav:Bool?
 
    
    enum CodingKeys: String, CodingKey {
        case id, admin, name, abbreviation,auth_code, merchant_code
        case logo, image
        case orgType = "org_type"
        case description, address, latitude, longitude
        case regNumber = "reg_number"
        case orgContactName = "org_contact_name"
        case orgContact = "org_contact"
        case orgEmail = "org_email"
        case orgURL = "org_url"
        case comments
        case donationAllowed = "donation_allowed"
        case taxDeductable = "tax_deductable"
        case poliPayID = "poli_pay_id"
        case isPrivate = "is_private"
        case services
        case favMemberCount = "fav_member_count"
        case orgApproved = "org_approved"
        case is_fav
    }
}
