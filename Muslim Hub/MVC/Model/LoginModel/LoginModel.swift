//
//  LoginModel.swift
//  Muslim Hub
//
//  Created by Sakshi Singh on 25/04/19.
//  Copyright © 2019 OnGraph. All rights reserved.
//

import Foundation

// Error model for app
struct ErrorModel: Decodable {
    
    let code:Int
    let status:String
    let message:String
    
    private enum CodingKeys: String, CodingKey {
        case code, status, message
    }
}

struct LoginModel: Codable {
    let success: Bool?
    let message: String?
    let data: LoginUser?
}
struct poliModel: Codable {
  
   let success: Bool?
    
}
// MARK: - Welcome
struct XlsModel: Codable {
    let success: Bool?
    let data: xlsData?
    let message: String?
}

// MARK: - Datum
struct xlsData: Codable {
    let email, name: String?
    let mobileNumber: Int?
    
    enum CodingKeys: String, CodingKey {
        case email, name
        case mobileNumber = "mobile_number"
    }
}

struct LoginUser: Codable {
    let id:Int?
    let mobileNumber, email: String?
    let firstName, lastName: String?
    let role, Token: String?
    let isSuperuser, isActive: Bool?
    let is_staff:Bool?
    let first_login:Bool?
    var privileges:[PriviligesData]?
    let has_profile:Bool?
    enum CodingKeys: String, CodingKey {
        case id
        case mobileNumber = "mobile_number"
        case email
        case firstName = "first_name"
        case lastName = "last_name"
        case role, Token
        case isSuperuser = "is_superuser"
        case isActive = "is_active"
        case is_staff
        case first_login
        case privileges
        case has_profile
    }
}
struct PriviligesData: Codable {

    let org_id:Int?
    let org_perm:String?
}

