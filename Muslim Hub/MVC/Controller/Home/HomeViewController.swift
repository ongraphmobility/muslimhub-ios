//
//  HomeViewController.swift
//  Muslim Hub
//
//  Created by Sakshi Singh on 01/04/19.
//  Copyright © 2019 OnGraph. All rights reserved.
//

import UIKit
import EventKit
import KVNProgress
import SafariServices
import SKPhotoBrowser
import Branch
import Crashlytics
import ActiveLabel
import TTTAttributedLabel

enum CategoryType: String {
    case event,activity,announcement,donation,iqama_change
}



class HomeViewController: UIViewController,UISearchBarDelegate,ActiveLabelDelegate {
    func didSelect(_ text: String, type: ActiveType) {
        
    }
    //Mark:IBOutlets----------------------
    @IBOutlet weak var tableView_Home:UITableView!
    @IBOutlet weak var collection_View:UICollectionView!
    @IBOutlet weak var btn_All:UIButton!
    @IBOutlet weak var lbl_TitleFav:UILabel!
    @IBOutlet weak var btn_AroundMe:UIButton!
    @IBOutlet weak var width_FavAll:NSLayoutConstraint!
    @IBOutlet weak var view_Header:UIImageView!
    @IBOutlet weak var btn_Profile:UIButton!
    @IBOutlet weak var searchBar:UISearchBar!
    @IBOutlet weak var btn_searchBar:UIButton!
    var allfavOnlyOrganizationlists = [OrganizationDataLists]()
    var homeList = [HomeFeedData]()
    var btnSelected = ""
    var dispatchGroup = DispatchGroup()
    var isFavonly = Bool()
    var goingCountCateogry:String!
    var imageSelectedView = 0
    var id = Int()
    var ReminderDate = Date()
    var shareDate = String()
    var shareTime = String()
    var isSerachBarTapped: Bool!
    
    var phoneNumber:String!
    var urlPhoneNumber:String!
    var array_String = [String]()
    
    
    //Mark:- View LifeCyclye methods--------------------
    override func viewDidLoad() {
        super.viewDidLoad()
        self.buttoncornerRadiusShadow()
        self.hideKeyboardWhenTappedAround()
        self.searchBar.isHidden = true
        if (UserDefaults.standard.value(forKey: "Login_Token") as? String) != nil{
            self.btn_Profile.isHidden = true
        }else{
            self.btn_Profile.isHidden = false
        }
        btnSelected = "All"
        statusBarColorChange(color:UIColor.init(hex: 0x05BDB9))
        
        if let langugae = UserDefaults.standard.value(forKey: Constant.LANGUAGE) as? String{
                  
                  if langugae == "ar"{
                    self.width_FavAll.constant = 150.0
                    
                  }else if langugae == "ms"{
                     self.width_FavAll.constant = 150.0
                    
                  }else{
                    
                     self.width_FavAll.constant = 100.0
                    
            }
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        self.getPrivilegesDataApi()
        
        self.favOnlyListApi()
        
        self.HomeListApi(keyAll: "")
        
        self.getProfileApi()
        
        
        self.btn_All.setTitle("All".localized, for: .normal)
        self.btn_AroundMe.setTitle("Fav".localized, for: .normal)
        self.lbl_TitleFav.text = "FAV ORG".localized
        
        searchBar.delegate = self
        self.isSerachBarTapped = false
        
        
        
    }
    
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        //mark:SearchBar-------------------------------------
        var searchTextField:UITextField!
        
        if #available(iOS 13, *) {
            searchTextField  = searchBar.subviews[0].subviews[2].subviews.last as? UITextField
        } else {
            searchTextField = searchBar.subviews[0].subviews.last as? UITextField
        }
        if #available(iOS 13.0, *) {
            statusBarColorChange(color:UIColor.init(hex: 0x0DC9E4))
        } else {
            UIApplication.statusBarBackgroundColor = UIColor.init(hex: 0x0DC9E4)
        }
        
        searchTextField.layer.cornerRadius = 15
        searchTextField.textAlignment = NSTextAlignment.left
        searchTextField.textColor = UIColor.white
        searchTextField.leftView = nil
        // searchTextField.placeholder = "Search".localized
        
        searchTextField.rightViewMode = UITextField.ViewMode.always
        
        if searchTextField.responds(to: #selector(getter: UITextField.attributedPlaceholder)) {
            let attributeDict = [NSAttributedString.Key.foregroundColor: UIColor.white]
            searchTextField.attributedPlaceholder = NSAttributedString(string: "Search".localized, attributes: attributeDict)
        }
        
    }
    
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        //searchBar.showsCancelButton = true
        if self.btnSelected != "All"{
            self.HomeListApi(keyAll: "")
        }else{
            self.favOnlyListApi()
        }
    }
    
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
        
        if btnSelected == "All"{
            self.serachApi(isfav:"false")
        }else{
            self.serachApi(isfav:"true")
        }
        
    }
    @IBAction func btn_search(sender:UIButton){
        
        if sender.isSelected == true{
            sender.isSelected = false
            searchBar.isHidden = true
        }else{
            sender.isSelected = true
            searchBar.isHidden = false
        }
        
    }
    //MARK: API Call-----------------------------------
    
    func serachApi(isfav:String){
        
        Network.shared.getListOfSearch_Home_Organization(searchKey: trimString(str: self.searchBar.text ?? ""), isfav: isfav) { (result) in
            KVNProgress.dismiss()
            guard let user = result else {
                
                return
            }
            
            self.homeList = user
            if self.homeList.count > 0 {
                for i in 0...self.homeList.count - 1 {
                    var arraySelectedImage: [Bool] = []
                    if self.homeList[i].images.count > 0{
                        for j in 0...self.homeList[i].images.count - 1{
                            if j == 0{
                                arraySelectedImage.append(true)
                            }else{
                                arraySelectedImage.append(false)
                            }
                        }
                    }
                    self.homeList[i].selectedImage_Array = arraySelectedImage
                    
                }
                
                
            }
            self.tableView_Home.reloadData()
        }
        
    }
    
    func trimString(str: String) -> String{
        return str.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
    }
    func getProfileApi(){
        
        Network.shared.getCreateUser { (result) in
            KVNProgress.dismiss()
            guard result != nil else{
                return
            }
            
            UserDefaults.standard.set(result?.data?.has_profile, forKey: "hasProfile")
        }}
    
    func favOnlyListApi(){
        
        Network.shared.favOnlyLists(fav_only:self.isFavonly) { (result) in
            
            guard let user = result else {
                return
            }
            self.allfavOnlyOrganizationlists = user
            self.collection_View.reloadData()
        }
    }
    
    func HomeListApi(keyAll:String){
        //   dispatchGroup.enter()
        KVNProgress.show()
        Network.shared.getListOfHomeFeed(keyAll:keyAll) { (result) in
            KVNProgress.dismiss()
            guard let user = result else {
                return
            }
            self.homeList = user
            // self.dispatchGroup.leave()
            if self.homeList.count > 0 {
                for i in 0...self.homeList.count - 1 {
                    var arraySelectedImage: [Bool] = []
                    if self.homeList[i].images.count > 0{
                        for j in 0...self.homeList[i].images.count - 1{
                            if j == 0{
                                arraySelectedImage.append(true)
                            }else{
                                arraySelectedImage.append(false)
                            }
                        }
                    }
                    self.homeList[i].selectedImage_Array = arraySelectedImage
                    
                }
                
                
            }
            
            self.tableView_Home.reloadData()
        }
    }
    func getGoingCountAPi(categoryId:Int,categoryTypes:String){
        var types = String()
        if categoryTypes == "Event"{
            types = "event"
        }else{
            types = "activity"
            
        }
        Network.shared.getGoingCount(types:types,id: categoryId) { (result) in
            
            guard let user = result else {
                return
            }
            
            print(user)
            
        }
    }
    //MARK:Functions And Methods------------------------------
    func buttoncornerRadiusShadow (){
        
        btn_All.cornerRadiusLower(cornerRadius: 8)
        btn_All.clipsToBounds = true
        btn_All.layer.cornerRadius = 8
        btn_All.layer.borderColor = UIColor.init(hex: 0x0DC9E4).cgColor
        btn_All.layer.borderWidth = 1.0
        btn_All.layer.maskedCorners = [.layerMinXMaxYCorner, .layerMinXMinYCorner]
        
        btn_AroundMe.clipsToBounds = true
        btn_AroundMe.layer.cornerRadius = 8
        btn_AroundMe.layer.maskedCorners = [.layerMaxXMaxYCorner, .layerMaxXMinYCorner]
        btn_AroundMe.layer.borderColor = UIColor.init(hex: 0x0DC9E4).cgColor
        btn_AroundMe.layer.borderWidth = 1.0
        // shadow
        view_Header.shadowBlackToHeader()
    }
    //MARK:Set Reminder method-----------------------
    func eventMethod(senderTag:Int){
        let eventStore = EKEventStore()
        eventStore.requestAccess(
            to: EKEntityType.event, completion: {(granted, error) in
                if !granted {
                    DispatchQueue.main.sync {
                        AKAlertController.alert("The app is not permitted to access reminders, make sure to grant permission in the settings and try again")
                        
                        return
                    }
                } else {
                    print("Access granted")
                    self.createReminder(in: eventStore, reminderText: "",indexPath:senderTag)
                    
                }
        })
    }
    
    func createReminder(in eventStore: EKEventStore, reminderText: String,indexPath:Int) {
        //   DispatchQueue.global(qos: .background).async {
        let reminder = EKEvent(eventStore: eventStore)
        
        
        let startTime = self.homeList[indexPath].startTime ?? ""
        let dateOrg = ((self.homeList[indexPath].startDate ?? "") + " " + startTime)
        let endDateOrg = self.homeList[indexPath].endDate ?? ""
        reminder.title = self.homeList[indexPath].title + " \(self.homeList[indexPath].startDate ?? "") To \(endDateOrg)"
        reminder.calendar = eventStore.defaultCalendarForNewEvents
        
        
        self.stringToDate(dateString: dateOrg)
        let date = self.ReminderDate
        let alarm = EKAlarm(absoluteDate: date)
        reminder.addAlarm(alarm)
        
        let earlierDate = date.addingTimeInterval(-3600*24)
        let earlierAlarm = EKAlarm(absoluteDate: earlierDate)
        reminder.addAlarm(earlierAlarm)
        
        reminder.startDate = date.addingTimeInterval(-24 * 60 * 60)
        reminder.endDate = date.addingTimeInterval(-24 * 60 * 60)
        do {
            try eventStore.save(reminder, span: .thisEvent, commit: true)
            
        } catch let error  {
            print("Reminder failed with error \(error.localizedDescription)")
            return
        }
        
        
        
        DispatchQueue.main.sync {
            // Create the alert controller
            let alertController = UIAlertController(title: "Reminder Created Successfully", message: "", preferredStyle: .alert)
            
            // Create the actions
            let okAction = UIAlertAction(title: "OK", style: UIAlertAction.Style.default) {
                UIAlertAction in
                NSLog("OK Pressed")
                
            }
            
            // Add the actions
            alertController.addAction(okAction)
            
            // Present the controller
            self.present(alertController, animated: true, completion: nil)
        }
        
        //}
    }
    
    
    //MARK:IBAction---------------------------------------
    @IBAction func btn_Profile(sender:UIButton){
        
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "CreateUserViewController") as! CreateUserViewController
        vc.modalPresentationStyle = .fullScreen
        self.present(vc, animated: true, completion: nil)
    }
    @objc func btn_Location(sender: UIButton){
        
        let lat = self.homeList[sender.tag].latitude ?? 123.123
        let long = self.homeList[sender.tag].longitude ?? 123.123
        
        self.location(lat: lat, long: long, title: self.homeList[sender.tag].address ?? "")
        
    }
    
    @objc func btn_GoingEnter(sender: UIButton) {
        
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "Alert_Activites_GoingViewController") as! Alert_Activites_GoingViewController
        vc.modalPresentationStyle = .overCurrentContext
        vc.weakSelf = self
        if self.homeList[sender.tag].category == "event"{
            vc.id = self.homeList[sender.tag].id ?? 0
            vc.category = "Event"
            
        }
        else if  self.homeList[sender.tag].category == "activity"{
            vc.id = self.homeList[sender.tag].id ?? 0
            vc.category = "Activity"
            //             getGoingCountAPi(categoryId:self.homeList[sender.tag].id ?? 0,categoryTypes:"Activity")
        }
        vc.id = self.homeList[sender.tag].id ?? 0
        vc.attendesType = self.homeList[sender.tag].attendeesType ?? 0
        
        self.present(vc, animated: true, completion: nil)
    }
    
    @objc func btn_GoingCount(sender: UIButton) {
        
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "Alert_Activites_GoingViewController") as! Alert_Activites_GoingViewController
        vc.modalPresentationStyle = .overCurrentContext
        // vc.weakSelf = self
        vc.menCount = self.homeList[sender.tag].men ?? 0
        vc.womenCount = self.homeList[sender.tag].women ?? 0
        vc.childrenCount = self.homeList[sender.tag].children ?? 0
        if self.homeList[sender.tag].category == "event"{
            vc.id = self.homeList[sender.tag].id ?? 0
            vc.category = "Event"
            
        }
        else if  self.homeList[sender.tag].category == "activity"{
            vc.id = self.homeList[sender.tag].id ?? 0
            vc.category = "Activity"
            
        }
        vc.id = self.homeList[sender.tag].id ?? 0
        vc.orgid = self.homeList[sender.tag].org ?? 0
        vc.onlyCountSee = "onlyCountSee"
        // vc.modalPresentationStyle = .fullScreen
        self.present(vc, animated: true, completion: nil)
    }
    
    @objc func btn_OrgDetails(sender: UIButton) {
        
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "ActivitiesViewController") as! ActivitiesViewController
        vc.orgId = self.homeList[sender.tag].org ?? 0
        vc.org_type = self.homeList[sender.tag].org_type ?? ""
        vc.modalPresentationStyle = .fullScreen
        self.present(vc, animated: true, completion: nil)
    }
    
    @IBAction func btn_All(sender:UIButton){
        btnSelected = "All"
        btn_AroundMe.backgroundColor = UIColor.white
        btn_All.backgroundColor = UIColor.init(hex: 0x0DC9E4)
        btn_All.setTitleColor(UIColor.white, for: .normal)
        btn_AroundMe.setTitleColor(UIColor.darkGray, for: .normal)
        self.HomeListApi(keyAll: "")
    }
    @IBAction func btn_Fav(sender:UIButton){
        btnSelected = "Fav"
        btn_All.backgroundColor = UIColor.white
        btn_AroundMe.backgroundColor = UIColor.init(hex: 0x0DC9E4)
        
        btn_AroundMe.setTitleColor(UIColor.white, for: .normal)
        btn_All.setTitleColor(UIColor.darkGray, for: .normal)
        self.HomeListApi(keyAll: "Fav")
    }
    
    @objc func btn_shareTextButton(sender: UIButton) {
        
        var image = String()
        
        
        for (index, element) in self.homeList[sender.tag].images.enumerated() {
            print("Item \(index): \(element)")
            
            
            image = image + "\n\n\(element)"
            
        }
        
        let buo = BranchUniversalObject.init(canonicalIdentifier: "content/12345")
        buo.title = self.homeList[sender.tag].title
        buo.contentDescription = "My Content Description"
        //  buo.imageUrl = self.homeList[sender.tag].images[0]
        buo.publiclyIndex = true
        buo.locallyIndex = true
        buo.contentMetadata.customMetadata["data"] = "\(self.homeList[sender.tag].org!)"
        
        let type = self.homeList[sender.tag].category
        if type == "event"{
            buo.contentMetadata.customMetadata["type"] = "Events"
        }else if type == "class"{
            buo.contentMetadata.customMetadata["type"] = "Classes"
        }else if type == "activity"{
            buo.contentMetadata.customMetadata["type"] = "Activities"
        }else if type == "announcement"{
            buo.contentMetadata.customMetadata["type"] = "Announcements"
        }
        
        
        
        
        buo.contentMetadata.customMetadata["typeId"] = "\(self.homeList[sender.tag].id!)"
        let lp: BranchLinkProperties = BranchLinkProperties()
        
        
        var brachioUrl = String()
        
        buo.getShortUrl(with: lp) { (url, error) in
            print(url ?? "")
            brachioUrl = url ?? ""
            
            
            if type == "announcement"{
                
                self.shareDate = ""
                self.shareTime = ""
                
                if image != ""{
                    let data =  "\n \n" + (self.homeList[sender.tag].address ?? "") + "\n \n" + (self.homeList[sender.tag].datumDescription)
                    let abbreviation = image + "\n" + self.homeList[sender.tag].title + "\n\n"
                        + self.homeList[sender.tag].org_abbr! + "\n \n" + "Take me to the event in MuslimHub \n\n" + brachioUrl + "\n \n" + data + "\n \n" + "Shared from MuslimHub. Download from Apple Store" + "\n\n" + "https://apps.apple.com/au/app/muslim-hub/id1014406101"
                    self.btn_ShareTapp(details: [abbreviation])
                }else{
                    let data = (self.homeList[sender.tag].address ?? "") + "\n \n" + (self.homeList[sender.tag].datumDescription)
                    
                    let abbreviation = self.homeList[sender.tag].title + "\n\n"
                        + self.homeList[sender.tag].org_abbr! + "\n \n" + "Take me to the event in MuslimHub \n\n"  + brachioUrl + "\n \n" + data + "\n \n" + "Shared from MuslimHub. Download from Apple Store" + "\n\n" + "https://apps.apple.com/au/app/muslim-hub/id1014406101"
                    
                    self.btn_ShareTapp(details: [abbreviation])
                }
                
                
            }else{
                self.shareDateFormat(startDate:self.homeList[sender.tag].startDate!)
                self.shareTimeFormat(startTime:self.homeList[sender.tag].startTime! , endTime: self.homeList[sender.tag].endTime!)
                if image != ""{
                    let data =  "\n \n" + (self.homeList[sender.tag].address ?? "") + "\n \n" + (self.homeList[sender.tag].datumDescription)
                    let abbreviation = image + "\n" + self.homeList[sender.tag].title + "\n\n"
                        + self.homeList[sender.tag].org_abbr! + "\n \n" + "Take me to the event in MuslimHub \n\n" + brachioUrl + "\n \n" + self.shareDate + ", " + self.shareTime + data + "\n \n" + "Shared from MuslimHub. Download from Apple Store" + "\n\n" + "https://apps.apple.com/au/app/muslim-hub/id1014406101"
                    self.btn_ShareTapp(details: [abbreviation])
                }else{
                    let data = ", " + self.shareTime + "\n \n" + (self.homeList[sender.tag].address ?? "") + "\n \n" + (self.homeList[sender.tag].datumDescription)
                    let abbreviation = self.homeList[sender.tag].title + "\n\n"
                        + self.homeList[sender.tag].org_abbr! + "\n \n" + "Take me to the event in MuslimHub \n\n"  + brachioUrl + "\n \n" + self.shareDate + data + "\n \n" + "Shared from MuslimHub. Download from Apple Store" + "\n\n" + "https://apps.apple.com/au/app/muslim-hub/id1014406101"
                    
                    self.btn_ShareTapp(details: [abbreviation])
                }
                
                
            }
            
            
        }
    }
    
    
    @objc func btn_EventRemainderTapped(sender: UIButton){
        
        self.eventMethod(senderTag: sender.tag)
    }
    @objc func imageTapped(tapGestureRecognizer: UITapGestureRecognizer)
    {
        _ = tapGestureRecognizer.view as! UIImageView
        
        SKPhotoBrowserOptions.displayAction = false
        var images = [SKPhoto]()
        for i in 0..<self.homeList[(tapGestureRecognizer.view?.tag)!].images.count{
            
            let photo = SKPhoto.photoWithImageURL(self.homeList[(tapGestureRecognizer.view?.tag)!].images[i])
            photo.shouldCachePhotoURLImage = false // you can use image cache by true(NSCache)
            images.append(photo)
        }
        
        
        let browser = SKPhotoBrowser(originImage: #imageLiteral(resourceName: "default_header_iPhone"), photos: images, animatedFromView: self.view)
        
        browser.initializePageIndex(imageSelectedView)
        present(browser, animated: true, completion: {})
        
    }
}

//Mark:TableView-------------------------------------
extension HomeViewController:UITableViewDelegate,UITableViewDataSource,NotSoGoodCellDelegate,IndexImageSendCellDelegate,shareDataDelegateHome  {
    
    func shareDataDelegateHome(cell: AnnouncementTableViewCell, date: String, time: String) {
        
        self.shareDate = date
        self.shareTime = time
    }
    
    func IndexImageSendCellDelegate(cell: AnnouncementTableViewCell, selected_imgIndex: Int) {
        self.imageSelectedView = selected_imgIndex
    }
    
    
    // Mark: - my cell delegate
    func moreTapped(cell: AnnouncementTableViewCell) {
        
        // this will "refresh" the row heights, without reloading
        tableView_Home.beginUpdates()
        tableView_Home.endUpdates()
        
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if homeList.count == 0{
            return 0
        }else{
            return homeList.count
        }
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        //Mark:This is Home All Cell
        let cell = tableView.dequeueReusableCell(withIdentifier: "AnnouncementTableViewCell", for: indexPath) as! AnnouncementTableViewCell
        cell.btn_GoingCount.setTitleColor(UIColor.init(hex: 0x0DC9E4), for: .normal)
        // cell.btn_GoingCount.setImage(#imageLiteral(resourceName: "group_icon_blue_iPhone"), for: .normal)
        cell.imageSelectedSendDelegate = self
        
        cell.img_Viewannouncement.tag = indexPath.row
        // self.imageSelectedView = cell.img_Viewannouncement.image
        
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(imageTapped(tapGestureRecognizer:)))
        
        
        cell.img_Viewannouncement.isUserInteractionEnabled = true
        cell.img_Viewannouncement.addGestureRecognizer(tapGestureRecognizer)
        
        cell.delegate = self
        cell.shareDataDelegatehome = self
        cell.view_ClassCost.isHidden = true
        cell.btnReminder.tag = indexPath.row
        cell.btnShare.tag = indexPath.row
        cell.btnGoing.tag = indexPath.row
        
        cell.lblLocation.tag = indexPath.row
        cell.lblLocation.addTarget(self, action: #selector(btn_Location(sender:)), for: .touchUpInside)
        
        if self.homeList.count != 0 {
            cell.configureCell(model: self.homeList[indexPath.row], indexPath: indexPath.row)
        }
        
        //adds shadow to the layer of cell
        cell.view_Announcment.layer.cornerRadius = 10.0
        cell.view_Announcment.shadowBlackToHeader()
        
        //makes the cell round
        let containerView = cell.contentView
        containerView.layer.cornerRadius = 2
        containerView.clipsToBounds = true
        
        if let langugae = UserDefaults.standard.value(forKey: Constant.LANGUAGE) as? String{
            
            if langugae == "ar"{
                cell.buttonMore.imageEdgeInsets = UIEdgeInsets(top: 0.0, left: 0.0, bottom: 0.0, right: -145.0)
                cell.buttonMore.titleEdgeInsets = UIEdgeInsets(top: 0.0, left: -38.0, bottom: 0.0, right: 0.0)
                cell.btnShowHide.imageEdgeInsets = UIEdgeInsets(top: 0.0, left: 0.0, bottom: 0.0, right: -145.0)
                cell.btnShowHide.titleEdgeInsets = UIEdgeInsets(top: 0.0, left: -38.0, bottom: 0.0, right: 0.0)
                cell.buttonMore.contentEdgeInsets = UIEdgeInsets(top: 0.0, left: 0.0, bottom: 0.0, right: 0.0)
                
            }else if langugae == "ms"{
                cell.buttonMore.imageEdgeInsets = UIEdgeInsets(top: 0.0, left: -10.0, bottom: 0.0, right: -244.0)
                cell.buttonMore.titleEdgeInsets = UIEdgeInsets(top: 0.0, left: -38.0, bottom: 0.0, right: 0.0)
                cell.buttonMore.contentEdgeInsets = UIEdgeInsets(top: 0.0, left: 0.0, bottom: 0.0, right: 0.0)
                
                cell.btnShowHide.imageEdgeInsets = UIEdgeInsets(top: 0.0, left: 10.0, bottom: 0.0, right: -244.0)
                cell.btnShowHide.titleEdgeInsets = UIEdgeInsets(top: 0.0, left: -38.0, bottom: 0.0, right: 0.0)
                
            }else if langugae == "en"{
                cell.buttonMore.imageEdgeInsets = UIEdgeInsets(top: 0.0, left: 0.0, bottom: 0.0, right: -145.0)
                cell.buttonMore.titleEdgeInsets = UIEdgeInsets(top: 0.0, left: -38.0, bottom: 0.0, right: 0.0)
                cell.btnShowHide.imageEdgeInsets = UIEdgeInsets(top: 0.0, left: 0.0, bottom: 0.0, right: -145.0)
                cell.btnShowHide.titleEdgeInsets = UIEdgeInsets(top: 0.0, left: -38.0, bottom: 0.0, right: 0.0)
                cell.buttonMore.contentEdgeInsets = UIEdgeInsets(top: 0.0, left: 0.0, bottom: 0.0, right: 0.0)
                
            }
        }else{
            cell.buttonMore.imageEdgeInsets = UIEdgeInsets(top: 0.0, left: 0.0, bottom: 0.0, right: -145.0)
            cell.buttonMore.titleEdgeInsets = UIEdgeInsets(top: 0.0, left: -38.0, bottom: 0.0, right: 0.0)
            cell.btnShowHide.imageEdgeInsets = UIEdgeInsets(top: 0.0, left: 0.0, bottom: 0.0, right: -145.0)
            cell.btnShowHide.titleEdgeInsets = UIEdgeInsets(top: 0.0, left: -38.0, bottom: 0.0, right: 0.0)
            cell.buttonMore.contentEdgeInsets = UIEdgeInsets(top: 0.0, left: 0.0, bottom: 0.0, right: 0.0)
        }
        
        //mark:Date
        let now = Date()
        let date = NSDate(timeIntervalSince1970: Double(self.homeList[indexPath.row].addedAt ))
        cell.lbl_Title3.text = now.timeAgoDisplay(dateCreated:date as Date,timeStamp:self.homeList[indexPath.row].addedAt )
        
        let categoryType = self.homeList[indexPath.row].category
        
        switch CategoryType(rawValue: categoryType) {
            
            
        case .announcement?:  //MARK: Type Announcement------------------------------
            cell.view_Divider.isHidden = false
            self.goingCountCateogry = "announcement"
            cell.view_Share.isHidden = false
            cell.layout_ShareHeight.constant = 34.0
            cell.btn_ClassType.isHidden = true
            cell.view_MonthType.isHidden = true
            cell.btnGoing.tag = indexPath.row
            cell.view_Location.isHidden  = true
            cell.view_stackViewTypes.isHidden = true
            cell.btnReminder.setTitle("", for: .normal)
            cell.btnReminder.setImage(nil, for: .normal)
            cell.btnReminder.isUserInteractionEnabled = false
            cell.height_stackViewTypes.constant = 0.0
            cell.view_stackViewDonation.isHidden = true
            cell.view_donationHeight.constant = 0.0
            cell.lbl_Title1.setTitle(homeList[indexPath.row].org_abbr, for: .normal)
            cell.lbl_Title1.tag = indexPath.row
            cell.btnImageLogo.tag = indexPath.row
            cell.lbl_Title1.addTarget(self, action: #selector(btn_OrgDetails(sender:)), for: .touchUpInside)
            cell.btnImageLogo.addTarget(self, action: #selector(btn_OrgDetails(sender:)), for: .touchUpInside)
            cell.lbl_Title2.text = ""
            cell.lbl_Title5.isHidden = true
            // cell.lbl_Title2.text = homeList[indexPath.row].org_name
            cell.btn_ViewCategory.setImage(#imageLiteral(resourceName: "annoucement_iPhone"), for: .normal)
            cell.lbl_Title4.text = homeList[indexPath.row].title
            //  cell.lbl_MonthType.text = homeList[indexPath.row].title
            
            cell.View_CategoryColor.backgroundColor = UIColor.init(hex: 0xb68efe)
            let goingCount = (homeList[indexPath.row].goingCount ?? 0)
            let going = "\(String(describing: goingCount))" + " going"
            cell.btn_GoingCount.setTitle(going, for: .normal)
            cell.view_ClassCost.isHidden = true
            cell.lbl_TeacherNAme.isHidden = true
            if (homeList[indexPath.row].free) == 1{
                cell.lbl_ClassCost.text = "Free"
            }else{
                cell.lbl_ClassCost.text = "Paid"
            }
            
            cell.view_AgeGroup.isHidden = true
            cell.view_Gender.isHidden = true
            cell.view_TeacherType.isHidden = true
            cell.view_AttendesType.isHidden = false
            cell.lbl_AttendesType.isHidden = false
            
            
            
            if homeList[indexPath.row].attendeesType ?? 0 == 1{
                cell.lbl_AttendesType.text = "Men Only"
            }else if homeList[indexPath.row].attendeesType ?? 0 == 2{
                cell.lbl_AttendesType.text = "Women Only"
            }else if homeList[indexPath.row].attendeesType ?? 0 == 3{
                cell.lbl_AttendesType.text = "Family"
            }else{
                cell.lbl_AttendesType.text = ""
            }
            if let imageUrl =  self.homeList[indexPath.row].logo{
                cell.img_Logo.sd_setImage(with: URL(string: imageUrl), placeholderImage: #imageLiteral(resourceName: "default_header_iPhone"))
            }else{
                cell.img_Logo.image = #imageLiteral(resourceName: "default_header_iPhone")
            }
            //  cell.btn_GoingCount.isHidden = false
            cell.btn_GoingCount.isHidden = true
            cell.btnGoing.isHidden = true
            //mark:GoingCount
            cell.btn_GoingCount.tag = indexPath.row
            cell.btn_GoingCount.addTarget(self, action: #selector(btn_GoingCount(sender:)), for: .touchUpInside)
        case .event?://MARK: event Announcement------------------------------
            cell.view_Share.isHidden = false
            cell.view_Divider.isHidden = false
            cell.lbl_Title5.isHidden = true
            cell.layout_ShareHeight.constant = 34.0
            cell.btnGoing.tag = indexPath.row
            cell.view_MonthType.isHidden = true
            cell.view_Location.isHidden  = false
            cell.btn_ClassType.isHidden = true
            cell.view_stackViewTypes.isHidden = false
            cell.height_stackViewTypes.constant = 120.0
            cell.view_donationHeight.constant = 0.0
            cell.view_stackViewDonation.isHidden = true
            cell.btnGoing.isHidden = false
            cell.view_ClassCost.isHidden = true
            cell.view_AgeGroup.isHidden = true
            cell.view_Gender.isHidden = true
            cell.view_TeacherType.isHidden = true
            cell.lbl_WebsiteUrl.isHidden = true
            cell.view_AttendesType.isHidden = false
            cell.lbl_AttendesType.isHidden = false
            cell.view_ContainAGGA.isHidden = false
            cell.btnReminder.setTitle("Reminder".localized, for: .normal)
            cell.btnReminder.setImage(#imageLiteral(resourceName: "reminder_iPhone"), for: .normal)
            cell.btnReminder.isUserInteractionEnabled = true
            if let imageUrl =  self.homeList[indexPath.row].eventLogo{
                cell.img_Logo.sd_setImage(with: URL(string: imageUrl), placeholderImage: #imageLiteral(resourceName: "default_header_iPhone"))
            }else{
                cell.img_Logo.image = #imageLiteral(resourceName: "default_header_iPhone")
            }
            
            cell.lblLocation.tag = indexPath.row
            cell.lblLocation.addTarget(self, action: #selector(btn_Location(sender:)), for: .touchUpInside)
            cell.lbl_Title1.tag = indexPath.row
            cell.btnImageLogo.tag = indexPath.row
            cell.lbl_Title1.setTitle(homeList[indexPath.row].org_abbr, for: .normal)
            cell.lbl_Title1.addTarget(self, action: #selector(btn_OrgDetails(sender:)), for: .touchUpInside)
            cell.btnImageLogo.addTarget(self, action: #selector(btn_OrgDetails(sender:)), for: .touchUpInside)
            
            cell.lbl_Title2.text = ""//homeList[indexPath.row].org_name
            
            cell.btn_ViewCategory.setImage(#imageLiteral(resourceName: "events_iPhone"), for: .normal)
            cell.lbl_Title4.text = homeList[indexPath.row].title
            cell.View_CategoryColor.backgroundColor = UIColor.init(hex: 0xFFDC65)
            self.goingCountCateogry = "event"
            cell.btnGoing.addTarget(self, action: #selector(btn_GoingEnter(sender:)), for: .touchUpInside)
            
            cell.btnGoing.tag = indexPath.row
            if homeList[indexPath.row].attendeesType ?? 0 == 1{
                cell.lbl_AttendesType.text = "Men Only"
            }else if homeList[indexPath.row].attendeesType ?? 0 == 2{
                cell.lbl_AttendesType.text = "Women Only"
            }else if homeList[indexPath.row].attendeesType ?? 0 == 3{
                cell.lbl_AttendesType.text = "Family"
            }else{
                cell.lbl_AttendesType.text = ""
            }
            //mark:GoingCount
            cell.btn_GoingCount.isHidden = false
            cell.btn_GoingCount.tag = indexPath.row
            let goingCount = (homeList[indexPath.row].goingCount ?? 0)
            let going = "\(String(describing: goingCount))" + " going"
            cell.btn_GoingCount.setTitle(going, for: .normal)
            cell.btn_GoingCount.addTarget(self, action: #selector(btn_GoingCount(sender:)), for: .touchUpInside)
            cell.lbl_Title5.isHidden = true
        case .activity?: //MARK: activity Announcement------------------------------
            cell.lbl_Title5.isHidden = true
            cell.btnGoing.tag = indexPath.row
            self.goingCountCateogry = "activity"
            cell.view_Divider.isHidden = false
            cell.view_Share.isHidden = false
            cell.layout_ShareHeight.constant = 34.0
            cell.view_stackViewDonation.isHidden = true
            cell.view_MonthType.isHidden = true
            cell.btn_ClassType.isHidden = true
            cell.view_Location.isHidden  = false
            cell.view_stackViewTypes.isHidden = false
            cell.height_stackViewTypes.constant = 60.0
            cell.view_donationHeight.constant = 0.0
            cell.lbl_WebsiteUrl.isHidden = true
            cell.view_ClassCost.isHidden = true
            cell.lbl_TeacherNAme.isHidden = true
            cell.view_TeacherType.isHidden = true
            cell.view_ContainAGGA.isHidden = true
            cell.btnReminder.setTitle("Reminder".localized, for: .normal)
            cell.btnReminder.setImage(#imageLiteral(resourceName: "reminder_iPhone"), for: .normal)
            cell.btnReminder.isUserInteractionEnabled = true
            DispatchQueue.main.async {
                
                if let imageUrl =  self.homeList[indexPath.row].activityLogo{
                    cell.img_Logo.sd_setImage(with: URL(string: imageUrl), placeholderImage: #imageLiteral(resourceName: "default_header_iPhone"))
                }else{
                    cell.img_Logo.image = #imageLiteral(resourceName: "default_header_iPhone")
                }
            }
            cell.lbl_Title1.tag = indexPath.row
            cell.btnImageLogo.tag = indexPath.row
            cell.lbl_Title1.addTarget(self, action: #selector(btn_OrgDetails(sender:)), for: .touchUpInside)
            cell.btnImageLogo.addTarget(self, action: #selector(btn_OrgDetails(sender:)), for: .touchUpInside)
            
            cell.lblLocation.tag = indexPath.row
            cell.lblLocation.addTarget(self, action: #selector(btn_Location(sender:)), for: .touchUpInside)
            cell.lbl_Title1.setTitle(homeList[indexPath.row].org_abbr, for: .normal)
            cell.lbl_Title2.text = ""
            
            cell.btn_ViewCategory.setImage(#imageLiteral(resourceName: "activities_iPhone"), for: .normal)
            cell.lbl_Title4.text = homeList[indexPath.row].title
            cell.View_CategoryColor.backgroundColor = UIColor.init(hex: 0xF6877C)
            
            cell.btnGoing.isHidden = false
            
            cell.btnGoing.addTarget(self, action: #selector(btn_GoingEnter(sender:)), for: .touchUpInside)
            
            
            if homeList[indexPath.row].attendeesType ?? 0 == 1{
                cell.lbl_AttendesType.text = "Men Only"
            }else if homeList[indexPath.row].attendeesType ?? 0 == 2{
                cell.lbl_AttendesType.text = "Women Only"
            }else if homeList[indexPath.row].attendeesType ?? 0 == 3{
                cell.lbl_AttendesType.text = "Family"
            }else{
                cell.lbl_AttendesType.text = ""
            }
            //mark:GoingCount
            cell.btn_GoingCount.isHidden = false
            
            cell.btn_GoingCount.tag = indexPath.row
            let goingCount = (homeList[indexPath.row].goingCount ?? 0)
            let going = "\(String(describing: goingCount))" + " going"
            cell.btn_GoingCount.setTitle(going, for: .normal)
            cell.btn_GoingCount.addTarget(self, action: #selector(btn_GoingCount(sender:)), for: .touchUpInside)
            cell.lbl_Title5.isHidden = true
            
        case .donation?: //MARK: donate Announcement------------------------------
            cell.lbl_Title5.isHidden = true
            cell.btnGoing.tag = indexPath.row
            self.goingCountCateogry = "donate"
            cell.view_MonthType.isHidden = true
            cell.btn_ClassType.isHidden = true
            cell.view_Location.isHidden  = true
            cell.view_stackViewTypes.isHidden = true
            cell.height_stackViewTypes.constant = 0.0
            cell.view_Share.isHidden = true
             cell.view_Divider.isHidden = true
            cell.layout_ShareHeight.constant = 0.0
            cell.view_stackViewDonation.isHidden = false
            // cell.lbl_WebsiteUrl.isHidden = false
            cell.view_ClassCost.isHidden = true
            cell.lbl_TeacherNAme.isHidden = true
            cell.view_TeacherType.isHidden = true
            cell.view_ContainAGGA.isHidden = true
            cell.btnReminder.setTitle("Reminder".localized, for: .normal)
            cell.btnReminder.setImage(#imageLiteral(resourceName: "reminder_iPhone"), for: .normal)
            cell.btnReminder.isUserInteractionEnabled = true
            DispatchQueue.main.async {
                
                if let imageUrl =  self.homeList[indexPath.row].donation_logo{
                    cell.img_Logo.sd_setImage(with: URL(string: imageUrl), placeholderImage: #imageLiteral(resourceName: "default_header_iPhone"))
                }else{
                    cell.img_Logo.image = #imageLiteral(resourceName: "default_header_iPhone")
                }
            }
            
            if self.homeList[indexPath.row].donationType == 1{
                cell.btn_ClassType.setTitle("Medical", for: .normal)
                
            }else if self.homeList[indexPath.row].donationType == 2{
                cell.btn_ClassType.setTitle("Education", for: .normal)
                
            }else if self.homeList[indexPath.row].donationType == 3{
                cell.btn_ClassType.setTitle("Religious", for: .normal)
                
            }
            
            
            
            cell.lbl_Title1.tag = indexPath.row
            cell.btnImageLogo.tag = indexPath.row
            cell.lbl_Title1.addTarget(self, action: #selector(btn_OrgDetails(sender:)), for: .touchUpInside)
            cell.btnImageLogo.addTarget(self, action: #selector(btn_OrgDetails(sender:)), for: .touchUpInside)
            
            cell.lblLocation.tag = indexPath.row
            cell.lblLocation.addTarget(self, action: #selector(btn_Location(sender:)), for: .touchUpInside)
            cell.lbl_Title1.setTitle(homeList[indexPath.row].org_abbr, for: .normal)
            cell.lbl_Title2.text = ""
            
            cell.btn_ViewCategory.setImage(#imageLiteral(resourceName: "donation_iPhone"), for: .normal)
            cell.lbl_Title4.text = homeList[indexPath.row].title
            cell.View_CategoryColor.backgroundColor = UIColor.init(hex: 0xF6877C)
            
            cell.btnGoing.isHidden = true
            
            //                    cell.btnGoing.addTarget(self, action: #selector(btn_GoingEnter(sender:)), for: .touchUpInside)
            //
            //
            //                    if homeList[indexPath.row].attendeesType ?? 0 == 1{
            //                        cell.lbl_AttendesType.text = "Men Only"
            //                    }else if homeList[indexPath.row].attendeesType ?? 0 == 2{
            //                        cell.lbl_AttendesType.text = "Women Only"
            //                    }else if homeList[indexPath.row].attendeesType ?? 0 == 3{
            //                        cell.lbl_AttendesType.text = "Family"
            //                    }else{
            //                        cell.lbl_AttendesType.text = ""
            //                    }
            //mark:GoingCount
            cell.btn_GoingCount.isHidden = true
            
            //                    cell.btn_GoingCount.tag = indexPath.row
            //                    let goingCount = (homeList[indexPath.row].goingCount ?? 0)
            //                    let going = "\(String(describing: goingCount))" + " going"
            //                    cell.btn_GoingCount.setTitle(going, for: .normal)
            //                    cell.btn_GoingCount.addTarget(self, action: #selector(btn_GoingCount(sender:)), for: .touchUpInside)
            cell.lbl_Title5.isHidden = true
            
            
        case .iqama_change?: //MARK: Iqama_Change-----------------------
            
            self.goingCountCateogry = "iqama_change"
            cell.view_Share.isHidden = true
            cell.view_Divider.isHidden = true
            cell.layout_ShareHeight.constant = 34.0
            cell.btn_ClassType.isHidden = true
            cell.view_MonthType.isHidden = true
            cell.btnGoing.tag = indexPath.row
            cell.view_Location.isHidden  = true
            cell.view_stackViewTypes.isHidden = true
            cell.btnReminder.setTitle("", for: .normal)
            cell.btnReminder.setImage(nil, for: .normal)
            cell.btnReminder.isUserInteractionEnabled = false
            cell.height_stackViewTypes.constant = 0.0
            cell.view_stackViewDonation.isHidden = true
            cell.view_donationHeight.constant = 0.0
            cell.lbl_Title1.setTitle(homeList[indexPath.row].org_abbr, for: .normal)
            cell.lbl_Title1.tag = indexPath.row
            cell.btnImageLogo.tag = indexPath.row
            cell.lbl_Title1.addTarget(self, action: #selector(btn_OrgDetails(sender:)), for: .touchUpInside)
            cell.btnImageLogo.addTarget(self, action: #selector(btn_OrgDetails(sender:)), for: .touchUpInside)
            cell.lbl_Title2.text = ""
            cell.lbl_Title5.isHidden = false
            cell.lbl_Title5.text = self.homeList[indexPath.row].message ?? ""
            // cell.lbl_Title2.text = homeList[indexPath.row].org_name
            cell.btn_ViewCategory.setImage(#imageLiteral(resourceName: "iqamah_change_iPhone"), for: .normal)
            cell.lbl_Title4.text = homeList[indexPath.row].title
            //  cell.lbl_MonthType.text = homeList[indexPath.row].title
            
            
            
            cell.View_CategoryColor.backgroundColor = UIColor.init(hex: 0x429dcc)
            let goingCount = (homeList[indexPath.row].goingCount ?? 0)
            let going = "\(String(describing: goingCount))" + " going"
            cell.btn_GoingCount.setTitle(going, for: .normal)
            cell.view_ClassCost.isHidden = true
            cell.lbl_TeacherNAme.isHidden = true
            
            cell.view_AgeGroup.isHidden = true
            cell.view_Gender.isHidden = true
            cell.view_TeacherType.isHidden = true
            cell.view_AttendesType.isHidden = true
            cell.lbl_AttendesType.isHidden = true
            cell.layout_ShareHeight.constant = 0.0
            
            if let imageUrl =  self.homeList[indexPath.row].logo{
                cell.img_Logo.sd_setImage(with: URL(string: imageUrl), placeholderImage: #imageLiteral(resourceName: "default_header_iPhone"))
            }else{
                cell.img_Logo.image = #imageLiteral(resourceName: "default_header_iPhone")
            }
            //  cell.btn_GoingCount.isHidden = false
            cell.btn_GoingCount.isHidden = true
            cell.btnGoing.isHidden = true
            
            
        default:
            cell.lbl_Title5.isHidden = true
            self.goingCountCateogry = "class"
            cell.view_Share.isHidden = false
            cell.layout_ShareHeight.constant = 34.0
            cell.view_Location.isHidden  = false
            cell.view_MonthType.isHidden = false
            cell.view_stackViewTypes.isHidden = false
            cell.height_stackViewTypes.constant = 120.0
            cell.view_donationHeight.constant = 0.0
            cell.view_stackViewDonation.isHidden = true
            cell.btnGoing.isHidden = true
            cell.btn_GoingCount.isHidden = true
            cell.view_ClassCost.isHidden = false
            cell.lbl_TeacherNAme.isHidden = false
            cell.view_ContainAGGA.isHidden = false
            cell.view_AgeGroup.isHidden = false
            cell.view_Gender.isHidden = false
            cell.view_TeacherType.isHidden = false
            cell.lbl_WebsiteUrl.isHidden = true
            cell.view_AttendesType.isHidden = true
            cell.lbl_AttendesType.isHidden = true
            cell.btnReminder.setTitle("Reminder".localized, for: .normal)
            cell.btnReminder.setImage(#imageLiteral(resourceName: "reminder_iPhone"), for: .normal)
            cell.btnReminder.isUserInteractionEnabled = true
            if homeList[indexPath.row].repeatingDay ?? 0 == 1{
                cell.lbl_MonthType.text = "Weekly"
            }else if homeList[indexPath.row].repeatingDay ?? 0 == 2{
                cell.lbl_MonthType.text = "Fortnightly"
            }else if homeList[indexPath.row].repeatingDay ?? 0 == 3{
                cell.lbl_MonthType.text = "Monthly"
            }else{
                cell.lbl_MonthType.text = ""
            }
            cell.btn_ClassType.isHidden = true
            
            let user_Input = self.homeList[indexPath.row].userInput ?? ""
            if self.homeList[indexPath.row].classInputType == 1 {
                if self.homeList[indexPath.row].selectClass == 1{
                    cell.btn_ClassType.setTitle("Quran & Tajweed", for: .normal)
                }else if self.homeList[indexPath.row].selectClass == 2{
                    cell.btn_ClassType.setTitle("Seerah & Hadith", for: .normal)
                }else if self.homeList[indexPath.row].selectClass == 3{
                    cell.btn_ClassType.setTitle("Islamic Studies", for: .normal)
                }else if self.homeList[indexPath.row].selectClass == 4{
                    cell.btn_ClassType.setTitle("Arabic learning", for: .normal)
                }else if self.homeList[indexPath.row].selectClass == 5{
                    cell.btn_ClassType.setTitle("Sports", for: .normal)
                }else if self.homeList[indexPath.row].selectClass == 6{
                    cell.btn_ClassType.setTitle("Academic", for: .normal)
                }
                else{
                    cell.btn_ClassType.setTitle("Language", for: .normal)
                }
            }else{
                cell.btn_ClassType.setTitle(user_Input, for: .normal)
            }
            
            if homeList[indexPath.row].attendeesType ?? 0 == 1{
                cell.lbl_Gender.text = "Men Only"
            }else if homeList[indexPath.row].attendeesType ?? 0 == 2{
                cell.lbl_Gender.text = "Women Only"
            }else if homeList[indexPath.row].attendeesType ?? 0 == 3{
                cell.lbl_Gender.text = "Family"
            }else{
                cell.lbl_Gender.text = ""
            }
            
            DispatchQueue.main.async {
                
                if let imageUrl =  self.homeList[indexPath.row].classesLogo{
                    cell.img_Logo.sd_setImage(with: URL(string: imageUrl), placeholderImage: #imageLiteral(resourceName: "default_header_iPhone"))
                }else{
                    cell.img_Logo.image = #imageLiteral(resourceName: "default_header_iPhone")
                }
            }
            cell.lbl_Title1.tag = indexPath.row
            cell.btnImageLogo.tag = indexPath.row
            cell.lbl_Title1.setTitle(homeList[indexPath.row].org_abbr, for: .normal)
            cell.lbl_Title1.addTarget(self, action: #selector(btn_OrgDetails(sender:)), for: .touchUpInside)
            cell.btnImageLogo.addTarget(self, action: #selector(btn_OrgDetails(sender:)), for: .touchUpInside)
            cell.lbl_Title2.text = ""
            
            cell.lbl_TeacherNAme.text = (homeList[indexPath.row].teacherName ?? "")
            
            cell.btn_ViewCategory.setImage(#imageLiteral(resourceName: "classes_iPhone"), for: .normal)
            cell.lbl_Title4.text = homeList[indexPath.row].title
            cell.View_CategoryColor.backgroundColor = UIColor.init(hex: 0xb68efe)
            cell.lbl_AgeGroup.text = homeList[indexPath.row].ageGroup ?? "0 to 0"
            
            break
        }
        
        
        
        
        cell.lbl_ReadMore.text = homeList[indexPath.row].datumDescription 
        
        //      //mark:number deduction an d url
        cell.lbl_ReadMore.enabledTextCheckingTypes = NSTextCheckingResult.CheckingType.link.rawValue
        cell.lbl_ReadMore.delegate = self
        
        self.phoneNumberDetctor(input:  cell.lbl_ReadMore.text as! String)
        self.uRLDetctor(input:  cell.lbl_ReadMore.text as! String)
        
        let newText =  cell.lbl_ReadMore.text as! NSString
        
        if self.array_String.count != 0{
            for i in 0...self.array_String.count - 1{
                let range:NSRange = newText.range(of: array_String[i])
                cell.lbl_ReadMore.addLink(to: URL(string: array_String[i]), with: range)
            }
        }
        let countLabl : Int =  cell.lbl_ReadMore.calculateMaxLines()
        
        if countLabl == 1{
            cell.btnShowHide.setTitle(nil, for: .normal)
            cell.btnShowHide.setImage(nil, for: .normal)
            cell.imgContraint_ButtonMoreHeight.constant = 0.0
            cell.btnShowHide.isUserInteractionEnabled = false
        }else{
            let model = self.homeList[indexPath.row]
            if model.isMoreTapped == true{
                cell.btnShowHide.setTitle("Show less".localized, for: .normal)
                cell.lbl_ReadMore.numberOfLines = 0
            }else{
                cell.btnShowHide.setTitle("Show more".localized, for: .normal)
                cell.lbl_ReadMore.numberOfLines = 1
            }
            
            cell.btnShowHide.setImage(#imageLiteral(resourceName: "down_black_arrow_iPhone"), for: .normal)
            cell.imgContraint_ButtonMoreHeight.constant = 30.0
            cell.btnShowHide.isUserInteractionEnabled = true
        }
        
        
        if homeList[indexPath.row].is_private == true{
            cell.btnShare.isHidden = true
        }else{
            cell.btnShare.isHidden = false
            cell.btnShare.addTarget(self, action: #selector(btn_shareTextButton(sender:)), for: .touchUpInside)
        }
        
        
        cell.btnReminder.addTarget(self, action: #selector(btn_EventRemainderTapped(sender:)), for: .touchUpInside)
        
        
        
        if self.homeList[indexPath.row].going == true{
            cell.btnGoing.setImage(#imageLiteral(resourceName: "tick_icon_iPhone"), for: .normal)
        }else{
            cell.btnGoing.setImage(#imageLiteral(resourceName: "event_Going"), for: .normal)
        }
        
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 1000
    }
}


//MARK:CollectionView-----------------------------------
extension HomeViewController:UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout{
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        
        return 2
    }
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        if section == 0{
            return 1
        }else{
            return self.allfavOnlyOrganizationlists.count
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if indexPath.section == 0{
            
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Home_AddMoreCollectionViewCell", for: indexPath) as! Home_AddMoreCollectionViewCell
            if let langugae = UserDefaults.standard.value(forKey: Constant.LANGUAGE) as? String{
                      
                if langugae == "ar"{
                    
                    cell.btn_Addmore.semanticContentAttribute = .forceRightToLeft
                }else if langugae == "ms"{
                     cell.btn_Addmore.semanticContentAttribute = .forceRightToLeft
                }else{
                     cell.btn_Addmore.semanticContentAttribute = .forceRightToLeft
                }
            }
               cell.btn_Addmore.semanticContentAttribute = .forceRightToLeft
            cell.btn_Addmore.setTitle("ADD MORE".localized, for: .normal)
            
            return cell
            
        }else{
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "HomeCollectionViewCell", for: indexPath) as! HomeCollectionViewCell
            
            if (UserDefaults.standard.value(forKey: "Login_Token") as? String) != nil{
                cell.img_View.borderColor = UIColor.init(hex: 0xF4A441)
                if let superAdmin = UserDefaults.standard.value(forKey: "isSuperAdmin") as? Bool{
                    
                    if superAdmin == true{
                        
                        cell.img_View.borderColor = UIColor.init(hex: 0xF4A441)
                    }else{
                        
                        if let is_staff = UserDefaults.standard.value(forKey: "is_staff") as? Bool{
                            
                            if is_staff == true{
                                //mark:check For own Organisation---------------------------
                                if let orgOwn_Array = UserDefaults.standard.value(forKey: "OwnOrg_Array") as? [Int]{
                                    
                                    if orgOwn_Array.contains(self.allfavOnlyOrganizationlists[indexPath.row].id ?? 0){
                                        
                                        cell.img_View.borderColor = UIColor.red
                                        
                                    }else{
                                        cell.img_View.borderColor = UIColor.init(hex: 0xF4A441)
                                        
                                    }//orgOwn
                                    
                                }
                            }//staff true
                        }//staff
                        
                    }
                }//SuperAdmin
            }//Login
            else{
                
                
                cell.img_View.borderColor = UIColor.init(hex: 0xF4A441)
                
            }
            
            //            let allFav_Notification = self.allfavOnlyOrganizationlists.map({$0.id }) as! [Int]
            //            print("org Id - ",allFav_Notification)
            //
            //
            let notication = AppDelegate.instance.notification_OrgId
            //
            //                if notication.count != 0{
            //                             for i in 0...notication.count - 1{
            //
            //                                       if (notication[i] == self.allfavOnlyOrganizationlists[indexPath.row].id){
            //                                             cell.img_Notification.isHidden = false
            //                                         }else{
            //                                             cell.img_Notification.isHidden = true
            //                                         }
            //                                     }
            //                         }else{
            //
            //                             cell.img_Notification.isHidden = true
            //                         }
            //
            //
            
            
            let contains = notication.contains(where: { $0 == self.allfavOnlyOrganizationlists[indexPath.row].id })
            
            if let index = notication.index(of: self.allfavOnlyOrganizationlists[indexPath.row].id!) {
                cell.img_Notification.isHidden = false
            }else{
                cell.img_Notification.isHidden = true
            }
            
            cell.lbl_Title.text = self.allfavOnlyOrganizationlists[indexPath.row].abbreviation ?? ""
            DispatchQueue.main.async {
                if let photo = self.allfavOnlyOrganizationlists[indexPath.row].logo, let url = URL(string: photo) {
                    cell.img_View.sd_setImage(with: url, placeholderImage: UIImage.init(named: "default_icon_iPhone_Logo.png"))
                } else {
                    cell.img_View.image = UIImage(named: "default_icon_iPhone_Logo.png")
                    
                }
            }
            return cell
        }
        
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if indexPath.section == 0{
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "TabVC") as! TabVC
            vc.tabRegister = "HomeAddMore"
            
            self.tabBarController?.tabBar.isHidden = false
            self.slideMenuController()?.changeMainViewController(vc, close: true)
            vc.tabBarController?.selectedIndex = 1
        } else {
            
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "ActivitiesViewController") as! ActivitiesViewController
            vc.orgId = self.allfavOnlyOrganizationlists[indexPath.row].id ?? 0
            vc.allFavOrg = "AllFavOrg"
            vc.org_type = self.allfavOnlyOrganizationlists[indexPath.row].org_type ?? ""
            let allfavOrgId = allfavOnlyOrganizationlists.map({ $0.id as! Int})
            vc.array_allfavOrgId = allfavOrgId
            vc.modalPresentationStyle = .fullScreen
            // print(allfavOrgId)
            self.present(vc, animated: true, completion: nil)
        }
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        if indexPath.section == 0{
            return  CGSize(width: 90 , height: 100)
        }else{
            return CGSize(width: collectionView.bounds.size.width/4 - 20 , height: 100)
            
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        insetForSectionAt section: Int) -> UIEdgeInsets{
        
        if section == 0{
            return  UIEdgeInsets( top: 0,  left: 5, bottom: 0,  right: 0)
        }else{
            return  UIEdgeInsets( top: 0,  left: 0, bottom: 0,  right: 10)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        if section == 0{
            return  5.0
        }else{
            return 0.0
        }
        
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        
        return 0.0
        
    }
    
    func stringToDate(dateString:String){
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd-MM-yyyy HH:mm"
        dateFormatter.locale = Locale(identifier: "en_US_POSIX") // set locale to reliable US_POSIX
        self.ReminderDate = dateFormatter.date(from:dateString)!
    }
    func shareDateFormat(startDate:String){
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd-MM-yyyy"
        let date = dateFormatter.date(from: startDate )
        let dateFormatter1 = DateFormatter()
        dateFormatter1.dateFormat = "EEE, d MMM yyyy"
        let date1 = dateFormatter1.string(from: date ?? Date())
        self.shareDate = date1
        
        
        
    }
    
    func shareTimeFormat(startTime:String,endTime:String){
        let timedateFormatter = DateFormatter()
        timedateFormatter.dateFormat = "HH:mm"
        let starttime = timedateFormatter.date(from: startTime )
        let endTime = timedateFormatter.date(from: endTime )
        let timedateFormatter1 = DateFormatter()
        timedateFormatter1.dateFormat = "h:mm a"
        let startTime1 = timedateFormatter1.string(from: starttime ?? Date())
        let endTime1 = timedateFormatter1.string(from: endTime ?? Date())
        
        self.shareTime = startTime1 + " - " + endTime1
    }
    
}

extension UIViewController{
    
    func btn_ShareTapp(details:[String]){
        
        
        // set up activity view controller
        
        let activityViewController = UIActivityViewController(activityItems: details, applicationActivities: nil)
        activityViewController.popoverPresentationController?.sourceView = self.view // so that iPads won't crash
        
        // exclude some activity types from the list (optional)
        activityViewController.excludedActivityTypes = [ UIActivity.ActivityType.airDrop, UIActivity.ActivityType.postToFacebook ]
        
        // present the view controller
        self.present(activityViewController, animated: true, completion: nil)
    }
}

extension UIViewController {
    
    func statusBarColorChange(color:UIColor){
        
        if #available(iOS 13.0, *) {
            
            let app = UIApplication.shared
            let statusBarHeight: CGFloat = app.statusBarFrame.size.height
            
            let statusbarView = UIView()
            statusbarView.backgroundColor = color
            view.addSubview(statusbarView)
            
            statusbarView.translatesAutoresizingMaskIntoConstraints = false
            statusbarView.heightAnchor
                .constraint(equalToConstant: statusBarHeight).isActive = true
            statusbarView.widthAnchor
                .constraint(equalTo: view.widthAnchor, multiplier: 1.0).isActive = true
            statusbarView.topAnchor
                .constraint(equalTo: view.topAnchor).isActive = true
            statusbarView.centerXAnchor
                .constraint(equalTo: view.centerXAnchor).isActive = true
            
        } else {
            
            let statusBar = UIApplication.shared.value(forKeyPath: "statusBarWindow.statusBar") as? UIView
            statusBar?.backgroundColor = color
        }
    }
}

extension UIViewController{
    
    func getPrivilegesDataApi(){
        KVNProgress.show()
        Network.shared.getPrivilegesData { (result) in
            KVNProgress.dismiss()
            guard result != nil else{
                return
            }
            if result?.success == true{
                var array_P = [PriviligesData]()
                if result?.data?.privileges != nil {
                    array_P = (result?.data?.privileges)!
                    let array_privileges = array_P.map({$0.org_id })
                    print("org Id - ",array_privileges)
                    UserDefaults.standard.set(array_privileges, forKey: "OwnOrg_Array")
                    Global.shared.priviligesData = array_P
                }
                UserDefaults.standard.synchronize()
            }
        }
        
    }
    
    func openCalendar(with date: Date) {
        guard let url = URL(string: "calshow:\(date.timeIntervalSinceReferenceDate)") else {
            return
        }
        UIApplication.shared.open(url, options: [:], completionHandler: nil)
    }
}
extension HomeViewController:TTTAttributedLabelDelegate{
    
    func phoneNumberDetctor(input:String){
        
        let detector = try! NSDataDetector(types: NSTextCheckingResult.CheckingType.phoneNumber.rawValue)
        let matches = detector.matches(in: input, options: [], range: NSRange(location: 0, length: input.utf16.count))
        
        for match in matches {
            guard let range = Range(match.range, in: input) else { continue }
            let url = input[range]
            print(url)
            array_String.append(String(url))
            
            
            
        }
        
        
    }
    func uRLDetctor(input:String){
        
        
        let detector = try! NSDataDetector(types: NSTextCheckingResult.CheckingType.link.rawValue)
        let matches = detector.matches(in: input, options: [], range: NSRange(location: 0, length: input.utf16.count))
        
        for match in matches {
            guard let range = Range(match.range, in: input) else { continue }
            let url = input[range]
            print(url)
            
            array_String.append(String(url))
            
            
            
        }
        
        
    }
    
    
    func phoneAndUrlDetctor(tap_string:String){
        
        //        let tap_string = array_String[view.tag]
        //
        //
        let detector = try! NSDataDetector(types: NSTextCheckingResult.CheckingType.phoneNumber.rawValue)
        let matches = detector.matches(in: tap_string, options: [], range: NSRange(location: 0, length: tap_string.utf16.count))
        
        for match in matches {
            guard let range = Range(match.range, in: tap_string) else { continue }
            let url = tap_string[range]
            print(url)
            
            self.phoneNumber = "true"
            
        }
        
        let detectorUrl = try! NSDataDetector(types: NSTextCheckingResult.CheckingType.link.rawValue)
        let matchesurl = detectorUrl.matches(in: tap_string, options: [], range: NSRange(location: 0, length: tap_string.utf16.count))
        
        for match in matchesurl {
            guard let range = Range(match.range, in: tap_string) else { continue }
            let url = tap_string[range]
            print(url)
            
            self.phoneNumber = "false"
            
            
            
        }
        
        if self.phoneNumber == "true"{
            
            if let url = URL(string:"tel://\(tap_string )"), UIApplication.shared.canOpenURL(url) {
                UIApplication.shared.openURL(url)
            }
        }else if self.phoneNumber == "false"{
            
            let urlString = ("\(tap_string )")
            var urll = String()
            
            
            if urlString.contains("https://") {
                urll = ("\(tap_string)")
            }else if urlString.contains("http://") {
                urll = ("\(tap_string)")
            }else{
                urll = ("http://\(tap_string)")
            }
            
            
            guard let url = URL(string: urll) else { return }
            UIApplication.shared.openURL(url)
        }
    }
    
    
    func attributedLabel(_ label: TTTAttributedLabel!, didSelectLinkWith url: URL!) {
        debugPrint("label>", label)
        
        debugPrint("addressComponents>", url)
        
        self.phoneAndUrlDetctor(tap_string: String(describing: url!) )
    }
    
    
}

