//
//  TabVC.swift
//  Muslim Hub
//
//  Created by Sakshi Singh on 03/04/19.
//  Copyright © 2019 OnGraph. All rights reserved.
//

import UIKit
import SlideMenuControllerSwift

class TabVC: UITabBarController,UINavigationControllerDelegate,UITabBarControllerDelegate {
    
    var tabRegister = String()
    var ManageAdminList = String()
    var currentFontSize = ""
    
  
    override func viewDidLoad() {
        super.viewDidLoad()
     
        tabBar.invalidateIntrinsicContentSize()
        tabBar.superview?.setNeedsLayout()
        tabBar.superview?.layoutSubviews()
        tabBar.items?[0].title = "News Feed".localized
        tabBar.items?[1].title = "Orgs".localized
        tabBar.items?[2].title = "Around Me".localized
        tabBar.items?[3].title = "Donate".localized
        tabBar.items?[4].title = "Menu".localized
        
        
        if tabRegister == "SuperAdminRegister"{
            print(self.selectedIndex)
            self.selectedIndex = 1
            print(self.selectedIndex)
            toggleLeft()
        }else if tabRegister == "HomeAddMore"{
            print(self.selectedIndex)
            self.selectedIndex = 1
            print(self.selectedIndex)
            toggleLeft()
        }

        UITabBar.appearance().barTintColor = UIColor.init(hex:  0x054669)
        UITabBar.appearance().tintColor = UIColor.white
         self.delegate = self
        UITabBarItem.appearance().setTitleTextAttributes([NSAttributedString.Key.font: UIFont(name: "Avenir Book", size: 12)!], for: .normal)
        UITabBarItem.appearance().setTitleTextAttributes([NSAttributedString.Key.font: UIFont(name: "Avenir Book", size: 12)!], for: .selected)
        UITabBarItem.appearance().setTitleTextAttributes([NSAttributedString.Key.foregroundColor: UIColor.white], for: .selected)
       
        UITabBarItem.appearance().setTitleTextAttributes([NSAttributedString.Key.foregroundColor: UIColor.init(hex: 0xABABAB)], for: .normal)
        
        if UIDevice().userInterfaceIdiom == .phone {
            switch UIScreen.main.nativeBounds.height {
            case 2436:
                print("iPhone X, XS")
                
                self.tabBarController?.tabBar.items![2].image = UIImage(named: "your image name")
                
                self.tabBarController?.tabBar.items![2].selectedImage = UIImage(named: "your image name")
                
            default:
                print("Unknown")
        }
        }
        
     
    }
   
    func tabBarController(_ tabBarController: UITabBarController, shouldSelect viewController: UIViewController) -> Bool {
        
        
        
        if viewController is MenuViewController {
            
//            if AppDelegate.instance.isLogout == true{
//              AppDelegate.instance.isLogout = false
//                print("Logout case")
//            }else{
             AppDelegate.instance.isLogout = false
                 self.toggleLeft()
            print("toggle Left")
         
            
            return false
        }
        return true
    }
    
    

}
