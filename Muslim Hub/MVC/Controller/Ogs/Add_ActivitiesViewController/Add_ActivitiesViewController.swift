//
//  Add_ActivitiesViewController.swift
//  Muslim Hub
//
//  Created by Ongraph on 30/05/19.
//  Copyright © 2019 OnGraph. All rights reserved.
//

import UIKit
import SDWebImage
import GooglePlaces
import YangMingShan
import KVNProgress

class Add_ActivitiesViewController: UIViewController,UITextFieldDelegate,UITextViewDelegate {
    
    //MARK:IBOUTLets----------------------------------------------------
    
    //mark:Outlets for Image_Upload-------------------------------------
    @IBOutlet weak var btn_Cancel:UIButton!
    @IBOutlet weak var btn_Save:UIButton!
    @IBOutlet weak var btn_Upload:UIButton!
    @IBOutlet weak var view_UploadPhoto:UIView!
    @IBOutlet weak var collectionView_Images:UICollectionView!
    @IBOutlet weak var view_UpperTableView:UIView!
    @IBOutlet weak var view_LowerTableView:UIView!
    @IBOutlet weak var lbl_NoImgae:UILabel!
    @IBOutlet weak var lbl_Header:UILabel!
      @IBOutlet weak var lbl_UploadImages:UILabel!
      @IBOutlet weak var lbl_Max6:UILabel!
    var imageData = [Data]()
    var arrayImages = [UIImage]()
    var imageArray: [Any] = []
    var orgId = Int()
    //mark:Outlets for Form-------------------------------------
    @IBOutlet weak var tableView_AddEvents:UITableView!
    var lat = Double()
    var long = Double()
    var dic_RegisterValue = [String:Any]()
    var tag_forAddress:Int!
    var activitesDetails: EventData!
    var addNewActivites:String!
    var startDate = Date()
    var endate = Date()
    var startTime = Date()
    var endatTime = Date()
    
    
    var computedCurrentDate : Date {
        let dat = MHHelper.shared.convertToDateFrom(date: Date())
        return dat
    }
    
    var str_Time12hrs = ""
    //mark:Array of variables-------------------------------------------
    let array_StartTime = ["Start Time","End Time"]
    let array_StartDate = ["Start Date","End Date"]
    let array_RepeatingDay = ["Weekly","Fortnightly","Monthly"]
    
    let array_AllData = ["Title","Description","Location","Start_Date","Start_Time","End_Date","End_Time","Repeating_Day"]
    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        statusBarColorChange(color:UIColor.init(hex: 0x05BDB9))
        
        self.btn_Cancel.setTitle("Cancel".localized, for: .normal)
            self.btn_Save.setTitle("Save".localized, for: .normal)
            self.lbl_Header.text = "Add Activities".localized
            self.lbl_UploadImages.text = "Upload Images".localized
            self.lbl_Max6.text = "6 Images Maximum".localized
        //MarK:Condition for Edit Event
        lbl_NoImgae.isHidden = true
        if activitesDetails != nil{
            if activitesDetails.latitude != nil{
                
                self.lat = activitesDetails.latitude?.rounded(toPlaces: 6) ?? 0.0
            }
            if activitesDetails.longitude != nil{
                
                self.long = activitesDetails.longitude?.rounded(toPlaces: 6) ?? 0.0
            }
            
            if activitesDetails.images?.count != 0{
                
                collectionView_Images.isHidden = false
                self.view_UploadPhoto.isHidden = true
                lbl_NoImgae.isHidden = true
            }else{
                collectionView_Images.isHidden = true
                self.view_UploadPhoto.isHidden = true
                lbl_NoImgae.isHidden = false
            }
            if activitesDetails.title != nil{
                dic_RegisterValue.updateValue(activitesDetails.title!, forKey: array_AllData[0])
            }
            if activitesDetails.description != nil{
                dic_RegisterValue.updateValue(activitesDetails.description!, forKey: array_AllData[1])
            }
            if activitesDetails.address != nil{
                dic_RegisterValue.updateValue(activitesDetails.address!, forKey: array_AllData[2])
            }
            if activitesDetails.start_date != nil{
                
                let dateString = activitesDetails.start_date
                let dateFormatter = DateFormatter()
                dateFormatter.dateFormat = "dd-MM-yyyy"
                dateFormatter.timeZone = TimeZone(identifier: "UTC")
                dateFormatter.locale = Locale(identifier: "en_US_POSIX")
                let date = dateFormatter.date(from: dateString!)
                print(date!)
                self.startDate = date!
                
                dic_RegisterValue.updateValue(activitesDetails.start_date!, forKey: array_AllData[3])
            }
            if activitesDetails.start_time != nil{
                dic_RegisterValue.updateValue(activitesDetails.start_time!, forKey: array_AllData[4])
            }
            if activitesDetails.end_date != nil{
                
                let dateString = activitesDetails.end_date
                let dateFormatter = DateFormatter()
                dateFormatter.dateFormat = "dd-MM-yyyy"
                dateFormatter.timeZone = TimeZone(identifier: "UTC")
                dateFormatter.locale = Locale(identifier: "en_US_POSIX")
                let date = dateFormatter.date(from: dateString!)
                print(date!)
                
                self.endate = date!
                
                dic_RegisterValue.updateValue(activitesDetails.end_date!, forKey: array_AllData[5])
            }
            if activitesDetails.end_time != nil{
                let whole_current = (activitesDetails.end_date! + " " + activitesDetails.end_time!)
                
                let dateFormatter = DateFormatter()
                dateFormatter.dateFormat = "dd-MM-yyyy HH:mm"
                dateFormatter.timeZone = TimeZone(identifier: "UTC")
                dateFormatter.locale = Locale(identifier: "en_US_POSIX")
                
                
                self.endatTime = dateFormatter.date(from: whole_current)!
                
                dic_RegisterValue.updateValue(activitesDetails.end_time!, forKey: array_AllData[6])
            }
            if activitesDetails.repeating_day != nil{
                dic_RegisterValue.updateValue(activitesDetails.repeating_day!, forKey: array_AllData[7])
            }
            print(dic_RegisterValue)
            
        }else{
            
            for i in 0...array_AllData.count - 1{
                if dic_RegisterValue[array_AllData[i]] == nil{
                    
                    self.dic_RegisterValue.updateValue("", forKey:array_AllData[i])
                    self.dic_RegisterValue.updateValue("Description".localized, forKey:"Description")
                }
            }
            
        }
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.cornerRadiusAndShadow()
        // UIApplication.statusBarBackgroundColor = UIColor.init(hex: 0x05BDB9)
        if addNewActivites == "addNewActivites"{
            if arrayImages.count > 0{
                
                collectionView_Images.isHidden = false
                self.view_UploadPhoto.isHidden = true
            }else{
                collectionView_Images.isHidden = true
                self.view_UploadPhoto.isHidden = false
            }}
    }
    
    //MARK:TextView Delegate and DataSource----------------------------------------------------
    func textViewDidBeginEditing(_ textView: UITextView) {
        guard let indexPath = textView.tableViewIndexPath(self.tableView_AddEvents) else { return }
        
        switch (indexPath.section, 1) {
            
        case (0,1):
            
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "MHDescViewController") as! MHDescViewController
            vc.delegate = self
            vc.textDesc = textView.text!
            textView.keyboardType = .default
            vc.modalPresentationStyle = .fullScreen
            present(vc, animated: false, completion: nil)
            
        default:
            break
            
        }
        
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        
        textView.textColor = UIColor.black
        self.dic_RegisterValue.updateValue(textView.text as Any, forKey: self.array_AllData[1]) // Description
    }
    
    
    //Mark:TextField Delegate and DataSource Method--------------------
    func textFieldDidBeginEditing(_ textField: UITextField) {
        guard let indexPath = textField.tableViewIndexPath(self.tableView_AddEvents) else { return }
        
        let index = IndexPath(row: textField.tag, section: 1)
        
        let cellSection1: Add_ActivitiesEvent_Section1TableViewCell = self.tableView_AddEvents.cellForRow(at: index) as! Add_ActivitiesEvent_Section1TableViewCell
        
        
        switch (indexPath.section, indexPath.row) {
        case (0,0):
            self.dic_RegisterValue[self.array_AllData[0]] = textField.text
        case (0,2):
            let autocompleteController = GMSAutocompleteViewController()
            autocompleteController.delegate = self
            let filter = GMSAutocompleteFilter()
            filter.type = .noFilter  //suitable filter type
            //filter.country = "AUS"  //appropriate country code
            autocompleteController.autocompleteFilter = filter
            self.tag_forAddress = 2
            present(autocompleteController, animated: true, completion: nil)
            
        case (1,0):
            
            if textField == cellSection1.txt_Date {
                AKDatePicker.openPicker(in: textField, currentDate: Date(), pickerMode: .date) { (value) in
                    
                    if let eDate  = self.dic_RegisterValue["End_Date"] as? String , !eDate.isEmpty {
                        let sDate = self.convertToString(date: value, dateformat: "dd-MM-yyyy")
                        let eDate = self.convertToString(date: self.endate , dateformat: "dd-MM-yyyy")
                        let sDt = MHHelper.shared.convertToDate(from: sDate, currentFormat: "dd-MM-yyyy")
                        let eDt = MHHelper.shared.convertToDate(from: eDate, currentFormat: "dd-MM-yyyy")
                        
                        if eDt.compare(sDt) == .orderedAscending {
                            AKAlertController.alert("Start date should be greater than End date", message: "")
                        } else {
                            textField.text = sDate
                            self.startDate = value
                            self.dic_RegisterValue[self.array_AllData[3]] = textField.text ?? ""
                        }
                    } else {
                        textField.text = self.convertToString(date: value, dateformat: "dd-MM-yyyy")
                        self.endate = value
                        self.startDate = value
                        self.dic_RegisterValue[self.array_AllData[3]] = textField.text ?? ""
                        self.dic_RegisterValue[self.array_AllData[5]] = textField.text ?? ""
                        self.tableView_AddEvents.reloadSections(NSIndexSet(index: 1) as IndexSet, with: UITableView.RowAnimation.none)
                    }
                    
                }
            } else if textField == cellSection1.txt_Time {
                
                if (cellSection1.txt_Date.text ?? "").isEmpty {
                    AKAlertController.alert("Please enter start date first")
                } else {
                    let startDT = MHHelper.shared.convertToDate(from: cellSection1.txt_Date.text ?? "", currentFormat: "dd-MM-yyyy")
                    
                    if startDT.compare(self.computedCurrentDate) == .orderedAscending {
                        // curent date is greater than the start date
                    } else {
                        // curent date is greater than or equal to the start date
                        var minDate : Date? = Date()
                        if self.computedCurrentDate.compare(startDT) == .orderedAscending {
                            minDate = nil
                        }
                        
                        AKTimePicker.openPicker(in: textField, currentDate: Date(), minimumDate: minDate, pickerMode: .time) { (value) in
                            //  self.startTime = value
                            
                            let startDtee = String.getString(with: self.dic_RegisterValue["Start_Date"])
                            let start_Time = self.convertToString(date: value, dateformat: "HH:mm")
                            let startTime = (startDtee + " " + start_Time)
                            
                            let finalStartDate = MHHelper.shared.convertToDate(from: startTime, currentFormat: "dd-MM-yyyy HH:mm")
                            
                            let eTime = String.getString(with:self.dic_RegisterValue["End_Time"])
                            if !eTime.isEmpty {
                                let endDT = String.getString(with:self.dic_RegisterValue["End_Date"])
                                let eT = "\(endDT) \(eTime)"
                                let finalEndDT = MHHelper.shared.convertToDate(from: eT, currentFormat: "dd-MM-yyyy HH:mm")
                                if finalEndDT.compare(finalStartDate) == .orderedAscending {
                                    AKAlertController.alert("Start time should be greater than End time", message: "")
                                } else {
                                    self.dic_RegisterValue[ self.array_AllData[4]] = start_Time
                                    self.startTime = finalStartDate
                                    textField.text = self.convertToString(date: value, dateformat: "h:mm a")
                                }
                            } else {
                                self.dic_RegisterValue[ self.array_AllData[4]] = start_Time
                                self.startTime = finalStartDate
                                textField.text = self.convertToString(date: value, dateformat: "h:mm a")
                            }
                        }
                        
                    }
                }
            }
        case (1,1):
            if textField == cellSection1.txt_Date {
                AKDatePicker.openPicker(in: textField, currentDate: Date(), pickerMode: .date) { (value) in
                    let sDT = String.getString(with:self.dic_RegisterValue["Start_Date"])
                    let sDate = MHHelper.shared.convertToDate(from: sDT, currentFormat: "dd-MM-yyyy")
                    if sDT.isEmpty {
                        AKAlertController.alert("Please select start Date First", message: "")
                    } else {
                        let eDT = self.convertToString(date: value, dateformat: "dd-MM-yyyy")
                        let eDate = MHHelper.shared.convertToDate(from: eDT, currentFormat: "dd-MM-yyyy")
                        if eDate.compare(sDate) == .orderedAscending {
                            AKAlertController.alert("Start date should be greater than End date", message: "")
                        } else {
                            self.endate = value
                            textField.text = self.convertToString(date: value, dateformat: "dd-MM-yyyy")
                            self.dic_RegisterValue[self.array_AllData[5]] = textField.text ?? "" // End Date
                        }
                    }
                    
                   
                }
            } else if textField == cellSection1.txt_Time {
                
                if (cellSection1.txt_Date.text ?? "").isEmpty {
                    AKAlertController.alert("Please enter start date first")
                } else {
                    let endDT = MHHelper.shared.convertToDate(from: cellSection1.txt_Date.text ?? "", currentFormat: "dd-MM-yyyy")
                    
                    if endDT.compare(self.computedCurrentDate) == .orderedAscending {
                        // curent date is greater than the start date
                    } else {
                        // curent date is greater than or equal to the start date
                        var minDate : Date? = Date()
                        if self.computedCurrentDate.compare(endDT) == .orderedAscending {
                            minDate = nil
                        }
                        AKTimePicker.openPicker(in: textField, currentDate: Date(), minimumDate: minDate, pickerMode: .time) { (value) in
                            //  self.startTime = value
                            
                            let endDtee = self.convertToString(date: self.endate, dateformat: "dd-MM-yyyy")
                            let end_Time = self.convertToString(date: value, dateformat: "HH:mm")
                            let endTime = (endDtee + " " + end_Time)
                            
                            let finalEndDate = MHHelper.shared.convertToDate(from: endTime, currentFormat: "dd-MM-yyyy HH:mm")
                            
                            if finalEndDate.compare(self.startTime) == .orderedAscending {
                                AKAlertController.alert("Start time should be greater than End time", message: "")
                            } else {
                                self.endatTime = finalEndDate
                                textField.text = self.convertToString(date: value, dateformat: "h:mm a")
                                let end_Time = self.convertToString(date: value, dateformat: "HH:mm")
                                self.dic_RegisterValue[self.array_AllData[6]] = end_Time
                            }
                        }
                    }
                }
            }
            
                    
            
        case(2,0):
            
            AKMultiPicker().openPickerIn(textField, firstComponentArray: array_RepeatingDay) { (value, _, index1, _) in
                textField.text = value
                
                if value == "Weekly"{
                    self.dic_RegisterValue.updateValue(1, forKey: self.array_AllData[7]) // Repeating day
                }else if value == "Fortnightly"{
                    self.dic_RegisterValue.updateValue(2, forKey: self.array_AllData[7]) // Repeating day
                }else if value == "Monthly"{
                    self.dic_RegisterValue.updateValue(3, forKey: self.array_AllData[7]) // Repeating day
                }
                
            }
            
        default:
            break
        }
        
        
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        guard let indexPath = textField.tableViewIndexPath(self.tableView_AddEvents) else { return }
        
        let index = IndexPath(row: textField.tag, section: 1)
        
        let _: Add_ActivitiesEvent_Section1TableViewCell = self.tableView_AddEvents.cellForRow(at: index) as! Add_ActivitiesEvent_Section1TableViewCell
        
        
        
        switch (indexPath.section, indexPath.row) {
            
        case (0,0):
            
            self.dic_RegisterValue.updateValue(textField.text as Any, forKey: self.array_AllData[0]) // Title
        case (0,2):
            
            self.dic_RegisterValue.updateValue(textField.text as Any, forKey: self.array_AllData[2]) // Location
            
            
        case(2,0):
            
            self.dic_RegisterValue.updateValue(textField.text as Any, forKey: self.array_AllData[7]) // Repeating day
            
            
        default:
            break
        }
        
    }
    
    //Function And Methods-------------------------------------------
    
    func cornerRadiusAndShadow(){
        
        // corner radius for Save
        btn_Save.layer.cornerRadius = 5
        btn_Save.shadowBlackToHeader()
        
        // corner radius for Cancel
        btn_Cancel.layer.cornerRadius = 5
        btn_Cancel.shadowBlackToHeader()
        
        
        
        view_UpperTableView.clipsToBounds = true
        view_UpperTableView.layer.cornerRadius = 6
        view_UpperTableView.layer.maskedCorners =
            [.layerMaxXMinYCorner, .layerMinXMinYCorner]
        
        
        
        view_LowerTableView.layer.cornerRadius = 8
        view_LowerTableView.layer.maskedCorners =  [.layerMaxXMaxYCorner, .layerMinXMaxYCorner]
        
        view_LowerTableView.layer.shadowColor = UIColor.black.cgColor
        view_LowerTableView.layer.shadowOffset = CGSize(width: 0, height: 3)
        view_LowerTableView.layer.shadowOpacity = 0.4
        view_LowerTableView.layer.shadowRadius = 2.0
        
    }
    
    
    
    //MARK:Function Api----------------------------------------
    func AddActivitesApi(imageData : [Data]){
        
        if dic_RegisterValue["Title"] as? String  == ""{
            
            AKAlertController.alert("Please enter Title")
        }
        else if dic_RegisterValue["Description"] as? String  == "Description" {
            
            AKAlertController.alert("Please enter Description")
        }else if dic_RegisterValue["Description"] as? String  == "" {
            AKAlertController.alert("Please enter Description")
        }
        else if dic_RegisterValue["Location"] as? String  == ""{
            
            AKAlertController.alert("Please enter Location")
        }
        else if dic_RegisterValue["Start_Date"] as? String  == ""{
            
            AKAlertController.alert("Please enter Start Date")
        }
        else if dic_RegisterValue["Start_Time"] as? String  == ""{
            
            AKAlertController.alert("Please enter Start Time")
        }
        else if dic_RegisterValue["End_Date"] as? String  == ""{
            
            AKAlertController.alert("Please enter End Date")
        }
        else if dic_RegisterValue["End_Time"] as? String  == ""{
            
            AKAlertController.alert("Please enter End Time")
        }
        else if dic_RegisterValue["Repeating_Day"] as? String  == ""{
            
            AKAlertController.alert("Please select Repeating Day")
            
        }else{
            
            let tempCurrentDate = MHHelper.shared.convertToStrinDate(date: Date(), desiredFormat: "dd-MM-yyyy HH:mm")
                   
                   let myCurrentDate = MHHelper.shared.convertToDate(from:tempCurrentDate , currentFormat: "dd-MM-yyyy HH:mm")
                   
                   
                   let startTime = dic_RegisterValue["Start_Time"] as! String
                   let whole_Date = (dic_RegisterValue["Start_Date"] as! String + " " + startTime)
                   
                   let end_Time = dic_RegisterValue["End_Time"] as! String
                   let whole_endDate = (dic_RegisterValue["End_Date"] as! String + " " + end_Time )
                   
                   
                   
                   
                   let startTime_enter = MHHelper.shared.convertToDate(from: whole_Date, currentFormat: "dd-MM-yyyy HH:mm")
                   
                   let endDate_enter = MHHelper.shared.convertToDate(from: whole_endDate, currentFormat: "dd-MM-yyyy HH:mm")
                   
             
                   
                   if startTime_enter.compare(myCurrentDate) == .orderedAscending {
                       AKAlertController.alert("Start time can not be lesser than current time")
                   }else {
                       if startTime_enter.compare(endDate_enter) == .orderedAscending || startTime_enter.compare(endDate_enter) == .orderedSame  {
                            self.addActivityApiCall()
                       }else{
                           AKAlertController.alert("Start time should be greater than End time")
                       }
                   }
                   
            
          
        }
        
        
    }
    
    
    func addActivityApiCall(){
        
        let repeating_Day = (dic_RegisterValue["Repeating_Day"] as! Int)
        let para:[String:Any] = ["org": orgId, "title": dic_RegisterValue["Title"] as! String,"description": dic_RegisterValue["Description"] as! String,"latitude":self.lat,"longitude":self.long,"address":dic_RegisterValue["Location"] as! String,"start_date":dic_RegisterValue["Start_Date"] as! String,"end_date":dic_RegisterValue["End_Date"] as! String,"start_time":dic_RegisterValue["Start_Time"] as! String,"end_time":dic_RegisterValue["End_Time"] as! String,"repeating_day":repeating_Day]
        KVNProgress.show()
        Network.shared.requestWithMultipleForActivites(imageData: imageData, image1: "", image2: "",image3: "",image4: "",image5: "",image6: "",parameters: para) { (result) in
            KVNProgress.dismiss()
            if result?.success == true{
                self.showSingleAlertMessage(message: "", subMsg: result?.message ?? "upload successfully", sender: self, completion:
                    { (success) -> Void in
                        if success == true{
                            self.presentingViewController?.presentingViewController?.dismiss(animated: true, completion: nil)
                        }
                        
                }
                )
            }else{
                self.showSingleAlertMessage(message: "", subMsg: result?.message ?? "", sender: self, completion:
                    { (success) -> Void in
                        if success == true{
                            
                        }
                        
                }
                )
            }
        }
    }
    
    
    func EditActivitesApi(){
        //        let myStartDt = MHHelper.shared.convertToDate(from:String.getString(with: dic_RegisterValue["Start_Date"]) , currentFormat: "dd-MM-yyyy")
        //
        //        let myEndDt = MHHelper.shared.convertToDate(from:String.getString(with: dic_RegisterValue["End_Date"]) , currentFormat: "dd-MM-yyyy")
        let tempCurrentDate = MHHelper.shared.convertToStrinDate(date: Date(), desiredFormat: "dd-MM-yyyy HH:mm")
        
        let myCurrentDate = MHHelper.shared.convertToDate(from:tempCurrentDate , currentFormat: "dd-MM-yyyy HH:mm")
        
        
        let startTime = dic_RegisterValue["Start_Time"] as! String
        let whole_Date = (dic_RegisterValue["Start_Date"] as! String + " " + startTime)
        
        let end_Time = dic_RegisterValue["End_Time"] as! String
        let whole_endDate = (dic_RegisterValue["End_Date"] as! String + " " + end_Time )
        
        
        
        
        let startTime_enter = MHHelper.shared.convertToDate(from: whole_Date, currentFormat: "dd-MM-yyyy HH:mm")
        
        let endDate_enter = MHHelper.shared.convertToDate(from: whole_endDate, currentFormat: "dd-MM-yyyy HH:mm")
        
        if dic_RegisterValue["Repeating_Day"] as? String  == ""{
            dic_RegisterValue.updateValue(1, forKey: "Repeating_Day")
        }
        
        
        if startTime_enter.compare(myCurrentDate) == .orderedAscending {
            AKAlertController.alert("Start time can not be lesser than current time")
        }else {
            if startTime_enter.compare(endDate_enter) == .orderedAscending || startTime_enter.compare(endDate_enter) == .orderedSame  {
                self.editactivityApiCall()
            }else{
                AKAlertController.alert("Start time should be greater than End time")
            }
        }
        
        
        
        //
        //        if (dic_RegisterValue["Start_Date"] as! String == AppDelegate.instance.currentDate) {
        //               AppDelegate.instance.current_date()
        //            let startTime = dic_RegisterValue["Start_Time"] as! String
        //            let whole_Date = (dic_RegisterValue["Start_Date"] as! String + " " + startTime)
        //
        //            let whole_current = (AppDelegate.instance.currentDate + " " + AppDelegate.instance.currentTime)
        //
        //            let dateFormatter = DateFormatter()
        //            dateFormatter.dateFormat = "dd-MM-yyyy HH:mm"
        //            dateFormatter.timeZone = TimeZone(identifier: "UTC")
        //            dateFormatter.locale = Locale(identifier: "en_US_POSIX")
        //
        //            let startTime_enter = dateFormatter.date(from: whole_Date)!
        //            let currentDate = dateFormatter.date(from: whole_current)!
        //
        //            if startTime_enter < currentDate{
        //
        //                AKAlertController.alert("Start time should be greater than End time")
        //            }else{
        //
        //                self.editactivityApiCall()
        //            }
        //
        //        } else if myStartDt.compare(myEndDt) == .orderedSame {
        //
        //
        //
        //
        //            if startTime_enter.compare(endDate_enter) == .orderedAscending || startTime_enter.compare(endDate_enter) == .orderedSame  {
        //                self.editactivityApiCall()
        //            }else{
        //                AKAlertController.alert("Start time should be greater than End time")
        //            }
        //
        //        }else{
        //
        //            self.editactivityApiCall()
        //        }
    }
    
    
    func editactivityApiCall(){
        
        
        let repeating_Day = (dic_RegisterValue["Repeating_Day"] as! Int)
//        guard let activityDetail = activitesDetails else {
//            return
//        }
        let para:[String:Any] = ["id":activitesDetails.id ?? 0,"org": activitesDetails.org ?? 0, "title": dic_RegisterValue["Title"] as! String,"description": dic_RegisterValue["Description"] as! String,"latitude":self.lat,"longitude":self.long.rounded(toPlaces: 6),"address":dic_RegisterValue["Location"] as! String,"start_date":dic_RegisterValue["Start_Date"] as! String,"end_date":dic_RegisterValue["End_Date"] as! String,"start_time":dic_RegisterValue["Start_Time"] as! String,"end_time":dic_RegisterValue["End_Time"] as! String,"repeating_day":repeating_Day]
        print(para)
        KVNProgress.show()
        Network.shared.editForActivites(parameters: para) { (result) in
            KVNProgress.dismiss()
            if result?.success == true{
                self.showSingleAlertMessage(message: "", subMsg: "upload successfully", sender: self, completion:
                    { (success) -> Void in
                        if success == true{
                            self.dismiss(animated: true, completion: nil)
                        }
                        
                }
                )
            }else{
                self.showSingleAlertMessage(message: "", subMsg: result?.message ?? "Something went wrong", sender: self, completion:
                    { (success) -> Void in
                        if success == true{
                            
                        }
                        
                }
                )
            }
        }}
    
    
    
    
    
    //MARK:IBACtion-----------------------------------------------
    @IBAction func btn_Save(_ sender:UIButton){
        if self.addNewActivites == "addNewActivites"{
            print(dic_RegisterValue)
            self.AddActivitesApi(imageData: imageData)
        }else{
            print(dic_RegisterValue)
            self.EditActivitesApi()
        }
        
    }
    
}

//MARK:TableView Delegate And DataSources--------------------------------------------------------
extension Add_ActivitiesViewController:UITableViewDelegate,UITableViewDataSource{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        
        return 3
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if section == 0{
            return 3
        }else if section == 1{
            return 2
        }else{
            
            return 1
        }
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if dic_RegisterValue[array_AllData[indexPath.row]] == nil{
            
            self.dic_RegisterValue.updateValue("", forKey:array_AllData[indexPath.row])
        }
        print(dic_RegisterValue)
        if indexPath.section == 0{
            
            let cell = self.tableView_AddEvents.dequeueReusableCell(withIdentifier: "Add_ActivitiesEventTableViewCell") as! Add_ActivitiesEventTableViewCell
            
            if indexPath.row == 0{
                if self.dic_RegisterValue["Title"] as? String != nil{
                    cell.txt_Title.text = self.dic_RegisterValue["Title"] as? String
                }
                if self.dic_RegisterValue["Title"] as? String == "" {
                    
                    cell.txt_Title.placeholder = "Title".localized
                    // cell.txt_Title.placeHolderColor = UIColor.init(hex: 0x807F7F)
                }
                cell.view_1.isHidden = false
                cell.view_2.isHidden = true
                cell.view_3.isHidden = true
                
                
            }else if indexPath.row == 1{
                if self.dic_RegisterValue["Description"] as? String != nil{
                    cell.txtView_Description.text = self.dic_RegisterValue["Description"] as? String
                }
                if cell.txtView_Description.text == "Description".localized{
                    cell.txtView_Description.textColor = UIColor.lightGray
                    self.dic_RegisterValue.updateValue("", forKey: "Description")
                }else{
                    cell.txtView_Description.textColor = UIColor.black
                    
                    
                }
                cell.view_1.isHidden = true
                cell.view_2.isHidden = false
                cell.view_3.isHidden = true
                
                
            }else if indexPath.row == 2{
                if self.dic_RegisterValue["Location"] as? String != nil{
                    cell.txt_Title.text = self.dic_RegisterValue["Location"] as? String
                }
                
                if self.dic_RegisterValue["Location"] as? String == "" {
                    
                    cell.txt_Title.placeholder = "Location".localized
                    //cell.txt_Title.placeholderColor = UIColor.init(hex: 0x807F7F)
                }
                
                
                cell.view_1.isHidden = false
                cell.view_2.isHidden = true
                cell.view_3.isHidden = true
                
                
            }
            return cell
        }else if indexPath.section == 1 {
            
            let cell = self.tableView_AddEvents.dequeueReusableCell(withIdentifier: "Add_ActivitiesEvent_Section1TableViewCell") as! Add_ActivitiesEvent_Section1TableViewCell
            
            cell.txt_Time.placeholder = array_StartTime[indexPath.row]
            cell.txt_Date.placeholder = array_StartDate[indexPath.row]
            
            cell.txt_Date.tag = indexPath.row
            cell.txt_Time.tag = indexPath.row
            
            if indexPath.row == 0{
                cell.txt_Time.placeholder = "Start Time".localized
                cell.txt_Date.placeholder = "Start Date".localized
                
                if self.dic_RegisterValue["Start_Date"] as? String != nil {
                    
                    cell.txt_Date.text = self.dic_RegisterValue["Start_Date"] as? String
                }
                if self.dic_RegisterValue["Start_Time"] as? String != "" {
                    self.str_Time12hrs =  TimeFormat24To12(timeString:(self.dic_RegisterValue["Start_Time"] as? String)!)
                    cell.txt_Time.text = str_Time12hrs
                }
            }else if indexPath.row == 1 {
                cell.txt_Time.placeholder = "End Time".localized
                cell.txt_Date.placeholder = "End Date".localized
                if self.dic_RegisterValue["End_Date"] as? String != nil{
                    cell.txt_Date.text = self.dic_RegisterValue["End_Date"] as? String
                }
                if self.dic_RegisterValue["End_Time"] as? String != ""{
                    
                    self.str_Time12hrs =  TimeFormat24To12(timeString:(self.dic_RegisterValue["End_Time"] as? String)!)
                    cell.txt_Time.text = str_Time12hrs
                }
            }
            
            return cell
            
        }else if indexPath.section == 2 {
            
            
            let cell = self.tableView_AddEvents.dequeueReusableCell(withIdentifier: "Add_ActivitiesEventTableViewCell") as! Add_ActivitiesEventTableViewCell
            cell.view_1.isHidden = true
            cell.view_2.isHidden = true
            cell.view_3.isHidden = false
            cell.txt_RepeatingDay.placeholder = "Repeating Day"
            if self.dic_RegisterValue["Repeating_Day"] as? Int != nil{
                
                let attendees =  self.dic_RegisterValue["Repeating_Day"] as? Int
                if attendees == 1{
                    cell.txt_RepeatingDay.text = "Weekly"
                }else if attendees == 2 {
                    cell.txt_RepeatingDay.text = "Fortnightly"
                }else if attendees == 3 {
                    cell.txt_RepeatingDay.text = "Monthly"
                }
                
            }
            return cell
        }
        
        return UITableViewCell()
    }
    
    
    
}
extension Add_ActivitiesViewController:UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout{
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        
        return 2
    }
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if section == 0{
            if self.addNewActivites != "addNewActivites" {
                return 0
            }else{
                return 1
            }
            
        }else{
            if arrayImages.count > 0{
                if self.arrayImages.count > 6{
                    return 6
                }else{
                    return self.arrayImages.count
                }
            }
            if self.addNewActivites != "addNewActivites" {
                
                return activitesDetails.images?.count ?? 0
            }
        }
        
        return 0
    }
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if indexPath.section == 0{
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Home_AddMoreCollectionViewCell", for: indexPath) as! Home_AddMoreCollectionViewCell
            cell.btn_Addmore.addTarget(self, action: #selector(btn_AddPhotos(sender:)), for: .touchUpInside)
            return cell
        }else{
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "AddimageAnnouncementCollectionViewCell", for: indexPath) as! AddimageAnnouncementCollectionViewCell
            
            
            if activitesDetails != nil{
                cell.btn_Delete.isHidden = true
                if activitesDetails.images?.count != nil{
                    
                    cell.btn_Delete.isHidden = true
                    cell.img_View.sd_setImage(with: URL(string:
                        (self.activitesDetails.images?[indexPath.item])!),placeholderImage: UIImage.init(named: "default_header_iPhone.png"), completed: nil)
                }
            }else{
                cell.img_View.image = arrayImages[indexPath.row]
                
                cell.btn_Delete.isHidden = false
                cell.btn_Delete.tag = indexPath.item
                cell.btn_Delete.addTarget(self, action: #selector(btn_deltePhot(sender:)), for: .touchUpInside)
                
                
            }
            
            return cell
        }
        
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        if indexPath.section == 0{
            return  CGSize(width: 100 , height: 100)
        }else{
            return CGSize(width: collectionView.bounds.size.width/2 - 20 , height: 160)
            
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        insetForSectionAt section: Int) -> UIEdgeInsets{
        
        if section == 0{
            return  UIEdgeInsets( top: 0,  left: 0, bottom: 0,  right: 0)
        }else{
            return  UIEdgeInsets( top: 0,  left: 0, bottom: 0,  right: 0)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        
        return 5.0
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        
        return 5.0
        
    }
}
extension Add_ActivitiesViewController: GMSAutocompleteViewControllerDelegate {
    
    // Handle the user's selection.
    func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {
        
        print("Place name: \(String(describing: place.name))")
        let index = IndexPath(row: self.tag_forAddress, section: 0)
        let cell: Add_ActivitiesEventTableViewCell = self.tableView_AddEvents.cellForRow(at: index) as! Add_ActivitiesEventTableViewCell
        let location:CLLocationCoordinate2D = place.coordinate
        let latitude_str = String(format: "%.6f", location.latitude)
        let longituted_str = String(format: "%.6f", location.longitude)
        self.lat = Double(latitude_str) ?? 123.123
        self.long = Double(longituted_str) ?? 123.123
        print("Place address \(String(describing: place.formattedAddress))")
        
        dic_RegisterValue.updateValue("\(String(describing: place.formattedAddress ?? ""))", forKey: array_AllData[2])
        
        cell.txt_Title.text = "\(String(describing: place.formattedAddress ?? ""))"
        
        
        print("Place address:",place.formattedAddress!)
        
        
        dismiss(animated: true, completion: nil)
    }
    
    func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
        
        // TODO: handle the error.
        print("Error: ", error.localizedDescription)
    }
    
    // User canceled the operation.
    func wasCancelled(_ viewController: GMSAutocompleteViewController) {
        //  addressTextView.resignFirstResponder()
        dismiss(animated: true, completion: nil)
    }
    
    // Turn the network activity indicator on and off again.
    func didRequestAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
    }
    
    func didUpdateAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
    }
    
    //MARK:IBACtions--------------------------------------------
    @IBAction func btn_Back(sender:UIButton){
        
        
        if addNewActivites == "addNewActivites"{
            self.presentingViewController?.presentingViewController?.dismiss(animated: true, completion: nil)
        }else{
            self.dismiss(animated: true, completion: nil)
        }
    }
    @objc func btn_deltePhot(sender:UIButton){
        let buttonTag = sender.tag
        
        print("self.arrayImages:22",self.imageArray.count)
        self.arrayImages.remove(at: buttonTag)
        self.imageData.remove(at: buttonTag)
        
        print("self.arrayImages:33",self.imageArray.count)
        self.collectionView_Images.reloadData()
        
    }
    @objc func btn_AddPhotos(sender:UIButton){
        
        if self.arrayImages.count == 6{
            
            AKAlertController.alert("You can't select more than 6 images")
        }
        
        self.view.endEditing(true)
        let pickerViewController = YMSPhotoPickerViewController.init()
        pickerViewController.numberOfPhotoToSelect = 6
        let customColor = UIColor.init(red: 3.0/255.0, green: 68.0/255.0, blue: 114.0/255.0, alpha: 1.0)
        pickerViewController.theme.titleLabelTextColor = UIColor.white
        pickerViewController.theme.navigationBarBackgroundColor = customColor
        pickerViewController.theme.tintColor = UIColor.white
        pickerViewController.theme.orderTintColor = customColor
        pickerViewController.theme.orderLabelTextColor = UIColor.white
        pickerViewController.theme.cameraVeilColor = customColor
        pickerViewController.theme.cameraIconColor = UIColor.white
        pickerViewController.theme.statusBarStyle = .default
        self.yms_presentCustomAlbumPhotoView(pickerViewController, delegate: self)
    }
    
    @IBAction func photoLibraryTapped(_ sender: Any) {
        self.view.endEditing(true)
        let pickerViewController = YMSPhotoPickerViewController.init()
        pickerViewController.numberOfPhotoToSelect = 6
        let customColor = UIColor.init(red: 3.0/255.0, green: 68.0/255.0, blue: 114.0/255.0, alpha: 1.0)
        pickerViewController.theme.titleLabelTextColor = UIColor.white
        pickerViewController.theme.navigationBarBackgroundColor = customColor
        pickerViewController.theme.tintColor = UIColor.white
        pickerViewController.theme.orderTintColor = customColor
        pickerViewController.theme.orderLabelTextColor = UIColor.white
        pickerViewController.theme.cameraVeilColor = customColor
        pickerViewController.theme.cameraIconColor = UIColor.white
        pickerViewController.theme.statusBarStyle = .default
        self.yms_presentCustomAlbumPhotoView(pickerViewController, delegate: self)
    }
}

extension Add_ActivitiesViewController : YMSPhotoPickerViewControllerDelegate{
    
    // MARK: - YMSPhotoPickerViewControllerDelegate
    
    func photoPickerViewControllerDidReceivePhotoAlbumAccessDenied(_ picker: YMSPhotoPickerViewController!) {
        let alertController = UIAlertController.init(title: "Allow photo album access?", message: "Need your permission to access photo albumbs", preferredStyle: .alert)
        let dismissAction = UIAlertAction.init(title: "Cancel", style: .cancel, handler: nil)
        let settingsAction = UIAlertAction.init(title: "Settings", style: .default) { (action) in
            UIApplication.shared.openURL(URL.init(string: UIApplication.openSettingsURLString)!)
        }
        alertController.addAction(dismissAction)
        alertController.addAction(settingsAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
    func photoPickerViewControllerDidReceiveCameraAccessDenied(_ picker: YMSPhotoPickerViewController!) {
        let alertController = UIAlertController.init(title: "Allow camera album access?", message: "Need your permission to take a photo", preferredStyle: .alert)
        let dismissAction = UIAlertAction.init(title: "Cancel", style: .cancel, handler: nil)
        let settingsAction = UIAlertAction.init(title: "Settings", style: .default) { (action) in
            UIApplication.shared.openURL(URL.init(string: UIApplication.openSettingsURLString)!)
        }
        alertController.addAction(dismissAction)
        alertController.addAction(settingsAction)
        picker.present(alertController, animated: true, completion: nil)
    }
    func photoPickerViewController(_ picker: YMSPhotoPickerViewController!, didFinishPicking image: UIImage!) {
        picker.dismiss(animated: true) {
        }
    }
    
    func photoPickerViewController(_ picker: YMSPhotoPickerViewController!, didFinishPickingImages photoAssets: [PHAsset]!) {
        
        picker.dismiss(animated: true) {
            let imageManager = PHImageManager.init()
            let options = PHImageRequestOptions.init()
            options.deliveryMode = .highQualityFormat
            options.resizeMode = .none
            options.isSynchronous = true
            
            
            if photoAssets.count + self.arrayImages.count > 6{
                
                AKAlertController.alert("You can't select more than 6 images")
            }
                
            else{
                for asset: PHAsset in photoAssets
                {
                    let targetSize = CGSize(width: UIScreen.main.bounds.size.width, height: UIScreen.main.bounds.size.width)
                    imageManager.requestImage(for: asset, targetSize: targetSize, contentMode: .default, options: options, resultHandler: { (image, info) in
                      
                               self.arrayImages.append(image!)
                            self.collectionView_Images.isHidden = false
                                                  self.imageData.append(image!.jpegData(compressionQuality: 0.7)!)
                    
                    
                      
                        // self.imageData.append(image!.pngData()!)
                        //                    self.imagesCollectionviewTopConstraint.constant = 0
                    })
                }
            }
            if self.arrayImages.count > 0{
                
                self.collectionView_Images.isHidden = false
                self.view_UploadPhoto.isHidden = true
            }else{
                self.collectionView_Images.isHidden = true
                self.view_UploadPhoto.isHidden = false
            }
            self.collectionView_Images.reloadData()
        }
    }
    
    
}

extension Add_ActivitiesViewController: MHDescDelegate{
    func getDescription(desc: String) {
        self.dic_RegisterValue["Description"] = desc
        self.tableView_AddEvents.reloadRows(at: [IndexPath(row: 1, section: 0)], with: UITableView.RowAnimation.none)
        
    }
    
    func TimeFormat24To12(timeString: String) -> String {
        
        let dateAsString = timeString
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "HH:mm"
        
        let date = dateFormatter.date(from: dateAsString)
        dateFormatter.dateFormat = "h:mm a"
        let Date12 = dateFormatter.string(from: date!)
        return Date12
    }
    
}

