//
//  AddJummaTableViewCell.swift
//  Muslim Hub
//
//  Created by Himanshu Goyal on 27/06/19.
//  Copyright © 2019 OnGraph. All rights reserved.
//

import UIKit

protocol AddIqamahOthersCellDelegate{
    func btnAddMoreIqamahCell(indexPath: IndexPath)
    func btnDeleteTapped(indexPath: IndexPath)
    func btnLocationTapped(indexPath: IndexPath)
    func saveDataModel(indexPath: IndexPath, jummaModel: JummaDataModel, specialPrayerModel: SpecialPrayerDataModel)
    func btnSaveTapped(indexPath: IndexPath)
}

extension AddIqamahOthersCellDelegate{
    func btnAddMoreIqamahCell(indexPath: IndexPath){}
    func btnDeleteTapped(indexPath: IndexPath){}
    func btnLocationTapped(indexPath: IndexPath){}
    func saveDataModel(indexPath: IndexPath, jummaModel: JummaDataModel, specialPrayerModel: SpecialPrayerDataModel){}
    func btnSaveTapped(indexPath: IndexPath){}
}

class AddJummaTableViewCell: UITableViewCell {

    @IBOutlet weak var txtDesc          : UITextView!
    @IBOutlet weak var btnSave          : UIButton!
    @IBOutlet weak var txtLocation      : UITextField!
    @IBOutlet weak var btnTime          : UIButton!
    @IBOutlet weak var btnJummaType     : UIButton!
    @IBOutlet weak var stackViewHeight  : NSLayoutConstraint!
    @IBOutlet weak var btnAddMore       : UIButton!
    @IBOutlet weak var btnCancel        : UIButton!
    var delegate                        : AddIqamahOthersCellDelegate!
    var indexPath                       : IndexPath!
    var viewController                  : UIViewController!
    var model                           : JummaDataModel!
    let array_SelectJummaType                               = ["Jumma 1",
                                                               "Jumma 2",
                                                               "Jumma 3",
                                                               "Jumma 4",
                                                               "Jumma 5",
                                                               "Jumma 6",
                                                               "Jumma 7",
                                                               "Jumma 8",
                                                               "Jumma 9",
                                                               "Jumma 10"]
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setUpCell(indexPath: IndexPath, viewC: UIViewController, model: JummaDataModel){
        self.indexPath = indexPath
        self.viewController = viewC
        self.model = model
           self.txtDesc.delegate = self
        if self.model != nil {
            //Location
            if let location = self.model.location{
                self.txtLocation.text = location
            }else{
                self.txtLocation.text = ""
            }
            
            
            //Jumma Type
            if let jummaType = self.model.jummaType{
                self.btnJummaType.setTitle(jummaType, for: .normal)
                self.btnJummaType.setTitleColor(UIColor.black, for: .normal)
            }else{
                self.btnJummaType.setTitle("Select Jumma Type", for: .normal)
                self.btnJummaType.setTitleColor(UIColor.lightGray, for: .normal)
            }

            //Time
            if self.model.time != nil{
               
                    let timedateFormatter = DateFormatter()
                    timedateFormatter.dateFormat = "HH:mm"
                    let starttime = timedateFormatter.date(from: model.timeIn24 ?? "")
                    
                    let timedateFormatter1 = DateFormatter()
                    timedateFormatter1.dateFormat = "hh:mm a"
                    let startTime1 = timedateFormatter1.string(from: starttime!)
                    
                    //Time
                    self.btnTime.setTitle(startTime1, for: .normal)
                        self.btnTime.setTitleColor(UIColor.black, for: .normal)
                   

            }else{
                self.btnTime.setTitle("Time", for: .normal)
                self.btnTime.setTitleColor(UIColor.lightGray, for: .normal)
            }
       
            //Desc
            if let desc = self.model.desc{
                self.txtDesc.text = desc
            }else{
                self.txtDesc.text = "Notes"
            }
            var isEdit: Bool = false
            
            if let check = self.model.isEdit{
                isEdit = check
            }else{
                isEdit = false
            }
            
            var isUpdated: Bool = false
            
            if let check = self.model.isUpdated{
                
                isUpdated = check
            }else{
                isUpdated = false
            }

            if isUpdated{
               
                self.btnSave.isHidden = true
                self.btnCancel.isHidden = true
                self.stackViewHeight.constant = 0
                self.btnJummaType.isUserInteractionEnabled = false
                self.btnTime.isUserInteractionEnabled = false
                self.txtLocation.isUserInteractionEnabled = false
                self.txtDesc.isUserInteractionEnabled = false
            }else{
                self.btnSave.isHidden = false
                self.btnCancel.isHidden = false
                self.stackViewHeight.constant = 45
                self.btnJummaType.isUserInteractionEnabled = true
                self.btnTime.isUserInteractionEnabled = true
                self.txtLocation.isUserInteractionEnabled = true
                self.txtDesc.isUserInteractionEnabled = true
            }
            if isEdit{
                self.btnCancel.setTitle("Delete", for: .normal)
                self.btnAddMore.isHidden = true
                
                                if isEdit == true {
                                    self.btnSave.isHidden = false
                                    self.btnCancel.isHidden = false
                                }
            }else{
                self.btnCancel.setTitle("Clear", for: .normal)
                self.btnAddMore.isHidden = false
            }
        }else{
            
            
            self.btnSave.isHidden = false
            self.btnCancel.isHidden = false
            self.stackViewHeight.constant = 45
            self.btnJummaType.isUserInteractionEnabled = true
            self.btnTime.isUserInteractionEnabled = true
            self.txtLocation.isUserInteractionEnabled = true
            self.txtDesc.isUserInteractionEnabled = true
            self.btnJummaType.setTitle("Select Jumma Type", for: .normal)
            self.btnTime.setTitle("Time", for: .normal)
            self.txtLocation.text = ""
            self.txtDesc.text = ""
        }
        
    }
    
    private func getValidate() -> (Bool, String){
        var error : (Bool, String) = (false, "")
        if self.model != nil{
            if self.model.jummaType == nil || self.model.jummaType == ""{
                error = (false, "Please Select Jumma Type")
            }else if self.model.time == nil || self.model.time == ""{
                error = (false, "Please Select Time")
            }else if self.model.location == nil || self.model.location == ""{
                error = (false, "Please Select Location")
            }
//            else if self.model.desc == nil || self.model.desc == ""{
//                error = (false, "Please enter Description")
//            }
            else{
                error = (true, "Success")
            }
        }else{
            error = (false, "Please enter all details")
        }
        return error
    }
    
    func callSaveModel(){
        self.delegate.saveDataModel(indexPath: self.indexPath, jummaModel: self.model, specialPrayerModel: SpecialPrayerDataModel())
    }
    
    @IBAction func btnAddMoreTapped(_ sender: Any) {
        self.delegate.btnAddMoreIqamahCell(indexPath: self.indexPath)
    }
    
    @IBAction func btnCancelTapped(_ sender: Any) {
        self.delegate.btnDeleteTapped(indexPath: self.indexPath)
    }
    
    @IBAction func btnJummaTypeTapped(_ sender: Any) {
        PickerViewController.openPickerView(viewC: self.viewController, dataArray: self.array_SelectJummaType) { (value, index) in
            self.btnJummaType.setTitleColor(UIColor.black, for: .normal)
            self.btnJummaType.setTitle(value, for: .normal)
            self.model.jummaType = value
            if value == "Jumma 1"{
               self.model.jummaTypeId = 1
            }else if value == "Jumma 2"{
                self.model.jummaTypeId = 2
            }else if value == "Jumma 3"{
                self.model.jummaTypeId = 3
            }else if value == "Jumma 4"{
                self.model.jummaTypeId = 4
            }else if value == "Jumma 5"{
                self.model.jummaTypeId = 5
            }else if value == "Jumma 6"{
                self.model.jummaTypeId = 6
            }else if value == "Jumma 7"{
                self.model.jummaTypeId = 7
            }else if value == "Jumma 8"{
                self.model.jummaTypeId = 8
            }else if value == "Jumma 9"{
                self.model.jummaTypeId = 9
            }else if value == "Jumma 10"{
                self.model.jummaTypeId = 10
            }
           
            self.callSaveModel()
        }
    }
    
    @IBAction func btnTimeTapped(_ sender: Any) {
        PickerViewController.openDatePickerView(viewC: self.viewController, currentDate: nil, minimumDate: nil, pickerMode: .time) { (value) in
            let timedateFormatter = DateFormatter()
            timedateFormatter.dateFormat = "h:mm a"
            let time = timedateFormatter.string(from: value)
            self.btnTime.setTitleColor(UIColor.black, for: .normal)
            self.btnTime.setTitle(time, for: .normal)
            
            let timedateFormatter1 = DateFormatter()
            timedateFormatter1.dateFormat = "HH:mm"
            let time1 = timedateFormatter1.string(from: value)
            
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "dd-MM-yyyy"
            let date = dateFormatter.string(from: Date())
                
            self.model.date = date
            self.model.timeIn24 = time1
            self.model.time = time
            self.callSaveModel()
        }
    }
    
    @IBAction func btnSaveTapped(_ sender: Any) {
        let (isValidate, errorMessage) = self.getValidate()
        if isValidate{
            self.delegate.btnSaveTapped(indexPath: self.indexPath)
        }else{
            self.viewController.showSingleAlertMessage(message: "Warning", subMsg: errorMessage, sender: self.viewController) { (_) in
            }
        }
    }

}

extension AddJummaTableViewCell: MHDescDelegate{
    func getDescription(desc: String) {
        self.txtDesc.text = desc
        self.model.desc = desc
        self.callSaveModel()
    }
}

extension AddJummaTableViewCell: UITextFieldDelegate,UITextViewDelegate{
    
    func textViewDidBeginEditing(_ textView: UITextView) {
         if textView == self.txtDesc{
        let vc = viewController.storyboard?.instantiateViewController(withIdentifier: "MHDescViewController") as! MHDescViewController
            vc.delegate = self
            vc.textTitle = "Add Notes"
            if self.txtDesc.text == "Notes"{
                vc.textDesc = ""
            }else{
                vc.textDesc = self.txtDesc.text ?? ""
            }
            
             vc.modalPresentationStyle = .fullScreen
            self.viewController.present(vc, animated: false, completion: nil)
        }
    }
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == self.txtLocation{
            self.delegate.btnLocationTapped(indexPath: self.indexPath)
        }
    }
}
