//
//  AddIqamahTableViewCell.swift
//  Muslim Hub
//
//  Created by Himanshu Goyal on 27/06/19.
//  Copyright © 2019 OnGraph. All rights reserved.
//

import UIKit

protocol AddIqamahCellDelegate{
    func reloadIqamagCell(indexPath: IndexPath, height: CGFloat)
    func btnChooseTypeIqamagCell(indexPath: IndexPath, value: String, isHeightUpdateNeeded: Bool, cellHeight: CGFloat)
    func saveDataIqamaModel(indexPath: IndexPath, iQAMAModel: IqamahDataModel)
    func btn_IqamaSaveTapped(indexPath: IndexPath,iqamaType:String)
}

class AddIqamahTableViewCell: UITableViewCell, UITextFieldDelegate,UITextViewDelegate {

    //MARK: - Properties
    @IBOutlet weak var btnCancelAuto            : UIButton!
    @IBOutlet weak var btnCancelMannual         : UIButton!
    @IBOutlet weak var btnSaveMannual           : UIButton!
    @IBOutlet weak var txtNotesMannual          : UITextView!
    @IBOutlet weak var btnTimeMannual           : UIButton!
    @IBOutlet weak var btnSaveAuto              : UIButton!
    @IBOutlet weak var txtNotes                 : UITextView!
    @IBOutlet weak var btnOffsetTimeAuto        : UIButton!
    @IBOutlet weak var btnCalculationMethodAuto : UIButton!
    @IBOutlet weak var viewTypeMannual          : UIView!
    @IBOutlet weak var viewTypeAuto             : UIView!
    @IBOutlet weak var viewIqama                : UIView!
    @IBOutlet weak var lblTitle                 : UILabel!
    @IBOutlet weak var btnChoosePrayerType      : UIButton!
    @IBOutlet weak var btnChooseAutoOrMannual   : UIButton!
    @IBOutlet weak var btnNotPrayed             : UIButton!
     @IBOutlet weak var lbl_NotPrayed           : UILabel!
  

    @IBOutlet weak var cellViewHeight           : NSLayoutConstraint!
    var delegate                                : AddIqamahCellDelegate!
    var indexPath                               : IndexPath!
    var viewController                          : UIViewController!
    var array_SelectIqamah                      = ["Auto",
                                                   "Manual"]
    var array_SelectPrayerType                  = ["Fajr",
                                                   "Zuhr",
                                                   "Asr",
                                                   "Maghrib",
                                                   "Isha"]

    var array_CalculationMethods                = [
        "University of Islamic Sciences, Karachi",
        "Islamic Society of North America",
        "Muslim World League",
        "Umm Al-Qura University, Makkah",
        "Egyptian General Authority of Survey",
        "Institute of Geophysics, University of Tehran",
        "Gulf Region",
        "Kuwait",
        "Qatar",
        "Majlis Ugama Islam Singapura, Singapore",
        "Union Organization islamic de France",
        "Diyanet İşleri Başkanlığı, Turkey",
        "Spiritual Administration of Muslims of Russia", "Pre uploaded Iqamah"]
   
    var model                                   : IqamahDataModel!
    var isAutoNote                              : Bool                      = false
    var notPrayed = String()
    var calculationMethod = Int()
    override func awakeFromNib() {
        super.awakeFromNib()
        self.txtNotes.delegate = self
        self.txtNotesMannual.delegate = self
        lblTitle.text = "Iqamah".localized
        if txtNotes.text == ""{
            txtNotes.text = "Notes".localized
        }
        if txtNotesMannual.text == ""{
          txtNotes.text = "Notes".localized
        }
        btnSaveMannual.setTitle("Save".localized, for: .normal)
        lbl_NotPrayed.text = "Not prayed".localized
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
 
    func setUpCell(indexPath: IndexPath, viewC: UIViewController, model: IqamahDataModel,numberOfRows:[String],indexRow:Int){
  
        self.array_SelectPrayerType = numberOfRows
        self.btnNotPrayed.isHidden = true
        self.lbl_NotPrayed.isHidden = true
        if model.notes != ""{
            txtNotes.text = "Notes".localized
        }
        if model.prayerType == nil{
            self.viewTypeAuto.isHidden = true
            self.viewTypeMannual.isHidden = true
            self.cellViewHeight.constant = 130
            self.layoutIfNeeded()
           self.layoutSubviews()
            
            self.btnChooseAutoOrMannual.setTitle("Auto/Manual".localized, for: .normal)
            self.btnChooseAutoOrMannual.setTitleColor(UIColor.lightGray, for: .normal)
        }else{
            if model.isEdit == false || model.isEdit == nil{
                if model.isUpdated == true{
                    self.btnSaveAuto.isHidden = true
                    self.btnSaveMannual.isHidden = true
                    self.btnCancelAuto.isHidden = true
                    self.btnCancelMannual.isHidden = true
                    
                    self.isUserInteractionEnabled = false
                    
                }else{
                    self.btnSaveAuto.isHidden = false
                    self.btnSaveMannual.isHidden = false
                    self.btnCancelAuto.isHidden = false
                    self.btnCancelMannual.isHidden = false
                    
                    self.isUserInteractionEnabled = true
                }
                
            }else{
                self.isUserInteractionEnabled = true
            }
            
            self.cellViewHeight.constant = model.cellHeight!
      
            self.btnChooseAutoOrMannual.setTitle(model.prayerType ?? "", for: .normal)
            self.btnChooseAutoOrMannual.setTitleColor(UIColor(named: "grey_TilteColor"), for: .normal)
            if model.prayerType!.lowercased() == "auto"{
                self.viewTypeAuto.isHidden = false
                self.viewTypeMannual.isHidden = true
                if model.notes == ""{
                    self.txtNotes.text = "Notes".localized
                    self.txtNotes.textColor = UIColor.lightGray
                }else{
                     self.txtNotes.textColor = UIColor.black
                      self.txtNotes.text = model.notes ?? "Notes".localized
                }
              
                self.btnNotPrayed.isHidden = false
                self.lbl_NotPrayed.isHidden = false
                self.layoutIfNeeded()
                self.layoutSubviews()
                  if  model.offestTime != nil{
                    let offsetTime = "\(model.offestTime!) mins"
                    self.btnOffsetTimeAuto.setTitle(offsetTime , for: .normal)
                     self.btnOffsetTimeAuto.setTitleColor(UIColor.black, for: .normal)

                  }else{
                   
                    self.btnOffsetTimeAuto.setTitle("Offset time".localized , for: .normal)
                    self.btnOffsetTimeAuto.setTitleColor(UIColor.lightGray, for: .normal)
                }
                self.notPrayed =  model.isPrayed ?? "Y"
                if self.notPrayed == "N"{
                    btnNotPrayed.isSelected = true
                    btnNotPrayed.setBackgroundImage(#imageLiteral(resourceName: "checkbox_iPhone"), for: .normal)
                }else{
                    
                    btnNotPrayed.isSelected = false
                    btnNotPrayed.setBackgroundImage(#imageLiteral(resourceName: "uncheckbox_iPhone"), for: .normal)
                }
            }else{
                self.btnNotPrayed.isHidden = false
                self.lbl_NotPrayed.isHidden = false
                self.viewTypeAuto.isHidden = true
                self.viewTypeMannual.isHidden = false
                //self.txtNotesMannual.text = model.notes ?? "Notes"
            if model.notes == ""{
                              self.txtNotesMannual.text = "Notes".localized
                            self.txtNotesMannual.textColor = UIColor.lightGray
                        }else{
                             self.txtNotesMannual.textColor = UIColor.black
                              self.txtNotesMannual.text = model.notes ?? "Notes".localized
                        }
                self.layoutIfNeeded()
                self.layoutSubviews()
                if model.time != nil && model.time != "00:00"{
                    let timedateFormatter = DateFormatter()
                    timedateFormatter.dateFormat = "HH:mm"
                    let starttime = timedateFormatter.date(from: model.timeIn24 ?? "")
                    
                    let timedateFormatter1 = DateFormatter()
                    timedateFormatter1.dateFormat = "hh:mm a"
                    let startTime1 = timedateFormatter1.string(from: starttime!)
                    
                    //Time
                    if startTime1 != ""{
                        self.btnTimeMannual.setTitle(startTime1, for: .normal)
                        self.btnTimeMannual.setTitleColor(UIColor.black, for: .normal)
                        
                    }else{
                        self.btnTimeMannual.setTitle("Time".localized, for: .normal)
                        self.btnTimeMannual.setTitleColor(UIColor.lightGray, for: .normal)
                    }
                }else{
                    self.btnTimeMannual.setTitle("Time".localized, for: .normal)
                    self.btnTimeMannual.setTitleColor(UIColor.lightGray, for: .normal)
                }
             
               
                self.notPrayed =  model.isPrayed ?? "Y"
                if self.notPrayed == "N"{
                      btnNotPrayed.isSelected = true
                     btnNotPrayed.setBackgroundImage(#imageLiteral(resourceName: "checkbox_iPhone"), for: .normal)
                }else{
                  
                     btnNotPrayed.isSelected = false
                    btnNotPrayed.setBackgroundImage(#imageLiteral(resourceName: "uncheckbox_iPhone"), for: .normal)
                }
            }
            if model.isEdit == true{
                self.btnCancelMannual.setTitle("Delete".localized, for: .normal)
                
                self.btnCancelAuto.setTitle("Delete".localized, for: .normal)
            }else{
                self.btnCancelMannual.setTitle("Clear".localized, for: .normal)
                self.btnCancelAuto.setTitle("Clear".localized, for: .normal)
            }
       }
     
        if model.calculationMethod != nil{
           
             self.btnCalculationMethodAuto.setTitleColor(UIColor.black, for: .normal)
            if model.calculationMethod == 0 {
               self.btnCalculationMethodAuto.setTitle("Shia Ithna-Ansari", for: .normal)
            }else if model.calculationMethod == 1 {
                self.btnCalculationMethodAuto.setTitle("University of Islamic Sciences, Karachi", for: .normal)
            }else if model.calculationMethod == 2{
                self.btnCalculationMethodAuto.setTitle("Islamic Society of North America", for: .normal)
            }else if model.calculationMethod == 3{
                 self.btnCalculationMethodAuto.setTitle( "Muslim World League", for: .normal)
            }else if model.calculationMethod == 4{
                  self.btnCalculationMethodAuto.setTitle("Umm Al-Qura University, Makkah", for: .normal)
            }else if model.calculationMethod == 5{
                 self.btnCalculationMethodAuto.setTitle("Egyptian General Authority of Survey", for: .normal)
            }else if model.calculationMethod == 7 {
              self.btnCalculationMethodAuto.setTitle("Institute of Geophysics, University of Tehran", for: .normal)
            }else if model.calculationMethod == 8 {
            self.btnCalculationMethodAuto.setTitle("Gulf Region", for: .normal)
            }else if model.calculationMethod == 9 {
            self.btnCalculationMethodAuto.setTitle("Kuwait", for: .normal)
            }else if model.calculationMethod == 10 {
                 self.btnCalculationMethodAuto.setTitle("Qatar", for: .normal)
            }else if model.calculationMethod == 11{
                self.btnCalculationMethodAuto.setTitle("Majlis Ugama Islam Singapura, Singapore", for: .normal)
            }else if model.calculationMethod == 12{
                 self.btnCalculationMethodAuto.setTitle("Union Organization islamic de France", for: .normal)
            }else if model.calculationMethod == 13{
                self.btnCalculationMethodAuto.setTitle("Diyanet İşleri Başkanlığı, Turkey", for: .normal)
            }else if model.calculationMethod == 14{
                self.btnCalculationMethodAuto.setTitle("Spiritual Administration of Muslims of Russia", for: .normal)
            }else if model.calculationMethod == 15{
                self.btnCalculationMethodAuto.setTitle("Pre uploaded Iqamah", for: .normal)
            }
            else{
               self.btnCalculationMethodAuto.setTitle("Qatar", for: .normal)
            }
           
        }else{
             self.btnCalculationMethodAuto.setTitleColor(UIColor.lightGray, for: .normal)
            self.btnCalculationMethodAuto.setTitle("Calculation method".localized, for: .normal)
        }
      
        if model.prayerName != nil {
            if model.prayerName != "" {
          self.btnChoosePrayerType.setTitle(model.prayerName ?? "", for: .normal)
            }
       }
            else{
            self.btnChoosePrayerType.setTitle(array_SelectPrayerType[indexRow] , for: .normal)

        }
//
   
        self.indexPath = indexPath
        self.viewController = viewC
        self.model = model
        
    }
    func callSaveModel(){
        
        self.delegate.saveDataIqamaModel(indexPath: self.indexPath, iQAMAModel: self.model)
    }
   
    //Mark:UITextView Delegate and Datasources-------------------------------
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView == self.txtNotes{
            self.isAutoNote = true
            let vc = viewController.storyboard?.instantiateViewController(withIdentifier: "MHDescViewController") as! MHDescViewController
            vc.delegate = self
            vc.textTitle = "Add Notes"
            if self.txtNotes.text == "Notes".localized{
                vc.textDesc = ""
            }else{
                vc.textDesc = self.txtNotes.text ?? ""
            }
            vc.modalPresentationStyle = .fullScreen
            self.viewController.present(vc, animated: false, completion: nil)
        }else if textView == self.txtNotesMannual{
            self.isAutoNote = false
            let vc = viewController.storyboard?.instantiateViewController(withIdentifier: "MHDescViewController") as! MHDescViewController
            vc.delegate = self
            vc.textTitle = "Add Notes"
            if self.txtNotesMannual.text == "Notes".localized{
                vc.textDesc = ""
            }else{
                vc.textDesc = self.txtNotesMannual.text ?? ""
            }
             vc.modalPresentationStyle = .fullScreen
            self.viewController.present(vc, animated: false, completion: nil)
        }
        
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
       
    }
    
    @IBAction func btn_NotPrayed(sender:UIButton){
        if btnNotPrayed.isSelected{
            btnNotPrayed.isSelected = false
            //  btn_Privateorg.backgroundColor = UIColor.white
            self.notPrayed = "Y"
            self.model.isPrayed = self.notPrayed
            btnNotPrayed.setBackgroundImage(#imageLiteral(resourceName: "uncheckbox_iPhone"), for: .normal)
        }else{
            btnNotPrayed.isSelected = true
            self.notPrayed = "N"
             self.model.isPrayed = self.notPrayed
            // btn_Privateorg.backgroundColor = UIColor.init(hex: 0x0BC9E4)
            btnNotPrayed.setBackgroundImage(#imageLiteral(resourceName: "checkbox_iPhone"), for: .normal)
        }
        self.callSaveModel()
    }
    
    @IBAction func btnAutoOrMannualTapped(_ sender: Any) {
       
        let title = btnChooseAutoOrMannual.titleLabel!.text!
        
        
        
        PickerViewController.openPickerView(viewC: self.viewController, dataArray: self.array_SelectIqamah) { (value, index) in
            self.btnChooseAutoOrMannual.setTitle("\(value)", for: .normal)

            self.btnChooseAutoOrMannual.setTitleColor(UIColor.black, for: .normal)
            
            if title != value && title != "Auto/Manual".localized{
                self.model.calc_type = ""
                self.model.time = nil
                self.model.timeIn24 = nil
                self.model.offestTime = nil
                self.model.notes = ""
                self.model.calculationMethod = nil
                
                self.callSaveModel()
                self.setUpCell(indexPath: self.indexPath, viewC: self.viewController, model: self.model, numberOfRows: self.array_SelectPrayerType, indexRow: self.indexPath.row)
            }
            
            if self.cellViewHeight.constant == 130{
                if self.model.prayerType == nil{
                    self.btnNotPrayed.isHidden = false
                    self.lbl_NotPrayed.isHidden = false
                    if value.lowercased() == "auto"{
                       
                        self.delegate.btnChooseTypeIqamagCell(indexPath: self.indexPath, value: "\(value)", isHeightUpdateNeeded: true, cellHeight: 330)
                       
                    }else{
                        self.delegate.btnChooseTypeIqamagCell(indexPath: self.indexPath, value: "\(value)", isHeightUpdateNeeded: true, cellHeight: 290)
                        
                    }
                }else{
                    self.btnNotPrayed.isHidden = false
                    self.lbl_NotPrayed.isHidden = false
                    if self.model.prayerType != value{
                        if value.lowercased() == "auto"{
                            
                            self.delegate.btnChooseTypeIqamagCell(indexPath: self.indexPath, value: "\(value)", isHeightUpdateNeeded: true, cellHeight: 330)
                        }else{
                            self.delegate.btnChooseTypeIqamagCell(indexPath: self.indexPath, value: "\(value)", isHeightUpdateNeeded: true, cellHeight: 290)
                        }
                    }else{
                        
                    }
                }
            }else{
                if self.model.prayerType == nil{
                    self.btnNotPrayed.isHidden = false
                    self.lbl_NotPrayed.isHidden = false
                    if value.lowercased() == "auto"{
                        self.delegate.btnChooseTypeIqamagCell(indexPath: self.indexPath, value: "\(value)", isHeightUpdateNeeded: true, cellHeight: 330)
                    }else{
                        self.delegate.btnChooseTypeIqamagCell(indexPath: self.indexPath, value: "\(value)", isHeightUpdateNeeded: true, cellHeight: 290)
                    }
                }else{
                    if self.model.prayerType != value{
                        self.btnNotPrayed.isHidden = false
                        self.lbl_NotPrayed.isHidden = false
                        if value.lowercased() == "auto"{
                            self.delegate.btnChooseTypeIqamagCell(indexPath: self.indexPath, value: "\(value)", isHeightUpdateNeeded: true, cellHeight: 330)
                        }else{
                            self.delegate.btnChooseTypeIqamagCell(indexPath: self.indexPath, value: "\(value)", isHeightUpdateNeeded: true, cellHeight: 290)
                        }
                    }else{
                        
                    }
                }
            }
        }
    }
    @IBAction func btnChoosePrayerTypeTapped(_ sender: Any) {
        
    }
    @IBAction func btnOffsetTimeAutoTapped(_ sender: Any) {
        var arrayOffset : [String] = []
        for i in 0...60{
            if i == 1 || i == 0{
                arrayOffset.append("\(i) min")
            }else{
                arrayOffset.append("\(i) mins")
            }
        }
        PickerViewController.openPickerView(viewC: self.viewController, dataArray: arrayOffset) { (value, index) in
            self.btnOffsetTimeAuto.setTitle(value, for: .normal)
            self.btnOffsetTimeAuto.setTitleColor(UIColor.black, for: .normal)
//            if index == 0{
              self.model.offestTime = index!
//            }else{
//               self.model.offestTime = index! + 1
//            }
            self.callSaveModel()
        }
    }
    @IBAction func btnCalculationMethodAutoTapped(_ sender: Any) {
        
    let preuploaded = UserDefaults.standard.object(forKey: "PreUploaded") as? Bool
           
        if preuploaded == false{
            
           self.array_CalculationMethods = self.array_CalculationMethods.dropLast()
            PickerViewController.openPickerView(viewC: self.viewController, dataArray: self.array_CalculationMethods) { (value, index) in
                self.btnCalculationMethodAuto.setTitle(value, for: .normal)
                self.btnCalculationMethodAuto.setTitleColor(UIColor.black, for: .normal)
                
                
                if value == "Shia Ithna-Ansari"{
                    self.calculationMethod = 0
                }else if value == "University of Islamic Sciences, Karachi"{
                    self.calculationMethod = 1
                }else if value == "Islamic Society of North America"{
                    self.calculationMethod = 2
                }else if value == "Muslim World League"{
                    self.calculationMethod = 3
                }else if value == "Umm Al-Qura University, Makkah"{
                    self.calculationMethod = 4
                }else if value == "Egyptian General Authority of Survey"{
                    self.calculationMethod = 5
                }else if value == "Institute of Geophysics, University of Tehran"{
                    self.calculationMethod = 7
                }else if value == "Gulf Region"{
                    self.calculationMethod = 8
                }else if value == "Kuwait"{
                    self.calculationMethod = 9
                }else if value == "Qatar"{
                    self.calculationMethod = 10
                }else if value == "Majlis Ugama Islam Singapura, Singapore"{
                    self.calculationMethod = 11
                }else if value == "Union Organization islamic de France"{
                    self.calculationMethod = 12
                }else if value == "Diyanet İşleri Başkanlığı, Turkey"{
                    self.calculationMethod = 13
                }else if value == "Spiritual Administration of Muslims of Russia"{
                    self.calculationMethod = 14
                }else if value == "Pre uploaded Iqamah"{
                    self.calculationMethod = 15
                }else{
                    self.calculationMethod = 0
                }
                self.model.calculationMethod = self.calculationMethod
                self.callSaveModel()
        }
        }else{
            
        
        PickerViewController.openPickerView(viewC: self.viewController, dataArray: self.array_CalculationMethods) { (value, index) in
            self.btnCalculationMethodAuto.setTitle(value, for: .normal)
            self.btnCalculationMethodAuto.setTitleColor(UIColor.black, for: .normal)

            
            if value == "Shia Ithna-Ansari"{
               self.calculationMethod = 0
            }else if value == "University of Islamic Sciences, Karachi"{
                 self.calculationMethod = 1
            }else if value == "Islamic Society of North America"{
                self.calculationMethod = 2
            }else if value == "Muslim World League"{
                self.calculationMethod = 3
            }else if value == "Umm Al-Qura University, Makkah"{
                self.calculationMethod = 4
            }else if value == "Egyptian General Authority of Survey"{
                self.calculationMethod = 5
            }else if value == "Institute of Geophysics, University of Tehran"{
                self.calculationMethod = 7
            }else if value == "Gulf Region"{
                self.calculationMethod = 8
            }else if value == "Kuwait"{
                self.calculationMethod = 9
            }else if value == "Qatar"{
                self.calculationMethod = 10
            }else if value == "Majlis Ugama Islam Singapura, Singapore"{
                self.calculationMethod = 11
            }else if value == "Union Organization islamic de France"{
                self.calculationMethod = 12
            }else if value == "Diyanet İşleri Başkanlığı, Turkey"{
                self.calculationMethod = 13
            }else if value == "Spiritual Administration of Muslims of Russia"{
                self.calculationMethod = 14
            }else if value == "Pre uploaded Iqamah"{
                self.calculationMethod = 15
            }else{
                self.calculationMethod = 0
            }
            self.model.calculationMethod = self.calculationMethod
            self.callSaveModel()
        }
        }
    }
    
    @IBAction func btnSaveAutoTapped(_ sender: Any) {
        self.delegate.btn_IqamaSaveTapped(indexPath: self.indexPath,iqamaType: "Auto")
    }
    
    @IBAction func btnTimeMannualTapped(_ sender: Any) {
        PickerViewController.openDatePickerView(viewC: self.viewController, currentDate: nil, minimumDate: nil, pickerMode: .time) { (value) in
            let timedateFormatter = DateFormatter()
            timedateFormatter.dateFormat = "h:mm a"
            let time = timedateFormatter.string(from: value)
            self.btnTimeMannual.setTitleColor(UIColor.black, for: .normal)
            self.btnTimeMannual.setTitle(time, for: .normal)
            
            let timedateFormatter1 = DateFormatter()
            timedateFormatter1.dateFormat = "HH:mm"
            let time1 = timedateFormatter1.string(from: value)
            
            self.model.timeIn24 = time1
            self.model.time = time
            self.callSaveModel()
        }
    }
    
    @IBAction func btnSaveMannualTapped(_ sender: Any) {
        self.delegate.btn_IqamaSaveTapped(indexPath: self.indexPath,iqamaType: "Manual")
    }
    
  
}


extension AddIqamahTableViewCell: MHDescDelegate{
    func getDescription(desc: String) {
        if self.isAutoNote{
            self.txtNotes.text = desc
            self.model.notes = desc
            self.callSaveModel()
        }else{
            self.txtNotesMannual.text = desc
            self.model.notes = desc
            self.callSaveModel()
        }
    }
}
