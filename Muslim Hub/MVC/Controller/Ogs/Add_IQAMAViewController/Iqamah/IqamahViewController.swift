//
//  AddIqamahViewController.swift
//  Muslim Hub
//
//  Created by Ongraph on 24/06/19.
//  Copyright © 2019 OnGraph. All rights reserved.
//

import UIKit

class IqamahViewController: UIViewController {
    
    @IBOutlet weak var bottomViewHeight     : NSLayoutConstraint!
    @IBOutlet weak var bottomButtonView     : UIView!
    @IBOutlet weak var tableView_AddIqamah  : UITableView!
    @IBOutlet weak var btnAddJumma          : UIButton!
    @IBOutlet weak var btnAddSpecialPrayer  : UIButton!
    @IBOutlet weak var lbl_Title  : UILabel!
    var editIqamah:String!
    var orgId                                               = Int()
    var numberOfIqamahRows                  : Int           = 5
    var numberOfJummaRows                   : Int           = 0
    var numberOfSpecialPrayerRows           : Int           = 0
    var addNewIqamah                        : String!
    var dic_RegisterValue                                   = [String:Any]()
    var array_IqamaCellHeight               : [CGFloat]                 = []
    var array_IqamahModel                   : [IqamahDataModel]         = []
    var array_JummaModel                    : [JummaDataModel]          = []
    var array_SpecialPrayerModel            : [SpecialPrayerDataModel]  = []
    var selectedLocationButton              : IndexPath!
    var iqamaData                           : IQAMALists!
    var newsFeedData: IQAMAListModel!
    var editButtonFrom = String()
    var iqamaFilled  : [Iqama]!
    var editSaveBtn = false
    var array_PrayerType                  = ["Fajr","Zuhr","Asr",
                                                   "Maghrib",
                                                   "Isha"]
 
    
    override func viewDidLoad() {
        super.viewDidLoad()
          statusBarColorChange(color:UIColor.init(hex: 0x05BDB9))
         // UIApplication.statusBarBackgroundColor = UIColor.init(hex: 0x05BDB9)
        setupLayout()
        if editIqamah == "editIqamah"{
            lbl_Title.text = "Edit Iqamah".localized
        }else{
            lbl_Title.text = "Add Iqamah".localized
        }

        btnAddSpecialPrayer.shadowWithCornerReadius(cornerRadius: 10)
         btnAddJumma.shadowWithCornerReadius(cornerRadius: 10)
        btnAddSpecialPrayer.setTitle("ADD SPECIAL PRAYER".localized, for: .normal)
        btnAddJumma.setTitle("ADD JUMMA".localized, for: .normal)
        
        if newsFeedData != nil{
            self.setDataArrayForNewsFeed()
          
        }else{
              self.setDataArray()
        }
        
    
        if iqamaFilled != nil{
            var  iqamafilledData = [Iqama]()
            iqamafilledData = (self.iqamaFilled)!
            
            numberOfIqamahRows = (numberOfIqamahRows - (self.iqamaFilled.count))
            
        }
        if iqamaFilled != nil{
            var  iqamafilledData = [Iqama]()
           iqamafilledData = (self.iqamaFilled)!
            
             // "[Playground, World]"
            
            let fhhd = iqamafilledData.map({$0.prayerCode})
            print(fhhd)
            if fhhd.contains("F"){
                array_PrayerType = array_PrayerType.filter{$0 != "Fajr"}
                
                print(array_PrayerType)
                //array_PrayerType.remove(at: 0)
            }
            if fhhd.contains("Z"){
                array_PrayerType = array_PrayerType.filter{$0 != "Zuhr"}
                
                print(array_PrayerType)
                //array_PrayerType.remove(at: 1)
            }
            if fhhd.contains("A"){
                array_PrayerType = array_PrayerType.filter{$0 != "Asr"}
                
                print(array_PrayerType)
            }
            if fhhd.contains("M"){
                array_PrayerType = array_PrayerType.filter{$0 != "Maghrib"}
                
                print(array_PrayerType)
            }
            if fhhd.contains("I"){
                array_PrayerType = array_PrayerType.filter{$0 != "Isha"}
                
                print(array_PrayerType)
            }
        }
    // tableView_AddIqamah.reloadData()
    }
    func setDataArrayForNewsFeed(){
        if self.newsFeedData != nil{
        
                self.numberOfIqamahRows = self.newsFeedData?.iqama?.count ?? 0
                self.numberOfJummaRows = self.newsFeedData?.jumma?.count ?? 0
                
                if self.newsFeedData!.jumma != nil && self.newsFeedData!.jumma!.count != 0{
                    var arrayOfJummaData: [JummaDataModel] = []
                    for i in 0...self.newsFeedData.jumma!.count - 1{
                        var model: JummaDataModel = JummaDataModel()
                        
                        let rawdataJumma = self.newsFeedData?.jumma?[i]
                        if rawdataJumma?.type == 1{
                            model.jummaType = "JUMMA1"
                            model.jummaTypeId = 1
                        }else if rawdataJumma?.type == 2{
                            model.jummaType = "JUMMA2"
                            model.jummaTypeId = 2
                        }else if rawdataJumma?.type == 3{
                            model.jummaType = "JUMMA3"
                            model.jummaTypeId = 3
                        }else if rawdataJumma?.type == 4{
                            model.jummaType = "JUMMA4"
                            model.jummaTypeId = 4
                        }else if rawdataJumma?.type == 5{
                            model.jummaType = "JUMMA5"
                            model.jummaTypeId = 5
                        }else if rawdataJumma?.type == 6{
                            model.jummaType = "JUMMA6"
                            model.jummaTypeId = 6
                        }else if rawdataJumma?.type == 7{
                            model.jummaType = "JUMMA7"
                            model.jummaTypeId = 7
                        }else if rawdataJumma?.type == 8{
                            model.jummaType = "JUMMA8"
                            model.jummaTypeId = 8
                        }else if rawdataJumma?.type == 9{
                            model.jummaType = "JUMMA9"
                            model.jummaTypeId = 9
                        }else if rawdataJumma?.type == 10{
                            model.jummaType = "JUMMA10"
                            model.jummaTypeId = 10
                        }else{
                            model.jummaType = "JUMMA"
                            model.jummaTypeId = 1
                        }
                        
                        model.location = self.newsFeedData.jumma?[i].address ?? ""
                        model.desc = self.newsFeedData?.jumma?[i].notes ?? ""
                        model.locationLat = Double((self.newsFeedData?.jumma?[i].latitude)!)
                        model.locationLong = Double((self.newsFeedData?.jumma?[i].longitude)!)
                        
                        model.time = self.newsFeedData?.jumma?[i].time ?? ""
                        model.timeIn24 = self.newsFeedData?.jumma?[i].time ?? ""
                        model.date = self.newsFeedData?.jumma?[i].date ?? ""
                        model.orgId = self.newsFeedData?.jumma?[i].org ?? 0
                        model.id = self.newsFeedData?.jumma?[i].id ?? 0
                        model.isEdit = true
                        arrayOfJummaData.append(model)
                    }
                    self.array_JummaModel = arrayOfJummaData
                }
            self.array_IqamahModel = []
                if self.newsFeedData!.iqama != nil && self.newsFeedData!.iqama!.count != 0{
                    var arrayOfIqamaData: [IqamahDataModel] = []
                    for i in 0...self.newsFeedData!.iqama!.count - 1{
                        var model: IqamahDataModel = IqamahDataModel()
                        
                        let rawdataiqama = self.newsFeedData?.iqama?[i]
                        
                        
                        model.isEdit = true
                        model.time = rawdataiqama?.time ?? ""
                        model.timeIn24 = rawdataiqama?.time ?? ""
                        model.date = rawdataiqama?.date ?? ""
                        model.orgId = rawdataiqama?.org ?? 0
                        model.id = rawdataiqama?.id ?? 0
                        model.notes = rawdataiqama?.notes ?? ""
                        model.isPrayed = rawdataiqama?.swIsPrayed ?? "Y"
                        if rawdataiqama?.calc_type == "A"{
                            model.prayerType = "auto"
                            model.cellHeight = 330
                            model.calculationMethod = rawdataiqama?.calc_method
                            model.offestTime = rawdataiqama?.offset ?? 0
                        }else{
                            model.prayerType = "manual"
                            model.cellHeight = 290
                            model.calculationMethod = nil
                        }
                        if rawdataiqama?.prayerCode == "F"{
                            model.prayerName = "Fajr"
                        }else if rawdataiqama?.prayerCode == "Z"{
                            model.prayerName = "Zuhr"
                        }else if rawdataiqama?.prayerCode == "A"{
                            model.prayerName = "Asr"
                        }else if rawdataiqama?.prayerCode == "M"{
                            model.prayerName = "Maghrib"
                        }else if rawdataiqama?.prayerCode == "I"{
                            model.prayerName = "Isha"
                        }
                        
                        model.isEdit = true
                        arrayOfIqamaData.append(model)
                    }
                    self.array_IqamahModel = arrayOfIqamaData
                }
                
                self.numberOfSpecialPrayerRows = 0
            
            self.btnAddSpecialPrayer.isHidden = true
            self.btnAddJumma.isHidden = true
        }
        self.tableView_AddIqamah.reloadData()
    }
    
    func setDataArray(){
        if self.iqamaData != nil{
            if editButtonFrom == "IQAMAH"{
                self.numberOfIqamahRows = self.iqamaData.data?.iqama?.count ?? 0
                self.numberOfJummaRows = self.iqamaData.data?.jumma?.count ?? 0
                
                if self.iqamaData.data!.jumma != nil && self.iqamaData.data!.jumma!.count != 0{
                    var arrayOfJummaData: [JummaDataModel] = []
                    for i in 0...self.iqamaData.data!.jumma!.count - 1{
                        var model: JummaDataModel = JummaDataModel()
                        
                        let rawdataJumma = self.iqamaData.data?.jumma?[i]
                        if rawdataJumma?.type == 1{
                            model.jummaType = "Jumma 1"
                             model.jummaTypeId = 1
                        }else if rawdataJumma?.type == 2{
                            model.jummaType = "Jumma 2"
                             model.jummaTypeId = 2
                        }else if rawdataJumma?.type == 3{
                            model.jummaType = "Jumma 3"
                             model.jummaTypeId = 3
                        }else if rawdataJumma?.type == 4{
                            model.jummaType = "Jumma 4"
                             model.jummaTypeId = 4
                        }else if rawdataJumma?.type == 5{
                            model.jummaType = "Jumma 5"
                             model.jummaTypeId = 5
                        }else if rawdataJumma?.type == 6{
                            model.jummaType = "Jumma 6"
                             model.jummaTypeId = 6
                        }else if rawdataJumma?.type == 7{
                            model.jummaType = "Jumma 7"
                             model.jummaTypeId = 7
                        }else if rawdataJumma?.type == 8{
                            model.jummaType = "Jumma 8"
                             model.jummaTypeId = 8
                        }else if rawdataJumma?.type == 9{
                             model.jummaType = "Jumma 9"
                             model.jummaTypeId = 9
                        }else if rawdataJumma?.type == 10{
                             model.jummaType = "Jumma 10"
                             model.jummaTypeId = 10
                        }else{
                            model.jummaType = "Jumma"
                             model.jummaTypeId = 1
                        }
                        
                        model.location = self.iqamaData.data?.jumma?[i].address ?? ""
                        model.desc = self.iqamaData.data?.jumma?[i].notes ?? ""
                        model.locationLat = Double((self.iqamaData.data?.jumma?[i].latitude)!)
                        model.locationLong = Double((self.iqamaData.data?.jumma?[i].longitude)!)
                        
                        model.time = self.iqamaData.data?.jumma?[i].time ?? ""
                        model.timeIn24 = self.iqamaData.data?.jumma?[i].time ?? ""
                        model.date = self.iqamaData.data?.jumma?[i].date ?? ""
                        model.orgId = self.iqamaData.data?.jumma?[i].org ?? 0
                        model.id = self.iqamaData.data?.jumma?[i].id ?? 0
                        model.isEdit = true
                        arrayOfJummaData.append(model)
                    }
                    self.array_JummaModel = arrayOfJummaData
                }
                self.array_IqamahModel = []
                if self.iqamaData.data!.iqama != nil && self.iqamaData.data!.iqama!.count != 0{
                      var arrayOfIqamaData: [IqamahDataModel] = []
                    for i in 0...self.iqamaData.data!.iqama!.count - 1{
                        var model: IqamahDataModel = IqamahDataModel()
                        
                        let rawdataiqama = self.iqamaData.data?.iqama?[i]
                        
                       
                        model.isEdit = true
                        model.time = rawdataiqama?.time ?? ""
                        model.timeIn24 = rawdataiqama?.time ?? ""
                        model.date = rawdataiqama?.date ?? ""
                        model.orgId = rawdataiqama?.org ?? 0
                        model.id = rawdataiqama?.id ?? 0
                        model.notes = rawdataiqama?.notes ?? ""
                        model.isPrayed = rawdataiqama?.swIsPrayed ?? "Y"
                        if rawdataiqama?.calc_type == "A"{
                             model.prayerType = "auto"
                            model.cellHeight = 330
                            model.calculationMethod = rawdataiqama?.calc_method //?? 0
                            model.offestTime = rawdataiqama?.offset ?? 0
                        }else{
                             model.prayerType = "manual"
                            model.cellHeight = 290
                             model.calculationMethod = nil
                        }
                      
                        if rawdataiqama?.prayerCode == "F"{
                           model.prayerName = "Fajr"
                        }else if rawdataiqama?.prayerCode == "Z"{
                            model.prayerName = "Zuhr"
                        }else if rawdataiqama?.prayerCode == "A"{
                            model.prayerName = "Asr"
                        }else if rawdataiqama?.prayerCode == "M"{
                            model.prayerName = "Maghrib"
                        }else if rawdataiqama?.prayerCode == "I"{
                            model.prayerName = "Isha"
                        }
                      
                        model.isEdit = true
                        arrayOfIqamaData.append(model)
                    }
                    self.array_IqamahModel = arrayOfIqamaData
                }
                
                self.numberOfSpecialPrayerRows = 0
            }else{
                self.numberOfSpecialPrayerRows = self.iqamaData.data?.specialPrayers?.count ?? 0
                self.numberOfIqamahRows = 0
                self.numberOfJummaRows = 0
                if self.iqamaData.data!.specialPrayers != nil || self.iqamaData.data!.specialPrayers!.count > 0{
                    var arrayOfSpecialPrayerData: [SpecialPrayerDataModel] = []
                    for i in 0...self.iqamaData.data!.specialPrayers!.count - 1{
                        var model: SpecialPrayerDataModel = SpecialPrayerDataModel()
                        model.prayerName = self.iqamaData.data?.specialPrayers?[i].name ?? ""
                        model.location = self.iqamaData.data?.specialPrayers?[i].address ?? ""
                        model.desc = self.iqamaData.data?.specialPrayers?[i].specialPrayerDescription ?? ""
                        model.locationLat = Double((self.iqamaData.data?.specialPrayers?[i].latitude)!)
                        model.locationLong = Double((self.iqamaData.data?.specialPrayers?[i].longitude)!)
                        
                        model.time = self.iqamaData.data?.specialPrayers?[i].time ?? ""
                        model.timeIn24 = self.iqamaData.data?.specialPrayers?[i].time ?? ""
                        model.date = self.iqamaData.data?.specialPrayers?[i].date ?? ""
                        model.orgId = self.iqamaData.data?.specialPrayers?[i].org ?? 0
                        model.id = self.iqamaData.data?.specialPrayers?[i].id ?? 0
                        model.isEdit = true
                        arrayOfSpecialPrayerData.append(model)
                    }
                    self.array_SpecialPrayerModel = arrayOfSpecialPrayerData
                }
            }
            self.btnAddSpecialPrayer.isHidden = true
            self.btnAddJumma.isHidden = true
        }else{
            self.btnAddSpecialPrayer.isHidden = false
            self.btnAddJumma.isHidden = false
        }
        self.tableView_AddIqamah.reloadData()
    }
    
    func setupLayout(){
        self.tableView_AddIqamah.rowHeight = UITableView.automaticDimension
        self.tableView_AddIqamah.estimatedRowHeight = 130
        self.tableView_AddIqamah.register(UINib(nibName: "AddIqamahTableViewCell", bundle: nil), forCellReuseIdentifier: "AddIqamahTableViewCell")
        self.tableView_AddIqamah.register(UINib(nibName: "AddJummaTableViewCell", bundle: nil), forCellReuseIdentifier: "AddJummaTableViewCell")
        self.tableView_AddIqamah.register(UINib(nibName: "AddSpecialPrayerTableViewCell", bundle: nil), forCellReuseIdentifier: "AddSpecialPrayerTableViewCell")
        self.setUpIqamahCell()
    }
    
    func setUpIqamahCell(){
        for i in 0...self.numberOfIqamahRows - 1{
            var model = IqamahDataModel()
            model.cellHeight = 130
            self.array_IqamahModel.insert(model, at: i)
        }
    }
    
    
    func scrollToBottom(row: Int, section: Int){
        DispatchQueue.main.async {
            let indexPath = IndexPath(row: row, section: section)
            self.tableView_AddIqamah.scrollToRow(at: indexPath, at: .bottom, animated: true)
        }
    }
    
    //MARK:- Actions Methods
    @IBAction func btn_Back(sender:UIButton){
        if addNewIqamah == "addNewIqamah"{
            self.presentingViewController?.presentingViewController?.dismiss(animated: true, completion: nil)
        }else{
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    @IBAction func btnAddJummaTapped(sender:UIButton){
        self.numberOfJummaRows = 1
        self.array_JummaModel.append(JummaDataModel())
        self.btnAddJumma.isHidden = true
        if self.numberOfSpecialPrayerRows > 0{
            //  self.bottomViewHeight.constant = 0
        }else{
            // self.bottomViewHeight.constant = 90
        }
        self.tableView_AddIqamah.reloadData()
        self.scrollToBottom(row: self.numberOfJummaRows - 1, section: 1)
    }
    
    @IBAction func btnAddSpecialPrayerTapped(sender:UIButton){
        self.numberOfSpecialPrayerRows = 1
        self.array_SpecialPrayerModel.append(SpecialPrayerDataModel())
        self.btnAddSpecialPrayer.isHidden = true
        if self.numberOfJummaRows > 0{
            // self.bottomViewHeight.constant = 0
        }else{
            // self.bottomViewHeight.constant = 135
        }
        self.tableView_AddIqamah.reloadData()
        self.scrollToBottom(row: self.numberOfSpecialPrayerRows - 1, section: 2)
    }
    
    
    
    
}
