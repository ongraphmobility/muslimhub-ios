//
//  AddSpecialPrayerTableViewCell.swift
//  Muslim Hub
//
//  Created by Himanshu Goyal on 27/06/19.
//  Copyright © 2019 OnGraph. All rights reserved.
//

import UIKit
import KVNProgress
class AddSpecialPrayerTableViewCell: UITableViewCell,UITextViewDelegate {

    @IBOutlet weak var stackViewHeight: NSLayoutConstraint!
    @IBOutlet weak var txtDesc       : UITextView!
    @IBOutlet weak var btnSave       : UIButton!
    @IBOutlet weak var txtLocation   : UITextField!
    @IBOutlet weak var btnDate       : UIButton!
    @IBOutlet weak var btnTime       : UIButton!
    @IBOutlet weak var txtPrayerName : UITextField!
    @IBOutlet weak var btnAddMore    : UIButton!
    @IBOutlet weak var btnCancel     : UIButton!
    var delegate                     : AddIqamahOthersCellDelegate!
    var indexPath                    : IndexPath!
    var viewController               : UIViewController!
    var model                        : SpecialPrayerDataModel!
    var specialPrayerDate = String()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.txtDesc.delegate = self
        self.txtPrayerName.delegate = self
        self.txtLocation.delegate = self
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func callSaveModel(){
        self.delegate.saveDataModel(indexPath: self.indexPath, jummaModel: JummaDataModel(), specialPrayerModel: self.model)
    }
    
    func setUpCell(indexPath: IndexPath, viewC: UIViewController, model: SpecialPrayerDataModel){
        self.indexPath = indexPath
        self.viewController = viewC
        self.model = model
          self.txtDesc.delegate = self
        if self.model != nil {
            //Location
            if let location = self.model.location{
                self.txtLocation.text = location
            }else{
                self.txtLocation.text = ""
            }
     
            //Prayer Name
            if let prayerName = self.model.prayerName{
                self.txtPrayerName.text = prayerName
            }else{
                self.txtPrayerName.text = ""
            }
            
            //Time
            if let time = self.model.time{
                self.btnTime.setTitle(time, for: .normal)
                self.btnTime.setTitleColor(UIColor.black, for: .normal)
            }else{
                self.btnTime.setTitle("Time", for: .normal)
                self.btnTime.setTitleColor(UIColor.lightGray, for: .normal)
            }
            
            //Date
            if let date = self.model.date{
                self.btnDate.setTitle(date, for: .normal)
                self.btnDate.setTitleColor(UIColor.black, for: .normal)
            }else{
                self.btnDate.setTitleColor(UIColor.lightGray, for: .normal)
                self.btnDate.setTitle("Date", for: .normal)
            }
            
            //Desc
            if let desc = self.model.desc{
                self.txtDesc.text = desc
            }else{
                self.txtDesc.text = "Notes"
            }

            var isUpdated: Bool = false
            
            if let check = self.model.isUpdated{
                isUpdated = check
            }else{
                isUpdated = false
            }
            
            var isEdit: Bool = false
            
            if let check = self.model.isEdit{
                isEdit = check
            }else{
                isEdit = false
            }
            
            self.model.isEdit = isEdit

            
            if isUpdated{
                self.btnSave.isHidden = true
                self.btnCancel.isHidden = true
                self.stackViewHeight.constant = 0
                self.txtPrayerName.isUserInteractionEnabled = false
                self.btnTime.isUserInteractionEnabled = false
                self.btnDate.isUserInteractionEnabled = false
                self.txtLocation.isUserInteractionEnabled = false
                self.txtDesc.isUserInteractionEnabled = false
            }else{
                self.btnSave.isHidden = false
                self.btnCancel.isHidden = false
                self.stackViewHeight.constant = 45
                self.txtPrayerName.isUserInteractionEnabled = true
                self.btnTime.isUserInteractionEnabled = true
                self.btnDate.isUserInteractionEnabled = true
                self.txtLocation.isUserInteractionEnabled = true
                self.txtDesc.isUserInteractionEnabled = true
            }
            
            if isEdit{
                self.btnCancel.setTitle("Delete", for: .normal)
                self.btnAddMore.isHidden = true
//                if isEdit == true && isUpdated == true{
//                    self.btnSave.isHidden = false
//                    self.btnCancel.isHidden = false
//                }else{
//                    self.btnSave.isHidden = true
//                    self.btnCancel.isHidden = true
//                }
            }else{
                self.btnCancel.setTitle("Clear", for: .normal)
                self.btnAddMore.isHidden = false
            }
        }else{
            self.btnSave.isHidden = false
            self.btnCancel.isHidden = false
            self.stackViewHeight.constant = 45
            self.txtPrayerName.isUserInteractionEnabled = true
            self.btnTime.isUserInteractionEnabled = true
            self.btnDate.isUserInteractionEnabled = true
            self.txtLocation.isUserInteractionEnabled = true
            self.txtDesc.isUserInteractionEnabled = true
            self.txtPrayerName.text = ""
            self.btnTime.setTitle("Time", for: .normal)
            self.btnDate.setTitle("Date", for: .normal)
            self.txtLocation.text = ""
            self.txtDesc.text = ""
        }
    }
    
    @IBAction func btnAddMoreTapped(_ sender: Any) {
        self.delegate.btnAddMoreIqamahCell(indexPath: self.indexPath)
    }
    
//    @IBAction func btnCancelTapped(_ sender: Any) {
//        self.delegate.btnDeleteTapped(indexPath: self.indexPath)
//        print("Delete Special Prayer")
//        
//        let id = self.model.id ?? 0
//        KVNProgress.show()
//        Network.shared.deleteSpecialPrayerFroIqamah(id: id) { (result) in
//              KVNProgress.dismiss()
//             guard let user = result else{
//                
//                return
//            }
//            if user.success == true{
//                self.showSingleAlertMessage(message: "", subMsg: user!.message, sender: self, completion: { (_) in
//                   
//                })
//                
//            }
//            print(user)
//        }
//    }
    
    @IBAction func btnTimeTapped(_ sender: Any) {
        
        if AppDelegate.instance.currentDate == self.specialPrayerDate{
            PickerViewController.openDatePickerView(viewC: self.viewController, currentDate: nil, minimumDate: Date(), pickerMode: .time) { (value) in
                let timedateFormatter = DateFormatter()
                
                
                timedateFormatter.dateFormat = "h:mm a"
                let time = timedateFormatter.string(from: value)
                self.btnTime.setTitleColor(UIColor.black, for: .normal)
                self.btnTime.setTitle(time, for: .normal)
                
                let timedateFormatter1 = DateFormatter()
                timedateFormatter1.dateFormat = "HH:mm"
                let time1 = timedateFormatter1.string(from: value)
                
                self.model.timeIn24 = time1
                self.model.time = time
                self.callSaveModel()
            }
        }else{
            PickerViewController.openDatePickerView(viewC: self.viewController, currentDate: nil, pickerMode: .time) { (value) in
                
               
                self.btnTime.setTitle("", for: .normal)
                
             
                
                self.model.timeIn24 = ""
                self.model.time = ""
              
                
                let timedateFormatter = DateFormatter()
                
                
                timedateFormatter.dateFormat = "h:mm a"
                let time = timedateFormatter.string(from: value)
                self.btnTime.setTitleColor(UIColor.black, for: .normal)
                self.btnTime.setTitle(time, for: .normal)
                
                let timedateFormatter1 = DateFormatter()
                timedateFormatter1.dateFormat = "HH:mm"
                let time1 = timedateFormatter1.string(from: value)
                
                self.model.timeIn24 = time1
                self.model.time = time
                self.callSaveModel()
            }
        }
     
    }
    
    @IBAction func btnDateTapped(_ sender: Any) {
        PickerViewController.openDatePickerView(viewC: self.viewController, currentDate: nil, minimumDate: Date(), pickerMode: .date) { (value) in
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "dd-MM-yyyy"
            let date = dateFormatter.string(from: value)
            self.btnDate.setTitleColor(UIColor.black, for: .normal)
            self.btnDate.setTitle(date, for: .normal)
            
           
                self.specialPrayerDate = date
                self.model.date = date
                self.callSaveModel()
           
          
        }
    }
    
    private func getValidate() -> (Bool, String){
        var error : (Bool, String) = (false, "")
        if self.model != nil{
            if self.model.prayerName == nil || self.model.prayerName == ""{
                error = (false, "Please enter Prayer Name")
            }else if self.model.time == nil || self.model.time == ""{
                error = (false, "Please Select Time")
            }else if self.model.date == nil || self.model.date == ""{
                error = (false, "Please Select Date")
            }else if self.model.location == nil || self.model.location == ""{
                error = (false, "Please Select Location")
            }else if self.model.desc == nil || self.model.desc == ""{
                error = (false, "Please enter Description")
            }else{
                error = (true, "Success")
            }
        }else{
            error = (false, "Please enter all details")
        }
        return error
    }
    
    @IBAction func btnSaveTapped(_ sender: Any) {
        let (isValidate, errorMessage) = self.getValidate()
        if isValidate{
            self.delegate.btnSaveTapped(indexPath: self.indexPath)
        }else{
            self.viewController.showSingleAlertMessage(message: "Warning", subMsg: errorMessage, sender: self.viewController) { (_) in
            }
        }
    }
    
}

extension AddSpecialPrayerTableViewCell: MHDescDelegate{
    func getDescription(desc: String) {
        self.txtDesc.text = desc
        
        self.model.desc = desc
        self.callSaveModel()
    }
}


extension AddSpecialPrayerTableViewCell: UITextFieldDelegate
{
    func textViewDidBeginEditing(_ textView: UITextView) {
         if textView == self.txtDesc{
            let vc = viewController.storyboard?.instantiateViewController(withIdentifier: "MHDescViewController") as! MHDescViewController
            vc.delegate = self
            vc.textTitle = "Add Notes"
            if self.txtDesc.text == "Notes"{
                vc.textDesc = ""
            }else{
                vc.textDesc = self.txtDesc.text ?? ""
            }
            vc.modalPresentationStyle = .fullScreen
            self.viewController.present(vc, animated: false, completion: nil)
        }
    }
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == self.txtLocation{
            self.delegate.btnLocationTapped(indexPath: self.indexPath)
        }
    }
    
    func textFieldDidEndEditing(_ textField: UITextField, reason: UITextField.DidEndEditingReason) {
        if textField == self.txtPrayerName{
            self.model.prayerName = textField.text
            self.callSaveModel()
        }
    }
}
