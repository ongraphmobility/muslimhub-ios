//
//  IqamahTableViewExtension.swift
//  Muslim Hub
//
//  Created by Himanshu Goyal on 27/06/19.
//  Copyright © 2019 OnGraph. All rights reserved.
//

import Foundation
import UIKit
import KVNProgress

enum IqamahCellType{
    case iqamah
    case jumma
    case specialPrayer
}

struct IqamahDataModel{
    var prayerName: String?
    var prayerType: String?
    var isPrayed: String?
    var calculationMethod: Int?
    var offestTime: Int?
    var notes: String?
    var cellHeight: CGFloat?
    var isUpdated: Bool?
    var org:Int?
    var prayer_code:String?
    var sw_is_prayed:String?
    var date:String?
    var time:String?
    var timeIn24: String?
    var calc_type:String?
    var isEdit: Bool?
    var orgId :Int?
    var id : Int?
}

struct JummaDataModel{
    var jummaType: String?
    var jummaTypeId: Int?
    var date: String?
    var time: String?
    var timeIn24: String?
    var location: String?
    var locationLat: Double?
    var locationLong: Double?
    var desc: String?
    var isUpdated: Bool?
    var isEdit: Bool?
    var orgId :Int?
    var id : Int?
}

struct SpecialPrayerDataModel{
    var prayerName: String?
    var time: String?
    var timeIn24: String?
    var date: String?
    var location: String?
    var locationLat: Double?
    var locationLong: Double?
    var desc: String?
    var isUpdated: Bool?
    var isEdit: Bool?
    var orgId :Int?
    var id : Int?
}

extension IqamahViewController: UITableViewDelegate, UITableViewDataSource,UITextViewDelegate{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 0:
            return self.numberOfIqamahRows
        case 1:
            return self.numberOfJummaRows
        case 2:
            return self.numberOfSpecialPrayerRows
        default:
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.section {
        case 0:
            let cell = tableView.dequeueReusableCell(withIdentifier: "AddIqamahTableViewCell", for: indexPath) as! AddIqamahTableViewCell
//
            cell.array_SelectPrayerType = array_PrayerType


           
                        
           
            cell.setUpCell(indexPath: indexPath, viewC: self, model: self.array_IqamahModel[indexPath.row],numberOfRows: cell.array_SelectPrayerType, indexRow: indexPath.row)
           // cell.btnChoosePrayerType.setTitle(cell.array_SelectPrayerType[indexPath.row], for: .normal)
           
            cell.btnCancelAuto.tag = indexPath.row
            cell.btnCancelAuto.addTarget(self, action: #selector(btnCancelTappedForIqamaMannual(sender:)), for: .touchUpInside)
            cell.btnCancelMannual.tag = indexPath.row
            cell.btnCancelMannual.addTarget(self, action: #selector(btnCancelTappedForIqamaMannual(sender:)), for: .touchUpInside)
            cell.delegate = self
            cell.btnNotPrayed.tag = indexPath.row
            return cell
        case 1:
            let cell = tableView.dequeueReusableCell(withIdentifier: "AddJummaTableViewCell", for: indexPath) as! AddJummaTableViewCell
          
            cell.setUpCell(indexPath: indexPath, viewC: self, model: self.array_JummaModel[indexPath.row])
            cell.btnCancel.tag = indexPath.row
            cell.btnCancel.addTarget(self, action: #selector(btnCancelTappedForJumma(sender:)), for: .touchUpInside)
            cell.delegate = self
            return cell
        case 2:
            let cell = tableView.dequeueReusableCell(withIdentifier: "AddSpecialPrayerTableViewCell", for: indexPath) as! AddSpecialPrayerTableViewCell
         
            cell.btnCancel.tag = indexPath.row
            cell.btnCancel.addTarget(self, action: #selector(btnCancelTapped(sender:)), for: .touchUpInside)
            cell.setUpCell(indexPath: indexPath, viewC: self, model: self.array_SpecialPrayerModel[indexPath.row])
            
            cell.delegate = self
            return cell
        default :
            break
        }
        return UITableViewCell()
    }
    
}

//MARK: - Add Iqamah Table View Cell Delegate
extension IqamahViewController: AddIqamahCellDelegate{
    
    
    func btnChooseTypeIqamagCell(indexPath: IndexPath, value: String, isHeightUpdateNeeded: Bool, cellHeight: CGFloat) {
        var model = self.array_IqamahModel[indexPath.row]
        model.prayerType = value
        if isHeightUpdateNeeded{
            model.cellHeight = cellHeight
        }
        self.array_IqamahModel[indexPath.row] = model
        self.tableView_AddIqamah.beginUpdates()
        self.tableView_AddIqamah.endUpdates()
        self.tableView_AddIqamah.reloadRows(at: [indexPath], with: .fade)
    }
    
    func reloadIqamagCell(indexPath: IndexPath, height: CGFloat) {
        //        self.array_IqamaCellHeight.insert(height, at: indexPath.row)
        //        self.tableView_AddIqamah.reloadRows(at: [indexPath], with: .none)
    }
    
    func btn_IqamaSaveTapped(indexPath: IndexPath,iqamaType: String) {
        if iqamaType == "Manual"{
            self.iqamaAddApi(indexPath: indexPath, iqamaType: "M")
        }else{
            self.iqamaAddApi(indexPath: indexPath, iqamaType: "A")
        }
        
    }
    
    func saveDataIqamaModel(indexPath: IndexPath, iQAMAModel: IqamahDataModel) {
        self.array_IqamahModel[indexPath.row] = iQAMAModel
    }
    
}

extension IqamahViewController: AddIqamahOthersCellDelegate{
    func btnAddMoreIqamahCell(indexPath: IndexPath) {
        if indexPath.section == 1{
            if self.numberOfJummaRows < 10{
                self.numberOfJummaRows = self.numberOfJummaRows + 1
                self.array_JummaModel.append(JummaDataModel())
                self.tableView_AddIqamah.reloadSections(IndexSet(integer: indexPath.section), with: .fade)
                self.scrollToBottom(row: indexPath.row + 1, section: indexPath.section)
            }
        }else {
            if self.numberOfSpecialPrayerRows < 10{
                self.numberOfSpecialPrayerRows = self.numberOfSpecialPrayerRows + 1
                self.array_SpecialPrayerModel.append(SpecialPrayerDataModel())
                self.tableView_AddIqamah.reloadSections(IndexSet(integer: indexPath.section), with: .fade)
                self.scrollToBottom(row: indexPath.row + 1, section: indexPath.section)
            }
        }
    }
    
    
    func btnLocationTapped(indexPath: IndexPath) {
        self.selectedLocationButton = indexPath
        self.setUpLocationView()
    }
    
    func saveDataModel(indexPath: IndexPath, jummaModel: JummaDataModel, specialPrayerModel: SpecialPrayerDataModel){
        if indexPath.section == 1{
            print(jummaModel)
            self.array_JummaModel[indexPath.row] = jummaModel
        }else if indexPath.section == 2{
            print(specialPrayerModel)
            self.array_SpecialPrayerModel[indexPath.row] = specialPrayerModel
        }else{
            
        }
    }
    
    func btnSaveTapped(indexPath: IndexPath) {
        
        var friday = String()
        let upcoingFriday = Date.today().next(.friday)
         let todayFriday = Date.today()
        let weekday = Calendar.current.component(.weekday, from: todayFriday)
        
        if weekday == 6{
            
            let dateFormatter1 = DateFormatter()
            dateFormatter1.dateFormat = "dd-MM-yyyy"
            friday = dateFormatter1.string(from: todayFriday)
            print(friday)
            
        }else{
            let dateFormatter1 = DateFormatter()
            dateFormatter1.dateFormat = "dd-MM-yyyy"
            friday = dateFormatter1.string(from: upcoingFriday)
            print(friday)
        }
        
        var requestDict = [String: Any]()
        
      
        
        if self.newsFeedData != nil{
            
            if indexPath.section == 1{
                var model = self.array_JummaModel[indexPath.row]
                
                if newsFeedData != nil{
                    
                    if newsFeedData?.jumma?.count != 0{
                        self.editSaveBtn = true
                        
                        requestDict = ["type"     : model.jummaTypeId!,
                                       "time"     : model.timeIn24!,
                                       "latitude" : model.locationLat!,
                                       "longitude": model.locationLong!,
                                       "address"  : model.location!,
                                       "notes"    : model.desc!,
                                       "org"      : model.orgId!,
                                       "date"     : friday,
                                       "id": model.id!]
                    }
                    
                }
                else{
                    requestDict = ["type"     : model.jummaTypeId!,
                                   "time"     : model.timeIn24!,
                                   "latitude" : model.locationLat!,
                                   "longitude": model.locationLong!,
                                   "address"  : model.location!,
                                   "notes"    : model.desc!,
                                   "org"      : self.orgId,
                                   "date"     : friday]
                }
                
                KVNProgress.show()
                Network.shared.addJummaForIqamah(requestDict: requestDict,editFrom:self.editSaveBtn) { (response) in
                    KVNProgress.dismiss()
                    
                    guard let user = response else{
                        return
                    }
                    if response!.success == true{
                        self.showSingleAlertMessage(message: "", subMsg: user.message, sender: self, completion: { (_) in
                            
                            if self.editSaveBtn == false{
                              
                                  model.isUpdated = true
                            }else{
                              model.isUpdated = false
                            }
                           
                            self.array_JummaModel[indexPath.row] = model
                            self.tableView_AddIqamah.reloadRows(at: [indexPath], with: .fade)
                        })
                        
                    }else{
                        self.showSingleAlertMessage(message: "", subMsg: user.message, sender: self, completion: { (_) in
                        })
                    }
                    
                    
                    
                }
                
            }
        }else{
            
        
        
        if indexPath.section == 1{
            var model = self.array_JummaModel[indexPath.row]
            
            if iqamaData != nil{
                
                if iqamaData?.data?.jumma?.count != 0{
                    self.editSaveBtn = true
                    
                    requestDict = ["type"     : model.jummaTypeId!,
                                   "time"     : model.timeIn24!,
                                   "latitude" : model.locationLat!,
                                   "longitude": model.locationLong!,
                                   "address"  : model.location!,
                                   "notes"    : model.desc!,
                                   "org"      : model.orgId!,
                                   "date"     : friday,
                                   "id": model.id!]
                }
                
            }
            else{
                requestDict = ["type"     : model.jummaTypeId!,
                               "time"     : model.timeIn24!,
                               "latitude" : model.locationLat!,
                               "longitude": model.locationLong!,
                               "address"  : model.location ?? "",
                               "notes"    : model.desc ?? "",
                               "org"      : self.orgId,
                               "date"     : friday]
            }
            
            KVNProgress.show()
            Network.shared.addJummaForIqamah(requestDict: requestDict,editFrom:self.editSaveBtn) { (response) in
                KVNProgress.dismiss()
                
                guard let user = response else{
                    return
                }
                if response!.success == true{
                    self.showSingleAlertMessage(message: "", subMsg: user.message, sender: self, completion: { (_) in
                        
                        if self.editSaveBtn == false{
                            
                            model.isUpdated = true
                        }else{
                            model.isUpdated = false
                        }
                        self.array_JummaModel[indexPath.row] = model
                        self.tableView_AddIqamah.reloadRows(at: [indexPath], with: .fade)
                    })
                    
                }else{
                    self.showSingleAlertMessage(message: "", subMsg: user.message, sender: self, completion: { (_) in
                    })
                }
                
                
            }
            
        }else{
            
            var requestDict = [String: Any]()
            var model = self.array_SpecialPrayerModel[indexPath.row]
            
            if iqamaData != nil{
                
                if iqamaData?.data?.specialPrayers?.count != 0{
                    self.editSaveBtn = true
                    
                    requestDict = ["name"        : model.prayerName!,
                                   "date"        : model.date!,
                                   "time"        : model.timeIn24!,
                                   "latitude"    : model.locationLat!,
                                   "longitude"   : model.locationLong!,
                                   "address"     : model.location!,
                                   "description" : model.desc!,
                                   "org"         : model.orgId!,
                                   "id"          : model.id!
                    ]
                    
                }
            }else{
                self.editSaveBtn = false
                
                requestDict = ["name"        : model.prayerName!,
                               "date"        : model.date!,
                               "time"        : model.timeIn24!,
                               "latitude"    : model.locationLat!,
                               "longitude"   : model.locationLong!,
                               "address"     : model.location!,
                               "description" : model.desc!,
                               "org"         : self.orgId
                    
                ]
            }
            
            
            KVNProgress.show()
            Network.shared.addSpecialPrayerFroIqamah(requestDict: requestDict,editSpecialPrayer:editSaveBtn) { (response) in
                KVNProgress.dismiss()
                guard let user = response else{
                    return
                }
                if response!.success == true{
                    self.showSingleAlertMessage(message: "", subMsg: user.message, sender: self, completion: { (_) in
                        model.isUpdated = true
                        self.array_SpecialPrayerModel[indexPath.row] = model
                        self.tableView_AddIqamah.reloadRows(at: [indexPath], with: .fade)
                    })
                }else{
                    self.showSingleAlertMessage(message: "", subMsg: user.message, sender: self, completion: { (_) in
                    })
                }
            }
        }
        
    }
}
    //MARK:Add IQAMA Manual Api---------------------
    
    
    func iqamaAddApi(indexPath: IndexPath,iqamaType:String){
        
      
        let cell = tableView_AddIqamah.cellForRow(at: indexPath) as! AddIqamahTableViewCell
        var model = self.array_IqamahModel[indexPath.row]
        var prayer_code = String()
        if cell.btnChoosePrayerType.currentTitle == "Fajr"{
            prayer_code = "F"
        }else if cell.btnChoosePrayerType.currentTitle == "Zuhr"{
            prayer_code = "Z"
        }else if cell.btnChoosePrayerType.currentTitle == "Asr"{
            prayer_code = "A"
        }else if  cell.btnChoosePrayerType.currentTitle == "Maghrib"{
            prayer_code = "M"
        }else if  cell.btnChoosePrayerType.currentTitle == "Isha"{
            prayer_code = "I"
        }
        if model.isPrayed == nil{
            model.isPrayed = "Y"
        }
        var iqamaTypeCal = String()
        var requestDict = [String: Any]()
        if iqamaType == "M"{
            iqamaTypeCal = "M"
           
            if model.isEdit == true{
                if model.isPrayed == "Y"{
                    
                    if cell.btnTimeMannual.currentTitle == "Time"{
                        AKAlertController.alert("Please select time")
                        return
                    }else{
                        requestDict = ["org" : self.orgId,
                                                      "prayer_code" : prayer_code,
                                                      "sw_is_prayed": model.isPrayed ?? "Y",
                                                      "date": AppDelegate.instance.currentDate,
                                                      "time": "\(model.timeIn24 ?? "")",
                                           "address": "Y",
                                           "notes": "\(model.notes ?? "")",
                                           "calc_type": "M",
                                           "id":model.id!
                                       ]
                    }
                }else{
                    
                    requestDict = ["org" : self.orgId,
                                                                        "prayer_code" : prayer_code,
                                                                        "sw_is_prayed": model.isPrayed ?? "Y",
                                                                        "date": AppDelegate.instance.currentDate,
                                                                        "time": "\(model.timeIn24 ?? "")",
                                                             "address": "Y",
                                                             "notes": "\(model.notes ?? "")",
                                                             "calc_type": "M",
                                                             "id":model.id!
                                                         ]
                }
               
            }else{
                
               
                
                if model.isPrayed == "Y"{
                    if model.time == nil{
                        AKAlertController.alert("Please fill data")
                    }else{
                        
                        requestDict = ["org" : self.orgId,
                                       "prayer_code" : prayer_code,
                                       "sw_is_prayed": model.isPrayed ?? "Y",
                                       "date": AppDelegate.instance.currentDate,
                                       "time": "\(model.timeIn24 ?? "00:00")",
                            "address": "Y",
                            "notes": "\(model.notes ?? "")",
                            "calc_type": "M"
                        ]
                        KVNProgress.show()
                    }
                }
                else{
                    requestDict = ["org" : self.orgId,
                                   "prayer_code" : prayer_code,
                                   "sw_is_prayed": model.isPrayed ?? "Y",
                                   "date": AppDelegate.instance.currentDate,
                                   "time": "\(model.timeIn24 ?? "00:00")",
                        "address": "Y",
                        "notes": "\(model.notes ?? "")",
                        "calc_type": "M"
                    ]
                   KVNProgress.show()
                }
            }
            
          
        
        }else{
            iqamaTypeCal = "A"
            
            
                if model.isEdit == true{
                    
                    if model.isPrayed == "Y"{
                        if model.calculationMethod == nil{
                            AKAlertController.alert("Please fill data")
                        }else{
                    requestDict = ["org" : self.orgId,
                                   "prayer_code" : prayer_code,
                                   "sw_is_prayed": model.isPrayed ?? "Y",
                                   "date": AppDelegate.instance.currentDate,
                                   "offset": model.offestTime ?? 0,
                                   "address": "Y",
                                   "notes": "\(model.notes ?? "")",
                        "calc_type": "A",
                        "calc_method":  model.calculationMethod ?? 0,
                        "id": model.id ?? 0
                    ]
                }
                    }else{
                        requestDict = ["org" : self.orgId,
                                       "prayer_code" : prayer_code,
                                       "sw_is_prayed": model.isPrayed ?? "Y",
                                       "date": AppDelegate.instance.currentDate,
                                       "offset": model.offestTime ?? 0,
                                       "address": "Y",
                                       "notes": "\(model.notes ?? "")",
                            "calc_type": "A",
                            "calc_method":  model.calculationMethod ?? 0,
                            "id": model.id ?? 0
                        ]
                    }
                }else{
                    if model.isPrayed == "Y"{
                        if model.calculationMethod == nil{
                            AKAlertController.alert("Please fill data")
                        }else{
                            requestDict = ["org" : self.orgId,
                                           "prayer_code" : prayer_code,
                                           "sw_is_prayed": model.isPrayed ?? "Y",
                                           "date": AppDelegate.instance.currentDate,
                                           "offset": model.offestTime ?? 0,
                                           "address": "Y",
                                           "notes": "\(model.notes ?? "")",
                                "calc_type": "A",
                                "calc_method":  model.calculationMethod ?? 0
                            ]
                            KVNProgress.show()
                        }
                    }else{
                        requestDict = ["org" : self.orgId,
                                       "prayer_code" : prayer_code,
                                       "sw_is_prayed": model.isPrayed ?? "Y",
                                       "date": AppDelegate.instance.currentDate,
                                       "offset": model.offestTime ?? 0,
                                       "address": "Y",
                                       "notes": "\(model.notes ?? "")",
                            "calc_type": "A",
                            "calc_method":  model.calculationMethod ?? 0
                        ]
                         KVNProgress.show()
                    }
                  
                }
            
            
            
            }
           
            
            
          
      
        Network.shared.addIqamaForIqamah(requestDict:requestDict,isEdit:model.isEdit ?? false) { (result) in
            KVNProgress.dismiss()
            guard let user = result else{
                return
            }
            if user.success == true {
                self.showSingleAlertMessage(message: "", subMsg: user.message, sender: self, completion: { (_) in
                 
                    model.isUpdated = true
                    self.array_IqamahModel[indexPath.row] = model
                    self.tableView_AddIqamah.reloadRows(at: [indexPath], with: .fade)
                })
                
            }else{
                self.showSingleAlertMessage(message: "", subMsg: user.message, sender: self, completion: { (_) in
                })
            }
        }
    }
    
    
    @objc func btnCancelTapped(sender: UIButton) {
        let id = self.array_SpecialPrayerModel[sender.tag].id ?? 0
        if sender.titleLabel!.text! == "Clear"{
            self.array_SpecialPrayerModel[sender.tag] = SpecialPrayerDataModel()
            self.numberOfSpecialPrayerRows = self.array_SpecialPrayerModel.count
            self.tableView_AddIqamah.reloadData()
        }else{
            AKAlertController.alert("", message: "Do you want to delete this Special Prayer?".localized, buttons: ["Ok".localized,"Cancel".localized], tapBlock: { (_, _, index) in
                if index == 1 {return}
                KVNProgress.show()
                Network.shared.deleteSpecialPrayerFroIqamah(id: id) { (result) in
                    KVNProgress.dismiss()
                    guard result != nil else{return}
                    self.showSingleAlertMessage(message: "", subMsg: result?.message ?? "", sender: self, completion: { (_) in
                        self.array_SpecialPrayerModel.remove(at: sender.tag)
                        self.numberOfSpecialPrayerRows = self.array_SpecialPrayerModel.count
                        self.tableView_AddIqamah.reloadData()
                    })
                    
                }
            })
        }
    }
    
    
    @objc func btnCancelTappedForJumma(sender: UIButton) {
        let id = self.array_JummaModel[sender.tag].id ?? 0
        if sender.titleLabel!.text! == "Clear"{
            self.array_JummaModel[sender.tag] = JummaDataModel()
            self.numberOfJummaRows = self.array_JummaModel.count
            self.tableView_AddIqamah.reloadData()
        }else{
            AKAlertController.alert("", message: "Do you want to delete this Jumma?".localized, buttons: ["Ok".localized,"Cancel".localized], tapBlock: { (_, _, index) in
                if index == 1 {return}
                KVNProgress.show()
                Network.shared.deleteJummaIqamah(id: id) { (result) in
                    KVNProgress.dismiss()
                    guard result != nil else{
                        return
                    }
                    if result?.success == true{
//                        self.showSingleAlertMessage(message: "", subMsg: result?.message ?? "", sender: self, completion: { (_) in
                            self.array_JummaModel.remove(at: sender.tag)
                            self.numberOfJummaRows = self.array_JummaModel.count
                            self.tableView_AddIqamah.reloadData()
                       // })
                    }else{
                        AKAlertController.alert(result?.message ?? "")
                    }
                  
                    
                }
            })
        }
    }
    @objc func btnCancelTappedForIqamaMannual(sender: UIButton) {
        let id = self.array_IqamahModel[sender.tag].id ?? 0
        if sender.titleLabel!.text! == "Clear"{
            var model = IqamahDataModel()
            
            
            if self.array_IqamahModel[sender.tag].prayerType == "Auto"{
                model.prayerType = "Auto"
                model.cellHeight = 320
                model.calculationMethod = nil
                model.offestTime = nil
                model.time = nil
                model.timeIn24 = ""
                model.notes = ""
                model.isPrayed = "Y"
                
            }else if self.array_IqamahModel[sender.tag].prayerType == "Manual"{
                model.prayerType = "Manual"
                model.cellHeight = 290
                model.time = nil
                model.timeIn24 = ""
                model.notes = ""
                model.isPrayed = "Y"
                
            }
            self.array_IqamahModel[sender.tag] = model
          
            self.tableView_AddIqamah.reloadData()
        }else{
            AKAlertController.alert("", message: "Do you want to delete this Iqamah?".localized, buttons: ["Ok".localized,"Cancel".localized], tapBlock: { (_, _, index) in
                if index == 1 {return}
                KVNProgress.show()
                Network.shared.deleteIqamah(id: id) { (result) in
                    KVNProgress.dismiss()
                    guard result != nil else{
                        return
                    }
                    
                    self.showSingleAlertMessage(message: "", subMsg: result?.message ?? "", sender: self, completion: { (_) in
                        self.array_IqamahModel.remove(at: sender.tag)
                     self.numberOfIqamahRows = self.array_IqamahModel.count
                        self.tableView_AddIqamah.reloadData()
                    })
                    
                }
            })
        }
    }
    
}
