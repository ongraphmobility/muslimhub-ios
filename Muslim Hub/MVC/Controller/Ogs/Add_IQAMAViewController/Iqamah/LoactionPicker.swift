//
//  LoactionPicker.swift
//  Muslim Hub
//
//  Created by Himanshu Goyal on 01/07/19.
//  Copyright © 2019 OnGraph. All rights reserved.
//

import Foundation
import UIKit
import GooglePlaces
import CountryPickerViewSwift

extension IqamahViewController: GMSAutocompleteViewControllerDelegate{
    
    func setUpLocationView(){
        let autocompleteController = GMSAutocompleteViewController()
        autocompleteController.delegate = self
        let filter = GMSAutocompleteFilter()
        filter.type = .noFilter  //suitable filter type
        filter.country = "AUS"  //appropriate country code
        autocompleteController.autocompleteFilter = filter
        self.present(autocompleteController, animated: true, completion: nil)
    }
    
    // Handle the user's selection.
    func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {
        
        
        print("Place name: \(String(describing: place.name))")
        let cell: OrgsAboutUsTableCell = self.tableView_Home.cellForRow(at: index) as! OrgsAboutUsTableCell
        let location:CLLocationCoordinate2D = place.coordinate
        let latitude_str = String(format: "%.6f", location.latitude)
        let longituted_str = String(format: "%.6f", location.longitude)
        self.lat = Double(latitude_str) ?? 123.123
        self.long = Double(longituted_str) ?? 123.123
        print("Place address \(String(describing: place.formattedAddress))")
        let placeName:String = place.formattedAddress ?? ""
        self.dic_RegisterValue.updateValue(placeName, forKey: "address")
        cell.btn_Location.setTitle(placeName, for: .normal)
        
        cell.btn_Location.underlineMyText()
        tableView_Home.reloadRows(at: [index], with: .none)
        print("Place address:",place.formattedAddress!)
        dismiss(animated: true, completion: nil)
    }
    
    func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
        
        // TODO: handle the error.
        print("Error: ", error.localizedDescription)
    }
    
    // User canceled the operation.
    func wasCancelled(_ viewController: GMSAutocompleteViewController) {
        //  addressTextView.resignFirstResponder()
        dismiss(animated: true, completion: nil)
    }
    
    // Turn the network activity indicator on and off again.
    func didRequestAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
    }
    
    func didUpdateAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
    }
}
