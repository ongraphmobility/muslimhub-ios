//
//  IqamahListSubTableViewCell.swift
//  Muslim Hub
//
//  Created by Himanshu Goyal on 05/07/19.
//  Copyright © 2019 OnGraph. All rights reserved.
//

import UIKit

class IqamahListSubTableViewCell: UITableViewCell {

    @IBOutlet weak var downSeprator: UILabel!
    @IBOutlet weak var btnTime: UIButton!
    @IBOutlet weak var lbl_Location: UILabel!
    @IBOutlet weak var lbl_Notes: UILabel!
    @IBOutlet weak var lbl_Title: UILabel!
    @IBOutlet weak var btn_Location            : UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
