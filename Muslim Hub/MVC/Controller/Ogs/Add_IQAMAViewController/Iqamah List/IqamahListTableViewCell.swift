//
//  IqamahListTableViewCell.swift
//  Muslim Hub
//
//  Created by Himanshu Goyal on 05/07/19.
//  Copyright © 2019 OnGraph. All rights reserved.
//

import UIKit

func setCurrentLanguage(language: String){
    UserDefaults.standard.set(language, forKey: "kLanguage")
    UserDefaults.standard.synchronize()
}

/**
 MARK: - Get Current Language
 - Returns: Language Code
 */
func getCurrentLanguage() -> String{
    if let code = UserDefaults.standard.value(forKey: "kLanguage") as? String{
        return code
    }else{
        return ""
    }
}

protocol IqamaListCellDelegate {
    func IqamaListmoreTapped(cell: IqamahListTableViewCell)
}

class IqamahListTableViewCell: UITableViewCell {
    
    @IBOutlet weak var tableViewHeight          : NSLayoutConstraint!
    @IBOutlet weak var collectionViewHeight     : NSLayoutConstraint!
    @IBOutlet weak var lblTitle                 : UILabel!
    @IBOutlet weak var collectionView           : UICollectionView!
    @IBOutlet weak var tableView                : UITableView!
    @IBOutlet weak var btn_editIqama            : UIButton!
    @IBOutlet weak var btn_showMoreJumma           : UIButton!
    @IBOutlet weak var btn_Report            : UIButton!
    @IBOutlet weak var btn_Share            : UIButton!
    @IBOutlet weak var view_Main            : UIView!
    
    @IBOutlet weak var lbl_ShowMore           : UILabel!
    @IBOutlet weak var layout_ContarintShowmore           : NSLayoutConstraint!
    @IBOutlet weak var lbl_HeaderTitle           : UILabel!
    var isJumaOrPrayer = String()
    var orgId = Int()
    var iqamaLists : IQAMALists!
    var iqamaListsNewsFeed : IQAMAListModel!
    var indexPathRow = IndexPath()
    var islatestIqamaTime = Bool()
    var jummaCount = Int()
    var iqamaListDelegate:IqamaListCellDelegate!
    var moreCheck = Bool()
   
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        // Initialization code
        self.tableView.rowHeight = UITableView.automaticDimension
        self.tableView.estimatedRowHeight = 100
        self.collectionView.register(UINib(nibName: "IqamahListCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "IqamahListCollectionViewCell")
        self.tableView.register(UINib(nibName: "IqamahListSubTableViewCell", bundle: nil), forCellReuseIdentifier: "IqamahListSubTableViewCell")
        
     
        self.btn_Report.setTitle("Report Iqamah".localized, for: .normal)
        self.btn_Share.setTitle("Share".localized, for: .normal)
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    func cellSetUp(indexPath: IndexPath,id : Int,iqamaData:IQAMALists){
        
       // view_Main.layer.cornerRadius = 5
        self.islatestIqamaTime = false
        // shadow
//        view_Main.layer.shadowColor = UIColor.black.cgColor
//        view_Main.layer.shadowOffset = CGSize(width: 0, height: 3)
//        view_Main.layer.shadowOpacity = 0.7
//        view_Main.layer.shadowRadius = 4.0
        
        view_Main.layer.cornerRadius = 8
               view_Main.layer.maskedCorners =  [.layerMaxXMaxYCorner, .layerMinXMaxYCorner]
               
               view_Main.layer.shadowColor = UIColor.black.cgColor
               view_Main.layer.shadowOffset = CGSize(width: 0, height: 3)
               view_Main.layer.shadowOpacity = 0.4
               view_Main.layer.shadowRadius = 2.0
        self.iqamaLists = iqamaData
        self.orgId = id
        self.PermissionForEditButton(indexPath: indexPath)
        if indexPath.row == 0{
            self.lblTitle.text = "Iqamah".localized
            self.btn_Report.isHidden = false
            if (iqamaData.data?.iqama?.count)! == 0{
                self.btn_Report.isHidden = true
                self.btn_Share.isHidden = true
            }else{
                self.btn_Report.isHidden = false
                self.btn_Share.isHidden = false
            }
            self.collectionView.reloadData()
            
        }else{
            self.btn_Share.isHidden = true
            self.btn_Report.isHidden = true
            self.collectionViewHeight.constant = 0
            self.lblTitle.text = "Special Prayer".localized
        }
        
        self.indexPathRow = indexPath
        
        //        if UserDefaults.standard.bool(forKey: "isJummaShowMore") == true{
        //             self.jummaCount = (self.iqamaLists.data?.jumma?.count)!
        //            self.btn_showMoreJumma.setImage(#imageLiteral(resourceName: "show_more_iPhone"), for: .normal)
        //             self.layout_ContarintShowmore.constant = 0.0
        //        }else{
        //            self.btn_showMoreJumma.setImage(#imageLiteral(resourceName: "show_more_iPhone"), for: .normal)
        //            self.jummaCount = 0
        //            if iqamaData.data?.iqama?.count == 0{
        //               self.layout_ContarintShowmore.constant = 0.0
        //            }else{
        //              self.layout_ContarintShowmore.constant = -12.0
        //            }
        //
        //        }
        //
        
        self.tableView.reloadData()
        
        //post a notification
        
        
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "iqamaReloadData"), object: self.iqamaLists, userInfo: ["innerTableView":self.tableView.contentSize.height])
    }
    
    //    @IBAction func btn_showMore(sender:UIButton){
    //           moreCheck = true
    //
    //
    //
    //         if UserDefaults.standard.bool(forKey: "isJummaShowMore") == true && moreCheck == true{
    //          self.btn_showMoreJumma.setImage(#imageLiteral(resourceName: "show_more_iPhone"), for: .normal)
    //
    //          self.jummaCount = 0
    //            if iqamaLists.data?.iqama?.count == 0{
    //                self.layout_ContarintShowmore.constant = 0.0
    //            }else{
    //                self.layout_ContarintShowmore.constant = -30.0
    //            }
    //        UserDefaults.standard.set(false, forKey: "isJummaShowMore")
    //        }else{
    //            self.layout_ContarintShowmore.constant = 0.0
    //
    //         self.btn_showMoreJumma.setImage(#imageLiteral(resourceName: "show_more_iPhone"), for: .normal)
    //            UserDefaults.standard.set(true, forKey: "isJummaShowMore")
    //
    //             btn_showMoreJumma.isSelected = true
    //                    self.jummaCount =  self.iqamaLists.data?.jumma?.count ?? 0
    //        }
    //
    //    iqamaListDelegate?.IqamaListmoreTapped(cell: self)
    //
    //
    //    }
}



extension IqamahListTableViewCell: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout{
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if self.iqamaLists != nil{
            return self.iqamaLists.data?.iqama?.count ?? 0
        }else{
            return 0
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "IqamahListCollectionViewCell", for: indexPath) as! IqamahListCollectionViewCell
        self.tableViewHeight.constant = self.tableView.contentSize.height
        let rawData = self.iqamaLists.data?.iqama?[indexPath.row]
        cell.lblTitle.textColor = .black
        if rawData?.prayerCode == "F"{
            cell.lblTitle.text = "FAJR"
        }else if rawData?.prayerCode == "Z"{
            cell.lblTitle.text = "ZUHR"
        } else if rawData?.prayerCode == "A"{
            cell.lblTitle.text = "ASR"
        } else if rawData?.prayerCode == "M"{
            cell.lblTitle.text = "MAGHRIB"
        }else if rawData?.prayerCode == "I"{
            cell.lblTitle.text = "ISHA"
        }
        
        if rawData?.swIsPrayed == "Y"{
            if rawData?.time != nil{
               
                AppDelegate.instance.current_date()
                let startTime = rawData?.time ?? ""
                
                let time_Added = (AppDelegate.instance.currentDate + " " + startTime)
                let time_Current = (AppDelegate.instance.currentDate + " " + AppDelegate.instance.currentTime)
                
                let dateFormatter = DateFormatter()
                dateFormatter.dateFormat = "dd-MM-yyyy HH:mm"
                dateFormatter.locale = Locale(identifier: "en_US_POSIX")
                
                let starttime = dateFormatter.date(from: time_Added)!
                let currentTime = dateFormatter.date(from: time_Current)!
                               
                 
                
                let timedateFormatter1 = DateFormatter()
                timedateFormatter1.dateFormat = "hh:mm a"
                timedateFormatter1.locale = Locale(identifier: "en_US_POSIX")
                let startTime1 = timedateFormatter1.string(from: starttime)
                
                cell.btnTime.setTitle(startTime1, for: .normal)
                
                
                if islatestIqamaTime == false{
                    if starttime >= currentTime{
                        islatestIqamaTime = true
                        cell.btnTime.layer.borderWidth = 1.5
                        cell.btnTime.layer.borderColor = UIColor(named: "Yellow_Border")?.cgColor
                        cell.btnTime.backgroundColor = UIColor(named: "Dark_Green")
                        cell.lblSeprator.isHidden = false
                        cell.layout_TopDesc.constant = 3.0
                        cell.btnTime.setTitleColor(UIColor.white, for: .normal)
                        cell.lblTitle.font = UIFont(name: "Avenir-Medium", size: 14.0)
                    }else{
                        islatestIqamaTime = false
                        cell.btnTime.layer.borderWidth = 1.5
                        cell.btnTime.layer.borderColor = UIColor(named: "LightBlue_Border")?.cgColor
                        cell.btnTime.backgroundColor = UIColor.white
                        cell.lblSeprator.isHidden = true
                        cell.layout_TopDesc.constant = -4.0
                        cell.btnTime.setTitleColor(UIColor.black, for: .normal)
                        cell.lblTitle.font = UIFont(name: "Avenir-Book", size: 14.0)
                    }
                }else{
                    if islatestIqamaTime == true{
                        islatestIqamaTime = true
                    }else{
                        islatestIqamaTime = false
                    }
                    
                    cell.btnTime.layer.borderWidth = 1.5
                    cell.btnTime.layer.borderColor = UIColor(named: "LightBlue_Border")?.cgColor
                    cell.btnTime.backgroundColor = UIColor.white
                    cell.lblSeprator.isHidden = true
                    cell.layout_TopDesc.constant = -4.0
                    cell.btnTime.setTitleColor(UIColor.black, for: .normal)
                    cell.lblTitle.font = UIFont(name: "Avenir-Book", size: 14.0)
                }
                
                
                
            }else{
                cell.btnTime.setTitle("" , for: .normal)
            }
            
        }else{
            
            cell.btnTime.layer.borderWidth = 1.5
            cell.btnTime.layer.borderColor = UIColor(named: "LightBlue_Border")?.cgColor
            cell.btnTime.backgroundColor = UIColor.white
            cell.lblSeprator.isHidden = true
            cell.layout_TopDesc.constant = -4.0
            cell.btnTime.setTitleColor(UIColor.black, for: .normal)
            cell.lblTitle.font = UIFont(name: "Avenir-Book", size: 14.0)
            cell.btnTime.setTitle("Not prayed", for: .normal)
        }
        
        cell.btnTime.layer.cornerRadius = 8
        cell.btnTime.layer.shadowColor = UIColor.darkGray.cgColor
        cell.btnTime.layer.shadowOffset = CGSize(width: 0, height: 3)
        cell.btnTime.layer.shadowOpacity = 0.5
        cell.btnTime.layer.shadowRadius = 2.0
        //        var isSelected: Bool = false
        //        if indexPath.item == 0{
        //            isSelected = true
        //        }else{
        //            isSelected = false
        //        }
        //        if isSelected{
        //            cell.btnTime.layer.borderWidth = 1.5
        //            cell.btnTime.layer.borderColor = UIColor(named: "Yellow_Border")?.cgColor
        //            cell.btnTime.backgroundColor = UIColor(named: "Dark_Green")
        //            cell.lblSeprator.isHidden = false
        //            cell.btnTime.setTitleColor(UIColor.white, for: .normal)
        //            cell.lblTitle.font = UIFont(name: "Avenir-Medium", size: 14.0)
        //        }else{
        //            cell.btnTime.layer.borderWidth = 1.5
        //            cell.btnTime.layer.borderColor = UIColor(named: "LightBlue_Border")?.cgColor
        //            cell.btnTime.backgroundColor = UIColor.white
        //            cell.lblSeprator.isHidden = true
        //            cell.btnTime.setTitleColor(UIColor.black, for: .normal)
        //            cell.lblTitle.font = UIFont(name: "Avenir-Book", size: 14.0)
        //        }
        
        cell.lblDec.text = rawData?.notes ?? ""
      
    
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.bounds.size.width/4.5 , height: 130)
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        
        return 0.0
        
    }
    
}

extension IqamahListTableViewCell: UITableViewDelegate, UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if self.indexPathRow.row == 0{
            if self.iqamaLists != nil{
                // return jummaCount
                return self.iqamaLists.data?.jumma?.count ?? 0
            }else{
                return 0
            }
        }else{
            if self.iqamaLists != nil{
                return self.iqamaLists.data?.specialPrayers?.count ?? 0
            }else{
                return 0
            }
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "IqamahListSubTableViewCell", for: indexPath) as! IqamahListSubTableViewCell
        self.tableViewHeight.constant = self.tableView.contentSize.height
        print("tableView Inside contensize",self.tableView.contentSize.height)
        
        cell.btnTime.layer.cornerRadius = 8
        cell.btnTime.layer.shadowColor = UIColor.darkGray.cgColor
        cell.btnTime.layer.shadowOffset = CGSize(width: 0, height: 3)
        cell.btnTime.layer.shadowOpacity = 0.5
        cell.btnTime.layer.shadowRadius = 2.0
        cell.btnTime.layer.borderWidth = 1.5
        cell.btnTime.layer.borderColor = UIColor(named: "LightBlue_Border")?.cgColor
        cell.btnTime.backgroundColor = UIColor.white
        cell.btnTime.setTitleColor(UIColor.black, for: .normal)
        
        if self.indexPathRow.row == 0{
            isJumaOrPrayer = "jumma"
            let rawdataJumma = self.iqamaLists.data?.jumma?[indexPath.row]
            cell.btn_Location.tag = indexPath.row
            cell.btn_Location.addTarget(self, action: #selector(btn_Location(sender:)), for: .touchUpInside)
            cell.lbl_Location.text = rawdataJumma?.address ?? ""
            cell.lbl_Notes.text = rawdataJumma?.notes ?? ""
            if rawdataJumma?.type == 1{
                cell.lbl_Title.text = "JUMMA1"
            }else if rawdataJumma?.type == 2{
                cell.lbl_Title.text = "JUMMA2"
            }else if rawdataJumma?.type == 3{
                cell.lbl_Title.text = "JUMMA3"
            }else if rawdataJumma?.type == 4{
                cell.lbl_Title.text = "JUMMA4"
            }else if rawdataJumma?.type == 5{
                cell.lbl_Title.text = "JUMMA5"
            }else if rawdataJumma?.type == 6{
                cell.lbl_Title.text = "JUMMA6"
            }else if rawdataJumma?.type == 7{
                cell.lbl_Title.text = "JUMMA7"
            }else if rawdataJumma?.type == 8{
                cell.lbl_Title.text = "JUMMA8"
            }else if rawdataJumma?.type == 9{
                cell.lbl_Title.text = "JUMMA9"
            }else if rawdataJumma?.type == 10{
                cell.lbl_Title.text = "JUMMA10"
            }else{
                cell.lbl_Title.text = "JUMMA1"
            }
            let timedateFormatter = DateFormatter()
            timedateFormatter.dateFormat = "HH:mm"
            let starttime = timedateFormatter.date(from: rawdataJumma?.time ?? "")
            let timedateFormatter1 = DateFormatter()
            timedateFormatter1.dateFormat = "hh:mm a"
            let startTime1 = timedateFormatter1.string(from: starttime ?? Date())
            cell.btnTime.setTitle(startTime1, for: .normal)
        }else{
            isJumaOrPrayer = "SpecialPrayer"
            let rawdataSpecialPrayer = self.iqamaLists.data?.specialPrayers?[indexPath.row]
            
            cell.btn_Location.tag = indexPath.row
            cell.btn_Location.addTarget(self, action: #selector(btn_Location(sender:)), for: .touchUpInside)
            cell.lbl_Location.text = rawdataSpecialPrayer?.address ?? ""
            cell.lbl_Notes.text = rawdataSpecialPrayer?.specialPrayerDescription ?? ""
            cell.lbl_Title.text = rawdataSpecialPrayer?.name ?? ""
            let timedateFormatter = DateFormatter()
            timedateFormatter.dateFormat = "HH:mm"
            let starttime = timedateFormatter.date(from: rawdataSpecialPrayer?.time ?? "")
            let timedateFormatter1 = DateFormatter()
            timedateFormatter1.dateFormat = "hh:mm a"
            let startTime1 = timedateFormatter1.string(from: starttime ?? Date())
            cell.btnTime.setTitle(startTime1, for: .normal)
        }
        
        //                if (self.iqamaLists.data?.jumma!.count)! - 1 == indexPath.row{
        //
        //                    if getCurrentLanguage() == "" || getCurrentLanguage() != "reload"{
        //                        setCurrentLanguage(language: "reload")
        ////                        self.tableView.contentSize.height = 100
        //                        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "iqamaReloadData"), object: self.iqamaLists, userInfo: ["innerTableView":self.tableView.contentSize.height])
        //
        //                    }
        //
        //                }else if (self.iqamaLists.data?.specialPrayers!.count)! - 1 == indexPath.row{
        //                    if getCurrentLanguage() == "" || getCurrentLanguage() != "reload"{
        //                        setCurrentLanguage(language: "reload")
        //                        //                        self.tableView.contentSize.height = 100
        //                        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "iqamaReloadData"), object: self.iqamaLists, userInfo: ["innerTableView":self.tableView.contentSize.height])
        //
        //                    }
        //                }
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func PermissionForEditButton(indexPath:IndexPath){
        if (UserDefaults.standard.value(forKey: "Login_Token") as? String) != nil{
            
            if let superAdmin = UserDefaults.standard.value(forKey: "isSuperAdmin") as? Bool{
                
                if superAdmin == true{
                    if indexPath.row == 0{
                        if self.iqamaLists.data?.iqama?.count != 0{
                            self.collectionViewHeight.constant = 130
                        }else{
                            self.collectionViewHeight.constant = 0
                        }
                        if self.iqamaLists.data?.jumma?.count != 0{
                            
                            self.tableViewHeight.constant = self.tableView.contentSize.height
                            
                        }else{
                            self.tableViewHeight.constant = 0
                        }
                    }else{
                        if (self.iqamaLists.data?.specialPrayers?.count != 0 ){
                            self.btn_editIqama.isHidden = false
                        }else{
                            self.btn_editIqama.isHidden = true
                        }
                    }
                    
                    
                    
                }else{
                    
                    if let is_staff = UserDefaults.standard.value(forKey: "is_staff") as? Bool{
                        
                        if is_staff == true{
                            //mark:check For own Organisation---------------------------
                            if let orgOwn_Array = UserDefaults.standard.value(forKey: "OwnOrg_Array") as? [Int]{
                                
                                
                                if orgOwn_Array.contains(orgId){
                                    var org_P = [PriviligesData]()
                                    org_P = Global.shared.priviligesData
                                    let filertedData = org_P.filter{$0.org_id == orgId}
                                    filertedData.forEach {_ in 
                                        
                                        if indexPath.row == 0{
                                            if self.iqamaLists.data?.iqama?.count != 0{
                                                self.collectionViewHeight.constant = 130
                                            }else{
                                                self.collectionViewHeight.constant = 0
                                            }
                                            if self.iqamaLists.data?.jumma?.count != 0{
                                                self.tableViewHeight.constant = 181
                                            }else{
                                                self.tableViewHeight.constant = 0
                                            }
                                            self.btn_editIqama.isHidden = false
                                        }else{
                                            if (self.iqamaLists.data?.specialPrayers?.count != 0 ){
                                                self.btn_editIqama.isHidden = false
                                            }else{
                                                self.btn_editIqama.isHidden = true
                                            }
                                        }
                                        
                                    }
                                    
                                    
                                }else{
                                    
                                    if indexPath.row == 0{
                                        if self.iqamaLists.data?.iqama?.count != 0{
                                            self.collectionViewHeight.constant = 130
                                        }else{
                                            self.collectionViewHeight.constant = 0
                                        }
                                        if self.iqamaLists.data?.jumma?.count != 0{
                                            self.tableViewHeight.constant = 181
                                        }else{
                                            self.tableViewHeight.constant = 0
                                        }
                                        self.btn_editIqama.isHidden = true
                                    }else{
                                        if (self.iqamaLists.data?.specialPrayers?.count != 0 ){
                                            self.btn_editIqama.isHidden = true
                                        }else{
                                            self.btn_editIqama.isHidden = true
                                        }
                                    }
                                    self.btn_editIqama.isHidden = true
                                }//orgOwn
                                
                            }
                        }//staff true
                    }//staff
                    
                }
            }//SuperAdmin
        }//Login
        else{
            if indexPath.row == 0{
                if self.iqamaLists.data?.iqama?.count != 0{
                    self.collectionViewHeight.constant = 130
                }else{
                    self.collectionViewHeight.constant = 0
                }
                if self.iqamaLists.data?.jumma?.count != 0{
                    
                    self.tableViewHeight.constant = self.tableView.contentSize.height
                    
                }else{
                    self.tableViewHeight.constant = 0
                }
            }else{
                if (self.iqamaLists.data?.specialPrayers?.count != 0 ){
                    self.btn_editIqama.isHidden = false
                }else{
                    self.btn_editIqama.isHidden = true
                }
            }
            self.btn_editIqama.isHidden = true
            
        }
        
        
        
    }
    
    @objc func btn_Location(sender:UIButton){
        var lat = Double()
        var long = Double()
        if isJumaOrPrayer == "SpecialPrayer"{
            lat = self.iqamaLists?.data?.specialPrayers?[sender.tag].latitude ?? 123.123
            long = self.iqamaLists?.data?.specialPrayers?[sender.tag].longitude ?? 123.123
        }else{
            lat = self.iqamaLists?.data?.jumma?[sender.tag].latitude ?? 123.123
            long = self.iqamaLists?.data?.jumma?[sender.tag].longitude ?? 123.123
        }
        
        self.locationCell(lat: lat, long: long, title: self.iqamaLists?.data?.jumma?[sender.tag].address ?? "" )
        
    }
    
}
