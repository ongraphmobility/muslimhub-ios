//
//  NewsFeedIqamTableViewCell.swift
//  Muslim Hub
//
//  Created by Ongraph on 25/07/19.
//  Copyright © 2019 OnGraph. All rights reserved.
//

import UIKit
protocol NewsIqamaListCellDelegate {
    func  NewsIqamaListmoreTapped(cell: NewsFeedIqamTableViewCell)
}
class NewsFeedIqamTableViewCell: UITableViewCell {
    
    @IBOutlet weak var tableViewHeight          : NSLayoutConstraint!
    @IBOutlet weak var collectionViewHeight     : NSLayoutConstraint!
    @IBOutlet weak var lblTitle                 : UILabel!
    @IBOutlet weak var collectionView           : UICollectionView!
    @IBOutlet weak var tableView                : UITableView!
    @IBOutlet var tableView_Bottom                : NSLayoutConstraint!
    @IBOutlet weak var btn_editIqama            : UIButton!
    @IBOutlet weak var btn_Report            : UIButton!
    @IBOutlet weak var btn_Share         : UIButton!
    @IBOutlet weak var view_Main            : UIView!
     @IBOutlet weak var view_SecondMain            : UIView!
    @IBOutlet weak var btn_showMoreJumma           : UIButton!
    @IBOutlet weak var lbl_ShowMore           : UILabel!
    @IBOutlet weak var layout_ContarintShowmore           : NSLayoutConstraint!
    
    
      @IBOutlet weak var lbl_HeaderTitle           : UILabel!
    
    var isJumaOrPrayer = String()
    var orgId = Int()
    var iqamaListsNewsFeed : IQAMAListModel!
    var indexPathRow = IndexPath()
    var islatestIqamaTime = Bool()
    var jummaCount = Int()
    var newsIqamaListDelegate:NewsIqamaListCellDelegate!
    var moreCheck = Bool()
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        self.tableView.rowHeight = UITableView.automaticDimension
        self.tableView.estimatedRowHeight = 150
        self.backgroundColor = UIColor.clear
        self.collectionView.register(UINib(nibName: "IqamahListCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "IqamahListCollectionViewCell")
        self.tableView.register(UINib(nibName: "IqamahListSubTableViewCell", bundle: nil), forCellReuseIdentifier: "IqamahListSubTableViewCell")
        
     
        self.btn_Report.setTitle("Report Iqamah".localized, for: .normal)
        self.btn_Share.setTitle("Share".localized, for: .normal)
        self.lbl_ShowMore.text = "Jumma +".localized
        //self.btn_showMoreJumma.setTitle("Show more".localized, for: .normal)
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    func cellSetUp(indexPath: IndexPath,id : Int,iqamaData:IQAMAListModel){
       
        self.islatestIqamaTime = false
        self.tableView_Bottom.isActive = true
        // shadow
//        view_Main.layer.shadowColor = UIColor.black.cgColor
//        view_Main.layer.shadowOffset = CGSize(width: 0, height: 3)
//        view_Main.layer.shadowOpacity = 0.7
//        view_Main.layer.shadowRadius = 4.0
       
        view_SecondMain.layer.cornerRadius = 8
                      view_SecondMain.layer.maskedCorners =  [.layerMaxXMaxYCorner, .layerMinXMaxYCorner]

                      view_SecondMain.layer.shadowColor = UIColor.black.cgColor
                      view_SecondMain.layer.shadowOffset = CGSize(width: 0, height: 3)
                      view_SecondMain.layer.shadowOpacity = 0.4
                      view_SecondMain.layer.shadowRadius = 2.0
        self.iqamaListsNewsFeed = iqamaData
        self.orgId = id
        self.PermissionForEditButton(indexPath: indexPath)
        if indexPath.row == 0{
            self.lblTitle.text = "Iqamah".localized
            if (iqamaData.iqama?.count)! == 0{
                self.btn_Report.isHidden = true
            }else{
                self.btn_Report.isHidden = false
            }
            
        }
        if iqamaData.jumma?.count == 0{
            self.lbl_ShowMore.isHidden = true
            self.btn_showMoreJumma.isHidden = true
            if tableView_Bottom != nil{
                tableView_Bottom.isActive = true
            }
            
        }else{
            if tableView_Bottom != nil{
                tableView_Bottom.isActive = false
            }
            
            self.lbl_ShowMore.isHidden = false
            self.btn_showMoreJumma.isHidden = false
        }
        if UserDefaults.standard.bool(forKey: "isNewsJummaShowMore") == true{
            self.jummaCount = (self.iqamaListsNewsFeed.jumma!.count)
            self.btn_showMoreJumma.setImage(#imageLiteral(resourceName: "show_less_iPhone"), for: .normal)
            
            self.layout_ContarintShowmore.constant = -9.0
        }else{
            self.btn_showMoreJumma.setImage(#imageLiteral(resourceName: "show_more_iPhone"), for: .normal)
            self.jummaCount = 0
            if iqamaData.iqama?.count == 0{
                self.layout_ContarintShowmore.constant = 0.0
            }else{
                self.layout_ContarintShowmore.constant = -12.0
            }
            
        }
        self.indexPathRow = indexPath
        
        self.collectionView.reloadData()
        self.tableView.reloadData()
        
        
        
        //post a notification
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "iqamaNewsFeedReloadData"), object: self.iqamaListsNewsFeed, userInfo: ["innerTableView":self.tableView.contentSize.height])
    }
    @IBAction func btn_showMore(sender:UIButton){
        moreCheck = true
        if UserDefaults.standard.bool(forKey: "isNewsJummaShowMore") == true && moreCheck == true{
            
            self.btn_showMoreJumma.setImage(#imageLiteral(resourceName: "show_more_iPhone"), for: .normal)
            if iqamaListsNewsFeed.iqama?.count == 0{
                self.layout_ContarintShowmore.constant = 0.0
            }else{
                self.layout_ContarintShowmore.constant = -30.0
            }
            self.jummaCount = 0
            UserDefaults.standard.set(false, forKey: "isNewsJummaShowMore")
            
        }else{
            
            self.layout_ContarintShowmore.constant = -9.0
            UserDefaults.standard.set(true, forKey: "isNewsJummaShowMore")
            
            self.btn_showMoreJumma.setImage(#imageLiteral(resourceName: "show_less_iPhone"), for: .normal)
            btn_showMoreJumma.isSelected = true
            self.jummaCount =  self.iqamaListsNewsFeed.jumma!.count
        }
        
        newsIqamaListDelegate?.NewsIqamaListmoreTapped(cell: self)
        
        
    }
}
extension NewsFeedIqamTableViewCell: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout{
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if self.iqamaListsNewsFeed != nil{
            return  self.iqamaListsNewsFeed.iqama?.count ?? 0
        }else{
            return 0
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "IqamahListCollectionViewCell", for: indexPath) as! IqamahListCollectionViewCell
        self.tableViewHeight.constant = self.tableView.contentSize.height
        let rawData = self.iqamaListsNewsFeed.iqama?[indexPath.row]
        cell.lblTitle.textColor = .black
        if rawData?.prayerCode == "F"{
            cell.lblTitle.text = "FAJR"
        }else if rawData?.prayerCode == "Z"{
            cell.lblTitle.text = "ZUHR"
        } else if rawData?.prayerCode == "A"{
            cell.lblTitle.text = "ASR"
        } else if rawData?.prayerCode == "M"{
            cell.lblTitle.text = "MAGHRIB"
        }else if rawData?.prayerCode == "I"{
            cell.lblTitle.text = "ISHA"
        }
        
        if rawData?.swIsPrayed == "Y"{
            if rawData?.time != nil{
                
                AppDelegate.instance.current_date()
                let startTime = rawData?.time ?? ""
                
                let time_Added = (AppDelegate.instance.currentDate + " " + startTime)
                let time_Current = (AppDelegate.instance.currentDate + " " + AppDelegate.instance.currentTime)
                
                let dateFormatter = DateFormatter()
                dateFormatter.dateFormat = "dd-MM-yyyy HH:mm"
                dateFormatter.locale = Locale(identifier: "en_US_POSIX")
                let starttime = dateFormatter.date(from: time_Added)!
                let currentTime = dateFormatter.date(from: time_Current)!
                
                
                
                let timedateFormatter1 = DateFormatter()
                timedateFormatter1.dateFormat = "hh:mm a"
                  timedateFormatter1.locale = Locale(identifier: "en_US_POSIX")
                let startTime1 = timedateFormatter1.string(from: starttime)
                
                cell.btnTime.setTitle(startTime1, for: .normal)
                
                
                if islatestIqamaTime == false{
                    if starttime >= currentTime{
                        islatestIqamaTime = true
                        cell.btnTime.layer.borderWidth = 1.5
                        cell.btnTime.layer.borderColor = UIColor(named: "Yellow_Border")?.cgColor
                        cell.btnTime.backgroundColor = UIColor(named: "Dark_Green")
                        cell.lblSeprator.isHidden = false
                        cell.layout_TopDesc.constant = 3.0
                        cell.btnTime.setTitleColor(UIColor.white, for: .normal)
                        cell.lblTitle.font = UIFont(name: "Avenir-Medium", size: 14.0)
                    }else{
                        islatestIqamaTime = false
                        cell.btnTime.layer.borderWidth = 1.5
                        cell.btnTime.layer.borderColor = UIColor(named: "LightBlue_Border")?.cgColor
                        cell.btnTime.backgroundColor = UIColor.white
                        cell.lblSeprator.isHidden = true
                        cell.layout_TopDesc.constant = -4.0
                        cell.btnTime.setTitleColor(UIColor.black, for: .normal)
                        cell.lblTitle.font = UIFont(name: "Avenir-Book", size: 14.0)
                    }
                }else{
                    
                    if islatestIqamaTime == true{
                        islatestIqamaTime = true
                    }else{
                        islatestIqamaTime = false
                    }
                    
                    cell.btnTime.layer.borderWidth = 1.5
                    cell.btnTime.layer.borderColor = UIColor(named: "LightBlue_Border")?.cgColor
                    cell.btnTime.backgroundColor = UIColor.white
                    cell.lblSeprator.isHidden = true
                    cell.layout_TopDesc.constant = -4.0
                    cell.btnTime.setTitleColor(UIColor.black, for: .normal)
                    cell.lblTitle.font = UIFont(name: "Avenir-Book", size: 14.0)
                }
                
                
                
            }else{
                cell.btnTime.setTitle("" , for: .normal)
            }
            
        }else{
            cell.btnTime.layer.borderWidth = 1.5
            cell.btnTime.layer.borderColor = UIColor(named: "LightBlue_Border")?.cgColor
            cell.btnTime.backgroundColor = UIColor.white
            cell.lblSeprator.isHidden = true
            cell.layout_TopDesc.constant = -4.0
            cell.btnTime.setTitleColor(UIColor.black, for: .normal)
            cell.lblTitle.font = UIFont(name: "Avenir-Book", size: 14.0)
            cell.btnTime.setTitle("Not prayed", for: .normal)
        }
        
        cell.btnTime.layer.cornerRadius = 8
        cell.btnTime.layer.shadowColor = UIColor.darkGray.cgColor
        cell.btnTime.layer.shadowOffset = CGSize(width: 0, height: 3)
        cell.btnTime.layer.shadowOpacity = 0.5
        cell.btnTime.layer.shadowRadius = 2.0
        
        cell.lblDec.text = rawData?.notes ?? ""
        
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.bounds.size.width/4.5 , height: 130)
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        
        return 0.0
        
    }
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
    }
    
}

extension NewsFeedIqamTableViewCell: UITableViewDelegate, UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.iqamaListsNewsFeed != nil{
            return jummaCount
        }else{
            return 0
        }
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "IqamahListSubTableViewCell", for: indexPath) as! IqamahListSubTableViewCell
        self.tableViewHeight.constant = self.tableView.contentSize.height
        print("tableView Inside contensize",self.tableView.contentSize.height)
        cell.btnTime.layer.cornerRadius = 8
        cell.btnTime.layer.shadowColor = UIColor.darkGray.cgColor
        cell.btnTime.layer.shadowOffset = CGSize(width: 0, height: 3)
        cell.btnTime.layer.shadowOpacity = 0.5
        cell.btnTime.layer.shadowRadius = 2.0
        cell.btnTime.layer.borderWidth = 1.5
        cell.btnTime.layer.borderColor = UIColor(named: "LightBlue_Border")?.cgColor
        cell.btnTime.backgroundColor = UIColor.white
        cell.btnTime.setTitleColor(UIColor.black, for: .normal)
        
        if self.indexPathRow.row == 0{
            isJumaOrPrayer = "jumma"
            let rawdataJumma = self.iqamaListsNewsFeed.jumma?[indexPath.row]
            cell.btn_Location.tag = indexPath.row
            cell.btn_Location.addTarget(self, action: #selector(btn_Location(sender:)), for: .touchUpInside)
            cell.lbl_Location.text = rawdataJumma?.address ?? ""
            cell.lbl_Notes.text = rawdataJumma?.notes ?? ""
            if rawdataJumma?.type == 1{
                cell.lbl_Title.text = "JUMMA1"
            }else if rawdataJumma?.type == 2{
                cell.lbl_Title.text = "JUMMA2"
            }else if rawdataJumma?.type == 3{
                cell.lbl_Title.text = "JUMMA3"
            }else if rawdataJumma?.type == 4{
                cell.lbl_Title.text = "JUMMA4"
            }else if rawdataJumma?.type == 5{
                cell.lbl_Title.text = "JUMMA5"
            }else if rawdataJumma?.type == 6{
                cell.lbl_Title.text = "JUMMA6"
            }else if rawdataJumma?.type == 7{
                cell.lbl_Title.text = "JUMMA7"
            }else if rawdataJumma?.type == 8{
                cell.lbl_Title.text = "JUMMA8"
            }else if rawdataJumma?.type == 9{
                cell.lbl_Title.text = "JUMMA9"
            }else if rawdataJumma?.type == 10{
                cell.lbl_Title.text = "JUMMA10"
            }else{
                cell.lbl_Title.text = "JUMMA1"
            }
            let timedateFormatter = DateFormatter()
            timedateFormatter.dateFormat = "HH:mm"
            let starttime = timedateFormatter.date(from: rawdataJumma?.time ?? "")
            let timedateFormatter1 = DateFormatter()
            timedateFormatter1.dateFormat = "hh:mm a"
            let startTime1 = timedateFormatter1.string(from: starttime ?? Date())
            cell.btnTime.setTitle(startTime1, for: .normal)
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }
    
    func PermissionForEditButton(indexPath:IndexPath){
        if (UserDefaults.standard.value(forKey: "Login_Token") as? String) != nil{
            
            if let superAdmin = UserDefaults.standard.value(forKey: "isSuperAdmin") as? Bool{
                
                if superAdmin == true{
                    if indexPath.row == 0{
                        if self.iqamaListsNewsFeed?.iqama?.count != 0{
                            self.collectionViewHeight.constant = 130
                        }else{
                            self.collectionViewHeight.constant = 0
                            
                        }
                        if self.iqamaListsNewsFeed?.jumma?.count != 0{
                            
                            self.tableViewHeight.constant = 181
                        }else{
                            self.tableViewHeight.constant = 0
                            
                        }
                        
                        
                    }
                    
                    
                    
                }else{
                    
                    if let is_staff = UserDefaults.standard.value(forKey: "is_staff") as? Bool{
                        
                        if is_staff == true{
                            //mark:check For own Organisation---------------------------
                            if let orgOwn_Array = UserDefaults.standard.value(forKey: "OwnOrg_Array") as? [Int]{
                                
                                
                                if orgOwn_Array.contains(orgId){
                                    var org_P = [PriviligesData]()
                                    org_P = Global.shared.priviligesData
                                    let filertedData = org_P.filter{$0.org_id == orgId}
                                    filertedData.forEach {_ in
                                        
                                        
                                        self.btn_editIqama.isHidden = false
                                        if indexPath.row == 0{
                                            if self.iqamaListsNewsFeed?.iqama?.count != 0{
                                                self.collectionViewHeight.constant = 130
                                            }else{
                                                self.collectionViewHeight.constant = 0
                                            }
                                            if self.iqamaListsNewsFeed?.jumma?.count != 0{
                                                self.tableViewHeight.constant = 181
                                            }else{
                                                self.tableViewHeight.constant = 0
                                            }
                                        }
                                    }
                                    
                                    
                                }else{
                                    if indexPath.row == 0{
                                        if self.iqamaListsNewsFeed?.iqama?.count != 0{
                                            self.collectionViewHeight.constant = 130
                                        }else{
                                            self.collectionViewHeight.constant = 0
                                        }
                                        if self.iqamaListsNewsFeed?.jumma?.count != 0{
                                            self.tableViewHeight.constant = 181
                                        }else{
                                            self.tableViewHeight.constant = 0
                                        }
                                    }
                                    
                                    self.btn_editIqama.isHidden = true
                                }//orgOwn
                                
                            }
                        }//staff true
                    }//staff
                    
                }
            }//SuperAdmin
        }//Login
        else{
            if indexPath.row == 0{
                if self.iqamaListsNewsFeed?.iqama?.count != 0{
                    self.collectionViewHeight.constant = 130
                }else{
                    self.collectionViewHeight.constant = 0
                    
                }
                if self.iqamaListsNewsFeed?.jumma?.count != 0{
                    
                    self.tableViewHeight.constant = 181
                }else{
                    self.tableViewHeight.constant = 0
                    
                }
                
                
            }
            self.btn_editIqama.isHidden = true
            
        }
        
        
        
    }
    
    @objc func btn_Location(sender:UIButton){
        var lat = Double()
        var long = Double()
        
        lat = self.iqamaListsNewsFeed?.jumma?[sender.tag].latitude ?? 123.123
        long = self.iqamaListsNewsFeed?.jumma?[sender.tag].longitude ?? 123.123
        
        self.locationCell(lat: lat, long: long, title: self.iqamaListsNewsFeed?.jumma?[sender.tag].address ?? "" )
        
        
        
    }
    
}
