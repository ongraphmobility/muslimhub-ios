//
//  IqamahListCollectionViewCell.swift
//  Muslim Hub
//
//  Created by Himanshu Goyal on 05/07/19.
//  Copyright © 2019 OnGraph. All rights reserved.
//

import UIKit

class IqamahListCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var lblTitle     : UILabel!
    @IBOutlet weak var btnTime      : UIButton!
    @IBOutlet weak var lblSeprator  : UILabel!
    @IBOutlet weak var lblDec  : UILabel!
    @IBOutlet weak var layout_TopDesc  : NSLayoutConstraint!
    @IBOutlet weak var lblTime: UILabel!
    @IBOutlet weak var viewParent: UIView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    @IBAction func btnTimeTapped(_ sender: Any) {
    }
}
