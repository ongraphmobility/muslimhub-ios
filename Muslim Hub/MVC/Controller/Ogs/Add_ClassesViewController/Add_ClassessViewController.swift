//
//  Add_ClassessViewController.swift
//  Muslim Hub
//
//  Created by Ongraph on 31/05/19.
//  Copyright © 2019 OnGraph. All rights reserved.
//

import UIKit
import SDWebImage
import GooglePlaces
import YangMingShan
import KVNProgress

class Add_ClassessViewController: UIViewController,UITextFieldDelegate,UITextViewDelegate {
    
    //Mark:IBOUTLets----------------------------------------------------
    
    //mark:Outlets for Image_Upload-------------------------------------
    @IBOutlet weak var btn_Cancel:UIButton!
    @IBOutlet weak var btn_Save:UIButton!
    @IBOutlet weak var btn_Upload:UIButton!
    @IBOutlet weak var view_UploadPhoto:UIView!
    @IBOutlet weak var collectionView_Images:UICollectionView!
    @IBOutlet weak var view_UpperTableView:UIView!
    @IBOutlet weak var view_LowerTableView:UIView!
    @IBOutlet weak var lbl_NoImageFound:UILabel!
    @IBOutlet weak var lbl_Header:UILabel!
      @IBOutlet weak var lbl_UploadImages:UILabel!
      @IBOutlet weak var lbl_Max6:UILabel!
    
    var imageData = [Data]()
    var arrayImages = [UIImage]()
    var imageArray: [Any] = []
    var orgId = Int()
    //mark:Outlets for Form-------------------------------------
    @IBOutlet weak var tableView_AddEvents:UITableView!
    var lat = Double()
    var long = Double()
    var dic_RegisterValue = [String:Any]()
    var tag_forAddress:Int!
    var addNewClasses:String!
    var addClassesDetails:GetClassData!
    var startDate = Date()
    var endate = Date()
    
    var startTime = Date()
    var endatTime = Date()
    
    var edit = ""
    var str_Time12hrs = ""
    
    var computedCurrentDate : Date {
            let dat = MHHelper.shared.convertToDateFrom(date: Date())
            return dat
        }
    
    //mark:Array of variables-------------------------------------------
    let array_StartTime = ["Select Class","Start Time","End Time"]
    let array_StartDate = ["Select Class Type","Start Date","End Date"]
    let array_RepeatingDay = ["Weekly","Fortnightly","Monthly"]
    let array_SectionLast_Placeholder = ["Attendess Type","Age Group","Repeating Day","Teacher Name","Free"]
    let array_Attendene = ["Men Only","Women Only","Family"]
    let array_SelectClassType = ["Select Predefined","Custom"]
    let array_SelectClass = ["Quran & Tajweed","Seerah & Hadith","Islamic Studies","Arabic learning","Sports","Academic","Language"]
    let array_AllData = ["Title","Description","Location","Select_Class_Type","Select_Type","Start_Date","Start_Time","End_Date","End_Time","Attendess_Type","Age_Group","Repeating_Day","Teacher_Type","Free","user_Input"]
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
        self.btn_Cancel.setTitle("Cancel".localized, for: .normal)
            self.btn_Save.setTitle("Save".localized, for: .normal)
            self.lbl_Header.text = "Add Classes".localized
            self.lbl_UploadImages.text = "Upload Images".localized
            self.lbl_Max6.text = "6 Images Maximum".localized
        
        statusBarColorChange(color:UIColor.init(hex: 0x05BDB9))
        
        //MARK:Condition for Edit Classes-------------------------------------------
        if addClassesDetails != nil{
            if addClassesDetails.latitude != nil{
                
                self.lat = addClassesDetails.latitude?.rounded(toPlaces: 6) ?? 0.0
            }
            if addClassesDetails.longitude != nil{
                
                self.long = addClassesDetails.longitude?.rounded(toPlaces: 6) ?? 0.0
            }
            
            
            if addClassesDetails.images?.count != 0{
                
                collectionView_Images.isHidden = false
                self.view_UploadPhoto.isHidden = true
                self.lbl_NoImageFound.isHidden = true
                
            }else{
                collectionView_Images.isHidden = true
                self.view_UploadPhoto.isHidden = true
                self.lbl_NoImageFound.isHidden = false
            }
            if addClassesDetails.title != nil{
                dic_RegisterValue.updateValue(addClassesDetails.title!, forKey: array_AllData[0])
            }
            if addClassesDetails.description != nil{
                dic_RegisterValue.updateValue(addClassesDetails.description!, forKey: array_AllData[1])
            }
            if addClassesDetails.address != nil{
                dic_RegisterValue.updateValue(addClassesDetails.address!, forKey: array_AllData[2])
            }
            if addClassesDetails.class_input_type != nil{
                dic_RegisterValue.updateValue(addClassesDetails.class_input_type!, forKey: array_AllData[3])
            }
            if addClassesDetails.select_class != nil{
                
                if (addClassesDetails.select_class == 1 || addClassesDetails.select_class == 2 || addClassesDetails.select_class == 3 || addClassesDetails.select_class == 4 || addClassesDetails.select_class == 5){
                    dic_RegisterValue.updateValue(addClassesDetails.select_class!, forKey: array_AllData[4])
                }else{
                    dic_RegisterValue.updateValue("", forKey: array_AllData[4])
                }
                
                
            }
            if addClassesDetails.user_input != nil{
                dic_RegisterValue.updateValue(addClassesDetails.user_input!, forKey: "user_Input")
            }else{
                dic_RegisterValue.updateValue("", forKey: "user_Input")
            }
            if addClassesDetails.start_date != nil{
                
                
                
                dic_RegisterValue.updateValue(addClassesDetails.start_date!, forKey: array_AllData[5])
                
                
            }
            
            if addClassesDetails.start_time != nil{
                
                dic_RegisterValue.updateValue(addClassesDetails.start_time!, forKey: array_AllData[6])
            }
            if addClassesDetails.end_date != nil{
                
                let dateString = addClassesDetails.end_date
                let dateFormatter = DateFormatter()
                dateFormatter.dateFormat = "dd-MM-yyyy"
                dateFormatter.timeZone = TimeZone(identifier: "UTC")
                dateFormatter.locale = Locale(identifier: "en_US_POSIX")
                let date = dateFormatter.date(from: dateString!)
                print(date!)
                
                self.endate = date!
          
                dic_RegisterValue.updateValue(addClassesDetails.end_date!, forKey: array_AllData[7])
            }
            if addClassesDetails.end_time != nil{
                let whole_current = (addClassesDetails.end_date! + " " + addClassesDetails.end_time!)
                                  
                    let dateFormatter = DateFormatter()
                    dateFormatter.dateFormat = "dd-MM-yyyy HH:mm"
                    dateFormatter.timeZone = TimeZone(identifier: "UTC")
                    dateFormatter.locale = Locale(identifier: "en_US_POSIX")
                                  
                                 
                                    self.endatTime = dateFormatter.date(from: whole_current)!
                dic_RegisterValue.updateValue(addClassesDetails.end_time!, forKey: array_AllData[8])
            }
            if addClassesDetails.attendees_type != nil{
                dic_RegisterValue.updateValue(addClassesDetails.attendees_type!, forKey: array_AllData[9])
            }
            if addClassesDetails.age_group != nil{
                dic_RegisterValue.updateValue(addClassesDetails.age_group!, forKey: array_AllData[10])
            }
            if addClassesDetails.repeating_day != nil{
                dic_RegisterValue.updateValue(addClassesDetails.repeating_day!, forKey: array_AllData[11])
            }
            
            if addClassesDetails.teacher_name != nil{
                dic_RegisterValue.updateValue(addClassesDetails.teacher_name!, forKey: array_AllData[12])
            }
            if addClassesDetails.free != nil{
                dic_RegisterValue.updateValue(addClassesDetails.free!, forKey: array_AllData[13])
            }
            print(dic_RegisterValue)
            
        }else{
            
            for i in 0...array_AllData.count - 1{
                if dic_RegisterValue[array_AllData[i]] == nil{
                    
                    self.dic_RegisterValue.updateValue("", forKey:array_AllData[i])
                    self.dic_RegisterValue.updateValue("Description".localized, forKey:"Description")
                }
            }
            
        }
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.cornerRadiusAndShadow()
        // UIApplication.statusBarBackgroundColor = UIColor.init(hex: 0x05BDB9)
        
        if addNewClasses == "addNewClasses"{
            
            if arrayImages.count > 0{
                
                collectionView_Images.isHidden = false
                self.view_UploadPhoto.isHidden = true
                self.lbl_NoImageFound.isHidden = true
            }else{
                collectionView_Images.isHidden = true
                self.view_UploadPhoto.isHidden = false
                self.lbl_NoImageFound.isHidden = false
                
            }
            
        }
    }
    
    //Mark:TextView Delegate and DataSource----------------------------------------------------
    func textViewDidBeginEditing(_ textView: UITextView) {
        guard let indexPath = textView.tableViewIndexPath(self.tableView_AddEvents) else { return }
        
        switch (indexPath.section, 1) {
            
        case (0,1):
            
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "MHDescViewController") as! MHDescViewController
            vc.delegate = self
            vc.textDesc = textView.text!
            textView.keyboardType = .default
            vc.modalPresentationStyle = .fullScreen
            present(vc, animated: false, completion: nil)
        default:
            break
            
        }
        
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        
        textView.textColor = UIColor.black
        self.dic_RegisterValue.updateValue(textView.text as Any, forKey: self.array_AllData[1]) // Description
    }
    
    
    //MARK:TextField Delegate and DataSource Method-------------------------
    func textFieldDidBeginEditing(_ textField: UITextField) {
        guard let indexPath = textField.tableViewIndexPath(self.tableView_AddEvents) else { return }
        
        let index = IndexPath(row: textField.tag, section: 1)
        
        let cellSection1: Add_ActivitiesEvent_Section1TableViewCell = self.tableView_AddEvents.cellForRow(at: index) as! Add_ActivitiesEvent_Section1TableViewCell
        
        
        switch (indexPath.section, indexPath.row) {
            
        case (0,0):
            
            self.dic_RegisterValue.updateValue(textField.text as Any, forKey: self.array_AllData[0]) // Title
            
        case (0,2):
            
            let autocompleteController = GMSAutocompleteViewController()
            autocompleteController.delegate = self
            let filter = GMSAutocompleteFilter()
            filter.type = .noFilter  //suitable filter type
            //filter.country = "AUS"  //appropriate country code
            autocompleteController.autocompleteFilter = filter
            self.tag_forAddress = 2
            present(autocompleteController, animated: true, completion: nil)
            
        case (1,0):
            
            
            if textField == cellSection1.txt_Date{
                
                AKMultiPicker().openPickerIn(textField, firstComponentArray: array_SelectClassType) { (value, _, index1, _) in
                    textField.text = value
                    
                    
                    
                    print(value)
                    
                    if value == "Select Predefined"{
                        self.dic_RegisterValue.updateValue(1, forKey: self.array_AllData[3]) // attendance Type
                        cellSection1.img_Time.isHidden = false
                        cellSection1.txt_Time.text = ""
                        cellSection1.txt_Time.placeholder = "Select class"
                    }else if value == "Custom"{
                        self.dic_RegisterValue.updateValue(2, forKey: self.array_AllData[3])  //
                        cellSection1.img_Time.isHidden = true
                        cellSection1.txt_Time.text = ""
                        cellSection1.txt_Time.placeholder = "Enter Input"
                    }
                    
                }
            }else if textField == cellSection1.txt_Time{
                
                if dic_RegisterValue[self.array_AllData[3]] as? Int == 2{
                    self.dic_RegisterValue.updateValue(textField.text as Any, forKey: "user_Input")
                    
                    
                }else{
                    
                    PickerViewController.openPickerView(viewC: self, dataArray: array_SelectClass) { (value, index) in
                        print(value)
                        
                        textField.text = value
                        if value == "Quran & Tajweed"{
                            self.dic_RegisterValue.updateValue(1, forKey: self.array_AllData[4]) // attendance Type
                        }else if value == "Seerah & Hadith"{
                            self.dic_RegisterValue.updateValue(2, forKey: self.array_AllData[4])  // attendance Type
                        }else if value == "Islamic Studies"{
                            self.dic_RegisterValue.updateValue(3, forKey: self.array_AllData[4])  // attendance Type
                        }else if value == "Arabic learning"{
                            self.dic_RegisterValue.updateValue(4, forKey: self.array_AllData[4])  // attendance Type
                        }else if value == "Sports"{
                            self.dic_RegisterValue.updateValue(5, forKey: self.array_AllData[4])  // attendance Type
                        }else if value == "Academic"{
                            self.dic_RegisterValue.updateValue(6, forKey: self.array_AllData[4])  // attendance Type
                        }else if value == "Language"{
                            self.dic_RegisterValue.updateValue(7, forKey: self.array_AllData[4])  // attendance Type
                        }
                        
                    }
                }    }
            
        case (1,1):
            
                      if textField == cellSection1.txt_Date {
                          AKDatePicker.openPicker(in: textField, currentDate: Date(), pickerMode: .date) { (value) in
                              
                              if let eDate  = self.dic_RegisterValue["End_Date"] as? String , !eDate.isEmpty {
                                  let sDate = self.convertToString(date: value, dateformat: "dd-MM-yyyy")
                                  let eDate = self.convertToString(date: self.endate , dateformat: "dd-MM-yyyy")
                                  let sDt = MHHelper.shared.convertToDate(from: sDate, currentFormat: "dd-MM-yyyy")
                                  let eDt = MHHelper.shared.convertToDate(from: eDate, currentFormat: "dd-MM-yyyy")
                                  
                                  if eDt.compare(sDt) == .orderedAscending {
                                      AKAlertController.alert("Start date should be greater than End date", message: "")
                                  } else {
                                      textField.text = sDate
                                      self.startDate = value
                                      self.dic_RegisterValue[self.array_AllData[5]] = textField.text ?? ""
                                  }
                              } else {
                                  textField.text = self.convertToString(date: value, dateformat: "dd-MM-yyyy")
                                  self.endate = value
                                  self.startDate = value
                                  self.dic_RegisterValue[self.array_AllData[5]] = textField.text ?? ""
                                  self.dic_RegisterValue[self.array_AllData[7]] = textField.text ?? ""
                                  self.tableView_AddEvents.reloadSections(NSIndexSet(index: 1) as IndexSet, with: UITableView.RowAnimation.none)
                              }
                              
                          }
                      } else if textField == cellSection1.txt_Time {
                          
                          if (cellSection1.txt_Date.text ?? "").isEmpty {
                              AKAlertController.alert("Please enter start date first")
                          } else {
                              let startDT = MHHelper.shared.convertToDate(from: cellSection1.txt_Date.text ?? "", currentFormat: "dd-MM-yyyy")
                              
                              if startDT.compare(self.computedCurrentDate) == .orderedAscending {
                                  // curent date is greater than the start date
                              } else {
                                  // curent date is greater than or equal to the start date
                                  var minDate : Date? = Date()
                                  if self.computedCurrentDate.compare(startDT) == .orderedAscending {
                                      minDate = nil
                                  }
                                  
                                  AKTimePicker.openPicker(in: textField, currentDate: Date(), minimumDate: minDate, pickerMode: .time) { (value) in
                                      //  self.startTime = value
                                      
                                      let startDtee = String.getString(with: self.dic_RegisterValue["Start_Date"])
                                      let start_Time = self.convertToString(date: value, dateformat: "HH:mm")
                                      let startTime = (startDtee + " " + start_Time)
                                      
                                      let finalStartDate = MHHelper.shared.convertToDate(from: startTime, currentFormat: "dd-MM-yyyy HH:mm")
                                      
                                      let eTime = String.getString(with:self.dic_RegisterValue["End_Time"])
                                      if !eTime.isEmpty {
                                          let endDT = String.getString(with:self.dic_RegisterValue["End_Date"])
                                          let eT = "\(endDT) \(eTime)"
                                          let finalEndDT = MHHelper.shared.convertToDate(from: eT, currentFormat: "dd-MM-yyyy HH:mm")
                                          if finalEndDT.compare(finalStartDate) == .orderedAscending {
                                              AKAlertController.alert("Start time should be greater than End time", message: "")
                                          } else {
                                              self.dic_RegisterValue[ self.array_AllData[6]] = start_Time
                                              self.startTime = finalStartDate
                                              textField.text = self.convertToString(date: value, dateformat: "h:mm a")
                                          }
                                      } else {
                                          self.dic_RegisterValue[ self.array_AllData[6]] = start_Time
                                          self.startTime = finalStartDate
                                          textField.text = self.convertToString(date: value, dateformat: "h:mm a")
                                      }
                                  }
                                  
                              }
                          }
                      }
//            if textField == cellSection1.txt_Date{
//                AKDatePicker.openPicker(in: textField, currentDate: Date(), pickerMode: .date) { (value) in
//                    self.startDate = value
//                    if self.dic_RegisterValue["End_Date"] as? String != ""{
//
//                        let sDate = self.convertToString(date: value, dateformat: "dd-MM-yyyy")
//                        let eDate = self.convertToString(date: self.endate , dateformat: "dd-MM-yyyy")
//
//
//
//                        if sDate == eDate{
//                            textField.text = self.convertToString(date: value, dateformat: "dd-MM-yyyy")
//                            self.dic_RegisterValue.updateValue(textField.text as Any, forKey: self.array_AllData[3]) // End Date
//
//                        }else{
//                            if self.startDate > self.endate  {
//                                AKAlertController.alert("Start date should be greater than End date", message: "")
//                                return
//                            }else{
//                                textField.text = self.convertToString(date: value, dateformat: "dd-MM-yyyy")
//                                self.dic_RegisterValue.updateValue(textField.text as Any, forKey: self.array_AllData[5]) // Start Date
//
//                            }
//                        }
//                    }else{
//                        textField.text = self.convertToString(date: value, dateformat: "dd-MM-yyyy")
//                        self.endate = value
//                        self.dic_RegisterValue.updateValue(textField.text as Any, forKey: self.array_AllData[5]) // Start Date
//                        self.dic_RegisterValue.updateValue(textField.text as Any, forKey: self.array_AllData[7])
//                        self.tableView_AddEvents.reloadSections(NSIndexSet(index: 1) as IndexSet, with: UITableView.RowAnimation.none)
//                    }
//
//                }
//
//
//            }else if textField == cellSection1.txt_Time{
//
//                if cellSection1.txt_Date.text == ""{
//
//                                AKAlertController.alert("Please enter start date first")
//                            }else{
//
//                if cellSection1.txt_Date.text == AppDelegate.instance.currentDate{
//
//                    AKTimePicker.openPicker(in: textField, currentDate: Date(), minimumDate: Date(), pickerMode: .time) { (value) in
//                       // self.startTime = value
//
//
//                        let startDtee = self.convertToString(date: self.startDate, dateformat: "dd-MM-yyyy")
//
//                        let start_Time = self.convertToString(date: value, dateformat: "HH:mm")
//
//
//
//                        let startTime = (startDtee + " " + start_Time)
//
//                        let dateFormatter = DateFormatter()
//                        dateFormatter.dateFormat = "dd-MM-yyyy HH:mm"
//                        dateFormatter.timeZone = TimeZone(identifier: "UTC")
//                        dateFormatter.locale = Locale(identifier: "en_US_POSIX")
//
//                        self.startTime = dateFormatter.date(from: startTime)!
//
//
//
//
//                        if self.dic_RegisterValue["End_Time"] as? String != ""{
//                                if self.startDate != self.endate{
//                            if self.startTime > self.endatTime  {
//                                AKAlertController.alert("Start time should be greater than End time", message: "")
//                                return
//                            }else{
//                                textField.text = self.convertToString(date: value, dateformat: "h:mm a")
//                                let start_time = self.convertToString(date: value, dateformat: "HH:mm")
//                                self.dic_RegisterValue.updateValue(start_time as Any, forKey: self.array_AllData[6]) // Start Time
//                                    }}else{
//
//                                    textField.text = self.convertToString(date: value, dateformat: "h:mm a")
//                                    let start_time = self.convertToString(date: value, dateformat: "HH:mm")
//                                    self.dic_RegisterValue.updateValue(start_time as Any, forKey: self.array_AllData[6]) // Start Time
//                            }
//
//                        }else{
//                            textField.text = self.convertToString(date: value, dateformat: "h:mm a")
//                            let start_time = self.convertToString(date: value, dateformat: "HH:mm")
//                            self.dic_RegisterValue.updateValue(start_time as Any, forKey: self.array_AllData[6]) // Start Time
//                        }
//
//                    }
//
//                }else{
//
//
//                    AKTimePicker.openPicker(in: textField, currentDate: Date(), minimumDate: nil, pickerMode: .time) { (value) in
//                     //   self.startTime = value
//
//
//                        let startDtee = self.convertToString(date: self.startDate, dateformat: "dd-MM-yyyy")
//
//                        let start_Time = self.convertToString(date: value, dateformat: "HH:mm")
//
//
//
//                        let startTime = (startDtee + " " + start_Time)
//
//                        let dateFormatter = DateFormatter()
//                        dateFormatter.dateFormat = "dd-MM-yyyy HH:mm"
//                        dateFormatter.timeZone = TimeZone(identifier: "UTC")
//                        dateFormatter.locale = Locale(identifier: "en_US_POSIX")
//
//                        self.startTime = dateFormatter.date(from: startTime)!
//
//
//
//
//                        if self.dic_RegisterValue["End_Time"] as? String != ""{
//                            if self.startTime > self.endatTime  {
//                                AKAlertController.alert("Start time should be greater than End time", message: "")
//                                return
//                            }else{
//                                textField.text = self.convertToString(date: value, dateformat: "h:mm a")
//                                let start_time = self.convertToString(date: value, dateformat: "HH:mm")
//                                self.dic_RegisterValue.updateValue(start_time as Any, forKey: self.array_AllData[6]) // Start Time
//                            }
//
//                        }else{
//                            textField.text = self.convertToString(date: value, dateformat: "h:mm a")
//                            let start_time = self.convertToString(date: value, dateformat: "HH:mm")
//                            self.dic_RegisterValue.updateValue(start_time as Any, forKey: self.array_AllData[6]) // Start Time
//                        }
//
//                    }
//
//                }
//                }
//            }
        case (1,2):
            
//            if textField == cellSection1.txt_Date{
//                AKDatePicker.openPicker(in: textField, currentDate: Date(), pickerMode: .date) { (value) in
//                    self.endate = value
//                    if self.startDate > self.endate  {
//                        AKAlertController.alert("Start date should be greater than End date", message: "")
//                        return
//                    }else{
//                        textField.text = self.convertToString(date: value, dateformat: "dd-MM-yyyy")
//                        self.dic_RegisterValue.updateValue(textField.text as Any, forKey: self.array_AllData[7]) // End Date
//                    }
//                    
//                }
//            }else if textField == cellSection1.txt_Time{
//                
//                if cellSection1.txt_Date.text == ""{
//                                
//                                AKAlertController.alert("Please enter end date first")
//                            }else{
//                if cellSection1.txt_Date.text == AppDelegate.instance.currentDate{
//                    
//                    AKTimePicker.openPicker(in: textField, currentDate: Date(),minimumDate: Date(), pickerMode: .time) { (value) in
//                      //  self.endatTime = value
//                        
//                        let startDtee = self.convertToString(date: self.startDate, dateformat: "dd-MM-yyyy")
//                        
//                        let enddDtee = self.convertToString(date: self.endate, dateformat: "dd-MM-yyyy")
//                       let end_Time = self.convertToString(date: value, dateformat: "HH:mm")
//                        let endTime = (enddDtee + " " + end_Time)
//                        
//                        let dateFormatter = DateFormatter()
//                        dateFormatter.dateFormat = "dd-MM-yyyy HH:mm"
//                        dateFormatter.timeZone = TimeZone(identifier: "UTC")
//                        dateFormatter.locale = Locale(identifier: "en_US_POSIX")
//                        
//                        self.endatTime = dateFormatter.date(from: endTime)!
//                        
//                        
//                        if startDtee == enddDtee{
//                            
//                            if self.startTime > self.endatTime  {
//                                AKAlertController.alert("Start time should be greater than End time", message: "")
//                                return
//                            }else{
//                                textField.text = self.convertToString(date: value, dateformat: "h:mm a")
//                                let end_time = self.convertToString(date: value, dateformat: "HH:mm")
//                                self.dic_RegisterValue.updateValue(end_time as Any, forKey: self.array_AllData[8]) // End Time
//                            }
//                        }else{
//                            textField.text = self.convertToString(date: value, dateformat: "h:mm a")
//                            let end_time = self.convertToString(date: value, dateformat: "HH:mm")
//                            self.dic_RegisterValue.updateValue(end_time as Any, forKey: self.array_AllData[8]) // End Time
//                        }
//                    }}else{
//                    
//                    AKTimePicker.openPicker(in: textField, currentDate: nil, pickerMode: .time) { (value) in
//                        //self.endatTime = value
//                        
//                        let startDtee = self.convertToString(date: self.startDate, dateformat: "dd-MM-yyyy")
//                        
//                        let enddDtee = self.convertToString(date: self.endate, dateformat: "dd-MM-yyyy")
//                        let end_Time = self.convertToString(date: value, dateformat: "HH:mm")
//                        let endTime = (enddDtee + " " + end_Time)
//                        
//                        let dateFormatter = DateFormatter()
//                        dateFormatter.dateFormat = "dd-MM-yyyy HH:mm"
//                        dateFormatter.timeZone = TimeZone(identifier: "UTC")
//                        dateFormatter.locale = Locale(identifier: "en_US_POSIX")
//                        
//                        self.endatTime = dateFormatter.date(from: endTime)!
//                        
//                        
//                        if startDtee == enddDtee{
//                            
//                            if self.startTime > self.endatTime  {
//                                AKAlertController.alert("Start time should be greater than End time", message: "")
//                                return
//                            }else{
//                                textField.text = self.convertToString(date: value, dateformat: "h:mm a")
//                                let end_time = self.convertToString(date: value, dateformat: "HH:mm")
//                                self.dic_RegisterValue.updateValue(end_time as Any, forKey: self.array_AllData[8]) // End Time
//                            }
//                        }else{
//                            textField.text = self.convertToString(date: value, dateformat: "h:mm a")
//                            let end_time = self.convertToString(date: value, dateformat: "HH:mm")
//                            self.dic_RegisterValue.updateValue(end_time as Any, forKey: self.array_AllData[8]) // End Time
//                        }
//                    }
//                    }}}
//
            
            if textField == cellSection1.txt_Date {
                           AKDatePicker.openPicker(in: textField, currentDate: Date(), pickerMode: .date) { (value) in
                               let sDT = String.getString(with:self.dic_RegisterValue["Start_Date"])
                               let sDate = MHHelper.shared.convertToDate(from: sDT, currentFormat: "dd-MM-yyyy")
                               if sDT.isEmpty {
                                   AKAlertController.alert("Please select start Date First", message: "")
                               } else {
                                   let eDT = self.convertToString(date: value, dateformat: "dd-MM-yyyy")
                                   let eDate = MHHelper.shared.convertToDate(from: eDT, currentFormat: "dd-MM-yyyy")
                                   if eDate.compare(sDate) == .orderedAscending {
                                       AKAlertController.alert("Start date should be greater than End date", message: "")
                                   } else {
                                       self.endate = value
                                       textField.text = self.convertToString(date: value, dateformat: "dd-MM-yyyy")
                                       self.dic_RegisterValue[self.array_AllData[7]] = textField.text ?? "" // End Date
                                   }
                               }
                               
                              
                           }
                       } else if textField == cellSection1.txt_Time {
                           
                           if (cellSection1.txt_Date.text ?? "").isEmpty {
                               AKAlertController.alert("Please enter start date first")
                           } else {
                               let endDT = MHHelper.shared.convertToDate(from: cellSection1.txt_Date.text ?? "", currentFormat: "dd-MM-yyyy")
                               
                               if endDT.compare(self.computedCurrentDate) == .orderedAscending {
                                   // curent date is greater than the start date
                               } else {
                                   // curent date is greater than or equal to the start date
                                   var minDate : Date? = Date()
                                   if self.computedCurrentDate.compare(endDT) == .orderedAscending {
                                       minDate = nil
                                   }
                                   AKTimePicker.openPicker(in: textField, currentDate: Date(), minimumDate: minDate, pickerMode: .time) { (value) in
                                       //  self.startTime = value
                                       
                                       let endDtee = self.convertToString(date: self.endate, dateformat: "dd-MM-yyyy")
                                       let end_Time = self.convertToString(date: value, dateformat: "HH:mm")
                                       let endTime = (endDtee + " " + end_Time)
                                       
                                       let finalEndDate = MHHelper.shared.convertToDate(from: endTime, currentFormat: "dd-MM-yyyy HH:mm")
                                       
                                       if finalEndDate.compare(self.startTime) == .orderedAscending {
                                           AKAlertController.alert("Start time should be greater than End time", message: "")
                                       } else {
                                           self.endatTime = finalEndDate
                                           textField.text = self.convertToString(date: value, dateformat: "h:mm a")
                                           let end_Time = self.convertToString(date: value, dateformat: "HH:mm")
                                           self.dic_RegisterValue[self.array_AllData[8]] = end_Time
                                       }
                                   }
                               }
                           }
                       }
        case(2,0):
            
            AKMultiPicker().openPickerIn(textField, firstComponentArray: array_Attendene) { (value, _, index1, _) in
                textField.text = value
                
                if value == "Men Only"{
                    self.dic_RegisterValue.updateValue(1, forKey: self.array_AllData[9]) // attendance Type
                }else if value == "Women Only"{
                    self.dic_RegisterValue.updateValue(2, forKey: self.array_AllData[9])  // attendance Type
                }else if value == "Family"{
                    self.dic_RegisterValue.updateValue(3, forKey: self.array_AllData[9])  // attendance Type
                }
                
            }
        case(2,2):
            
            AKMultiPicker().openPickerIn(textField, firstComponentArray: array_RepeatingDay) { (value, _, index1, _) in
                textField.text = value
                
                if value == "Weekly"{
                    self.dic_RegisterValue.updateValue(1, forKey: self.array_AllData[11]) // Repeating day
                }else if value == "Fortnightly"{
                    self.dic_RegisterValue.updateValue(2, forKey: self.array_AllData[11]) // Repeating day
                }else if value == "Monthly"{
                    self.dic_RegisterValue.updateValue(3, forKey: self.array_AllData[11]) // Repeating day
                }
                
            }
            
        case(2,4):
            let array_Free = ["Free","Paid"]
            AKMultiPicker().openPickerIn(textField, firstComponentArray: array_Free) { (value, _, index1, _) in
                textField.text = value
                if value == "Free"{
                    self.dic_RegisterValue.updateValue(1, forKey: self.array_AllData[13]) // array_Free
                }else if value == "Paid"{
                    self.dic_RegisterValue.updateValue(2, forKey: self.array_AllData[13]) // array_Free
                }
                
                
                
            }
        default:
            break
        }
        
        
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        guard let indexPath = textField.tableViewIndexPath(self.tableView_AddEvents) else { return }
        
        let index = IndexPath(row: textField.tag, section: 1)
        let cellSection1: Add_ActivitiesEvent_Section1TableViewCell = self.tableView_AddEvents.cellForRow(at: index) as! Add_ActivitiesEvent_Section1TableViewCell
        
        
        switch (indexPath.section, indexPath.row) {
            
        case (0,0):
            
            self.dic_RegisterValue.updateValue(textField.text as Any, forKey: self.array_AllData[0]) // Title
        case (0,2):
            
            self.dic_RegisterValue.updateValue(textField.text as Any, forKey: self.array_AllData[2]) // Location
        case (1,0):
            
            if textField == cellSection1.txt_Time{
                
                if dic_RegisterValue[self.array_AllData[3]] as? Int == 2{
                    self.dic_RegisterValue.updateValue(textField.text as Any, forKey: "user_Input")
                }
                
            }
        case(2,1):
            
            self.dic_RegisterValue.updateValue(textField.text as Any, forKey: self.array_AllData[10])
            
        case(2,3):
            
            self.dic_RegisterValue.updateValue(textField.text as Any, forKey: self.array_AllData[12])
            
        default:
            break
        }
        
    }
    
    //Function And Methods-------------------------------------------
    
    func cornerRadiusAndShadow(){
        
        // corner radius for Save
        btn_Save.layer.cornerRadius = 5
        btn_Save.shadowBlackToHeader()
        
        // corner radius for Cancel
        btn_Cancel.layer.cornerRadius = 5
        btn_Cancel.shadowBlackToHeader()
        
        
        
        view_UpperTableView.clipsToBounds = true
        view_UpperTableView.layer.cornerRadius = 6
        view_UpperTableView.layer.maskedCorners =
            [.layerMaxXMinYCorner, .layerMinXMinYCorner]
        
        
        
        view_LowerTableView.layer.cornerRadius = 8
        view_LowerTableView.layer.maskedCorners =  [.layerMaxXMaxYCorner, .layerMinXMaxYCorner]
        
        view_LowerTableView.layer.shadowColor = UIColor.black.cgColor
        view_LowerTableView.layer.shadowOffset = CGSize(width: 0, height: 3)
        view_LowerTableView.layer.shadowOpacity = 0.4
        view_LowerTableView.layer.shadowRadius = 2.0
        
    }
    
    
    
    //MARK:Function Api----------------------------------------
    func AddClassesApi(imageData : [Data]){
        
        
        if dic_RegisterValue["Title"] as? String  == ""{
            
            AKAlertController.alert("Please enter Title")
        }
        else if dic_RegisterValue["Description"] as? String  == ""{
            
            AKAlertController.alert("Please enter Description")
            
        } else if dic_RegisterValue["Description"] as? String  == "Description"{
                   
                   AKAlertController.alert("Please enter Description")
            }
        else if dic_RegisterValue["Location"] as? String  == ""{
            
            AKAlertController.alert("Please enter Location")
        }
        else if dic_RegisterValue["Select_Class_Type"] as? String  == ""{
            
            AKAlertController.alert("Please select class type")
        }
            //        else if dic_RegisterValue["Select_Type"] as? String  == ""{
            //
            //            dic_RegisterValue.updateValue(1, forKey: "Select_Type")
            //        }
        else if dic_RegisterValue["Start_Date"] as? String  == ""{
            
            AKAlertController.alert("Please enter Start Date")
        }
        else if dic_RegisterValue["Start_Time"] as? String  == ""{
            
            AKAlertController.alert("Please enter Start Time")
        }
        else if dic_RegisterValue["End_Date"] as? String  == ""{
            
            AKAlertController.alert("Please enter End Date")
        }
        else if dic_RegisterValue["End_Time"] as? String  == ""{
            
            AKAlertController.alert("Please enter End Time")
        }
        else if dic_RegisterValue["Attendess_Type"] as? String  == ""{
            
            AKAlertController.alert("Please select attendees type")
        }
        else  if dic_RegisterValue["Age_Group"] as? String  == ""{
            AKAlertController.alert("Please enter age group")
            
        }
            
        else  if dic_RegisterValue["Repeating_Day"] as? String  == ""{
            AKAlertController.alert("Please select repeating day")
            
        }
        else  if dic_RegisterValue["Teacher_Type"] as? String  == ""{
            AKAlertController.alert("Please enter teacher name")
            
        }
        else if dic_RegisterValue["Free"] as? String  == ""{
            
            AKAlertController.alert("Please select cost type")
        }else if (dic_RegisterValue["Start_Date"] as! String == AppDelegate.instance.currentDate){
                   AppDelegate.instance.current_date()
                let startTime = dic_RegisterValue["Start_Time"] as! String
                let whole_Date = (dic_RegisterValue["Start_Date"] as! String + " " + startTime)
                
                let whole_current = (AppDelegate.instance.currentDate + " " + AppDelegate.instance.currentTime)
                
                let dateFormatter = DateFormatter()
                dateFormatter.dateFormat = "dd-MM-yyyy HH:mm"
                dateFormatter.timeZone = TimeZone(identifier: "UTC")
                dateFormatter.locale = Locale(identifier: "en_US_POSIX")
             
                let startTime_enter = dateFormatter.date(from: whole_Date)!
                let currentDate = dateFormatter.date(from: whole_current)!
               
                if startTime_enter < currentDate{
                    
                    AKAlertController.alert("Start time should be greater than End time")
                }else{
                    self.addClassesApiCall()
            }
                    
            }else if (dic_RegisterValue["Start_Date"] as! String == dic_RegisterValue["End_Date"] as! String){
               
                 let startTime = dic_RegisterValue["Start_Time"] as! String
                                   let whole_Date = (dic_RegisterValue["Start_Date"] as! String + " " + startTime)
                                   
                                   let end_Time = dic_RegisterValue["End_Time"] as! String
                                   let whole_endDate = (dic_RegisterValue["End_Date"] as! String + " " + end_Time )
                                   
                                   let dateFormatter = DateFormatter()
                                   dateFormatter.dateFormat = "dd-MM-yyyy HH:mm"
                                   dateFormatter.timeZone = TimeZone(identifier: "UTC")
                                   dateFormatter.locale = Locale(identifier: "en_US_POSIX")
                                
                                   let startTime_enter = dateFormatter.date(from: whole_Date)!
                                   let endDate_enter = dateFormatter.date(from: whole_endDate)!
                                  
                                   if startTime_enter > endDate_enter{
                                       
                                       AKAlertController.alert("Start time should be greater than End time")
                                    return
                                   }else {
                                    self.addClassesApiCall()
            }
                      
                   
                   
               }
                
            
        else{
            self.addClassesApiCall()
        }
    }
    
    
    func addClassesApiCall(){
        
        
        let repeating_Day = (dic_RegisterValue["Repeating_Day"] as! Int)
        var para = [String:Any]()
        if dic_RegisterValue["Select_Class_Type"] as? Int  == 2 {
            if dic_RegisterValue["user_Input"] as? String  == ""{
                
                AKAlertController.alert("Please enter user input")
            }else{
                para   = ["org": orgId, "title": dic_RegisterValue["Title"] as! String,"description": dic_RegisterValue["Description"] as! String,"latitude":self.lat,"longitude":self.long,"address":dic_RegisterValue["Location"] as! String,"class_input_type":dic_RegisterValue["Select_Class_Type"] as! Int,"user_input":dic_RegisterValue["user_Input"] as! String,"start_date":dic_RegisterValue["Start_Date"] as! String,"end_date":dic_RegisterValue["End_Date"] as! String,"start_time":dic_RegisterValue["Start_Time"] as! String,"end_time":dic_RegisterValue["End_Time"] as! String,"attendees_type":dic_RegisterValue["Attendess_Type"] as!
                    Int,"age_group":dic_RegisterValue["Age_Group"] as! String,"repeating_day":repeating_Day,"teacher_name":dic_RegisterValue["Teacher_Type"] as! String,"free":dic_RegisterValue["Free"] as! Int]
                
                KVNProgress.show()
                Network.shared.requestWithMultipleForClasses(imageData: imageData, image1: "", image2: "",image3: "",image4: "",image5: "",image6: "",parameters: para) { (result) in
                    KVNProgress.dismiss()
                    if result?.success == true{
                        self.showSingleAlertMessage(message: "", subMsg: result?.message ?? "upload successfully", sender: self, completion:
                            { (success) -> Void in
                                if success == true{
                                    self.presentingViewController?.presentingViewController?.dismiss(animated: true, completion: nil)
                                }
                                
                        }
                        )
                    }else{
                        self.showSingleAlertMessage(message: "", subMsg: result?.message ?? "", sender: self, completion:
                            { (success) -> Void in
                                if success == true{
                                    
                                }
                                
                        }
                        )
                    }
                }
                
            }
            
        }else{
            if dic_RegisterValue["Select_Type"] as? String  == "" || dic_RegisterValue["Select_Type"] as? Int  == nil{
                
                AKAlertController.alert("Please select class")
            }else{
                para = ["org": orgId, "title": dic_RegisterValue["Title"] as! String,"description": dic_RegisterValue["Description"] as! String,"latitude":self.lat,"longitude":self.long,"address":dic_RegisterValue["Location"] as! String,"class_input_type":dic_RegisterValue["Select_Class_Type"] as! Int,"select_class":dic_RegisterValue["Select_Type"] as! Int,"user_input":dic_RegisterValue["user_Input"] as! String,"start_date":dic_RegisterValue["Start_Date"] as! String,"end_date":dic_RegisterValue["End_Date"] as! String,"start_time":dic_RegisterValue["Start_Time"] as! String,"end_time":dic_RegisterValue["End_Time"] as! String,"attendees_type":dic_RegisterValue["Attendess_Type"] as!
                    Int,"age_group":dic_RegisterValue["Age_Group"] as! String,"repeating_day":repeating_Day,"teacher_name":dic_RegisterValue["Teacher_Type"] as! String,"free":dic_RegisterValue["Free"] as! Int]
                KVNProgress.show()
                Network.shared.requestWithMultipleForClasses(imageData: imageData, image1: "", image2: "",image3: "",image4: "",image5: "",image6: "",parameters: para) { (result) in
                    KVNProgress.dismiss()
                    if result?.success == true{
                        self.showSingleAlertMessage(message: "", subMsg: result?.message ?? "upload successfully", sender: self, completion:
                            { (success) -> Void in
                                if success == true{
                                    self.presentingViewController?.presentingViewController?.dismiss(animated: true, completion: nil)
                                }
                                
                        }
                        )
                    }else{
                        self.showSingleAlertMessage(message: "", subMsg: result?.message ?? "", sender: self, completion:
                            { (success) -> Void in
                                if success == true{
                                    
                                }
                                
                        }
                        )
                    }
                }
                
            }
            
        }
        
        
    }
        
    
    
    
    func EditClassesApi(){
        
        if dic_RegisterValue["Select_Class_Type"] as? String  == ""{
            
            dic_RegisterValue.updateValue(1, forKey: "Select_Class_Type")
        }
        if dic_RegisterValue["Select_Type"] as? String  == "" || dic_RegisterValue["Select_Type"] as? String == nil {
            
            dic_RegisterValue.updateValue(1, forKey: "Select_Type")
        }
        if dic_RegisterValue["Attendess_Type"] as? String  == ""{
            
            dic_RegisterValue.updateValue(1, forKey: "Attendess_Type")
        }
        if dic_RegisterValue["Free"] as? String  == ""{
            
            dic_RegisterValue.updateValue(1, forKey: "Free")
        }
        if dic_RegisterValue["Repeating_Day"] as? String  == ""{
            
            dic_RegisterValue.updateValue(1, forKey: "Repeating_Day")
        }
       
        
//         if (dic_RegisterValue["Start_Date"] as! String == AppDelegate.instance.currentDate){
//            
//            let startTime = dic_RegisterValue["Start_Time"] as! String
//            let whole_Date = (dic_RegisterValue["Start_Date"] as! String + " " + startTime)
//               AppDelegate.instance.current_date()
//            let whole_current = (AppDelegate.instance.currentDate + " " + AppDelegate.instance.currentTime)
//            
//            let dateFormatter = DateFormatter()
//            dateFormatter.dateFormat = "dd-MM-yyyy HH:mm"
//            dateFormatter.timeZone = TimeZone(identifier: "UTC")
//            dateFormatter.locale = Locale(identifier: "en_US_POSIX")
//         
//            let startTime_enter = dateFormatter.date(from: whole_Date)!
//            let currentDate = dateFormatter.date(from: whole_current)!
//           
//            if startTime_enter < currentDate{
//                
//                AKAlertController.alert("Start time should be greater than End time")
//                return
//            }else {
//                 self.editClassesApiCall()
//            }
//                   
//                
//                
//            }else if (dic_RegisterValue["Start_Date"] as! String == dic_RegisterValue["End_Date"] as! String){
//               
//                 let startTime = dic_RegisterValue["Start_Time"] as! String
//                                   let whole_Date = (dic_RegisterValue["Start_Date"] as! String + " " + startTime)
//                                   
//                                   let end_Time = dic_RegisterValue["End_Time"] as! String
//                                   let whole_endDate = (dic_RegisterValue["End_Date"] as! String + " " + end_Time )
//                                   
//                                   let dateFormatter = DateFormatter()
//                                   dateFormatter.dateFormat = "dd-MM-yyyy HH:mm"
//                                   dateFormatter.timeZone = TimeZone(identifier: "UTC")
//                                   dateFormatter.locale = Locale(identifier: "en_US_POSIX")
//                                
//                                   let startTime_enter = dateFormatter.date(from: whole_Date)!
//                                   let endDate_enter = dateFormatter.date(from: whole_endDate)!
//                                  
//                                   if startTime_enter > endDate_enter{
//                                       
//                                       AKAlertController.alert("Start time should be greater than End time")
//                                    return
//                                   }else {
//                                    self.editClassesApiCall()
//            }
//                      
//                   
//                   
//               }
//            
            
//         else{
//            self.editClassesApiCall()
//        }
        
        let tempCurrentDate = MHHelper.shared.convertToStrinDate(date: Date(), desiredFormat: "dd-MM-yyyy HH:mm")
         
         let myCurrentDate = MHHelper.shared.convertToDate(from:tempCurrentDate , currentFormat: "dd-MM-yyyy HH:mm")
         
         
         let startTime = dic_RegisterValue["Start_Time"] as! String
         let whole_Date = (dic_RegisterValue["Start_Date"] as! String + " " + startTime)
         
         let end_Time = dic_RegisterValue["End_Time"] as! String
         let whole_endDate = (dic_RegisterValue["End_Date"] as! String + " " + end_Time )
         
         
         
         
         let startTime_enter = MHHelper.shared.convertToDate(from: whole_Date, currentFormat: "dd-MM-yyyy HH:mm")
         
         let endDate_enter = MHHelper.shared.convertToDate(from: whole_endDate, currentFormat: "dd-MM-yyyy HH:mm")
         
       
         
         if startTime_enter.compare(myCurrentDate) == .orderedAscending {
             AKAlertController.alert("Start time can not be lesser than current time")
         }else {
             if startTime_enter.compare(endDate_enter) == .orderedAscending || startTime_enter.compare(endDate_enter) == .orderedSame  {
                 self.editClassesApiCall()
             }else{
                 AKAlertController.alert("Start time should be greater than End time")
             }
         }
         
      
    }
    
    
    
    func editClassesApiCall(){
        
        var para = [String:Any]()
        let repeating_Day = (dic_RegisterValue["Repeating_Day"] as! Int)
               
        if dic_RegisterValue["Select_Class_Type"] as? Int  == 2 {
            para  = ["id":addClassesDetails.id ?? 0,"org": addClassesDetails.org!, "title": dic_RegisterValue["Title"] as! String,"description": dic_RegisterValue["Description"] as! String,"latitude":self.lat,"longitude":self.long.rounded(toPlaces: 6),"address":dic_RegisterValue["Location"] as! String,"class_input_type":dic_RegisterValue["Select_Class_Type"] as? Int ?? 2,"user_input":dic_RegisterValue["user_Input"] as! String,"start_date":dic_RegisterValue["Start_Date"] as! String,"end_date":dic_RegisterValue["End_Date"] as! String,"start_time":dic_RegisterValue["Start_Time"] as! String,"end_time":dic_RegisterValue["End_Time"] as! String,"attendees_type":dic_RegisterValue["Attendess_Type"] as!
                Int,"age_group":dic_RegisterValue["Age_Group"] as! String,"repeating_day":repeating_Day,"teacher_name":dic_RegisterValue["Teacher_Type"] as! String,"free":dic_RegisterValue["Free"] as! Int]
            
        }else{
            guard let addClassDetaiil = addClassesDetails else {
                return
            }
            para  = ["id":addClassDetaiil.id ?? 0,"org": addClassDetaiil.org!, "title": dic_RegisterValue["Title"] as! String,"description": dic_RegisterValue["Description"] as! String,"latitude":self.lat,"longitude":self.long.rounded(toPlaces: 6),"address":dic_RegisterValue["Location"] as! String,"class_input_type":dic_RegisterValue["Select_Class_Type"] as? Int ?? 1,"select_class":dic_RegisterValue["Select_Type"] as? Int ?? 0,"user_input":dic_RegisterValue["user_Input"] as! String,"start_date":dic_RegisterValue["Start_Date"] as! String,"end_date":dic_RegisterValue["End_Date"] as! String,"start_time":dic_RegisterValue["Start_Time"] as! String,"end_time":dic_RegisterValue["End_Time"] as! String,"attendees_type":dic_RegisterValue["Attendess_Type"] as!
                Int,"age_group":dic_RegisterValue["Age_Group"] as! String,"repeating_day":repeating_Day,"teacher_name":dic_RegisterValue["Teacher_Type"] as! String,"free":dic_RegisterValue["Free"] as! Int]
            
        }
        
              KVNProgress.show()
              Network.shared.editForClasses(parameters: para) { (result) in
                  KVNProgress.dismiss()
                  if result?.success == true{
                      self.showSingleAlertMessage(message: "", subMsg: result?.message ?? "upload successfully", sender: self, completion:
                          { (success) -> Void in
                              if success == true{
                                  self.dismiss(animated: true, completion: nil)
                              }
                              
                      }
                      )
                  }else{
                      self.showSingleAlertMessage(message: "", subMsg: result?.message ?? "Class Does Not Exists", sender: self, completion:
                          { (success) -> Void in
                              if success == true{
                                  
                              }
                              
                      }
                      )
                  }
              }
        
    }
        
        
    
}

//MARK:TableView Delegate And DataSources--------------------------------------------------------
extension Add_ClassessViewController:UITableViewDelegate,UITableViewDataSource{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        
        return 3
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if section == 0{
            return 3
        }else if section == 1{
            return 3
        }else if section == 2{
            return 5
        }else{
            
            return 1
        }
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        print(dic_RegisterValue)
        if indexPath.section == 0{
            
            let cell = self.tableView_AddEvents.dequeueReusableCell(withIdentifier: "Add_ActivitiesEventTableViewCell") as! Add_ActivitiesEventTableViewCell
            
            if indexPath.row == 0{
                if self.dic_RegisterValue["Title"] as? String != nil{
                    cell.txt_Title.text = self.dic_RegisterValue["Title"] as? String
                }
                if self.dic_RegisterValue["Title"] as? String == "" {
                    
                    cell.txt_Title.placeholder = "Title".localized
                    
                }
                cell.view_1.isHidden = false
                cell.view_2.isHidden = true
                cell.view_3.isHidden = true
                
                
            }else if indexPath.row == 1{
                if self.dic_RegisterValue["Description"] as? String != nil{
                    cell.txtView_Description.text = self.dic_RegisterValue["Description"] as? String
                }
                if cell.txtView_Description.text == "Description".localized{
                    cell.txtView_Description.textColor = UIColor.lightGray
                    
                }else{
                    cell.txtView_Description.textColor = UIColor.black
                    
                    
                }
                cell.view_1.isHidden = true
                cell.view_2.isHidden = false
                cell.view_3.isHidden = true
                
                
            }else if indexPath.row == 2{
                if self.dic_RegisterValue["Location"] as? String != nil{
                    cell.txt_Title.text = self.dic_RegisterValue["Location"] as? String
                }else{
                    
                    cell.txt_Title.placeholder = "Location".localized
                    //cell.txt_Title.placeHolderColor = UIColor.init(hex: 0x807F7F)
                }
                if self.dic_RegisterValue["Location"] as? String == "" {
                    
                    cell.txt_Title.placeholder = "Location"
                    
                }
                
                cell.view_1.isHidden = false
                cell.view_2.isHidden = true
                cell.view_3.isHidden = true
                
                
            }
            return cell
        }else if indexPath.section == 1{
            
            let cell = self.tableView_AddEvents.dequeueReusableCell(withIdentifier: "Add_ActivitiesEvent_Section1TableViewCell") as! Add_ActivitiesEvent_Section1TableViewCell
            
            cell.txt_Date.tag = indexPath.row
            cell.txt_Time.tag = indexPath.row
            cell.txt_Time.placeholder = array_StartTime[indexPath.row]
            cell.txt_Date.placeholder = array_StartDate[indexPath.row]
            
            if indexPath.row == 0{
                cell.img_Date.image = UIImage(named: "down_arrow_iPhone_black.png")
                cell.img_Time.image = UIImage(named: "down_arrow_iPhone_black.png")
                cell.txt_Date.placeholder = "Select Class Type".localized
                cell.txt_Time.placeholder = "Select Class".localized
                               
                if self.dic_RegisterValue["Select_Class_Type"] as? Int != nil{
                    if  self.dic_RegisterValue["Select_Class_Type"] as? Int == 1{
                        cell.txt_Date.text = "Select Predefined"
                        //  cell.txt_Time.text = self.dic_RegisterValue["user_Input"] as? String
                        if self.dic_RegisterValue["Select_Type"] as? Int != nil{
                            if  self.dic_RegisterValue["Select_Type"] as? Int == 1{
                                cell.txt_Time.text = "Quran & Tajweed"
                                
                            }else if self.dic_RegisterValue["Select_Type"] as? Int == 2{
                                
                                cell.txt_Time.text = "Seerah & Hadith"
                            }
                            else if self.dic_RegisterValue["Select_Type"] as? Int == 3{
                                
                                cell.txt_Time.text = "Islamic Studies"
                            }else if self.dic_RegisterValue["Select_Type"] as? Int == 4{
                                
                                cell.txt_Time.text = "Arabic learning"
                            }else if self.dic_RegisterValue["Select_Type"] as? Int == 5{
                                
                                cell.txt_Time.text = "Sports"
                            }else if self.dic_RegisterValue["Select_Type"] as? Int == 6{
                                
                                cell.txt_Time.text = "Academic"
                            }else if self.dic_RegisterValue["Select_Type"] as? Int == 7{
                                
                                cell.txt_Time.text = "Language"
                            }
                        }
                        
                    }else if self.dic_RegisterValue["Select_Class_Type"] as? Int == 2{
                        cell.txt_Date.text = "Custom"
                        cell.txt_Time.text = self.dic_RegisterValue["user_Input"] as? String
                    }
                    
                    
                }
                
            } else if indexPath.row == 1{
                
                cell.txt_Date.placeholder = "Start Date".localized
                cell.txt_Time.placeholder = "Start Time".localized
                 
                if self.dic_RegisterValue["Start_Date"] as? String != nil{
                    cell.txt_Date.text = self.dic_RegisterValue["Start_Date"] as? String
                }
                if self.dic_RegisterValue["Start_Time"] as? String != "" {
                    
                    self.TimeFormat24To12(timeString:(self.dic_RegisterValue["Start_Time"] as? String)!)
                    
                    cell.txt_Time.text = str_Time12hrs //self.dic_RegisterValue["Start_Time"] as? String
                }
            }else if indexPath.row == 2{
                
                cell.txt_Date.placeholder = "End Date".localized
                cell.txt_Time.placeholder = "End Time".localized
                
                if self.dic_RegisterValue["End_Date"] as? String != nil{
                    cell.txt_Date.text = self.dic_RegisterValue["End_Date"] as? String
                }
                if self.dic_RegisterValue["End_Time"] as? String != ""{
                    self.TimeFormat24To12(timeString:(self.dic_RegisterValue["End_Time"] as? String)!)
                    cell.txt_Time.text = str_Time12hrs
                }
            }
            
            return cell
            
        }else if indexPath.section == 2{
            
            
            let cell = self.tableView_AddEvents.dequeueReusableCell(withIdentifier: "Add_ActivitiesEventTableViewCell") as! Add_ActivitiesEventTableViewCell
            
            cell.view_1.isHidden = true
            cell.view_2.isHidden = true
            cell.view_3.isHidden = false
            
            if indexPath.row == 1 || indexPath.row == 3{
                cell.img_DropDown.isHidden = true
            }else{
                cell.img_DropDown.isHidden = false
            }
            cell.txt_RepeatingDay.placeholder = array_SectionLast_Placeholder[indexPath.row].localized
            
            if indexPath.row == 0{
                
                if self.dic_RegisterValue["Attendess_Type"] as? Int != nil{
                    
                    let attendees =  self.dic_RegisterValue["Attendess_Type"] as? Int
                    if attendees == 1{
                        cell.txt_RepeatingDay.text = "Men Only"
                    }else if attendees == 2 {
                        cell.txt_RepeatingDay.text = "Women Only"
                    }else if attendees == 3 {
                        cell.txt_RepeatingDay.text = "Family"
                    }
                }else{
                    cell.txt_RepeatingDay.text = dic_RegisterValue["Attendess_Type"] as? String
                }
                
                
            }else if indexPath.row == 1{
                
                cell.txt_RepeatingDay.text = dic_RegisterValue["Age_Group"] as? String
            }else if indexPath.row == 2{
                
                if self.dic_RegisterValue["Repeating_Day"] as? Int != nil{
                    
                    let attendees =  self.dic_RegisterValue["Repeating_Day"] as? Int
                    if attendees == 1{
                        cell.txt_RepeatingDay.text = "Weekly"
                    }else if attendees == 2 {
                        cell.txt_RepeatingDay.text = "Fortnightly"
                    }else if attendees == 3 {
                        cell.txt_RepeatingDay.text = "Monthly"
                    }
                }else{
                    cell.txt_RepeatingDay.text = dic_RegisterValue["Repeating_Day"] as? String
                }
                
                
            }else if indexPath.row == 3{
                cell.txt_RepeatingDay.text = dic_RegisterValue["Teacher_Type"] as? String
            }else if indexPath.row == 4{
                
                if self.dic_RegisterValue["Free"] as? Int != nil{
                    
                    let Cost =  self.dic_RegisterValue["Free"] as? Int
                    if Cost == 1{
                        cell.txt_RepeatingDay.text = "Free"
                    }else if Cost == 2 {
                        cell.txt_RepeatingDay.text = "Paid"
                    }
                }else{
                    cell.txt_RepeatingDay.text = dic_RegisterValue["Free"] as? String
                }
                
            }
            return cell
        }
        
        return UITableViewCell()
    }
    
    
    
}

//MARK:Collection View Delegtae And DataSources------------
extension Add_ClassessViewController: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout{
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 2
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if section == 0{
            if self.addNewClasses != "addNewClasses" {
                return 0
            }else{
                return 1
            }
        }else{
            if arrayImages.count > 0{
                if self.arrayImages.count > 6{
                    return 6
                }else{
                    return self.arrayImages.count
                }
            }
            if self.addNewClasses != "addNewClasses" {
                return addClassesDetails.images?.count ?? 0
            }
        }
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if indexPath.section == 0{
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Home_AddMoreCollectionViewCell", for: indexPath) as! Home_AddMoreCollectionViewCell
            cell.btn_Addmore.addTarget(self, action: #selector(btn_AddPhotos(sender:)), for: .touchUpInside)
            return cell
        }else{
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "AddimageAnnouncementCollectionViewCell", for: indexPath) as! AddimageAnnouncementCollectionViewCell
            if addClassesDetails != nil{
                cell.btn_Delete.isHidden = true
                if addClassesDetails.images?.count != nil{
                    cell.btn_Delete.isHidden = true
                    cell.img_View.sd_setImage(with: URL(string:(self.addClassesDetails.images?[indexPath.item])!),placeholderImage: UIImage.init(named: "default_header_iPhone.png"), completed: nil)
                }
            }else{
                cell.img_View.image = arrayImages[indexPath.row]
                cell.btn_Delete.isHidden = false
                cell.btn_Delete.tag = indexPath.item
                cell.btn_Delete.addTarget(self, action: #selector(btn_deltePhot(sender:)), for: .touchUpInside)
            }
            return cell
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if indexPath.section == 0{
            return  CGSize(width: 100 , height: 100)
        }else{
            return CGSize(width: collectionView.bounds.size.width/2 - 20 , height: 160)
        }
    }
    
    private func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAtIndex section: Int) -> UIEdgeInsets {
        if section == 1{
            let totalCellWidth = 40 * collectionView_Images.numberOfItems(inSection: 1)
            let totalSpacingWidth = 4 * (collectionView_Images.numberOfItems(inSection: 1) - 1)
            
            let leftInset = (collectionView.layer.frame.size.width - CGFloat(totalCellWidth + totalSpacingWidth)) / 1
            let rightInset = leftInset
            
            return UIEdgeInsets(top: 0, left: leftInset, bottom: 0, right: rightInset)
        }else{
            return  UIEdgeInsets( top: 0,  left: 0, bottom: 0,  right: 0)
        }
    }
    //    func collectionView(_ collectionView: UICollectionView,
    //                        layout collectionViewLayout: UICollectionViewLayout,
    //                        insetForSectionAt section: Int) -> UIEdgeInsets{
    //
    //        if section == 0{
    //            return  UIEdgeInsets( top: 0,  left: 0, bottom: 0,  right: 0)
    //        }else{
    //            return  UIEdgeInsets( top: 0,  left: 0, bottom: 0,  right: 0)
    //        }
    //    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 5.0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 5.0
    }
    
    
}
extension Add_ClassessViewController: GMSAutocompleteViewControllerDelegate {
    
    // Handle the user's selection.
    func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {
        
        print("Place name: \(String(describing: place.name))")
        let index = IndexPath(row: self.tag_forAddress, section: 0)
        let cell: Add_ActivitiesEventTableViewCell = self.tableView_AddEvents.cellForRow(at: index) as! Add_ActivitiesEventTableViewCell
        let location:CLLocationCoordinate2D = place.coordinate
        let latitude_str = String(format: "%.6f", location.latitude)
        let longituted_str = String(format: "%.6f", location.longitude)
        self.lat = Double(latitude_str) ?? 123.123
        self.long = Double(longituted_str) ?? 123.123
        print("Place address \(String(describing: place.formattedAddress))")
        
        dic_RegisterValue.updateValue("\(String(describing:place.formattedAddress ?? ""))", forKey: array_AllData[2])
        
        cell.txt_Title.text = "\(String(describing: place.formattedAddress ?? ""))"
        
        
        print("Place address:",place.formattedAddress!)
        
        
        dismiss(animated: true, completion: nil)
    }
    
    func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
        
        // TODO: handle the error.
        print("Error: ", error.localizedDescription)
    }
    
    // User canceled the operation.
    func wasCancelled(_ viewController: GMSAutocompleteViewController) {
        //  addressTextView.resignFirstResponder()
        dismiss(animated: true, completion: nil)
    }
    
    // Turn the network activity indicator on and off again.
    func didRequestAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
    }
    
    func didUpdateAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
    }
    
    //MARK:IBACtion-----------------------------------------------
    @IBAction func btn_Save(_ sender:UIButton){
        if self.addNewClasses == "addNewClasses"{
            print(dic_RegisterValue)
            self.AddClassesApi(imageData: imageData)
        }else{
            print(dic_RegisterValue)
            self.EditClassesApi()
        }
        
    }
    @IBAction func btn_Back(sender:UIButton){
        
        if addNewClasses == "addNewClasses"{
            self.presentingViewController?.presentingViewController?.dismiss(animated: true, completion: nil)
        }else{
            self.dismiss(animated: true, completion: nil)
        }
    }
    @objc func btn_deltePhot(sender:UIButton){
        let buttonTag = sender.tag
        
        print("self.arrayImages:22",self.imageArray.count)
        self.arrayImages.remove(at: buttonTag)
        self.imageData.remove(at: buttonTag)
        
        print("self.arrayImages:33",self.imageArray.count)
        self.collectionView_Images.reloadData()
        
    }
    @objc func btn_AddPhotos(sender:UIButton){
        
        if self.arrayImages.count == 6{
            
            AKAlertController.alert("You can't select more than 6 images")
        }
        
        self.view.endEditing(true)
        let pickerViewController = YMSPhotoPickerViewController.init()
        pickerViewController.numberOfPhotoToSelect = 6
        let customColor = UIColor.init(red: 3.0/255.0, green: 68.0/255.0, blue: 114.0/255.0, alpha: 1.0)
        pickerViewController.theme.titleLabelTextColor = UIColor.white
        pickerViewController.theme.navigationBarBackgroundColor = customColor
        pickerViewController.theme.tintColor = UIColor.white
        pickerViewController.theme.orderTintColor = customColor
        pickerViewController.theme.orderLabelTextColor = UIColor.white
        pickerViewController.theme.cameraVeilColor = customColor
        pickerViewController.theme.cameraIconColor = UIColor.white
        pickerViewController.theme.statusBarStyle = .default
        self.yms_presentCustomAlbumPhotoView(pickerViewController, delegate: self)
    }
    
    @IBAction func photoLibraryTapped(_ sender: Any) {
        self.view.endEditing(true)
        let pickerViewController = YMSPhotoPickerViewController.init()
        pickerViewController.numberOfPhotoToSelect = 6
        let customColor = UIColor.init(red: 3.0/255.0, green: 68.0/255.0, blue: 114.0/255.0, alpha: 1.0)
        pickerViewController.theme.titleLabelTextColor = UIColor.white
        pickerViewController.theme.navigationBarBackgroundColor = customColor
        pickerViewController.theme.tintColor = UIColor.white
        pickerViewController.theme.orderTintColor = customColor
        pickerViewController.theme.orderLabelTextColor = UIColor.white
        pickerViewController.theme.cameraVeilColor = customColor
        pickerViewController.theme.cameraIconColor = UIColor.white
        pickerViewController.theme.statusBarStyle = .default
        self.yms_presentCustomAlbumPhotoView(pickerViewController, delegate: self)
    }
}

extension Add_ClassessViewController : YMSPhotoPickerViewControllerDelegate{
    
    // MARK: - YMSPhotoPickerViewControllerDelegate
    
    func photoPickerViewControllerDidReceivePhotoAlbumAccessDenied(_ picker: YMSPhotoPickerViewController!) {
        let alertController = UIAlertController.init(title: "Allow photo album access?", message: "Need your permission to access photo albumbs", preferredStyle: .alert)
        let dismissAction = UIAlertAction.init(title: "Cancel", style: .cancel, handler: nil)
        let settingsAction = UIAlertAction.init(title: "Settings", style: .default) { (action) in
            UIApplication.shared.openURL(URL.init(string: UIApplication.openSettingsURLString)!)
        }
        alertController.addAction(dismissAction)
        alertController.addAction(settingsAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
    func photoPickerViewControllerDidReceiveCameraAccessDenied(_ picker: YMSPhotoPickerViewController!) {
        let alertController = UIAlertController.init(title: "Allow camera album access?", message: "Need your permission to take a photo", preferredStyle: .alert)
        let dismissAction = UIAlertAction.init(title: "Cancel", style: .cancel, handler: nil)
        let settingsAction = UIAlertAction.init(title: "Settings", style: .default) { (action) in
            UIApplication.shared.openURL(URL.init(string: UIApplication.openSettingsURLString)!)
        }
        alertController.addAction(dismissAction)
        alertController.addAction(settingsAction)
        picker.present(alertController, animated: true, completion: nil)
    }
    func photoPickerViewController(_ picker: YMSPhotoPickerViewController!, didFinishPicking image: UIImage!) {
        picker.dismiss(animated: true) {
        }
    }
    
    func photoPickerViewController(_ picker: YMSPhotoPickerViewController!, didFinishPickingImages photoAssets: [PHAsset]!) {
        
        picker.dismiss(animated: true) {
            let imageManager = PHImageManager.init()
            let options = PHImageRequestOptions.init()
            options.deliveryMode = .highQualityFormat
            options.resizeMode = .none
            options.isSynchronous = true
            
            
            if photoAssets.count + self.arrayImages.count > 6{
                
                AKAlertController.alert("You can't select more than 6 images")
            }
                
            else{
                for asset: PHAsset in photoAssets
                {
                    let targetSize = CGSize(width: UIScreen.main.bounds.size.width, height: UIScreen.main.bounds.size.width)
                    imageManager.requestImage(for: asset, targetSize: targetSize, contentMode: .default, options: options, resultHandler: { (image, info) in
                       
                             self.arrayImages.append(image!)
                            self.collectionView_Images.isHidden = false
                         self.imageData.append(image!.jpegData(compressionQuality: 0.7)!)
                       
                      
                       
                        // self.imageData.append(image!.pngData()!)
                        //                    self.imagesCollectionviewTopConstraint.constant = 0
                    })
                }
            }
            if self.arrayImages.count > 0{
                
                self.collectionView_Images.isHidden = false
                self.view_UploadPhoto.isHidden = true
                self.lbl_NoImageFound.isHidden = true
            }else{
                self.collectionView_Images.isHidden = true
                self.view_UploadPhoto.isHidden = false
                self.lbl_NoImageFound.isHidden = false
            }
            self.collectionView_Images.reloadData()
        }
    }
    
    
}

extension Add_ClassessViewController: MHDescDelegate{
    func getDescription(desc: String) {
        self.dic_RegisterValue["Description"] = desc
        self.tableView_AddEvents.reloadRows(at: [IndexPath(row: 1, section: 0)], with: UITableView.RowAnimation.none)
        
    }
    
    func TimeFormat24To12(timeString: String){
        
        let dateAsString = timeString
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "HH:mm"
        
        let date = dateFormatter.date(from: dateAsString)
        dateFormatter.dateFormat = "h:mm a"
        let Date12 = dateFormatter.string(from: date!)
        str_Time12hrs = Date12
    }
    
    
}
