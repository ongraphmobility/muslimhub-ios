//
//  LocationOrgViewController.swift
//  Muslim Hub
//
//  Created by Ongraph on 05/07/19.
//  Copyright © 2019 OnGraph. All rights reserved.
//

import UIKit
import GoogleMaps
import Alamofire
import DropDown

class LocationOrgViewController: UIViewController,GMSMapViewDelegate,CLLocationManagerDelegate,UISearchBarDelegate {
    
    @IBOutlet weak var view_mapView:GMSMapView!
    @IBOutlet weak var serach_Loacation:UISearchBar!
    var allLocationdata = [OrganizationDataLists]()
    var arrayMapMarkers = [GMSMarker]()
    var allOragnizationLists = [OrganizationDataLists]()
    var allLocationdataeid = [EidSalahListData]()
    var eidSalah = ""
    var locationManager = CLLocationManager()
    var isSearch = Bool()
    
    
    //MARK:ViewLifeCycle---------------------------------------
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
        serach_Loacation.delegate = self
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestAlwaysAuthorization()
        locationManager.startUpdatingLocation()
        if CLLocationManager.locationServicesEnabled() {
            switch (CLLocationManager.authorizationStatus()) {
            case .notDetermined, .restricted, .denied:
                print("No access")
            case .authorizedAlways, .authorizedWhenInUse:
                print("Access")
            }
        } else {
            print("Location services are not enabled")
        }
        
        self.showCurrentLocation()
        
        if self.eidSalah != "EidSalah"{
            
            for i in 0..<allLocationdata.count {
                let state_marker = GMSMarker()
                state_marker.position = CLLocationCoordinate2D(latitude: allLocationdata[i].latitude!, longitude: allLocationdata[i].longitude!)
                state_marker.title = allLocationdata[i].name ?? ""
                state_marker.snippet = allLocationdata[i].address ?? ""
                state_marker.map = view_mapView
                state_marker.icon = #imageLiteral(resourceName: "masjid_icon_iPhone")
                self.arrayMapMarkers.append(state_marker)
            }
            
        }else{
            for i in 0..<allLocationdataeid.count {
                let state_marker = GMSMarker()
                state_marker.position = CLLocationCoordinate2D(latitude: allLocationdataeid[i].latitude, longitude: allLocationdataeid[i].longitude)
                state_marker.title = allLocationdataeid[i].orgName
                state_marker.snippet = allLocationdataeid[i].address
                state_marker.map = view_mapView
                state_marker.icon = #imageLiteral(resourceName: "masjid_icon_iPhone")
                self.arrayMapMarkers.append(state_marker)
            }
        }
        
    }
    //Location Manager delegates
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        let userLocation = locations.last
        let center = CLLocationCoordinate2D(latitude: userLocation!.coordinate.latitude, longitude: userLocation!.coordinate.longitude)
        
        let camera = GMSCameraPosition.camera(withLatitude: userLocation!.coordinate.latitude, longitude: userLocation!.coordinate.longitude, zoom: 15);
        self.view_mapView.camera = camera
        self.view_mapView.isMyLocationEnabled = true
        
        let marker = GMSMarker(position: center)
        
        print("Latitude :- \(userLocation!.coordinate.latitude)")
        print("Longitude :-\(userLocation!.coordinate.longitude)")
        marker.map = self.view_mapView
        
        marker.title = "Current Location"
        locationManager.stopUpdatingLocation()
        
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        //mark:SearchBar-------------------------------------
     
            var searchTextField:UITextField!
            
        if #available(iOS 13, *) {
            searchTextField  = serach_Loacation.subviews[0].subviews[2].subviews.last as? UITextField
        } else {
            searchTextField = serach_Loacation.subviews[0].subviews.last as? UITextField
        }
               
        searchTextField.layer.cornerRadius = 15
        searchTextField.textAlignment = NSTextAlignment.left
        searchTextField.textColor = UIColor.lightGray
        searchTextField.leftView = nil
        searchTextField.placeholder = "Search"
           searchTextField.textColor = UIColor.white
        searchTextField.rightViewMode = UITextField.ViewMode.always
        
        if searchTextField.responds(to: #selector(getter: UITextField.attributedPlaceholder)) {
            let attributeDict = [NSAttributedString.Key.foregroundColor: UIColor.white]
            searchTextField.attributedPlaceholder = NSAttributedString(string: "Search", attributes: attributeDict)
        }
    }
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        
        if self.eidSalah == "EidSalah"{
            
            self.getEidsalhaListApi()
            
        }else{
            self.organizationListApi()
        }
    }
    
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
        
        self.serachOrganizationListApi()
    }
    func showCurrentLocation() {
        // view_mapView.settings.myLocationButton = true
        let locationObj = locationManager.location as! CLLocation
        let coord = locationObj.coordinate
        let lattitude = coord.latitude
        let longitude = coord.longitude
        print(" lat in  updating \(lattitude) ")
        print(" long in  updating \(longitude)")
        view_mapView.isMyLocationEnabled = true
        let center = CLLocationCoordinate2D(latitude: locationObj.coordinate.latitude, longitude: locationObj.coordinate.longitude)
        let marker = GMSMarker()
        marker.position = center
        marker.title = "current location"
        marker.map = view_mapView
        
        

        
    }
    
    
    //Mark:- Tap Function
    func mapView(_ mapView: GMSMapView, didTap marker: GMSMarker) -> Bool {
        
        print(self.arrayMapMarkers.index(of: marker) as Any)
        view_mapView.selectedMarker = marker
     
        return true
    }
    func mapView(_ mapView: GMSMapView, didTapInfoWindowOf marker: GMSMarker){
        if self.eidSalah != "EidSalah"{
            
            
            if (self.arrayMapMarkers.index(of: marker) ?? 0 < self.arrayMapMarkers.count){
                let vc = self.storyboard?.instantiateViewController(withIdentifier: "ActivitiesViewController") as! ActivitiesViewController
                let index = (self.arrayMapMarkers.index(of: marker)) ?? 0
               
                if isSearch == true{
                    if (index < self.arrayMapMarkers.count){
                        let ghfgh = self.allOragnizationLists[index].id
                        vc.orgId = ghfgh
                        vc.modalPresentationStyle = .fullScreen
                        self.present(vc, animated: true, completion: nil)
                        
                    }else{
                        
                    }
                }else{
                    if (index < self.allLocationdata.count){
                        let ghfgh = self.allLocationdata[index].id
                        vc.orgId = ghfgh
                        vc.modalPresentationStyle = .fullScreen
                        self.present(vc, animated: true, completion: nil)
                        
                    }else{
                        
                    }
                }
               
            }
        }
    }
    
    //MARK:IBAction------------------------------
    @IBAction func btn_Back(sender:UIButton){
        
        self.dismiss(animated: true, completion: nil)
    }
    
    func serachOrganizationListApi(){
        isSearch = true
        if self.eidSalah == "EidSalah"{
            
            Network.shared.getListOfSearchEidSalaha(searchKey: self.serach_Loacation.text ?? ""){ (result) in
                
                guard let user = result else {
                    
                    return
                }
                
                self.allLocationdataeid = user!.data
                for i in 0..<self.allLocationdataeid.count {
                    let state_marker = GMSMarker()
                    state_marker.position = CLLocationCoordinate2D(latitude: self.allLocationdataeid[i].latitude, longitude: self.allLocationdataeid[i].longitude)
                    state_marker.title = self.allLocationdataeid[i].orgName
                    state_marker.snippet = self.allLocationdataeid[i].address
                    state_marker.map = self.view_mapView
                    state_marker.icon = #imageLiteral(resourceName: "masjid_icon_iPhone")
                    self.arrayMapMarkers.append(state_marker)
                    let camera: GMSCameraPosition = GMSCameraPosition.camera(withLatitude: self.allLocationdataeid[i].latitude, longitude: self.allLocationdataeid[i].longitude, zoom: Float(10.0))
                    self.view_mapView.animate(to: camera)
                }
                
                
            }
            
        }else{
            Network.shared.getListOfSearchOrganization(searchKey: self.serach_Loacation.text ?? "", isfav: "false") { (result) in
                
                guard let user = result else {
                    
                    return
                }
                
                self.allOragnizationLists = user
                self.arrayMapMarkers = []
                for i in 0..<self.allOragnizationLists.count {
                    let state_marker = GMSMarker()
                    
                    state_marker.position = CLLocationCoordinate2D(latitude: self.allOragnizationLists[i].latitude!, longitude: self.allOragnizationLists[i].longitude!)
                    state_marker.title = self.allOragnizationLists[i].name ?? ""
                    state_marker.snippet = self.allOragnizationLists[i].address ?? ""
                    state_marker.map = self.view_mapView
                    state_marker.icon = #imageLiteral(resourceName: "masjid_icon_iPhone")
                    
                    self.arrayMapMarkers.append(state_marker)
                    let camera: GMSCameraPosition = GMSCameraPosition.camera(withLatitude: self.allOragnizationLists[i].latitude!, longitude: self.allOragnizationLists[i].longitude!, zoom: Float(10.0))
                    self.view_mapView.animate(to: camera)
                    //                    var bounds = GMSCoordinateBounds()
                    //
                    //                        state_marker.position = CLLocationCoordinate2D(latitude:self.allOragnizationLists[i].latitude!, longitude:self.allOragnizationLists[i].longitude!)
                    //                        state_marker.map = self.view_mapView
                    //                        bounds = bounds.includingCoordinate(state_marker.position)
                    //
                    //                    let update = GMSCameraUpdate.fit(bounds, withPadding: 0.0)
                    //                    self.view_mapView.animate(with: update)
                    
                }
                
                
            }
            
        }
        
    }
    func organizationListApi(){
        
        Network.shared.organizationLists() { (result) in
            
            guard let user = result else {
                
                return
            }
            
            self.allOragnizationLists = user
            
        }
    }
    
    func getEidsalhaListApi(){
        
        
        Network.shared.getEidSalahList { (response) in
            self.allLocationdataeid = response!.data
            
            
        }
    }
    
}


