//
//  ActivitiesEventExtension.swift
//  Muslim Hub
//
//  Created by Himanshu Goyal on 31/05/19.
//  Copyright © 2019 OnGraph. All rights reserved.
//

import Foundation
import UIKit
import DropDown
import KVNProgress
import AlamofireImage

//HG
//MARK: - Event Cell Action
extension ActivitiesViewController: EventShowMoreDelegate{
    func showMoreTapped(index: Int, isSelected: Bool) {
        print("Slected: ", isSelected)
        self.arrayEventModel[index].isMoreTapped = isSelected
        self.tableView_Home.reloadRows(at: [IndexPath(row: index, section: 1)], with: .none)
    }
    
    
    @objc func btn_Delete(sender : UIButton!) {
        AKAlertController.alert("Alert", message: "Do you really want to delete this event?", buttons: ["Ok","Cancel"], tapBlock: { (_, _, index) in
            if index == 1 {return}
            let eventId = self.arrayEventModel[sender.tag].id ?? 0
            self.callDeleteEventListItem(eventId: "\(eventId)")
        })
    }
    @objc func btn_Edit(sender : UIButton) {
                let vc = self.storyboard?.instantiateViewController(withIdentifier: "Add_EventViewController") as! Add_EventViewController
          vc.eventDetails = self.arrayEventModel[sender.tag]
                self.present(vc, animated: true, completion: nil)
    }
    
    @objc func btn_shareTextButton(sender: UIButton) {
        // text to share
        let text = "This is some text that I want to share."
        // set up activity view controller
        let textToShare = [ text ]
        let activityViewController = UIActivityViewController(activityItems: textToShare, applicationActivities: nil)
        activityViewController.popoverPresentationController?.sourceView = self.view // so that iPads won't crash
        // exclude some activity types from the list (optional)
        activityViewController.excludedActivityTypes = [ UIActivity.ActivityType.airDrop, UIActivity.ActivityType.postToFacebook ]
        // present the view controller
        self.present(activityViewController, animated: true, completion: nil)
    }
    
    @objc func btn_GoingTapped(sender: UIButton){
        
    }
    @objc func btn_RemainderTapped(sender: UIButton){
        
    }
    @objc func btn_Location(sender: UIButton){
        let lat = self.arrayEventModel[sender.tag].latitude ?? 123.123
        let long = self.arrayEventModel[sender.tag].longitude ?? 123.123
        
        // if GoogleMap installed
        if (UIApplication.shared.canOpenURL(URL(string:"comgooglemaps://")!)) {
            UIApplication.shared.openURL(NSURL(string: "comgooglemaps://?saddr=&daddr=\(lat)),\(long))&directionsmode=driving")! as URL)
        } else {
            UIApplication.shared.openURL(NSURL(string:
                "https://maps.google.com/?q=@\(lat)),\(long))")! as URL)
        }
    }
}
