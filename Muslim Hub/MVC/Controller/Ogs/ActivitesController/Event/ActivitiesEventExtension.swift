//
//  ActivitiesEventExtension.swift
//  Muslim Hub
//
//  Created by Himanshu Goyal on 31/05/19.
//  Copyright © 2019 OnGraph. All rights reserved.
//

import Foundation
import UIKit
import DropDown
import KVNProgress
import AlamofireImage
import EventKit

//HG
//MARK: - Event Cell Action
extension ActivitiesViewController: EventShowMoreDelegate,NotSoGoodCellDelegateOrgClasess{
    func showMoreTapped(index: Int, isSelected: Bool) {
        
        if selectedIndexActivites != nil{
            if selectedIndexActivites == 1{
                print("Slected: ", isSelected)
                self.arrayEventModel[index].isMoreTapped = isSelected
                self.tableView_Home.beginUpdates()
                self.tableView_Home.endUpdates()
                self.tableView_Home.layoutIfNeeded()
               // self.tableView_Home.reloadRows(at: [IndexPath(row: index, section: 1)], with: .none)
            }else if selectedIndexActivites == 4{
                
                print("Slected: ", isSelected)
                self.arrayActivityModel[index].isMoreTapped = isSelected
                self.tableView_Home.beginUpdates()
                self.tableView_Home.endUpdates()
              //  self.tableView_Home.reloadRows(at: [IndexPath(row: index, section: 4)], with: .none)
            }
        }
        
        
      
    }
  
    func moreTapped(index: Int, isSelected: Bool) {
        DispatchQueue.main.async {
           // self.tableView_Home.beginUpdates()
            print("SelectedClassesCell: ", isSelected)
            self.classesModelData.getClassData?[index].isMoreTapped = isSelected
            self.tableView_Home.reloadData()
        }
       
    }
    
    
    @objc func btn_Delete(sender : UIButton!) {
        if selectedIndexActivites != nil{
            if selectedIndexActivites == 1{
                AKAlertController.alert("", message: "Do you really want to delete this event?".localized, buttons: ["Ok".localized,"Cancel".localized], tapBlock: { (_, _, index) in
                            if index == 1 {return}
                            let eventId = self.arrayEventModel[sender.tag].id ?? 0
                            self.callDeleteEventListItem(eventId: "\(eventId)")
                        })
            }else if selectedIndexActivites == 4{
                
                AKAlertController.alert("", message: "Do you really want to delete this activity?".localized, buttons: ["Ok".localized,"Cancel".localized], tapBlock: { (_, _, index) in
                    if index == 1 {return}
                    let eventId = self.arrayActivityModel[sender.tag].id ?? 0
                    self.callDeleteActivityListItem(activityId: "\(eventId)")
                })
            }
        }
        
//        self.showTwoBtnsAlertMsg(message: "Alert", subMsg: "Do you really want to delete this event?", sender: self, completion:
//            { (success) -> Void in
//                if success == true{
//                    let eventId = self.arrayEventModel[sender.tag].id ?? 0
//                    self.callDeleteEventListItem(eventId: "\(eventId)")
//                }
//        })
    

    }
    @objc func btn_DeleteClasses(sender : UIButton!) {
        AKAlertController.alert("", message: "Do you really want to delete this class?".localized, buttons: ["Ok".localized,"Cancel".localized], tapBlock: { (_, _, index) in
            if index == 1 {return}
            let classId = self.classesModelData.getClassData?[sender.tag].id ?? 0
            self.callDeleteClassListItem(classId: "\(classId)")
        })
    }
    @objc func btn_EditClasses(sender : UIButton!) {
      
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "Add_ClassessViewController") as! Add_ClassessViewController
        vc.addClassesDetails = self.classesModelData.getClassData?[sender.tag]
        vc.edit = "true"
        vc.modalPresentationStyle = .fullScreen
        self.present(vc, animated: true, completion: nil)
   
    }
    @objc func btn_Edit(sender : UIButton) {
        
        if selectedIndexActivites != nil{
            if selectedIndexActivites == 1{
                let vc = self.storyboard?.instantiateViewController(withIdentifier: "Add_EventViewController") as! Add_EventViewController
                vc.eventDetails = self.arrayEventModel[sender.tag]
                vc.modalPresentationStyle = .fullScreen
                self.present(vc, animated: true, completion: nil)
            }else if selectedIndexActivites == 4{
                let vc = self.storyboard?.instantiateViewController(withIdentifier: "Add_ActivitiesViewController") as! Add_ActivitiesViewController
                vc.activitesDetails = self.arrayActivityModel[sender.tag]
                vc.modalPresentationStyle = .fullScreen
                self.present(vc, animated: true, completion: nil)
            }
        }
        
    }
    
    @objc func btn_EventshareTextButton(sender: UIButton) {
        // text to share
        let text = "This is some text that I want to share."
        // set up activity view controller
        let textToShare = [ text ]
        let activityViewController = UIActivityViewController(activityItems: textToShare, applicationActivities: nil)
        activityViewController.popoverPresentationController?.sourceView = self.view // so that iPads won't crash
        // exclude some activity types from the list (optional)
        activityViewController.excludedActivityTypes = [ UIActivity.ActivityType.airDrop, UIActivity.ActivityType.postToFacebook ]
        // present the view controller
        self.present(activityViewController, animated: true, completion: nil)
    }
    
    @objc func btn_EventGoingTapped(sender: UIButton){
        
    }
    @objc func btn_EventRemainderTapped(sender: UIButton){
        self.eventMethod(senderTag:sender.tag)
    }
   
    @objc func btn_Location(sender: UIButton){
        
        let lat = self.arrayEventModel[sender.tag].latitude ?? 123.123
        let long = self.arrayEventModel[sender.tag].longitude ?? 123.123
       
        self.location(lat: lat, long: long, title: self.arrayEventModel[sender.tag].address ?? "")
    }
    
    
    //MARK:Activity controller
    func showMoreTappedActivites(index: Int, isSelected: Bool) {
        print("Slected: ", isSelected)
        self.arrayActivityModel[index].isMoreTapped = isSelected
        self.tableView_Home.reloadRows(at: [IndexPath(row: index, section: 1)], with: .none)
    }
    
    
    @objc func btn_DeleteActivites(sender : UIButton!) {
        
        
      
    }

    
    @objc func btn_shareTextButtonActivites(sender: UIButton) {
        // text to share
        let text = "This is some text that I want to share."
        // set up activity view controller
        let textToShare = [ text ]
        let activityViewController = UIActivityViewController(activityItems: textToShare, applicationActivities: nil)
        activityViewController.popoverPresentationController?.sourceView = self.view // so that iPads won't crash
        // exclude some activity types from the list (optional)
        activityViewController.excludedActivityTypes = [ UIActivity.ActivityType.airDrop, UIActivity.ActivityType.postToFacebook ]
        // present the view controller
        self.present(activityViewController, animated: true, completion: nil)
    }
    
    @objc func btn_GoingTappedActivites(sender: UIButton){
        
    }
    @objc func btn_RemainderTappedActivites(sender: UIButton){
        self.eventMethod(senderTag: sender.tag)
    }
    @objc func btn_LocationActivites(sender: UIButton){
        let lat = self.arrayActivityModel[sender.tag].latitude ?? 123.123
        let long = self.arrayActivityModel[sender.tag].longitude ?? 123.123
        
        // if GoogleMap installed
        if (UIApplication.shared.canOpenURL(URL(string:"comgooglemaps://")!)) {
            UIApplication.shared.openURL(NSURL(string: "comgooglemaps://?saddr=&daddr=\(lat)),\(long))&directionsmode=driving")! as URL)
        } else {
            UIApplication.shared.openURL(NSURL(string:
                "https://maps.google.com/?q=@\(lat)),\(long))")! as URL)
        }
    }
    @objc func btn_ClassLocation(sender: UIButton){
        let lat = self.classesModelData.getClassData?[sender.tag].latitude ?? 123.123
        let long = self.classesModelData.getClassData?[sender.tag].longitude ?? 123.123
        
       self.location(lat: lat, long: long, title: "")
    }
    @objc func btn_OrgAboutUsLocation(sender: UIGestureRecognizer){
        let lat = self.arrayAboutUsOrgData?[0].latitude ?? 123.123
        let long = self.arrayAboutUsOrgData?[0].longitude ?? 123.123
        
       self.location(lat: lat, long: long, title: "")
    }
    
    //MARK:Set Reminder method-----------------------
    func eventMethod(senderTag:Int){
        
       
        
        let eventStore = EKEventStore()
        eventStore.requestAccess(
            to: EKEntityType.event, completion: {(granted, error) in
                if !granted {
                     DispatchQueue.main.sync {
                     AKAlertController.alert("The app is not permitted to access reminders, make sure to grant permission in the settings and try again")
                    //print(error?.localizedDescription as Any)
                        return
                    }
                } else {
                   // print("Access granted")
                    
                  
                    
                    self.createReminder(in: eventStore, reminderText: "", indexPath: senderTag)
                }
        })
    
    }
    func createReminder(in eventStore: EKEventStore, reminderText: String,indexPath:Int) {
        
        let reminder = EKEvent(eventStore: eventStore)
      
        var startTime = ""
       
        var dateOrg = ""
        var endDateOrg = ""
        
        if self.selectedIndexActivites == 0{
            startTime = self.homeList[indexPath].startTime ?? ""
            dateOrg = ((self.homeList[indexPath].startDate ?? "") + " " + startTime)
            endDateOrg = self.homeList[indexPath].endDate ?? ""
            reminder.title = self.homeList[indexPath].title + " \(self.homeList[indexPath].startDate ?? "") To \(endDateOrg)"
        }else if self.selectedIndexActivites == 1{
            startTime = self.arrayEventModel[indexPath].start_time ?? ""
            dateOrg = ((self.arrayEventModel[indexPath].start_date ?? "") + " " + startTime)
            endDateOrg = self.arrayEventModel[indexPath].end_date ?? ""
            reminder.title = self.arrayEventModel[indexPath].title! + " \(self.arrayEventModel[indexPath].start_date ?? "") To \(endDateOrg)"
        }else if self.selectedIndexActivites == 2{
            startTime = self.classesModelData?.getClassData?[indexPath].start_time ?? ""
            dateOrg = ((self.classesModelData?.getClassData?[indexPath].start_date ?? "") + " " + startTime)
            endDateOrg = self.classesModelData?.getClassData?[indexPath].end_date ?? ""
            reminder.title = (self.classesModelData?.getClassData?[indexPath].title)! + " \(self.classesModelData?.getClassData?[indexPath].start_date ?? "") To \(endDateOrg)"
        }else if self.selectedIndexActivites == 4{
            startTime = self.arrayActivityModel[indexPath].start_time ?? ""
            dateOrg = ((self.arrayActivityModel[indexPath].start_date ?? "") + " " + startTime)
            endDateOrg = self.arrayActivityModel[indexPath].end_date ?? ""
            reminder.title = self.arrayActivityModel[indexPath].title! + " \(self.arrayActivityModel[indexPath].start_date ?? "") To \(endDateOrg)"
        }
        
     
     
        reminder.calendar = eventStore.defaultCalendarForNewEvents
        
        
        
        self.stringToDate(dateString: dateOrg)
        let date = self.ReminderDate
        let alarm = EKAlarm(absoluteDate: date)
        reminder.addAlarm(alarm)
        
        
        let earlierDate = date.addingTimeInterval(-3600*24)
        let earlierAlarm = EKAlarm(absoluteDate: earlierDate)
        reminder.addAlarm(earlierAlarm)
        
      reminder.startDate = date.addingTimeInterval(-24 * 60 * 60)
        reminder.endDate = date.addingTimeInterval(-24 * 60 * 60)
        
        do {
            try eventStore.save(reminder, span: .thisEvent, commit: true)
        } catch let error  {
            print("Reminder failed with error \(error.localizedDescription)")
            return
        }
        
        DispatchQueue.main.sync {
            // Create the alert controller
                   let alertController = UIAlertController(title: "Reminder Created Successfully", message: "", preferredStyle: .alert)
                   
                   // Create the actions
                   let okAction = UIAlertAction(title: "OK", style: UIAlertAction.Style.default) {
                       UIAlertAction in
                       NSLog("OK Pressed")
                       //            reminderText = ""
                       //            self.activityIndicator.stopAnimating()
                   }
                   
                   // Add the actions
                   alertController.addAction(okAction)
                   
                   // Present the controller
                   self.present(alertController, animated: true, completion: nil)
        }
      
        
        
    }
}

