//
//  OrgEventWebservice.swift
//  Muslim Hub
//
//  Created by Himanshu Goyal on 31/05/19.
//  Copyright © 2019 OnGraph. All rights reserved.
//

import Foundation
import UIKit
import DropDown
import KVNProgress
import AlamofireImage

extension ActivitiesViewController{
    func callEventListApi(orgId: Int){
        KVNProgress.show()
        Network.shared.getListOfEventInOrganization(orgId: orgId) { (response) in
            KVNProgress.dismiss()
            guard let arrayEvent = response?.data else {
                return
            }
            print(arrayEvent)
            self.arrayEventModel = arrayEvent
            self.tableView_Home.delegate = self
                  self.tableView_Home.dataSource = self
            if self.arrayEventModel.count > 0{
                for i in 0...self.arrayEventModel.count - 1{
                    var arraySelectedImage: [Bool] = []
                    if self.arrayEventModel[i].images!.count > 0{
                        for j in 0...self.arrayEventModel[i].images!.count - 1{
                            if j == 0{
                                arraySelectedImage.append(true)
                            }else{
                                arraySelectedImage.append(false)
                            }
                        }
                    }
                    self.arrayEventModel[i].selectedImage_Array = arraySelectedImage
                    self.arrayEventModel[i].isMoreTapped = false
                }
            }
            
            if self.brachio == 1{
                let indexxx =  self.arrayEventModel.index(where: {$0.id == self.brachTypeId})
                
                  self.tableView_Home.reloadData()
                  if indexxx != nil{
                       if indexxx != 0{
                let index = NSIndexPath(row: indexxx ?? 0, section: 1)
                self.tableView_Home.scrollToRow(at: index as IndexPath, at: .top, animated: true)
                    }}
            }else{
                self.tableView_Home.reloadData()
            }
         
        }
    }
    func callAboutUsApi(orgId: Int){
      KVNProgress.show()
        Network.shared.getListOfAboutUs(orgId: orgId) { (response) in
            KVNProgress.dismiss()
            guard let aboutUsData = response?.aboutUsOrgData else {
                return
            }
            print(aboutUsData)
            self.arrayAboutUsOrgData = aboutUsData
            self.tableView_Home.reloadData()
        }
    }
    
    func EditActivitesAboutUsApi(id: Int, description: String, president: String, gs: String, imam: String, latitude: Double, longitude: Double, address: String, org_email: String, org_contact: String,pre_up_iq: Bool){
      //  KVNProgress.show()
        Network.shared.editactivitesOfAboutUs(id: id, description: description, president: president, gs: gs, imam: imam, latitude: latitude, longitude: longitude, address: address, org_email: org_email, org_contact: org_contact, pre_up_iq: pre_up_iq){ (response) in
         //   KVNProgress.dismiss()
           // self.callAboutUsApi(orgId: self.orgId!)
//            guard let aboutUsData = response else {
//                return
//            }
            if response?.success == true{
                self.showSingleAlertMessage(message: "", subMsg: response!.message!, sender: self, completion:
                             { (success) -> Void in
                                 if success == true{
                                     self.callAboutUsApi(orgId: self.orgId!)
                                 }
                         })
               
              //  AKAlertController.alert("", message: response?.message ?? "")
            }else{
                AKAlertController.alert("", message: response?.message ?? "")
            }
             
             self.tableView_Home.reloadRows(at: [IndexPath(row: 0, section: 5)], with: UITableView.RowAnimation.none)
      // print(aboutUsData)
           
        }
    }
   
    func callDeleteEventListItem(eventId: String){
        KVNProgress.show()
        Network.shared.deleteOrgEventListItem(id: eventId) { (response) in
            KVNProgress.dismiss()
            
            self.showSingleAlertMessage(message: "Success", subMsg: response!.message, sender: self, completion:
                { (success) -> Void in
                    if success == true{
                        self.callEventListApi(orgId: self.orgId!)
                    }
            })
            
//            AKAlertController.alert("Success", message: response!.message, buttons: ["Ok"], tapBlock: { (_, _, index) in
//                self.callEventListApi(orgId: self.orgId!)
//            })
        }
    }
    func callDeleteClassListItem(classId: String){
        KVNProgress.show()
        Network.shared.deleteOrgClassListItem(id: classId) { (response) in
            KVNProgress.dismiss()
            self.showSingleAlertMessage(message: "Success", subMsg: response!.message, sender: self, completion:
                { (success) -> Void in
                    if success == true{
                      self.getClassesDataApi()
                    }
            })

//            AKAlertController.alert("Success", message: response!.message, buttons: ["Ok"], tapBlock: { (_, _, index) in
//                self.getClassesDataApi()
//            })
        }
    }
}
