//
//  Activities+NewsFeedExtension.swift
//  Muslim Hub
//
//  Created by Himanshu Goyal on 24/06/19.
//  Copyright © 2019 OnGraph. All rights reserved.
//

import Foundation
import UIKit
import DropDown
import KVNProgress
import AlamofireImage
import EventKit

extension ActivitiesViewController{
    
    @objc func btnEditTapped_NewsFeed(sender: UIButton){
        
        if self.homeList[sender.tag].category == "announcement"{
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "AddAnnoucementViewController") as! AddAnnoucementViewController
            vc.addNewAnnouncent = "editNewAnnouncentFromOrg"
            vc.orgId = self.orgId
            var dataModel: AnnouncementData! = AnnouncementData()
            let homeData = self.homeList[sender.tag]
            dataModel.id = homeData.id
            dataModel.title = homeData.title
            dataModel.description = homeData.datumDescription
            dataModel.addedAt = homeData.addedAt
            dataModel.images = homeData.images
            dataModel.selectedImage_Array = homeData.selectedImage_Array
            vc.annoucementData = dataModel
            vc.modalPresentationStyle = .fullScreen
            self.present(vc, animated: true, completion: nil)
        }else if self.homeList[sender.tag].category == "event"{
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "Add_EventViewController") as! Add_EventViewController
            var dataModel: EventData! = EventData()
            let homeData = self.homeList[sender.tag]
            dataModel.id = homeData.id
            dataModel.org = homeData.org
            dataModel.title = homeData.title
            dataModel.description = homeData.datumDescription
            dataModel.latitude = homeData.latitude
            dataModel.longitude = homeData.longitude
            dataModel.address = homeData.address
            dataModel.start_date = homeData.startDate
            dataModel.end_date = homeData.endDate
            dataModel.start_time = homeData.startTime
            dataModel.end_time = homeData.endTime
            dataModel.attendees_type = homeData.attendeesType
            dataModel.added_at = homeData.addedAt
            dataModel.images = homeData.images
            dataModel.going_count = homeData.goingCount
            dataModel.event_logo = homeData.eventLogo
            dataModel.selectedImage_Array = homeData.selectedImage_Array
            dataModel.isMoreTapped = homeData.isMoreTapped
            dataModel.repeating_day = homeData.repeatingDay
            vc.eventDetails = dataModel
            vc.modalPresentationStyle = .fullScreen
            self.present(vc, animated: true, completion: nil)
        }else if self.homeList[sender.tag].category == "class"{
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "Add_ClassessViewController") as! Add_ClassessViewController
            var dataModel: GetClassData! = GetClassData()
            let homeData = self.homeList[sender.tag]
            dataModel.id = homeData.id
            dataModel.org = homeData.org
            dataModel.title = homeData.title
            dataModel.description = homeData.datumDescription
            dataModel.latitude = homeData.latitude
            dataModel.longitude = homeData.longitude
            dataModel.address = homeData.address
            dataModel.class_input_type = homeData.classInputType
            dataModel.select_class = homeData.selectClass
            dataModel.user_input = homeData.userInput
            dataModel.start_date = homeData.startDate
            dataModel.end_date = homeData.endDate
            dataModel.start_time = homeData.startTime
            dataModel.end_time = homeData.endTime
            dataModel.attendees_type = homeData.attendeesType
            dataModel.age_group = homeData.ageGroup
            dataModel.added_at = homeData.addedAt
            dataModel.images = homeData.images
            dataModel.teacher_name = homeData.teacherName
            dataModel.free = homeData.free
            dataModel.classes_logo = homeData.classesLogo
            dataModel.selectedImage_Array = homeData.selectedImage_Array
            dataModel.isMoreTapped = homeData.isMoreTapped
            dataModel.repeating_day = homeData.repeatingDay
            vc.addClassesDetails = dataModel
            vc.modalPresentationStyle = .fullScreen
            self.present(vc, animated: true, completion: nil)
        } else if self.homeList[sender.tag].category == "activity"{
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "Add_ActivitiesViewController") as! Add_ActivitiesViewController
            var dataModel: EventData! = EventData()
            let homeData = self.homeList[sender.tag]
            dataModel.id = homeData.id
            dataModel.org = homeData.org
            dataModel.title = homeData.title
            dataModel.description = homeData.datumDescription
            dataModel.latitude = homeData.latitude
            dataModel.longitude = homeData.longitude
            dataModel.address = homeData.address
            dataModel.start_date = homeData.startDate
            dataModel.end_date = homeData.endDate
            dataModel.start_time = homeData.startTime
            dataModel.end_time = homeData.endTime
            dataModel.attendees_type = homeData.attendeesType
            dataModel.added_at = homeData.addedAt
            dataModel.images = homeData.images
            dataModel.going_count = homeData.goingCount
            dataModel.event_logo = homeData.eventLogo
            dataModel.selectedImage_Array = homeData.selectedImage_Array
            dataModel.isMoreTapped = homeData.isMoreTapped
            dataModel.repeating_day = homeData.repeatingDay
            vc.activitesDetails = dataModel
            vc.modalPresentationStyle = .fullScreen
            self.present(vc, animated: true, completion: nil)
        }else if self.homeList[sender.tag].category == "donation"{
            
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "Add_DonationViewController") as! Add_DonationViewController
            var dataModel: DonationData!
            vc.donationData = self.homeList[sender.tag]
            vc.taxDeductable = self.homeList[sender.tag].tax_deductable!
            vc.orgId = self.orgId
            
            vc.modalPresentationStyle = .fullScreen
            self.present(vc, animated: true, completion: nil)
            
        }else{
            
        }
    }
    
    @objc func btnDeleteTapped_NewsFeed(sender: UIButton){
        
        if self.homeList[sender.tag].category == "announcement"{
            AKAlertController.alert("", message: "Do you really want to delete this announcement?".localized, buttons: ["Ok".localized,"Cancel".localized], tapBlock: { (_, _, index) in
                if index == 1 {return}
                let announcementId = self.homeList[sender.tag].id ?? 0
                self.callDeleteNewsFeedListItem(id: "\(announcementId)", type: self.homeList[sender.tag].category)
            })
        }else if self.homeList[sender.tag].category == "event"{
            AKAlertController.alert("", message: "Do you really want to delete this event?".localized, buttons: ["Ok".localized,"Cancel".localized], tapBlock: { (_, _, index) in
                if index == 1 {return}
                let eventId = self.homeList[sender.tag].id ?? 0
                self.callDeleteNewsFeedListItem(id: "\(eventId)", type: self.homeList[sender.tag].category)
            })
        }else if self.homeList[sender.tag].category == "class"{
            AKAlertController.alert("", message: "Do you really want to delete this class?".localized, buttons: ["Ok".localized,"Cancel".localized], tapBlock: { (_, _, index) in
                if index == 1 {return}
                let classId = self.homeList[sender.tag].id ?? 0
                self.callDeleteNewsFeedListItem(id: "\(classId)", type: self.homeList[sender.tag].category)
            })
        } else if self.homeList[sender.tag].category == "activity"{
            AKAlertController.alert("", message: "Do you really want to delete this activity?".localized, buttons: ["Ok".localized,"Cancel".localized], tapBlock: { (_, _, index) in
                if index == 1 {return}
                let activityId = self.homeList[sender.tag].id ?? 0
                self.callDeleteNewsFeedListItem(id: "\(activityId)", type: self.homeList[sender.tag].category)
            })}else if self.homeList[sender.tag].category == "donation"{
            AKAlertController.alert("", message: "Do you really want to delete this donation?".localized, buttons: ["Ok".localized,"Cancel".localized], tapBlock: { (_, _, index) in
                if index == 1 {return}
                let donationId = self.homeList[sender.tag].id ?? 0
                self.callDeleteNewsFeedListItem(id: "\(donationId)", type: self.homeList[sender.tag].category)
            })
        }else{
            
        }
    }
}

//MARK: - News Feeds Webservice
extension ActivitiesViewController{
    func callDeleteNewsFeedListItem(id: String, type: String){
        KVNProgress.show()
        if type == "announcement"{
            Network.shared.deleteOrgAnnouncementListItem(id: Int(id)!, orgId: self.orgId!) { (response) in
                KVNProgress.dismiss()
                if response!.success{
                    self.showSingleAlertMessage(message: "Success", subMsg: response!.message, sender: self, completion:
                        { (success) -> Void in
                            if success == true{
                                self.HomeListApi(keyAll: "")
                            }
                    })
                }else{
                    self.showSingleAlertMessage(message: "", subMsg: response!.message, sender: self, completion:
                        { (success) -> Void in
                    })
                }
            }
        }else if type == "event"{
            Network.shared.deleteOrgEventListItem(id: id) { (response) in
                KVNProgress.dismiss()
                if response!.success{
                    self.showSingleAlertMessage(message: "Success", subMsg: response!.message, sender: self, completion:
                        { (success) -> Void in
                            if success == true{
                                self.HomeListApi(keyAll: "")
                            }
                    })
                }else{
                    self.showSingleAlertMessage(message: "", subMsg: response!.message, sender: self, completion:
                        { (success) -> Void in
                    })
                }
            }
        }else if type == "class"{
            Network.shared.deleteOrgClassListItem(id: id) { (response) in
                KVNProgress.dismiss()
                self.showSingleAlertMessage(message: "Success", subMsg: response!.message, sender: self, completion:
                    { (success) -> Void in
                        if success == true{
                            self.HomeListApi(keyAll: "")
                        }
                })
            }
        } else if type == "activity"{
            Network.shared.deleteOrgActivityListItem(id: id) { (response) in
                KVNProgress.dismiss()
                if response!.success{
                    AKAlertController.alert("Success", message: response!.message, buttons: ["Ok"], tapBlock: { (_, _, index) in
                        self.HomeListApi(keyAll: "")
                    })
                }else{
                    self.showSingleAlertMessage(message: "", subMsg: response!.message, sender: self, completion:
                        { (success) -> Void in
                    })
                }
            }
        }else if type == "donation"{
            Network.shared.deleteOrgDoantionListItem(id: id) { (response) in
                KVNProgress.dismiss()
                if response!.success{
                    self.showSingleAlertMessage(message: "", subMsg: response!.message, sender: self, completion:
                                          { (success) -> Void in
                                             self.HomeListApi(keyAll: "")
                                      })
//                    AKAlertController.alert("Success", message: response!.message, buttons: ["Ok"], tapBlock: { (_, _, index) in
//                       
//                    })
                }else{
                    self.showSingleAlertMessage(message: "", subMsg: response!.message, sender: self, completion:
                        { (success) -> Void in
                    })
                }
            }
        }else{
            
        }
    }
}
