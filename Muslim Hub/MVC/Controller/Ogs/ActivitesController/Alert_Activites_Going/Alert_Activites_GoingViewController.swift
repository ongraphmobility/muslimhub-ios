//
//  Alert_Activites_GoingViewController.swift
//  Muslim Hub
//
//  Created by Ongraph on 11/06/19.
//  Copyright © 2019 OnGraph. All rights reserved.
//

import UIKit
import KVNProgress

class Alert_Activites_GoingViewController: UIViewController {

    @IBOutlet weak var txt_men:UITextField!
    @IBOutlet weak var txt_women:UITextField!
    @IBOutlet weak var txt_children:UITextField!
    @IBOutlet weak var txt_name:UITextField!
    @IBOutlet weak var btn_Cancel:UIButton!
    @IBOutlet weak var btn_Save:UIButton!
    @IBOutlet weak var view_Name:UIView!
    @IBOutlet weak var btn_rsvp:UIButton!
   
    var category:String!
    weak var weakSelf : HomeViewController?
     weak var weakSelfNewsFeed : ActivitiesViewController?
    var FromActivitesController:String!
    var menCount:Int!
    var id = Int()
    var womenCount:Int!
   var childrenCount:Int!
   var onlyCountSee:String!
    var orgid = Int()
    var attendesType = Int()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
         btn_rsvp.isHidden = true
          if onlyCountSee == "onlyCountSee"{
            self.btn_Cancel.isHidden = true
            self.view_Name.isHidden = true
            self.btn_Save.setTitle("OK", for: .normal)
            self.txt_women.isUserInteractionEnabled = false
            self.txt_children.isUserInteractionEnabled = false
            self.txt_men.isUserInteractionEnabled = false
            if menCount != nil && menCount != 0 {
                self.txt_men.text = ("\(String(describing: menCount!))")
            }
            if childrenCount != nil && childrenCount != 0 {
                self.txt_children.text = ("\(String(describing: childrenCount!))")
            }
            if womenCount != nil && womenCount != 0{
                self.txt_women.text = ("\(String(describing: womenCount!))")
            }
               btn_rsvp.isHidden = false
             self.FirstPermission()
        }
          else{
            
            if attendesType == 1{
                self.txt_women.isUserInteractionEnabled = false
                self.txt_children.isUserInteractionEnabled = false
                 self.txt_men.isUserInteractionEnabled = true
            }else if attendesType == 2{
                self.txt_men.isUserInteractionEnabled = false
                self.txt_children.isUserInteractionEnabled = false
                   self.txt_women.isUserInteractionEnabled = true
            }else if attendesType == 2{
                self.txt_women.isUserInteractionEnabled = true
                self.txt_children.isUserInteractionEnabled = true
                self.txt_men.isUserInteractionEnabled = true
            }
       
         
            self.btn_Save.setTitle("Save", for: .normal)
            self.view_Name.isHidden = false
            self.btn_Cancel.isHidden = false
            getGoingCountAPi()
               btn_rsvp.isHidden = true
        }
       
      
       
    }
    
    @IBAction func btn_BAck(sender:UIButton){
        
        self.presentationController?.presentedViewController.dismiss(animated: true, completion: nil)
    }
    
    
    @IBAction func btn_Save(sender:UIButton){
        
        if onlyCountSee == "onlyCountSee"{
            self.btn_Save.setTitle("OK", for: .normal)
           self.presentationController?.presentedViewController.dismiss(animated: true, completion: nil)
        }else{
               self.btn_Save.setTitle("Save", for: .normal)
            if category == "Event"{
                self.goingCountApi()
            }else{
                self.self.goingCountActivityApi()
            }
        }
      
   
    }
    
    @IBAction func btn_RSVPExport(sender:UIButton){
        
        self.getrsvApi()
    }
    
    
    func FirstPermission(){
        if (UserDefaults.standard.value(forKey: "Login_Token") as? String) != nil{
            
            if let superAdmin = UserDefaults.standard.value(forKey: "isSuperAdmin") as? Bool{
                
                if superAdmin == true{
                    btn_rsvp.isHidden = false
                  
                }else{
                    
                    if let is_staff = UserDefaults.standard.value(forKey: "is_staff") as? Bool{
                        
                        if is_staff == true{
                            //mark:check For own Organisation---------------------------
                            if let orgOwn_Array = UserDefaults.standard.value(forKey: "OwnOrg_Array") as? [Int]{
                                
                                
                                if orgOwn_Array.contains(orgid){
                                    
                                    var org_P = [PriviligesData]()
                                    org_P = Global.shared.priviligesData
                                    let filertedData = org_P.filter{$0.org_id == orgid ?? 0}
                                    filertedData.forEach {
                                        //   print("model data objects::",$0.org_perm as? String)
                                        
                                        if $0.org_perm == "iqama_only"{
                                             btn_rsvp.isHidden = true
                                        }else{
                                             btn_rsvp.isHidden = false
                                        }
                                    
                                    }
                                    
                                }
                                else{
                                    self.btn_rsvp.isHidden = true
                                }//orgOwn
                                
                            }
                        }//staff true
                    }//staff
                    
                }
            }//SuperAdmin
        }//Login
        else{
           btn_rsvp.isHidden = true
        }
        
        
        
}

    //MARK:rsvp get Api-------------------------------------------
    func getrsvApi(){
        
        var types = String()
        if category == "Event"{
            types = "event"
        }else{
            types = "activity"
            
        }
        KVNProgress.show()
        Network.shared.getrsvp(types:types, id: id) { (result) in
             KVNProgress.dismiss()
            guard let user = result else {
                return
            }
            print(user)
            self.showSingleAlertMessage(message: "", subMsg: user.message ?? "", sender: self, completion:
                { (success) -> Void in
                    if success == true{
                        
                       self.presentationController?.presentedViewController.dismiss(animated: true, completion: nil)
                    }
            })
        }
        
    }
    
    
    
    //MARK:Function Going Count Api--------------------------------
    
    func getGoingCountAPi(){
        var types = String()
        if category == "Event"{
            types = "event"
        }else{
           types = "activity"
            
        }
        Network.shared.getGoingCount(types:types,id: id) { (result) in
            
            guard let user = result else {
                return
            }
            if user.women != nil && user.women != 0 {
                self.txt_women.text = ("\(String(describing: user.women!))")
            }
            if user.children != nil && user.children != 0 {
                self.txt_children.text = ("\(String(describing: user.children!))")
            }
            if user.men != nil && user.men != 0{
                self.txt_men.text = ("\(String(describing: user.men!))")
            }
            if user.name != nil && user.men != 0{
                self.txt_name.text = user.name ?? ""
            }
            
          
        }
    }
    func goingCountActivityApi(){
        
        if txt_name.text == "" {
            
            AKAlertController.alert("Please enter name")
        }else{
            let men = Int(txt_men.text ?? "") //?? 0
            let women = Int(txt_women.text ?? "") //?? 0
            let children = Int(txt_children.text ?? "") //?? 0
            
            Network.shared.goingCountActivity(activity: id , men: men ?? 0, women: women ?? 0, children: children ?? 0,name: self.txt_name.text ?? ""){ (result) in
                
                guard result != nil else {
                    
                    return
                }
                self.showSingleAlertMessage(message: "", subMsg: result?.message ?? "Going Count Updated", sender: self, completion:
                    { (success) -> Void in
                        if success == true{
                            
                            self.presentationController?.presentedViewController.dismiss(animated: true, completion: {
                                if self.FromActivitesController == "FromActivitesController"{
                                    self.weakSelfNewsFeed?.viewWillAppear(true)
                                }else{
                                    self.weakSelf?.viewWillAppear(true)
                                }
                                
                                
                            })
                        }
                })
                //  self.presentationController?.presentedViewController.dismiss(animated: true, completion: nil)
            }
        }
      
    }
    func goingCountApi(){
        if txt_name.text == "" {
            
            AKAlertController.alert("Please enter name")
        }else{
        let men = Int(txt_men.text ?? "")
        let women = Int(txt_women.text ?? "")
        let children = Int(txt_children.text ?? "")
        
            Network.shared.goingCountEvent(event: id , men: men ?? 0, women: women ?? 0, children: children ?? 0,name: self.txt_name.text ?? ""){ (result) in
            
            guard result != nil else {
                
                return
            }
            self.showSingleAlertMessage(message: "", subMsg: result?.message ?? "Going Count Updated", sender: self, completion:
                { (success) -> Void in
                    if success == true{
                        
                        self.presentationController?.presentedViewController.dismiss(animated: true, completion: {
                            if self.FromActivitesController == "FromActivitesController"{
                                self.weakSelfNewsFeed?.viewWillAppear(true)
                            }else{
                                self.weakSelf?.viewWillAppear(true)
                            }
                        })
        }
            })
    }
        }
}
}
