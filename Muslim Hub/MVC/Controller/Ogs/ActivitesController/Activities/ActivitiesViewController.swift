//
//  ActivitiesViewController.swift
//  Muslim Hub
//
//  Created by Sakshi Singh on 15/04/19.
//  Copyright © 2019 OnGraph. All rights reserved.
//

import UIKit
import DropDown
import KVNProgress
import AlamofireImage
import GooglePlaces
import CountryPickerViewSwift
import SDWebImage
import MessageUI
import EventKit
import SKPhotoBrowser
import SlideMenuControllerSwift
import Branch
import ActiveLabel
import TTTAttributedLabel



class ActivitiesViewController: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate, UITextViewDelegate, UITextFieldDelegate,UISearchBarDelegate,ActiveLabelDelegate {
    func didSelect(_ text: String, type: ActiveType) {
        print("")
    }
    
    
    @IBOutlet weak var tableView_Home:UITableView!
    @IBOutlet weak var collectionView_Org:UICollectionView!
    @IBOutlet weak var btn_Moree:UIButton!
    var dispatchGroup = DispatchGroup()
    var aboutus_Addres = ""
    //mark:Iboutlets for  org details
    @IBOutlet weak var txt_Abbreviation:UITextField!
    @IBOutlet weak var btn_EditAbbre:UIButton!
    @IBOutlet weak var txt_Name:UITextField!
    @IBOutlet weak var btn_EditName:UIButton!
    @IBOutlet weak var btn_FollowUs:UIButton!
    @IBOutlet weak var collectionView_height:NSLayoutConstraint!
    @IBOutlet weak var img_profile: UIImageView!
    @IBOutlet weak var img_banner: UIImageView!
    @IBOutlet weak var btn_profile: UIButton!
    @IBOutlet weak var btn_banner: UIButton!
    @IBOutlet weak var btn_Previous: UIButton!
    @IBOutlet weak var btn_Next: UIButton!
    @IBOutlet weak var view_noDatafound: UIView!
    @IBOutlet weak var view_TopHeader:UIView!
    @IBOutlet weak var img_Flag: UIImageView!
    var imageSelectedView = 0
    var choosenImg = UIImage()
    var selectedimg: UIImage!
    var picker = UIImagePickerController()
    var imageData:Data!
    var isImageUpdate = Bool()
    var dic_RegisterValue = [String:Any]()
    var tag_forAddress:Int!
    var countrycode:String!
    var allFavOrg:String!
    var array_allfavOrgId = [Int]()
    var isEditModeOnOFF = false
    var selectedIndexActivites:Int! = 0
    var orgId:Int!
    var orgDetails:OrgDetailsData!
    var favbtnSelectedtag:Int!
    var isfavbtnSelectedt:String!
    var refreshControl = UIRefreshControl()
    
    var lat:Double!
    var long:Double!
    var fullPhoneNumber:String!
    var remaingData = [String]()
    var goingCountCateogry:String!
    var tableViewSection:Int = 0
    var homeList = [HomeFeedData]()
    var iqamaListNewsFeed : IQAMAListModel!
    var isMessageCancel:Bool?
    var iqamaListsData : IQAMALists!
    var ReminderDate = Date()
    var org_type = String()
    //mark:IBOutlets for services Images----------------------------
    @IBOutlet weak var btn_service1: UIButton!
    @IBOutlet weak var btn_service2: UIButton!
    @IBOutlet weak var btn_service3: UIButton!
    @IBOutlet weak var btn_service4: UIButton!
    @IBOutlet weak var btn_service5: UIButton!
    @IBOutlet weak var btn_service6: UIButton!
    @IBOutlet weak var btn_service7: UIButton!
    @IBOutlet weak var btn_servicesMore: UIButton!
    
    
    @IBOutlet weak var collectionView_Services: UICollectionView!
    @IBOutlet weak var stackView_Services: UIStackView!
    @IBOutlet weak var btn_Add_EditServices: UIButton!
    @IBOutlet weak var btn_Add_Activites: UIButton!
    
    @IBOutlet weak var btn_Heart:UIButton!
    @IBOutlet weak var btn_Flagg:UIButton!
    
    //marK:EditMode Button----------------------------
    @IBOutlet weak var view_EditMode:UIView!
    @IBOutlet weak var btn_ON:UIButton!
    @IBOutlet weak var btn_OFF:UIButton!
    //Lanaguage
    @IBOutlet weak var lang_EditModeTitle:UILabel!
       
    
    //mark:IBoutlet for Searching
    @IBOutlet weak var searchBar:UISearchBar!
    @IBOutlet weak var btn_searchBar:UIButton!
    var isSerachBarTapped: Bool!
    
    var array_Activities = ["NEWS FEED","EVENTS","CLASSES","IQAMAH","ACTIVITIES","ABOUT US"]
    
    
    //HG
    var arrayEventModel                     : [EventData]!  = []
    var arrayActivityModel                  : [EventData]!  = []
    //As
    var arrayAboutUsOrgData                     : [AboutUsOrgData]! = []
    
    var classesModelData:GetClassModel!
    
    var presidentTxt = String()
    var gsTxt = String()
    var imamTxt = String()
    
    // For use Set remider Data
    var eventStore = EKEventStore()
    var calendars:Array<EKCalendar> = []
    
    var orgOwnArray = [Int]()
    var iqamaInnertableviewHeight:CGFloat = 0.0
    
    //Mark: variables for branch and Share features--------
    var brachio = 0
    var shareTime = String()
    var shareDate = String()
    var brachTypeId = Int()
    var IqamaShareTime = String()
    
    //mark:phone number detction and url in description
    var phoneNumber:String!
    var urlPhoneNumber:String!
    var array_String = [String]()
    
    
   
    
    
    
    //Mark: View Methods-----------------------------------------------------
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
        
        //lanaguae change
        
        self.lang_EditModeTitle.text = "Edit Mode".localized
       
         self.btn_ON.setTitle("ON".localized, for: .normal)
         self.btn_OFF.setTitle("OFF".localized, for: .normal)
        self.btn_Add_Activites.setTitle("Add".localized, for: .normal)
        self.btn_Add_EditServices.setTitle("Add Services".localized, for: .normal)
        
        
        statusBarColorChange(color:UIColor.init(hex: 0x0DC9E4))
        picker.delegate=self
        
        self.buttoncornerRadiusShadow() //edit mode button
        self.hideKeyboardWhenTappedAround()
        
        self.tableView_Home.rowHeight = UITableView.automaticDimension
        self.tableView_Home.estimatedRowHeight = 200
        self.btn_Add_EditServices.isHidden = true
        self.searchBar.isHidden = true
        
        img_profile.clipsToBounds = true
        img_profile.layer.masksToBounds = false
        
        //mark:Flag for missing items-----------------------
        let jeremyGif = UIImage.gifImageWithName("red_flag_iPhone")
        self.img_Flag.image = jeremyGif
        
        isEditModeOnOFF = false
        
        
        self.tableView_Home.register(UINib(nibName: "OrgEventListTableViewCell", bundle: nil), forCellReuseIdentifier: "OrgEventListTableViewCell")
        
        self.tableView_Home.register(UINib(nibName: "IqamahListTableViewCell", bundle: nil), forCellReuseIdentifier: "IqamahListTableViewCell")
        
        self.tableView_Home.register(UINib(nibName: "NewsFeedIqamTableViewCell", bundle: nil), forCellReuseIdentifier: "NewsFeedIqamTableViewCell")
        
        
        
        self.isMessageCancel = true
        
        //mark:notification delegate
        if AppDelegate.instance.notification_OrgId.contains(self.orgId){
            
            AppDelegate.instance.notification_OrgId.removeAll{$0 == self.orgId}
        }
        
        UserDefaults.standard.set(false, forKey:"isJummaShowMore")
        UserDefaults.standard.set(false, forKey:"isNewsJummaShowMore")
        
        self.btn_Flagg.isHidden = true
        self.img_Flag.isHidden = true
        
        
        if let langugae = UserDefaults.standard.value(forKey: Constant.LANGUAGE) as? String{
            
            if langugae == "ar"{
                
                btn_Add_Activites.semanticContentAttribute = .forceRightToLeft
            }else if langugae == "ms"{
              btn_Add_Activites.semanticContentAttribute = .forceRightToLeft
            }else{
                btn_Add_Activites.semanticContentAttribute = .forceRightToLeft
            }
        }else{
              btn_Add_Activites.semanticContentAttribute = .forceRightToLeft
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        //mark: services shown-----------------
        
        self.view_noDatafound.isHidden = true
        searchBar.delegate = self
        self.isSerachBarTapped = false
        // UIApplication.statusBarBackgroundColor = UIColor.init(hex: 0x0DC9E4)
        if isMessageCancel == true{
            
            
            self.btn_service1.isHidden = true
            self.btn_service2.isHidden = true
            self.btn_service3.isHidden = true
            self.btn_service4.isHidden = true
            self.btn_service5.isHidden = true
            self.btn_service6.isHidden = true
            self.btn_service7.isHidden = true
            self.btn_servicesMore.isHidden = true
            
            btn_EditName.isHidden = true
            btn_EditAbbre.isHidden = true
            btn_banner.isHidden = true
            btn_profile.isHidden = true
            
            //mark:check for next and previous button coming from org lists---------------
            if allFavOrg != "AllFavOrg"{
                btn_Previous.isHidden = true
                btn_Next.isHidden = true
            }else{
                btn_Previous.isHidden = false
                btn_Next.isHidden = false
            }
            
            /*
             if isImageUpdate == false{
             //                if self.brachio != 1{
             //                          dispatchGroup.enter()
             //                      }
             //
             
             self.orgDetailsApi()
             self.dispatchGroup.notify(queue: .main) {
             
             self.iqamaListsApi()
             }
             
             }
             */
            
            self.callFlagApiList(isCallFrom: "")
            
            
//            self.dispatchGroup.notify(queue: .main) {
//                self.iqamaListsApi()
//            }
            
            self.img_banner.contentMode = .scaleAspectFill
            img_banner.clipsToBounds = true
            
//            if self.selectedIndexActivites != nil{
//                if self.selectedIndexActivites == 0{
//                    self.HomeListApi(keyAll: "")
//
//                }else if self.selectedIndexActivites == 1{
//                    self.callEventListApi(orgId: self.orgId!)
//                }else if self.selectedIndexActivites == 2{
//                    self.getClassesDataApi()
//                }else if self.selectedIndexActivites == 3 {
//                    self.iqamaListsApi()
//                }else if self.selectedIndexActivites == 4{
//                    self.callListOfActivitiesInOrganization(orgId: self.orgId!)
//                }else if self.selectedIndexActivites == 5{
//                    self.callAboutUsApi(orgId: self.orgId!)
//                }
//                self.collectionView_Org.reloadData()
//            }
            
            // tableView_Home.reloadData()
            
        }else{
            
            self.isMessageCancel = true
            print("Messagecancel")
            
        }
        
        // Register to receive notification in your iqama class
        NotificationCenter.default.addObserver(self, selector: #selector(self.showIqamaLists(_:)), name: NSNotification.Name(rawValue: "iqamaReloadData"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.showIqamaListsForNewsFeed(_:)), name: NSNotification.Name(rawValue: "iqamaNewsFeedReloadData"), object: nil)
        
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        //mark:SearchBar-------------------------------------
        var searchTextField:UITextField!
        
        if #available(iOS 13, *) {
            searchTextField  = searchBar.subviews[0].subviews[2].subviews.last as? UITextField
        } else {
            searchTextField = searchBar.subviews[0].subviews.last as? UITextField
        }
        
        searchTextField.layer.cornerRadius = 15
        searchTextField.textAlignment = NSTextAlignment.left
        searchTextField.textColor = UIColor.white
        searchTextField.leftView = nil
        // searchTextField.placeholder = "Search"
        
        searchTextField.rightViewMode = UITextField.ViewMode.always
        
        if searchTextField.responds(to: #selector(getter: UITextField.attributedPlaceholder)) {
            let attributeDict = [NSAttributedString.Key.foregroundColor: UIColor.white]
            searchTextField.attributedPlaceholder = NSAttributedString(string: "Search".localized, attributes: attributeDict)
        }
    }
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        //searchBar.showsCancelButton = true
        
    }
    
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
        
        self.serachApi()
    }
    @IBAction func btn_search(sender:UIButton){
        
        if sender.isSelected == true{
            sender.isSelected = false
            searchBar.isHidden = true
        }else{
            sender.isSelected = true
            searchBar.isHidden = false
        }
        
    }
    func FirstPermission(){
        if (UserDefaults.standard.value(forKey: "Login_Token") as? String) != nil{
            
            if let superAdmin = UserDefaults.standard.value(forKey: "isSuperAdmin") as? Bool{
                
                if superAdmin == true{
                    btn_Moree.isHidden = false
                    btn_Add_Activites.isHidden = false
                    self.view_EditMode.isHidden = false
                    if self.remaingData == []{
                        self.btn_Flagg.isHidden = true
                        self.img_Flag.isHidden = true
                        
                    }else{
                        self.btn_Flagg.isHidden = false
                        self.img_Flag.isHidden = false
                        
                    }
                    self.collectionView_height.constant = 0
                }else{
                    
                    if let is_staff = UserDefaults.standard.value(forKey: "is_staff") as? Bool{
                        
                        if is_staff == true{
                            //mark:check For own Organisation---------------------------
                            if let orgOwn_Array = UserDefaults.standard.value(forKey: "OwnOrg_Array") as? [Int]{
                                
                                
                                if orgOwn_Array.contains(orgId){
                                    self.collectionView_height.constant = 0.0
                                    var org_P = [PriviligesData]()
                                    org_P = Global.shared.priviligesData
                                    let filertedData = org_P.filter{$0.org_id == orgId}
                                    filertedData.forEach {
                                        //   print("model data objects::",$0.org_perm as? String)
                                        
                                        if $0.org_perm == "iqama_only"{
                                            self.collectionView_height.constant = 0
                                            self.view_EditMode.isHidden = true
                                            btn_Moree.isHidden = true
                                            btn_Add_Activites.isHidden = false
                                            btn_Add_EditServices.isHidden = true
                                            if self.remaingData == []{
                                                btn_Flagg.isHidden = true
                                                img_Flag.isHidden = true
                                            }else{
                                                btn_Flagg.isHidden = false
                                                img_Flag.isHidden = false
                                            }
                                        }else{
                                            self.view_EditMode.isHidden = false
                                            btn_Add_EditServices.isHidden = false
                                            btn_Moree.isHidden = true
                                            self.collectionView_height.constant = 0
                                            btn_Add_Activites.isHidden = false
                                            if self.remaingData == []{
                                                self.btn_Flagg.isHidden = true
                                                self.img_Flag.isHidden = true
                                            }else{
                                                self.btn_Flagg.isHidden = false
                                                self.img_Flag.isHidden = false
                                            }
                                        }
                                    }
                                    self.btn_Heart.isUserInteractionEnabled = false
                                    
                                }else{
                                    self.view_EditMode.isHidden = true
                                    btn_Flagg.isHidden = true
                                    img_Flag.isHidden = true
                                    btn_Moree.isHidden = true
                                    btn_Add_Activites.isHidden = true
                                    self.collectionView_height.constant = 30
                                    self.view_TopHeader.height = 350.0
                                    self.tableView_Home.layoutIfNeeded()
                                    self.btn_Heart.isUserInteractionEnabled = true
                                }//orgOwn
                                
                            }
                        }//staff true
                    }//staff
                    
                }
            }//SuperAdmin
        }//Login
        else{
            btn_Flagg.isHidden = true
            img_Flag.isHidden = true
            btn_Moree.isHidden = true
            btn_Add_Activites.isHidden = true
            self.view_EditMode.isHidden = true
            self.collectionView_height.constant = 30
            self.view_TopHeader.height = 350.0
            self.tableView_Home.layoutIfNeeded()
        }
        
        
        
    }
    /* //MARK: text view Delegate and datatsources---------------------
     func textViewDidBeginEditing(_ textView: UITextView) {
     guard let indexPath = textView.tableViewIndexPath(self.tableView_Home) else { return }
     
     switch (indexPath.section, 0) {
     
     case (5,0):
     
     let vc = self.storyboard?.instantiateViewController(withIdentifier: "MHDescViewController") as! MHDescViewController
     vc.delegate = self
     vc.textDesc = textView.text!
     textView.keyboardType = .default
     present(vc, animated: false, completion: nil)
     default:
     break
     
     }
     
     }*/
    func moveToMHDescViewController(cell: OrgsAboutUsTableCell){
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "MHDescViewController") as! MHDescViewController
        vc.delegate = self
        vc.textDesc = cell.txtDesc_View.text
        vc.modalPresentationStyle = .fullScreen
        cell.btn_DescEdit.isSelected = false
        present(vc, animated: false, completion: nil)
    }
    //Mark:Function And Method---------------------------------------
    func openAutoComplete(){
        let autocompleteController = GMSAutocompleteViewController()
        autocompleteController.delegate = self
        let filter = GMSAutocompleteFilter()
        filter.type = .noFilter  //suitable filter type
        // filter.country = "AUS"  //appropriate country code
        autocompleteController.autocompleteFilter = filter
        self.tag_forAddress = 0
        
        self.aboutus_Addres = "true"
        present(autocompleteController, animated: true, completion: nil)
    }
    
    func showCountryPickerView() {
        let countryView = CountrySelectView.shared
        countryView.barTintColor = .red
        countryView.displayLanguage = .english
        countryView.show()
        countryView.selectedCountryCallBack = { (countryDic) -> Void in
            self.countrycode = "+\(countryDic["code"] as! NSNumber)"
            
            let index = IndexPath(row: 0, section: 5)
            let cell: OrgsAboutUsTableCell = self.tableView_Home.cellForRow(at: index) as! OrgsAboutUsTableCell
            //            cell.lbl_countryCodeLabel.text = self.countrycode
            cell.btn_CountryCode.setTitle(self.countrycode, for: .normal)
            // self.phoneCode = self.countrycode
            // cell.img_CountryCode.image = countryDic["countryImage"] as? UIImage
            // self.tableView_Home.reloadData()
        }
    }
    func commiteePicker(textField : UITextField){
        let arr_OrgType  = ["President", "General Secretary", "Imam"]
        AKMultiPicker().openPickerIn(textField, firstComponentArray: arr_OrgType) { (value1, _, index1, _) in
            textField.text = value1
            
            self.dic_RegisterValue.updateValue(value1 as AnyObject, forKey: "committee")
            
        }
    }
    //Mark: ImageView Picker Delegate and datatsources---------------------
    
    @IBAction func btn_BannerView(sender: UIButton) {
        
        self.btn_profile.tag = 1
        let alert = UIAlertController(title: "Choose Image", message: nil, preferredStyle: UIAlertController.Style.actionSheet)
        
        let cameraAction = UIAlertAction(title: "Camera", style: UIAlertAction.Style.default) {
            UIAlertAction in
            self.openCamera()
        }
        let gallaryAction = UIAlertAction(title: "Gallery", style: UIAlertAction.Style.default) {
            UIAlertAction in
            self.openGallary()
        }
        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel) {
            UIAlertAction in
        }
        
        // Add the actions
        picker.delegate = self
        alert.addAction(cameraAction)
        alert.addAction(gallaryAction)
        alert.addAction(cancelAction)
        // Present the controller
        
        self.present(alert, animated: true, completion: nil)
    }
    @IBAction func btn_profile(sender: UIButton) {
        self.btn_profile.tag = 0
        
        let alert = UIAlertController(title: "Choose Image", message: nil, preferredStyle: UIAlertController.Style.actionSheet)
        
        let cameraAction = UIAlertAction(title: "Camera", style: UIAlertAction.Style.default) {
            UIAlertAction in
            self.openCamera()
        }
        let gallaryAction = UIAlertAction(title: "Gallery", style: UIAlertAction.Style.default) {
            UIAlertAction in
            self.openGallary()
        }
        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel) {
            UIAlertAction in
        }
        
        // Add the actions
        picker.delegate = self
        alert.addAction(cameraAction)
        alert.addAction(gallaryAction)
        alert.addAction(cancelAction)
        // Present the controller
        
        self.present(alert, animated: true, completion: nil)
    }
    
    func openCamera() {
        if UIImagePickerController .isSourceTypeAvailable(UIImagePickerController.SourceType.camera) {
            picker.sourceType = UIImagePickerController.SourceType.camera
            self.present(picker, animated: true, completion: nil)
        }
        
    }
    func openGallary() {
        picker.sourceType = UIImagePickerController.SourceType.photoLibrary
        self.present(picker, animated: true, completion: nil)
    }
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
        choosenImg = info[UIImagePickerController.InfoKey.originalImage] as! UIImage
        isImageUpdate = true
        if btn_profile.tag == 0{
            self.img_profile.contentMode = .scaleToFill
            self.img_profile.image = choosenImg
            
            self.imageData = choosenImg.jpegData(compressionQuality: 0.40)
            self.btn_profile.setBackgroundImage(#imageLiteral(resourceName: "upload_edit_iPhone"), for: .normal)
            self.uploadImageApi()
        }else{
            self.img_banner.contentMode = .scaleAspectFill
            self.img_banner.image = choosenImg
            
            self.imageData = choosenImg.jpegData(compressionQuality: 0.50)
            self.uploadImageApi()
        }
        
        
        isMessageCancel = false
        dismiss(animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
    }
    
    
    //MARK:IBAction---------------------------------------
    @IBAction func buttonYesAction(_ sender: UIButton) {
        if sender.tag == 0{
            self.Permission()
            isEditModeOnOFF = true
            btn_ON.backgroundColor = UIColor.init(hex: 0x0DC9E4)
            btn_OFF.backgroundColor = UIColor.white
            btn_ON.setTitleColor(UIColor.white, for: .normal)
            btn_OFF.setTitleColor(UIColor.darkGray, for: .normal)
        }else if sender.tag == 1{
            isEditModeOnOFF = false
            btn_EditName.isHidden = true
            btn_EditAbbre.isHidden = true
            btn_banner.isHidden = true
            btn_profile.isHidden = true
            btn_OFF.backgroundColor = UIColor.init(hex: 0x0DC9E4)
            btn_ON.backgroundColor = UIColor.white
            btn_OFF.setTitleColor(UIColor.white, for: .normal)
            btn_ON.setTitleColor(UIColor.darkGray, for: .normal)
        }
    }
    
    //MARK:Newws Feed Action------------------------------------
    
    @objc func btn_LocationNewsFeed(sender: UIButton){
        let lat = self.homeList[sender.tag].latitude ?? 123.123
        let long = self.homeList[sender.tag].longitude ?? 123.123
        
        self.location(lat: lat, long: long, title: self.homeList[sender.tag].address ?? "")
    }
    
    @objc func btn_GoingEnterNewsFeed(sender: UIButton) {
        
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "Alert_Activites_GoingViewController") as! Alert_Activites_GoingViewController
        vc.modalPresentationStyle = .overCurrentContext
        vc.weakSelfNewsFeed = self
        vc.FromActivitesController = "FromActivitesController"
        if self.homeList[sender.tag].category == "event"{
            vc.id = self.homeList[sender.tag].id ?? 0
            vc.category = "Event"
            
        }
        else if  self.homeList[sender.tag].category == "activity"{
            vc.id = self.homeList[sender.tag].id ?? 0
            vc.category = "Activity"
            
        }
        vc.attendesType = self.homeList[sender.tag].attendeesType ?? 0
        //  vc.modalPresentationStyle = .fullScreen
        self.present(vc, animated: true, completion: nil)
    }
    
    @objc func btn_GoingCountNewsFeed(sender: UIButton) {
        
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "Alert_Activites_GoingViewController") as! Alert_Activites_GoingViewController
        vc.modalPresentationStyle = .overCurrentContext
        // vc.weakSelf = self
        vc.menCount = self.homeList[sender.tag].men ?? 0
        vc.womenCount = self.homeList[sender.tag].women ?? 0
        vc.childrenCount = self.homeList[sender.tag].children ?? 0
        if self.homeList[sender.tag].category == "event"{
            vc.id = self.homeList[sender.tag].id ?? 0
            vc.category = "Event"
            
        }
        else if  self.homeList[sender.tag].category == "activity"{
            vc.id = self.homeList[sender.tag].id ?? 0
            vc.category = "Activity"
            
        }
        vc.id = self.homeList[sender.tag].id ?? 0
        vc.orgid = self.homeList[sender.tag].org ?? 0
        vc.onlyCountSee = "onlyCountSee"
        //  vc.modalPresentationStyle = .fullScreen
        self.present(vc, animated: true, completion: nil)
    }
    
    @objc func btn_GoingCountEventActivity(sender: UIButton) {
        
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "Alert_Activites_GoingViewController") as! Alert_Activites_GoingViewController
        vc.modalPresentationStyle = .overCurrentContext
        // vc.weakSelf = self
        
        if self.goingCountCateogry == "Event"{
            vc.id = self.arrayEventModel[sender.tag].id ?? 0
            vc.category = "Event"
            vc.menCount = self.arrayEventModel[sender.tag].men ?? 0
            vc.womenCount = self.arrayEventModel[sender.tag].women ?? 0
            vc.childrenCount = self.arrayEventModel[sender.tag].children ?? 0
            vc.id = self.arrayEventModel[sender.tag].id ?? 0
            vc.orgid = self.arrayEventModel[sender.tag].org ?? 0
        }
        else if  self.goingCountCateogry == "Activity"{
            vc.id = self.arrayActivityModel[sender.tag].id ?? 0
            vc.category = "Activity"
            vc.menCount = self.arrayActivityModel[sender.tag].men ?? 0
            vc.womenCount = self.arrayActivityModel[sender.tag].women ?? 0
            vc.childrenCount = self.arrayActivityModel[sender.tag].children ?? 0
            vc.id = self.arrayActivityModel[sender.tag].id ?? 0
            vc.orgid = self.arrayActivityModel[sender.tag].org ?? 0
        }
        
        vc.onlyCountSee = "onlyCountSee"
        //     vc.modalPresentationStyle = .fullScreen
        self.present(vc, animated: true, completion: nil)
    }
    @objc func btn_OrgDetailsNewsFeed(sender: UIButton) {
        
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "ActivitiesViewController") as! ActivitiesViewController
        vc.orgId = self.homeList[sender.tag].org ?? 0
        vc.modalPresentationStyle = .fullScreen
        self.present(vc, animated: true, completion: nil)
    }
    
    @objc func btn_GoingEnterEvent(sender: UIButton) {
        _ = IndexPath(item:sender.tag, section: tableViewSection)
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "Alert_Activites_GoingViewController") as! Alert_Activites_GoingViewController
        vc.modalPresentationStyle = .overCurrentContext
        vc.weakSelfNewsFeed = self
        vc.category = "Event"
        vc.id = self.arrayEventModel[sender.tag].id ?? 0
        vc.attendesType = self.arrayEventModel[sender.tag].attendees_type ?? 0
        // vc.modalPresentationStyle = .fullScreen
        
        self.present(vc, animated: true, completion: nil)
    }
    @objc func btn_GoingEnterActivity(sender: UIButton) {
        _ = IndexPath(item:sender.tag, section: tableViewSection)
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "Alert_Activites_GoingViewController") as! Alert_Activites_GoingViewController
        vc.modalPresentationStyle = .overCurrentContext
        vc.FromActivitesController = "FromActivitesController"
        vc.category = "activity"
        vc.id = self.arrayActivityModel?[sender.tag].id ?? 0
        vc.attendesType = self.arrayActivityModel?[sender.tag].attendees_type ?? 0
        vc.weakSelfNewsFeed = self
        //  vc.modalPresentationStyle = .fullScreen
        self.present(vc, animated: true, completion: nil)
    }
    
    
    @IBAction func btn_EditAdminProfile(sender:UIButton){
        
        if btn_EditAbbre.isSelected{
            btn_EditAbbre.isSelected = false
            txt_Abbreviation.isUserInteractionEnabled = false
            btn_EditAbbre.setImage(#imageLiteral(resourceName: "white_edit_pencil_iPhone"), for: .normal)
        }else{
            
            btn_EditAbbre.setImage(#imageLiteral(resourceName: "gray_edit_pencil_iPhone"), for: .normal)
            btn_EditAbbre.isSelected = true
            txt_Abbreviation.isUserInteractionEnabled = true
        }
        self.AdminEditApi()
    }
    
    
    @IBAction func btn_EditAdminName(sender:UIButton){
        
        
        if btn_EditName.isSelected{
            btn_EditName.setImage(#imageLiteral(resourceName: "white_edit_pencil_iPhone"), for: .normal)
            btn_EditName.isSelected = false
            txt_Name.isUserInteractionEnabled = false
        }else{
            
            btn_EditName.setImage(#imageLiteral(resourceName: "gray_edit_pencil_iPhone"), for: .normal)
            btn_EditName.isSelected = true
            txt_Name.isUserInteractionEnabled = true
        }
        self.AdminEditApi()
    }
    
    @IBAction func btn_NextAction(sender:UIButton){
        //        if self.brachio != 1{
        //                                dispatchGroup.enter()
        //                            }
        self.img_banner.image = nil
        if array_allfavOrgId.contains(orgId){
            let rorgId = array_allfavOrgId.index(of: orgId) ?? 0
            var nextIndex = rorgId + 1
            nextIndex = array_allfavOrgId.indices.contains(nextIndex) ? nextIndex : 0
            
            self.orgId = array_allfavOrgId[nextIndex]
            
            self.callFlagApiList(isCallFrom: "")
           // self.orgDetailsApi(isCallFrom:"Next")
//            if self.selectedIndexActivites == 0{
//                self.HomeListApi(keyAll: "")
//            }else if self.selectedIndexActivites == 1{
//                self.callEventListApi(orgId: self.orgId!)
//            }else if self.selectedIndexActivites == 2{
//                self.getClassesDataApi()
//            }else if self.selectedIndexActivites == 3{
//                self.iqamaListsApi()
//            }else if self.selectedIndexActivites == 4{
//                self.callListOfActivitiesInOrganization(orgId: self.orgId!)
//            }else if self.selectedIndexActivites == 5{
//                self.callAboutUsApi(orgId: self.orgId!)
//            }
//            collectionView_Org.reloadData()
        }
        
    }
    @IBAction func btn_PreviousAction(sender:UIButton){
        //        if self.brachio != 1{
        //                                dispatchGroup.enter()
        //                            }
        self.img_banner.image = nil
        if array_allfavOrgId.contains(orgId){
            let rorgId = array_allfavOrgId.index(of: orgId) ?? 0
            var nextIndex = rorgId - 1
            nextIndex = array_allfavOrgId.indices.contains(nextIndex) ? nextIndex : 0
            
            self.orgId = array_allfavOrgId[nextIndex]
            
            self.callFlagApiList(isCallFrom: "")
            //self.orgDetailsApi(isCallFrom:"Prev")
//            if self.selectedIndexActivites == 0{
//                self.HomeListApi(keyAll: "")
//            }else if self.selectedIndexActivites == 1{
//                self.callEventListApi(orgId: self.orgId!)
//            }else if self.selectedIndexActivites == 2{
//                self.getClassesDataApi()
//            }else if self.selectedIndexActivites == 3{
//                self.iqamaListsApi()
//            }else if self.selectedIndexActivites == 4{
//                self.callListOfActivitiesInOrganization(orgId: self.orgId!)
//            }else if self.selectedIndexActivites == 5{
//                self.callAboutUsApi(orgId: self.orgId!)
//            }
//            collectionView_Org.reloadData()
        }
        
    }
    @IBAction func btn_RemaningActivites(sender:UIButton){
        
        self.callFlagApiList(isCallFrom: "imgUpload")
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "Alert_AddActivitesViewController") as! Alert_AddActivitesViewController
        vc.delegate = self
        vc.modalPresentationStyle = .overCurrentContext
        vc.orDetails = self.orgDetails
        vc.orgId = self.orgDetails.id ?? 0
        vc.addRemaning = "addRemaning"
        // vc.modalPresentationStyle = .fullScreen
        self.present(vc, animated: true, completion: nil)
        
    }
    @IBAction func btn_AddActivites(sender:UIButton){
         self.iqamaListsApi(isCalledFromAdd: true)
        DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {
            
//            let vc = self.storyboard?.instantiateViewController(withIdentifier: "Alert_AddActivitesViewController") as! Alert_AddActivitesViewController
//            vc.delegate = self
//            vc.modalPresentationStyle = .overCurrentContext
//            vc.orDetails = self.orgDetails
//            vc.orgId = self.orgDetails.id ?? 0
//
//            if self.brachio == 1{
//                vc.orgType = self.orgDetails.orgType ?? ""
//            }else{
//                vc.orgType = self.org_type
//            }
//
//            vc.iqamaData = self.iqamaListNewsFeed.iqama
//            vc.enableDonation = self.orgDetails.donationAllowed
//
//            self.present(vc, animated: true, completion: nil)
            
        }
    }
    
    @IBAction func btn_addServices(sender:UIButton){
        
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "AlertAddServicesViewController") as! AlertAddServicesViewController
        vc.modalPresentationStyle = .overFullScreen
        vc.addServiceTicked = self.orgDetails.services ?? []
        vc.orgId = self.orgDetails.id ?? 0
        vc.FromOrgDetails = "FromOrgDetails"
        
        
        var org_P = [PriviligesData]()
        org_P = Global.shared.priviligesData
        let filertedData = org_P.filter{$0.org_id == self.orgId}
        filertedData.forEach {
            //   print("model data objects::",$0.org_perm as? String)
            
            if $0.org_perm == "iqama_only"{
                vc.isorgAll = "iqama_only"
                
            }else{
                
                vc.isorgAll = "All"
            }
        }
        
        
        vc.weakSelf = self
        self.present(vc, animated: true, completion: nil)
    }
    
    @IBAction func btn_favUnfav(sender:UIButton){
        self.dispatchGroup.enter()
        if self.orgDetails.isPrivate == true{
            if sender.isSelected {
                
                AKAlertController.alert("", message: "You are a member of this Private Organisation.Are you sure you want to remove from your favourtie list", buttons: ["Ok","Cancel"], tapBlock: { (_, _, index) in
                    if index == 1 {return}
                    sender.isSelected = false
                    self.orgDetails.is_fav = false
                    self.isfavbtnSelectedt = "false"
                    self.dispatchGroup.leave()
                })
            }else{
                sender.isSelected = true
                self.orgDetails.is_fav = true
                self.isfavbtnSelectedt = "true"
                self.dispatchGroup.leave()
            }
        }else{
            if sender.isSelected {
                sender.isSelected = false
                self.orgDetails.is_fav = false
                self.isfavbtnSelectedt = "false"
                self.dispatchGroup.leave()
                
            }else{
                sender.isSelected = true
                self.orgDetails.is_fav = true
                self.isfavbtnSelectedt = "true"
                self.dispatchGroup.leave()
            }
        }
        self.dispatchGroup.notify(queue: .main) {
            
            self.btn_Heart.tintColor = UIColor.clear
            
            self.favUnFavApi()
        }
    }
    
    @IBAction func btn_Back(sender:UIButton){
        
        if brachio == 1{
            
            self.btn_BackBranchIo()
            
        }else{
            self.dismiss(animated: true, completion: nil)
        }
        
        
    }
    
    func btn_BackBranchIo(){
        
        SlideMenuOptions.hideStatusBar = false
        SlideMenuOptions.leftViewWidth = UIScreen.main.bounds.size.width - 70
        
        let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
        let mainViewController = storyboard.instantiateViewController(withIdentifier: "TabVC") as! TabVC
        let leftViewController = storyboard.instantiateViewController(withIdentifier: "MenuViewController") as! MenuViewController
        
        let slideMenuController = SlideMenuController(mainViewController: mainViewController, leftMenuViewController: leftViewController)
        AppDelegate.instance.window?.rootViewController = slideMenuController
        
        AppDelegate.instance.window?.makeKeyAndVisible()
    }
    @IBAction func btn_EditServices(sender:UIButton){
        
        //Check for Admin,super Admin,Normal User----------------------
        if (UserDefaults.standard.value(forKey: "Login_Token") as? String) != nil{
            
            if let superAdmin = UserDefaults.standard.value(forKey: "isSuperAdmin") as? Bool{
                
                if superAdmin == true{
                    let editVC = self.storyboard?.instantiateViewController(withIdentifier: "Edit_SuperAdminDetailsViewController") as! Edit_SuperAdminDetailsViewController
                    
                    editVC.orgDetailsEdit = self.orgDetails
                    self.present(editVC, animated: true, completion: nil)
                }else{
                    
                    if let is_staff = UserDefaults.standard.value(forKey: "is_staff") as? Bool{
                        
                        if is_staff == true{
                            //mark:check For own Organisation---------------------------
                            if let orgOwn_Array = UserDefaults.standard.value(forKey: "OwnOrg_Array") as? [Int]{
                                
                                if orgOwn_Array.contains(orgId ?? 0){
                                    
                                    
                                    var org_P = [PriviligesData]()
                                    org_P = Global.shared.priviligesData
                                    let filertedData = org_P.filter{$0.org_id == orgId ?? 0}
                                    filertedData.forEach {
                                        //   print("model data objects::",$0.org_perm as? String)
                                        
                                        if $0.org_perm == "iqama_only"{
                                            print("no staff")
                                        }else{
                                            let vc = self.storyboard?.instantiateViewController(withIdentifier: "AlertAddServicesViewController") as! AlertAddServicesViewController
                                            vc.modalPresentationStyle = .overCurrentContext
                                            vc.addServiceTicked = self.orgDetails.services ?? []
                                            vc.orgId = self.orgDetails.id ?? 0
                                            vc.FromOrgDetails = "FromOrgDetails"
                                            
                                            var org_P = [PriviligesData]()
                                            org_P = Global.shared.priviligesData
                                            let filertedData = org_P.filter{$0.org_id == self.orgId}
                                            filertedData.forEach {
                                                //   print("model data objects::",$0.org_perm as? String)
                                                
                                                if $0.org_perm == "iqama_only"{
                                                    vc.isorgAll = "iqama_only"
                                                    
                                                }else{
                                                    
                                                    vc.isorgAll = "All"
                                                }
                                            }
                                            // vc.modalPresentationStyle = .fullScreen
                                            vc.weakSelf = self
                                            self.present(vc, animated: true, completion: nil)
                                        }
                                    }
                                    
                                    
                                }else{
                                    print("no staff")
                                    
                                    // self.btn_Add_EditServices.setTitle("No Services", for: .normal)
                                    // self.btn_Add_EditServices.setImage(nil, for: .normal)
                                }//orgOwn
                                
                            }
                        }//staff true
                    }//staff
                    
                }
            }//SuperAdmin
        }//Login
        else{
            
            
        }
        
    }
    
    @IBAction func btn_More(sender:UIButton){
        
        let dropDown = DropDown()
        
        dropDown.anchorView = self.btn_Moree
        dropDown.bottomOffset = CGPoint(x: 80, y:(dropDown.anchorView?.plainView.bounds.height)!)
        
        // The list of items to display. Can be changed dynamically
        dropDown.dataSource = ["Edit          ".localized, "Delete            ".localized]
        dropDown.textColor = UIColor.black
        dropDown.backgroundColor = UIColor.white
        dropDown.selectionAction = { [unowned self] (index: Int, item: String) in
            //    print("Selected item: \(item) at index: \(index)")
            
            if index == 0 {
                //mark:for editing value-------------------------------------------------
                let editVC = self.storyboard?.instantiateViewController(withIdentifier: "Edit_SuperAdminDetailsViewController") as! Edit_SuperAdminDetailsViewController
                
                editVC.orgDetailsEdit = self.orgDetails
                editVC.modalPresentationStyle = .fullScreen
                self.present(editVC, animated: true, completion: nil)
                
            }else{
                //mark:for delete value-------------------------------------------------
                AKAlertController.alert("", message: "Do you want to delete this Organization?".localized, buttons: ["Ok".localized,"Cancel".localized], tapBlock: { (_, _, index) in
                    if index == 1 {return}
                    self.deleteorgApi()
                    
                    return
                })
            }
        }
        dropDown.show()
    }
    
    @objc func imageTapped(tapGestureRecognizer: UITapGestureRecognizer)
    {
        _ = tapGestureRecognizer.view as! UIImageView
        
        
        if self.selectedIndexActivites == 0{
            
            
            SKPhotoBrowserOptions.displayAction = false
            var images = [SKPhoto]()
            for i in 0..<self.homeList[(tapGestureRecognizer.view?.tag)!].images.count{
                
                let photo = SKPhoto.photoWithImageURL(self.homeList[(tapGestureRecognizer.view?.tag)!].images[i])
                photo.shouldCachePhotoURLImage = false // you can use image cache by true(NSCache)
                images.append(photo)
            }
            
            
            let browser = SKPhotoBrowser(originImage: #imageLiteral(resourceName: "default_header_iPhone"), photos: images, animatedFromView: self.view)
            for _ in 0..<self.homeList[(tapGestureRecognizer.view?.tag)!].images.count{
                browser.initializePageIndex(imageSelectedView)
            }
            present(browser, animated: true, completion: {})
            
            
        }else if self.selectedIndexActivites == 1{
            
            SKPhotoBrowserOptions.displayAction = false
            var images = [SKPhoto]()
            for i in 0..<self.arrayEventModel[(tapGestureRecognizer.view?.tag)!].images!.count{
                
                let photo = SKPhoto.photoWithImageURL(self.arrayEventModel[(tapGestureRecognizer.view?.tag)!].images![i])
                photo.shouldCachePhotoURLImage = false // you can use image cache by true(NSCache)
                images.append(photo)
            }
            
            
            let browser = SKPhotoBrowser(originImage: #imageLiteral(resourceName: "default_header_iPhone"), photos: images, animatedFromView: self.view)
            for _ in 0..<self.arrayEventModel[(tapGestureRecognizer.view?.tag)!].images!.count{
                browser.initializePageIndex(imageSelectedView)
            }
            present(browser, animated: true, completion: {})
            
            
        } else if self.selectedIndexActivites == 2{
            
            SKPhotoBrowserOptions.displayAction = false
            var images = [SKPhoto]()
            
            let classData = self.classesModelData?.getClassData?[(tapGestureRecognizer.view?.tag)!].images
            for i in 0..<classData!.count{
                
                let photo = SKPhoto.photoWithImageURL(classData![i])
                photo.shouldCachePhotoURLImage = false // you can use image cache by true(NSCache)
                images.append(photo)
            }
            
            
            let browser = SKPhotoBrowser(originImage: #imageLiteral(resourceName: "default_header_iPhone"), photos: images, animatedFromView: self.view)
            for _ in 0..<classData!.count{
                browser.initializePageIndex(imageSelectedView)
            }
            present(browser, animated: true, completion: {})
            
        }
        
        
    }
    
    // handle notification
    @objc func showIqamaLists(_ notification: NSNotification) {
        print(notification.object ?? "")
        self.iqamaListsData = notification.object as? IQAMALists
        
        if self.iqamaListsData != nil{
            //   print("iqamaLists")
            // print(notification.userInfo as Any)
            if let dict = notification.userInfo {
                //   print(dict["innerTableView"] ?? [AnyHashable: Any].self)
                let height = dict["innerTableView"] ?? [AnyHashable: Any].self
                iqamaInnertableviewHeight = height as! CGFloat
            }
            
        }
    }
    // handle notification
    @objc func showIqamaListsForNewsFeed(_ notification: NSNotification) {
        // print(notification.object ?? "")
        self.iqamaListNewsFeed = notification.object as? IQAMAListModel
        
        if self.iqamaListNewsFeed != nil{
            //print("iqamaListNewsFeed")
            //print(notification.userInfo as Any)
            if let dict = notification.userInfo {
                //print(dict["innerTableView"] ?? [AnyHashable: Any].self)
                let height = dict["innerTableView"] ?? [AnyHashable: Any].self
                iqamaInnertableviewHeight = height as! CGFloat
            }
            
        }
    }
    //mark:IqamaList edit---------------------------------
    @objc func iqamaListsEdit(sender:UIButton){
        
        
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "IqamahViewController") as! IqamahViewController
        
        if self.tableViewSection == 0{
            if sender.tag == 0{
                
                vc.editButtonFrom = "IQAMAH"
            }
            
            vc.editIqamah = "editIqamah"
            vc.newsFeedData = self.iqamaListNewsFeed
            vc.orgId = orgId
        }else{
            if sender.tag == 0{
                
                vc.editButtonFrom = "IQAMAH"
            }else{
                vc.editButtonFrom = "SPECIAL PRAYER"
            }
            vc.editIqamah = "editIqamah"
            vc.newsFeedData = nil
            vc.iqamaData = self.iqamaListsData
            vc.orgId = orgId
        }
        vc.modalPresentationStyle = .fullScreen
        self.present(vc, animated: true, completion: nil)
    }
    
    //Mark:Function And Method---------------------------------------
    func pullToRefresh(){
        
        let refresh = UIRefreshControl()
        refresh.addTarget(self, action: #selector(handleTopRefresh(_:)), for: .valueChanged )
        refresh.tintColor = UIColor.red
        self.tableView_Home.addSubview(refresh)
        
    }
    @objc func handleTopRefresh(_ sender:UIRefreshControl){
        self.HomeListApi(keyAll: "")
        sender.endRefreshing()
    }
    func buttoncornerRadiusShadow (){
        
        btn_ON.clipsToBounds = true
        btn_ON.layer.cornerRadius = 8
        btn_ON.layer.borderColor = UIColor.init(hex: 0x0DC9E4).cgColor
        btn_ON.layer.borderWidth = 1.0
        btn_ON.layer.maskedCorners = [.layerMinXMaxYCorner, .layerMinXMinYCorner]
        
        btn_OFF.clipsToBounds = true
        btn_OFF.layer.cornerRadius = 8
        btn_OFF.layer.maskedCorners = [.layerMaxXMaxYCorner, .layerMaxXMinYCorner]
        btn_OFF.layer.borderColor = UIColor.init(hex: 0x0DC9E4).cgColor
        btn_OFF.layer.borderWidth = 1.0
        
    }
    
    
    func Permission(){
        //mark:check for More button--------------------------------
        
        if (UserDefaults.standard.value(forKey: "Login_Token") as? String) != nil{
            
            if let superAdmin = UserDefaults.standard.value(forKey: "isSuperAdmin") as? Bool{
                
                if superAdmin == true{
                    btn_Moree.isHidden = false
                    btn_Add_Activites.isHidden = false
                    btn_profile.isHidden = false
                    btn_banner.isHidden = false
                    btn_EditName.isHidden = true
                    btn_EditAbbre.isHidden = true
                    if self.remaingData == []{
                        btn_Flagg.isHidden = true
                        img_Flag.isHidden = true
                    }else{
                        btn_Flagg.isHidden = false
                        img_Flag.isHidden = false
                    }
                    
                }else{
                    
                    if let is_staff = UserDefaults.standard.value(forKey: "is_staff") as? Bool{
                        
                        if is_staff == true{
                            
                            //mark:check For own Organisation---------------------------
                            if let orgOwn_Array = UserDefaults.standard.value(forKey: "OwnOrg_Array") as? [Int]{
                                self.orgOwnArray = orgOwn_Array
                                
                                if self.orgOwnArray.contains(orgId){
                                    btn_Moree.isHidden = true
                                    btn_Add_Activites.isHidden = false
                                    btn_profile.isHidden = false
                                    btn_banner.isHidden = false
                                    btn_EditName.isHidden = false
                                    btn_EditAbbre.isHidden = false
                                    if self.remaingData == []{
                                        btn_Flagg.isHidden = true
                                        img_Flag.isHidden = true
                                    }else{
                                        btn_Flagg.isHidden = false
                                        img_Flag.isHidden = false
                                    }
                                }else{
                                    btn_Moree.isHidden = true
                                    btn_Add_Activites.isHidden = false
                                    btn_profile.isHidden = true
                                    btn_banner.isHidden = true
                                    
                                    btn_EditName.isHidden = true
                                    btn_EditAbbre.isHidden = true
                                    btn_Flagg.isHidden = true
                                    img_Flag.isHidden = true
                                }
                            }
                            
                        }else{
                            btn_Moree.isHidden = true
                            btn_Add_Activites.isHidden = true
                            btn_profile.isHidden = true
                            btn_banner.isHidden = true
                            
                            btn_EditName.isHidden = true
                            btn_EditAbbre.isHidden = true
                            btn_Flagg.isHidden = true
                            img_Flag.isHidden = true
                        }
                        
                    }
                    
                }
            }else{ // for super admin nil
                
                
                btn_Moree.isHidden = true
                btn_Add_Activites.isHidden = true
                btn_profile.isHidden = true
                btn_banner.isHidden = true
                btn_EditName.isHidden = true
                btn_EditAbbre.isHidden = true
                btn_Flagg.isHidden = true
                img_Flag.isHidden = true
                
            }
        }
        else{ // For Login Token
            
            
            btn_Moree.isHidden = true
            btn_Add_Activites.isHidden = true
            btn_profile.isHidden = true
            btn_banner.isHidden = true
            btn_EditName.isHidden = true
            btn_EditAbbre.isHidden = true
            btn_Flagg.isHidden = true
            img_Flag.isHidden = true
            
        }
        
        
    }
    
    
    
    func callFlagApiList(isCallFrom:String){
        if isCallFrom != "imgUpload"{
             KVNProgress.show()
        }
       
        Network.shared.getFlag(orgId:orgId) { (result) in
            if isCallFrom != "imgUpload"{
                        KVNProgress.dismiss()
                   }
            
            guard let user = result else {
                return
            }
            self.remaingData = user.data ?? []
            if (UserDefaults.standard.value(forKey: "Login_Token") as? String) != nil{
                if let superAdmin = UserDefaults.standard.value(forKey: "isSuperAdmin") as? Bool{
                    if superAdmin == true{
                        if self.remaingData == []{
                            self.btn_Flagg.isHidden = true
                            self.img_Flag.isHidden = true
                        }else{
                            self.btn_Flagg.isHidden = false
                            self.img_Flag.isHidden = false
                        }
                    }else{
                        if let is_staff = UserDefaults.standard.value(forKey: "is_staff") as? Bool{
                            if is_staff == true{
                                //mark:check For own Organisation---------------------------
                                if let orgOwn_Array = UserDefaults.standard.value(forKey: "OwnOrg_Array") as? [Int]{
                                    if orgOwn_Array.contains(self.orgId){
                                        var org_P = [PriviligesData]()
                                        org_P = Global.shared.priviligesData
                                        let filertedData = org_P.filter{$0.org_id == self.orgId}
                                        filertedData.forEach {
                                            //   print("model data objects::",$0.org_perm as? String)
                                            
                                            if $0.org_perm == "iqama_only"{
                                                
                                                if self.remaingData == []{
                                                    self.btn_Flagg.isHidden = true
                                                    self.img_Flag.isHidden = true
                                                }else{
                                                    self.btn_Flagg.isHidden = false
                                                    self.img_Flag.isHidden = false
                                                }
                                            }else{
                                                
                                                if self.remaingData == []{
                                                    self.btn_Flagg.isHidden = true
                                                    self.img_Flag.isHidden = true
                                                }else{
                                                    self.btn_Flagg.isHidden = false
                                                    self.img_Flag.isHidden = false
                                                }
                                            }
                                        }
                                        
                                        
                                    }else{
                                        
                                        self.btn_Flagg.isHidden = true
                                        self.img_Flag.isHidden = true
                                        
                                    }//orgOwn
                                    
                                }
                            }//staff true
                        }//staff
                        
                    }
                }//SuperAdmin
            }//Login
            else{
                self.btn_Flagg.isHidden = true
                self.img_Flag.isHidden = true
                
                
            }
            if isCallFrom != "imgUpload"{
               self.orgDetailsApi(isCallFrom: "")
            }
           
        }
    }
    
    
}

//MARK:TableView Delegate And DataSources-----------------------
extension ActivitiesViewController:UITableViewDelegate,UITableViewDataSource,NotSoGoodCellDelegateOrg,NewsFeedCellDelegate,MFMessageComposeViewControllerDelegate,IndexImageSendCellDelegate,IndexImageSendCellDelegateActivities,IndexImageSendCellDelegateClass,IqamaListCellDelegate,NewsIqamaListCellDelegate,shareDataDelegateHome{
    func NewsIqamaListmoreTapped(cell: NewsFeedIqamTableViewCell) {
        // cell.tableView.reloadData()
        tableView_Home.reloadRows(at: [IndexPath(row:0 , section: 0)], with: .fade)
        tableView_Home.beginUpdates()
        tableView_Home.endUpdates()
        tableView_Home.layoutIfNeeded()
        
    }
    
    
    func IqamaListmoreTapped(cell: IqamahListTableViewCell) {
        
        // cell.tableView.reloadData()
        tableView_Home.reloadRows(at: [IndexPath(row:0 , section: 3)], with: .fade)
        tableView_Home.beginUpdates()
        tableView_Home.endUpdates()
        tableView_Home.layoutIfNeeded()
    }
    
    
    func IndexImageSendCellDelegateClass(cell: Org_Classes_TableViewCell, selected_imgIndex: Int) {
        self.imageSelectedView = selected_imgIndex
    }
    
    
    func IndexImageSendCellDelegateActivities(cell: OrgEventListTableViewCell, selected_imgIndex: Int) {
        self.imageSelectedView = selected_imgIndex
    }
    
    
    func IndexImageSendCellDelegate(cell: AnnouncementTableViewCell, selected_imgIndex: Int) {
        self.imageSelectedView = selected_imgIndex
    }
    
    
    // MARK: - my cell delegate
    func moreTapped(cell: OrgActivityTableViewCell) {
        
        tableView_Home.reloadData()
    }
    func moreNewsFeedTapped(cell: AnnouncementTableViewCell) {
        tableView_Home.beginUpdates()
        tableView_Home.endUpdates()
    }
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return array_Activities.count
    }
    func shareDataDelegateHome(cell: AnnouncementTableViewCell, date: String, time: String) {
        
        self.shareDate = date
        self.shareTime = time
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if self.selectedIndexActivites == 0{
            switch section{
            case 0:
                
                return homeList.count + 1
                
                
            default:
                return 0
            }
            
        }
            
        else if self.selectedIndexActivites == 1{
            
            switch section{
            case 1:
                return self.arrayEventModel.count
            default:
                return 0
            }
            
        }else if selectedIndexActivites == 2{
            
            switch section{
            case 2:
                return self.classesModelData?.getClassData?.count ?? 0
                
            default:
                return 0
            }
            
        }
        else if selectedIndexActivites == 3{
            
            switch section{
            case 3:
                if self.iqamaListsData.data?.specialPrayers?.count != 0 && (self.iqamaListsData.data?.jumma?.count != 0 || self.self.iqamaListsData.data?.iqama?.count != 0){
                    
                    return 2
                }else if self.iqamaListsData.data?.specialPrayers?.count == 0 && (self.iqamaListsData.data?.jumma?.count != 0 || self.self.iqamaListsData.data?.iqama?.count != 0){
                    return 1
                }else if self.iqamaListsData.data?.specialPrayers?.count != 0 && (self.iqamaListsData.data?.jumma?.count == 0 || self.self.iqamaListsData.data?.iqama?.count == 0){
                    return 2
                }else{
                    
                    return 0
                }
                
            default:
                return 0
            }
            
        }else if selectedIndexActivites == 4{
            
            switch section{
            case 4:
                return self.arrayActivityModel.count
                
            default:
                return 0
            }
        }else if selectedIndexActivites == 5{
            
            switch section{
            case 5:
                if let superAdmin = UserDefaults.standard.value(forKey: "isSuperAdmin") as? Bool{
                    
                    if superAdmin == true{
                        return 2
                    }else{
                        return 1
                    }
                }else{
                    return 1
                }
                
            default:
                return 0
            }
        }
        
        
        
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        switch indexPath.section {
        case 0:
            tableViewSection = 0
            
            
            if indexPath.row == 0{
                
                
                if self.iqamaListNewsFeed?.iqama != nil || self.iqamaListNewsFeed?.jumma != nil {
                    if self.iqamaListNewsFeed?.iqama?.count != 0 || self.iqamaListNewsFeed?.jumma?.count != 0 {
                        let cell = tableView.dequeueReusableCell(withIdentifier: "NewsFeedIqamTableViewCell", for: indexPath) as! NewsFeedIqamTableViewCell
                        cell.newsIqamaListDelegate = self
                        cell.cellSetUp(indexPath: indexPath ,id: orgId ?? 0,iqamaData: self.iqamaListNewsFeed)
                        
                        cell.btn_editIqama.tag = indexPath.row
                        cell.btn_editIqama.addTarget(self, action: #selector(iqamaListsEdit(sender:)), for: .touchUpInside)
                        
                        cell.btn_Report.addTarget(self, action: #selector(reportIqamaApi(sender:)), for: .touchUpInside)
                        
                        if self.orgDetails != nil{
                            if self.orgDetails.isPrivate == true{
                                cell.btn_Share.isHidden = true
                            }else{
                                cell.btn_Share.isHidden = false
                                cell.btn_Share.addTarget(self, action: #selector(btn_shareIQama(sender:)), for: .touchUpInside)
                            }
                        }
                        
                        cell.tableViewHeight.constant = iqamaInnertableviewHeight
                        cell.tableView.layoutIfNeeded()
                        cell.tableView.beginUpdates()
                        cell.tableView.endUpdates()
                        return cell
                        
                    }else{
                        
                        return UITableViewCell()
                    }
                }else{
                    
                    return UITableViewCell()
                }
                
                
            }else{
                
                
                let cell = tableView.dequeueReusableCell(withIdentifier: "AnnouncementTableViewCell", for: indexPath) as! AnnouncementTableViewCell
                cell.imageSelectedSendDelegate = self
                cell.img_Viewannouncement.tag = indexPath.row - 1
                let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(imageTapped(tapGestureRecognizer:)))
                cell.shareDataDelegatehome = self
                cell.img_Viewannouncement.isUserInteractionEnabled = true
                cell.img_Viewannouncement.addGestureRecognizer(tapGestureRecognizer)
                if let langugae = UserDefaults.standard.value(forKey: Constant.LANGUAGE) as? String{
                    
                    if langugae == "ar"{
                        cell.buttonMore.imageEdgeInsets = UIEdgeInsets(top: 0.0, left: 0.0, bottom: 0.0, right: -145.0)
                        cell.buttonMore.titleEdgeInsets = UIEdgeInsets(top: 0.0, left: -38.0, bottom: 0.0, right: 0.0)
                        cell.btnShowHide.imageEdgeInsets = UIEdgeInsets(top: 0.0, left: 0.0, bottom: 0.0, right: -145.0)
                        cell.btnShowHide.titleEdgeInsets = UIEdgeInsets(top: 0.0, left: -38.0, bottom: 0.0, right: 0.0)
                        cell.buttonMore.contentEdgeInsets = UIEdgeInsets(top: 0.0, left: 0.0, bottom: 0.0, right: 0.0)
                        
                    }else if langugae == "ms"{
                        cell.buttonMore.imageEdgeInsets = UIEdgeInsets(top: 0.0, left: -10.0, bottom: 0.0, right: -265.0)
                        cell.buttonMore.titleEdgeInsets = UIEdgeInsets(top: 0.0, left: -38.0, bottom: 0.0, right: 0.0)
                        cell.buttonMore.contentEdgeInsets = UIEdgeInsets(top: 0.0, left: 0.0, bottom: 0.0, right: 0.0)
                       // cell.buttonMore.backgroundColor = UIColor.red
                        cell.btnShowHide.imageEdgeInsets = UIEdgeInsets(top: 0.0, left: -10.0, bottom: 0.0, right: -265.0)
                        cell.btnShowHide.titleEdgeInsets = UIEdgeInsets(top: 0.0, left: -38.0, bottom: 0.0, right: 0.0)
                        cell.btnShowHide.contentEdgeInsets = UIEdgeInsets(top: 0.0, left: 0.0, bottom: 0.0, right: 0.0)
                        
                    }else if langugae == "en"{
                        cell.buttonMore.imageEdgeInsets = UIEdgeInsets(top: 0.0, left: 0.0, bottom: 0.0, right: -145.0)
                        cell.buttonMore.titleEdgeInsets = UIEdgeInsets(top: 0.0, left: -38.0, bottom: 0.0, right: 0.0)
                        cell.btnShowHide.imageEdgeInsets = UIEdgeInsets(top: 0.0, left: 0.0, bottom: 0.0, right: -145.0)
                        cell.btnShowHide.titleEdgeInsets = UIEdgeInsets(top: 0.0, left: -38.0, bottom: 0.0, right: 0.0)
                        cell.buttonMore.contentEdgeInsets = UIEdgeInsets(top: 0.0, left: 0.0, bottom: 0.0, right: 0.0)
                        
                    }
                }else{
                    cell.buttonMore.imageEdgeInsets = UIEdgeInsets(top: 0.0, left: 0.0, bottom: 0.0, right: -145.0)
                    cell.buttonMore.titleEdgeInsets = UIEdgeInsets(top: 0.0, left: -38.0, bottom: 0.0, right: 0.0)
                     cell.buttonMore.contentEdgeInsets = UIEdgeInsets(top: 0.0, left: 0.0, bottom: 0.0, right: 0.0)
                    cell.btnShowHide.imageEdgeInsets = UIEdgeInsets(top: 0.0, left: 0.0, bottom: 0.0, right: -145.0)
                    cell.btnShowHide.titleEdgeInsets = UIEdgeInsets(top: 0.0, left: -38.0, bottom: 0.0, right: 0.0)
                    cell.buttonMore.contentEdgeInsets = UIEdgeInsets(top: 0.0, left: 0.0, bottom: 0.0, right: 0.0)
                }
                cell.newsFeedDelegate = self
                if (UserDefaults.standard.value(forKey: "Login_Token") as? String) != nil{
                    
                    if let superAdmin = UserDefaults.standard.value(forKey: "isSuperAdmin") as? Bool{
                        
                        if superAdmin == true{
                            cell.btnEdit.isHidden = false
                            cell.btnDelete.isHidden = false
                            
                            
                        }else{
                            
                            if let is_staff = UserDefaults.standard.value(forKey: "is_staff") as? Bool{
                                
                                if is_staff == true{
                                    //mark:check For own Organisation---------------------------
                                    if let orgOwn_Array = UserDefaults.standard.value(forKey: "OwnOrg_Array") as? [Int]{
                                        
                                        if orgOwn_Array.contains(self.homeList[indexPath.row - 1].org ?? 0){
                                            
                                            var org_P = [PriviligesData]()
                                            org_P = Global.shared.priviligesData
                                            let filertedData = org_P.filter{$0.org_id == self.homeList[indexPath.row - 1].org ?? 0}
                                            filertedData.forEach {
                                                
                                                
                                                if $0.org_perm == "iqama_only"{
                                                    
                                                    cell.btnEdit.isHidden = true
                                                    cell.btnDelete.isHidden = true
                                                    
                                                }else{
                                                    cell.btnEdit.isHidden = false
                                                    cell.btn_GoingCount.setTitleColor(UIColor.init(hex: 0x0DC9E4), for: .normal)
                                                    
                                                }
                                            }
                                            
                                        }else{
                                            cell.btnEdit.isHidden = true
                                            cell.btnDelete.isHidden = true
                                            
                                        }//orgOwn
                                        
                                    }
                                }//staff true
                            }//staff
                            
                        }
                    }//SuperAdmin
                }//Login
                else{
                    cell.btnEdit.isHidden = true
                    cell.btnDelete.isHidden = true
                    
                    
                }
                
                // cell.delegate = self
                cell.view_ClassCost.isHidden = true
                cell.btnReminder.tag = indexPath.row - 1
                cell.btnShare.tag = indexPath.row - 1
                cell.btnGoing.tag = indexPath.row - 1
                cell.lblLocation.tag = indexPath.row - 1
                cell.lblLocation.addTarget(self, action: #selector(btn_LocationNewsFeed(sender:)), for: .touchUpInside)
                if self.homeList.count != 0 {
                    
                    cell.configureCell(model: self.homeList[indexPath.row - 1], indexPath: indexPath.row - 1)
                }
                
                //adds shadow to the layer of cell
                cell.view_Announcment.layer.cornerRadius = 10.0
                cell.view_Announcment.shadowBlackToHeader()
                
                //makes the cell round
                let containerView = cell.contentView
                containerView.layer.cornerRadius = 2
                containerView.clipsToBounds = true
                
                
                
                //mark:Date
                let now = Date()
                let date = NSDate(timeIntervalSince1970: Double(self.homeList[indexPath.row - 1].addedAt ))
                cell.lbl_Title3.text = now.timeAgoDisplay(dateCreated:date as Date,timeStamp:self.homeList[indexPath.row - 1].addedAt )
                
                
                //Mark: Type Announcement------------------------------
                if self.homeList[indexPath.row - 1].category == "announcement"{
                    self.goingCountCateogry = "announcement"
                    cell.view_Divider.isHidden = false
                    cell.btn_ClassType.isHidden = true
                    cell.view_stackViewDonation.isHidden = true
                    cell.view_donationHeight.constant = 0.0
                    cell.view_Share.isHidden = false
                    cell.layout_ShareHeight.constant = 34.0
                    cell.view_MonthType.isHidden = true
                    cell.btnGoing.tag = indexPath.row - 1
                    cell.view_Location.isHidden  = false
                    //  cell.lbl_Title1.tag = indexPath.row - 1
                    cell.view_ContainAGGA.isHidden = false
                    cell.view_stackViewTypes.isHidden = true
                    cell.height_stackViewTypes.constant = 0.0
                    cell.view_Location.isHidden  = false
                    cell.view_MonthType.isHidden = true
                    cell.view_stackViewTypes.isHidden = true
                    cell.view_AgeGroup.isHidden = true
                    cell.view_Gender.isHidden = true
                    cell.view_TeacherType.isHidden = true
                    cell.view_AttendesType.isHidden = false
                    cell.lbl_AttendesType.isHidden = false
                    cell.btnReminder.setTitle("", for: .normal)
                    cell.btnReminder.setImage(nil, for: .normal)
                    cell.btnReminder.isUserInteractionEnabled = false
                    
                    cell.lbl_Title1.setTitle(homeList[indexPath.row - 1].org_abbr, for: .normal)
                    
                    cell.lbl_Title2.text = ""
                    cell.btn_ViewCategory.setImage(#imageLiteral(resourceName: "annoucement_iPhone"), for: .normal)
                    cell.lbl_Title4.text = homeList[indexPath.row - 1].title
                    cell.View_CategoryColor.backgroundColor = UIColor.init(hex: 0xb68efe)
                    
                    cell.view_ClassCost.isHidden = true
                    cell.lbl_TeacherNAme.isHidden = true
                    if (homeList[indexPath.row - 1].free) == 1{
                        cell.lbl_ClassCost.text = "Free"
                    }else{
                        cell.lbl_ClassCost.text = "Paid"
                    }
                    DispatchQueue.main.async {
                        
                        if let imageUrl =  self.homeList[indexPath.row - 1].logo{
                            cell.img_Logo.sd_setImage(with: URL(string: imageUrl), placeholderImage: #imageLiteral(resourceName: "default_header_iPhone"))
                        }else{
                            cell.img_Logo.image = #imageLiteral(resourceName: "default_header_iPhone")
                        }
                    }
                    if homeList[indexPath.row - 1].attendeesType ?? 0 == 1{
                        cell.lbl_AttendesType.text = "Men Only"
                    }else if homeList[indexPath.row - 1].attendeesType ?? 0 == 2{
                        cell.lbl_AttendesType.text = "Women Only"
                    }else if homeList[indexPath.row - 1].attendeesType ?? 0 == 3{
                        cell.lbl_AttendesType.text = "Family"
                    }else{
                        cell.lbl_AttendesType.text = ""
                    }
                    self.goingCountCateogry = "announcement"
                    cell.btnGoing.addTarget(self, action: #selector(btn_GoingEnterNewsFeed(sender:)), for: .touchUpInside)
                    
                    //mark:GoingCount
                    
                    cell.btn_GoingCount.isHidden = true
                    cell.btnGoing.isHidden = true
                    cell.btn_GoingCount.tag = indexPath.row - 1
                    let goingCount = (homeList[indexPath.row - 1].goingCount ?? 0)
                    let going = "\(String(describing: goingCount))" + " going".localized
                    cell.btn_GoingCount.setTitle(going, for: .normal)
                    cell.btn_GoingCount.addTarget(self, action: #selector(btn_GoingCountNewsFeed(sender:)), for: .touchUpInside)
                    
                }
                    //Mark: Type event------------------------------
                else if self.homeList[indexPath.row - 1].category == "event"{
                    cell.view_Divider.isHidden = false
                    cell.btnGoing.tag = indexPath.row - 1
                    cell.height_stackViewTypes.constant = 120.0
                    cell.view_Share.isHidden = false
                    cell.layout_ShareHeight.constant = 34.0
                    cell.view_stackViewTypes.isHidden = false
                    cell.view_ContainAGGA.isHidden = false
                    cell.view_stackViewDonation.isHidden = true
                    cell.view_donationHeight.constant = 0.0
                    cell.btnReminder.setTitle("Reminder".localized, for: .normal)
                    cell.btnReminder.setImage(#imageLiteral(resourceName: "reminder_iPhone"), for: .normal)
                    cell.btnReminder.isUserInteractionEnabled = true
                    cell.view_Location.isHidden = false
                    if let imageUrl =  self.homeList[indexPath.row - 1].eventLogo{
                        cell.img_Logo.sd_setImage(with: URL(string: imageUrl), placeholderImage: #imageLiteral(resourceName: "default_header_iPhone"))
                    }else{
                        cell.img_Logo.image = #imageLiteral(resourceName: "default_header_iPhone")
                    }
                    cell.btn_ClassType.isHidden = true
                    cell.lblLocation.tag = indexPath.row - 1
                    cell.lblLocation.addTarget(self, action: #selector(btn_LocationNewsFeed(sender:)), for: .touchUpInside)
                    cell.lbl_Title1.tag = indexPath.row - 1
                    
                    cell.lbl_Title1.setTitle(homeList[indexPath.row - 1].org_abbr, for: .normal)
                    
                    cell.lbl_Title2.text = ""
                    
                    cell.btn_ViewCategory.setImage(#imageLiteral(resourceName: "events_iPhone"), for: .normal)
                    cell.lbl_Title4.text = homeList[indexPath.row - 1].title
                    cell.View_CategoryColor.backgroundColor = UIColor.init(hex: 0xFFDC65)
                    self.goingCountCateogry = "event"
                    cell.btnGoing.addTarget(self, action: #selector(btn_GoingEnterNewsFeed(sender:)), for: .touchUpInside)
                    cell.btnGoing.isHidden = false
                    cell.view_ClassCost.isHidden = true
                    cell.view_AgeGroup.isHidden = true
                    cell.view_Gender.isHidden = true
                    cell.view_TeacherType.isHidden = true
                    cell.view_AttendesType.isHidden = false
                    cell.lbl_AttendesType.isHidden = false
                    cell.btnGoing.tag = indexPath.row - 1
                    if homeList[indexPath.row - 1].attendeesType ?? 0 == 1{
                        cell.lbl_AttendesType.text = "Men Only"
                    }else if homeList[indexPath.row - 1].attendeesType ?? 0 == 2{
                        cell.lbl_AttendesType.text = "Women Only"
                    }else if homeList[indexPath.row - 1].attendeesType ?? 0 == 3{
                        cell.lbl_AttendesType.text = "Family"
                    }else{
                        cell.lbl_AttendesType.text = ""
                    }
                    //mark:GoingCount
                    cell.btn_GoingCount.isHidden = false
                    cell.btn_GoingCount.tag = indexPath.row - 1
                    let goingCount = (homeList[indexPath.row - 1].goingCount ?? 0)
                    let going = "\(String(describing: goingCount))" + " going"
                    cell.btn_GoingCount.setTitle(going, for: .normal)
                    cell.btn_GoingCount.addTarget(self, action: #selector(btn_GoingCountNewsFeed(sender:)), for: .touchUpInside)
                }
                    //Mark: Type class------------------------------
                else if self.homeList[indexPath.row - 1].category == "class"{
                    cell.view_Divider.isHidden = false
                    self.goingCountCateogry = "class"
                    cell.view_Share.isHidden = false
                    cell.layout_ShareHeight.constant = 34.0
                    cell.btnGoing.isHidden = true
                    cell.view_ClassCost.isHidden = false
                    cell.lbl_TeacherNAme.isHidden = false
                    cell.btn_ClassType.isHidden = true
                    cell.btn_GoingCount.isHidden = true
                    cell.view_Location.isHidden  = false
                    cell.view_MonthType.isHidden = false
                    cell.view_stackViewTypes.isHidden = false
                    cell.view_stackViewDonation.isHidden = true
                    cell.view_donationHeight.constant = 0.0
                    cell.height_stackViewTypes.constant = 120.0
                    cell.view_ContainAGGA.isHidden = false
                    
                    cell.btnReminder.setTitle("Reminder".localized, for: .normal)
                    cell.btnReminder.setImage(#imageLiteral(resourceName: "reminder_iPhone"), for: .normal)
                    cell.btnReminder.isUserInteractionEnabled = true
                    
                    if homeList[indexPath.row - 1].repeatingDay ?? 0 == 1{
                        cell.lbl_MonthType.text = "Weekly"
                    }else if homeList[indexPath.row - 1].repeatingDay ?? 0 == 2{
                        cell.lbl_MonthType.text = "Fortnightly"
                    }else if homeList[indexPath.row - 1].repeatingDay ?? 0 == 3{
                        cell.lbl_MonthType.text = "Monthly"
                    }else{
                        cell.lbl_MonthType.text = ""
                    }
                    cell.lblLocation.tag = indexPath.row - 1
                    cell.lblLocation.addTarget(self, action: #selector(btn_LocationNewsFeed(sender:)), for: .touchUpInside)
                    
                    let user_Input = self.homeList[indexPath.row - 1].userInput ?? ""
                    if self.homeList[indexPath.row - 1].classInputType == 1 {
                     
                        if self.homeList[indexPath.row - 1].selectClass == 1{
                            cell.btn_ClassType.setTitle("Quran & Tajweed", for: .normal)
                        }else if self.homeList[indexPath.row - 1].selectClass == 2{
                            cell.btn_ClassType.setTitle("Seerah & Hadith", for: .normal)
                        }else if self.homeList[indexPath.row - 1].selectClass == 3{
                            cell.btn_ClassType.setTitle("Islamic Studies", for: .normal)
                        }else if self.homeList[indexPath.row - 1].selectClass == 4{
                            cell.btn_ClassType.setTitle("Arabic learning", for: .normal)
                        }else if self.homeList[indexPath.row - 1].selectClass == 5{
                            cell.btn_ClassType.setTitle("Sports", for: .normal)
                        }else if self.homeList[indexPath.row - 1].selectClass == 6{
                            cell.btn_ClassType.setTitle("Academic", for: .normal)
                        }
                        else{
                            cell.btn_ClassType.setTitle("Language", for: .normal)
                        }
                    }else{
                        cell.btn_ClassType.setTitle(user_Input, for: .normal)
                    }
                    cell.view_ContainAGGA.isHidden = false
                    cell.view_AgeGroup.isHidden = false
                    cell.view_Gender.isHidden = false
                    cell.view_TeacherType.isHidden = false
                    cell.view_AttendesType.isHidden = true
                    cell.lbl_AttendesType.isHidden = true
                    DispatchQueue.main.async {
                        
                        if let imageUrl =  self.homeList[indexPath.row - 1].classesLogo{
                            cell.img_Logo.sd_setImage(with: URL(string: imageUrl), placeholderImage: #imageLiteral(resourceName: "default_header_iPhone"))
                        }else{
                            cell.img_Logo.image = #imageLiteral(resourceName: "default_header_iPhone")
                        }
                    }
                    cell.lbl_Title1.setTitle(homeList[indexPath.row - 1].org_abbr, for: .normal)
                    cell.lbl_Title1.tag = indexPath.row - 1
                    
                    cell.lbl_Title2.text = ""
                    cell.lbl_TeacherNAme.text = (homeList[indexPath.row - 1].teacherName ?? "")
                    
                    cell.btn_ViewCategory.setImage(#imageLiteral(resourceName: "classes_iPhone"), for: .normal)
                    cell.lbl_Title4.text = homeList[indexPath.row - 1].title
                    cell.View_CategoryColor.backgroundColor = UIColor.init(hex: 0xb68efe)
                    cell.lbl_AgeGroup.text = homeList[indexPath.row - 1].ageGroup ?? "0 to 0"
                    
                    if homeList[indexPath.row - 1].attendeesType ?? 0 == 1{
                        cell.lbl_Gender.text = "Men Only"
                    }else if homeList[indexPath.row - 1].attendeesType ?? 0 == 2{
                        cell.lbl_Gender.text = "Women Only"
                    }else if homeList[indexPath.row - 1].attendeesType ?? 0 == 3{
                        cell.lbl_Gender.text = "Family"
                    }else{
                        cell.lbl_Gender.text = ""
                    }
                    
                }    //Mark: Type activity------------------------------
                else if self.homeList[indexPath.row - 1].category == "activity"{
                    cell.view_Divider.isHidden = false
                    cell.btnGoing.tag = indexPath.row - 1
                    cell.view_Share.isHidden = false
                    cell.layout_ShareHeight.constant = 34.0
                    self.goingCountCateogry = "activity"
                    cell.view_MonthType.isHidden = true
                    cell.view_Location.isHidden  = false
                    cell.btn_ClassType.isHidden = true
                    cell.view_stackViewTypes.isHidden = false
                    cell.view_stackViewDonation.isHidden = true
                    cell.view_donationHeight.constant = 0.0
                    cell.height_stackViewTypes.constant = 60.0
                    cell.view_ContainAGGA.isHidden = true
                    cell.view_ClassCost.isHidden = true
                    cell.lbl_TeacherNAme.isHidden = true
                    cell.view_TeacherType.isHidden = true
                    cell.btnReminder.setTitle("Reminder".localized, for: .normal)
                    cell.btnReminder.setImage(#imageLiteral(resourceName: "reminder_iPhone"), for: .normal)
                    cell.btnReminder.isUserInteractionEnabled = true
                    DispatchQueue.main.async {
                        cell.btn_ClassType.isHidden = true
                        if let imageUrl =  self.homeList[indexPath.row - 1].activityLogo{
                            cell.img_Logo.sd_setImage(with: URL(string: imageUrl), placeholderImage: #imageLiteral(resourceName: "default_header_iPhone"))
                        }else{
                            cell.img_Logo.image = #imageLiteral(resourceName: "default_header_iPhone")
                        }
                    }
                    
                    cell.lblLocation.tag = indexPath.row - 1
                    cell.lblLocation.addTarget(self, action: #selector(btn_LocationNewsFeed(sender:)), for: .touchUpInside)
                    cell.lbl_Title1.tag = indexPath.row - 1
                    
                    cell.lbl_Title1.setTitle(homeList[indexPath.row - 1].org_abbr, for: .normal)
                    cell.lbl_Title2.text = ""
                    
                    cell.btn_ViewCategory.setImage(#imageLiteral(resourceName: "activities_iPhone"), for: .normal)
                    cell.lbl_Title4.text = homeList[indexPath.row - 1].title
                    cell.View_CategoryColor.backgroundColor = UIColor.init(hex: 0xF6877C)
                    
                    cell.btnGoing.isHidden = false
                    
                    cell.btnGoing.addTarget(self, action: #selector(btn_GoingEnterNewsFeed(sender:)), for: .touchUpInside)
                    
                    
                    if homeList[indexPath.row - 1].attendeesType ?? 0 == 1{
                        cell.lbl_AttendesType.text = "Men Only"
                    }else if homeList[indexPath.row - 1].attendeesType ?? 0 == 2{
                        cell.lbl_AttendesType.text = "Women Only"
                    }else if homeList[indexPath.row - 1].attendeesType ?? 0 == 3{
                        cell.lbl_AttendesType.text = "Family"
                    }else{
                        cell.lbl_AttendesType.text = ""
                    }
                    //mark:GoingCount
                    cell.btn_GoingCount.isHidden = false
                    cell.btn_GoingCount.tag = indexPath.row - 1
                    let goingCount = (homeList[indexPath.row - 1].goingCount ?? 0)
                    let going = "\(String(describing: goingCount))" + " going"
                    cell.btn_GoingCount.setTitle(going, for: .normal)
                    cell.btn_GoingCount.addTarget(self, action: #selector(btn_GoingCountNewsFeed(sender:)), for: .touchUpInside)
                    
                    
                    
                }else if self.homeList[indexPath.row - 1].category == "donation"{//Mark: Type donation------------------------------
                    cell.btnGoing.tag = indexPath.row - 1
                    cell.view_Divider.isHidden = true
                    self.goingCountCateogry = "donate"
                    cell.view_Share.isHidden = true
                    cell.layout_ShareHeight.constant = 0.0
                    cell.view_MonthType.isHidden = true
                    cell.btn_ClassType.isHidden = true
                    cell.view_Location.isHidden  = true
                    cell.view_stackViewTypes.isHidden = true
                    cell.height_stackViewTypes.constant = 0.0
                    cell.view_stackViewDonation.isHidden = false
                    
                    cell.view_ClassCost.isHidden = true
                    cell.lbl_TeacherNAme.isHidden = true
                    cell.view_TeacherType.isHidden = true
                    cell.view_ContainAGGA.isHidden = true
                    cell.btnReminder.setTitle("Reminder".localized, for: .normal)
                    cell.btnReminder.setImage(#imageLiteral(resourceName: "reminder_iPhone"), for: .normal)
                    cell.btnReminder.isUserInteractionEnabled = true
                    
                    DispatchQueue.main.async {
                        cell.btn_ClassType.isHidden = true
                        if let imageUrl =  self.homeList[indexPath.row - 1].donation_logo{
                            cell.img_Logo.sd_setImage(with: URL(string: imageUrl), placeholderImage: #imageLiteral(resourceName: "default_header_iPhone"))
                        }else{
                            cell.img_Logo.image = #imageLiteral(resourceName: "default_header_iPhone")
                        }
                    }
                    
                    cell.lblLocation.tag = indexPath.row - 1
                    cell.lblLocation.addTarget(self, action: #selector(btn_LocationNewsFeed(sender:)), for: .touchUpInside)
                    cell.lbl_Title1.tag = indexPath.row - 1
                    
                    cell.lbl_Title1.setTitle(homeList[indexPath.row - 1].org_abbr, for: .normal)
                    cell.lbl_Title2.text = ""
                    
                    cell.btn_ViewCategory.setImage(#imageLiteral(resourceName: "donation_iPhone"), for: .normal)
                    cell.lbl_Title4.text = homeList[indexPath.row - 1].title
                    cell.View_CategoryColor.backgroundColor = UIColor.init(hex: 0xF6877C)
                    
                    cell.btnGoing.isHidden = true
                    
                    cell.btnGoing.addTarget(self, action: #selector(btn_GoingEnterNewsFeed(sender:)), for: .touchUpInside)
                    
                    
                    if homeList[indexPath.row - 1].attendeesType ?? 0 == 1{
                        cell.lbl_AttendesType.text = "Men Only"
                    }else if homeList[indexPath.row - 1].attendeesType ?? 0 == 2{
                        cell.lbl_AttendesType.text = "Women Only"
                    }else if homeList[indexPath.row - 1].attendeesType ?? 0 == 3{
                        cell.lbl_AttendesType.text = "Family"
                    }else{
                        cell.lbl_AttendesType.text = ""
                    }
                    //mark:GoingCount
                    cell.btn_GoingCount.isHidden = true
                    cell.btn_GoingCount.tag = indexPath.row - 1
                    let goingCount = (homeList[indexPath.row - 1].goingCount ?? 0)
                    let going = "\(String(describing: goingCount))" + " going"
                    cell.btn_GoingCount.setTitle(going, for: .normal)
                    cell.btn_GoingCount.addTarget(self, action: #selector(btn_GoingCountNewsFeed(sender:)), for: .touchUpInside)
                    
                    
                    
                    
                }
                
//                cell.lbl_ReadMore.numberOfLines = 0
//                cell.lbl_ReadMore.enabledTypes = [.mention, .hashtag, .url]
                
               
                
                cell.btnEdit.tag = indexPath.row - 1
                cell.btnDelete.tag = indexPath.row - 1
                cell.btnEdit.addTarget(self, action: #selector(btnEditTapped_NewsFeed), for: .touchUpInside)
                cell.btnDelete.addTarget(self, action: #selector(btnDeleteTapped_NewsFeed), for: .touchUpInside)
//                cell.lbl_ReadMore.handleURLTap { url in
//                    
//                    let urlString = ("\(url)")
//                    var urll = String()
//                    
//                    if urlString.contains("https://") {
//                        urll = ("\(url)")
//                    }else if urlString.contains("http://") {
//                        urll = ("\(url)")
//                    }else{
//                        urll = ("http://\(url)")
//                    }
//                    
//                    
//                    guard let url = URL(string: urll) else { return }
//                    UIApplication.shared.open(url)
//                }
//             
//                var string = ""
//                                let input = homeList[indexPath.row - 1].datumDescription
//                                let detector = try! NSDataDetector(types: NSTextCheckingResult.CheckingType.phoneNumber.rawValue)
//                                let matches = detector.matches(in: input, options: [], range: NSRange(location: 0, length: input.utf16.count))
//
//                                for match in matches {
//                                    guard let range = Range(match.range, in: input) else { continue }
//                                    let url = input[range]
//                                    print(url)
//                                    string = String(url)
//                                }
//                              
//                                let customType = ActiveType.custom(pattern: "\\s\(string)\\b") //Regex that looks for "with"
//                                  cell.lbl_ReadMore.enabledTypes.append(customType)
//                                 cell.lbl_ReadMore.enabledTypes = [.mention, .hashtag, .url, customType]
//                               
//                                 cell.lbl_ReadMore.text = homeList[indexPath.row - 1].datumDescription
//                                 cell.lbl_ReadMore.customColor[customType] = UIColor.blue
//                                 cell.lbl_ReadMore.customSelectedColor[customType] = UIColor.blue
//
//                                cell.lbl_ReadMore.handleCustomTap(for: customType) { element in
//                                   
//                                    if let url = URL(string:"tel://\(element)"), UIApplication.shared.canOpenURL(url) {
//                                         UIApplication.shared.open(url)
//                                    }
//                                }
//
               cell.lbl_ReadMore.text = homeList[indexPath.row - 1].datumDescription
                                    
                      //      //mark:number deduction an d url
                              cell.lbl_ReadMore.enabledTextCheckingTypes = NSTextCheckingResult.CheckingType.link.rawValue
                              cell.lbl_ReadMore.delegate = self

                              self.phoneNumberDetctor(input:  cell.lbl_ReadMore.text as! String)
                              self.uRLDetctor(input:  cell.lbl_ReadMore.text as! String)

                                let newText =  cell.lbl_ReadMore.text as! NSString

                              if self.array_String.count != 0{
                                  for i in 0...self.array_String.count - 1{
                                          let range:NSRange = newText.range(of: array_String[i])
                                          cell.lbl_ReadMore.addLink(to: URL(string: array_String[i]), with: range)
                                         }
                              }
                
                let countLabl : Int =  cell.lbl_ReadMore.calculateMaxLines()
                //  print("readMore:",indexPath.row,countLabl)
                
                if countLabl == 1{
                    cell.btnShowHide.setTitle(nil, for: .normal)
                    cell.btnShowHide.setImage(nil, for: .normal)
                    cell.imgContraint_ButtonMoreHeight.constant = 0.0
                    cell.btnShowHide.isUserInteractionEnabled = false
                }else{
                    let model = self.homeList[indexPath.row - 1]
                    if model.isMoreTapped == true{
                        cell.btnShowHide.setTitle("Show less".localized, for: .normal)
                        cell.lbl_ReadMore.numberOfLines = 0
                    }else{
                        
                        cell.btnShowHide.setTitle("Show more".localized, for: .normal)
                        cell.lbl_ReadMore.numberOfLines = 1
                        //  print("cell height cell",cell.view_Announcment.size.height)
                        //  print("cell height main View",cell.size.height)
                        
                    }
                    
                    cell.btnShowHide.setImage(#imageLiteral(resourceName: "down_black_arrow_iPhone"), for: .normal)
                    cell.imgContraint_ButtonMoreHeight.constant = 30.0
                    cell.btnShowHide.isUserInteractionEnabled = true
                }
                
                if self.orgDetails != nil{
                    if self.orgDetails.isPrivate == true{
                        cell.btnShare.isHidden = true
                    }else{
                        
                        cell.btnShare.isHidden = false
                        cell.btnShare.addTarget(self, action: #selector(btn_shareTextButton(sender:)), for: .touchUpInside)
                    }
                }
                
                
                
                
                cell.btnReminder.addTarget(self, action: #selector(btn_EventRemainderTapped(sender:)), for: .touchUpInside)
                
                
                if self.homeList[indexPath.row - 1].going == true{
                    cell.btnGoing.setImage(#imageLiteral(resourceName: "tick_icon_iPhone"), for: .normal)
                }else{
                    cell.btnGoing.setImage(#imageLiteral(resourceName: "event_Going"), for: .normal)
                }
                return cell
                
            }
            
        case 1:
            //HG
            tableViewSection = 1
            let cell = tableView.dequeueReusableCell(withIdentifier: "OrgEventListTableViewCell", for: indexPath) as! OrgEventListTableViewCell
            cell.view_headerColor.backgroundColor = UIColor.init(hex: 0xFFDC65)
            cell.constrain_lblAttendesTitle_height.constant = 90
            cell.img_repeatingDay.isHidden = true
            cell.img_repeatingDay.image = nil
            cell.IndexImageSendCellDelegateActivities = self
            
            cell.imgEvent.tag = indexPath.row
            let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(imageTapped(tapGestureRecognizer:)))
            
            cell.imgEvent.isUserInteractionEnabled = true
            cell.imgEvent.addGestureRecognizer(tapGestureRecognizer)
            
            
            cell.configureCell(model: self.arrayEventModel[indexPath.row])
            var attendeesType: String = ""
            cell.lblAttendesTitle.isHidden = false
            cell.lblAttendeeType.isHidden = false
            if self.arrayEventModel[indexPath.row].attendees_type! == 1{
                attendeesType = "Men Only"
            }else if self.arrayEventModel[indexPath.row].attendees_type! == 2{
                attendeesType = "Women Only"
            }else if self.arrayEventModel[indexPath.row].attendees_type! == 3{
                attendeesType = "Family"
            }else{
                attendeesType = ""
            }
            cell.lblAttendeeType.text = attendeesType
            
            cell.btnReminder.tag = indexPath.row
            cell.btnShare.tag = indexPath.row
            cell.btnGoing.tag = indexPath.row
            cell.btnShowHide.tag = indexPath.row
            cell.btnEdit.tag = indexPath.row
            cell.btnDelete.tag = indexPath.row
            cell.btnLocation.tag = indexPath.row
            cell.btnLocation.addTarget(self, action: #selector(btn_Location(sender:)), for: .touchUpInside)
            cell.btnDelete.addTarget(self, action: #selector(btn_Delete(sender:)), for: .touchUpInside)
            cell.btnEdit.addTarget(self, action: #selector(btn_Edit(sender:)), for: .touchUpInside)
            
            
            cell.btnReminder.addTarget(self, action: #selector(btn_EventRemainderTapped(sender:)), for: .touchUpInside)
            
            cell.lblDesc.text = self.arrayEventModel[indexPath.row].description ?? ""
            
            //      //mark:number deduction an d url
            cell.lblDesc.enabledTextCheckingTypes = NSTextCheckingResult.CheckingType.link.rawValue
            cell.lblDesc.delegate = self as? TTTAttributedLabelDelegate

            self.phoneNumberDetctor(input:  cell.lblDesc.text as! String)
            self.uRLDetctor(input:  cell.lblDesc.text as! String)

            let newText =  cell.lblDesc.text as! NSString

            if self.array_String.count != 0{
                for i in 0...self.array_String.count - 1{
                    let range:NSRange = newText.range(of: array_String[i])
                    cell.lblDesc.addLink(to: URL(string: array_String[i]), with: range)
                }
            }

            
        /*    cell.lblDesc.numberOfLines = 0
            cell.lblDesc.enabledTypes = [.mention, .hashtag, .url]

            
            cell.lblDesc.handleURLTap { url in
                
                let urlString = ("\(url)")
                var urll = String()
                
                if urlString.contains("https://") {
                    urll = ("\(url)")
                }else if urlString.contains("http://") {
                    urll = ("\(url)")
                }else{
                    urll = ("http://\(url)")
                }
                
                
                guard let url = URL(string: urll) else { return }
                UIApplication.shared.open(url)
            }
            var string = ""
                                           let input = self.arrayEventModel[indexPath.row].description ?? ""
                                           let detector = try! NSDataDetector(types: NSTextCheckingResult.CheckingType.phoneNumber.rawValue)
                                           let matches = detector.matches(in: input, options: [], range: NSRange(location: 0, length: input.utf16.count))

                                           for match in matches {
                                               guard let range = Range(match.range, in: input) else { continue }
                                               let url = input[range]
                                               print(url)
                                               string = String(url)
                                           }
                                         
                                           let customType = ActiveType.custom(pattern: "\\s\(string)\\b") //Regex that looks for "with"
                                              cell.lblDesc.enabledTypes.append(customType)
                                             cell.lblDesc.enabledTypes = [.mention, .hashtag, .url, customType]
                                          
                                             cell.lblDesc.text = self.arrayEventModel[indexPath.row].description ?? ""
                                             cell.lblDesc.customColor[customType] = UIColor.blue
                                             cell.lblDesc.customSelectedColor[customType] = UIColor.blue

                                            cell.lblDesc.handleCustomTap(for: customType) { element in
                                              
                                               if let url = URL(string:"tel://\(element)"), UIApplication.shared.canOpenURL(url) {
                                                    UIApplication.shared.open(url)
                                               }
                                           }
            */
            // cell.lblDesc.text = self.arrayEventModel[indexPath.row].description ?? ""
            let countLabl : Int = cell.lblDesc.calculateMaxLines()
            
            if countLabl == 1{
                cell.btnShowHide.setTitle(nil, for: .normal)
                cell.btnShowHide.setImage(nil, for: .normal)
                cell.imgContraint_ButtonMoreHeight.constant = 0.0
                cell.btnShowHide.isUserInteractionEnabled = false
            }else{
                let model = self.arrayEventModel[indexPath.row]
                if model.isMoreTapped == true{
                    cell.btnShowHide.setTitle("Show less".localized, for: .normal)
                    cell.lblDesc.numberOfLines = 0
                }else{
                    cell.btnShowHide.setTitle("Show more".localized, for: .normal)
                     cell.lblDesc.numberOfLines = 1
                }
                
                cell.btnShowHide.setImage(#imageLiteral(resourceName: "down_black_arrow_iPhone"), for: .normal)
                cell.imgContraint_ButtonMoreHeight.constant = 30 //sakshi
                cell.btnShowHide.isUserInteractionEnabled = true
            }
            
            if self.orgDetails != nil{
                if self.orgDetails.isPrivate == true{
                    cell.btnShare.isHidden = true
                }else{
                    cell.btnShare.isHidden = false
                    cell.btnShare.addTarget(self, action: #selector(btn_shareTextButton(sender:)), for: .touchUpInside)
                }
                
            }
            
            self.goingCountCateogry = "Event"
            cell.btnGoing.addTarget(self, action: #selector(btn_GoingEnterEvent(sender:)), for: .touchUpInside)
            cell.btnGoingCount.tag = indexPath.row
            cell.btnGoingCount.addTarget(self, action: #selector(btn_GoingCountEventActivity(sender:)), for: .touchUpInside)
            
            let now = Date()
            let date = NSDate(timeIntervalSince1970: Double(self.arrayEventModel[indexPath.row].added_at ?? 0))
            cell.lblEventDay.text = now.timeAgoDisplay(dateCreated:date as Date,timeStamp:self.arrayEventModel[indexPath.row].added_at ?? 0)
            cell.delegate = self
            
            
            
            
            return cell
        case 2:
            tableViewSection = 2
            let cell = tableView.dequeueReusableCell(withIdentifier: "Org_Classes_TableViewCell", for: indexPath) as! Org_Classes_TableViewCell
            
            //Check for Admin,super Admin,Normal User----------------------
            if (UserDefaults.standard.value(forKey: "Login_Token") as? String) != nil{
                
                if let superAdmin = UserDefaults.standard.value(forKey: "isSuperAdmin") as? Bool{
                    
                    if superAdmin == true{
                        cell.btnEdit.isHidden = false
                        cell.btnDelete.isHidden = false
                        
                    }else{
                        
                        if let is_staff = UserDefaults.standard.value(forKey: "is_staff") as? Bool{
                            
                            if is_staff == true{
                                //mark:check For own Organisation---------------------------
                                if let orgOwn_Array = UserDefaults.standard.value(forKey: "OwnOrg_Array") as? [Int]{
                                    
                                    if orgOwn_Array.contains(self.classesModelData?.getClassData?[indexPath.row].org ?? 0){
                                        
                                        
                                        var org_P = [PriviligesData]()
                                        org_P = Global.shared.priviligesData
                                        let filertedData = org_P.filter{$0.org_id == self.classesModelData?.getClassData?[indexPath.row].org ?? 0}
                                        filertedData.forEach {
                                            //   print("model data objects::",$0.org_perm as? String)
                                            
                                            if $0.org_perm == "iqama_only"{
                                                
                                                cell.btnEdit.isHidden = true
                                                cell.btnDelete.isHidden = true
                                            }else{
                                                cell.btnEdit.isHidden = false
                                                cell.btnDelete.isHidden = false
                                                
                                                
                                            }
                                        }
                                        
                                    }else{
                                        cell.btnEdit.isHidden = true
                                        cell.btnDelete.isHidden = true
                                        
                                    }//orgOwn
                                    
                                }
                            }//staff true
                        }//staff
                        
                    }
                }//SuperAdmin
            }//Login
            else{
                
                cell.btnEdit.isHidden = true
                cell.btnDelete.isHidden = true
                
            }
            
            cell.configureCell(model: (self.classesModelData?.getClassData?[indexPath.row])!)
            cell.IndexImageSendCellDelegateClass = self
            cell.img_Viewannouncement.tag = indexPath.row
            let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(imageTapped(tapGestureRecognizer:)))
            
            
            cell.lblDesc.text = self.classesModelData?.getClassData?[indexPath.row].description ?? ""
                            
              //      //mark:number deduction an d url
                      cell.lblDesc.enabledTextCheckingTypes = NSTextCheckingResult.CheckingType.link.rawValue
                      cell.lblDesc.delegate = self

              self.phoneNumberDetctor(input:  cell.lbl_ReadMore.text!)
                      self.uRLDetctor(input:  cell.lblDesc.text as! String)

                        let newText =  cell.lblDesc.text as! NSString

                      if self.array_String.count != 0{
                          for i in 0...self.array_String.count - 1{
                                  let range:NSRange = newText.range(of: array_String[i])
                                  cell.lblDesc.addLink(to: URL(string: array_String[i]), with: range)
                                 }
                      }
                      
            let countLabl : Int = cell.lblDesc.calculateMaxLines()
            if countLabl == 1{
                cell.buttonMore.setTitle(nil, for: .normal)
                cell.buttonMore.setImage(nil, for: .normal)
                cell.imgContraint_ButtonMoreHeight.constant = 0.0
                cell.buttonMore.isUserInteractionEnabled = false
            }else{
                //            let model = self.arrayEventModel[indexPath.row]
                if (self.classesModelData?.getClassData?[indexPath.row])?.isMoreTapped == true{
                    cell.buttonMore.setTitle("Show less".localized, for: .normal)
                    cell.lblDesc.numberOfLines = 0
                }else{
                    cell.buttonMore.setTitle("Show more".localized, for: .normal)
                    cell.lblDesc.numberOfLines = 1
                }
                
                cell.buttonMore.setImage(#imageLiteral(resourceName: "down_black_arrow_iPhone"), for: .normal)
                cell.imgContraint_ButtonMoreHeight.constant = 39.5
                cell.buttonMore.isUserInteractionEnabled = true
            }
            cell.img_Viewannouncement.isUserInteractionEnabled = true
            cell.img_Viewannouncement.addGestureRecognizer(tapGestureRecognizer)
            self.goingCountCateogry = "Class"
            cell.buttonMore.tag = indexPath.row
            cell.btnDelete.tag = indexPath.row
            cell.btn_Location.tag = indexPath.row
            cell.btnEdit.tag = indexPath.row
            
            let linkAttributes: [NSAttributedString.Key : Any] = [
                               NSAttributedString.Key(rawValue: NSAttributedString.Key.foregroundColor.rawValue): UIColor.black,
                               NSAttributedString.Key(rawValue: NSAttributedString.Key.underlineColor.rawValue): UIColor.black,
                               NSAttributedString.Key(rawValue: NSAttributedString.Key.underlineStyle.rawValue): NSUnderlineStyle.single.rawValue
                           ]
                
                  let attributedString = NSMutableAttributedString(string: self.classesModelData?.getClassData?[indexPath.row].address ?? "", attributes: linkAttributes)
                  
                  attributedString.addAttribute(NSAttributedString.Key.underlineStyle, value: NSUnderlineStyle.single.rawValue, range: NSRange(location: 0, length: (self.classesModelData?.getClassData?[indexPath.row].address!.count)!))
                        
                
                           cell.btn_Location.setAttributedTitle(attributedString, for: .normal)
            
            cell.btn_Location.addTarget(self, action: #selector(btn_ClassLocation(sender:)), for: .touchUpInside)
            cell.btnDelete.addTarget(self, action: #selector(btn_DeleteClasses(sender:)), for: .touchUpInside)
            cell.btnEdit.addTarget(self, action: #selector(btn_EditClasses(sender:)), for: .touchUpInside)
            
            
            
            cell.btnReminder.addTarget(self, action: #selector(btn_EventRemainderTapped(sender:)), for: .touchUpInside)
            
            
            
            
            
            //adds shadow to the layer of cell
            
            cell.view_Announcment.layer.cornerRadius = 10.0
            cell.view_Announcment.shadowBlackToHeader()
            //cell.btn_Location.underlineMyText()
            //makes the cell round
            let containerView = cell.contentView
            containerView.layer.cornerRadius = 2
            containerView.clipsToBounds = true
            
            let now = Date()
            let date = NSDate(timeIntervalSince1970: Double(self.classesModelData?.getClassData?[indexPath.row].added_at ?? 0))
            cell.lblClassesDay.text = now.timeAgoDisplay(dateCreated:date as Date,timeStamp:self.classesModelData?.getClassData?[indexPath.row].added_at ?? 0)
            
            cell.lblAge.text = self.classesModelData?.getClassData?[indexPath.row].age_group ?? ""
            
            if self.orgDetails != nil{
                if self.orgDetails.isPrivate == true{
                    cell.btnShare.isHidden = true
                }else{
                    cell.btnShare.isHidden = false
                    cell.btnShare.tag = indexPath.row
                    cell.btnShare.addTarget(self, action: #selector(btn_shareTextButton(sender:)), for: .touchUpInside)
                    
                }
            }
            
            let cost = self.classesModelData?.getClassData?[indexPath.row].free ?? 0
            if cost == 1{
                cell.lbl_Cost.text = "Free"
            }else{
                cell.lbl_Cost.text = "Paid"
            }
            
            
            //   let select_Class = self.classesModelData?.getClassData?[indexPath.row].select_class ?? 0
            
            
            let user_Input = self.classesModelData?.getClassData?[indexPath.row].user_input ?? ""
            if self.classesModelData?.getClassData?[indexPath.row].class_input_type == 1 {
            
                if self.classesModelData?.getClassData?[indexPath.row].select_class == 1{
                    cell.btnQuran.setTitle("Quran & Tajweed", for: .normal)
                }else if self.classesModelData?.getClassData?[indexPath.row].select_class == 2{
                    cell.btnQuran.setTitle("Seerah & Hadith", for: .normal)
                }else if self.classesModelData?.getClassData?[indexPath.row].select_class == 3{
                    cell.btnQuran.setTitle("Islamic Studies", for: .normal)
                }else if self.classesModelData?.getClassData?[indexPath.row].select_class == 4{
                    cell.btnQuran.setTitle("Arabic learning", for: .normal)
                }else if self.classesModelData?.getClassData?[indexPath.row].select_class == 5{
                    cell.btnQuran.setTitle("Sports", for: .normal)
                }else if self.classesModelData?.getClassData?[indexPath.row].select_class == 6{
                    cell.btnQuran.setTitle("Academic", for: .normal)
                }
                else{
                    cell.btnQuran.setTitle("Language", for: .normal)
                }
                
            }else{
                cell.btnQuran.setTitle(user_Input, for: .normal)
            }
            
            //            if select_Class == 1{
            //                cell.btnQuran.setTitle("Quran", for: .normal)
            //            }else{
            //                cell.btnQuran.setTitle("Kids Quran", for: .normal)
            //            }
            
            if self.classesModelData?.getClassData?[indexPath.row].repeating_day ?? 0 == 1{
                cell.lblMonthType.text = "Weekly"
            }else if self.classesModelData?.getClassData?[indexPath.row].repeating_day ?? 0 == 2{
                cell.lblMonthType.text = "Fortnightly"
            }else if self.classesModelData?.getClassData?[indexPath.row].repeating_day ?? 0 == 3{
                cell.lblMonthType.text = "Monthly"
            }else{
                cell.lblMonthType.text = ""
            }
            if self.classesModelData?.getClassData?[indexPath.row].attendees_type ?? 0 == 1{
                cell.lblGender.text = "Men Only"
            }else if self.classesModelData?.getClassData?[indexPath.row].attendees_type ?? 0 == 2{
                cell.lblGender.text = "Women Only"
            }else if self.classesModelData?.getClassData?[indexPath.row].attendees_type ?? 0 == 3{
                cell.lblGender.text = "Family"
            }else{
                cell.lblGender.text = ""
            }
            
            cell.registerCell()
            cell.delegateClasses = self
            //cell.collection_Images.reloadData()
            
            
            // cell.btnShare.addTarget(self, action: #selector(btn_shareTextButton(sender:)), for: .touchUpInside)
            
            
            return cell
        case 3:
            tableViewSection = 3
            
            if indexPath.row == 0{
                
                if (self.iqamaListsData?.data?.iqama?.count == 0) && (self.iqamaListsData?.data?.jumma?.count == 0){
                    
                    return UITableViewCell()
                }else{
                    
                    let cell = tableView.dequeueReusableCell(withIdentifier: "IqamahListTableViewCell", for: indexPath) as! IqamahListTableViewCell //IqamahTableViewCell
                    
                    cell.iqamaListDelegate = self
                    
                    cell.btn_editIqama.tag = indexPath.row
                    cell.cellSetUp(indexPath: indexPath ,id: orgId ?? 0,iqamaData: self.iqamaListsData)
                    cell.btn_editIqama.addTarget(self, action: #selector(iqamaListsEdit(sender:)), for: .touchUpInside)
                    
                    cell.btn_Report.addTarget(self, action: #selector(reportIqamaApi(sender:)), for: .touchUpInside)
                    
                    if self.orgDetails != nil{
                        if self.orgDetails.isPrivate == true{
                            cell.btn_Share.isHidden = true
                        }else{
                            cell.btn_Share.isHidden = false
                            cell.btn_Share.addTarget(self, action: #selector(btn_shareIQama(sender:)), for: .touchUpInside)
                            
                            
                        }}
                    
                    cell.tableViewHeight.constant = iqamaInnertableviewHeight 
                    cell.tableView.layoutIfNeeded()
                    cell.tableView.beginUpdates()
                    cell.tableView.endUpdates()
                    
                    return cell
                }
            }else{
                let cell = tableView.dequeueReusableCell(withIdentifier: "IqamahListTableViewCell", for: indexPath) as! IqamahListTableViewCell //IqamahTableViewCell
                
                cell.iqamaListDelegate = self
                cell.btn_editIqama.tag = indexPath.row
                cell.cellSetUp(indexPath: indexPath ,id: orgId ?? 0,iqamaData: self.iqamaListsData)
                cell.btn_editIqama.addTarget(self, action: #selector(iqamaListsEdit(sender:)), for: .touchUpInside)
                
                cell.btn_Report.addTarget(self, action: #selector(reportIqamaApi(sender:)), for: .touchUpInside)
                
                cell.tableViewHeight.constant = iqamaInnertableviewHeight 
                cell.tableView.layoutIfNeeded()
                cell.tableView.beginUpdates()
                cell.tableView.endUpdates()
                
                
                
                return cell
            }
            
            
        case 4:
            
            tableViewSection = 4
            let cell = tableView.dequeueReusableCell(withIdentifier: "OrgEventListTableViewCell", for: indexPath) as! OrgEventListTableViewCell
            cell.IndexImageSendCellDelegateActivities = self
            cell.view_headerColor.backgroundColor = UIColor.init(hex: 0xF6877C)
            cell.lblAttendeeType.isHidden = false
            cell.lblAttendesTitle.isHidden = false
            cell.img_repeatingDay.isHidden = false
            cell.img_repeatingDay.image = #imageLiteral(resourceName: "event_Calender")
            //    cell.constrain_lblAttendesTitle_height.constant = 0.0
            
            cell.imgEvent.tag = indexPath.row
            let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(imageTapped(tapGestureRecognizer:)))
            
            cell.imgEvent.isUserInteractionEnabled = true
            cell.imgEvent.addGestureRecognizer(tapGestureRecognizer)
            
            
            //Check for Admin,super Admin,Normal User----------------------
            if (UserDefaults.standard.value(forKey: "Login_Token") as? String) != nil{
                
                if let superAdmin = UserDefaults.standard.value(forKey: "isSuperAdmin") as? Bool{
                    
                    if superAdmin == true{
                        cell.btnEdit.isHidden = false
                        cell.btnDelete.isHidden = false
                        //  cell.lblNumberOfAttendee.textColor = UIColor.init(hex: 0x0DC9E4)
                        //  cell.imgGoingCount.image = #imageLiteral(resourceName: "group_icon_blue_iPhone")
                    }else{
                        
                        if let is_staff = UserDefaults.standard.value(forKey: "is_staff") as? Bool{
                            
                            if is_staff == true{
                                //mark:check For own Organisation---------------------------
                                if let orgOwn_Array = UserDefaults.standard.value(forKey: "OwnOrg_Array") as? [Int]{
                                    
                                    if orgOwn_Array.contains(self.arrayActivityModel[indexPath.row].org ?? 0){
                                        var org_P = [PriviligesData]()
                                        org_P = Global.shared.priviligesData
                                        let filertedData = org_P.filter{$0.org_id == self.arrayActivityModel[indexPath.row].org ?? 0}
                                        filertedData.forEach {
                                            //   print("model data objects::",$0.org_perm as? String)
                                            
                                            if $0.org_perm == "iqama_only"{
                                                
                                                cell.btnEdit.isHidden = true
                                                cell.btnDelete.isHidden = true
                                            }else{
                                                cell.btnEdit.isHidden = false
                                                cell.btnDelete.isHidden = false
                                                
                                                
                                            }
                                        }
                                        
                                        // cell.lblNumberOfAttendee.textColor = UIColor.init(hex: 0x0DC9E4)
                                        // cell.imgGoingCount.image = #imageLiteral(resourceName: "group_icon_blue_iPhone")
                                        
                                    }else{
                                        cell.btnEdit.isHidden = true
                                        cell.btnDelete.isHidden = true
                                        //   cell.lblNumberOfAttendee.textColor = UIColor.gray
                                        //   cell.imgGoingCount.image = #imageLiteral(resourceName: "group_icon_iPhone")
                                    }//orgOwn
                                    
                                }
                            }//staff true
                        }//staff
                        
                    }
                }//SuperAdmin
            }//Login
            else{
                
                cell.btnEdit.isHidden = true
                cell.btnDelete.isHidden = true
                // cell.lblNumberOfAttendee.textColor = UIColor.gray
                // cell.imgGoingCount.image = #imageLiteral(resourceName: "group_icon_iPhone")
            }
            
            cell.constrain_lblAttendesTitle_height.constant = 0.0
            self.goingCountCateogry = "Activity"
            cell.configureCell(model: self.arrayActivityModel[indexPath.row])
            var attendeesType = String()
            if self.arrayActivityModel[indexPath.row].repeating_day! == 1{
                attendeesType = "Weekly"
            }else if self.arrayActivityModel[indexPath.row].repeating_day! == 2{
                attendeesType = "Fortnightly"
            }else if self.arrayActivityModel[indexPath.row].repeating_day! == 3{
                attendeesType = "Monthly"
            }else{
                attendeesType = ""
            }
            
            
            cell.lblAttendeeType.text = attendeesType
            cell.btnShowHide.tag = indexPath.row
            cell.btnEdit.tag = indexPath.row
            cell.btnDelete.tag = indexPath.row
            cell.btnLocation.tag = indexPath.row
            cell.btnReminder.tag = indexPath.row
            
            cell.btnLocation.addTarget(self, action: #selector(btn_LocationActivites(sender:)), for: .touchUpInside)
            cell.btnDelete.addTarget(self, action: #selector(btn_Delete(sender:)), for: .touchUpInside)
            cell.btnEdit.addTarget(self, action: #selector(btn_Edit(sender:)), for: .touchUpInside)
//            cell.lblDesc.numberOfLines = 0
//            cell.lblDesc.enabledTypes = [.mention, .hashtag, .url]
//            cell.lblDesc.text = self.arrayActivityModel[indexPath.row].description ?? ""
//            //        cell.lbl_ReadMore.handleHashtagTap { hashtag in
//            //            print("Success. You just tapped the \(hashtag) hashtag")
//            //        }
//            cell.lblDesc.handleURLTap { url in
//
//                let urlString = ("\(url)")
//                var urll = String()
//
//
//                if urlString.contains("https://") {
//                    urll = ("\(url)")
//                }else if urlString.contains("http://") {
//                    urll = ("\(url)")
//                }else{
//                    urll = ("http://\(url)")
//                }
//
//
//                guard let url = URL(string: urll) else { return }
//                UIApplication.shared.open(url)
//            }
//
//            var string = ""
//                                                  let input = self.arrayActivityModel[indexPath.row].description ?? ""
//                                                  let detector = try! NSDataDetector(types: NSTextCheckingResult.CheckingType.phoneNumber.rawValue)
//                                                  let matches = detector.matches(in: input, options: [], range: NSRange(location: 0, length: input.utf16.count))
//
//                                                  for match in matches {
//                                                      guard let range = Range(match.range, in: input) else { continue }
//                                                      let url = input[range]
//                                                      print(url)
//                                                      string = String(url)
//                                                  }
//
//                                                  let customType = ActiveType.custom(pattern: "\\s\(string)\\b") //Regex that looks for "with"
//                                                     cell.lblDesc.enabledTypes.append(customType)
//                                                    cell.lblDesc.enabledTypes = [.mention, .hashtag, .url, customType]
//
//                                                    cell.lblDesc.text = self.arrayActivityModel[indexPath.row].description ?? ""
//                                                    cell.lblDesc.customColor[customType] = UIColor.blue
//                                                    cell.lblDesc.customSelectedColor[customType] = UIColor.blue
//
//                                                   cell.lblDesc.handleCustomTap(for: customType) { element in
//
//                                                      if let url = URL(string:"tel://\(element)"), UIApplication.shared.canOpenURL(url) {
//                                                           UIApplication.shared.open(url)
//                                                      }
//                                                  }
//
            cell.lblDesc.text = self.arrayActivityModel[indexPath.row].description ?? ""

                  //      //mark:number deduction an d url
                  cell.lblDesc.enabledTextCheckingTypes = NSTextCheckingResult.CheckingType.link.rawValue
                  cell.lblDesc.delegate = self as? TTTAttributedLabelDelegate

                  self.phoneNumberDetctor(input:  cell.lblDesc.text as! String)
                  self.uRLDetctor(input:  cell.lblDesc.text as! String)

                  let newText =  cell.lblDesc.text as! NSString

                  if self.array_String.count != 0{
                      for i in 0...self.array_String.count - 1{
                          let range:NSRange = newText.range(of: array_String[i])
                          cell.lblDesc.addLink(to: URL(string: array_String[i]), with: range)
                      }
                  }
            // cell.lblDesc.text = self.arrayActivityModel[indexPath.row].description ?? ""
            let countLabl : Int = cell.lblDesc.calculateMaxLines()
            //  print("readMore:",indexPath.row,countLabl)
            
            cell.btnReminder.addTarget(self, action: #selector(btn_EventRemainderTapped(sender:)), for: .touchUpInside)
            
            
            
            if countLabl == 1{
                cell.btnShowHide.setTitle(nil, for: .normal)
                cell.btnShowHide.setImage(nil, for: .normal)
                cell.imgContraint_ButtonMoreHeight.constant = 0.0
                cell.btnShowHide.isUserInteractionEnabled = false
            }else{
                let model = self.arrayActivityModel[indexPath.row]
                if model.isMoreTapped == true{
                    cell.btnShowHide.setTitle("Show less".localized, for: .normal)
                    cell.lblDesc.numberOfLines = 0
                }else{
                    cell.btnShowHide.setTitle("Show more".localized, for: .normal)
                    cell.lblDesc.numberOfLines = 1
                }
                
                cell.btnShowHide.setImage(#imageLiteral(resourceName: "down_black_arrow_iPhone"), for: .normal)
                cell.imgContraint_ButtonMoreHeight.constant = 30 //sakshi
                cell.btnShowHide.isUserInteractionEnabled = true
            }
            cell.btnGoing.tag = indexPath.row
            if self.orgDetails != nil{
                if self.orgDetails.isPrivate == true{
                    cell.btnShare.isHidden = true
                }else{
                    cell.btnShare.isHidden = false
                    cell.btnShare.addTarget(self, action: #selector(btn_shareTextButton(sender:)), for: .touchUpInside)
                    
                    
                }
            }
            cell.btnGoing.addTarget(self, action: #selector(btn_GoingEnterActivity(sender:)), for: .touchUpInside)
            
            cell.btnGoingCount.tag = indexPath.row
            cell.btnGoingCount.addTarget(self, action: #selector(btn_GoingCountEventActivity(sender:)), for: .touchUpInside)
            
            cell.delegate = self
            let now = Date()
            let date = NSDate(timeIntervalSince1970: Double(self.arrayActivityModel[indexPath.row].added_at ?? 0))
            cell.lblEventDay.text = now.timeAgoDisplay(dateCreated:date as Date,timeStamp:self.arrayActivityModel[indexPath.row].added_at ?? 0)
            return cell
        case 5:
            tableViewSection = 5
            
            if indexPath.row == 0{
                
                let cell = tableView.dequeueReusableCell(withIdentifier: "OrgsAboutUsTableCell", for: indexPath) as! OrgsAboutUsTableCell
                
                
                //Check for Admin,super Admin,Normal User----------------------
                if (UserDefaults.standard.value(forKey: "Login_Token") as? String) != nil{
                    
                    cell.stackView_GeneralSecetry.isHidden = false
                    cell.stackView_Imam.isHidden = false
                    cell.stackView_President.isHidden = false
                    
                    if let superAdmin = UserDefaults.standard.value(forKey: "isSuperAdmin") as? Bool{
                        
                        if superAdmin == true{
                            cell.stackView_GeneralSecetry.isHidden = false
                            cell.stackView_Imam.isHidden = false
                            cell.stackView_President.isHidden = false
                            cell.btn_DescEdit.isHidden = false
                            
                            cell.btn_ContactEdit.isHidden = false
                            cell.btn_AddressEdit.isHidden = false
                            cell.btn_PresidentEdit.isHidden = false
                        }else{
                            
                            if let is_staff = UserDefaults.standard.value(forKey: "is_staff") as? Bool{
                                
                                if is_staff == true{
                                    //mark:check For own Organisation---------------------------
                                    if let orgOwn_Array = UserDefaults.standard.value(forKey: "OwnOrg_Array") as? [Int]{
                                        
                                        if orgOwn_Array.contains(arrayAboutUsOrgData[0].id ?? 0){
                                            
                                            var org_P = [PriviligesData]()
                                            org_P = Global.shared.priviligesData
                                            let filertedData = org_P.filter{$0.org_id == arrayAboutUsOrgData[0].id ?? 0}
                                            filertedData.forEach {
                                                //   print("model data objects::",$0.org_perm as? String)
                                                
                                                if $0.org_perm == "iqama_only"{
                                                    if self.arrayAboutUsOrgData[0].gs == ""{
                                                        cell.stackView_GeneralSecetry.isHidden = true
                                                    }
                                                    if self.arrayAboutUsOrgData[0].imam == ""{
                                                        cell.stackView_Imam.isHidden = true
                                                    }
                                                    if self.arrayAboutUsOrgData[0].president == ""{
                                                        cell.stackView_President.isHidden = true
                                                    }
                                                    
                                                    if self.arrayAboutUsOrgData[0].gs == "" && self.arrayAboutUsOrgData[0].imam == "" && self.arrayAboutUsOrgData[0].president == ""{
                                                        cell.layout_TopAdress.constant = -30
                                                        cell.view_LineAdress.isHidden = true
                                                        
                                                    }
                                                    cell.btn_DescEdit.isHidden = true
                                                    
                                                    cell.btn_ContactEdit.isHidden = true
                                                    cell.btn_AddressEdit.isHidden = true
                                                    cell.btn_PresidentEdit.isHidden = true
                                                }else{
                                                    
                                                    
                                                    cell.btn_DescEdit.isHidden = false
                                                    
                                                    cell.btn_ContactEdit.isHidden = false
                                                    cell.btn_AddressEdit.isHidden = false
                                                    cell.btn_PresidentEdit.isHidden = false
                                                }
                                            }
                                            
                                            
                                            
                                            
                                        }else{
                                            if self.arrayAboutUsOrgData[0].gs == "" || self.arrayAboutUsOrgData[0].gs == nil {
                                                cell.stackView_GeneralSecetry.isHidden = true
                                            }
                                            if self.arrayAboutUsOrgData[0].imam == "" || self.arrayAboutUsOrgData[0].imam == nil {
                                                cell.stackView_Imam.isHidden = true
                                            }
                                            if self.arrayAboutUsOrgData[0].president == "" || self.arrayAboutUsOrgData[0].president == nil {
                                                cell.stackView_President.isHidden = true
                                            }
                                            if self.arrayAboutUsOrgData[0].gs == "" && self.arrayAboutUsOrgData[0].imam == "" && self.arrayAboutUsOrgData[0].president == ""{
                                                cell.layout_TopAdress.constant = -30
                                                cell.view_LineAdress.isHidden = true
                                            }
                                            cell.btn_DescEdit.isHidden = true
                                            
                                            cell.btn_ContactEdit.isHidden = true
                                            cell.btn_AddressEdit.isHidden = true
                                            cell.btn_PresidentEdit.isHidden = true
                                        }//orgOwn
                                        
                                    }
                                }//staff true
                            }//staff
                            
                        }
                    }//SuperAdmin
                }//Login
                else{
                    
                    if self.arrayAboutUsOrgData[0].gs == ""{
                        cell.stackView_GeneralSecetry.isHidden = true
                    }
                    if self.arrayAboutUsOrgData[0].imam == ""{
                        cell.stackView_Imam.isHidden = true
                    }
                    if self.arrayAboutUsOrgData[0].president == ""{
                        cell.stackView_President.isHidden = true
                    }
                    if self.arrayAboutUsOrgData[0].gs == "" && self.arrayAboutUsOrgData[0].imam == "" && self.arrayAboutUsOrgData[0].president == ""{
                        cell.layout_TopAdress.constant = -30
                        cell.view_LineAdress.isHidden = true
                    }
                    cell.btn_DescEdit.isHidden = true
                    cell.btn_CountryCode.isHidden = false
                    cell.btn_ContactEdit.isHidden = true
                    cell.btn_AddressEdit.isHidden = true
                    cell.btn_PresidentEdit.isHidden = true
                }
                
                cell.txtDesc_View.text = self.arrayAboutUsOrgData[0].description
                cell.txtField_EmailID.text = self.arrayAboutUsOrgData[0].org_email
                cell.btn_Location.tag = indexPath.row
                let linkAttributes: [NSAttributedString.Key : Any] = [
                    NSAttributedString.Key(rawValue: NSAttributedString.Key.foregroundColor.rawValue): UIColor.gray,
                    NSAttributedString.Key(rawValue: NSAttributedString.Key.underlineColor.rawValue): UIColor.lightGray,
                    NSAttributedString.Key(rawValue: NSAttributedString.Key.underlineStyle.rawValue): NSUnderlineStyle.single.rawValue
                ]
                
                if self.dic_RegisterValue["address"] != nil{
                    
                    
                    let attributeString = NSMutableAttributedString(string: self.dic_RegisterValue["address"] as? String ?? "", attributes: linkAttributes)
                    
                    cell.btn_Location.attributedText = attributeString
                }else{
                    
                    
                    let attributeString = NSMutableAttributedString(string: (self.arrayAboutUsOrgData[0].address)!, attributes: linkAttributes)
                    
                    cell.btn_Location.attributedText = attributeString
                    
                }
                
                
                
                
                
                let gesture = UITapGestureRecognizer(target: self, action: #selector(btn_OrgAboutUsLocation))
                // if labelView is not set userInteractionEnabled, you must do so
                cell.btn_Location.isUserInteractionEnabled = true
                cell.btn_Location.addGestureRecognizer(gesture)
                
                
                if self.arrayAboutUsOrgData[0].gs != ""{
                    
                    cell.txtField_GeneralSecetry.text =  self.arrayAboutUsOrgData[0].gs
                }
                if self.arrayAboutUsOrgData[0].imam != ""{
                    
                    cell.txtField_Imam.text =  self.arrayAboutUsOrgData[0].imam
                }
                if self.arrayAboutUsOrgData[0].president != ""{
                    
                    cell.txtField_President.text =  self.arrayAboutUsOrgData[0].president
                }
                
                
                let phoneCodeWithNumber = self.arrayAboutUsOrgData[0].org_contact?.components(separatedBy: "-")
                if phoneCodeWithNumber![0] == "" || phoneCodeWithNumber?[1] == ""{
                    cell.btn_CountryCode.setTitle("+61", for: .normal)
                    cell.txtField_phoneNumber.text = ""
                    self.fullPhoneNumber = "+61"
                }else{
                    cell.btn_CountryCode.setTitle(phoneCodeWithNumber?[0], for: .normal)
                    cell.txtField_phoneNumber.text = phoneCodeWithNumber?[1]
                    let code = phoneCodeWithNumber![0]
                    self.fullPhoneNumber = (code + "-" + cell.txtField_phoneNumber.text!)
                }
                
                
                //cell.txtDesc_View.delegate = self
                cell.txtField_President.delegate = self
                
                //Mark:Description Edit--------------------------
                if cell.btn_DescEdit.isSelected{
                    cell.txtDesc_View.isEditable = true
                    
                }else{
                    
                    cell.txtDesc_View.isEditable = false
                    
                }
                cell.buttonDescPressed = {
                    //Code
                    if cell.txtDesc_View.isEditable == true{
                        cell.btn_DescEdit.isSelected = true
                        self.moveToMHDescViewController(cell: cell)
                        
                    }else{
                        
                        cell.btn_DescEdit.isSelected = false
                        self.EditActivitesAboutUsApi(id: self.arrayAboutUsOrgData[0].id ?? 0, description: cell.txtDesc_View.text ?? "", president: cell.txtField_President.text ?? "", gs: cell.txtField_GeneralSecetry.text ?? "", imam: cell.txtField_Imam.text ?? "", latitude: self.arrayAboutUsOrgData[0].latitude!, longitude: self.arrayAboutUsOrgData[0].longitude!, address: cell.btn_Location.text ?? "", org_email: cell.txtField_EmailID.text ?? "", org_contact: self.fullPhoneNumber ?? "", pre_up_iq: self.arrayAboutUsOrgData[0].pre_up_iq!)
                    }
                }
                cell.txtField_EmailID.delegate = self
                //Mark:Contact Edit--------------------------
                if cell.btn_ContactEdit.isSelected{
                    
                    cell.txtField_phoneNumber.isUserInteractionEnabled = true
                    cell.txtField_EmailID.isUserInteractionEnabled = true
                    cell.btn_CountryCode.isUserInteractionEnabled = true
                    self.EditActivitesAboutUsApi(id: self.arrayAboutUsOrgData[0].id ?? 0, description: cell.txtDesc_View.text ?? "", president: cell.txtField_President.text ?? "", gs: cell.txtField_GeneralSecetry.text ?? "", imam: cell.txtField_Imam.text ?? "", latitude: self.arrayAboutUsOrgData[0].latitude!, longitude: self.arrayAboutUsOrgData[0].longitude!, address: cell.btn_Location.text ?? "", org_email: cell.txtField_EmailID.text ?? "", org_contact: self.fullPhoneNumber ?? "", pre_up_iq: self.arrayAboutUsOrgData[0].pre_up_iq!)
                }else{
                    cell.txtField_phoneNumber.isUserInteractionEnabled = false
                    cell.txtField_EmailID.isUserInteractionEnabled = false
                    cell.btn_CountryCode.isUserInteractionEnabled = false
                }
                cell.buttonContactPressed = {
                    if cell.txtField_EmailID.isUserInteractionEnabled == true{
                        cell.btn_ContactEdit.isSelected = true
                        cell.txtField_EmailID.becomeFirstResponder()
                    }else{
                        var phoneNumber = cell.txtField_phoneNumber.text ?? ""
                        while (phoneNumber.hasPrefix("0")) {
                            phoneNumber.remove(at: (phoneNumber.startIndex))
                            
                        }
                        
                        if phoneNumber.count > 10 {
                            
                            AKAlertController.alert("Please Enter Valid Contact")
                        }else{
                            self.countrycode = cell.btn_CountryCode.titleLabel?.text ?? "+61"
                            self.fullPhoneNumber = (self.countrycode + "-" + phoneNumber)
                        }
                        
                        
                        
                        self.EditActivitesAboutUsApi(id: self.arrayAboutUsOrgData[0].id ?? 0, description: cell.txtDesc_View.text ?? "", president: cell.txtField_President.text ?? "", gs: cell.txtField_GeneralSecetry.text ?? "", imam: cell.txtField_Imam.text ?? "", latitude: self.arrayAboutUsOrgData[0].latitude!, longitude: self.arrayAboutUsOrgData[0].longitude!, address: cell.btn_Location.text ?? "", org_email: cell.txtField_EmailID.text ?? "", org_contact: self.fullPhoneNumber ?? "", pre_up_iq: self.arrayAboutUsOrgData[0].pre_up_iq!)
                        
                        cell.btn_ContactEdit.isSelected = false
                    }
                }
                //Mark:Adress Edit--------------------------
                
                
                cell.buttonAddressPressed = {
                    if cell.btn_Location.isUserInteractionEnabled == true{
                        cell.btn_AddressEdit.isSelected = true
                        self.openAutoComplete()
                        
                    }else{
                        if self.lat == nil{
                                            
                                            self.lat = self.arrayAboutUsOrgData[0].latitude
                                        }
                                        if self.long == nil{
                                            
                                            self.long = self.arrayAboutUsOrgData[0].longitude
                                        }
                                        if self.dic_RegisterValue["address"] as? String == nil{
                                            
                                            self.dic_RegisterValue.updateValue(self.arrayAboutUsOrgData[0].address, forKey: "address")
                                                
                                               
                                        }
                        self.EditActivitesAboutUsApi(id: self.arrayAboutUsOrgData[0].id ?? 0, description: cell.txtDesc_View.text ?? "", president: cell.txtField_President.text ?? "", gs: cell.txtField_GeneralSecetry.text ?? "", imam: cell.txtField_Imam.text ?? "", latitude: self.lat, longitude: self.long, address: self.dic_RegisterValue["address"] as! String, org_email: cell.txtField_EmailID.text ?? "", org_contact:   self.fullPhoneNumber, pre_up_iq: self.arrayAboutUsOrgData[0].pre_up_iq!)
                        self.aboutus_Addres = "false"
                        cell.btn_AddressEdit.isSelected = false
                    }
                }
                if self.aboutus_Addres == "true"{
                    self.aboutus_Addres = "false"
                    self.EditActivitesAboutUsApi(id: self.arrayAboutUsOrgData[0].id ?? 0, description: cell.txtDesc_View.text ?? "", president: cell.txtField_President.text ?? "", gs: cell.txtField_GeneralSecetry.text ?? "", imam: cell.txtField_Imam.text ?? "", latitude: self.lat, longitude: self.long, address: self.dic_RegisterValue["address"] as! String, org_email: cell.txtField_EmailID.text ?? "", org_contact:   self.fullPhoneNumber, pre_up_iq: self.arrayAboutUsOrgData[0].pre_up_iq!)
                    
                    
                }
                //
                cell.buttonCountryCodePressed = {
                    self.showCountryPickerView()
                }
                if cell.btn_PresidentEdit.isSelected{
                    
                    cell.txtField_President.isUserInteractionEnabled = true
                    cell.txtField_Imam.isUserInteractionEnabled = true
                    cell.txtField_GeneralSecetry.isUserInteractionEnabled = true
                    
                }else{
                    
                    cell.txtField_President.isUserInteractionEnabled = false
                    cell.txtField_Imam.isUserInteractionEnabled = false
                    cell.txtField_GeneralSecetry.isUserInteractionEnabled = false
                }
                cell.buttonPresidentPressed = {
                    if cell.btn_PresidentEdit.isSelected == false{
                        cell.btn_PresidentEdit.isSelected = true
                        cell.txtField_GeneralSecetry.becomeFirstResponder()
                        //  self.commiteePicker(textField: cell.txtField_Committee)
                    }else{
                        
                        
                        
                        self.presidentTxt = cell.txtField_President.text ?? ""
                        self.gsTxt = cell.txtField_GeneralSecetry.text ?? ""
                        self.imamTxt = cell.txtField_Imam.text ?? ""
                        
                        self.EditActivitesAboutUsApi(id: self.arrayAboutUsOrgData[0].id ?? 0, description: cell.txtDesc_View.text ?? "", president: self.presidentTxt, gs: self.gsTxt, imam: self.imamTxt, latitude: self.arrayAboutUsOrgData[0].latitude!, longitude: self.arrayAboutUsOrgData[0].longitude!, address: cell.btn_Location.text ?? "", org_email: cell.txtField_EmailID.text ?? "", org_contact: self.fullPhoneNumber, pre_up_iq: self.arrayAboutUsOrgData[0].pre_up_iq!)
                        
                        cell.btn_PresidentEdit.isSelected = false
                    }
                }
                cell.buttonSmsPressed = {
                    if cell.txtField_EmailID.isUserInteractionEnabled == true{
                        self.sendText(phoneNumber: cell.txtField_phoneNumber.text ?? "")
                    }
                }
                return cell
            }else{
                let cell = tableView.dequeueReusableCell(withIdentifier: "IqamahTableViewCell", for: indexPath) as! IqamahTableViewCell
                
                cell.btn_No.tag = 1
                cell.btn_Yes.tag = 0
                
                
                if self.arrayAboutUsOrgData[0].pre_up_iq == false{
                    cell.btn_No.backgroundColor = UIColor.init(hex: 0x0DC9E4)
                    cell.btn_Yes.backgroundColor = UIColor.white
                    cell.btn_No.setTitleColor(UIColor.white, for: .normal)
                    cell.btn_Yes.setTitleColor(UIColor.darkGray, for: .normal)
                    UserDefaults.standard.setValue(false, forKey: "PreUploaded")
                }else{
                    
                    cell.btn_Yes.backgroundColor = UIColor.init(hex: 0x0DC9E4)
                    cell.btn_No.backgroundColor = UIColor.white
                    cell.btn_Yes.setTitleColor(UIColor.white, for: .normal)
                    cell.btn_No.setTitleColor(UIColor.darkGray, for: .normal)
                    UserDefaults.standard.setValue(true, forKey: "PreUploaded")
                }
                cell.btn_No.addTarget(self, action: #selector(aboutEditbuttonYesAction(sender:)), for: .touchUpInside)
                cell.btn_Yes.addTarget(self, action: #selector(aboutEditbuttonYesAction(sender:)), for: .touchUpInside)
                
                
                return cell
                
            }
            
            
        default:
            return UITableViewCell()
        }
        
        
        
    }
    
    
    @objc func buttonAddressAction(_ sender: UIButton) {
        
        
    }
    
    
    @objc func aboutEditbuttonYesAction(sender: UIButton) {
        
        
        if sender.tag == 0{
            
            self.EditActivitesAboutUsApi(id: self.arrayAboutUsOrgData[0].id ?? 0, description: self.arrayAboutUsOrgData[0].description ?? "", president: self.presidentTxt, gs: self.gsTxt, imam: self.imamTxt, latitude: self.arrayAboutUsOrgData[0].latitude!, longitude: self.arrayAboutUsOrgData[0].longitude!, address: self.arrayAboutUsOrgData[0].address ?? "", org_email: self.arrayAboutUsOrgData[0].org_email ?? "", org_contact:   self.fullPhoneNumber, pre_up_iq: true)
            UserDefaults.standard.setValue(true, forKey: "PreUploaded")
            
        }else if sender.tag == 1{
            
            UserDefaults.standard.setValue(false, forKey: "PreUploaded")
            self.EditActivitesAboutUsApi(id: self.arrayAboutUsOrgData[0].id ?? 0, description: self.arrayAboutUsOrgData[0].description ?? "", president: self.presidentTxt, gs: self.gsTxt, imam: self.imamTxt, latitude: self.arrayAboutUsOrgData[0].latitude!, longitude: self.arrayAboutUsOrgData[0].longitude!, address: self.arrayAboutUsOrgData[0].address ?? "", org_email: self.arrayAboutUsOrgData[0].org_email ?? "", org_contact:   self.fullPhoneNumber, pre_up_iq: false)
            
        }
        self.callAboutUsApi(orgId: self.orgId!)
        
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if indexPath.section == 0 && indexPath.row == 0{
            
            
            if self.iqamaListNewsFeed?.iqama != nil || self.iqamaListNewsFeed?.jumma != nil{
                
                if self.iqamaListNewsFeed?.iqama?.count != 0 || self.iqamaListNewsFeed?.jumma?.count != 0 {
                    return UITableView.automaticDimension
                }else{
                    return 0
                }
            }
            
        }else if indexPath.section == 3 && indexPath.row == 0{
            
            if (self.iqamaListsData?.data?.iqama?.count == 0) && (self.iqamaListsData?.data?.jumma?.count == 0){
                
                return 0
            }else{
                return UITableView.automaticDimension
            }
        }
        return UITableView.automaticDimension
    }
    
    //
    //MARK:Send Sms Method-----------------------
    func sendText(phoneNumber: String) {
        if (MFMessageComposeViewController.canSendText()) {
            let controller = MFMessageComposeViewController()
            controller.body = ""
            controller.recipients = [phoneNumber]
            controller.messageComposeDelegate = self
            self.present(controller, animated: true, completion: nil)
        }
    }
    //MARK:MessageUI Delegate And DataSources-----------------------
    func messageComposeViewController(_ controller: MFMessageComposeViewController, didFinishWith result: MessageComposeResult) {
        //... handle sms screen actions
        self.isMessageCancel = false
        self.dismiss(animated: true, completion: nil)
    }
    //MARK:Set Reminder method-----------------------
    func setReminder(_ sender: Any) {
        let reminder = EKReminder(eventStore: self.eventStore)
        reminder.title = "Event Scheduled"
        reminder.calendar = eventStore.defaultCalendarForNewReminders()
        
        do {
            try eventStore.save(reminder,
                                commit: true)
        } catch let error {
            // print("Reminder failed with error \(error.localizedDescription)")
        }
        
    }
}

extension ActivitiesViewController: GMSAutocompleteViewControllerDelegate {
    
    // Handle the user's selection.
    func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {
        
        // print("Place name: \(String(describing: place.name))")
        let index = IndexPath(row: self.tag_forAddress, section: 5)
        let cell: OrgsAboutUsTableCell = self.tableView_Home.cellForRow(at: index) as! OrgsAboutUsTableCell
        let location:CLLocationCoordinate2D = place.coordinate
        let latitude_str = String(format: "%.6f", location.latitude)
        let longituted_str = String(format: "%.6f", location.longitude)
        self.lat = Double(latitude_str) ?? 123.123
        self.long = Double(longituted_str) ?? 123.123
        // print("Place address \(String(describing: place.formattedAddress))")
        let placeName:String = place.formattedAddress ?? ""
        self.dic_RegisterValue.updateValue(placeName, forKey: "address")
        
        
        let linkAttributes: [NSAttributedString.Key : Any] = [
            NSAttributedString.Key(rawValue: NSAttributedString.Key.foregroundColor.rawValue): UIColor.gray,
            NSAttributedString.Key(rawValue: NSAttributedString.Key.underlineColor.rawValue): UIColor.lightGray,
            NSAttributedString.Key(rawValue: NSAttributedString.Key.underlineStyle.rawValue): NSUnderlineStyle.single.rawValue
        ]
        let attributeString = NSMutableAttributedString(string: placeName ?? "", attributes: linkAttributes)
        
        
        
        cell.btn_Location.attributedText = attributeString
        tableView_Home.reloadRows(at: [index], with: .none)
        //  print("Place address:",place.formattedAddress!)
        dismiss(animated: true, completion: nil)
    }
    
    func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
        
        // TODO: handle the error.
        // print("Error: ", error.localizedDescription)
    }
    
    // User canceled the operation.
    func wasCancelled(_ viewController: GMSAutocompleteViewController) {
        //  addressTextView.resignFirstResponder()
        dismiss(animated: true, completion: nil)
    }
    
    // Turn the network activity indicator on and off again.
    func didRequestAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
    }
    
    func didUpdateAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
    }
}

extension ActivitiesViewController:UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        if collectionView == self.collectionView_Services{
            return 4
        }else{
            return array_Activities.count
        }
        
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionView == self.collectionView_Services{
            
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "OrgDetails_ServicesCollectionViewCell", for: indexPath) as! OrgDetails_ServicesCollectionViewCell
            return cell
            
        }else{
            
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ActivitesNewsFeed", for: indexPath) as! Orgs_ActivitesNewsFeedCollectionViewCell
            cell.lbl_Title.text = array_Activities[indexPath.item].localized
            if selectedIndexActivites == indexPath.row{
                cell.lbl_Title.backgroundColor = UIColor.init(hex: 0x00A783)
                cell.lbl_Title.layer.borderColor = UIColor.yellow.cgColor
                cell.lbl_Title.layer.borderWidth = 1.0
                cell.lbl_Title.clipsToBounds = true
                cell.lbl_Title.textColor = UIColor.white
                
            }else{
                cell.lbl_Title.backgroundColor = UIColor.white
                cell.lbl_Title.layer.borderColor = UIColor.white.cgColor
                cell.lbl_Title.layer.borderWidth = 1.0
                cell.lbl_Title.textColor = UIColor.black
                cell.lbl_Title.clipsToBounds = true
            }
            return cell
            
        }
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        self.selectedIndexActivites = indexPath.item
        if self.selectedIndexActivites == 0{
            self.searchBar.isHidden = true
            btn_searchBar.isHidden = false
            
            self.HomeListApi(keyAll: "")
            
            
        }else if self.selectedIndexActivites == 1{
            btn_searchBar.isHidden = true
            self.searchBar.isHidden = true
            self.callEventListApi(orgId: self.orgId!)
        }else if self.selectedIndexActivites == 2{
            self.searchBar.isHidden = true
            btn_searchBar.isHidden = true
            self.getClassesDataApi()
        }else if self.selectedIndexActivites == 3{
            self.searchBar.isHidden = true
            btn_searchBar.isHidden = true
            setCurrentLanguage(language: "")
            if self.orgDetails.orgType != "mosque"{
                self.showSingleAlertMessage(message: "Your organisation is not mosque type", subMsg: "", sender: self, completion:
                    { (success) -> Void in
                        
                })
            }else{
                self.iqamaListsApi()
            }
            
            
        }else if self.selectedIndexActivites == 4{
            self.searchBar.isHidden = true
            btn_searchBar.isHidden = true
            self.callListOfActivitiesInOrganization(orgId: self.orgId!)
        }else if self.selectedIndexActivites == 5{
            self.searchBar.isHidden = true
            btn_searchBar.isHidden = true
            self.callAboutUsApi(orgId: self.orgId!)
        }
        collectionView_Org.reloadData()
        //tableView_Home.reloadData()
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        
        return UIEdgeInsets(top: 0, left: 8, bottom: 0, right: 8)
        
    }
    
}
extension ActivitiesViewController: MHDescDelegate{
    func getDescription(desc: String) {
        self.dic_RegisterValue["Description"] = desc
        self.isMessageCancel = false
        self.arrayAboutUsOrgData[0].description = desc
        self.EditActivitesAboutUsApi(id: self.arrayAboutUsOrgData[0].id ?? 0, description: self.arrayAboutUsOrgData[0].description  ?? "", president: self.arrayAboutUsOrgData[0].president ?? "" , gs: self.arrayAboutUsOrgData[0].gs ??  "", imam: self.arrayAboutUsOrgData[0].imam ?? "", latitude: self.arrayAboutUsOrgData[0].latitude!, longitude: self.arrayAboutUsOrgData[0].longitude!, address: self.arrayAboutUsOrgData[0].address  ?? "", org_email: self.arrayAboutUsOrgData[0].org_email ?? "", org_contact: self.fullPhoneNumber ?? "",pre_up_iq: self.arrayAboutUsOrgData[0].pre_up_iq!)
        self.tableView_Home.reloadRows(at: [IndexPath(row: 0, section: 5)], with: UITableView.RowAnimation.none)
        
    }
}



extension ActivitiesViewController: AddActivityDelegate{
    func addActivityViewDissmissed() {
        statusBarColorChange(color:UIColor.init(hex: 0x0DC9E4))
        if self.selectedIndexActivites != nil{
            if self.selectedIndexActivites == 0{
                btn_searchBar.isHidden = false
                self.HomeListApi(keyAll: "")
            }else if self.selectedIndexActivites == 1{
                btn_searchBar.isHidden = true
                self.callEventListApi(orgId: self.orgId!)
            }else if self.selectedIndexActivites == 2{
                btn_searchBar.isHidden = true
                self.getClassesDataApi()
            }else if self.selectedIndexActivites == 3{
                btn_searchBar.isHidden = true
                self.iqamaListsApi()
                
            }else if self.selectedIndexActivites == 4{
                btn_searchBar.isHidden = true
                self.callListOfActivitiesInOrganization(orgId: self.orgId!)
            }else if self.selectedIndexActivites == 5{
                btn_searchBar.isHidden = true
                self.callAboutUsApi(orgId: self.orgId!)
            }
            
            collectionView_Org.reloadData()
        }
    }
    
    func stringToDate(dateString:String){
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd-MM-yyyy HH:mm"
        dateFormatter.locale = Locale(identifier: "en_US_POSIX") // set locale to reliable US_POSIX
        self.ReminderDate = dateFormatter.date(from:dateString)!
    }
}


