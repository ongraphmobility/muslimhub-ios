//
//  OrgActivitiesWebservice.swift
//  Muslim Hub
//
//  Created by Himanshu Goyal on 31/05/19.
//  Copyright © 2019 OnGraph. All rights reserved.
//

import Foundation
import UIKit
import DropDown
import KVNProgress
import Alamofire
import TTTAttributedLabel

extension ActivitiesViewController {
    func callListOfActivitiesInOrganization(orgId: Int){
        KVNProgress.show()
        Network.shared.getListOfActivitiesInOrganization(orgId: orgId) { (response) in
            KVNProgress.dismiss()
            guard let arrayEvent = response?.data else {
                return
            }
            //  print(arrayEvent)
            self.arrayActivityModel = arrayEvent
            self.tableView_Home.delegate = self
            self.tableView_Home.dataSource = self
            if self.arrayActivityModel.count > 0{
                for i in 0...self.arrayActivityModel.count - 1{
                    var arraySelectedImage: [Bool] = []
                    if self.arrayActivityModel[i].images!.count > 0{
                        for j in 0...self.arrayActivityModel[i].images!.count - 1{
                            if j == 0{
                                arraySelectedImage.append(true)
                            }else{
                                arraySelectedImage.append(false)
                            }
                        }
                    }
                    self.arrayActivityModel[i].selectedImage_Array = arraySelectedImage
                    self.arrayActivityModel[i].isMoreTapped = false
                }
                self.view_noDatafound.isHidden = true
            }else{
                
                self.view_noDatafound.isHidden = false
            }
            KVNProgress.dismiss()
            
            if self.brachio == 1{
                let indexxx =  self.arrayActivityModel.index(where: {$0.id == self.brachTypeId})
                
                self.tableView_Home.reloadData()
                if indexxx != nil{
                    if indexxx != 0{
                        let index = NSIndexPath(row: indexxx ?? 0, section: 4)
                        self.tableView_Home.scrollToRow(at: index as IndexPath, at: .top, animated: true)
                    }
                }
            }else{
                self.tableView_Home.reloadData()
            }
            
        }
    }
    
    func callDeleteActivityListItem(activityId: String){
        KVNProgress.show()
        Network.shared.deleteOrgActivityListItem(id: activityId) { (response) in
            KVNProgress.dismiss()
            AKAlertController.alert("Success", message: response!.message, buttons: ["Ok"], tapBlock: { (_, _, index) in
                self.callListOfActivitiesInOrganization(orgId: self.orgId!)
            })
        }
    }
    
    
    //MARK:Function Api----------------------------------------------
    
    @objc func reportIqamaApi(sender:UIButton){
        
        
        AKAlertController.alert("", message: "Are the Iqamah times wrong in MuslimHub? Report it to the Admins of this Organisation?", buttons: ["Yes","No"], tapBlock: { (_, _, index) in
            if index == 1 {return}
            KVNProgress.show()
            var id = Int()
            if  self.tableViewSection == 0{
                
                id = self.iqamaListNewsFeed.iqama?[0].id ?? 0
            }else{
                id = self.iqamaListsData.data?.iqama?[0].id ?? 0
            }
            Network.shared.addReport(id: id) { (result) in
                KVNProgress.dismiss()
                guard let user = result else{
                    
                    return
                }
                if user.success == true{
                    self.showSingleAlertMessage(message: "", subMsg: user.message , sender: self, completion:
                        { (success) -> Void in
                            
                    })
                }
                
                
            }
        })
        
        
        
        
        
    }
    
    func iqamaListsApi(isCalledFromAdd: Bool? = false){
        
        if brachio == 1{
            KVNProgress.show()
            
            Network.shared.IQAMAListsApi(id: self.orgId ?? 0) { (result) in
                KVNProgress.dismiss()
                guard result != nil else{
                    
                    return
                }
                self.iqamaListsData = result
                
                if self.iqamaListsData.data == nil{
                    
                    self.view_noDatafound.isHidden = true
                }else{
                    self.view_noDatafound.isHidden = false
                    self.tableView_Home.dataSource = self
                    self.tableView_Home.delegate = self
                    self.tableView_Home.reloadData()
                    
                    
                }
                
                if self.iqamaListsData != nil && isCalledFromAdd == true{
                    let vc = self.storyboard?.instantiateViewController(withIdentifier: "Alert_AddActivitesViewController") as! Alert_AddActivitesViewController
                    vc.delegate = self
                    vc.modalPresentationStyle = .overCurrentContext
                    vc.orDetails = self.orgDetails
                    vc.orgId = self.orgDetails.id ?? 0
                    
                    if self.brachio == 1{
                        vc.orgType = self.orgDetails.orgType ?? ""
                    }else{
                        vc.orgType = self.org_type
                    }
                    
                    vc.iqamaData = self.iqamaListNewsFeed.iqama
                    vc.enableDonation = self.orgDetails.donationAllowed
                    
                    self.present(vc, animated: true, completion: nil)
                }
            }
            
        }else{
            KVNProgress.show()
            Network.shared.IQAMAListsApi(id: self.orgId ?? 0) { (result) in
                KVNProgress.dismiss()
                guard result != nil else{
                    
                    return
                }
                self.iqamaListsData = result
                
                if self.iqamaListsData.data == nil{
                    
                    self.view_noDatafound.isHidden = true
                }else{
                    self.view_noDatafound.isHidden = false
                    self.tableView_Home.dataSource = self
                    self.tableView_Home.delegate = self
                    self.tableView_Home.reloadData()
                    
                    
                }
                
                if self.iqamaListsData != nil && isCalledFromAdd == true{
                    let vc = self.storyboard?.instantiateViewController(withIdentifier: "Alert_AddActivitesViewController") as! Alert_AddActivitesViewController
                    vc.delegate = self
                    vc.modalPresentationStyle = .overCurrentContext
                    vc.orDetails = self.orgDetails
                    vc.orgId = self.orgDetails.id ?? 0
                    
                    if self.brachio == 1{
                        vc.orgType = self.orgDetails.orgType ?? ""
                    }else{
                        vc.orgType = self.org_type
                    }
                    
                    vc.iqamaData = self.iqamaListNewsFeed.iqama
                    vc.enableDonation = self.orgDetails.donationAllowed
                    
                    self.present(vc, animated: true, completion: nil)
                }
                
            }
            
        }
        
        
        
    }
    
    
    func AdminEditApi(){
        
        KVNProgress.show()
        
        Network.shared.AdminEdit(id: orgId ?? 0,name: txt_Name.text ?? "",abbreviation: txt_Abbreviation.text ?? "") { (result) in
            if KVNProgress.isVisible(){
                KVNProgress.dismiss()
            }
            
            
            guard let user = result else {
                return
            }
            
            if user.success == true{
                
            }
            
        }
        
    }
    
    func favUnFavApi(){
        
        Network.shared.favUnFav(org_id: self.orgDetails.id ?? 0, is_fav: isfavbtnSelectedt) { (result) in
            
            guard result != nil else {
                
                return
            }
            self.dispatchGroup.enter()
            self.orgDetailsApi(isCallFrom: "")
            
        }
    }
    //NEWSFeed Api----------------------------------
    func HomeListApi(keyAll:String){
        
        KVNProgress.show()
        
        Network.shared.getListOfNewFeed(orgId: orgId ?? 0) { (homeResult,iqamaResult) in
            
            KVNProgress.dismiss()
            guard let user = homeResult else {
                return
            }
            guard let iqamaUserNews = iqamaResult else {
                return
            }
            
            self.homeList = user
            self.iqamaListNewsFeed = iqamaUserNews
            
            DispatchQueue.main.async {
                self.tableView_Home.delegate = self
                self.tableView_Home.dataSource = self
                
                if self.homeList.count > 0{
                    for i in 0...self.homeList.count - 1{
                        var arraySelectedImage: [Bool] = []
                        if self.homeList[i].images.count > 0{
                            for j in 0...self.homeList[i].images.count - 1{
                                if j == 0{
                                    arraySelectedImage.append(true)
                                }else{
                                    arraySelectedImage.append(false)
                                }
                            }
                        }
                        self.homeList[i].selectedImage_Array = arraySelectedImage
                    }
                    self.view_noDatafound.isHidden = true
                    
                }else{
                    
                    self.view_noDatafound.isHidden = false
                }
                
                if self.brachio == 1{
                    let indexxx = self.homeList.index(where: {$0.id == self.brachTypeId})
                    self.tableView_Home.reloadData()
                    
                    if indexxx != nil{
                        if indexxx != 0{
                            let index = NSIndexPath(row: indexxx ?? 0, section: 0)
                            self.tableView_Home.scrollToRow(at: index as IndexPath, at: .top, animated: true)
                        }}
                }else{
                    self.tableView_Home.reloadData()
                }
                
            }
        }
    }
    //MARK:Function get Classes Data Api----------------------------------------------
    func getClassesDataApi(){
        KVNProgress.show()
        
        Network.shared.getClassesDataLists(orgId:orgId ?? 0){ (result) in
            KVNProgress.dismiss()
            guard let data = result else {return}
            self.classesModelData = data
            
            
            
            self.tableView_Home.delegate = self
            self.tableView_Home.dataSource = self
            if self.classesModelData.getClassData?.count ?? 0 > 0{
                for i in 0...self.classesModelData.getClassData!.count - 1{
                    var arraySelectedImage: [Bool] = []
                    if self.classesModelData.getClassData![i].images!.count > 0{
                        for j in 0...self.classesModelData.getClassData![i].images!.count - 1{
                            if j == 0{
                                arraySelectedImage.append(true)
                            }else{
                                arraySelectedImage.append(false)
                            }
                        }
                    }
                    self.classesModelData.getClassData![i].selectedImage_Array = arraySelectedImage
                    self.classesModelData.getClassData![i].isMoreTapped = false
                }
            }
            
            DispatchQueue.main.async {
                if self.brachio == 1 {
                    
                    let indexxx = self.classesModelData.getClassData?.index(where: {$0.id == self.brachTypeId})
                    
                    self.tableView_Home.reloadData()
                    if indexxx != nil{
                        
                        let index = IndexPath(row: indexxx ?? 0, section: self.selectedIndexActivites)
                        self.tableView_Home.scrollToRow(at: index, at: .top, animated: true)
                        
                        
                    }
                    
                }else{
                    self.tableView_Home.reloadData()
                }
            }
            
            
            
        }
        
    }
    
    func trimString(str: String) -> String{
        return str.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
    }
    
    
    func serachApi(){
        KVNProgress.show()
        Network.shared.getListOfSearch_NewsFeed_Organization(searchKey: trimString(str: self.searchBar.text ?? ""), isfav: "true", orgId: self.orgId) { (result) in
            KVNProgress.dismiss()
            guard let user = result else {
                
                return
            }
            
            self.homeList = user
            if self.homeList.count > 0 {
                for i in 0...self.homeList.count - 1 {
                    var arraySelectedImage: [Bool] = []
                    if self.homeList[i].images.count > 0{
                        for j in 0...self.homeList[i].images.count - 1{
                            if j == 0{
                                arraySelectedImage.append(true)
                            }else{
                                arraySelectedImage.append(false)
                            }
                        }
                    }
                    self.homeList[i].selectedImage_Array = arraySelectedImage
                    
                }
                
                
            }
            self.tableView_Home.reloadData()
        }
        
    }
    
    func orgDetailsApi(isCallFrom:String){
        
        //        if self.brachio == 1{
        //             dispatchGroup.enter()
        //        }
        //
        //        self.dispatchGroup.enter()
        KVNProgress.show()
        Network.shared.organizationDetails(id:orgId ?? 0) { (result) in
            KVNProgress.dismiss()
            guard let user = result else {
                return
            }
            self.orgDetails = user
            
            self.txt_Name.text = (self.orgDetails.name!)
            self.txt_Abbreviation.text = self.orgDetails.abbreviation ?? ""
            var favCount: String!
            favCount = "\(String(describing: self.orgDetails.favMemberCount ?? 0))" + " followers".localized
            self.btn_FollowUs.setTitle(favCount, for: .normal)
            
            self.FirstPermission()
            if self.isEditModeOnOFF == true{
                self.Permission()
            }
            
            self.img_profile.contentMode = .scaleToFill
            if self.orgDetails.logo != nil && self.orgDetails.logo != ""{
                self.img_profile.sd_setImage(with: URL(string:
                    self.orgDetails.logo!),placeholderImage: UIImage.init(named: "default_icon_iPhone_Logo.png"), completed: nil)
                self.btn_profile.setBackgroundImage(#imageLiteral(resourceName: "upload_edit_iPhone"), for: .normal)
                // self.img_profile.image = #imageLiteral(resourceName: "upload_edit_iPhone")
            }else{
                //self.btn_profile.setBackgroundImage(#imageLiteral(resourceName: "upload_default_iPhone"), for: .normal)
                self.img_profile.image = #imageLiteral(resourceName: "upload_default_iPhone")
            }
            
            if self.orgDetails.image != nil && self.orgDetails.image != ""{
                self.img_banner.sd_setImage(with: URL(string:
                    self.orgDetails.image!),placeholderImage: UIImage.init(named: "default_header_iPhone.png"), completed: nil)
                self.btn_banner.setTitle("", for: .normal)
                self.btn_banner.setImage(#imageLiteral(resourceName: "edit_profile_iPhone"), for: .normal)
            }else{
                self.btn_banner.setTitle("", for: .normal)
                self.btn_banner.setImage(#imageLiteral(resourceName: "upload_icon_admin_iPhone"), for: .normal)
            }
            
            
            //mark:check for services shown----
            if self.orgDetails.services?.count != 0{
                self.btn_service1.isHidden = true
                self.btn_service2.isHidden = true
                self.btn_service3.isHidden = true
                self.btn_service4.isHidden = true
                self.btn_service5.isHidden = true
                self.btn_service6.isHidden = true
                self.btn_service7.isHidden = true
                self.stackView_Services.isHidden = false
                self.btn_Add_EditServices.isHidden = true
                
                if self.orgDetails.services!.count >= 3{
                    
                    self.btn_service1.isHidden = true
                    self.btn_service2.isHidden = true
                    self.btn_service3.isHidden = true
                    self.btn_service4.isHidden = true
                    self.btn_service5.isHidden = true
                    self.btn_service6.isHidden = true
                    self.btn_service7.isHidden = true
                    
                    
                    let servicesbtn = self.orgDetails.services?.prefix(through: 2)
                    // print(servicesbtn!)
                    
                    if (servicesbtn!.contains(1)){
                        self.btn_service1.isHidden = false
                        self.btn_service1.setBackgroundImage(#imageLiteral(resourceName: "sisters_prayer_iPhone"), for: .normal)
                    }
                    if (servicesbtn!.contains(2)){
                        self.btn_service2.isHidden = false
                        self.btn_service2.setBackgroundImage(#imageLiteral(resourceName: "marriage_celebrant_iPhone"), for: .normal)
                    }
                    if (servicesbtn!.contains(3)){
                        self.btn_service3.isHidden = false
                        self.btn_service3.setBackgroundImage(#imageLiteral(resourceName: "sporting_activities_iPhone"), for: .normal)
                    }
                    if (servicesbtn!.contains(4)){
                        self.btn_service4.isHidden = false
                        self.btn_service4.setBackgroundImage(#imageLiteral(resourceName: "muslim_program_iPhone"), for: .normal)
                    }
                    if (servicesbtn!.contains(5)){
                        self.btn_service5.isHidden = false
                        self.btn_service5.setBackgroundImage(#imageLiteral(resourceName: "onsite_parking_iPhone"), for: .normal)
                    }
                    if (servicesbtn!.contains(6)){
                        self.btn_service6.isHidden = false
                        self.btn_service6.setBackgroundImage(#imageLiteral(resourceName: "accept_donations_iPhone"), for: .normal)
                    }
                    if (servicesbtn!.contains(7)){
                        self.btn_service7.isHidden = false
                        self.btn_service7.setBackgroundImage(#imageLiteral(resourceName: "justice_peace_iPhone"), for: .normal)
                    }
                    
                    
                }else{
                    self.stackView_Services.isHidden = false
                    if (self.orgDetails.services!.contains(1)){
                        self.btn_service1.isHidden = false
                        self.btn_service1.setBackgroundImage(#imageLiteral(resourceName: "sisters_prayer_iPhone"), for: .normal)
                    }
                    if (self.orgDetails.services!.contains(2)){
                        self.btn_service2.isHidden = false
                        self.btn_service2.setBackgroundImage(#imageLiteral(resourceName: "marriage_celebrant_iPhone"), for: .normal)
                    }
                    if (self.orgDetails.services!.contains(3)){
                        self.btn_service3.isHidden = false
                        self.btn_service3.setBackgroundImage(#imageLiteral(resourceName: "sporting_activities_iPhone"), for: .normal)
                    }
                    if (self.orgDetails.services!.contains(4)){
                        self.btn_service4.isHidden = false
                        self.btn_service4.setBackgroundImage(#imageLiteral(resourceName: "muslim_program_iPhone"), for: .normal)
                    }
                    if (self.orgDetails.services!.contains(5)){
                        self.btn_service5.isHidden = false
                        self.btn_service5.setBackgroundImage(#imageLiteral(resourceName: "onsite_parking_iPhone"), for: .normal)
                    }
                    if (self.orgDetails.services!.contains(6)){
                        self.btn_service6.isHidden = false
                        self.btn_service6.setBackgroundImage(#imageLiteral(resourceName: "accept_donations_iPhone"), for: .normal)
                    }
                    if (self.orgDetails.services!.contains(7)){
                        self.btn_service7.isHidden = false
                        self.btn_service7.setBackgroundImage(#imageLiteral(resourceName: "justice_peace_iPhone"), for: .normal)
                    }
                    
                }
                
                self.btn_servicesMore.isHidden = false
            }else{
                self.btn_Add_EditServices.isHidden = false
                self.btn_servicesMore.isHidden = true
                
                //Check for Admin,super Admin,Normal User----------------------
                if (UserDefaults.standard.value(forKey: "Login_Token") as? String) != nil{
                    
                    if let superAdmin = UserDefaults.standard.value(forKey: "isSuperAdmin") as? Bool{
                        
                        if superAdmin == true{
                            
                            self.btn_Add_EditServices.setTitle("Add Services", for: .normal)
                            self.btn_Add_EditServices.setImage(#imageLiteral(resourceName: "plus_iPhone_white"), for: .normal)
                        }else{
                            
                            if let is_staff = UserDefaults.standard.value(forKey: "is_staff") as? Bool{
                                
                                if is_staff == true{
                                    //mark:check For own Organisation---------------------------
                                    if let orgOwn_Array = UserDefaults.standard.value(forKey: "OwnOrg_Array") as? [Int]{
                                        
                                        if orgOwn_Array.contains(self.orgId ?? 0){
                                            
                                            var org_P = [PriviligesData]()
                                            org_P = Global.shared.priviligesData
                                            let filertedData = org_P.filter{$0.org_id == self.orgId}
                                            filertedData.forEach {
                                                //   print("model data objects::",$0.org_perm as? String)
                                                
                                                if $0.org_perm == "iqama_only"{
                                                    //                                                    if self.remaingData == []{
                                                    //                                                        self.btn_Flagg.isHidden = true
                                                    //                                                        self.img_Flag.isHidden = true
                                                    //                                                    }else{
                                                    //                                                        self.btn_Flagg.isHidden = false
                                                    //                                                       self.img_Flag.isHidden = false
                                                    //                                                    }
                                                    self.btn_Add_EditServices.setTitle("No Services", for: .normal)
                                                    self.btn_Add_EditServices.setImage(nil, for: .normal)
                                                }else{
                                                    //                                                    if self.remaingData == []{
                                                    //                                                        self.btn_Flagg.isHidden = true
                                                    //                                                        self.img_Flag.isHidden = true
                                                    //                                                    }else{
                                                    //                                                        self.btn_Flagg.isHidden = false
                                                    //                                                        self.img_Flag.isHidden = false
                                                    //                                                    }
                                                    self.btn_Add_EditServices.setTitle("Add Services", for: .normal)
                                                    self.btn_Add_EditServices.setImage(#imageLiteral(resourceName: "plus_iPhone_white"), for: .normal)
                                                    
                                                    
                                                }
                                            }
                                            
                                            
                                            
                                        }else{
                                            //print("no staff")
                                            //   self.btn_Flagg.isHidden = true
                                            //   self.img_Flag.isHidden = true
                                            self.btn_Add_EditServices.setTitle("No Services", for: .normal)
                                            self.btn_Add_EditServices.setImage(nil, for: .normal)
                                        }//orgOwn
                                        
                                    }
                                }//staff true
                            }//staff
                            
                        }
                    }//SuperAdmin
                }//Login
                else{
                    // self.btn_Flagg.isHidden = true
                    //  self.img_Flag.isHidden = true
                    
                    self.btn_Add_EditServices.setTitle("No Services", for: .normal)
                    self.btn_Add_EditServices.setImage(nil, for: .normal)
                    
                }
                
                self.stackView_Services.isHidden = true
                self.btn_service1.isHidden = true
                self.btn_service2.isHidden = true
                self.btn_service3.isHidden = true
                self.btn_servicesMore.isHidden = true
            }
            
            self.btn_Heart.tintColor = UIColor.clear
            if self.orgDetails.is_fav == true{
                self.btn_Heart.setBackgroundImage(#imageLiteral(resourceName: "heart_selected_iPhone"), for: .normal)
                self.btn_Heart.isSelected = true
                self.isfavbtnSelectedt = "true"
                
            }else{
                self.btn_Heart.setBackgroundImage(#imageLiteral(resourceName: "heart_unselected_iPhone"), for: .normal)
                self.btn_Heart.isSelected = false
                self.isfavbtnSelectedt = "false"
            }
            
            
            if isCallFrom == ""{
                if self.selectedIndexActivites != nil{
                              if self.selectedIndexActivites == 0{
                                  self.HomeListApi(keyAll: "")
                                  
                              }else if self.selectedIndexActivites == 1{
                                  self.callEventListApi(orgId: self.orgId!)
                              }else if self.selectedIndexActivites == 2{
                                  self.getClassesDataApi()
                              }else if self.selectedIndexActivites == 3 {
                                  self.iqamaListsApi()
                              }else if self.selectedIndexActivites == 4{
                                  self.callListOfActivitiesInOrganization(orgId: self.orgId!)
                              }else if self.selectedIndexActivites == 5{
                                  self.callAboutUsApi(orgId: self.orgId!)
                              }
                              self.collectionView_Org.reloadData()
                          }
            }
     
        }
        
        
        
        
    }
    
    func deleteorgApi(){
        Network.shared.deletOrgApi(id:orgId ?? 0) { (result) in
            
            guard result != nil else {
                
                return
            }
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    func uploadImageApi(){
        
        let key:String!
        if btn_profile.tag == 0{
            key = "logo"
        }else{
            key = "image"
        }
        let para:[String:Any] = [ "id": orgDetails.id ?? 0,
                                  "name": orgDetails.name ?? ""
        ]
        Network.shared.requestWith(imageData: imageData,key: key ,parameters: para) { (result) in
            
            //  print(result as Any)
        }
       // self.callFlagApiList(isCallFrom:"imgUpload")
        
    }
}
extension ActivitiesViewController:TTTAttributedLabelDelegate{
    
    func phoneNumberDetctor(input:String){
                 
                 let detector = try! NSDataDetector(types: NSTextCheckingResult.CheckingType.phoneNumber.rawValue)
                           let matches = detector.matches(in: input, options: [], range: NSRange(location: 0, length: input.utf16.count))
                   
                           for match in matches {
                               guard let range = Range(match.range, in: input) else { continue }
                               let url = input[range]
                               print(url)
                            array_String.append(String(url))
                       
                         
                           
                           }
              
                 
             }
             func uRLDetctor(input:String){
                   
               
                   let detector = try! NSDataDetector(types: NSTextCheckingResult.CheckingType.link.rawValue)
                   let matches = detector.matches(in: input, options: [], range: NSRange(location: 0, length: input.utf16.count))

                   for match in matches {
                       guard let range = Range(match.range, in: input) else { continue }
                       let url = input[range]
                       print(url)
                 
                
                   
                      
                        
                   }
              
            
               }
    
    
    func phoneAndUrlDetctor(tap_string:String){
        
        //        let tap_string = array_String[view.tag]
        //
        //
                let detector = try! NSDataDetector(types: NSTextCheckingResult.CheckingType.phoneNumber.rawValue)
                          let matches = detector.matches(in: tap_string, options: [], range: NSRange(location: 0, length: tap_string.utf16.count))
        
                          for match in matches {
                              guard let range = Range(match.range, in: tap_string) else { continue }
                              let url = tap_string[range]
                              print(url)
        
                            self.phoneNumber = "true"
        
                          }
        
              let detectorUrl = try! NSDataDetector(types: NSTextCheckingResult.CheckingType.link.rawValue)
                          let matchesurl = detectorUrl.matches(in: tap_string, options: [], range: NSRange(location: 0, length: tap_string.utf16.count))
        
                          for match in matchesurl {
                              guard let range = Range(match.range, in: tap_string) else { continue }
                              let url = tap_string[range]
                              print(url)
        
                          self.phoneNumber = "false"
        
        
        
                          }
        
                if self.phoneNumber == "true"{
        
                    if let url = URL(string:"tel://\(tap_string )"), UIApplication.shared.canOpenURL(url) {
                                        UIApplication.shared.openURL(url)
                                   }
                }else if self.phoneNumber == "false"{
        
                    let urlString = ("\(tap_string )")
                                   var urll = String()
        
        
                                   if urlString.contains("https://") {
                                       urll = ("\(tap_string)")
                                   }else if urlString.contains("http://") {
                                       urll = ("\(tap_string)")
                                   }else{
                                       urll = ("http://\(tap_string)")
                                   }
        
        
                                   guard let url = URL(string: urll) else { return }
                                   UIApplication.shared.openURL(url)
                }
    }
    

        func attributedLabel(_ label: TTTAttributedLabel!, didSelectLinkWith url: URL!) {
         debugPrint("label>", label)
                
                debugPrint("addressComponents>", url)
            
            self.phoneAndUrlDetctor(tap_string: String(describing: url!) )
        }
        
    
}
