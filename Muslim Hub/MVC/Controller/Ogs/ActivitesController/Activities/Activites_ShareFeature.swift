//
//  Activites_ShareFeature.swift
//  Muslim Hub
//
//  Created by Ongraph on 23/09/19.
//  Copyright © 2019 OnGraph. All rights reserved.
//

import Foundation
import UIKit
import Branch

extension ActivitiesViewController{
    
    @objc func btn_shareTextButton(sender: UIButton) {
        
        if selectedIndexActivites == 0{
            
            var image = String()
            for (index, element) in self.homeList[sender.tag].images.enumerated() {
              //  print("Item \(index): \(element)")
                
                
                image = image + "\n\n\(element)"
                
            }
            
            let buo = BranchUniversalObject.init(canonicalIdentifier: "content/12345")
            buo.title = self.homeList[sender.tag].title
            buo.contentDescription = "My Content Description"
            //  buo.imageUrl = self.homeList[sender.tag].images[0]
            buo.publiclyIndex = true
            buo.locallyIndex = true
            buo.contentMetadata.customMetadata["data"] =  "\(self.homeList[sender.tag].org!)"
            
            let type = self.homeList[sender.tag].category
            if type == "event"{
                buo.contentMetadata.customMetadata["type"] = "Events"
                self.shareDateFormat(startDate:self.homeList[sender.tag].startDate!)
                          self.shareTimeFormat(startTime:self.homeList[sender.tag].startTime! , endTime: self.homeList[sender.tag].endTime!)
            }else if type == "class"{
                buo.contentMetadata.customMetadata["type"] = "Classes"
                self.shareDateFormat(startDate:self.homeList[sender.tag].startDate!)
                          self.shareTimeFormat(startTime:self.homeList[sender.tag].startTime! , endTime: self.homeList[sender.tag].endTime!)
            }else if type == "activity"{
                buo.contentMetadata.customMetadata["type"] = "Activities"
                self.shareDateFormat(startDate:self.homeList[sender.tag].startDate!)
                          self.shareTimeFormat(startTime:self.homeList[sender.tag].startTime! , endTime: self.homeList[sender.tag].endTime!)
            }else if type == "announcement"{
                buo.contentMetadata.customMetadata["type"] = "Announcements"
            }
            
          
         
            buo.contentMetadata.customMetadata["typeId"] = "\(self.homeList[sender.tag].id!)"
            let lp: BranchLinkProperties = BranchLinkProperties()
            
            
            var brachioUrl = String()
            
            buo.getShortUrl(with: lp) { (url, error) in
                print(url ?? "")
                brachioUrl = url ?? ""
                
                
                if image != ""{
                    let data =  "\n \n" + (self.homeList[sender.tag].address ?? "") + "\n \n" + (self.homeList[sender.tag].datumDescription)
                    let abbreviation = image + "\n \n" + self.homeList[sender.tag].title + "\n\n"
                        + self.homeList[sender.tag].org_abbr! + "\n \n" + "Take me to the" + self.homeList[sender.tag].category + "in MuslimHub \n\n" + brachioUrl + "\n \n" + self.shareDate + ", " + self.shareTime + data + "\n \n" + "Shared from MuslimHub. Download from Apple Store" + "\n\n" + "https://apps.apple.com/au/app/muslim-hub/id1014406101"
                    self.btn_ShareTapp(details: [abbreviation])
                }else{
                    let data = ", " + self.shareTime + "\n \n" + (self.homeList[sender.tag].address ?? "") + "\n \n" + (self.homeList[sender.tag].datumDescription)
                    let abbreviation = self.homeList[sender.tag].title + "\n\n"
                        + self.homeList[sender.tag].org_abbr! + "\n \n" + "Take me to the" + self.homeList[sender.tag].category + "in MuslimHub \n\n" + brachioUrl + "\n \n" + self.shareDate + data + "\n \n" + "Shared from MuslimHub. Download from Apple Store" + "\n\n" + "https://apps.apple.com/au/app/muslim-hub/id1014406101"
                    
                    self.btn_ShareTapp(details: [abbreviation])
                }
                
            }
        }else if selectedIndexActivites == 1{
            
            var image = String()
            for (index, element) in self.arrayEventModel[sender.tag].images!.enumerated() {
                print("Item \(index): \(element)")
                
                
                image = image + "\n\n\(element)"
                
            }
            
            let buo = BranchUniversalObject.init(canonicalIdentifier: "content/12345")
            buo.title = self.arrayEventModel[sender.tag].title
            buo.contentDescription = "My Content Description"
            //  buo.imageUrl = self.homeList[sender.tag].images[0]
            buo.publiclyIndex = true
            buo.locallyIndex = true
            buo.contentMetadata.customMetadata["data"] = "\(self.arrayEventModel[sender.tag].org!)"
            buo.contentMetadata.customMetadata["type"] = "Events"
            buo.contentMetadata.customMetadata["typeId"] = "\(self.arrayEventModel[sender.tag].id!)"
            let lp: BranchLinkProperties = BranchLinkProperties()
            
            self.shareDateFormat(startDate:self.arrayEventModel[sender.tag].start_date!)
            self.shareTimeFormat(startTime:self.arrayEventModel[sender.tag].start_time! , endTime: self.arrayEventModel[sender.tag].end_time!)
                    
       
            var brachioUrl = String()
            
            buo.getShortUrl(with: lp) { (url, error) in
                print(url ?? "")
                brachioUrl = url ?? ""
                
                
                if image != ""{
                    let data =  "\n \n" + (self.arrayEventModel[sender.tag].address ?? "") + "\n \n" + (self.arrayEventModel[sender.tag].description!)
                    let abbreviation = image + "\n \n" + self.arrayEventModel[sender.tag].title! + "\n\n" + "Take me to the event in MuslimHub \n\n" + brachioUrl + "\n \n" + self.shareDate + ", " + self.shareTime + data + "\n \n" + "Shared from MuslimHub. Download from Apple Store" + "\n\n" + "https://apps.apple.com/au/app/muslim-hub/id1014406101"
                    self.btn_ShareTapp(details: [abbreviation])
                }else{
                    let data = ", " + self.shareTime + "\n \n" + (self.arrayEventModel[sender.tag].address ?? "") + "\n \n" + (self.arrayEventModel[sender.tag].description!)
                    
                    let abbreviation = self.arrayEventModel[sender.tag].title! + "\n\n" + "Take me to the event in MuslimHub \n\n"  + brachioUrl + "\n \n" + self.shareDate + data + "\n \n" + "Shared from MuslimHub. Download from Apple Store" + "\n\n" + "https://apps.apple.com/au/app/muslim-hub/id1014406101"
                    
                    self.btn_ShareTapp(details: [abbreviation])
                }
                
            }
        }else if selectedIndexActivites == 2{
            
            var image = String()
            for (index, element) in (self.classesModelData?.getClassData?[sender.tag].images?.enumerated())! {
                print("Item \(index): \(element)")
                
                
                image = image + "\n\n\(element)"
                
            }
            
            let buo = BranchUniversalObject.init(canonicalIdentifier: "content/12345")
            buo.title = self.classesModelData?.getClassData?[sender.tag].title
            buo.contentDescription = "My Content Description"
            //  buo.imageUrl = self.homeList[sender.tag].images[0]
            buo.publiclyIndex = true
            buo.locallyIndex = true
            buo.contentMetadata.customMetadata["data"] = "\(self.classesModelData?.getClassData?[sender.tag].org! ?? 0)"
            buo.contentMetadata.customMetadata["type"] = "Classes"
            buo.contentMetadata.customMetadata["typeId"] = "\(self.classesModelData?.getClassData?[sender.tag].id! ?? 0)"
            let lp: BranchLinkProperties = BranchLinkProperties()
         
            self.shareDateFormat(startDate:self.classesModelData?.getClassData?[sender.tag].start_date ?? "")
            self.shareTimeFormat(startTime:(self.classesModelData?.getClassData?[sender.tag].start_time ?? "") , endTime: self.classesModelData?.getClassData?[sender.tag].end_time ?? "")
                            
            var brachioUrl = String()
            
            buo.getShortUrl(with: lp) { (url, error) in
                print(url ?? "")
                brachioUrl = url ?? ""
                
                
                if image != ""{
                    let desctription =  "\n \n" + (self.classesModelData?.getClassData?[sender.tag].description ?? "")
                let data =  "\n \n" + (self.classesModelData?.getClassData?[sender.tag].address ?? "") + desctription
                    let abbreviation = image + "\n \n" + (self.classesModelData?.getClassData?[sender.tag].title)! + "\n \n" + "Take me to the class in MuslimHub \n\n" + brachioUrl + "\n \n" + self.shareDate + ", " + self.shareTime + data + "\n \n" + "Shared from MuslimHub. Download from Apple Store" + "\n\n" + "https://apps.apple.com/au/app/muslim-hub/id1014406101"
                    self.btn_ShareTapp(details: [abbreviation])
                }else{
                      let desctription =  "\n \n" + (self.classesModelData?.getClassData?[sender.tag].description ?? "")
                    let data = ", " + self.shareTime + "\n \n" + (self.classesModelData?.getClassData?[sender.tag].address ?? "") + desctription
                    let abbreviation = (self.classesModelData?.getClassData?[sender.tag].title)! + "\n\n"
                        + "Take me to the class in MuslimHub \n\n"  + brachioUrl + "\n \n" + self.shareDate + data + "\n \n" + "Shared from MuslimHub. Download from Apple Store" + "\n\n" + "https://apps.apple.com/au/app/muslim-hub/id1014406101"
                    
                    self.btn_ShareTapp(details: [abbreviation])
                }
                
            }
        }else if selectedIndexActivites == 4{
            
            var image = String()
            for (index, element) in self.arrayActivityModel[sender.tag].images!.enumerated() {
                print("Item \(index): \(element)")
                
                
                image = image + "\n\n\(element)"
                
            }
            
            let buo = BranchUniversalObject.init(canonicalIdentifier: "content/12345")
            buo.title = self.arrayActivityModel[sender.tag].title
            buo.contentDescription = "My Content Description"
            //  buo.imageUrl = self.homeList[sender.tag].images[0]
            buo.publiclyIndex = true
            buo.locallyIndex = true
            buo.contentMetadata.customMetadata["data"] = "\(self.arrayActivityModel[sender.tag].org!)"
            buo.contentMetadata.customMetadata["type"] = "Activities"
            buo.contentMetadata.customMetadata["typeId"] = "\(self.arrayActivityModel[sender.tag].id!)"
            let lp: BranchLinkProperties = BranchLinkProperties()
            
          
            var brachioUrl = String()
            
            buo.getShortUrl(with: lp) { (url, error) in
                print(url ?? "")
                brachioUrl = url ?? ""
                
                
                if image != ""{
                    let data =  "\n \n" + (self.arrayActivityModel[sender.tag].address ?? "") + "\n \n" + (self.arrayActivityModel[sender.tag].description!)
                    let abbreviation = image + "\n \n" + self.arrayActivityModel[sender.tag].title! + "\n\n" + "Take me to the activites in MuslimHub \n\n" + brachioUrl + "\n \n" + self.shareDate + ", " + self.shareTime + data + "\n \n" + "Shared from MuslimHub. Download from Apple Store" + "\n\n" + "https://apps.apple.com/au/app/muslim-hub/id1014406101"
                    self.btn_ShareTapp(details: [abbreviation])
                }else{
                    let data = ", " + self.shareTime + "\n \n" + (self.arrayActivityModel[sender.tag].address ?? "") + "\n\n" + (self.arrayActivityModel[sender.tag].description ?? "")
                    
                    let abbreviation = self.arrayActivityModel[sender.tag].title! + "\n\n" + "Take me to the activites in MuslimHub \n\n"  + brachioUrl + "\n \n" + self.shareDate + data + "\n \n" + "Shared from MuslimHub. Download from Apple Store" + "\n\n" + "https://apps.apple.com/au/app/muslim-hub/id1014406101"
                    
                    self.btn_ShareTapp(details: [abbreviation])
                }
                
            }
        }
        
    }
    
    @objc func btn_shareIQama(sender: UIButton) {
        if selectedIndexActivites == 0{
            AKAlertController.alert("", message: "Share", buttons: ["All ","Jummah Only?","Cancel"], tapBlock: { (_, _, index) in
                
                if index == 2 {return}
                if index == 0{
                    self.share_NewsFeedAllIqmqJumma()
                }
                if index == 1{
                   self.share_NewsFeedJummaOnly()
                }
            })
        }else{
            AKAlertController.alert("", message: "Share", buttons: ["All ","Jummah Only?","Cancel"], tapBlock: { (_, _, index) in
                
                if index == 2 {return}
                if index == 0{
                    self.share_AllIqmqJumma()
                }
                if index == 1{
                    self.share_JummaOnly()
                }
            })
        }
      
        
        
    }
    
    func share_JummaOnly(){
        let jummaDataCount = (self.iqamaListsData.data?.jumma!.count)!
       var shareJummaList = String()
     var JummaSahreList = String()
        if jummaDataCount != 0{
            for i in 0...jummaDataCount - 1{
                
                let jummaType = (self.iqamaListsData.data?.jumma?[i].type ?? 1)
                var jummaTitle = String()
                if jummaType == 1{
                    jummaTitle = "JUMMA1"
                }else if jummaType == 2{
                    jummaTitle = "JUMMA2"
                }else if jummaType == 3{
                    jummaTitle = "JUMMA3"
                }else if jummaType == 4{
                    jummaTitle = "JUMMA4"
                }else if jummaType == 5{
                    jummaTitle = "JUMMA5"
                }else if jummaType == 6{
                    jummaTitle = "JUMMA6"
                }else if jummaType == 7{
                    jummaTitle = "JUMMA7"
                }else if jummaType == 8{
                    jummaTitle = "JUMMA8"
                }else if jummaType == 9{
                    jummaTitle = "JUMMA9"
                }else if jummaType == 10{
                    jummaTitle = "JUMMA10"
                }else{
                    jummaTitle = "JUMMA1"
                }
                let time = (self.iqamaListsData?.data?.jumma?[i].time)!
                let timedateFormatter = DateFormatter()
                timedateFormatter.dateFormat = "HH:mm"
                let starttime = timedateFormatter.date(from: time)
                let timedateFormatter1 = DateFormatter()
                timedateFormatter1.dateFormat = "hh:mm a"
                let startTime1 = timedateFormatter1.string(from: starttime!)
                
              
                let jummaDescription = (self.iqamaListsData.data?.jumma?[i].notes)
                let jummaAddress = (self.iqamaListsData.data?.jumma?[i].address ?? "")
                
                shareJummaList = shareJummaList + "\(jummaTitle) \n" + "\(startTime1)\n" + "\(jummaAddress)\n" + jummaDescription! + "\n \n********************** \n \n"
                
                
        }
            let Jumma = "Jumma Times in " + self.orgDetails.abbreviation! + "\n" + "\nAs of " + "\(AppDelegate.instance.currentDate) \n"
            
            JummaSahreList = Jumma + "\nJUMMA: \n\n\(shareJummaList)"
            
            let buo = BranchUniversalObject.init(canonicalIdentifier: "content/12345")
            buo.contentDescription = "Jumma Share"
            buo.publiclyIndex = true
            buo.locallyIndex = true
            buo.contentMetadata.customMetadata["data"] = "\(self.orgId!)"
            buo.contentMetadata.customMetadata["type"] = "News Feed"
            buo.contentMetadata.customMetadata["typeId"] = "0"
            let lp: BranchLinkProperties = BranchLinkProperties()
            
            
            var brachioUrl = String()
            
            buo.getShortUrl(with: lp) { (url, error) in
                print(url ?? "")
                brachioUrl = url ?? ""
                
                
                
                JummaSahreList = JummaSahreList + "Take me to the jumma in MuslimHub \n" + brachioUrl + "\n \n" + "Shared from MuslimHub. Download from Apple Store" + "\n" + "https://apps.apple.com/au/app/muslim-hub/id1014406101"
                 self.btn_ShareTapp(details: [JummaSahreList])
            }
           
            
        }
        
        
    }
    func share_NewsFeedJummaOnly(){
        let newsFeed_jummaDataCount = (self.iqamaListNewsFeed?.jumma?.count)! 
        var shareJummaList = String()
        var JummaSahreList = String()
        if newsFeed_jummaDataCount != 0{
            for i in 0...newsFeed_jummaDataCount - 1{
                
                let jummaType = (self.iqamaListNewsFeed?.jumma?[i].type ?? 1)
                var jummaTitle = String()
                if jummaType == 1{
                    jummaTitle = "JUMMA1"
                }else if jummaType == 2{
                    jummaTitle = "JUMMA2"
                }else if jummaType == 3{
                    jummaTitle = "JUMMA3"
                }else if jummaType == 4{
                    jummaTitle = "JUMMA4"
                }else if jummaType == 5{
                    jummaTitle = "JUMMA5"
                }else if jummaType == 6{
                    jummaTitle = "JUMMA6"
                }else if jummaType == 7{
                    jummaTitle = "JUMMA7"
                }else if jummaType == 8{
                    jummaTitle = "JUMMA8"
                }else if jummaType == 9{
                    jummaTitle = "JUMMA9"
                }else if jummaType == 10{
                    jummaTitle = "JUMMA10"
                }else{
                    jummaTitle = "JUMMA1"
                }
                let time = (self.iqamaListNewsFeed?.jumma?[i].time)!
                let timedateFormatter = DateFormatter()
                timedateFormatter.dateFormat = "HH:mm"
                let starttime = timedateFormatter.date(from: time)
                let timedateFormatter1 = DateFormatter()
                timedateFormatter1.dateFormat = "hh:mm a"
                let startTime1 = timedateFormatter1.string(from: starttime!)
                
                
                let jummaDescription = (self.iqamaListNewsFeed?.jumma?[i].notes)
                let jummaAddress = (self.iqamaListNewsFeed?.jumma?[i].address ?? "")
                
                shareJummaList = shareJummaList + "\(jummaTitle) \n" + "\(startTime1)\n" + "\(jummaAddress)\n" + jummaDescription! + "\n \n********************** \n \n"
                
                
            }
            let Jumma = "Jumma Times in " + self.orgDetails.abbreviation! + "\n" + "\nAs of " + "\(AppDelegate.instance.currentDate) \n"
            
            JummaSahreList = Jumma + "\nJUMMA: \n\n\(shareJummaList)"
            
            let buo = BranchUniversalObject.init(canonicalIdentifier: "content/12345")
            buo.contentDescription = "Jumma Share"
            buo.publiclyIndex = true
            buo.locallyIndex = true
            buo.contentMetadata.customMetadata["data"] = "\(self.orgId!)"
            buo.contentMetadata.customMetadata["type"] = "News Feed"
              buo.contentMetadata.customMetadata["typeId"] = "0"
            let lp: BranchLinkProperties = BranchLinkProperties()
            
            
            var brachioUrl = String()
            
            buo.getShortUrl(with: lp) { (url, error) in
                print(url ?? "")
                brachioUrl = url ?? ""
                
                
                
                JummaSahreList = JummaSahreList + "Take me to the jumma in MuslimHub \n" + brachioUrl + "\n \n" + "Shared from MuslimHub. Download from Apple Store" + "\n" + "https://apps.apple.com/au/app/muslim-hub/id1014406101"
                self.btn_ShareTapp(details: [JummaSahreList])
            }
            
            
        }
        
        
    }

    func share_AllIqmqJumma(){
        var shareIqamaData = String()
        var shareJummaList = String()
        let iqamaCount = self.iqamaListsData.data?.iqama?.count ?? 0
        for i in 0...iqamaCount - 1{
            
            for (index, element) in (self.iqamaListsData.data?.iqama?[i].prayerCode?.enumerated())! {
                print("Item \(index): \(element)")
                
                var fullname = String()
                if element == "F"{
                    fullname = "FAJR"
                }else if element == "Z"{
                    fullname = "ZUHR"
                } else if element == "A"{
                    fullname = "ASR"
                } else if element == "M"{
                    fullname = "MAGHRIB"
                }else if element == "I"{
                    fullname = "ISHA"
                }
                
                let time = (self.iqamaListsData?.data?.iqama?[i].time)!
                let timedateFormatter = DateFormatter()
                timedateFormatter.dateFormat = "HH:mm"
                let starttime = timedateFormatter.date(from: time)
                let timedateFormatter1 = DateFormatter()
                timedateFormatter1.dateFormat = "hh:mm a"
                let startTime1 = timedateFormatter1.string(from: starttime!)
                shareIqamaData = shareIqamaData + "\(fullname)" + " -  \(startTime1)" + "\n"
                
            }
        }
        
        let jummaDataCount = (self.iqamaListsData.data?.jumma!.count)!
        let buo = BranchUniversalObject.init(canonicalIdentifier: "content/12345")
        buo.contentDescription = "Iqamah & Jumma Share"
        buo.publiclyIndex = true
        buo.locallyIndex = true
        buo.contentMetadata.customMetadata["data"] = "\(self.orgId!)"
        buo.contentMetadata.customMetadata["type"] = "News Feed"
          buo.contentMetadata.customMetadata["typeId"] = "0"
        let lp: BranchLinkProperties = BranchLinkProperties()
        var brachioUrl = String()
        
        if jummaDataCount != 0{
            for i in 0...jummaDataCount - 1{
                
                let jummaType = (self.iqamaListsData.data?.jumma?[i].type ?? 1)
                var jummaTitle = String()
                if jummaType == 1{
                    jummaTitle = "JUMMA1"
                }else if jummaType == 2{
                    jummaTitle = "JUMMA2"
                }else if jummaType == 3{
                    jummaTitle = "JUMMA3"
                }else if jummaType == 4{
                    jummaTitle = "JUMMA4"
                }else if jummaType == 5{
                    jummaTitle = "JUMMA5"
                }else if jummaType == 6{
                    jummaTitle = "JUMMA6"
                }else if jummaType == 7{
                    jummaTitle = "JUMMA7"
                }else if jummaType == 8{
                    jummaTitle = "JUMMA8"
                }else if jummaType == 9{
                    jummaTitle = "JUMMA9"
                }else if jummaType == 10{
                    jummaTitle = "JUMMA10"
                }else{
                    jummaTitle = "JUMMA1"
                }
                let time = (self.iqamaListsData?.data?.jumma?[i].time)!
                let timedateFormatter = DateFormatter()
                timedateFormatter.dateFormat = "HH:mm"
                let starttime = timedateFormatter.date(from: time)
                let timedateFormatter1 = DateFormatter()
                timedateFormatter1.dateFormat = "hh:mm a"
                let startTime1 = timedateFormatter1.string(from: starttime!)
                
                
                let jummaDescription = (self.iqamaListsData.data?.jumma?[i].notes)
                let jummaAddress = (self.iqamaListsData.data?.jumma?[i].address ?? "")
                shareJummaList = shareJummaList + "\(jummaTitle) \n" + "\(startTime1)\n" + "\(jummaAddress)\n" + jummaDescription! + "\n \n********************** \n \n"
                
            }
            
         
     
            let iqamaTitle = "Iqamah Times in " + self.orgDetails.abbreviation! + "\n" + self.orgDetails.address! + "\n \nAs of " + "\(AppDelegate.instance.currentDate) \n \n"
            
            var iqamaSahreList = iqamaTitle + shareIqamaData + "\n \nJUMMA: \n\n\(shareJummaList)"
            
       
            
            buo.getShortUrl(with: lp) { (url, error) in
                print(url ?? "")
                brachioUrl = url ?? ""
                
                
                
                iqamaSahreList = iqamaSahreList + "Take me to the Iqamah in MuslimHub \n" + brachioUrl + "\n \n" + "Shared from MuslimHub. Download from Apple Store" + "\n" + "https://apps.apple.com/au/app/muslim-hub/id1014406101"
                
                  self.btn_ShareTapp(details: [iqamaSahreList])
            }
          
        }else{
            let iqamaTitle = "Iqamah Times in " + self.orgDetails.abbreviation! + "\n" + self.orgDetails.address! + "\n \nAs of " + "\(AppDelegate.instance.currentDate) \n \n"
            
            var iqamaSahreList = iqamaTitle + shareIqamaData
            
            buo.getShortUrl(with: lp) { (url, error) in
                print(url ?? "")
                brachioUrl = url ?? ""
                
                
                
                iqamaSahreList = iqamaSahreList + "Take me to the Iqamah in MuslimHub \n" + brachioUrl + "\n \n" + "Shared from MuslimHub. Download from Apple Store" + "\n" + "https://apps.apple.com/au/app/muslim-hub/id1014406101"
                    self.btn_ShareTapp(details: [iqamaSahreList])
                
            }
        
        }
        
        
        
    }
    
    func share_NewsFeedAllIqmqJumma(){
        var shareIqamaData = String()
        var shareJummaList = String()
        let iqamaCount = self.iqamaListNewsFeed?.iqama?.count ?? 0
        for i in 0...iqamaCount - 1{
            
            for (index, element) in (self.iqamaListNewsFeed?.iqama?[i].prayerCode?.enumerated())! {
                print("Item \(index): \(element)")
                
                var fullname = String()
                if element == "F"{
                    fullname = "FAJR"
                }else if element == "Z"{
                    fullname = "ZUHR"
                } else if element == "A"{
                    fullname = "ASR"
                } else if element == "M"{
                    fullname = "MAGHRIB"
                }else if element == "I"{
                    fullname = "ISHA"
                }
                
                let time = (self.iqamaListNewsFeed?.iqama?[i].time)!
                let timedateFormatter = DateFormatter()
                timedateFormatter.dateFormat = "HH:mm"
                let starttime = timedateFormatter.date(from: time)
                let timedateFormatter1 = DateFormatter()
                timedateFormatter1.dateFormat = "hh:mm a"
                let startTime1 = timedateFormatter1.string(from: starttime!)
                shareIqamaData = shareIqamaData + "\(fullname)" + " -  \(startTime1)" + "\n"
                
            }
        }
        
        let jummaDataCount = (self.iqamaListNewsFeed?.jumma?.count)!
        let buo = BranchUniversalObject.init(canonicalIdentifier: "content/12345")
        buo.contentDescription = "Iqamah & Jumma Share"
        buo.publiclyIndex = true
        buo.locallyIndex = true
        buo.contentMetadata.customMetadata["data"] = "\(self.orgId!)"
        buo.contentMetadata.customMetadata["type"] = "News Feed"
          buo.contentMetadata.customMetadata["typeId"] = "0"
        let lp: BranchLinkProperties = BranchLinkProperties()
        var brachioUrl = String()
        
        if jummaDataCount != 0{
            for i in 0...jummaDataCount - 1{
                
                let jummaType = (self.iqamaListNewsFeed?.jumma?[i].type ?? 1)
                var jummaTitle = String()
                if jummaType == 1{
                    jummaTitle = "JUMMA1"
                }else if jummaType == 2{
                    jummaTitle = "JUMMA2"
                }else if jummaType == 3{
                    jummaTitle = "JUMMA3"
                }else if jummaType == 4{
                    jummaTitle = "JUMMA4"
                }else if jummaType == 5{
                    jummaTitle = "JUMMA5"
                }else if jummaType == 6{
                    jummaTitle = "JUMMA6"
                }else if jummaType == 7{
                    jummaTitle = "JUMMA7"
                }else if jummaType == 8{
                    jummaTitle = "JUMMA8"
                }else if jummaType == 9{
                    jummaTitle = "JUMMA9"
                }else if jummaType == 10{
                    jummaTitle = "JUMMA10"
                }else{
                    jummaTitle = "JUMMA1"
                }
                let time = (self.iqamaListNewsFeed?.jumma?[i].time)!
                let timedateFormatter = DateFormatter()
                timedateFormatter.dateFormat = "HH:mm"
                let starttime = timedateFormatter.date(from: time)
                let timedateFormatter1 = DateFormatter()
                timedateFormatter1.dateFormat = "hh:mm a"
                let startTime1 = timedateFormatter1.string(from: starttime!)
                
                
                let jummaDescription = (self.iqamaListNewsFeed?.jumma?[i].notes)
                let jummaAddress = (self.iqamaListNewsFeed?.jumma?[i].address ?? "")
                shareJummaList = shareJummaList + "\(jummaTitle) \n" + "\(startTime1)\n" + "\(jummaAddress)\n" + jummaDescription! + "\n \n********************** \n \n"
                
            }
            
            
            
            let iqamaTitle = "Iqamah Times in " + self.orgDetails.abbreviation! + "\n" + self.orgDetails.address! + "\n \nAs of " + "\(AppDelegate.instance.currentDate) \n \n"
            
            var iqamaSahreList = iqamaTitle + shareIqamaData + "\n \nJUMMA: \n\n\(shareJummaList)"
            
            
            
            buo.getShortUrl(with: lp) { (url, error) in
                print(url ?? "")
                brachioUrl = url ?? ""
                
                
                
                iqamaSahreList = iqamaSahreList + "Take me to the Iqamah in MuslimHub \n" + brachioUrl + "\n \n" + "Shared from MuslimHub. Download from Apple Store" + "\n" + "https://apps.apple.com/au/app/muslim-hub/id1014406101"
                
                self.btn_ShareTapp(details: [iqamaSahreList])
            }
            
        }else{
            let iqamaTitle = "Iqamah Times in " + self.orgDetails.abbreviation! + "\n" + self.orgDetails.address! + "\n \nAs of " + "\(AppDelegate.instance.currentDate) \n \n"
            
            var iqamaSahreList = iqamaTitle + shareIqamaData
            
            buo.getShortUrl(with: lp) { (url, error) in
                print(url ?? "")
                brachioUrl = url ?? ""
                
                
                
                iqamaSahreList = iqamaSahreList + "Take me to the Iqamah in MuslimHub \n" + brachioUrl + "\n \n" + "Shared from MuslimHub. Download from Apple Store" + "\n" + "https://apps.apple.com/au/app/muslim-hub/id1014406101"
                self.btn_ShareTapp(details: [iqamaSahreList])
                
            }
            
        }
        
        
        
    }
    
    
    func shareDateFormat(startDate:String){
        let dateFormatter = DateFormatter()
         dateFormatter.dateFormat = "dd-MM-yyyy"
        let date = dateFormatter.date(from: startDate )
         let dateFormatter1 = DateFormatter()
         dateFormatter1.dateFormat = "EEE, d MMM yyyy"
         let date1 = dateFormatter1.string(from: date ?? Date())
        self.shareDate = date1
        
         
        
    }
    
    func shareTimeFormat(startTime:String,endTime:String){
        let timedateFormatter = DateFormatter()
                timedateFormatter.dateFormat = "HH:mm"
        let starttime = timedateFormatter.date(from: startTime )
        let endTime = timedateFormatter.date(from: endTime )
                let timedateFormatter1 = DateFormatter()
                timedateFormatter1.dateFormat = "h:mm a"
                let startTime1 = timedateFormatter1.string(from: starttime ?? Date())
                let endTime1 = timedateFormatter1.string(from: endTime ?? Date())
                
            self.shareTime = startTime1 + " - " + endTime1
    }
}

