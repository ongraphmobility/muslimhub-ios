//
//  Alert_AddActivitesViewController.swift
//  Muslim Hub
//
//  Created by Ongraph on 28/05/19.
//  Copyright © 2019 OnGraph. All rights reserved.
//

import UIKit

protocol AddActivityDelegate {
    func addActivityViewDissmissed()
}

extension AddActivityDelegate{
    func addActivityViewDissmissed(){}
}

class Alert_AddActivitesViewController: UIViewController {
    
    @IBOutlet weak var lbl_Add              : UILabel!
    @IBOutlet weak var View_Remainig        : UIView!
    @IBOutlet weak var View_AddActivites    : UIView!
    @IBOutlet weak var tableView_Remaning   : UITableView!
    var orgId                                                   = Int()
    var addRemaning                                             = String()
    var orDetails                           : OrgDetailsData!
    var remaingData                                             = [String]()
    var isActivityTapped                    : Bool              = false
    var delegate                            : AddActivityDelegate!
    var iqamaData : [Iqama]!
    var homeList = [HomeFeedData]()
    var orgType = String()
    var enableDonation:Bool!
    var iqamaListNewsFeed : IQAMAListModel!
    @IBOutlet weak var btn_AddIQMA              : UIButton!
    @IBOutlet weak var btn_AddDonation          : UIButton!
    @IBOutlet weak var btn_AddEvent             : UIButton!
    @IBOutlet weak var btn_AddClass             : UIButton!
    @IBOutlet weak var btn_AddAnnoucement            : UIButton!
    @IBOutlet weak var btn_AddActivites           : UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.FlagApiList()
        
        self.HomeListApi(keyAll: "")
        lbl_Add.clipsToBounds = true
        lbl_Add.layer.cornerRadius = 6
        lbl_Add.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner]
        
        if addRemaning == "addRemaning"{
            View_Remainig.isHidden = false
            View_AddActivites.isHidden = true
            lbl_Add.text = "MISSING ITEM".localized
        }else{
            View_Remainig.isHidden = true
            View_AddActivites.isHidden = false
            lbl_Add.text = "ADD".localized
            
             self.btn_AddIQMA.setTitle("IQAMAH".localized, for: .normal)
             self.btn_AddDonation.setTitle("DONATION".localized, for: .normal)
             self.btn_AddEvent.setTitle("EVENTS".localized, for: .normal)
             self.btn_AddClass.setTitle("CLASSES".localized, for: .normal)
             self.btn_AddAnnoucement.setTitle("ANNOUNCEMENTS".localized, for: .normal)
             self.btn_AddActivites.setTitle("ACTIVITIES".localized, for: .normal)
        }
        if orgType == "mosque"{
            self.FirstPermission()
            // self.btn_AddIQMA.isHidden = true
        }else{
            self.btn_AddIQMA.isHidden = true
        }
        
      
        
        self.tableView_Remaning.tableFooterView = UIView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if self.isActivityTapped{
            self.delegate.addActivityViewDissmissed()
        }
    }
    
    //MARK:IBACtions-------------------------------------------------
    @IBAction func btn_Back(sender:UIButton){
        self.dismiss(animated: true, completion: nil)
    }
    @IBAction func btn_AddActivites(sender:UIButton){
        self.isActivityTapped = true
        switch sender.tag {
        case 0:
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "Add_DonationViewController") as! Add_DonationViewController
            vc.addNewDonation = "addNewDonationFromOrg"
            vc.orgId = self.orgId
            vc.taxDeductable = self.orDetails.taxDeductable!
            vc.modalPresentationStyle = .fullScreen
            self.present(vc, animated: true, completion: nil)
            
            print("Donation")
        case 1:
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "Add_EventViewController") as! Add_EventViewController
            vc.orgId = orgId
            vc.addEventNew = "addEventNew"
            vc.modalPresentationStyle = .fullScreen
            self.present(vc, animated: true, completion: nil)
            print("Add Event")
        case 2:
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "Add_ClassessViewController") as! Add_ClassessViewController
            vc.orgId = orgId
            vc.addNewClasses = "addNewClasses"
            vc.modalPresentationStyle = .fullScreen
            self.present(vc, animated: true, completion: nil)
            print("Add Event")
        case 3:
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "IqamahViewController") as! IqamahViewController
            vc.orgId = orgId
            vc.addNewIqamah = "addNewIqamah"
            //vc.iqamaFilled = iqamaData
            vc.iqamaFilled = iqamaData
            vc.modalPresentationStyle = .fullScreen
            self.present(vc, animated: true, completion: nil)
            print("iqama")
        case 4:
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "Add_ActivitiesViewController") as! Add_ActivitiesViewController
            vc.orgId = orgId
            vc.addNewActivites = "addNewActivites"
            vc.modalPresentationStyle = .fullScreen
            self.present(vc, animated: true, completion: nil)
            print("Add Event")
        case 5:
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "AddAnnoucementViewController") as! AddAnnoucementViewController
            vc.addNewAnnouncent = "addNewAnnouncentFromOrg"
            vc.orgId = self.orgId
            vc.modalPresentationStyle = .fullScreen
            self.present(vc, animated: true, completion: nil)
            
            print("Add Announcement")
            
            
        default:
            //            let vc = self.storyboard?.instantiateViewController(withIdentifier: "Add_EventViewController") as! Add_EventViewController
            //            vc.orgId = orgId
            //            self.present(vc, animated: true, completion: nil)
            break
        }
    }
    
    //MARK:Api's Function-------------------------------------
    func FlagApiList(){
        Network.shared.getFlag(orgId:orgId) { (result) in
            guard let user = result else {return}
            self.remaingData = user.data!
            self.tableView_Remaning.reloadData()
        }
    }
    
    
    func HomeListApi(keyAll:String){
        
        Network.shared.getListOfNewFeed(orgId: orgId ?? 0) { (homeResult,iqamaResult) in
            
            
            guard let user = homeResult else {
                return
            }
            guard let iqamaUserNews = iqamaResult else {
                return
            }
            self.iqamaListNewsFeed = iqamaUserNews
            self.homeList = user
            
        }
    }
    //MARK:Permission
    func FirstPermission(){
        
        
        
        if (UserDefaults.standard.value(forKey: "Login_Token") as? String) != nil{
            
            if let superAdmin = UserDefaults.standard.value(forKey: "isSuperAdmin") as? Bool{
                
                if superAdmin == true{
                    
                }else{
                    
                    if let is_staff = UserDefaults.standard.value(forKey: "is_staff") as? Bool{
                        
                        if is_staff == true{
                            //mark:check For own Organisation---------------------------
                            if let orgOwn_Array = UserDefaults.standard.value(forKey: "OwnOrg_Array") as? [Int]{
                                
                                
                                if orgOwn_Array.contains(orgId){
                                    
                                    var org_P = [PriviligesData]()
                                    org_P = Global.shared.priviligesData
                                    let filertedData = org_P.filter{$0.org_id == orgId}
                                    filertedData.forEach {
                                        //   print("model data objects::",$0.org_perm as? String)
                                        
                                        if $0.org_perm == "iqama_only"{
                                            self.btn_AddIQMA.isHidden = false
                                            self.btn_AddEvent.isHidden = true
                                            self.btn_AddDonation.isHidden = true
                                            self.btn_AddClass.isHidden = true
                                            self.btn_AddActivites.isHidden = true
                                            self.btn_AddAnnoucement.isHidden = true
                                        }else{
                                            
                                            self.btn_AddIQMA.isHidden = false
                                            self.btn_AddEvent.isHidden = false
                                            if enableDonation == true{
                                                      
                                                      self.btn_AddDonation.isHidden = false
                                                  }else{
                                                      self.btn_AddDonation.isHidden = true
                                                  }
                                            self.btn_AddClass.isHidden = false
                                            self.btn_AddActivites.isHidden = false
                                            self.btn_AddAnnoucement.isHidden = false
                                            
                                        }
                                    }
                                    
                                    
                                }else{
                                    
                                }//orgOwn
                                
                            }
                        }//staff true
                    }//staff
                    
                }
            }//SuperAdmin
        }//Login
        else{
            
        }
        
        
        
    }
}

extension Alert_AddActivitesViewController: UITableViewDelegate, UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return remaingData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Alert_AddRemaningTableViewCell") as! Alert_AddRemaningTableViewCell
        cell.lbl_Title.text = remaingData[indexPath.row]
        return cell
    }
}
