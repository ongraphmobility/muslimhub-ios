//
//  OrgsViewController.swift
//  Muslim Hub
//
//  Created by Sakshi Singh on 04/04/19.
//  Copyright © 2019 OnGraph. All rights reserved.
//

import UIKit
import MapKit
import KVNProgress
import SDWebImage
import GoogleMaps

class OrgsViewController: UIViewController,CLLocationManagerDelegate,UITextFieldDelegate,UISearchBarDelegate {
    
    //MARK:IBOUTlets---------------------------------------------
    @IBOutlet weak var btn_Orgs:UIButton!
    @IBOutlet weak var btn_Fav:UIButton!
     @IBOutlet weak var btn_Search:UIButton!
    @IBOutlet weak var searchBar:UISearchBar!
    @IBOutlet weak var btn_All:UIButton!
    @IBOutlet weak var btn_AroundMe:UIButton!
    @IBOutlet weak var img_All:UIImageView!
    @IBOutlet weak var img_AroundMe:UIImageView!
    @IBOutlet weak var stackView_TwoButtons:UIStackView!
    var dispatchGroup  = DispatchGroup()
    
    

    var btnSelected:String!
    var favbtnSelectedtag:Int!
    var isfavbtnSelectedt:String!
    var allOragnizationLists = [OrganizationDataLists]()
    var allfavOnlyOrganizationlists = [OrganizationDataLists]()
    var searchOrgList = [OrganizationDataLists]()
    @IBOutlet weak var tableView:UITableView!
    var lbel_array = ["Islamic Society of Belconnen","Islamic Society of Belconnen","Islamic Society of Belconnen"]
    var isFavonly = Bool()
    //mark:location variables
    let locationManager = CLLocationManager() // create Location Manager object
    var latitude : Double?
    var longitude : Double?
    var isSerachBarTapped: Bool!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
          statusBarColorChange(color:UIColor.init(hex: 0x05BDB9))
        self.buttoncornerRadiusShadow()
        self.searchBar.isHidden = true
        img_All.isHidden = false
        img_AroundMe.isHidden = true
        self.hideKeyboardWhenTappedAround()
        btn_Search.isHidden = false
        
        self.btn_Fav.setTitle("MY FAVOURITE".localized, for: .normal)
        self.btn_Orgs.setTitle("ALL ORGS".localized, for: .normal)
        self.btn_All.setTitle("All".localized, for: .normal)
        self.btn_AroundMe.setTitle("Around Me".localized, for: .normal)
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
      //  UIApplication.statusBarBackgroundColor = UIColor.init(hex: 0x05BDB9)
        self.organizationListApi()
        self.currentLocationLatLong()
        self.tabBarController?.tabBar.isHidden = false
        searchBar.delegate = self
        self.isSerachBarTapped = false
         getPrivilegesDataApi()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        //mark:SearchBar-------------------------------------
        var searchTextField:UITextField!
        
        if #available(iOS 13, *) {
            searchTextField  = searchBar.subviews[0].subviews[2].subviews.last as? UITextField
        } else {
            searchTextField = searchBar.subviews[0].subviews.last as? UITextField
        }
        
        searchTextField.layer.cornerRadius = 15
        searchTextField.textAlignment = NSTextAlignment.left
        searchTextField.textColor = UIColor.white
        searchTextField.leftView = nil
        // searchTextField.placeholder = "Search"
        
        searchTextField.rightViewMode = UITextField.ViewMode.always
        
        if searchTextField.responds(to: #selector(getter: UITextField.attributedPlaceholder)) {
            let attributeDict = [NSAttributedString.Key.foregroundColor: UIColor.white]
            searchTextField.attributedPlaceholder = NSAttributedString(string: "Search".localized, attributes: attributeDict)
        }
    }
    
     func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
       // searchBar.showsCancelButton = true
        if self.btnSelected != "Classes"{
        self.organizationListApi()
        }else{
            self.favOnlyListApi()
        }
    }

    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
      
      self.serachOrganizationListApi()
    }
    @IBAction func btn_Back(sender:UIButton){
        
        if let controller = self.storyboard?.instantiateViewController(withIdentifier: "TabVC") {
            self.slideMenuController()?.changeMainViewController(controller, close: true)
        }
        
        
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    @IBAction func btn_search(sender:UIButton){
        
        if sender.isSelected == true{
            sender.isSelected = false
            searchBar.isHidden = true
            self.isSerachBarTapped = false
        }else{
            sender.isSelected = true
            searchBar.isHidden = false
            self.isSerachBarTapped = true
        }
        
    }
    
    
    //Mark:IBACtions--------------------------------------------
    
    @IBAction func btn_MapSearch(sender:UIButton){
       AppDelegate.instance.checkLocationPermission { (currentLat, currentLong) in
               
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "LocationOrgViewController" ) as! LocationOrgViewController
        vc.allLocationdata = self.allOragnizationLists
        vc.modalPresentationStyle = .fullScreen
        self.present(vc, animated: true, completion: nil)
    }
    }
    
    @IBAction func btn_EnableDonationOrg(sender:UIButton){
        
        if sender.tag == 0 {
            
            btn_All.backgroundColor = UIColor.init(hex: 0x0DC9E4)
            btn_AroundMe.backgroundColor = UIColor.white
            self.organizationListApi()
            btn_All.setTitleColor(UIColor.white, for: .normal)
            btn_AroundMe.setTitleColor(UIColor.darkGray, for: .normal)
          
            btn_Search.isHidden = false
        }else if sender.tag == 1{
            btn_AroundMe.backgroundColor = UIColor.init(hex: 0x0DC9E4)
            btn_All.backgroundColor = UIColor.white
            btn_AroundMe.setTitleColor(UIColor.white, for: .normal)
            btn_All.setTitleColor(UIColor.darkGray, for: .normal)
            searchBar.isHidden = true
            btn_Search.isHidden = true
            self.organizationNearMeListApi()
        }
    }
    @IBAction func btn_Orgs(sender:UIButton){
        
        if sender.tag == 0{
             btn_Search.isHidden = false
            btn_All.backgroundColor = UIColor.init(hex: 0x0DC9E4)
            btn_AroundMe.backgroundColor = UIColor.white
            btn_All.setTitleColor(UIColor.white, for: .normal)
            btn_AroundMe.setTitleColor(UIColor.darkGray, for: .normal)
            
            btn_Orgs.backgroundColor = UIColor.init(hex: 0x0DC9E4)
            btn_Fav.backgroundColor = UIColor.white
            self.isFavonly = false
            self.btnSelected = "IQAMA"
            btn_Orgs.setTitleColor(UIColor.white, for: .normal)
            btn_Fav.setTitleColor(UIColor.darkGray, for: .normal)
            img_All.isHidden = false
            img_AroundMe.isHidden = true
            self.organizationListApi()
        }else{
       btn_Search.isHidden = false
            btn_All.backgroundColor = UIColor.init(hex: 0x0DC9E4)
            btn_AroundMe.backgroundColor = UIColor.white
            btn_All.setTitleColor(UIColor.white, for: .normal)
            btn_AroundMe.setTitleColor(UIColor.darkGray, for: .normal)
            
            btn_Fav.backgroundColor = UIColor.init(hex: 0x0DC9E4)
            btn_Orgs.backgroundColor = UIColor.white
            self.btnSelected = "Classes"
            self.isFavonly = true
            btn_Fav.setTitleColor(UIColor.white, for: .normal)
            btn_Orgs.setTitleColor(UIColor.darkGray, for: .normal)
            img_All.isHidden = true
            img_AroundMe.isHidden = false
            self.favOnlyListApi()
        }
        
        UIView.transition(with: tableView,
                          duration: 0.35,
                          options: .transitionCrossDissolve,
                          animations: { self.tableView.reloadData() }) // left out the unnecessary syntax in the completion block and the optional completion parameter
    }
    //MARK:Functions And Methods------------------------------
    func buttoncornerRadiusShadow (){
        
        btn_All.clipsToBounds = true
        btn_All.layer.cornerRadius = 8
        btn_All.layer.borderColor = UIColor.init(hex: 0x0DC9E4).cgColor
        btn_All.layer.borderWidth = 1.0
        btn_All.layer.maskedCorners = [.layerMinXMaxYCorner, .layerMinXMinYCorner]
        
        btn_AroundMe.clipsToBounds = true
        btn_AroundMe.layer.cornerRadius = 8
        btn_AroundMe.layer.maskedCorners = [.layerMaxXMaxYCorner, .layerMaxXMinYCorner]
        btn_AroundMe.layer.borderColor = UIColor.init(hex: 0x0DC9E4).cgColor
        btn_AroundMe.layer.borderWidth = 1.0
        
        self.btn_Orgs.shadowBlackToHeader()
        self.btn_Fav.shadowBlackToHeader()
        
    }
    
    //mark:Function for current Location Lat long--------------------------------
    func currentLocationLatLong(){
        
        self.locationManager.requestAlwaysAuthorization()
        
        // For use in foreground
        // You will need to update your .plist file to request the authorization
        self.locationManager.requestWhenInUseAuthorization()
        
        if CLLocationManager.locationServicesEnabled() {
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
            locationManager.startUpdatingLocation()
        }
        
    }
    
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let location: CLLocationCoordinate2D = manager.location?.coordinate else { return }
        // set the value of lat and long
        latitude = location.latitude
        longitude = location.longitude
        
    }
    @objc func longPressGestureRecognized(gestureRecognizer: UIGestureRecognizer) {
        
        let longpress = gestureRecognizer as! UILongPressGestureRecognizer
        let state = longpress.state
        let locationInView = longpress.location(in: self.tableView)
        var indexPath = self.tableView.indexPathForRow(at: locationInView)
        
        switch state {
        case .began:
            if indexPath != nil {
                if indexPath?.section == 0{
                    print("section Fav")
                }else{
                     if self.isSerachBarTapped == false {
                    Path.initialIndexPath = indexPath
                    let cell = self.tableView.cellForRow(at: indexPath!) as! Orgs_AllFavTableViewCell
                    
                    My.cellSnapShot = snapshopOfCell(inputView: cell)
                    var center = cell.center
                    My.cellSnapShot?.center = center
                    My.cellSnapShot?.alpha = 0.0
                    self.tableView.addSubview(My.cellSnapShot!)
                    
                    UIView.animate(withDuration: 0.25, animations: {
                        center.y = locationInView.y
                        My.cellSnapShot?.center = center
                        My.cellSnapShot?.transform = CGAffineTransform(scaleX: 1.05, y: 1.05)
                        My.cellSnapShot?.alpha = 0.98
                        cell.alpha = 0.0
                    }, completion: { (finished) -> Void in
                        if finished {
                            cell.isHidden = true
                            
                            
                        }
                           })
                }
                }
            }
            
        case .changed:
            var center = My.cellSnapShot?.center
            center?.y = locationInView.y
            My.cellSnapShot?.center = center!
            
            if indexPath?.section == 0{
                print("section 0")
            }else{
                  if self.isSerachBarTapped == false {
                if ((indexPath != nil) && (indexPath != Path.initialIndexPath)) {
                    
                    self.allfavOnlyOrganizationlists.swapAt((indexPath?.row)!, (Path.initialIndexPath?.row)!)
                    print("new index%@",indexPath!.row)
                    let indexPathAdd = indexPath!.row + 1
                    self.priorityOrganizationListApi(org_id: self.allfavOnlyOrganizationlists[indexPath!.row].id!,priority_index:indexPathAdd)
                    self.tableView.moveRow(at: Path.initialIndexPath!, to: indexPath!)
                    print(Path.initialIndexPath!)
                    Path.initialIndexPath = indexPath
                }
            }
            }
            print("Ended move")
        default:
            if indexPath?.section == 0{
                print("section Fav")
            }else{
                if self.isSerachBarTapped == false {
                    let cell = self.tableView.cellForRow(at: Path.initialIndexPath!) as! Orgs_AllFavTableViewCell
                    cell.isHidden = false
                    cell.alpha = 0.0
                    UIView.animate(withDuration: 0.25, animations: {
                        My.cellSnapShot?.center = cell.center
                        My.cellSnapShot?.transform = .identity
                        My.cellSnapShot?.alpha = 0.0
                        cell.alpha = 1.0
                    }, completion: { (finished) -> Void in
                        if finished {
                            Path.initialIndexPath = nil
                            My.cellSnapShot?.removeFromSuperview()
                            My.cellSnapShot = nil
                        }
                    })
                }
            
            }
        }
    }
    func snapshopOfCell(inputView: UIView) -> UIView {
        
        UIGraphicsBeginImageContextWithOptions(inputView.bounds.size, false, 0.0)
        inputView.layer.render(in: UIGraphicsGetCurrentContext()!)
        let image = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        let cellSnapshot : UIView = UIImageView(image: image)
        cellSnapshot.layer.masksToBounds = false
        cellSnapshot.layer.cornerRadius = 0.0
        cellSnapshot.layer.shadowOffset = CGSize(width: -5.0, height: 0.0)
        cellSnapshot.layer.shadowRadius = 5.0
        cellSnapshot.layer.shadowOpacity = 0.4
        return cellSnapshot
    }
    
    struct My {
        static var cellSnapShot: UIView? = nil
    }
    
    struct Path {
        static var initialIndexPath: IndexPath? = nil
    }
    
    @objc func btn_favUnfav(sender:UIButton){
        
        if  self.allOragnizationLists[sender.tag].is_private == true{
       
            self.dispatchGroup.enter()
            if sender.isSelected {
               
                AKAlertController.alert("", message: "You are a member of this Private Organisation.Are you sure you want to remove from your favourtie list", buttons: ["Ok","Cancel"], tapBlock: { (_, _, index) in
                if index == 1 {return}
                    
                    sender.isSelected = false
                    self.allOragnizationLists[sender.tag].is_fav = false
                    self.favbtnSelectedtag = self.allOragnizationLists[sender.tag].id
                    self.isfavbtnSelectedt = "false"
                    self.dispatchGroup.leave()
                     })
                
                }else{
               
                    sender.isSelected = true
                    self.allOragnizationLists[sender.tag].is_fav = true
                    self.favbtnSelectedtag = self.allOragnizationLists[sender.tag].id
                    
                    self.isfavbtnSelectedt = "true"
                    self.dispatchGroup.leave()
                }
            
            self.dispatchGroup.notify(queue: .main) {
                DispatchQueue.global(qos: .background).async {
                    self.favUnFavApi()
                    DispatchQueue.main.async {
                        let indexpath = IndexPath(row: sender.tag, section: 0)
                        self.tableView.reloadRows(at: [indexpath], with: .none)
                        
                    }
                }
            }
             
            
           
        }else{
         
                if sender.isSelected {
                    sender.isSelected = false
                    self.allOragnizationLists[sender.tag].is_fav = false
                    self.favbtnSelectedtag = self.allOragnizationLists[sender.tag].id
                    self.isfavbtnSelectedt = "false"
                }else{
                    sender.isSelected = true
                    self.allOragnizationLists[sender.tag].is_fav = true
                    self.favbtnSelectedtag = self.allOragnizationLists[sender.tag].id
                    
                    self.isfavbtnSelectedt = "true"
                }
                
                DispatchQueue.global(qos: .background).async {
                    self.favUnFavApi()
                    DispatchQueue.main.async {
                        let indexpath = IndexPath(row: sender.tag, section: 0)
                        self.tableView.reloadRows(at: [indexpath], with: .none)
                        
                    }
                }
           
        }
       
    }
    @objc func btn_favUnfavOnly(sender:UIButton){
         self.dispatchGroup.enter()
        if  self.allfavOnlyOrganizationlists[sender.tag].is_private == true{
           
            if sender.isSelected {
                
                
                AKAlertController.alert("", message: "You are a member of this Private Organisation.Are you sure you want to remove from your favourtie list", buttons: ["Ok","Cancel"], tapBlock: { (_, _, index) in
                    if index == 1 {return}
                    sender.isSelected = false
                    self.allfavOnlyOrganizationlists[sender.tag].is_fav = false
                    self.favbtnSelectedtag = self.allfavOnlyOrganizationlists[sender.tag].id
                    self.isfavbtnSelectedt = "false"
                    self.dispatchGroup.leave()
                }
                )
                
               
            }else{
                
                self.allfavOnlyOrganizationlists[sender.tag].is_fav = true
                self.favbtnSelectedtag = allfavOnlyOrganizationlists[sender.tag].id
                isfavbtnSelectedt = "true"
                  self.dispatchGroup.leave()
            }
            
        }else{
            if sender.isSelected {
                sender.isSelected = false
                self.allfavOnlyOrganizationlists[sender.tag].is_fav = false
                self.favbtnSelectedtag = allfavOnlyOrganizationlists[sender.tag].id
                isfavbtnSelectedt = "false"
                  self.dispatchGroup.leave()
            }else{
                self.allfavOnlyOrganizationlists[sender.tag].is_fav = true
                self.favbtnSelectedtag = allfavOnlyOrganizationlists[sender.tag].id
                isfavbtnSelectedt = "true"
                  self.dispatchGroup.leave()
            }
        }
       
           self.dispatchGroup.notify(queue: .main) {
            
            let indexpath = IndexPath(row: sender.tag, section: 1)
            self.tableView.reloadRows(at: [indexpath], with: .none)
            self.favUnFavonlyApi()
        }
        
      
    }
    @objc func btn_Location(sender: UITapGestureRecognizer){
        
        let lat = self.allOragnizationLists[sender.view!.tag].latitude ?? 123.123
        let long = self.allOragnizationLists[sender.view!.tag].longitude ?? 123.123
        self.location(lat: lat, long: long,title: self.allOragnizationLists[sender.view!.tag].address ?? "")
 
        
    }
    
    //MARK:Function Api-----------------------------
    
    func organizationListApi(){
        KVNProgress.show()
        Network.shared.organizationLists() { (result) in
            KVNProgress.dismiss()
            guard let user = result else {
                
                return
            }
            
            self.allOragnizationLists = user
            self.tableView.reloadData()
        }
    }
    
    func favOnlyListApi(){
        // KVNProgress.show()
        Network.shared.favOnlyLists(fav_only:self.isFavonly) { (result) in
            //   KVNProgress.dismiss()
            guard let user = result else {
                
                return
            }
            
            self.allfavOnlyOrganizationlists = user
            self.tableView.reloadData()
        }
    }
    func favUnFavApi(){
        
        Network.shared.favUnFav(org_id:  self.favbtnSelectedtag ?? 0, is_fav: isfavbtnSelectedt) { (result) in
            
            guard result != nil else {
                
                return
            }
            // self.organizationListApi()
            
        }
    }
    func favUnFavonlyApi(){
        
        Network.shared.favUnFav(org_id:  self.favbtnSelectedtag ?? 0, is_fav: isfavbtnSelectedt) { (result) in
            
            guard result != nil else {
                
                return
            }
            self.favOnlyListApi()
        }
    }
    
    func priorityOrganizationListApi(org_id:Int,priority_index:Int){
        var token: String = ""
        var device_id:String = ""
        if let loginToken = UserDefaults.standard.value(forKey: "Login_Token") as? String{
            token = (loginToken)
            device_id = ""
        }else{
            if let deviceId = UserDefaults.standard.value(forKeyPath: "deviceToken") as? String{ //2.  if user is Anonymous:
                token = ""
                device_id = deviceId
            }
        }
        KVNProgress.show()
        Network.shared.orgPriority(token: token, device_id: device_id, org_id: org_id, priority_index: priority_index){ (result) in
            KVNProgress.dismiss()
            guard result != nil else {
                
                return
            }
            
            //  self.tableView.reloadData()
        }
    }
    
    
    
    func organizationNearMeListApi(){
        KVNProgress.show()
        //         let latitude_str:String = String(format:"%.1f", self.latitude ?? 123.555)
        //         let longitute_str:String = String(format:"%.1f", self.longitude ?? 123.234)
        Network.shared.organizationListsNearMe(latitude:self.latitude ?? 123.123, longitude: self.longitude ?? 123.123){ (result) in
            KVNProgress.dismiss()
            guard let user = result else {
                
                return
            }
            
            self.allOragnizationLists = user
            self.tableView.reloadData()
        }
    }
    
    func serachOrganizationListApi(){
        KVNProgress.show()
         if self.btnSelected == "Classes"{
            Network.shared.getListOfSearchOrganization(searchKey: trimString(str: self.searchBar.text ?? ""), isfav: "true") { (result) in
            KVNProgress.dismiss()
            guard let user = result else {

                return
            }
           
               self.allfavOnlyOrganizationlists = user
            self.tableView.reloadData()
            }
           
          
         }else{
            Network.shared.getListOfSearchOrganization(searchKey: trimString(str: self.searchBar.text ?? ""), isfav: "false") { (result) in
                KVNProgress.dismiss()
                guard let user = result else {
                    
                    return
                }
                
              self.allOragnizationLists = user
                self.tableView.reloadData()
            }
        }
        
    }
    
}
//Mark:TableView DataSource Delegate-------------------------------------------
extension OrgsViewController:UITableViewDelegate,UITableViewDataSource{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return 2
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.btnSelected == "Classes"{
            
            switch section{
            case 0:
                return 0
            case 1:
                return self.allfavOnlyOrganizationlists.count
            default:
                break
            }
            
        }else{
            
            switch section{
            case 0:
                return self.allOragnizationLists.count
            case 1:
                return 0
            default:
                break
            }
            
        }
        return 0
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        switch indexPath.section {
        case 0:
      
            let cell = tableView.dequeueReusableCell(withIdentifier: "OrgsTableViewCell", for: indexPath) as! OrgsTableViewCell
            
            
            
            if (UserDefaults.standard.value(forKey: "Login_Token") as? String) != nil{
                 cell.img_Logo.borderColor = UIColor.init(hex: 0xF4A441)
                if let superAdmin = UserDefaults.standard.value(forKey: "isSuperAdmin") as? Bool{
                    
                    if superAdmin == true{
                     cell.img_Logo.borderColor = UIColor.init(hex: 0xF4A441)
                         cell.btn_Heart.isHidden = false
                    }else{
                        
                        if let is_staff = UserDefaults.standard.value(forKey: "is_staff") as? Bool{
                            
                            if is_staff == true{
                                //mark:check For own Organisation---------------------------
                                if let orgOwn_Array = UserDefaults.standard.value(forKey: "OwnOrg_Array") as? [Int]{
                                    
                                    if orgOwn_Array.contains(self.allOragnizationLists[indexPath.row].id ?? 0){
                                        
                                        cell.img_Logo.borderColor = UIColor.red
                                        cell.btn_Heart.isHidden = true
                                    }else{
                                        cell.btn_Heart.isHidden = false
                                           cell.img_Logo.borderColor = UIColor.init(hex: 0xF4A441)
                                        
                                    }//orgOwn
                                    
                                }
                            }//staff true
                        }//staff
                        
                    }
                }//SuperAdmin
            }//Login
            else{
                 cell.btn_Heart.isHidden = false
                cell.img_Logo.borderColor = UIColor.init(hex: 0xF4A441)
                
                
            }
            
            
            
            
            let longpress = UILongPressGestureRecognizer(target: self, action: #selector(longPressGestureRecognized(gestureRecognizer:)))
            self.tableView.addGestureRecognizer(longpress)
            cell.btn_Heart.tag = indexPath.row
            cell.btn_Heart.addTarget(self, action: #selector(btn_favUnfav(sender:)), for: .touchUpInside)
            if self.allOragnizationLists[indexPath.row].is_fav == true{
                cell.btn_Heart.isSelected = true
                cell.btn_Heart.setBackgroundImage(#imageLiteral(resourceName: "heart_selected_iPhone"), for: .normal)
                
            }else{
                cell.btn_Heart.isSelected = false
                cell.btn_Heart.setBackgroundImage(#imageLiteral(resourceName: "heart_unselected_iPhone"), for: .normal)
            }
            
            cell.btn_Heart.tintColor = UIColor.white
            
            
            //      cell.img_Logo.layer.masksToBounds = true
            
            DispatchQueue.main.async {
                if let photo = self.allOragnizationLists[indexPath.row].logo, let url = URL(string: photo) {
                    print("indexpath:",indexPath.row)
                    print("logo url :",url)
                    
                    cell.img_Logo.sd_setImage(with: url, placeholderImage: UIImage.init(named: "default_icon_iPhone_Logo.png"))
                    
                } else {
                    
                    cell.img_Logo.image = UIImage(named: "default_icon_iPhone_Logo.png")
                }
                
            }
            
            
            
            let linkAttributes: [NSAttributedString.Key : Any] = [
                NSAttributedString.Key(rawValue: NSAttributedString.Key.foregroundColor.rawValue): UIColor.gray,
                NSAttributedString.Key(rawValue: NSAttributedString.Key.underlineColor.rawValue): UIColor.lightGray,
                NSAttributedString.Key(rawValue: NSAttributedString.Key.underlineStyle.rawValue): NSUnderlineStyle.single.rawValue
            ]
            let attributeString = NSMutableAttributedString(string: self.allOragnizationLists[indexPath.row].address ?? "", attributes: linkAttributes)
            
            cell.btn_Location.tag = indexPath.row
            
            
            cell.btn_Location.attributedText = attributeString
            
           let gesture = UITapGestureRecognizer(target: self, action: #selector(btn_Location))
           // if labelView is not set userInteractionEnabled, you must do so
           cell.btn_Location.isUserInteractionEnabled = true
           cell.btn_Location.addGestureRecognizer(gesture)

            
            cell.lbl_Details.text = self.allOragnizationLists[indexPath.row].abbreviation ?? ""
            
            
            cell.lbl_Title.text = self.allOragnizationLists[indexPath.row].name ?? ""
            return cell
        case 1:
            let cell = tableView.dequeueReusableCell(withIdentifier: "Orgs_AllFavTableViewCell", for: indexPath) as! Orgs_AllFavTableViewCell
            
            
            if (UserDefaults.standard.value(forKey: "Login_Token") as? String) != nil{
                 cell.img_Logo.borderColor = UIColor.init(hex: 0xF4A441)
                if let superAdmin = UserDefaults.standard.value(forKey: "isSuperAdmin") as? Bool{
                    
                    if superAdmin == true{
                         cell.btn_Heart.isHidden = false
                         cell.img_Logo.borderColor = UIColor.init(hex: 0xF4A441)
                    }else{
                        
                        if let is_staff = UserDefaults.standard.value(forKey: "is_staff") as? Bool{
                            
                            if is_staff == true{
                                //mark:check For own Organisation---------------------------
                                if let orgOwn_Array = UserDefaults.standard.value(forKey: "OwnOrg_Array") as? [Int]{
                                    
                                    if orgOwn_Array.contains(self.allfavOnlyOrganizationlists[indexPath.row].id ?? 0){
                                         cell.btn_Heart.isHidden = true
                                        cell.img_Logo.borderColor = UIColor.red
                                        
                                    }else{
                                         cell.btn_Heart.isHidden = false
                                        cell.img_Logo.borderColor = UIColor.init(hex: 0xF4A441)
                                        
                                    }//orgOwn
                                    
                                }
                            }//staff true
                        }//staff
                        
                    }
                }//SuperAdmin
            }//Login
            else{
                
                 cell.btn_Heart.isHidden = false
                cell.img_Logo.borderColor = UIColor.init(hex: 0xF4A441)
                
            }
            cell.lbl_Details.text = allfavOnlyOrganizationlists[indexPath.row].abbreviation ?? ""
            cell.lbl_Title.text = self.allfavOnlyOrganizationlists[indexPath.row].name ?? ""
            
            //cell.img_Logo.contentMode = .scaleToFill
            DispatchQueue.main.async {
                
                if let photo = self.allfavOnlyOrganizationlists[indexPath.row].logo, let url = URL(string: photo) {
                    
                    cell.img_Logo.sd_setImage(with: url, placeholderImage: UIImage.init(named: "default_icon_iPhone_Logo.png"))
                } else {
                    cell.img_Logo.image = UIImage(named: "default_icon_iPhone_Logo.png")
                    
                }
            }
            
            
            let linkAttributes: [NSAttributedString.Key : Any] = [
                NSAttributedString.Key(rawValue: NSAttributedString.Key.foregroundColor.rawValue): UIColor.gray,
                NSAttributedString.Key(rawValue: NSAttributedString.Key.underlineColor.rawValue): UIColor.lightGray,
                NSAttributedString.Key(rawValue: NSAttributedString.Key.underlineStyle.rawValue): NSUnderlineStyle.single.rawValue
            ]
            let attributeString = NSMutableAttributedString(string: self.allfavOnlyOrganizationlists[indexPath.row].address ?? "", attributes: linkAttributes)
            
            cell.btn_Location.tag = indexPath.row
            
           
             cell.btn_Location.attributedText = attributeString
             
            let gesture = UITapGestureRecognizer(target: self, action: #selector(btn_Location))
            // if labelView is not set userInteractionEnabled, you must do so
            cell.btn_Location.isUserInteractionEnabled = true
            cell.btn_Location.addGestureRecognizer(gesture)

            cell.btn_Heart.tag = indexPath.row
            cell.btn_Heart.addTarget(self, action: #selector(btn_favUnfavOnly(sender:)), for: .touchUpInside)
            if self.allfavOnlyOrganizationlists[indexPath.row].is_fav == true{
                cell.btn_Heart.isSelected = true
                cell.btn_Heart.setBackgroundImage(#imageLiteral(resourceName: "heart_selected_iPhone"), for: .normal)
                
            }else{
                cell.btn_Heart.isSelected = false
                cell.btn_Heart.setBackgroundImage(#imageLiteral(resourceName: "heart_unselected_iPhone"), for: .normal)
            }
            
            cell.btn_Heart.tintColor = UIColor.white
            return cell
        default:
            break
            
        }
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
        if btnSelected == "Classes"{
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "ActivitiesViewController") as! ActivitiesViewController
            vc.modalPresentationStyle = .fullScreen
            vc.orgId = self.allfavOnlyOrganizationlists[indexPath.row].id ?? 0
            vc.allFavOrg = "AllFavOrg"
            let allfavOrgId = allfavOnlyOrganizationlists.map({ $0.id})
            vc.array_allfavOrgId = allfavOrgId as! [Int]
            vc.org_type = self.allfavOnlyOrganizationlists[indexPath.row].org_type ?? ""
            print(allfavOrgId)
            vc.modalPresentationStyle = .fullScreen
            self.present(vc, animated: true, completion: nil)
        }else{
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "ActivitiesViewController") as! ActivitiesViewController
            vc.orgId = self.allOragnizationLists[indexPath.row].id ?? 0
              vc.org_type = self.allOragnizationLists[indexPath.row].org_type ?? ""
             vc.modalPresentationStyle = .fullScreen
            self.present(vc, animated: true, completion: nil)
        }
        
    }
    
    
    func trimString(str: String) -> String{
        return str.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
    }
}

extension UIButton {

    func makeMultiLineSupport() {
        guard let titleLabel = titleLabel else {
            return
        }
        titleLabel.numberOfLines = 0
        titleLabel.setContentHuggingPriority(.required, for: .vertical)
        titleLabel.setContentHuggingPriority(.required, for: .horizontal)
        addConstraints([
            .init(item: titleLabel,
                  attribute: .top,
                  relatedBy: .greaterThanOrEqual,
                  toItem: self,
                  attribute: .top,
                  multiplier: 1.0,
                  constant: contentEdgeInsets.top),
            .init(item: titleLabel,
                  attribute: .bottom,
                  relatedBy: .greaterThanOrEqual,
                  toItem: self,
                  attribute: .bottom,
                  multiplier: 1.0,
                  constant: contentEdgeInsets.bottom),
            .init(item: titleLabel,
                  attribute: .left,
                  relatedBy: .greaterThanOrEqual,
                  toItem: self,
                  attribute: .left,
                  multiplier: 1.0,
                  constant: contentEdgeInsets.left),
            .init(item: titleLabel,
                  attribute: .right,
                  relatedBy: .greaterThanOrEqual,
                  toItem: self,
                  attribute: .right,
                  multiplier: 1.0,
                  constant: contentEdgeInsets.right)
            ])
    }

}
