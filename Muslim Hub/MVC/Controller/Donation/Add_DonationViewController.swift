//
//  DonationViewController.swift
//  Muslim Hub
//
//  Created by Ongraph on 31/07/19.
//  Copyright © 2019 OnGraph. All rights reserved.
//

import UIKit
import YangMingShan
import CountryPickerViewSwift
import KVNProgress



class Add_DonationViewController: UIViewController,UIImagePickerControllerDelegate,UITextFieldDelegate,UITextViewDelegate {
    
    @IBOutlet weak var tableView_Donation:UITableView!
    @IBOutlet weak var view_UploadPhoto:UIView!
    @IBOutlet weak var collectionView_Images:UICollectionView!
    @IBOutlet weak var view_UpperTableView:UIView!
    @IBOutlet weak var view_LowerTableView:UIView!
    @IBOutlet weak var lbl_NoImgae:UILabel!
    @IBOutlet weak var btn_Upload:UIButton!
    @IBOutlet weak var btn_Save:UIButton!
    @IBOutlet weak var btn_Cancel:UIButton!
    @IBOutlet weak var btn_AddMore:UIButton!
    
    var eventDetails: EventData!
    var addNewDonation = String()
    var addEventNew:String!
    var arrayImages = [UIImage]()
    var orgId = Int()
    var imageData = [Data]()
    var imageArray: [Any] = []
    var donation_Array = ["Title","Description","Tax_Deductable","Start_Date","Donation_Type","Phone","Email","Payment_Gateway"]
    var payement_array = [1]
    var myArrayDonation = [[String: Any]]()
    var dict = [String : Any]()
    var dict_Donation = [String : Any]()
    var startDate = Date()
    var endDate = Date()
    var str_webSite = ""
    var donationData:HomeFeedData!
    var phoneCode = ""
    var phoneNumber = ""
    var taxDeductable = false
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if #available(iOS 13.0, *) {
            statusBarColorChange(color:UIColor.init(hex: 0x0DC9E4))
        } else {
            UIApplication.statusBarBackgroundColor = UIColor.init(hex: 0x0DC9E4)
        }
        
        if donationData != nil{
            
            if taxDeductable == false{
                self.dict.updateValue(false , forKey:"Tax_Deductable")
                
            }
     
                               
            if donationData.images.count != 0{
                
                collectionView_Images.isHidden = false
                self.view_UploadPhoto.isHidden = true
                lbl_NoImgae.isHidden = true
            }else{
                collectionView_Images.isHidden = true
                self.view_UploadPhoto.isHidden = true
                lbl_NoImgae.isHidden = false
            }
            if donationData.title != ""{
                self.dict.updateValue(donationData.title , forKey:"Title")
                
            }
            if donationData.datumDescription != ""{
                self.dict.updateValue(donationData.datumDescription , forKey:"Description")
                
            }
            if donationData.tax_deductable != nil{
                self.dict.updateValue(donationData.tax_deductable!, forKey:"Tax_Deductable")
                
            }
            if donationData.startDate != nil{
                
                self.dict.updateValue(donationData.startDate ?? "", forKey:"Start_Date")
                
            }
            if donationData.endDate != nil{
                self.dict.updateValue(donationData.endDate ?? "", forKey:"End_Date")
                
            }
            if donationData.donationType != nil{
                
                
                
                self.dict.updateValue(donationData.donationType ?? "", forKey:"Donation_Type")
                
            }
            if donationData.phone_number != nil{
                
                if donationData.phone_number == "" || donationData.phone_number == nil{
                    self.phoneCode = "+61"
                    self.phoneNumber = ""
                }else{
                    let phone =  donationData.phone_number?.components(separatedBy: "-")
                    self.phoneCode = phone?[0] ?? "+61"
                    self.phoneNumber = phone?[1] ?? ""
                    
                }
                self.dict.updateValue(self.phoneCode, forKey: "Code")
                self.dict.updateValue(self.phoneNumber , forKey:"Phone")
                
            }
            if donationData.email != nil{
                
                self.dict.updateValue(donationData.email!, forKey:"Email")
                
            }
            if donationData.paymentGateway != nil{
                
                if donationData.paymentGateway == 2{
                    self.str_webSite = "Website"
                    self.payement_array.removeAll()
                    self.btn_AddMore.isHidden = true
                    
                    if taxDeductable == false{
                        self.donation_Array = ["Title","Description","Start_Date","Donation_Type","Phone","Email","Payment_Gateway","WebSite"]
                        
                    }else{
                        self.donation_Array = ["Title","Description","Tax_Deductable","Start_Date","Donation_Type","Phone","Email","Payment_Gateway","WebSite"]
                        
                    }
                    
                    self.dict.updateValue(1, forKey: "defind")
                    self.dict.updateValue(2, forKey: "Payment_Gateway")
                    if donationData.website != nil{
                        
                        self.dict.updateValue(donationData.website ?? "", forKey:"WebSite_URL")
                        
                    }
                }else{
                    self.str_webSite = "Poli"
                    
                    self.btn_AddMore.isHidden = false
                    
                    let amount = self.donationData.pre_dif_amount_list!.map({ $0.amount})
                    let description = self.donationData.pre_dif_amount_list!.map({ $0.preDIFAmountListDescription})
                    
                    self.myArrayDonation = []
                    
                    for i in 0...amount.count - 1{
                        
                        let dic = ["amount": amount[i], "description": description[i]] as [String : Any]
                        self.myArrayDonation.append(dic)
                    }
                    
                    if taxDeductable == false{
                        self.donation_Array = ["Title","Description","Start_Date","Donation_Type","Phone","Email","Payment_Gateway","defind"]
                    }else{
                        
                        self.donation_Array = ["Title","Description","Tax_Deductable","Start_Date","Donation_Type","Phone","Email","Payment_Gateway","defind"]
                        
                    }
                    
                    
                    
                    if self.donationData.amountType == 1{
                        self.dict.updateValue(1, forKey: "defind")
                    }else{
                        self.dict.updateValue(2, forKey: "defind")
                    }
                    
                    self.dict.updateValue(1, forKey: "Payment_Gateway")
                }
                
                self.dict.updateValue(donationData.website ?? "", forKey:"WebSite_URL")
                
            }
            self.tableView_Donation.reloadData()
        }else{
            
            for i in 0...donation_Array.count - 1{
                self.dict.updateValue("", forKey: donation_Array[i])
            }
            self.btn_Save.shadowWithCornerReadius(cornerRadius: 5)
            self.btn_Cancel.shadowWithCornerReadius(cornerRadius: 5)
            if taxDeductable == false{
                self.dict.updateValue(false , forKey:"Tax_Deductable")
                
            }else{
                
                self.dict.updateValue(true, forKey:donation_Array[2])
            }
            
            
            
            self.payement_array.removeAll()
            self.btn_AddMore.isHidden = true
            self.dict.updateValue("+61", forKey: "Code")
            self.dict.updateValue("Description", forKey:"Description")
            self.dict.updateValue("", forKey:"WebSite_URL")
        }
        
        
        
        statusBarColorChange(color:UIColor.init(hex: 0x0DC9E4))
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.cornerRadiusAndShadow()
        //  UIApplication.statusBarBackgroundColor = UIColor.init(hex: 0x05BDB9)
        
        if addNewDonation == "addNewDonationFromOrg"{
            if arrayImages.count > 0{
                
                collectionView_Images.isHidden = false
                self.view_UploadPhoto.isHidden = true
            }else{
                collectionView_Images.isHidden = true
                self.view_UploadPhoto.isHidden = false
            }
        }
        
    }
    //Mark:TextView Delegate and DataSource Method------------------------------
    func textViewDidBeginEditing(_ textView: UITextView) {
        guard let indexPath = textView.tableViewIndexPath(self.tableView_Donation) else { return }
        
        switch (indexPath.section, 8) {
            
        case (0,8):
            
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "MHDescViewController") as! MHDescViewController
            vc.delegate = self
            vc.modalPresentationStyle = .fullScreen
            vc.textDesc = textView.text!
            textView.keyboardType = .default
            
            present(vc, animated: false, completion: nil)
        default:
            break
            
        }
        
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        guard let indexPath = textView.tableViewIndexPath(self.tableView_Donation) else { return }
        
        switch (indexPath.section, 8) {
            
        case (0,8):
            
            textView.textColor = UIColor.black
        default:
            break
            
        }
        
    }
    
    
    //MARK:TextField Delegate and DataSource Method--------------------
    func textFieldDidBeginEditing(_ textField: UITextField) {
        guard let indexPath = textField.tableViewIndexPath(self.tableView_Donation) else { return }
        
        if taxDeductable == false{
            switch (indexPath.section, indexPath.row) {
                
            case (0,2):
                
                
                let cell: Add_DonationTableViewCell = self.tableView_Donation.cellForRow(at: IndexPath(row: indexPath.row, section: indexPath.section)) as! Add_DonationTableViewCell
                if textField == cell.txt_StartDate{
                    AKDatePicker.openPicker(in: textField, currentDate: Date(), pickerMode: .date) { (value) in
                        self.startDate = value
                        //  self.dict.updateValue("", forKey: "End_Date")
                        if self.dict["End_Date"] as? String != ""{
                            
                            
                            if self.startDate > self.endDate  {
                                AKAlertController.alert("Start date should be greater than End date", message: "")
                            }else{
                                textField.text = self.convertToString(date: value, dateformat: "dd-MM-yyyy")
                                self.dict.updateValue(textField.text!, forKey: "Start_Date") // End Date
                            }
                        }else{
                            textField.text = self.convertToString(date: value, dateformat: "dd-MM-yyyy")
                            cell.txt_EndDate.text = self.convertToString(date: value, dateformat: "dd-MM-yyyy")
                            self.dict.updateValue(cell.txt_EndDate.text!, forKey: "End_Date")
                            self.dict.updateValue(textField.text!, forKey: "Start_Date") // Start Date
                        }
                        
                    }
                }else if textField == cell.txt_EndDate{
                    AKDatePicker.openPicker(in: textField, currentDate: Date(), pickerMode: .date) { (value) in
                        self.endDate = value
                        if self.dict["End_Date"] as? String != ""{
                            
                            
                            if self.startDate > self.endDate  {
                                AKAlertController.alert("Start date should be greater than End date", message: "")
                            }else{
                                textField.text = self.convertToString(date: value, dateformat: "dd-MM-yyyy")
                                self.dict.updateValue(textField.text!, forKey: "End_Date") // End Date
                            }
                        }else{
                            textField.text = self.convertToString(date: value, dateformat: "dd-MM-yyyy")
                            self.dict.updateValue(textField.text!, forKey: "End_Date") // End Date
                        }
                        
                    }
                    
                    
                }
                
                
            case (0,3):
                
                let array_DonationType = ["Medical", "Education","Religious"]
                AKMultiPicker().openPickerIn(textField, firstComponentArray: array_DonationType) { (value, _, index1, _) in
                    textField.text = value
                    
                    if value == "Medical"{
                        self.dict.updateValue(1, forKey: "Donation_Type")
                    }else if value == "Education"{
                        self.dict.updateValue(2, forKey: "Donation_Type")
                    }else if value == "Religious"{
                        self.dict.updateValue(3, forKey: "Donation_Type")
                    }
                }
                
                
            case (0,4):
                textField.keyboardType = .phonePad
                let index = IndexPath(row: 4, section: 0)
                let cell: Add_DonationTableViewCell = self.tableView_Donation.cellForRow(at: index) as! Add_DonationTableViewCell
                
                if textField == cell.txt_Code{
                    
                    let countryView = CountrySelectView.shared
                    countryView.barTintColor = .red
                    countryView.displayLanguage = .english
                    countryView.show()
                    countryView.selectedCountryCallBack = { (countryDic) -> Void in
                        
                        
                        
                        cell.txt_Code.text = "+\(countryDic["code"] as! NSNumber)"
                        
                        self.dict.updateValue(cell.txt_Code.text!, forKey: "Code")
                        
                    }
                }else if textField == cell.txt_Title{
                    
                    self.dict.updateValue(cell.txt_Title.text!, forKey: "Phone")
                    
                }
                
            case (0,5):
                textField.keyboardType = .emailAddress
                
                
            case (0,6):
                
                let array_Payment = ["Poli","Website"]
                AKMultiPicker().openPickerIn(textField, firstComponentArray: array_Payment) { (value, _, index1, _) in
                    textField.text = value
                    
                    if value == "Website"{
                        self.str_webSite = "Website"
                        self.myArrayDonation.removeAll()
                        self.btn_AddMore.isHidden = true
                        
                        
                        self.donation_Array = ["Title","Description","Start_Date","Donation_Type","Phone","Email","Payment_Gateway","WebSite"]
                        self.dict.updateValue(2, forKey: "Payment_Gateway")
                        self.tableView_Donation.reloadData()
                    }else{
                        self.str_webSite = "Poli"
                        self.payement_array = [1]
                        self.btn_AddMore.isHidden = false
                        self.donation_Array = ["Title","Description","Start_Date","Donation_Type","Phone","Email","Payment_Gateway","defind"]
                        
                        if self.myArrayDonation.count == 0{
                            self.dict_Donation = ["amount": 1.0, "description": ""]
                            self.myArrayDonation.append(self.dict_Donation)
                        }
                        
                        
                        self.dict.updateValue(1, forKey: "defind")
                        self.dict.updateValue(1, forKey: "Payment_Gateway")
                    }
                    self.tableView_Donation.reloadData()
                }
                
            case (0,7):
                
                if self.str_webSite == "Website"{
                    self.dict.updateValue(textField.text!, forKey: "WebSite_URL")
                }
            default:
                break
            }
            
        }else{
            switch (indexPath.section, indexPath.row) {
                
            case (0,3):
                
                
                let cell: Add_DonationTableViewCell = self.tableView_Donation.cellForRow(at: IndexPath(row: indexPath.row, section: indexPath.section)) as! Add_DonationTableViewCell
                if textField == cell.txt_StartDate{
                    AKDatePicker.openPicker(in: textField, currentDate: Date(), pickerMode: .date) { (value) in
                        self.startDate = value
                        //  self.dict.updateValue("", forKey: "End_Date")
                        if self.dict["End_Date"] as? String != ""{
                            
                            
                            if self.startDate > self.endDate  {
                                AKAlertController.alert("Start date should be greater than End date", message: "")
                            }else{
                                textField.text = self.convertToString(date: value, dateformat: "dd-MM-yyyy")
                                self.dict.updateValue(textField.text!, forKey: "Start_Date") // End Date
                            }
                        }else{
                            textField.text = self.convertToString(date: value, dateformat: "dd-MM-yyyy")
                            cell.txt_EndDate.text = self.convertToString(date: value, dateformat: "dd-MM-yyyy")
                            self.dict.updateValue(cell.txt_EndDate.text!, forKey: "End_Date")
                            self.dict.updateValue(textField.text!, forKey: "Start_Date") // Start Date
                        }
                        
                    }
                }else if textField == cell.txt_EndDate{
                    AKDatePicker.openPicker(in: textField, currentDate: Date(), pickerMode: .date) { (value) in
                        self.endDate = value
                        if self.dict["End_Date"] as? String != ""{
                            
                            
                            if self.startDate > self.endDate  {
                                AKAlertController.alert("Start date should be greater than End date", message: "")
                            }else{
                                textField.text = self.convertToString(date: value, dateformat: "dd-MM-yyyy")
                                self.dict.updateValue(textField.text!, forKey: "End_Date") // End Date
                            }
                        }else{
                            textField.text = self.convertToString(date: value, dateformat: "dd-MM-yyyy")
                            self.dict.updateValue(textField.text!, forKey: "End_Date") // End Date
                        }
                        
                    }
                    
                    
                }
                
                
            case (0,4):
                
                let array_DonationType = ["Medical", "Education","Religious"]
                AKMultiPicker().openPickerIn(textField, firstComponentArray: array_DonationType) { (value, _, index1, _) in
                    textField.text = value
                    
                    if value == "Medical"{
                        self.dict.updateValue(1, forKey: "Donation_Type")
                    }else if value == "Education"{
                        self.dict.updateValue(2, forKey: "Donation_Type")
                    }else if value == "Religious"{
                        self.dict.updateValue(3, forKey: "Donation_Type")
                    }
                }
                
                
            case (0,5):
                textField.keyboardType = .phonePad
                let index = IndexPath(row: 5, section: 0)
                let cell: Add_DonationTableViewCell = self.tableView_Donation.cellForRow(at: index) as! Add_DonationTableViewCell
                
                if textField == cell.txt_Code{
                    let countryView = CountrySelectView.shared
                    countryView.barTintColor = .red
                    countryView.displayLanguage = .english
                    countryView.show()
                    countryView.selectedCountryCallBack = { (countryDic) -> Void in
                        
                        
                        
                        cell.txt_Code.text = "+\(countryDic["code"] as! NSNumber)"
                        
                        self.dict.updateValue(cell.txt_Code.text!, forKey: "Code")
                        
                    }
                }else if textField == cell.txt_Title{
                    
                    self.dict.updateValue(cell.txt_Title.text!, forKey: "Phone")
                    
                }
                
            case (0,6):
                textField.keyboardType = .emailAddress
                
                
            case (0,7):
                
                let array_Payment = ["Poli","Website"]
                AKMultiPicker().openPickerIn(textField, firstComponentArray: array_Payment) { (value, _, index1, _) in
                    textField.text = value
                    
                    if value == "Website"{
                        self.str_webSite = "Website"
                        self.myArrayDonation.removeAll()
                        self.btn_AddMore.isHidden = true
                        self.donation_Array = ["Title","Description","Tax_Deductable","Start_Date","Donation_Type","Phone","Email","Payment_Gateway","WebSite"]
                        self.dict.updateValue(2, forKey: "Payment_Gateway")
                        self.tableView_Donation.reloadData()
                    }else{
                        self.str_webSite = "Poli"
                        self.payement_array = [1]
                        self.btn_AddMore.isHidden = false
                        self.donation_Array = ["Title","Description","Tax_Deductable","Start_Date","Donation_Type","Phone","Email","Payment_Gateway","defind"]
                        
                        if self.myArrayDonation.count == 0{
                            self.dict_Donation = ["amount": 1.0, "description": ""]
                            self.myArrayDonation.append(self.dict_Donation)
                        }
                        
                        self.dict.updateValue(1, forKey: "defind")
                        self.dict.updateValue(1, forKey: "Payment_Gateway")
                    }
                    self.tableView_Donation.reloadData()
                }
                
            case (0,8):
                
                if self.str_webSite == "Website"{
                    self.dict.updateValue(textField.text!, forKey: "WebSite_URL")
                }
            default:
                break
            }
            
        }
        
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        guard let indexPath = textField.tableViewIndexPath(tableView_Donation) else { return }
        
        if taxDeductable == false{
            switch (indexPath.section, indexPath.row) {
                
            case (0,0):
                
                self.dict.updateValue(textField.text as Any, forKey: donation_Array[0]) // Title
            case (0,1):
                
                self.dict.updateValue(textField.text as Any, forKey: donation_Array[1]) // Description
                
            case (0,2):
                
                self.dict.updateValue(textField.text as Any, forKey: "Start_Date") // Start_Date
                self.dict.updateValue(textField.text as Any, forKey: "End_Date") // End_Date
                
                
            case (0,4):
                let index = IndexPath(row: 4, section: 0)
                let cell: Add_DonationTableViewCell = self.tableView_Donation.cellForRow(at: index) as! Add_DonationTableViewCell
                
                if textField == cell.txt_Title{
                var Phone = textField.text ?? ""
                while (Phone.hasPrefix("0")) {
                    Phone.remove(at: (Phone.startIndex))
                }
                
                if (Phone.count > 10){
                    AKAlertController.alert("", message: "Please Enter Valid Mobile Number")
                }else{
                    self.dict.updateValue(Phone as Any, forKey: "Phone") // Phone
                }
                }else if textField == cell.txt_Code{
                    self.dict.updateValue(textField.text as Any, forKey: "Code")
                }
                
                
            case (0,5):
                
                if ((textField.text?.validateEmail())!) == false{
                    AKAlertController.alert("", message: "Please Enter Valid Email Id")
                }else{
                    self.dict.updateValue(textField.text as Any, forKey: "Email") // Email
                }
            case (0,7):
                
                if self.str_webSite == "Website"{
                    self.dict.updateValue(textField.text!, forKey: "WebSite_URL")
                }
            case (1,indexPath.row):
                let index = IndexPath(row: textField.tag, section: 1)
                let cell: DonationAmountTableViewCell = self.tableView_Donation.cellForRow(at: index) as! DonationAmountTableViewCell
                
                let amount = Double(cell.btn_DonationAmount.text ?? "1.0")
                
                if (amount ?? 1.0 < 1.0){
                    cell.btn_DonationAmount.text = "1.0"

                    myArrayDonation[indexPath.row].updateValue(Double(cell.btn_DonationAmount.text!) ?? 0.0, forKey: "amount")
                    AKAlertController.alert("amount should be greater or equal to $1")
                }else{
                    
                    
                    if (cell.btn_DonationDescription.text != "" || cell.btn_DonationAmount.text != ""){
                        dict_Donation = ["amount": Int(cell.btn_DonationAmount.text!) ?? 0, "description": cell.btn_DonationDescription.text!]
                        
                        
                        if myArrayDonation.count > indexPath.row{
                            myArrayDonation[indexPath.row].updateValue(Double(cell.btn_DonationAmount.text!) ?? 0, forKey: "amount")
                            myArrayDonation[indexPath.row].updateValue(cell.btn_DonationDescription.text!, forKey: "description")
                            
                            print("\(myArrayDonation)")
                        }else{
                            myArrayDonation.append(dict_Donation)
                            print("\(myArrayDonation)")
                        }
                    }
                }
                
            default:
                break
            }
            
        }else{
            
            switch (indexPath.section, indexPath.row) {
                
            case (0,0):
                
                self.dict.updateValue(textField.text as Any, forKey: donation_Array[0]) // Title
            case (0,1):
                
                self.dict.updateValue(textField.text as Any, forKey: donation_Array[1]) // Description
                
            case (0,3):
                
                self.dict.updateValue(textField.text as Any, forKey: "Start_Date") // Start_Date
                self.dict.updateValue(textField.text as Any, forKey: "End_Date") // End_Date
                
                
            case (0,5):
                let index = IndexPath(row: 5, section: 0)
                              let cell: Add_DonationTableViewCell = self.tableView_Donation.cellForRow(at: index) as! Add_DonationTableViewCell
                              
                              if textField == cell.txt_Title{
                var Phone = textField.text ?? ""
                while (Phone.hasPrefix("0")) {
                    Phone.remove(at: (Phone.startIndex))
                }
                
                if (Phone.count > 10){
                    AKAlertController.alert("", message: "Please Enter Valid Admin Mobile Number")
                }else{
                    self.dict.updateValue(Phone as Any, forKey: "Phone") // Phone
                }
                              }else if textField == cell.txt_Code{
                                
                                self.dict.updateValue(textField.text, forKey: "Code")
                }
                
            case (0,6):
                
                if ((textField.text?.validateEmail())!) == false{
                    AKAlertController.alert("", message: "Please Enter Valid Email Id")
                }else{
                    self.dict.updateValue(textField.text as Any, forKey: donation_Array[6]) // Email
                }
                
            case (0,8):
                if self.str_webSite == "Website"{
                    self.dict.updateValue(textField.text!, forKey: "WebSite_URL")
                }
                
            case (1,indexPath.row):
                let index = IndexPath(row: textField.tag, section: 1)
                let cell: DonationAmountTableViewCell = self.tableView_Donation.cellForRow(at: index) as! DonationAmountTableViewCell
                
                
                if (cell.btn_DonationDescription.text != "" || cell.btn_DonationAmount.text != ""){
                    dict_Donation = ["amount": Int(cell.btn_DonationAmount.text!) ?? 0, "description": cell.btn_DonationDescription.text!]
                    
                    let amount = Double(cell.btn_DonationAmount.text ?? "1.0")
                    
                    if (amount ?? 1.0 < 1.0){
                        
                        cell.btn_DonationAmount.text = "1.0"

                                           myArrayDonation[indexPath.row].updateValue(Double(cell.btn_DonationAmount.text!) ?? 0.0, forKey: "amount")
                        AKAlertController.alert("amount should be greater or equal to $1")
                    }else{
                        
                        
                        
                        
                        if myArrayDonation.count > indexPath.row{
                            myArrayDonation[indexPath.row].updateValue(Double(cell.btn_DonationAmount.text!) ?? 0, forKey: "amount")
                            myArrayDonation[indexPath.row].updateValue(cell.btn_DonationDescription.text!, forKey: "description")
                            
                            print("\(myArrayDonation)")
                        }else{
                            myArrayDonation.append(dict_Donation)
                            print("\(myArrayDonation)")
                        }
                    }
                }else{
                    cell.btn_DonationAmount.text = "1.0"

                                                             myArrayDonation[indexPath.row].updateValue(Double(cell.btn_DonationAmount.text!) ?? 0.0, forKey: "amount")
                      AKAlertController.alert("Please Enter amount")
                    return
                }
                
            default:
                break
            }
        }
    }
    
    //MARK:IBACtion------------------------------------------------------
    
    @IBAction func btn_Back(sender:UIButton){
        
        if addNewDonation == "addNewDonationFromOrg"{
            self.presentingViewController?.presentingViewController?.dismiss(animated: true, completion: nil)
        }else{
            self.dismiss(animated: true, completion: nil)
        }
        
    }
    @IBAction func btn_Save(sender:UIButton){
        if dict.isEmpty == true{
            
            AKAlertController.alert("Please Fill Form")
            return
        }else{
            
            if addNewDonation != "addNewDonationFromOrg"{
                EditDonation()
            }else{
                self.AddDonationApi(imageData: imageData)
            }
            
            
        }
        print(dict)
        print(self.myArrayDonation)
    }
    @objc func btn_TaxDeductable(sender: UIButton) {
        if sender.tag == 0{
            dict.updateValue(true, forKey: donation_Array[2])
        }else if sender.tag == 1{
            
            dict.updateValue(false, forKey: donation_Array[2])
        }
        
        self.tableView_Donation.reloadRows(at: [IndexPath(row: 2, section: 0)], with: UITableView.RowAnimation.none)
    }
    
    @IBAction func btn_Cancel(sender: UIButton) {
        
        self.presentingViewController?.dismiss(animated: true, completion: nil)
        
    }
    
    @objc func btn_PreDefineOrUserDefine(sender: UIButton) {
        if sender.tag == 2{
            if self.donationData != nil{
                let amount = self.donationData.pre_dif_amount_list!.map({ $0.amount})
                let description = self.donationData.pre_dif_amount_list!.map({ $0.preDIFAmountListDescription})
                
                self.myArrayDonation = []
                
                for i in 0...amount.count - 1{
                    
                    let dic = ["amount": amount[i], "description": description[i]] as [String : Any]
                    self.myArrayDonation.append(dic)
                }
            }else{
                
                dict_Donation = ["amount": 1.0, "description": ""]
                self.myArrayDonation.append(dict_Donation)
            }
            
            
            self.btn_AddMore.isHidden = false
            dict.updateValue(1, forKey: "defind")
        }else if sender.tag == 3{
            self.myArrayDonation.removeAll()
            self.btn_AddMore.isHidden = true
            dict.updateValue(2, forKey: "defind")
        }
        self.tableView_Donation.reloadData()
        
        
    }
}




//MARK:TableView Delegate And DataSources--------------------------------------------------------
extension Add_DonationViewController:UITableViewDelegate,UITableViewDataSource{
    
    
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 1:
            
            
            return myArrayDonation.count
            
        default:
            return donation_Array.count
        }
        
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        switch indexPath.section {
        case 0:
            let cell = tableView.dequeueReusableCell(withIdentifier: "Add_DonationTableViewCell") as! Add_DonationTableViewCell
            cell.txt_Title.delegate = self
            cell.txt_Code.delegate = self
            cell.view_1.isHidden = true
            cell.view_2.isHidden = true
            cell.view_3.isHidden = true
            cell.view_4.isHidden = true
            cell.view_5.isHidden = true
            cell.view_6.isHidden = true
            cell.txt_Code.isHidden = true
            cell.btn_No.tag = 1
            cell.btn_Yes.tag = 0
            cell.btn_PreDefined.tag = 2
            cell.btn_UserDefined.tag = 3
            cell.btn_PreDefined.addTarget(self, action: #selector(btn_PreDefineOrUserDefine(sender:)), for: .touchUpInside)
            cell.btn_UserDefined.addTarget(self, action: #selector(btn_PreDefineOrUserDefine(sender:)), for: .touchUpInside)
            
            
            if taxDeductable == false{
                switch indexPath.row{
                case 0:
                    
                    cell.view_1.isHidden = false
                    cell.txt_Title.text = dict["Title"] as? String
                    
                case 1:
                    cell.view_2.isHidden = false
                    cell.txtView_Description.text = dict["Description"] as? String
                    
                    if cell.txtView_Description.text == "Description"{
                        cell.txtView_Description.textColor = UIColor.gray
                      
                        
                    }else{
                        cell.txtView_Description.textColor = UIColor.black
                        
                        
                    }
                    
                    
                    
                    
                case 2:
                    cell.view_4.isHidden = false
                    if let Start_Date = dict["Start_Date"] as? String  {
                        
                        cell.txt_StartDate.text = Start_Date
                    }
                    if let End_Date = dict["End_Date"] as? String  {
                        cell.txt_EndDate.text = End_Date
                    }
                    
                case 3:
                    
                    cell.view_3.isHidden = false
                    if let donationType = dict["Donation_Type"] as? Int  {
                        if donationType == 1{
                            
                            
                            cell.txt_RepeatingDay.text = "Medical"
                        }else if donationType == 2{
                            cell.txt_RepeatingDay.text = "Education"
                        }else if donationType == 3{
                            cell.txt_RepeatingDay.text = "Religious"
                        }  }else{
                        cell.txt_RepeatingDay.placeholder = "Donation Type"
                    }
                case 4:
                    cell.txt_Code.isHidden = false
                    
                    
                    cell.view_1.isHidden = false
                    
                    if let donationType = dict["Phone"] as? String{
                        if donationType != ""{
                            cell.txt_Title.text = donationType
                                                  
                        }else{
                            cell.txt_Title.placeholder = "Phone no"
                        }
                        
                    }else{
                        cell.txt_Title.placeholder = "Phone no"
                    }
                    
                    if let code = dict["Code"] as? String{
                        cell.txt_Code.text = code
                        
                    }
                    
                    
                case 5:
                    cell.txt_Title.placeholder = "Email"
                    cell.view_1.isHidden = false
                    if let email = dict["Email"] as? String{
                        cell.txt_Title.text = email
                        
                    }else{
                        cell.txt_Title.placeholder = "Email"
                    }
                case 6:
                    
                    
                    cell.view_3.isHidden = false
                    if let donationType = dict["Payment_Gateway"] as? Int  {
                        if donationType == 2{
                            
                            
                            cell.txt_RepeatingDay.text = "Website"
                        }else if donationType == 1{
                            cell.txt_RepeatingDay.text = "Poli"
                        } }else{
                        cell.txt_RepeatingDay.placeholder = "Select Payment Gateway"
                    }
                case 7:
                    if self.str_webSite == "Website"{
                        
                        cell.txt_Title.placeholder = "URL"
                        cell.view_6.isHidden = true
                        cell.view_1.isHidden = false
                        
                        if let website = dict["WebSite_URL"] as? String {
                            
                            cell.txt_Title.text = website
                        }
                    }else{
                        cell.view_6.isHidden = false
                    }
                    
                    
                default:
                    break
                }
                
            }else{
                switch indexPath.row{
                case 0:
                    
                    cell.view_1.isHidden = false
                    cell.txt_Title.text = dict["Title"] as? String
                    
                case 1:
                    cell.view_2.isHidden = false
                    cell.txtView_Description.text = dict["Description"] as? String
                    
                    if cell.txtView_Description.text == "Description"{
                        cell.txtView_Description.textColor = UIColor.gray
                      
                    }else{
                        cell.txtView_Description.textColor = UIColor.black
                        
                        
                    }
                    
                    
                case 2:
                    cell.view_5.isHidden = false
                    
                case 3:
                    cell.view_4.isHidden = false
                    if let Start_Date = dict["Start_Date"] as? String  {
                        
                        cell.txt_StartDate.text = Start_Date
                    }
                    if let End_Date = dict["End_Date"] as? String  {
                        cell.txt_EndDate.text = End_Date
                    }
                    
                case 4:
                    
                    cell.view_3.isHidden = false
                    if let donationType = dict["Donation_Type"] as? Int  {
                        if donationType == 1{
                            
                            
                            cell.txt_RepeatingDay.text = "Medical"
                        }else if donationType == 2{
                            cell.txt_RepeatingDay.text = "Education"
                        }else if donationType == 3{
                            cell.txt_RepeatingDay.text = "Religious"
                        }  }else{
                        cell.txt_RepeatingDay.placeholder = "Donation Type"
                    }
                case 5:
                    cell.txt_Code.isHidden = false
                    cell.txt_Title.placeholder = "Phone no"
                    
                    cell.view_1.isHidden = false
                    
                    if let donationType = dict["Phone"] as? String{
                        cell.txt_Title.text = donationType
                        
                    }else{
                        cell.txt_Title.placeholder = "Phone no"
                    }
                    
                    if let code = dict["Code"] as? String{
                        cell.txt_Code.text = code
                        
                    }
                    
                    
                case 6:
                    cell.txt_Title.placeholder = "Email"
                    cell.view_1.isHidden = false
                    if let email = dict["Email"] as? String{
                        cell.txt_Title.text = email
                        
                    }else{
                        cell.txt_Title.placeholder = "Email"
                    }
                case 7:
                    
                    
                    cell.view_3.isHidden = false
                    if let donationType = dict["Payment_Gateway"] as? Int  {
                        if donationType == 2{
                            
                            
                            cell.txt_RepeatingDay.text = "Website"
                        }else if donationType == 1{
                            cell.txt_RepeatingDay.text = "Poli"
                        } }else{
                        cell.txt_RepeatingDay.placeholder = "Select Payment Gateway"
                    }
                case 8:
                    if self.str_webSite == "Website"{
                        
                        cell.txt_Title.placeholder = "URL"
                        cell.view_6.isHidden = true
                        cell.view_1.isHidden = false
                        
                        if let website = dict["WebSite_URL"] as? String {
                            
                            cell.txt_Title.text = website
                        }
                    }else{
                        cell.view_6.isHidden = false
                    }
                case 9:
                    cell.view_6.isHidden = false
                    
                    
                default:
                    break
                }
                
            }
            
            
            //mark: button txDeductabl-----------------
            if let tax = dict[donation_Array[2]] as? Bool{
                
                if tax == true{
                    cell.btn_Yes.backgroundColor = UIColor.init(hex: 0x0DC9E4)
                    cell.btn_No.backgroundColor = UIColor.white
                    cell.btn_No.setTitleColor(UIColor.darkGray, for: .normal)
                    cell.btn_Yes.setTitleColor(UIColor.white, for: .normal)
                }else{
                    cell.btn_Yes.backgroundColor = UIColor.white
                    cell.btn_No.backgroundColor = UIColor.init(hex: 0x0DC9E4)
                    cell.btn_Yes.setTitleColor(UIColor.darkGray, for: .normal)
                    cell.btn_No.setTitleColor(UIColor.white, for: .normal)
                }
            }
            
            //mark: button preDefined and Userdefined----------
            if let deinfed = dict["defind"] as? Int{
                
                if deinfed == 1{
                    
                    cell.btn_PreDefined.setImage(#imageLiteral(resourceName: "radio_selected_iPhone"), for: .normal)
                    cell.btn_UserDefined.setImage(#imageLiteral(resourceName: "radio_unselected_iPhone"), for: .normal)
                }else{
                    cell.btn_UserDefined.setImage(#imageLiteral(resourceName: "radio_selected_iPhone"), for: .normal)
                    cell.btn_PreDefined.setImage(#imageLiteral(resourceName: "radio_unselected_iPhone"), for: .normal)
                }
            }
            
            cell.btn_No.addTarget(self, action: #selector(btn_TaxDeductable(sender:)), for: .touchUpInside)
            cell.btn_Yes.addTarget(self, action: #selector(btn_TaxDeductable(sender:)), for: .touchUpInside)
            
            
            
            return cell
        case 1:
            let cell = tableView.dequeueReusableCell(withIdentifier: "DonationAmountTableViewCell") as! DonationAmountTableViewCell
            if indexPath.row == 0{
                cell.btn_Delete.isHidden = true
                
            }else{
                cell.btn_Delete.isHidden = false
                cell.btn_Delete.tag = indexPath.row
                cell.btn_Delete.addTarget(self, action: #selector(btn_Deletee(sender:)), for: .touchUpInside)
            }
            cell.btn_DonationDescription.delegate = self
            cell.btn_DonationAmount.delegate = self
            cell.btn_DonationDescription.tag = indexPath.row
            cell.btn_DonationAmount.tag = indexPath.row
            
            
            let dic = self.myArrayDonation[indexPath.row]
            print(dic)
            cell.btn_DonationAmount.text = "\(dic["amount"] as? Double ?? 1.0)"
            cell.btn_DonationDescription.text = (dic["description"] as! String)
            
            
            
            
            return cell
            
        default:
            return UITableViewCell()
        }
        
        
    }
    
    
    //MArk:api functions-------------
    //MARK:Function Api----------------------------------------
    func AddDonationApi(imageData : [Data]){
        
        
        if dict["Title"] as? String  == ""{
            
            AKAlertController.alert("Please enter title")
        }
        else if dict["Description"] as? String  == ""{
            
          AKAlertController.alert("Please enter description")
            
        }
            else if dict["Description"] as? String  == "Description"{
                
              AKAlertController.alert("Please enter description")
                
            }
        else if dict["Tax_Deductable"] as? String  == ""{
            
            AKAlertController.alert("Please select tax deductable")
        }
        else if dict["Start_Date"] as? String  == ""{
            
            AKAlertController.alert("Please enter Start Date")
        }
            
        else if dict["Donation_Type"] as? String  == ""{
            
            AKAlertController.alert("Please enter donation type")
        }
        else if dict["Phone"] as? String  == ""{
            
            AKAlertController.alert("Please enter phone number")
        }
        else if dict["Email"] as? String  == ""{
            
            AKAlertController.alert("Please enter email")
        }
        else if dict["Payment_Gateway"] as? String  == ""{
            
            AKAlertController.alert("Please enter payment gateway")
        }
            
        else if dict["defind"] as? String  == ""{
            
            AKAlertController.alert("Please enter defind")
        }
            
        else{
            let amount_type = dict["Payment_Gateway"] as! Int
            var para = [String:Any]()
            let code = dict["Code"] as? String
            let phone = dict["Phone"] as? String
            let full_PhoneNumber = code! + "-" + phone!
            
            
            if amount_type == 2{
                
                if dict["WebSite_URL"] as? String  == ""{
                    
                    self.showSingleAlertMessage(message: "", subMsg: "Please Enter Website" , sender: self, completion:
                        { (success) -> Void in
                            
                    })
                    
                }else{
                    
                    
                    para   = ["org": orgId,
                              "title": dict["Title"] as! String,
                              "description": dict["Description"] as! String,
                              "tax_deductable":dict["Tax_Deductable"] as! Bool,
                              "start_date":dict["Start_Date"] as! String,
                              "end_date":dict["End_Date"] as! String,
                              "phone_number":full_PhoneNumber,
                              "email":dict["Email"] as! String,
                              "donation_type":dict["Donation_Type"] as! Int,
                              "payment_gateway":dict["Payment_Gateway"] as! Int,
                              "website":dict["WebSite_URL"] as! String]
                    print(para)
                    KVNProgress.show()
                    Network.shared.requestWithMultipleForDonation(imageData: imageData, image1: "", image2: "",image3: "",image4: "",image5: "",image6: "",parameters: para) { (result) in
                        KVNProgress.dismiss()
                        if result?.success == true{
                            self.showSingleAlertMessage(message: "", subMsg: result?.message ?? "upload successfully", sender: self, completion:
                                { (success) -> Void in
                                    if success == true{
                                        self.presentingViewController?.presentingViewController?.dismiss(animated: true, completion: nil)
                                    }else{
                                        self.showSingleAlertMessage(message: "", subMsg: result?.message ?? "", sender: self, completion:
                                            { (success) -> Void in
                                        })
                                    }
                                    
                            }
                            )
                        }else{
                            self.showSingleAlertMessage(message: "", subMsg: result?.message ?? "", sender: self, completion:
                                { (success) -> Void in
                            })
                            
                        }
                    }
                }
                
            }else{
                
                let code = dict["Code"] as? String
                let phone = dict["Phone"] as? String
                let full_PhoneNumber = code! + "-" + phone!
                
                if (dict["defind"] as! Int == 1){
                    para   = ["org": orgId,
                              "title": dict["Title"] as! String,
                              "description": dict["Description"] as! String,
                              "tax_deductable":dict["Tax_Deductable"] as! Bool,
                              "start_date":dict["Start_Date"] as! String,
                              "end_date":dict["End_Date"] as! String,
                              "phone_number":full_PhoneNumber,
                              "email":dict["Email"] as! String,
                              "donation_type":dict["Donation_Type"] as! Int,
                              "payment_gateway":dict["Payment_Gateway"] as! Int,
                              "amount_type":dict["defind"] as! Int,"website":dict["WebSite_URL"] as? String ?? "","pre_dif_amount_list":myArrayDonation]
                }else{
                    para   = ["org": orgId,
                              "title": dict["Title"] as! String,
                              "description": dict["Description"] as! String,
                              "tax_deductable":dict["Tax_Deductable"] as! Bool,
                              "start_date":dict["Start_Date"] as! String,
                              "end_date":dict["End_Date"] as! String,
                              "phone_number":full_PhoneNumber,
                              "email":dict["Email"] as! String,
                              "donation_type":dict["Donation_Type"] as! Int,
                              "payment_gateway":dict["Payment_Gateway"] as! Int,
                              "amount_type":dict["defind"] as! Int,"website":dict["WebSite_URL"] as? String ?? ""]
                }
                
                
                print(para)
                KVNProgress.show()
                Network.shared.requestWithMultipleForDonation(imageData: imageData, image1: "", image2: "",image3: "",image4: "",image5: "",image6: "",parameters: para) { (result) in
                    KVNProgress.dismiss()
                    if result?.success == true{
                        self.showSingleAlertMessage(message: "", subMsg: result?.message ?? "upload successfully", sender: self, completion:
                            { (success) -> Void in
                                if success == true{
                                    self.presentingViewController?.presentingViewController?.dismiss(animated: true, completion: nil)
                                }else{
                                    self.showSingleAlertMessage(message: "", subMsg: result?.message ?? "", sender: self, completion:
                                        { (success) -> Void in
                                    })
                                }
                                
                        }
                        )
                    }else{
                        self.showSingleAlertMessage(message: "", subMsg: result?.message ?? "", sender: self, completion:
                            { (success) -> Void in
                        })
                        
                    }
                }
                
            }
            
            
            
        }
    }
    
    func EditDonation(){
        
        
        if dict["Title"] as? String  == ""{
            
            AKAlertController.alert("Please enter title")
        }
        else if dict["Description"] as? String  == ""{
            
            AKAlertController.alert("Please enter description")
        }
        else if dict["Tax_Deductable"] as? String  == ""{
            
            AKAlertController.alert("Please select tax deductable")
        }
        else if dict["Start_Date"] as? String  == ""{
            
            AKAlertController.alert("Please enter Start Date")
        }
            
        else if dict["Donation_Type"] as? String  == ""{
            
            AKAlertController.alert("Please enter donation type")
        }
        else if dict["Phone"] as? String  == ""{
            
            AKAlertController.alert("Please enter phone number")
        }
        else if dict["Email"] as? String  == ""{
            
            AKAlertController.alert("Please enter email")
        }
        else if dict["Payment_Gateway"] as? String  == ""{
            
            AKAlertController.alert("Please enter payment gateway")
        }
            
        else if dict["defind"] as? String  == ""{
            
            AKAlertController.alert("Please enter defind")
        }
            
        else{
            let amount_type = dict["Payment_Gateway"] as! Int
            var para = [String:Any]()
            let code = dict["Code"] as? String
            let phone = dict["Phone"] as? String
            let full_PhoneNumber = code! + "-" + phone!
            
            
            if amount_type == 2{
                
                if dict["WebSite_URL"] as? String  == ""{
                    
                    self.showSingleAlertMessage(message: "", subMsg: "Please Enter Website" , sender: self, completion:
                        { (success) -> Void in
                            
                    })
                    
                }else{
                    
                    
                    para   = ["id":self.donationData.id!,"org": orgId,
                              "title": dict["Title"] as! String,
                              "description": dict["Description"] as! String,
                              "tax_deductable":dict["Tax_Deductable"] as! Bool,
                              "start_date":dict["Start_Date"] as! String,
                              "end_date":dict["End_Date"] as! String,
                              "phone_number":full_PhoneNumber,
                              "email":dict["Email"] as! String,
                              "donation_type":dict["Donation_Type"] as! Int,
                              "payment_gateway":dict["Payment_Gateway"] as! Int,
                              "website":dict["WebSite_URL"] as! String]
                    print(para)
                    KVNProgress.show()
                    Network.shared.editForDonation(parameters: para){ (result) in
                        KVNProgress.dismiss()
                        if result?.success == true{
                            self.showSingleAlertMessage(message: "", subMsg: result?.message ?? "upload successfully", sender: self, completion:
                                { (success) -> Void in
                                    if success == true{
                                        self.presentingViewController?.dismiss(animated: true, completion: nil)
                                    }else{
                                        self.showSingleAlertMessage(message: "", subMsg: result?.message ?? "", sender: self, completion:
                                            { (success) -> Void in
                                        })
                                    }
                                    
                            }
                            )
                        }else{
                            self.showSingleAlertMessage(message: "", subMsg: result?.message ?? "", sender: self, completion:
                                                                   { (success) -> Void in
                                                               })
                        }
                    }
                }
                
            }else{
                
                let code = dict["Code"] as? String
                let phone = dict["Phone"] as? String
                let full_PhoneNumber = code! + "-" + phone!
                
                if (dict["defind"] as! Int == 1){
                    para   = ["id":self.donationData.id!,"org": orgId,
                              "title": dict["Title"] as! String,
                              "description": dict["Description"] as! String,
                              "tax_deductable":dict["Tax_Deductable"] as! Bool,
                              "start_date":dict["Start_Date"] as! String,
                              "end_date":dict["End_Date"] as! String,
                              "phone_number":full_PhoneNumber,
                              "email":dict["Email"] as! String,
                              "donation_type":dict["Donation_Type"] as! Int,
                              "payment_gateway":dict["Payment_Gateway"] as! Int,
                              "amount_type":dict["defind"] as! Int,"website":dict["WebSite_URL"] as? String ?? "","pre_dif_amount_list":myArrayDonation]
                }else{
                    para   = ["id":self.donationData.id!,"org": orgId,
                              "title": dict["Title"] as! String,
                              "description": dict["Description"] as! String,
                              "tax_deductable":dict["Tax_Deductable"] as! Bool,
                              "start_date":dict["Start_Date"] as! String,
                              "end_date":dict["End_Date"] as! String,
                              "phone_number":full_PhoneNumber,
                              "email":dict["Email"] as! String,
                              "donation_type":dict["Donation_Type"] as! Int,
                              "payment_gateway":dict["Payment_Gateway"] as! Int,
                              "amount_type":dict["defind"] as! Int,"website":dict["WebSite_URL"] as? String ?? ""]
                }
                
                
                print(para)
                KVNProgress.show()
                Network.shared.editForDonation(parameters: para){ (result) in
                    KVNProgress.dismiss()
                    if result?.success == true{
                        self.showSingleAlertMessage(message: "", subMsg: result?.message ?? "upload successfully", sender: self, completion:
                            { (success) -> Void in
                                if success == true{
                                    self.presentingViewController?.dismiss(animated: true, completion: nil)
                                }else{
                                    self.showSingleAlertMessage(message: "", subMsg: result?.message ?? "", sender: self, completion:
                                        { (success) -> Void in
                                    })
                                }
                                
                        }
                        )
                    }else{
                        self.showSingleAlertMessage(message: "", subMsg: result?.message ?? "", sender: self, completion:
                                                               { (success) -> Void in
                                                           })
                    }
                }
                
            }
            
            
            
        }
    }
    
}


extension Add_DonationViewController : YMSPhotoPickerViewControllerDelegate{
    
    // MARK: - YMSPhotoPickerViewControllerDelegate
    
    func photoPickerViewControllerDidReceivePhotoAlbumAccessDenied(_ picker: YMSPhotoPickerViewController!) {
        let alertController = UIAlertController.init(title: "Allow photo album access?", message: "Need your permission to access photo albumbs", preferredStyle: .alert)
        let dismissAction = UIAlertAction.init(title: "Cancel", style: .cancel, handler: nil)
        let settingsAction = UIAlertAction.init(title: "Settings", style: .default) { (action) in
            
            UIApplication.shared.openURL(URL.init(string: UIApplication.openSettingsURLString)!)
        }
        alertController.addAction(dismissAction)
        alertController.addAction(settingsAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
    func photoPickerViewControllerDidReceiveCameraAccessDenied(_ picker: YMSPhotoPickerViewController!) {
        let alertController = UIAlertController.init(title: "Allow camera album access?", message: "Need your permission to take a photo", preferredStyle: .alert)
        let dismissAction = UIAlertAction.init(title: "Cancel", style: .cancel, handler: nil)
        let settingsAction = UIAlertAction.init(title: "Settings", style: .default) { (action) in
            UIApplication.shared.openURL(URL.init(string: UIApplication.openSettingsURLString)!)
        }
        alertController.addAction(dismissAction)
        alertController.addAction(settingsAction)
        picker.present(alertController, animated: true, completion: nil)
    }
    func photoPickerViewController(_ picker: YMSPhotoPickerViewController!, didFinishPicking image: UIImage!) {
        picker.dismiss(animated: true) {
        }
    }
    
    func photoPickerViewController(_ picker: YMSPhotoPickerViewController!, didFinishPickingImages photoAssets: [PHAsset]!) {
        
        picker.dismiss(animated: true) {
            let imageManager = PHImageManager.init()
            let options = PHImageRequestOptions.init()
            options.deliveryMode = .highQualityFormat
            options.resizeMode = .none
            options.isSynchronous = true
            
            
            if photoAssets.count + self.arrayImages.count > 6{
                
                AKAlertController.alert("You can't select more than 6 images")
            }
                
            else{
                for asset: PHAsset in photoAssets
                {
                    let targetSize = CGSize(width: UIScreen.main.bounds.size.width, height: UIScreen.main.bounds.size.width)
                    imageManager.requestImage(for: asset, targetSize: targetSize, contentMode: .default, options: options, resultHandler: { (image, info) in
                    
                            self.arrayImages.append(image!)
                        
                        self.collectionView_Images.isHidden = false
                        self.imageData.append(image!.jpegData(compressionQuality: 0.7)!)
                     
                        //self.imageData.append(image!.pngData()!)
                        //                    self.imagesCollectionviewTopConstraint.constant = 0
                    })
                }
            }
            if self.arrayImages.count > 0{
                self.lbl_NoImgae.isHidden = true
                self.collectionView_Images.isHidden = false
                self.view_UploadPhoto.isHidden = true
                
            }else{
                self.lbl_NoImgae.isHidden = false
                self.collectionView_Images.isHidden = true
                self.view_UploadPhoto.isHidden = false
            }
            self.collectionView_Images.reloadData()
        }
        
        
    }
    
    
}
extension Add_DonationViewController:UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout{
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        
        return 2
    }
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        //        if section == 0{
        //            return 1
        //        }else{
        //            return arrayImages.count
        //        }
        if section == 0{
            if self.addNewDonation != "addNewDonationFromOrg" {
                return 0
            }else{
                return 1
            }
            
        }else{
            if arrayImages.count > 0{
                if self.arrayImages.count > 6{
                    return 6
                }else{
                    return self.arrayImages.count
                }
            }
            if self.addNewDonation != "addNewDonationFromOrg" {
                
                return donationData.images.count
            }
        }
        
        return 0
    }
    
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if indexPath.section == 0{
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Home_AddMoreCollectionViewCell", for: indexPath) as! Home_AddMoreCollectionViewCell
            cell.btn_Addmore.addTarget(self, action: #selector(btn_AddPhotos(sender:)), for: .touchUpInside)
            return cell
        }else{
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "AddimageAnnouncementCollectionViewCell", for: indexPath) as! AddimageAnnouncementCollectionViewCell
            
            if donationData != nil{
                cell.btn_Delete.isHidden = true
                if donationData.images.count != nil{
                    
                    cell.btn_Delete.isHidden = true
                    cell.img_View.sd_setImage(with: URL(string:
                        (self.donationData.images[indexPath.item])),placeholderImage: UIImage.init(named: "default_header_iPhone.png"), completed: nil)
                }
            }else{
                
                cell.img_View.image = arrayImages[indexPath.row]
                
                cell.btn_Delete.isHidden = false
                cell.btn_Delete.tag = indexPath.item
                cell.btn_Delete.addTarget(self, action: #selector(btn_deltePhot(sender:)), for: .touchUpInside)
                
                
            }
            
            return cell
        }
        
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        if indexPath.section == 0{
            return  CGSize(width: 100 , height: 100)
        }else{
            return CGSize(width: collectionView.bounds.size.width/2 - 20 , height: 160)
            
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        insetForSectionAt section: Int) -> UIEdgeInsets{
        
        if section == 0{
            return  UIEdgeInsets( top: 0,  left: 0, bottom: 0,  right: 0)
        }else{
            return  UIEdgeInsets( top: 0,  left: 0, bottom: 0,  right: 0)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        
        return 5.0
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        
        return 5.0
        
    }
    
    @objc func btn_AddPhotos(sender:UIButton){
        
        if self.arrayImages.count == 6{
            
            AKAlertController.alert("You can't select more than 6 images")
        }
        
        self.view.endEditing(true)
        let pickerViewController = YMSPhotoPickerViewController.init()
        pickerViewController.numberOfPhotoToSelect = 6
        let customColor = UIColor.init(red: 3.0/255.0, green: 68.0/255.0, blue: 114.0/255.0, alpha: 1.0)
        pickerViewController.theme.titleLabelTextColor = UIColor.white
        pickerViewController.theme.navigationBarBackgroundColor = customColor
        pickerViewController.theme.tintColor = UIColor.white
        pickerViewController.theme.orderTintColor = customColor
        pickerViewController.theme.orderLabelTextColor = UIColor.white
        pickerViewController.theme.cameraVeilColor = customColor
        pickerViewController.theme.cameraIconColor = UIColor.white
        pickerViewController.theme.statusBarStyle = .default
        pickerViewController.modalPresentationStyle = .fullScreen
        self.yms_presentCustomAlbumPhotoView(pickerViewController, delegate: self)
    }
    @objc func btn_deltePhot(sender:UIButton){
        let buttonTag = sender.tag
        
        // print("self.arrayImages:22",self.imageArray.count)
        self.arrayImages.remove(at: buttonTag)
        self.imageData.remove(at: buttonTag)
        
        // print("self.arrayImages:33",self.imageArray.count)
        self.collectionView_Images.reloadData()
        
    }
    @IBAction func photoLibraryTapped(_ sender: Any) {
        self.view.endEditing(true)
        let pickerViewController = YMSPhotoPickerViewController.init()
        pickerViewController.numberOfPhotoToSelect = 6
        let customColor = UIColor.init(red: 3.0/255.0, green: 68.0/255.0, blue: 114.0/255.0, alpha: 1.0)
        pickerViewController.theme.titleLabelTextColor = UIColor.white
        pickerViewController.theme.navigationBarBackgroundColor = customColor
        pickerViewController.theme.tintColor = UIColor.white
        pickerViewController.theme.orderTintColor = customColor
        pickerViewController.theme.orderLabelTextColor = UIColor.white
        pickerViewController.theme.cameraVeilColor = customColor
        pickerViewController.theme.cameraIconColor = UIColor.white
        pickerViewController.theme.statusBarStyle = .default
        pickerViewController.modalPresentationStyle = .fullScreen
        self.yms_presentCustomAlbumPhotoView(pickerViewController, delegate: self)
    }
    
    @IBAction func btn_AddMore(sender:UIButton){
        
        
        dict_Donation = ["amount": 1.0, "description": ""]
        
        self.myArrayDonation.append(dict_Donation)
        
        tableView_Donation.reloadSections([1], with: .none)
        
        
        
        
        
        
    }
    
    @objc func btn_Deletee(sender:UIButton){
        
        
        self.myArrayDonation.remove(at: sender.tag - 1)
        self.tableView_Donation.deleteRows(at: [IndexPath(row: sender.tag, section: 1)], with: UITableView.RowAnimation.automatic)
        //  print("remaning donation",self.payement_array)
        // print("remaning donation",self.myArrayDonation)
        
        tableView_Donation.layoutIfNeeded()
        tableView_Donation.beginUpdates()
        tableView_Donation.endUpdates()
        
    }
}
extension Add_DonationViewController: MHDescDelegate{
    func getDescription(desc: String) {
        
        self.dict["Description"] = desc
        
        
        self.tableView_Donation.reloadRows(at: [IndexPath(row: 1, section: 0)], with: UITableView.RowAnimation.none)
        
    }
    
    
    func cornerRadiusAndShadow(){
        
        // corner radius for Save
        btn_Save.layer.cornerRadius = 5
        btn_Save.shadowBlackToHeader()
        
        // corner radius for Cancel
        btn_Cancel.layer.cornerRadius = 5
        btn_Cancel.shadowBlackToHeader()
        
        
        
        view_UpperTableView.clipsToBounds = true
        view_UpperTableView.layer.cornerRadius = 6
        view_UpperTableView.layer.maskedCorners =
            [.layerMaxXMinYCorner, .layerMinXMinYCorner]
        
        
        
        view_LowerTableView.layer.cornerRadius = 8
        view_LowerTableView.layer.maskedCorners =  [.layerMaxXMaxYCorner, .layerMinXMaxYCorner]
        
        view_LowerTableView.layer.shadowColor = UIColor.black.cgColor
        view_LowerTableView.layer.shadowOffset = CGSize(width: 0, height: 3)
        view_LowerTableView.layer.shadowOpacity = 0.4
        view_LowerTableView.layer.shadowRadius = 2.0
        
    }
}


