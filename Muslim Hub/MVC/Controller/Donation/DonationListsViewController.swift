//
//  DonationListsViewController.swift
//  Muslim Hub
//
//  Created by sakshi on 05/10/19.
//  Copyright © 2019 OnGraph. All rights reserved.
//

import UIKit
import KVNProgress
import SKPhotoBrowser

class DonationListsViewController: UIViewController {
    
    @IBOutlet weak var tableView_Donation:UITableView!
    var allDonationLists = [DonationData]()
    @IBOutlet weak var btn_All:UIButton!
    @IBOutlet weak var btn_Fav:UIButton!
       @IBOutlet weak var width_FavAll:NSLayoutConstraint!
    var btnSelected = ""
     var imageSelectedView = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        if #available(iOS 13.0, *) {
            statusBarColorChange(color:UIColor.init(hex: 0x0DC9E4))
        } else {
            UIApplication.statusBarBackgroundColor = UIColor.init(hex: 0x0DC9E4)
        }
        
        self.buttoncornerRadiusShadow()
        NotificationCenter.default.addObserver(self, selector: #selector(onReceiveData(_:)), name: NSNotification.Name(rawValue: "DonationReload"), object: nil)
        if let langugae = UserDefaults.standard.value(forKey: Constant.LANGUAGE) as? String{
                  
                  if langugae == "ar"{
                    self.width_FavAll.constant = 150.0
                    
                  }else if langugae == "ms"{
                     self.width_FavAll.constant = 150.0
                    
                  }else{
                    
                     self.width_FavAll.constant = 100.0
                    
            }
        }

        self.btn_Fav.setTitle("FAV".localized, for: .normal)
        self.btn_All.setTitle("ALL".localized, for: .normal)
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        self.donationListApi(keyAll: "")
    }
    
    override func viewWillDisappear(_ animated: Bool) {
          super.viewWillDisappear(animated)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "DonationReload"), object: nil)
    }

    @objc func onReceiveData(_ notification:Notification) {
        tableView_Donation.beginUpdates()
        tableView_Donation.endUpdates()
    }
    //Mark:- IBaction-------------------------------------------
    @IBAction func btn_All(sender:UIButton){
        btnSelected = "All"
        btn_Fav.backgroundColor = UIColor.white
        btn_All.backgroundColor = UIColor.init(hex: 0x0DC9E4)
        btn_All.setTitleColor(UIColor.white, for: .normal)
        btn_Fav.setTitleColor(UIColor.darkGray, for: .normal)
        donationListApi(keyAll:"")
    }
    @IBAction func btn_Fav(sender:UIButton){
        btnSelected = "Fav"
        btn_All.backgroundColor = UIColor.white
        btn_Fav.backgroundColor = UIColor.init(hex: 0x0DC9E4)
        
        btn_Fav.setTitleColor(UIColor.white, for: .normal)
        btn_All.setTitleColor(UIColor.darkGray, for: .normal)
        donationListApi(keyAll:"FAV")
    }
    func buttoncornerRadiusShadow (){
           
           btn_All.cornerRadiusLower(cornerRadius: 8)
           btn_All.clipsToBounds = true
           btn_All.layer.cornerRadius = 8
           btn_All.layer.borderColor = UIColor.init(hex: 0x0DC9E4).cgColor
           btn_All.layer.borderWidth = 1.0
           btn_All.layer.maskedCorners = [.layerMinXMaxYCorner, .layerMinXMinYCorner]
           
           btn_Fav.clipsToBounds = true
           btn_Fav.layer.cornerRadius = 8
           btn_Fav.layer.maskedCorners = [.layerMaxXMaxYCorner, .layerMaxXMinYCorner]
           btn_Fav.layer.borderColor = UIColor.init(hex: 0x0DC9E4).cgColor
           btn_Fav.layer.borderWidth = 1.0
           
       }
    
    //Mark:- function Api---------------------------------------
    func donationListApi(keyAll:String){
        KVNProgress.show()
        Network.shared.donationLists(keyAll: keyAll) { (result) in
            KVNProgress.dismiss()
            guard let user = result else {
                
                return
            }
            
            self.allDonationLists = user.data ?? []
            if self.allDonationLists.count != 0{
                       for i in 0...self.allDonationLists.count - 1{
                           var arraySelectedImage: [Bool] = []
                           if self.allDonationLists[i].images!.count > 0{
                               for j in 0...self.allDonationLists[i].images!.count - 1{
                                   if j == 0{
                                       arraySelectedImage.append(true)
                                   }else{
                                       arraySelectedImage.append(false)
                                   }
                               }
                           }
                           self.allDonationLists[i].selectedImage_Array = arraySelectedImage
                       }
                   }
                   
            self.tableView_Donation.reloadData()
        }
    }
}

extension DonationListsViewController:UITableViewDelegate,UITableViewDataSource,NotSoGoodCellDelegateDonation,IndexImageSendCellDelegateDonation{
    func IndexImageSendCellDelegateClass(cell: DonationListsTableViewCell, selected_imgIndex: Int) {
        self.imageSelectedView = selected_imgIndex
    }
    
    func moreTapped(cell:DonationListsTableViewCell) {
        tableView_Donation.beginUpdates()
           
           tableView_Donation.endUpdates()
        
    }
    
   
      
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.allDonationLists.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
          let cell = tableView.dequeueReusableCell(withIdentifier: "DonationListsTableViewCell", for: indexPath) as! DonationListsTableViewCell
        cell.delegateDonation = self
        if self.allDonationLists.count != 0 {
            cell.configureCell(model: self.allDonationLists[indexPath.row],indexPath:indexPath.row)
        }
        cell.delegateImageSendCellDelegateDonation = self
        
        cell.img_Viewannouncement.tag = indexPath.row
        // self.imageSelectedView = cell.img_Viewannouncement.image
        
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(imageTapped(tapGestureRecognizer:)))
        
        
        cell.img_Viewannouncement.isUserInteractionEnabled = true
        cell.img_Viewannouncement.addGestureRecognizer(tapGestureRecognizer)
           
        //mark:Date
     let now = Date()
    let date = NSDate(timeIntervalSince1970: Double(self.allDonationLists[indexPath.row].addedAt! ))
      cell.lblClassesDay.text = now.timeAgoDisplay(dateCreated:date as Date,timeStamp:self.allDonationLists[indexPath.row].addedAt!)
            
        
        
//        if let langugae = UserDefaults.standard.value(forKey: Constant.LANGUAGE) as? String{
//
//                   if langugae == "ar"{
//
//                    cell.buttonMore.imageEdgeInsets = UIEdgeInsets(top: 95.0, left: 0.0, bottom: 0.0, right: -58.0)
//                    cell.buttonMore.titleEdgeInsets = UIEdgeInsets(top: 0.0, left: 0.0, bottom: 0.0, right: 0.0)
//
//                    cell.buttonMore.contentEdgeInsets = UIEdgeInsets(top: 0.0, left: 0.0, bottom: 0.0, right: 0.0)
//
//                   }else if langugae == "ms"{
//                    cell.buttonMore.imageEdgeInsets = UIEdgeInsets(top: 110.0, left: 0.0, bottom: 0.0, right: -58.0)
//                    cell.buttonMore.titleEdgeInsets = UIEdgeInsets(top: 0.0, left: 0.0, bottom: 0.0, right: 0.0)
//
//                    cell.buttonMore.contentEdgeInsets = UIEdgeInsets(top: 0.0, left: 0.0, bottom: 0.0, right: 0.0)
//
//
//
//                   }else if langugae == "en"{
//                       cell.buttonMore.imageEdgeInsets = UIEdgeInsets(top: 95.0, left: 0.0, bottom: 0.0, right: -58.0)
//                       cell.buttonMore.titleEdgeInsets = UIEdgeInsets(top: 0.0, left: 0.0, bottom: 0.0, right: 0.0)
//
//                       cell.buttonMore.contentEdgeInsets = UIEdgeInsets(top: 0.0, left: 0.0, bottom: 0.0, right: 0.0)
//
//                   }
//               }else{
//
//            cell.buttonMore.imageEdgeInsets = UIEdgeInsets(top: 95.0, left: 0.0, bottom: 0.0, right: -58.0)
//            cell.buttonMore.titleEdgeInsets = UIEdgeInsets(top: 0.0, left: 0.0, bottom: 0.0, right: 0.0)
//
//            cell.buttonMore.contentEdgeInsets = UIEdgeInsets(top: 0.0, left: 0.0, bottom: 0.0, right: 0.0)
//               }
        
        
        return cell
        
    }
    
    @objc func imageTapped(tapGestureRecognizer: UITapGestureRecognizer)
    {
        _ = tapGestureRecognizer.view as! UIImageView
        
        SKPhotoBrowserOptions.displayAction = false
        var images = [SKPhoto]()
        
        for i in 0..<self.allDonationLists[(tapGestureRecognizer.view?.tag)!].images!.count{
            
            let photo = SKPhoto.photoWithImageURL(self.allDonationLists[(tapGestureRecognizer.view?.tag)!].images![i])
            photo.shouldCachePhotoURLImage = false // you can use image cache by true(NSCache)
            images.append(photo)
        }
        
        
        let browser = SKPhotoBrowser(originImage: #imageLiteral(resourceName: "default_header_iPhone"), photos: images, animatedFromView: self.view)
        
        browser.initializePageIndex(imageSelectedView)
        present(browser, animated: true, completion: {})
        
    }
   
    
}



