//
//  AddEditAdminManageViewController.swift
//  Muslim Hub
//
//  Created by Sakshi Singh on 26/04/19.
//  Copyright © 2019 OnGraph. All rights reserved.
//

import UIKit
import CountryPickerViewSwift
import KVNProgress

class AddEditAdminManageViewController: UIViewController {
    
    @IBOutlet weak var txt_FirstName:UITextField!
    @IBOutlet weak var txt_Email:UITextField!
    @IBOutlet weak var txt_LastName:UITextField!
    @IBOutlet weak var txt_Phone_No:UITextField!
    @IBOutlet weak var btn_Iqamah:UIButton!
    @IBOutlet weak var btn_All:UIButton!
    @IBOutlet weak var btn_Code:UIButton!
    @IBOutlet weak var view_Header:UIView!
     @IBOutlet weak var lbl_HeaderTitle:UILabel!
     @IBOutlet weak var lbl_AdminPermissionTitle:UILabel!
    @IBOutlet weak var lbl_IqamahOnlyTitle:UILabel!
        @IBOutlet weak var lbl_AllTitle:UILabel!
    var dic_Privileges = [String:Any]()
    var str_phnCode = "+61"
    var str_IqamaOrAll = String()
    var orgDetails:OrganizationDataLists!
    @IBOutlet weak var btn_Save:UIButton!
    @IBOutlet weak var btn_Cancel:UIButton!
    var adminId = Int()
    var adminDetails:AdminListSData!
    var phoneCode: String = ""
    var phoneNumber: String = ""
    
    
    //MARK:ViewLife Cycle--------------------------------------------------------------------
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        // shadow
        view_Header.shadowBlackToHeader()
        
        // corner radius
        btn_Save.layer.cornerRadius = 5
        // shadow
        btn_Save.shadowBlackToHeader()
        
        // corner radius
        btn_Cancel.layer.cornerRadius = 5
        // shadow
        btn_Cancel.shadowBlackToHeader()
        self.hideKeyboardWhenTappedAround()
        if #available(iOS 13.0, *) {
            statusBarColorChange(color:UIColor.init(hex:  0x05BDB9))
        } else {
            UIApplication.statusBarBackgroundColor = UIColor.init(hex: 0x05BDB9)
        }
        self.lbl_HeaderTitle.text = "Add Admin".localized
        self.txt_FirstName.placeholder = "First Name".localized
        self.txt_LastName.placeholder = "Last Name".localized
        self.txt_Email.placeholder = "Email Id".localized
        self.txt_Phone_No.placeholder = "Phone no".localized
        self.lbl_AdminPermissionTitle.text = "Admin Permission".localized
        self.lbl_IqamahOnlyTitle.text = "Iqamah Only".localized
        self.lbl_AllTitle.text = "All".localized
        self.btn_Save.setTitle("Save".localized, for: .normal)
        self.btn_Cancel.setTitle("Cancel".localized, for: .normal)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        if adminDetails != nil{
            
            self.txt_FirstName.text = adminDetails.firstName ?? ""
            self.txt_LastName.text = adminDetails.lastName ?? ""
            self.txt_Email.text = adminDetails.email ?? ""
         
            self.str_IqamaOrAll = adminDetails.privileges?.orgPerm ?? ""
            
            //mark: Phone number trim----------------------------
            if adminDetails.mobileNumber == "" ||  adminDetails.mobileNumber == nil{
                self.phoneCode = "+61"
                self.phoneNumber = ""
                self.txt_Phone_No.text = ""
            }else{
                let phone =  adminDetails.mobileNumber?.components(separatedBy: "-")
                self.phoneCode = phone?[0] ?? "+61"
                self.phoneNumber = phone?[1] ?? ""
                self.btn_Code.setTitle(self.phoneCode, for: .normal)
                self.txt_Phone_No.text = (phone?[1])!
            }
            
            
            if str_IqamaOrAll == "All"{
                btn_All.setBackgroundImage(#imageLiteral(resourceName: "radio_selected_iPhone"), for: .normal)
                self.str_IqamaOrAll = "All"
            }else{
                btn_Iqamah.setBackgroundImage(#imageLiteral(resourceName: "radio_selected_iPhone"), for: .normal)
                self.str_IqamaOrAll = "iqama_only"
            }
        }
    }
    
    //MARK:IBAction------------------------------------------------
    @IBAction func btn_AddAdmin(sendr:UIButton){
        if adminDetails != nil{
            self.EditAdminApi()
        }else{
            self.AddAdminApi()
        }
        
        
    }
    @IBAction func btn_Back(sendr:UIButton){
        
        self.dismiss(animated: true, completion: nil)
        
        // self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btn_All_IQama(sender:UIButton){
        
        if sender.tag == 0{
            
            btn_Iqamah.setBackgroundImage(#imageLiteral(resourceName: "radio_selected_iPhone"), for: .normal)
            self.str_IqamaOrAll = "iqama_only"
            btn_All.setBackgroundImage(#imageLiteral(resourceName: "radio_unselected_iPhone"), for: .normal)
        }else if sender.tag == 1{
            btn_All.setBackgroundImage(#imageLiteral(resourceName: "radio_selected_iPhone"), for: .normal)
            self.str_IqamaOrAll = "All"
            btn_Iqamah.setBackgroundImage(#imageLiteral(resourceName: "radio_unselected_iPhone"), for: .normal)
        }
    }
    @IBAction func showCountryPickerView(_ sender: UIButton) {
        let countryView = CountrySelectView.shared
        countryView.countryNameColor = .blue
        countryView.barTintColor = .red
        countryView.displayLanguage = .english
        countryView.show()
        countryView.selectedCountryCallBack = { (countryDic) -> Void in
            self.btn_Code.setTitle("+\(countryDic["code"] as! NSNumber)", for: .normal)
            self.str_phnCode =  "+\(countryDic["code"] as! NSNumber)" 
            
        }
    }
   
    //Function Api-------------------------------------------------------
    func AddAdminApi(){
        var mobile = String()
       var phoneNumber = String()
        self.dic_Privileges.updateValue(self.orgDetails.id ?? 0, forKey: "org_id")
        self.dic_Privileges.updateValue( self.str_IqamaOrAll, forKey: "org_perm")
        phoneNumber = self.txt_Phone_No.text ?? ""
        while (phoneNumber.hasPrefix("0")) {
            phoneNumber.remove(at: (phoneNumber.startIndex))
        }
        
        mobile =  (self.str_phnCode + "-" + (phoneNumber))

        if (self.txt_FirstName.text == ""){
            AKAlertController.alert("", message: "Please Enter First Name".localized)
        }else if (self.txt_LastName.text == ""){
            AKAlertController.alert("", message: "Please Enter Last Name".localized)
        } else if (self.txt_Phone_No.text == ""){
            AKAlertController.alert("", message: "Please Enter Contact".localized)
            
        } else if ((self.txt_Email.text?.validateEmail())!) == false{
            AKAlertController.alert("", message: "Please Enter Valid Email Id".localized)
        }
        else if self.dic_Privileges["org_perm"] as! String == ""{
            AKAlertController.alert("Please select admin permission".localized)
            return
        }
        else if (phoneNumber.count > 10){
            AKAlertController.alert("", message: "Please Enter Valid Contact".localized)
            
        }else{
        KVNProgress.show()
        Network.shared.AddAdminApi(email: txt_Email.text ?? "",first_name:txt_FirstName.text ?? "", last_name: txt_LastName.text ?? "", password: "", mobile_number_verified: true, privileges: self.dic_Privileges,mobile_number:mobile) { (result) in
            KVNProgress.dismiss()
            guard let user = result else {
                
                return
            }
            
            if user.success == true{
                
                
                self.showSingleAlertMessage(message: "", subMsg: user.message ?? "", sender: self, completion:
                    { (success) -> Void in
                        if success == true{
                            self.dismiss(animated: true, completion: nil)
                            
                            
                        }
                })
                
                
            }else{
                self.showSingleAlertMessage(message: "", subMsg: user.message ?? "", sender: self, completion:
                    { (success) -> Void in
                      
                })
            }
            
        }
        }
    }
    //EditAdminapi Api------------------------------------
    func EditAdminApi(){
        var mobileNumber = String()
        let adminid = String(adminId)
        UserDefaults.standard.set(adminid, forKey: "addAdminId")
        self.dic_Privileges.updateValue(self.orgDetails.id ?? 0, forKey: "org_id")
        self.dic_Privileges.updateValue( self.str_IqamaOrAll, forKey: "org_perm")
        var phoneNumber = String()
       
        phoneNumber = self.txt_Phone_No.text ?? ""
        while (phoneNumber.hasPrefix("0")) {
            phoneNumber.remove(at: (phoneNumber.startIndex))
        }
        
        mobileNumber =  (self.phoneCode + "-" + (phoneNumber))

     
  
        
        if (self.txt_FirstName.text == ""){
            AKAlertController.alert("", message: "Please Enter First Name".localized)
        }else if (self.txt_LastName.text == ""){
            AKAlertController.alert("", message: "Please Enter Last Name".localized)
        } else if (self.txt_Phone_No.text == ""){
            AKAlertController.alert("", message: "Please Enter Contact".localized)
            
        }else if self.dic_Privileges["org_perm"] as! String == ""{
            AKAlertController.alert("Please select admin permission".localized)
            return
        }else if ((self.txt_Email.text?.validateEmail())!) == false{
            AKAlertController.alert("", message: "Please Enter Valid Email Id".localized)
        }
        else if (phoneNumber.count > 10){
            AKAlertController.alert("", message: "Please Enter Valid  Contact".localized)
            
        }else{
            
          KVNProgress.show()
        Network.shared.editAdminDetails(email: txt_Email.text ?? "",first_name:txt_FirstName.text ?? "", last_name: txt_LastName.text ?? "", password: "", mobile_number_verified: true, privileges: self.dic_Privileges,mobile_number:mobileNumber ) { (result) in
            KVNProgress.dismiss()
            guard let user = result else {
                
                return
            }
            
            if user.success == true{
                self.showSingleAlertMessage(message: "", subMsg: user.message ?? "Admin saved Successfully.".localized, sender: self, completion:
                    { (success) -> Void in
                        if success == true{
                            
//                            var array_P = [PriviligesData]()
                            //                            if user.user?.privileges != nil{
                            //                                array_P = (user.user?.privileges)!
                            //                                let array_privileges = array_P.map({$0.org_id })
                            //                                print("org Id - ",array_privileges)
                            //                               // UserDefaults.standard.set(array_privileges, forKey: "OwnOrg_Array")
                            //                                //mark:all Org_Permission save-------
                            //                                //
                            //
                            //                                Global.shared.priviligesData = array_P
                            //
                            //
                            //                            }
//                            
//                            UserDefaults.standard.synchronize()
                            
                          
                            self.dismiss(animated: true, completion: nil)
                            
                            
                        }
                })
            }else{
                self.showSingleAlertMessage(message: "", subMsg: user.message ?? "", sender: self, completion:
                    { (success) -> Void in
                        if success == true{
                      
                            
                            
                        }
                })
               
            }
            
        }
    }
    }
    
 
}
