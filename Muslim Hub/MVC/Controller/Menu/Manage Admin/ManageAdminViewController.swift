//
//  ManageAdminViewController.swift
//  Muslim Hub
//
//  Created by Sakshi Singh on 24/04/19.
//  Copyright © 2019 OnGraph. All rights reserved.
//

import UIKit
import KVNProgress

class ManageAdminViewController: UIViewController {
    
    @IBOutlet weak var tableiew_Manage:UITableView!
    @IBOutlet weak var view_Header:UIView!
    @IBOutlet weak var lbl_title:UILabel!
    var allOragnizationLists = [OrganizationDataLists]()
    var mangeUser = String()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        getPrivilegesDataApi()
        if #available(iOS 13.0, *) {
            statusBarColorChange(color:UIColor.init(hex:  0x05BDB9))
        } else {
            UIApplication.statusBarBackgroundColor = UIColor.init(hex: 0x05BDB9)
        }
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        if let mangeUser = UserDefaults.standard.value(forKey: "ManageUser") as? String{
            if mangeUser == "ManageUser"{
                lbl_title.text = "Manage Users".localized
                self.organizationListApi(adminType:"false")
            }else{
                lbl_title.text = "Manage Admin".localized
                self.organizationListApi(adminType:"true")
            }
        }
        
        // shadow
        view_Header.shadowBlackToHeader()
    }
    
    //MARK:IBAction------------------------------------------------
    @IBAction func btn_Back(sendr:UIButton){
        
        if let controller = self.storyboard?.instantiateViewController(withIdentifier: "TabVC") {
            self.slideMenuController()?.changeMainViewController(controller, close: true)
        }
    }
    
    @objc func btn_Location(sender: UITapGestureRecognizer){
        
        let lat = self.allOragnizationLists[sender.view!.tag].latitude ?? 123.123
        let long = self.allOragnizationLists[sender.view!.tag].longitude ?? 123.123
        self.location(lat: lat, long: long,title: self.allOragnizationLists[sender.view!.tag].address ?? "")
        
        
    }
    
    
    //Mark:Function Api-----------------------------
    func organizationListApi(adminType:String){
        KVNProgress.show()
        Network.shared.ManageorganizationLists(adminType:adminType) { (result) in
            KVNProgress.dismiss()
            guard let user = result else {
                return
            }
            
            self.allOragnizationLists = user
            self.tableiew_Manage.reloadData()
        }
    }
    
}


//MARK:TableView DataSource Delegate--------------------------------------------------
extension ManageAdminViewController:UITableViewDataSource,UITableViewDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return allOragnizationLists.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "ManageAdminTableViewCell", for: indexPath) as! ManageAdminTableViewCell
        
        
        let linkAttributes: [NSAttributedString.Key : Any] = [
            NSAttributedString.Key(rawValue: NSAttributedString.Key.foregroundColor.rawValue): UIColor.gray,
            NSAttributedString.Key(rawValue: NSAttributedString.Key.underlineColor.rawValue): UIColor.lightGray,
            NSAttributedString.Key(rawValue: NSAttributedString.Key.underlineStyle.rawValue): NSUnderlineStyle.single.rawValue
        ]
        let attributeString = NSMutableAttributedString(string: self.allOragnizationLists[indexPath.row].address ?? "", attributes: linkAttributes)
        
        cell.lbl_Details.tag = indexPath.row
        cell.lbl_Details.attributedText = attributeString
        
        
        let gesture = UITapGestureRecognizer(target: self, action: #selector(btn_Location))
        // if labelView is not set userInteractionEnabled, you must do so
        cell.lbl_Details.isUserInteractionEnabled = true
        cell.lbl_Details.addGestureRecognizer(gesture)
        
        
        cell.lbl_Title.text = ((self.allOragnizationLists[indexPath.row].abbreviation ?? "") + " - " + (self.allOragnizationLists[indexPath.row].name ?? ""))
        
        if let photo = self.allOragnizationLists[indexPath.row].logo, let url = URL(string: photo) {
            cell.img_Logo.af_setImage(withURL: url)
        } else {
            cell.img_Logo.image = UIImage(named: "default_icon_iPhone_Logo.png")
        }
        
        return cell
    }
    
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if let mangeUser = UserDefaults.standard.value(forKey: "ManageUser") as? String{
            
            if mangeUser == "ManageUser"{
                
                self.permission(indexPath: indexPath.row)
                
                
            }else{
                
                let vc = self.storyboard?.instantiateViewController(withIdentifier: "ManageAdminDetailsViewController") as! ManageAdminDetailsViewController
                vc.orgDetails = self.allOragnizationLists[indexPath.row]
                vc.modalPresentationStyle = .fullScreen
                self.present(vc, animated: true, completion: nil)
            }
            
        }
    }
    
    func permission(indexPath:Int){
        
        //mark:Check for permission authorization-------------------
        if (UserDefaults.standard.value(forKey: "Login_Token") as? String) != nil{
            
            if let superAdmin = UserDefaults.standard.value(forKey: "isSuperAdmin") as? Bool{
                
                if superAdmin == true{
                    let vc = self.storyboard?.instantiateViewController(withIdentifier: "Manage_UsersListsViewController") as! Manage_UsersListsViewController
                    vc.orgDetails = self.allOragnizationLists[indexPath]
                    vc.modalPresentationStyle = .fullScreen
                    self.present(vc, animated: true, completion: nil)
                    //   self.btn_Add.isHidden = false
                }else{
                    
                    if let is_staff = UserDefaults.standard.value(forKey: "is_staff") as? Bool{
                        
                        if is_staff == true{
                            //mark:check For own Organisation---------------------------
                            if let orgOwn_Array = UserDefaults.standard.value(forKey: "OwnOrg_Array") as? [Int]{
                                
                                
                                if orgOwn_Array.contains(self.allOragnizationLists[indexPath].id ?? 0){
                                    
                                    var org_P = [PriviligesData]()
                                    org_P = Global.shared.priviligesData
                                    let filertedData = org_P.filter{$0.org_id == self.allOragnizationLists[indexPath].id ?? 0}
                                    filertedData.forEach {
                                        //   print("model data objects::",$0.org_perm as? String)
                                        
                                        if $0.org_perm == "iqama_only"{
                                            
                                            self.showSingleAlertMessage(message: "You do not have ALL admin permission".localized, subMsg: "", sender: self, completion:
                                                { (success) -> Void in
                                                    
                                            })
                                            
                                        }else{
                                            let vc = self.storyboard?.instantiateViewController(withIdentifier: "Manage_UsersListsViewController") as! Manage_UsersListsViewController
                                            vc.orgDetails = self.allOragnizationLists[indexPath]
                                            vc.modalPresentationStyle = .fullScreen
                                            self.present(vc, animated: true, completion: nil)
                                        }
                                    }
                                    
                                    
                                }else{
                                    //    self.btn_Add.isHidden = true
                                    
                                }//orgOwn
                                
                            }
                        }//staff true
                    }//staff
                    
                }
            }//SuperAdmin
        }//Login
        else{
            
            
            //  self.btn_Add.isHidden = true
            
        }
    }
}
