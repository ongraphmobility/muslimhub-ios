//
//  ManageAdminDetailsViewController.swift
//  Muslim Hub
//
//  Created by Sakshi Singh on 24/04/19.
//  Copyright © 2019 OnGraph. All rights reserved.
//

import UIKit
import KVNProgress

class ManageAdminDetailsViewController: UIViewController {
    
    @IBOutlet weak var tableiew_Manage:UITableView!
    @IBOutlet weak var view_Header:UIView!
    @IBOutlet weak var lbl_HeaderTitle:UILabel!
    @IBOutlet weak var btn_Add:UIButton!
    var orgDetails:OrganizationDataLists!
    var allOragnizationAdminLists = [AdminListSData]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        let org_id = String(self.orgDetails.id ?? 0)
        UserDefaults.standard.set(org_id, forKey: "Org_id")
        if #available(iOS 13.0, *) {
                              statusBarColorChange(color:UIColor.init(hex:  0x05BDB9))
                          } else {
                              UIApplication.statusBarBackgroundColor = UIColor.init(hex: 0x05BDB9)
                          }
        // shadow
        view_Header.shadowBlackToHeader()
        
        self.lbl_HeaderTitle.text = "Manage Admin".localized
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.adminListApi()
        
    }
    
    //MARK:IBAction------------------------------------------------
    
    @IBAction func btn_Back(sendr:UIButton){
        
        self.dismiss(animated: true, completion: nil)
        //  self.navigationController?.popViewController(animated: true)
    }
    @IBAction func btn_AddAdmin(sendr:UIButton){
        
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "AddEditAdminManageViewController") as! AddEditAdminManageViewController
        vc.orgDetails = orgDetails
        vc.modalPresentationStyle = .fullScreen
        self.present(vc, animated: true, completion: nil)
    }
    
    @objc func btn_Delete(sender : UIButton!) {
        
        
        AKAlertController.alert("", message: "Do you really want to delete this admin?".localized, buttons: ["Ok".localized,"Cancel".localized], tapBlock: { (_, _, index) in
            if index == 1 {return}
            let admin_id = self.allOragnizationAdminLists[sender.tag].id ?? 0
            let admin_id_str = String(admin_id)
            UserDefaults.standard.set(admin_id_str, forKey: "admin_id")
            self.allOragnizationAdminLists.remove(at: sender.tag)
            self.adminDeleteApi(adminId:admin_id)
            return
        })
     
    }
    @objc func btn_Edit(sender : UIButton!) {
        
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "AddEditAdminManageViewController") as! AddEditAdminManageViewController
        vc.adminId = allOragnizationAdminLists[sender.tag].id ?? 0
        vc.adminDetails = allOragnizationAdminLists[sender.tag]
        vc.orgDetails = orgDetails
        vc.modalPresentationStyle = .fullScreen
        self.present(vc, animated: true, completion: nil)
        
    }
    //MARK:Functionc Api-----------------------------------------
    func adminListApi(){
        KVNProgress.show()
        Network.shared.AdminLists() { (result) in
            KVNProgress.dismiss()
            guard let user = result else {
                
                return
            }
            
            self.allOragnizationAdminLists = user
            
            //mark:Check for permission authorization-------------------
            if (UserDefaults.standard.value(forKey: "Login_Token") as? String) != nil{
                
                if let superAdmin = UserDefaults.standard.value(forKey: "isSuperAdmin") as? Bool{
                    
                    if superAdmin == true{
                        
                        self.btn_Add.isHidden = false
                    }else{
                        
                        if let is_staff = UserDefaults.standard.value(forKey: "is_staff") as? Bool{
                            
                            if is_staff == true{
                                //mark:check For own Organisation---------------------------
                                if let orgOwn_Array = UserDefaults.standard.value(forKey: "OwnOrg_Array") as? [Int]{
                                    
                                    
                                    if orgOwn_Array.contains(self.orgDetails.id ?? 0){
                                        
                                        var org_P = [PriviligesData]()
                                        org_P = Global.shared.priviligesData
                                        let filertedData = org_P.filter{$0.org_id == self.orgDetails.id ?? 0}
                                        filertedData.forEach {
                                            //   print("model data objects::",$0.org_perm as? String)
                                            
                                            if $0.org_perm == "iqama_only"{
                                                self.btn_Add.isHidden = true
                                            }else{
                                                self.btn_Add.isHidden = false
                                                
                                            }
                                        }
                                        
                                        
                                    }else{
                                        self.btn_Add.isHidden = true
                                        
                                    }//orgOwn
                                    
                                }
                            }//staff true
                        }//staff
                        
                    }
                }//SuperAdmin
            }//Login
            else{
                
                
                self.btn_Add.isHidden = true
                
            }
            
            self.tableiew_Manage.reloadData()
        }
    }
    
    func adminDeleteApi(adminId:Int){
        KVNProgress.show()
        Network.shared.deletAdminApi(user_id:adminId ,org_id:orgDetails.id ?? 0) { (result) in
            KVNProgress.dismiss()
            guard result != nil else {
                
                return
            }
            self.tableiew_Manage.reloadData()
        }
    }
}

//MARK:TableView DataSource Delegate--------------------------------------------------
extension ManageAdminDetailsViewController:UITableViewDataSource,UITableViewDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return allOragnizationAdminLists.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "ManageAdminDeatilsTableViewCell", for: indexPath) as! ManageAdminDeatilsTableViewCell
        
        
        //language
        
        cell.lbl_NAmeTitle.text = "Name".localized
        cell.lbl_EmailIdTitle.text = "Email Id".localized
        cell.lbl_PhoneNoTitle.text = "Phone no".localized
        cell.lbl_AdminTypeTitle.text = "Admin Type".localized
        
        //mark:Check for permission authorization-------------------
        if (UserDefaults.standard.value(forKey: "Login_Token") as? String) != nil{
            
            if let superAdmin = UserDefaults.standard.value(forKey: "isSuperAdmin") as? Bool{
                
                if superAdmin == true{
                    
                    cell.btn_Edit.isHidden = false
                    cell.btn_Delete.isHidden = false
                }else{
                    
                    if let is_staff = UserDefaults.standard.value(forKey: "is_staff") as? Bool{
                        
                        if is_staff == true{
                            //mark:check For own Organisation---------------------------
                            if let orgOwn_Array = UserDefaults.standard.value(forKey: "OwnOrg_Array") as? [Int]{
                                
                                
                                if orgOwn_Array.contains(self.orgDetails.id ?? 0){
                                    
                                    var org_P = [PriviligesData]()
                                    org_P = Global.shared.priviligesData
                                    let filertedData = org_P.filter{$0.org_id == self.orgDetails.id ?? 0}
                                    filertedData.forEach {
                                        //   print("model data objects::",$0.org_perm as? String)
                                        
                                        if $0.org_perm == "iqama_only"{
                                            cell.btn_Edit.isHidden = true
                                            cell.btn_Delete.isHidden = true
                                        }else{
                                            cell.btn_Edit.isHidden = false
                                            cell.btn_Delete.isHidden = false
                                            
                                        }
                                    }
                                    
                                    
                                }else{
                                    cell.btn_Edit.isHidden = true
                                    cell.btn_Delete.isHidden = true
                                }//orgOwn
                                
                            }
                        }//staff true
                    }//staff
                    
                }
            }//SuperAdmin
        }//Login
        else{
            
            
            cell.btn_Edit.isHidden = true
            cell.btn_Delete.isHidden = true
            
        }
        
        
        
        cell.tag = indexPath.row
        cell.btn_Edit.tag = indexPath.row
        cell.btn_Delete.tag = indexPath.row
        cell.btn_Delete.addTarget(self, action: #selector(btn_Delete(sender:)), for: .touchUpInside)
        cell.btn_Edit.addTarget(self, action: #selector(btn_Edit(sender:)), for: .touchUpInside)
        let firstName = ": " + (allOragnizationAdminLists[indexPath.row].firstName ?? "")
        let lastName = (allOragnizationAdminLists[indexPath.row].lastName) ?? ""
        let fullName = (firstName + " " + lastName)
        cell.lbl_NAme.text = fullName
        
        cell.lbl_EmailId.text = (": " + (allOragnizationAdminLists[indexPath.row].email ?? "") )
        cell.lbl_PhoneNo.text = (": " + (allOragnizationAdminLists[indexPath.row].mobileNumber ?? "No Number") )
        let permission = (allOragnizationAdminLists[indexPath.row].privileges)
        if permission?.orgPerm == "iqama_only"{
            cell.lbl_AdminType.text = (": " + "Iqamah Only".localized)
        }else{
            cell.lbl_AdminType.text = (": " + "All".localized)
        }
      return cell
    }
    
    
    
}
