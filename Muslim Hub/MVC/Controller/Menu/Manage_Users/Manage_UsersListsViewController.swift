//
//  Manage_UsersListsViewController.swift
//  Muslim Hub
//
//  Created by Ongraph on 28/06/19.
//  Copyright © 2019 OnGraph. All rights reserved.
//

import UIKit
import MobileCoreServices
import UIKit
import KVNProgress

class Manage_UsersListsViewController: UIViewController {
    
    @IBOutlet weak var tableiew_Manage:UITableView!
    @IBOutlet weak var view_Header:UIView!
    @IBOutlet weak var lbl_Alert10:UILabel!
    @IBOutlet weak var btn_AddUser:UIButton!
    @IBOutlet weak var btn_Import:UIButton!
    @IBOutlet weak var lbl_HeaderTitle:UILabel!
    @IBOutlet weak var btn_xls:UIButton!
    @IBOutlet weak var btn_pdf:UIButton!
    
    
    
    var orgDetails:OrganizationDataLists!
    var filePath = Data()
    var allOragnizationLists = [OrganizationDataLists]()
    var user_id = Int()
    
    
    //MARK:View Lifecycle Methods-----------------------------
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.lbl_HeaderTitle.text = "Manage Users".localized
        self.btn_pdf.setTitle("Email as PDF".localized, for: .normal)
        self.btn_xls.setTitle("Email as XLS".localized, for: .normal)
        self.lbl_Alert10.text = "Only top ten users listed here. To get the full list click one of the emails buttons below.".localized
        // Do any additional setup after loading the view.
        if #available(iOS 13.0, *) {
                              statusBarColorChange(color:UIColor.init(hex: 0x05BDB9))
                          } else {
                              UIApplication.statusBarBackgroundColor = UIColor.init(hex: 0x05BDB9)
                          }
        lbl_Alert10.shadowWhiteToHeader()
        getPrivilegesDataApi()
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.MangeUserorganizationListApi()
        // shadow
        view_Header.shadowBlackToHeader()
    }
    
    
    
    //MARK:IBAction------------------------------------------------
    @IBAction func btn_Importxls(sendr:UIButton){
        self.clickFunction()
    }
    @IBAction func btn_AddManageUser(sendr:UIButton){
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "AddManageUserViewController") as! AddManageUserViewController
        vc.org_id = self.orgDetails.id ?? 0
        vc.modalPresentationStyle = .fullScreen
        self.present(vc, animated: true, completion: nil)
    }
    @IBAction func btn_Back(sendr:UIButton){
        
        self.dismiss(animated: true, completion: nil)
        //  self.navigationController?.popViewController(animated: true)
    }
    @objc func btn_Delete(sender : UIButton!) {
        
        
        AKAlertController.alert("", message: "Do you really want to delete this user?".localized, buttons: ["Ok".localized,"Cancel".localized], tapBlock: { (_, _, index) in
            if index == 1 {return}
            self.user_id = self.allOragnizationLists[sender.tag].id ?? 0
            
            
            self.allOragnizationLists.remove(at: sender.tag)
            self.manageuserDeleteApi()
            return
        })
    }
    
    @IBAction func btn_PdfExport(sender: UIButton){
        
        Network.shared.getpdfExport(orgId: orgDetails.id ?? 0, type: "pdf") { (result) in
            guard let user = result else {
                
                return
            }
            if result?.success == true{
                AKAlertController.alert("", message: result?.message ?? "An email with attachment has been sent to you.")
            }else{
                AKAlertController.alert("", message: result?.message ?? "")
            }
            print(user)
        }
    }
    @IBAction func btn_XlsExport(sender: UIButton){
        
        Network.shared.getpdfExport(orgId: orgDetails.id ?? 0, type: "xls") { (result) in
            guard let user = result else {
                
                return
            }
            if result?.success == true{
                
                AKAlertController.alert("", message: result?.message ?? "An email with attachment has been sent to you.")
            }else{
                
                AKAlertController.alert("", message: result?.message ?? "")
            }
            print(user)
        }
    }
    @objc func btn_Edit(sender : UIButton!) {
        
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "AddManageUserViewController") as! AddManageUserViewController
        vc.org_id = self.allOragnizationLists[sender.tag].id ?? 0
        vc.manageUSerDetails = self.allOragnizationLists[sender.tag]
        vc.editManageUsers = "EditUsers"
        vc.modalPresentationStyle = .fullScreen
        self.present(vc, animated: true, completion: nil)
        
    }
    
    //MARK:-Funtion Api's-------------------------------------------------
    
    //mark:-DeleteUserApi--
    func manageuserDeleteApi(){
        KVNProgress.show()
        Network.shared.deletManageUserApi(user_id:self.user_id,orgId:self.orgDetails.id ?? 0) { (result) in
            KVNProgress.dismiss()
            guard let user = result else {
                
                return
            }
            if user.success == true{
                self.showSingleAlertMessage(message: "", subMsg:  user.message ?? "", sender: self, completion:
                    { (success) -> Void in
                        self.tableiew_Manage.reloadData()
                })
            }
            
            
            
            
        }
    }
    
    //mark:manageUserList Api--
    
    func MangeUserorganizationListApi(){
        KVNProgress.show()
        Network.shared.ManageorganizationUsersLists(org_id: orgDetails.id ?? 0) { (result) in
            KVNProgress.dismiss()
            guard let user = result else {
                
                return
            }
            
            self.allOragnizationLists = user
            self.tableiew_Manage.reloadData()
        }
    }
}
//MARK:TableView DataSource Delegate--------------------------------------------------
extension Manage_UsersListsViewController:UITableViewDataSource,UITableViewDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return allOragnizationLists.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "Manage_UserListCellTableViewCell", for: indexPath) as! Manage_UserListCellTableViewCell
        
        //language
          
          cell.lbl_NAmeTitle.text = "Name".localized
          cell.lbl_EmailIdTitle.text = "Email Id".localized
          cell.lbl_PhoneNoTitle.text = "Phone no".localized
        
        
        //mark:Check for permission authorization-------------------
        if (UserDefaults.standard.value(forKey: "Login_Token") as? String) != nil{
            
            if let superAdmin = UserDefaults.standard.value(forKey: "isSuperAdmin") as? Bool{
                
                if superAdmin == true{
                    
                    cell.btn_Edit.isHidden = false
                    cell.btn_Delete.isHidden = false
                    self.btn_Import.isHidden = false
                    self.btn_AddUser.isHidden = false
                    self.btn_pdf.isHidden = false
                    self.btn_xls.isHidden = false
                }else{
                    
                    if let is_staff = UserDefaults.standard.value(forKey: "is_staff") as? Bool{
                        
                        if is_staff == true{
                            //mark:check For own Organisation---------------------------
                            if let orgOwn_Array = UserDefaults.standard.value(forKey: "OwnOrg_Array") as? [Int]{
                                
                                
                                if orgOwn_Array.contains(self.orgDetails.id ?? 0){
                                    
                                    var org_P = [PriviligesData]()
                                    org_P = Global.shared.priviligesData
                                    let filertedData = org_P.filter{$0.org_id == self.orgDetails.id ?? 0}
                                    filertedData.forEach {
                                        //   print("model data objects::",$0.org_perm as? String)
                                        
                                        if $0.org_perm == "iqama_only"{
                                            cell.btn_Edit.isHidden = true
                                            cell.btn_Delete.isHidden = true
                                            self.btn_Import.isHidden = true
                                            self.btn_AddUser.isHidden = true
                                            self.btn_pdf.isHidden = true
                                            self.btn_xls.isHidden = true
                                        }else{
                                            self.btn_pdf.isHidden = false
                                            self.btn_xls.isHidden = false
                                            if orgDetails.is_private == true{
                                                cell.btn_Edit.isHidden = false
                                                cell.btn_Delete.isHidden = false
                                                self.btn_Import.isHidden = false
                                                self.btn_AddUser.isHidden = false
                                            }else{
                                                cell.btn_Edit.isHidden = true
                                                cell.btn_Delete.isHidden = false
                                                self.btn_Import.isHidden = true
                                                self.btn_AddUser.isHidden = true
                                            }
                                            
                                            
                                        }
                                    }
                                    
                                    
                                }else{
                                    cell.btn_Edit.isHidden = true
                                    cell.btn_Delete.isHidden = true
                                    self.btn_Import.isHidden = true
                                    self.btn_AddUser.isHidden = true
                                    
                                    self.btn_pdf.isHidden = false
                                    self.btn_xls.isHidden = false
                                }//orgOwn
                                
                            }
                        }//staff true
                    }//staff
                    
                }
            }//SuperAdmin
        }//Login
        else{
            
            cell.btn_Edit.isHidden = true
            cell.btn_Delete.isHidden = true
            
        }
        
        cell.tag = indexPath.row
        cell.btn_Edit.tag = indexPath.row
        cell.btn_Delete.tag = indexPath.row
        cell.btn_Delete.addTarget(self, action: #selector(btn_Delete(sender:)), for: .touchUpInside)
        cell.btn_Edit.addTarget(self, action: #selector(btn_Edit(sender:)), for: .touchUpInside)
        let firstName = ": " + (allOragnizationLists[indexPath.row].first_name ?? "") + " " + (allOragnizationLists[indexPath.row].last_name ?? "")
        
        cell.lbl_NAme.text = firstName
        
        cell.lbl_EmailId.text = (": " + (allOragnizationLists[indexPath.row].email ?? "") )
        cell.lbl_PhoneNo.text = (": " + (allOragnizationLists[indexPath.row].mobile_number ?? "No Number") )
        
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
    }
    
    func uploadxlsFile(){
        
        Network.shared.uploadWithAlamofire(org_id: orgDetails.id ?? 0,filePath: self.filePath){(result) in
            
            guard result != nil else {
                
                return
            }
            
            if result?.success == true{
                AKAlertController.alert((result?.message)!)
                self.MangeUserorganizationListApi()
            }else{
                AKAlertController.alert((result?.message)!)
            }
            
        }
    }
}
extension Manage_UsersListsViewController: UIDocumentMenuDelegate,UIDocumentPickerDelegate,UINavigationControllerDelegate{
    public func documentPicker(_ controller: UIDocumentPickerViewController, didPickDocumentsAt urls: [URL]) {
        guard let myURL = urls.first else {
            return
        }
        print("import result : \(myURL)")
        let data = try! Data(contentsOf: urls.first!)
        self.filePath = data
        // self.filePath = "\(myURL)"
        self.uploadxlsFile()
    }
    
    
    public func documentMenu(_ documentMenu:UIDocumentMenuViewController, didPickDocumentPicker documentPicker: UIDocumentPickerViewController) {
        documentPicker.delegate = self
        present(documentPicker, animated: true, completion: nil)
    }
    
    
    func documentPickerWasCancelled(_ controller: UIDocumentPickerViewController) {
        print("view was cancelled")
        dismiss(animated: true, completion: nil)
    }
    func clickFunction(){
        
        let importMenu = UIDocumentMenuViewController(documentTypes: [String(kUTTypeSpreadsheet)], in: .import)
        importMenu.delegate = self
        importMenu.modalPresentationStyle = .formSheet
        self.present(importMenu, animated: true, completion: nil)
    }
}
