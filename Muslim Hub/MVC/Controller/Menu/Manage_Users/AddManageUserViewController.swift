//
//  AddManageUserViewController.swift
//  Muslim Hub
//
//  Created by Ongraph on 01/07/19.
//  Copyright © 2019 OnGraph. All rights reserved.
//

import UIKit
import CountryPickerViewSwift
import KVNProgress

class AddManageUserViewController: UIViewController,UITextFieldDelegate {
    
    @IBOutlet weak var txt_FName:UITextField!
    @IBOutlet weak var txt_LName:UITextField!
    @IBOutlet weak var txt_Email:UITextField!
    @IBOutlet weak var txt_Code:UITextField!
    @IBOutlet weak var txt_PhoneNumber:UITextField!
    @IBOutlet weak var btn_AddUSers:UIButton!
    @IBOutlet weak var lbl_Header:UILabel!
    var org_id = Int()
    var manageUSerDetails : OrganizationDataLists!
    var editManageUsers = String()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        if #available(iOS 13.0, *) {
                                   statusBarColorChange(color:UIColor.init(hex: 0x05BDB9))
                               } else {
                                   UIApplication.statusBarBackgroundColor = UIColor.init(hex: 0x05BDB9)
                               }
              
        btn_AddUSers.layer.cornerRadius = 6
        // shadow
        btn_AddUSers.layer.shadowColor = UIColor.darkGray.cgColor
        btn_AddUSers.layer.shadowOffset = CGSize(width: 0, height: 3)
        btn_AddUSers.layer.shadowOpacity = 0.7
        btn_AddUSers.layer.shadowRadius = 4.0
        lbl_Header.shadowBlackToHeader()
        
        self.txt_FName.placeholder = "First Name".localized
        self.txt_LName.placeholder = "Last Name".localized
        self.txt_Email.placeholder = "Email Id".localized
        self.txt_PhoneNumber.placeholder = "Mobile Number".localized
        self.lbl_Header.text = "Manage Users".localized
        self.btn_AddUSers.setTitle("Save".localized, for: .normal)
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        if manageUSerDetails != nil{
            
            self.txt_FName.text = manageUSerDetails.first_name ?? ""
            self.txt_LName.text = manageUSerDetails.last_name ?? ""
            self.txt_Email.text = manageUSerDetails.email ?? ""
            let MobileNumber = manageUSerDetails.mobile_number ?? ""
            if MobileNumber == ""{
                self.txt_Code.text = "+61"
                self.txt_PhoneNumber.text = ""
            }else{
                let phone = MobileNumber.components(separatedBy: "-")
                self.txt_Code.text = phone[0]
                if phone[1] != ""{
                    self.txt_PhoneNumber.text = phone[1]
                }else{
                    self.txt_PhoneNumber.text = ""
                }
                
            }
        }
    }
    
    //MARK:TextField Delegate and DataSource------------------------------------------------------------------
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        if textField.text == txt_Code.text{
            self.hideKeyboardWhenTappedAround()
            let countryView = CountrySelectView.shared
            countryView.barTintColor = .red
            countryView.displayLanguage = .english
            countryView.show()
            countryView.selectedCountryCallBack = { (countryDic) -> Void in
                //    self.countrycode = "+\(countryDic["code"] as! NSNumber)"
                
                
                self.txt_Code.text = "+\(countryDic["code"] as! NSNumber)"
                
            }
            
        }
    }
    //MARK:IBACtions------------------------------------------------------------------
    @IBAction func btn_Back(sendr:UIButton){
        
        self.dismiss(animated: true, completion: nil)
        //  self.navigationController?.popViewController(animated: true)
        
        
    }
    @IBAction func btn_UserAdd(sendr:UIButton){
        
        if editManageUsers == "EditUsers"{
            self.editManageUSerApi()
        }else{
            self.addManageUSerApi()
        }
        
    }
    
    //MARK:Function Api's------------------------
    
    //mark:-Add Manage User Api
    func addManageUSerApi(){
        var trimPhoneNumber = String()
       
        trimPhoneNumber = self.txt_PhoneNumber.text ?? "" 
        while (trimPhoneNumber.hasPrefix("0")) {
            trimPhoneNumber.remove(at: (trimPhoneNumber.startIndex))
        }
        if (self.txt_FName.text == ""){
            AKAlertController.alert("", message: "Please Enter First Name")
        }else if (self.txt_LName.text == ""){
            AKAlertController.alert("", message: "Please Enter Last Name")
        } else if (self.txt_PhoneNumber.text == ""){
            AKAlertController.alert("", message: "Please Enter Contact")
            
        }
        else if (trimPhoneNumber.count > 10){
            AKAlertController.alert("", message: "Please Enter Valid Contact")
            
        }else{
            let countryCode = self.txt_Code.text ?? "+61"
            let fullPhoneNumber =  (countryCode + "-" + (trimPhoneNumber ))
            KVNProgress.show()
           
            Network.shared.addManageUsers(org_id: org_id
            , email: self.txt_Email.text ?? "",first_name: self.txt_FName.text ?? "",last_name: self.txt_LName.text ?? "", password: "", mobile_number: fullPhoneNumber ) { (result) in
                KVNProgress.dismiss()
                guard let user = result else {
                    return
                }
                if user.success == true{
                    self.showSingleAlertMessage(message: "", subMsg: user.message ?? "", sender: self, completion:
                        { (success) -> Void in
                            if success == true{
                                
                                self.dismiss(animated: true, completion: nil)
                                
                            }
                    })
                }else{
                    self.showSingleAlertMessage(message: "", subMsg: user.message ?? "", sender: self, completion:
                        { (success) -> Void in
                            if success == true{
                                
                                
                                
                            }
                    })
                }
                
            }
        }
        
        
    }
    func editManageUSerApi(){
        
        var trimPhoneNumber = String()
        trimPhoneNumber =  self.txt_PhoneNumber.text ?? ""
        while (trimPhoneNumber.hasPrefix("0")) {
            trimPhoneNumber.remove(at: (trimPhoneNumber.startIndex))
        }
        
        if (self.txt_FName.text == ""){
            AKAlertController.alert("", message: "Please Enter First Name")
        }else if (self.txt_LName.text == ""){
            AKAlertController.alert("", message: "Please Enter Last Name")
        }
        else if (self.txt_PhoneNumber.text == ""){
            AKAlertController.alert("", message: "Please Enter Contact")
            
        }
        else if (trimPhoneNumber.count > 10){
            AKAlertController.alert("", message: "Please Enter Valid Contact")
            
        }else{
            let countryCode = self.txt_Code.text ?? "+61"
            
            KVNProgress.show()
            let fullPhoneNumber =  (countryCode + "-" + (trimPhoneNumber ))
            Network.shared.editManageUsers(org_id: org_id
            , email: self.txt_Email.text ?? "",first_name: self.txt_FName.text ?? "",last_name: self.txt_LName.text ?? "" ,id: org_id, mobile_number: fullPhoneNumber ) { (result) in
                KVNProgress.dismiss()
                guard let user = result else {
                    return
                }
                if user.success == true{
                    self.showSingleAlertMessage(message: "", subMsg: user.message ?? "", sender: self, completion:
                        { (success) -> Void in
                            if success == true{
                                
                                self.dismiss(animated: true, completion: nil)
                                
                            }
                    })
                }else{
                    self.showSingleAlertMessage(message: "", subMsg: user.message ?? "", sender: self, completion:
                        { (success) -> Void in
                            if success == true{
                                
                                
                                
                            }
                    })
                }
                
            }
        } }
    
    
    func trimString(str: String) -> String{
           return str.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
       }
}
