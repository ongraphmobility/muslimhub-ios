//
//  CreateUserViewController.swift
//  Muslim Hub
//
//  Created by Ongraph on 31/07/19.
//  Copyright © 2019 OnGraph. All rights reserved.
//

import UIKit
import KVNProgress
import  CountryPickerViewSwift
import DropDown

class CreateUserViewController: UIViewController,UITextFieldDelegate {
    

    @IBOutlet weak var txt_FullName:UITextField!
    @IBOutlet weak var txt_LastName:UITextField!
    @IBOutlet weak var txt_Email:UITextField!
    @IBOutlet weak var txt_Code:UITextField!
    @IBOutlet weak var txt_PhoneNumber:UITextField!
    @IBOutlet weak var txt_FontSize:UITextField!
    @IBOutlet weak var txt_Language:UITextField!
    @IBOutlet weak var btn_USerProfileEdit:UIButton!
  
    @IBOutlet weak var btn_UserPreferences_Lang:UIButton!
    @IBOutlet weak var btn_UserPreferences_Font:UIButton!
    @IBOutlet weak var btn_UserPreferencesEdit:UIButton!
    @IBOutlet weak var btn_CreateUser:UIButton!
    @IBOutlet weak var lbl_UserPreferences_Title:UILabel!
   
    @IBOutlet weak var lbl_UserProfile_Title:UILabel!
    var isFontChanged = false
    var fromHome: String!
    
    var isFirstTimeFontSize = false
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        if #available(iOS 13.0, *) {
                           statusBarColorChange(color:UIColor.init(hex:  0x05BDB9))
                       } else {
                           UIApplication.statusBarBackgroundColor = UIColor.init(hex: 0x05BDB9)
                       }
        self.txt_Code.delegate = self
       //self.txt_PhoneNumber.text = "9971807131"
       // btn_UserPreferences_Lang.isUserInteractionEnabled = false
        //btn_UserPreferences_Font.isUserInteractionEnabled = false
        if let fontSize = UserDefaults.standard.value(forKey: "FontSize") as? String{
            
            if fontSize == "Small"{
                self.txt_FontSize.text = "Small".localized
            }else if fontSize == "Medium"{
                self.txt_FontSize.text = "Medium".localized
            }else if fontSize == "Large"{
                self.txt_FontSize.text = "Large".localized
            }
        }
        if let langugae = UserDefaults.standard.value(forKey: Constant.LANGUAGE) as? String{
            
            if langugae == "ar"{
                self.txt_Language.text = "Arabic".localized
            }else if langugae == "ms"{
                self.txt_Language.text = "Malay".localized
            }else if langugae == "en"{
                self.txt_Language.text = "English".localized
            }
        }
        
        
        lbl_UserPreferences_Title.text = "User Preferences".localized
        lbl_UserProfile_Title.text = "User Profile".localized
        
        self.getProfileApi()
    }
    
    //MARK:TextField Delegate and DataSource------------------------------------------------------------------
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        if textField.text == txt_Code.text{
            let countryView = CountrySelectView.shared
            countryView.barTintColor = .red
            countryView.displayLanguage = .english
            countryView.show()
            countryView.selectedCountryCallBack = { (countryDic) -> Void in
                //    self.countrycode = "+\(countryDic["code"] as! NSNumber)"
                
                
                self.txt_Code.text = "+\(countryDic["code"] as! NSNumber)"
                
            }
            
        }
    }
    
    
    //MARK:IBAction---------------------
    @IBAction func btn_UserPreference(sendr:UIButton){
        btn_UserPreferences_Lang.isUserInteractionEnabled = true
        btn_UserPreferences_Font.isUserInteractionEnabled = true
        
        let dropDown = DropDown()
        
        if sendr.tag == 2{
            
            dropDown.anchorView = self.txt_Language
            dropDown.bottomOffset = CGPoint(x: 50, y:(dropDown.anchorView?.plainView.bounds.height)!)
            
            // The list of items to display. Can be changed dynamically
            dropDown.dataSource = ["Arabic    ".localized, "Malay    ".localized,"English   ".localized]
            dropDown.textColor = UIColor.black
            dropDown.backgroundColor = UIColor.white
            dropDown.selectionAction = { [unowned self] (index: Int, item: String) in
                print("Selected item: \(item) at index: \(index)")
                
                if index == 0{
                    //mark:for editing value-------------------------------------------------
                                        UserDefaults.standard.set(["ar"], forKey: "AppleLanguages")
                    
                                        UserDefaults.standard.set("ar", forKey: Constant.LANGUAGE)
                                        UserDefaults.standard.synchronize()
                    
                                        self.showChangeLanguageAlert(language:"ar")
                                        self.txt_Language.text = "Arabic".localized
                    
                }else if index == 1{
                    //mark:for delete value-------------------------------------------------
                    
                                        UserDefaults.standard.set(["ms"], forKey: "AppleLanguages")
                    
                                        UserDefaults.standard.set("ms", forKey: Constant.LANGUAGE)
                                        UserDefaults.standard.synchronize()
                                        self.showChangeLanguageAlert(language:"ms")
                                        self.txt_Language.text = "Malay".localized
                    
                }else if index == 2{
                    //mark:for delete value-------------------------------------------------
                                        self.isFontChanged = true
                                        self.txt_Language.text = "English".localized
                    
                                        UserDefaults.standard.set(["en"], forKey: "AppleLanguages")
                    
                                        UserDefaults.standard.set("en", forKey: Constant.LANGUAGE)
                                        UserDefaults.standard.synchronize()
                                        self.showChangeLanguageAlert(language:"en")
                }
            }
            dropDown.show()
        }else if sendr.tag == 1{
            
            
            dropDown.anchorView = self.txt_FontSize
            dropDown.bottomOffset = CGPoint(x: 50, y:(dropDown.anchorView?.plainView.bounds.height)!)
            
            // The list of items to display. Can be changed dynamically
            dropDown.dataSource = ["Small    ".localized, "Medium    ".localized,"Large   ".localized]
            dropDown.textColor = UIColor.black
            dropDown.backgroundColor = UIColor.white
            dropDown.selectionAction = { [unowned self] (index: Int, item: String) in
                print("Selected item: \(item) at index: \(index)")
                if let newFontSize = UserDefaults.standard.value(forKey: "FontSize") as? String
                {
                    
                    UserDefaults.standard.set(newFontSize, forKey: "Current_FontSize")
                    
                }
                if index == 0{
                    //mark:for editing value-------------------------------------------------
                    
                    UserDefaults.standard.set("Small", forKey: "FontSize")
                    self.txt_FontSize.text = "Small".localized
                }else if index == 1{
                    //mark:for delete value-------------------------------------------------
                    self.isFontChanged = true
                    self.txt_FontSize.text = "Medium".localized
                    //  UserDefaults.standard.set(newFontSize, forKey: "Current_FontSize")
                    UserDefaults.standard.set(nil, forKey: "FontSize")
                }else if index == 2{
                    //mark:for delete value-------------------------------------------------
                    self.isFontChanged = true
                    self.txt_FontSize.text = "Large".localized
                    
                    UserDefaults.standard.set("Large", forKey: "FontSize")
                }
                
                
                
            }
            dropDown.show()
            
        }
        
    }
    
    @IBAction func btn_UserProfile(sender:UIButton){
        
        if sender.tag == 3{
            if sender.isSelected{
                sender.isSelected = false
                txt_FullName.isUserInteractionEnabled = false
                txt_LastName.isUserInteractionEnabled = false
                txt_Email.isUserInteractionEnabled = false
                txt_Code.isUserInteractionEnabled = false
                txt_PhoneNumber.isUserInteractionEnabled = false
                btn_USerProfileEdit.setImage(#imageLiteral(resourceName: "edit_profile_iPhone"), for: .normal)
               self.editCreateUserAPi()
            }else{
                sender.isSelected = true
                txt_FullName.isUserInteractionEnabled = true
                txt_LastName.isUserInteractionEnabled = true
                txt_Email.isUserInteractionEnabled = true
                txt_Code.isUserInteractionEnabled = true
                txt_PhoneNumber.isUserInteractionEnabled = true
                btn_USerProfileEdit.setImage(#imageLiteral(resourceName: "edit_profile_tick_iPhone"), for: .normal)
            }
        }else if sender.tag == 4{
            
            if sender.isSelected{
                sender.isSelected = false
                btn_UserPreferences_Lang.isUserInteractionEnabled = false
                btn_UserPreferences_Font.isUserInteractionEnabled = false
                btn_UserPreferencesEdit.setImage(#imageLiteral(resourceName: "edit_profile_iPhone"), for: .normal)
               AppDelegate.instance.sideMenu()
//                let vc = self.storyboard?.instantiateViewController(withIdentifier: "TabVC") as! TabVC
//                vc.currentFontSize = self.txt_FontSize.text ?? ""
//                self.present(vc, animated: true, completion: nil)
            }else{
                sender.isSelected = true
                btn_UserPreferences_Lang.isUserInteractionEnabled = true
                btn_UserPreferences_Font.isUserInteractionEnabled = true
                
                btn_UserPreferencesEdit.setImage(#imageLiteral(resourceName: "edit_profile_tick_iPhone"), for: .normal)
                
            }
        }
        
    }
    
    
    func showChangeLanguageAlert(language:String) {
        
        //    AKAlertController.alert("Alert", message: "You have selected a new language, press ok to continue".localized, buttons: ["Cancel".localized, "Ok".localized]) { (_, index) in
        //            guard index == 1 else { return }
        
        UserDefaults.standard.set([language], forKey: "AppleLanguages")
        
        UserDefaults.standard.set(language, forKey: Constant.LANGUAGE)
        UserDefaults.standard.synchronize()
        //            if language == "ar" {
        //                UIView.appearance().semanticContentAttribute = .forceLeftToRight
        //            }else{
        //                UIView.appearance().semanticContentAttribute = .forceLeftToRight
        //            }
        UIView.appearance().semanticContentAttribute = .forceLeftToRight
        
        AppDelegate.instance.sideMenu()
        //}
    }
    @IBAction func btn_Back(sender:UIButton){
        
    self.dismiss(animated: true, completion: nil)
        
    }
    
    @IBAction func btn_CreateUser(sender:UIButton)
    {
        guard !txt_FullName.text!.isEmpty else {
            AKAlertController.alert("", message: "Please enter your first name")
            return
        }
        
        guard !txt_LastName.text!.isEmpty else {
            AKAlertController.alert("", message: "Please enter your last name")
            return
        }
//        guard !txt_Email.text!.isEmpty else {
//            AKAlertController.alert("", message: "Please enter your email")
//            return
//        }
        guard !txt_PhoneNumber.text!.isEmpty else {
            AKAlertController.alert("", message: "Please enter your mobile number")
            return
        }
     
        if (txt_PhoneNumber.text?.count ?? 0 > 10){
            AKAlertController.alert("", message: "Please Enter Valid Organization Contact")
        }
       
//        else if ((txt_Email.text?.validateEmail())!) == false{
//                       AKAlertController.alert("", message: "Please Enter Valid Email Id")
//        }
//

        else{
             self.createUserAPi()
//            if btn_CreateUser.titleLabel?.text == "Save Profile"{
//               self.editCreateUserAPi()
//            }else{
//                 self.createUserAPi()
//            }
         
        }
       
    }
    
    //MARK:Api Functions----------------
    func createUserAPi(){
        
        let numb = self.txt_PhoneNumber.text ?? ""
        let code = self.txt_Code.text ?? "+61"
        let fullPhoneNumber = (code + "-" + numb)
        KVNProgress.show()
        Network.shared.createUserLogin(first_name: self.txt_FullName.text ?? "", last_name: self.txt_LastName.text ?? "", email: self.txt_Email.text ?? "", mobile_number: fullPhoneNumber) { (result) in
        KVNProgress.dismiss()
            guard let createUser = result else{
                return
            }
            
           
            if createUser.success == true{
                
                self.showSingleAlertMessage(message:createUser.message ?? "", subMsg: "", sender: self, completion:
                    { (success) -> Void in
                      
                        let vc = self.storyboard?.instantiateViewController(withIdentifier: "OTPViewController") as! OTPViewController
                        vc.modalPresentationStyle = .fullScreen
                       self.present(vc, animated: true, completion: nil)
                })
               
            }else{
                     self.showSingleAlertMessage(message:createUser.message ?? "", subMsg: "", sender: self, completion:
                               { (success) -> Void in
                                 
                                  
                           })
            }
        }
    }
    
    func editCreateUserAPi(){
        
        let numb = self.txt_PhoneNumber.text ?? ""
        let code = self.txt_Code.text ?? "+61"
        let fullPhoneNumber = (code + "-" + numb)
        KVNProgress.show()
        Network.shared.editCreateUserLogin(first_name: self.txt_FullName.text ?? "", last_name: self.txt_LastName.text ?? "", email: self.txt_Email.text ?? "", mobile_number: fullPhoneNumber) { (result) in
            KVNProgress.dismiss()
            guard let createUser = result else{
                return
            }
            
            
            if createUser.success == true{
                
                self.showSingleAlertMessage(message:createUser.message ?? "", subMsg: "", sender: self, completion:
                    { (success) -> Void in
                        
                        let vc = self.storyboard?.instantiateViewController(withIdentifier: "TabVC") as! TabVC
                        vc.modalPresentationStyle = .fullScreen
                        self.present(vc, animated: true, completion: nil)
                })
                
            }else{
                self.showSingleAlertMessage(message:createUser.message ?? "", subMsg: "", sender: self, completion:
                    { (success) -> Void in
                     
                })
            }
        }
    }
    
    func getProfileApi(){
        
        Network.shared.getCreateUser { (result) in
            KVNProgress.dismiss()
            guard result != nil else{
                return
            }
            
            if result?.data?.has_profile == true{
                self.btn_USerProfileEdit.isHidden = false
                self.btn_UserPreferencesEdit.isHidden = false
                self.txt_FullName.isUserInteractionEnabled = false
                self.txt_LastName.isUserInteractionEnabled = false
                self.txt_Email.isUserInteractionEnabled = false
                self.txt_Code.isUserInteractionEnabled = false
                self.txt_PhoneNumber.isUserInteractionEnabled = false
                self.btn_USerProfileEdit.setImage(#imageLiteral(resourceName: "edit_profile_iPhone"), for: .normal)
                
                self.txt_FullName.text = result?.data?.firstName ?? ""
                self.txt_LastName.text = result?.data?.lastName ?? ""
                self.txt_Email.text = result?.data?.email ?? ""
                self.btn_CreateUser.isHidden = true
                self.btn_CreateUser.setTitle("Save Profile", for: .normal)
                if result?.data?.mobileNumber ?? "" != ""{
                    
                    let phone =  result?.data?.mobileNumber?.components(separatedBy: "-")
                    self.txt_Code.text = phone?[0] ?? "+61"
                    self.txt_PhoneNumber.text = phone?[1] ?? ""
                    
                    UserDefaults.standard.set(result?.data?.has_profile, forKey: "hasProfile")
                    UserDefaults.standard.set(result?.data?.firstName, forKey: "userName")
                    UserDefaults.standard.set(result?.data?.email, forKey: "userEmail")
                }
              
                
            }else{
                self.btn_CreateUser.isHidden = false
                self.btn_USerProfileEdit.isHidden = true
                self.btn_UserPreferencesEdit.isHidden = false
                 self.btn_CreateUser.setTitle("Create Profile", for: .normal)
            }
        }
    }
}
