//
//  OTPViewController.swift
//  Muslim Hub
//
//  Created by Ongraph on 29/08/19.
//  Copyright © 2019 OnGraph. All rights reserved.
//

import UIKit
import KVNProgress
import IQKeyboardManagerSwift

class OTPViewController: UIViewController,UITextFieldDelegate {
    
    @IBOutlet weak var view_Header:UIView!
    @IBOutlet weak var lbl_HaederTitle:UILabel!
    @IBOutlet weak var lbl_OneTimeTitle:UILabel!
    @IBOutlet weak var btn_Verify:UIButton!
    @IBOutlet weak var btn_Resend:UIButton!
    
    @IBOutlet weak var txt_Otp:UITextField!
    @IBOutlet weak var txt_Otp1:UITextField!
    @IBOutlet weak var txt_Otp2:UITextField!
    @IBOutlet weak var txt_Otp3:UITextField!
    @IBOutlet weak var txt_Otp4:UITextField!
    @IBOutlet weak var txt_Otp5:UITextField!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        if #available(iOS 13.0, *) {
            statusBarColorChange(color:UIColor.init(hex: 0x0DC9E4))
        } else {
            UIApplication.statusBarBackgroundColor = UIColor.init(hex: 0x0DC9E4)
        }
        txt_Otp.delegate = self
        txt_Otp1.delegate = self
        txt_Otp2.delegate = self
        txt_Otp3.delegate = self
        txt_Otp4.delegate = self
        txt_Otp5.delegate = self
        self.hideKeyboardWhenTappedAround()
        IQKeyboardManager.shared.disabledToolbarClasses = [OTPViewController.self]
        btn_Verify.shadowWithCornerReadius(cornerRadius: 10)
        view_Header.shadowBlackToHeader()
        if #available(iOS 12.0, *) {
            txt_Otp.textContentType = .oneTimeCode
        } else {
            // Fallback on earlier versions
        }
        txt_Otp.becomeFirstResponder()
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        return true
    }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        // Fill your textfields here
        if ((textField.text?.count)! < 1) && (string.count > 0){
            
            if textField == txt_Otp{
                
                txt_Otp1.becomeFirstResponder()
            }
            if textField == txt_Otp1{
                
                txt_Otp2.becomeFirstResponder()
            }
            if textField == txt_Otp2{
                
                txt_Otp3.becomeFirstResponder()
            }
            if textField == txt_Otp3{
                
                txt_Otp4.becomeFirstResponder()
            }
            if textField == txt_Otp4{
                
                txt_Otp5.becomeFirstResponder()
            }
            if textField == txt_Otp5{
                
                txt_Otp5.resignFirstResponder()
            }
            textField.text = string
            return false
        }else if ((textField.text?.count ?? 0 >= 1) && (string.count == 0)){
            
            if textField == txt_Otp1{
                
                txt_Otp2.becomeFirstResponder()
            }
            if textField == txt_Otp2{
                
                txt_Otp3.becomeFirstResponder()
            }
            if textField == txt_Otp3{
                
                txt_Otp4.becomeFirstResponder()
            }
            if textField == txt_Otp4{
                
                txt_Otp5.becomeFirstResponder()
            }
            if textField == txt_Otp5{
                
                txt_Otp5.resignFirstResponder()
            }
            textField.text = ""
            return false
        }else if (textField.text?.count)! >= 1{
            
            textField.text = string
            return false
        }
        return true
    }
    @IBAction func btn_Back(sender:UIButton){
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func btn_otpVerify(sender:UIButton){
        
        self.otpVerifyAPi()
        
        
    }
    @IBAction func btn_Resend(sender:UIButton){
        
        
        self.resendOtpVerifyAPi()
        
    }
    
    //MARK:Api Functions----------------
    func otpVerifyAPi(){
        
        let textFieldArray = [txt_Otp.text!,txt_Otp1.text!,txt_Otp2.text!,txt_Otp3.text!,txt_Otp4.text!,txt_Otp5.text!]
        
        let stringRepresentation = textFieldArray.joined(separator:"")
        let otp = Int(stringRepresentation) ?? 0
        KVNProgress.show()
        Network.shared.otpVerification(otp:otp) { (result) in
            KVNProgress.dismiss()
            
            
            if result?.success == true{
                
                self.showSingleAlertMessage(message:result?.message ?? "", subMsg: "", sender: self, completion:
                    { (success) -> Void in
                        
                        let vc = self.storyboard?.instantiateViewController(withIdentifier: "TabVC") as! TabVC
                        vc.modalPresentationStyle = .fullScreen
                        self.present(vc, animated: true, completion: nil)
                })
                
            }else{
                
                self.showSingleAlertMessage(message:result?.message ?? "", subMsg: "", sender: self, completion:
                    { (success) -> Void in
                        
                        
                })
            }
        }
    }
    
    func resendOtpVerifyAPi(){
        
        KVNProgress.show()
        Network.shared.getResendOtp{ (result) in
            KVNProgress.dismiss()
            
            
            if result?.success == true{
                
                self.showSingleAlertMessage(message:result?.message ?? "", subMsg: "", sender: self, completion:
                    { (success) -> Void in
                        
                        self.txt_Otp.becomeFirstResponder()
                })
                
            }else{
                
                AKAlertController.alert(result?.message ?? "")
            }
        }
    }
}
