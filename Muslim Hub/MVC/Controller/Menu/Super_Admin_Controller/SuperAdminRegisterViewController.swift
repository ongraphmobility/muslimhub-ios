//
//  SuperAdminRegisterViewController.swift
//  Muslim Hub
//
//  Created by Sakshi Singh on 22/04/19.
//  Copyright © 2019 OnGraph. All rights reserved.
//

import UIKit
import CountryPickerViewSwift
import GooglePlaces
import GooglePlacePicker
import KVNProgress
import IQKeyboardManagerSwift



class SuperAdminRegisterViewController: UIViewController,UITextFieldDelegate,addServiceDelegate,UITextViewDelegate {
    @IBOutlet weak var tableview_RegisterOrg:UITableView!
    @IBOutlet weak var view_TableViewLastCell:UIView!
    @IBOutlet weak var btn_Yes:UIButton!
    @IBOutlet weak var btn_No:UIButton!
    @IBOutlet weak var btn_txtDeductableYes:UIButton!
    @IBOutlet weak var btn_txtDeductableNo:UIButton!
    @IBOutlet weak var btn_AddServices:UIButton!
    @IBOutlet weak var btn_Privateorg:UIButton!
    @IBOutlet weak var btn_Register:UIButton!
    @IBOutlet weak var view_Header:UIView!
    @IBOutlet weak var lbl_HeaderTitle:UILabel!
    @IBOutlet weak var lbl_EnableDonationTitle:UILabel!
    @IBOutlet weak var lbl_PrivateOrgTitle:UILabel!
    @IBOutlet weak var lbl_TaxDeductableTitle:UILabel!
    @IBOutlet weak var lbl_ServiceProvidedTitle:UILabel!
    
    @IBOutlet weak var view_UpperTableView:UIView!
    @IBOutlet weak var txt_MerchantCode:UITextField!
    @IBOutlet weak var txt_AuthenticationCode:UITextField!
    
    
    
    var countrycode:String!
    var selectedServices = [Int]()
    var dic_RegisterValue = [String:Any]()
    var donationOrg = false
    var OrgType:String!
    var isPrivate:Bool!
    var txtDeductable:Bool!
    var tag_forAddress:Int!
    var lat = Double()
    var long = Double()
    var latitude_str = ""
    var longituted_str = ""
    //mark:Location variables-----------
    var placesClient: GMSPlacesClient!
    
    
    
    let array_Org = ["Organization Name","Organization abbreviation","Address","Org Contact","Org Email","Org URL","Org Type","Registration No","Description","Admin First Name","Admin Last Name","Admin Mobile Number","Admin Email","Password","Confirm Password"]
    
    let array_Fill = ["ongraph","OGg","fhfhf","9897897890","sakshisingh212@gmail.com","http://www.google.com","mosque","878","hjgjh","jkhj","jgjg","9086890789","sakshisingh212@gmail.com","d","d"]
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        if #available(iOS 13.0, *) {
            statusBarColorChange(color:UIColor.init(hex: 0x0DC9E4))
        } else {
            UIApplication.statusBarBackgroundColor = UIColor.init(hex: 0x0DC9E4)
        }
        
        let center = CLLocationCoordinate2D(latitude: -33.865143, longitude: 151.2099)
        let northEast = CLLocationCoordinate2D(latitude: center.latitude + 0.001,
                                               longitude: center.longitude + 0.001)
        let southWest = CLLocationCoordinate2D(latitude: center.latitude - 0.001,
                                               longitude: center.longitude - 0.001)
        _ = GMSCoordinateBounds(coordinate: northEast, coordinate: southWest)
        
        self.dic_RegisterValue.updateValue("Description".localized, forKey:"Description")
        btn_No.backgroundColor = UIColor.init(hex: 0x0BC9E4)
        btn_Yes.backgroundColor = UIColor.white
        btn_Yes.setTitleColor(UIColor.darkGray, for: .normal)
        btn_No.setTitleColor(UIColor.white, for: .normal)
        
        self.txt_MerchantCode.isHidden = true
        self.txt_AuthenticationCode.isHidden = true
        
        IQKeyboardManager.shared.previousNextDisplayMode = .alwaysHide
        lbl_HeaderTitle.text = "Onboard Registeration - MH".localized
        lbl_EnableDonationTitle.text = "Enable donation for ORG?".localized
        lbl_PrivateOrgTitle.text = "Private Org".localized
        lbl_TaxDeductableTitle.text = "Tax Deductable".localized
        lbl_ServiceProvidedTitle.text = "Services Provided".localized
        btn_AddServices.setTitle("Add Services".localized, for: .normal)
        self.txt_MerchantCode.placeholder = "Merchant Code".localized
        self.txt_AuthenticationCode.placeholder = "Authentication Code".localized
        self.btn_Register.setTitle("Register".localized, for: .normal)
        self.btn_Yes.setTitle("Yes".localized, for: .normal)
        self.btn_No.setTitle("No".localized, for: .normal)
        self.btn_txtDeductableYes.setTitle("Yes".localized, for: .normal)
        self.btn_txtDeductableNo.setTitle("No".localized, for: .normal)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.cornerRadiusShadow()
        self.hideKeyboardWhenTappedAround()
    }
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        guard let indexPath = textView.tableViewIndexPath(self.tableview_RegisterOrg) else { return }
        
        switch (indexPath.section, 8) {
            
        case (0,8):
            
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "MHDescViewController") as! MHDescViewController
            vc.delegate = self
            vc.textDesc = textView.text!
            textView.keyboardType = .default
            vc.modalPresentationStyle = .fullScreen
            present(vc, animated: false, completion: nil)
        default:
            break
            
        }
        
    }
    
    
    func textViewDidEndEditing(_ textView: UITextView) {
        
        textView.textColor = UIColor.black
        
    }
    
    
    //MARK:TextField Delegate and DataSource Method--------------------
    func textFieldDidBeginEditing(_ textField: UITextField) {
        guard let indexPath = textField.tableViewIndexPath(self.tableview_RegisterOrg) else { return }
        switch (indexPath.section, indexPath.row) {
            
        case (0,2):
            
            
            let autocompleteController = GMSAutocompleteViewController()
            autocompleteController.delegate = self
            let filter = GMSAutocompleteFilter()
            filter.type = .noFilter  //suitable filter type
            //  filter.country = "AUS"  //appropriate country code
            autocompleteController.autocompleteFilter = filter
            self.tag_forAddress = 2
            present(autocompleteController, animated: true, completion: nil)
            
            
        case (0,3):
            
            textField.keyboardType = .numberPad
        case (0,4):
            textField.keyboardType = .emailAddress
        case (0,5):
            textField.keyboardType = .URL
            self.dic_RegisterValue.updateValue(textField.text as Any, forKey: "Org URL")
        case (0,6):
            
            let arr_OrgType  = ["mosque", "charity", "community","educational"]
            //            AKMultiPicker().openPickerIn(textField, firstComponentArray: arr_OrgType) { (value1, _, index1, _) in
            //                self.OrgType = value1
            //                self.dic_RegisterValue.updateValue(self.OrgType, forKey: "Org Type")
            //                textField.endEditing(true)
            //                textField.resignFirstResponder()
            //
            //            }
            PickerViewController.openPickerView(viewC: self, dataArray: arr_OrgType) { (value, index) in
                print(value)
                
                textField.text = value
                self.OrgType = value
                self.dic_RegisterValue.updateValue(self.OrgType, forKey: "Org Type")
            }
        case (0,7):
            
            textField.keyboardType = .numberPad
            
        case (0,11):
            
            textField.keyboardType = .numberPad
        case (0,12):
            textField.keyboardType = .emailAddress
        case (0,13):
            dic_RegisterValue.updateValue(textField.text as AnyObject, forKey: array_Org[13])
        case (0,14):
            dic_RegisterValue.updateValue(textField.text as AnyObject, forKey: array_Org[14])
            
        default:
            break
        }
    }
    
    
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        guard let indexPath = textField.tableViewIndexPath(self.tableview_RegisterOrg) else { return }
        
        switch (indexPath.section, indexPath.row) {
            //        case (0,0):
            //
            //            if (textField.text?.isEmpty)!{
            //                AKAlertController.alert("Alert", message: "Please Enter Organization Name")
            //            }
            //        case (0,1):
            //
            //            if (textField.text?.isEmpty)!{
            //                AKAlertController.alert("Alert", message: "Please Enter Organization abbreviation")
            //            }
            
            
        case (0, 3):
            
            var org_Contact = textField.text ?? ""
            while (org_Contact.hasPrefix("0")) {
                org_Contact.remove(at: (org_Contact.startIndex))
            }
            
            if (org_Contact.count > 10){
                AKAlertController.alert("", message: "Please Enter Valid Organization Contact")
            }
            
        case (0, 4):
            
            if ((textField.text?.validateEmail())!) == false{
                AKAlertController.alert("", message: "Please Enter Valid Email Id")
            }
            //            //else if (textField.text?.isEmpty)!{
            //                AKAlertController.alert("Alert", message: "Please Enter Email Id")
        //            }
        case ( 0, 5):
            
            if (textField.text?.isValidURL)! == false {
                AKAlertController.alert("", message: "Please Enter Valid Url")
            }
            
            
            
            //        case (0, 9):
            //            if (textField.text?.isEmpty)!{
            //                AKAlertController.alert("Alert", message: "Please Enter Admin First Name")
            //            }
            //        case (0, 10):
            //            if (textField.text?.isEmpty)!{
            //                AKAlertController.alert("Alert", message: "Please Enter Admin Last Name")
        //            }
        case (0, 11):
            var admin_Contact = textField.text ?? ""
            while (admin_Contact.hasPrefix("0")) {
                admin_Contact.remove(at: (admin_Contact.startIndex))
            }
            
            if (admin_Contact.count > 10){
                AKAlertController.alert("", message: "Please Enter Valid Admin Mobile Number")
            }
        case (0, 12):
            
            if ((textField.text?.validateEmail())!) == false{
                AKAlertController.alert("", message: "Please Enter Valid Admin Email Id")
            }
            //            else if (textField.text?.isEmpty)!{
            //                AKAlertController.alert("Alert", message: "Please Enter Admin Email Id")
            //            }
            //        case (0, 13):
            //
            //            if (textField.text?.isEmpty)!{
            //                AKAlertController.alert("Alert", message: "Please Enter Password")
            //            }
            //        case (0, 14):
            //
            //            if (textField.text?.isEmpty)!{
            //                AKAlertController.alert("Alert", message: "Please Enter Confirm")
        //            }
        default:
            break
        }
        if dic_RegisterValue[array_Org[textField.tag]] == nil{
            
            self.dic_RegisterValue.updateValue("", forKey:array_Org[textField.tag])
        }
        let index = IndexPath(row: textField.tag, section: 0)
        let cell: SuperAdminRegisterTableViewCell = self.tableview_RegisterOrg.cellForRow(at: index) as! SuperAdminRegisterTableViewCell
        
        
        
        dic_RegisterValue.updateValue(cell.txt_FillForm.text as AnyObject, forKey: array_Org[textField.tag])
        
        //mark:Condition for appending mobile Number--------------
        let key = array_Org[textField.tag]
        if key == "Org Contact" || key == "Admin Mobile Number"{
            
            let countryCode = cell.lbl_countryCodeLabel.text ?? "+61"
            
            let mobileNumber = dic_RegisterValue[key] as? String
            let mobileNumberWithCode = (countryCode + "-" + mobileNumber!)
            
            dic_RegisterValue.updateValue(mobileNumberWithCode, forKey: array_Org[textField.tag])
            
        }
        
        tableview_RegisterOrg.reloadData()
        
    }
    
    
    //MARK:IBACtion----------------------------------------
    @IBAction func btn_Back(sendr:UIButton){
        
        if let controller = self.storyboard?.instantiateViewController(withIdentifier: "TabVC") {
            self.slideMenuController()?.changeMainViewController(controller, close: true)
        }
    }
    @IBAction func btn_PrivateOrg(sender:UIButton){
        
        if btn_Privateorg.isSelected{
            btn_Privateorg.isSelected = false
            //  btn_Privateorg.backgroundColor = UIColor.white
            self.isPrivate = false
            btn_Privateorg.setBackgroundImage(#imageLiteral(resourceName: "uncheckbox_iPhone"), for: .normal)
        }else{
            btn_Privateorg.isSelected = true
            self.isPrivate = true
            // btn_Privateorg.backgroundColor = UIColor.init(hex: 0x0BC9E4)
            btn_Privateorg.setBackgroundImage(#imageLiteral(resourceName: "checkbox_iPhone"), for: .normal)
        }
    }
    
    @IBAction func btn_AddServices(sender:UIButton){
        
        if let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "AlertAddServicesViewController") as? AlertAddServicesViewController
        {
            vc.servicesDelegate = self
            vc.modalPresentationStyle = .overCurrentContext
            present(vc, animated: true, completion: nil)
        }
    }
    @IBAction func btn_EnableDonationOrg(sender:UIButton){
        
        if sender.tag == 0{
            
            btn_Yes.backgroundColor = UIColor.init(hex: 0x0BC9E4)
            self.donationOrg = true
            btn_No.backgroundColor = UIColor.white
            btn_No.setTitleColor(UIColor.darkGray, for: .normal)
            btn_Yes.setTitleColor(UIColor.white, for: .normal)
            self.txt_MerchantCode.isHidden = false
            self.txt_AuthenticationCode.isHidden = false
        }else if sender.tag == 1{
            btn_No.backgroundColor = UIColor.init(hex: 0x0BC9E4)
            self.donationOrg = false
            btn_Yes.backgroundColor = UIColor.white
            btn_Yes.setTitleColor(UIColor.darkGray, for: .normal)
            btn_No.setTitleColor(UIColor.white, for: .normal)
            self.txt_MerchantCode.isHidden = true
            self.txt_AuthenticationCode.isHidden = true
        }
    }
    @IBAction func btn_TextDeductale(sender:UIButton){
        
        if sender.tag == 2{
            
            btn_txtDeductableYes.backgroundColor = UIColor.init(hex: 0x0BC9E4)
            btn_txtDeductableYes.setTitleColor(UIColor.white, for: .normal)
            self.txtDeductable = true
            btn_txtDeductableNo.backgroundColor = UIColor.white
            btn_txtDeductableNo.setTitleColor(UIColor.darkGray, for: .normal)
            
        }else if sender.tag == 3{
            btn_txtDeductableNo.backgroundColor = UIColor.init(hex: 0x0BC9E4)
            self.txtDeductable = false
            btn_txtDeductableYes.backgroundColor = UIColor.white
            btn_txtDeductableNo.setTitleColor(UIColor.white, for: .normal)
            btn_txtDeductableYes.setTitleColor(UIColor.darkGray, for: .normal)
            
        }
    }
    
    @IBAction func btn_Register(sender:UIButton){
        
        self.registerApi()
        
        
    }
    
    
    
    
    @objc func showCountryPickerView(_ sender: UIButton) {
        let countryView = CountrySelectView.shared
        countryView.barTintColor = .red
        countryView.displayLanguage = .english
        countryView.show()
        countryView.selectedCountryCallBack = { (countryDic) -> Void in
            self.countrycode = "+\(countryDic["code"] as! NSNumber)"
            
            let index = IndexPath(row: sender.tag, section: 0)
            let cell: SuperAdminRegisterTableViewCell = self.tableview_RegisterOrg.cellForRow(at: index) as! SuperAdminRegisterTableViewCell
            cell.lbl_countryCodeLabel.text = self.countrycode
            
            // cell.img_CountryCode.image = countryDic["countryImage"] as? UIImage
            self.tableview_RegisterOrg.reloadData()
        }
    }
    
    func addservices(service: [Int]) {
        
        selectedServices = service
    }
    
    func cornerRadiusShadow(){
        
        btn_Yes.clipsToBounds = true
        btn_Yes.layer.cornerRadius = 8
        btn_Yes.layer.borderColor = UIColor.init(hex: 0x0DC9E4).cgColor
        btn_Yes.layer.borderWidth = 1.0
        btn_Yes.layer.maskedCorners = [.layerMinXMaxYCorner, .layerMinXMinYCorner]
        
        btn_No.clipsToBounds = true
        btn_No.layer.cornerRadius = 8
        btn_No.layer.maskedCorners = [.layerMaxXMaxYCorner, .layerMaxXMinYCorner]
        btn_No.layer.borderColor = UIColor.init(hex: 0x0DC9E4).cgColor
        btn_No.layer.borderWidth = 1.0
        
        // corner radius
        btn_Register.layer.cornerRadius = 5
        // shadow
        btn_Register.shadowBlackToHeader()
        
        
        // shadow
        view_Header.shadowBlackToHeader()
        
        
        
        btn_txtDeductableYes.clipsToBounds = true
        btn_txtDeductableYes.layer.cornerRadius = 8
        btn_txtDeductableYes.layer.borderColor = UIColor.init(hex: 0x0DC9E4).cgColor
        btn_txtDeductableYes.layer.borderWidth = 1.0
        btn_txtDeductableYes.layer.maskedCorners = [.layerMinXMaxYCorner, .layerMinXMinYCorner]
        
        btn_txtDeductableNo.clipsToBounds = true
        btn_txtDeductableNo.layer.cornerRadius = 8
        btn_txtDeductableNo.layer.maskedCorners = [.layerMaxXMaxYCorner, .layerMaxXMinYCorner]
        btn_txtDeductableNo.layer.borderColor = UIColor.init(hex: 0x0DC9E4).cgColor
        btn_txtDeductableNo.layer.borderWidth = 1.0
        
        view_UpperTableView.clipsToBounds = true
        view_UpperTableView.layer.cornerRadius = 5
        view_UpperTableView.layer.maskedCorners =
            [.layerMaxXMinYCorner, .layerMinXMinYCorner]
        
        
        
        view_TableViewLastCell.layer.cornerRadius = 8
        view_TableViewLastCell.layer.maskedCorners =  [.layerMaxXMaxYCorner, .layerMinXMaxYCorner]
        
        view_TableViewLastCell.layer.shadowColor = UIColor.black.cgColor
        view_TableViewLastCell.layer.shadowOffset = CGSize(width: 0, height: 3)
        view_TableViewLastCell.layer.shadowOpacity = 0.4
        view_TableViewLastCell.layer.shadowRadius = 2.0
        
        
    }
    
}

//MARK:TablView Delegate and DataSources----------------------------------
extension SuperAdminRegisterViewController: UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return array_Org.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "SuperAdminRegisterTableViewCell", for: indexPath) as! SuperAdminRegisterTableViewCell
        
        if dic_RegisterValue[array_Org[indexPath.row]] == nil{
            
            self.dic_RegisterValue.updateValue("", forKey:array_Org[indexPath.row])
        }
        
        
        if indexPath.row == 8{
            cell.txtView_Description.isHidden = false
            cell.txt_FillForm.isHidden = true
        }else{
            cell.txtView_Description.isHidden = true
            cell.txt_FillForm.isHidden = false
        }
        
        cell.txt_FillForm.placeholder = array_Org[indexPath.row].localized
        cell.txt_FillForm.delegate = self
        cell.txt_FillForm.tag = indexPath.row
        cell.btn_countryCodeLabel.tag = indexPath.row
        cell.btn_DropDown.isHidden = true
        cell.txt_FillForm.isSecureTextEntry = false
        if indexPath.row == 3 || indexPath.row == 11{
            cell.view_Contry.isHidden = false
            
        }else if indexPath.row == 6{
            cell.btn_DropDown.isHidden = false
            cell.view_Contry.isHidden = true
        }else{
            cell.view_Contry.isHidden = true
        }
        
        
        
        if indexPath.row == 0{
            
            cell.txt_FillForm.text = self.dic_RegisterValue["Organization Name"] as? String
        }else if indexPath.row == 1{
            cell.txt_FillForm.text = self.dic_RegisterValue["Organization abbreviation"] as? String
        }
        else if indexPath.row == 2{
            cell.txt_FillForm.text = self.dic_RegisterValue["Address"] as? String
        }
            
        else if indexPath.row == 3{
            let phonenumber = self.dic_RegisterValue["Org Contact"] as? String
            if phonenumber == ""{
                cell.txt_FillForm.text = ""
            }else{
                let trimPhoneNumber =  phonenumber?.components(separatedBy: "-")
                
                cell.txt_FillForm.text = trimPhoneNumber?[1]
            }
        }
        else if indexPath.row == 4{
            
            cell.txt_FillForm.text = self.dic_RegisterValue["Org Email"] as? String
        }else if indexPath.row == 5{
            cell.txt_FillForm.text = self.dic_RegisterValue["Org URL"] as? String
        }else if indexPath.row == 6{
            
            cell.txt_FillForm.text = self.dic_RegisterValue["Org Type"] as? String
        }
        else if indexPath.row == 7{
            cell.txt_FillForm.text = self.dic_RegisterValue["Registration No"] as? String
        }else if indexPath.row == 8{
            cell.txtView_Description.text = self.dic_RegisterValue["Description"] as? String
            
            if cell.txtView_Description.text == "Description".localized{
                cell.txtView_Description.textColor = UIColor.lightGray
                
            }else{
                cell.txtView_Description.textColor = UIColor.black
                
            }
            
        }else if indexPath.row == 9{
            cell.txt_FillForm.text = self.dic_RegisterValue["Admin First Name"] as? String
        }else if indexPath.row == 10{
            cell.txt_FillForm.text = self.dic_RegisterValue["Admin Last Name"] as? String
        }else if indexPath.row == 11{
            let phonenumber = self.dic_RegisterValue["Admin Mobile Number"] as? String
            
            if phonenumber == ""{
                cell.txt_FillForm.text = ""
            }else{
                
                let trimPhoneNumber =  phonenumber?.components(separatedBy: "-")
                cell.txt_FillForm.text = trimPhoneNumber?[1]
            }
            
            
            
        }else if indexPath.row == 12{
            cell.txt_FillForm.text = self.dic_RegisterValue["Admin Email"] as? String
        }else if indexPath.row == 13{
            cell.txt_FillForm.isSecureTextEntry = true
            cell.txt_FillForm.text = self.dic_RegisterValue["Password"] as? String
        }else if indexPath.row == 14{
            cell.txt_FillForm.isSecureTextEntry = true
            cell.txt_FillForm.text = self.dic_RegisterValue["Confirm Password"] as? String
        }
        
        print(dic_RegisterValue)
        cell.btn_countryCodeLabel.addTarget(self, action: #selector(showCountryPickerView(_:)), for: .touchUpInside)
        
        
        
        cell.layoutIfNeeded()
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    
    func registerApi(){
        
        if dic_RegisterValue.isEmpty == true{
            
            AKAlertController.alert("Please Fill Form".localized)
            return
        }
        if dic_RegisterValue["Org Type"] as! String == ""{
            AKAlertController.alert("Please Enter org Type".localized)
            return
        }else if dic_RegisterValue["Organization Name"] as! String == ""{
            
            AKAlertController.alert("Please Enter Organization Name".localized)
            return
        }else if dic_RegisterValue["Organization abbreviation"] as! String == ""  {
            AKAlertController.alert("Please Enter Organization abbreviation".localized)
            return
        }
        else if dic_RegisterValue["Address"] as! String == ""  {
            AKAlertController.alert("Please Enter Address".localized)
            return
        }else if dic_RegisterValue["Org Contact"] as! String == ""  {
            AKAlertController.alert("Please Enter Organization Contact".localized)
            return
        }
            //        else if dic_RegisterValue["Org Email"] as! String == ""  {
            //            AKAlertController.alert("Please Enter Organization Email")
            //            return
            //        }
            //        else if dic_RegisterValue["Org URL"] as! String == ""  {
            //            AKAlertController.alert("Please Enter Organization URL")
            //            return
            //        }
        else if dic_RegisterValue["Org Type"] as! String == ""  {
            AKAlertController.alert("Please Enter Organization Type".localized)
            return
        }
            //        else if dic_RegisterValue["Registration No"] as! String == ""  {
            //            AKAlertController.alert("Please Enter Registration No")
            //            return
            //        }
        else if dic_RegisterValue["Description"] as! String == ""  {
            AKAlertController.alert("Please Enter Description".localized)
            return
        }else if dic_RegisterValue["Admin First Name"] as! String == ""  {
            AKAlertController.alert("Please Enter Admin First Name".localized)
            return
        }else if dic_RegisterValue["Admin Last Name"] as! String == ""  {
            AKAlertController.alert("Please Enter Admin Last Name".localized)
            return
        }else if dic_RegisterValue["Admin Mobile Number"] as! String == ""  {
            AKAlertController.alert("Please Enter Admin Mobile Number".localized)
            return
        }else if dic_RegisterValue["Admin Email"] as! String == ""  {
            AKAlertController.alert("Please Enter Admin Email".localized)
            return
        }else if self.donationOrg == true {
            
            if self.txt_MerchantCode.text == ""{
                AKAlertController.alert("Please Enter Merchant Code".localized)
                return
            }else if self.txt_AuthenticationCode.text == ""{
                AKAlertController.alert("Please Enter Authentication Code".localized)
                return
            }
            
        }
        if dic_RegisterValue["Password"] as! String != dic_RegisterValue["Confirm Password"] as! String{
            
            AKAlertController.alert("Please Enter Same Passowrd".localized)
            return
        }
        if self.selectedServices.isEmpty == true{
            AKAlertController.alert("Please add services".localized)
            return
        }
        
        print(self.selectedServices,"jflkjklfjklgjklfjglkjfklgj")
        let org_Conatct = dic_RegisterValue["Org Contact"] as? String
        
        var phoneCode: String = ""
        var phoneNumber: String = ""
        let phone =  org_Conatct?.components(separatedBy: "-")
        phoneCode = phone?[0] ?? "+61"
        phoneNumber = phone?[1] ?? ""
        while (phoneNumber.hasPrefix("0")) {
            phoneNumber.remove(at: (phoneNumber.startIndex))
            
            self.dic_RegisterValue.updateValue(phoneCode + "-" + phoneNumber, forKey: "Org Contact")
        }
        
        
        let admin_Contact = dic_RegisterValue["Admin Mobile Number"] as? String
        var admin_phoneCode: String = ""
        var admin_phoneNumber: String = ""
        let admin_phone = admin_Contact?.components(separatedBy: "-")
        admin_phoneCode = admin_phone?[0] ?? "+61"
        admin_phoneNumber = admin_phone?[1] ?? ""
        while (admin_phoneNumber.hasPrefix("0")) {
            admin_phoneNumber.remove(at: (admin_phoneNumber.startIndex))
            
            self.dic_RegisterValue.updateValue(admin_phoneCode + "-" + admin_phoneNumber, forKey: "Admin Mobile Number")
        }
        
        let services = self.selectedServices
        
        KVNProgress.show()
        Network.shared.registerOrg(user_email: dic_RegisterValue["Admin Email"] as? String ?? "", user_mobile_number:  dic_RegisterValue["Admin Mobile Number"] as? String ?? "", user_first_name: dic_RegisterValue["Admin First Name"] as? String ?? "", user_last_name: dic_RegisterValue["Admin Last Name"] as? String ?? "", password1: dic_RegisterValue["Password"] as? String ?? "", password2: dic_RegisterValue["Confirm Password"] as? String ?? "", name: dic_RegisterValue["Organization Name"] as? String ?? "", abbreviation: dic_RegisterValue["Organization abbreviation"] as? String ?? "", org_type: dic_RegisterValue["Org Type"] as? String ?? "", description: dic_RegisterValue["Description"] as? String ?? "", address: dic_RegisterValue["Address"] as? String ?? "", latitude: Double(latitude_str) ?? 123.123, longitude: Double(longituted_str) ?? 123.123, reg_number: dic_RegisterValue["Registration No"] as? String ?? "", org_contact_name: "", org_contact: dic_RegisterValue["Org Contact"] as? String ?? "" , org_email: dic_RegisterValue["Org Email"] as? String ?? "", org_url: dic_RegisterValue["Org URL"] as? String ?? "", comments: "", donation_allowed: self.donationOrg, poli_pay_id: "", is_private: self.isPrivate ?? false, services: services, org_approved: "", tax_deductable: self.txtDeductable ?? true,merchant_code: txt_MerchantCode.text ?? "",auth_code:txt_AuthenticationCode.text ?? "") { (result) in
            
            if KVNProgress.isVisible(){
                KVNProgress.dismiss()
            }
            
            guard let user = result else {
                
                return
            }
            print(user)
            
            if user.success == true{
                
                self.showSingleAlertMessage(message: "", subMsg: "Organization has been on-boarded".localized, sender: self, completion:
                    { (success) -> Void in
                        if success == true{
                            let vc = self.storyboard?.instantiateViewController(withIdentifier: "TabVC") as! TabVC
                            vc.tabRegister = "SuperAdminRegister"
                            self.tabBarController?.tabBar.isHidden = false
                            self.slideMenuController()?.changeMainViewController(vc, close: true)
                            vc.tabBarController?.selectedIndex = 1
                            
                        }
                })
                
            }else{
                self.showSingleAlertMessage(message: "", subMsg:  user.message ?? "Something Went Wrong", sender: self, completion:
                    { (success) -> Void in
                        
                })
            }
        }
    }
}
extension SuperAdminRegisterViewController: MHDescDelegate{
    func getDescription(desc: String) {
        self.dic_RegisterValue["Description"] = desc
        
        
        self.tableview_RegisterOrg.reloadRows(at: [IndexPath(row: 8, section: 0)], with: UITableView.RowAnimation.none)
        
    }
}

extension SuperAdminRegisterViewController: GMSAutocompleteViewControllerDelegate {
    
    // Handle the user's selection.
    func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {
        
        print("Place name: \(String(describing: place.name))")
        let index = IndexPath(row: self.tag_forAddress, section: 0)
        let cell: SuperAdminRegisterTableViewCell = self.tableview_RegisterOrg.cellForRow(at: index) as! SuperAdminRegisterTableViewCell
        let location:CLLocationCoordinate2D = place.coordinate
        self.latitude_str = String(format: "%.6f", location.latitude)
        self.longituted_str = String(format: "%.6f", location.longitude)
        //        self.lat = Double(latitude_str) ?? 123.123
        //        self.long = Double(longituted_str) ?? 123.123
        print("Place address \(String(describing: place.formattedAddress))")
        
        dic_RegisterValue.updateValue("\(String(describing: place.formattedAddress ?? ""))", forKey: "Address")
        
        cell.txt_FillForm.text = "\(String(describing: place.formattedAddress ?? ""))"
        
        
        print("Place address:",place.formattedAddress!)
        
        
        dismiss(animated: true, completion: nil)
    }
    
    func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
        
        // TODO: handle the error.
        print("Error: ", error.localizedDescription)
    }
    
    // User canceled the operation.
    func wasCancelled(_ viewController: GMSAutocompleteViewController) {
        //  addressTextView.resignFirstResponder()
        dismiss(animated: true, completion: nil)
    }
    
    // Turn the network activity indicator on and off again.
    func didRequestAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
    }
    
    func didUpdateAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
    }
    
}
