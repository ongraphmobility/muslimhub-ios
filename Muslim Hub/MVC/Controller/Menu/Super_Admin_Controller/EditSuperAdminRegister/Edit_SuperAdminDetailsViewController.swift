//
//  Edit_SuperAdminDetailsViewController.swift
//  Muslim Hub
//
//  Created by Sakshi Singh on 10/05/19.
//  Copyright © 2019 OnGraph. All rights reserved.
//

import UIKit
import CountryPickerViewSwift
import GooglePlaces
import GooglePlacePicker
import KVNProgress
import IQKeyboardManagerSwift

class Edit_SuperAdminDetailsViewController: UIViewController,UITextFieldDelegate,addServiceDelegate,GMSPlacePickerViewControllerDelegate,UITextViewDelegate {
    
    var orgDetailsEdit:OrgDetailsData!
    
    @IBOutlet weak var tableview_RegisterOrg:UITableView!
    @IBOutlet weak var view_TableViewLastCell:UIView!
    @IBOutlet weak var btn_Yes:UIButton!
    @IBOutlet weak var btn_No:UIButton!
    @IBOutlet weak var btn_txtDeductableYes:UIButton!
    @IBOutlet weak var btn_txtDeductableNo:UIButton!
    @IBOutlet weak var btn_Privateorg:UIButton!
    @IBOutlet weak var btn_Register:UIButton!
    @IBOutlet weak var view_Header:UIView!
    @IBOutlet weak var lbl_HeaderTitle:UILabel!
    
    @IBOutlet weak var view_tableViewUpperRadius:UIView!
    @IBOutlet weak var view_tableViewLowerRadius:UIView!
    @IBOutlet weak var btn_Save:UIButton!
    @IBOutlet weak var btn_Cancel:UIButton!
    
    @IBOutlet weak var txt_MerchantCode:UITextField!
    @IBOutlet weak var txt_AuthenticationCode:UITextField!
    @IBOutlet weak var lbl_EnableDonationTitle:UILabel!
      @IBOutlet weak var lbl_PrivateOrgTitle:UILabel!
      @IBOutlet weak var lbl_TaxDeductableTitle:UILabel!
      @IBOutlet weak var lbl_ServiceProvidedTitle:UILabel!
     @IBOutlet weak var btn_AddServices:UIButton!
    var phoneCode: String = ""
    var phoneNumber: String = ""
    var isFirstTym = Bool()
    
    
    var countrycode:String!
    var selectedServices = [Int]()
    var dic_RegisterValue = [String:Any]()
    var donationOrg:Bool!
    var isPrivate:Bool!
    var txtDeductable:Bool!
    var tag_forAddress:Int!
    var lat = Double()
    var long = Double()
    //mark:Location variables-----------
    var placesClient: GMSPlacesClient!
    
    
    
    let array_Org = ["Organization Name","Organization abbreviation","Address","Org Contact","Org Email","Org URL","Org Type","Registration No","Description"]
    var array_OrgText = [String]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
       
        let org_name = orgDetailsEdit.name ?? ""
        let org_abb = orgDetailsEdit.abbreviation ?? ""
        let org_address = orgDetailsEdit.address ?? "No Address"
        
        if orgDetailsEdit.latitude != nil{
            
            self.lat = orgDetailsEdit.latitude!
        }
        if orgDetailsEdit.longitude != nil{
                   
            self.long = orgDetailsEdit.longitude!
               }
        
        if orgDetailsEdit.donationAllowed != false{
            
            self.txt_MerchantCode.isHidden = false
            self.txt_AuthenticationCode.isHidden = false
            self.txt_AuthenticationCode.text = orgDetailsEdit.auth_code
            self.txt_MerchantCode.text = orgDetailsEdit.merchant_code

        }else{
            
           self.txt_MerchantCode.isHidden = true
            self.txt_AuthenticationCode.isHidden = true
        }
        
        if orgDetailsEdit.orgContact == "" || orgDetailsEdit.orgContact == nil{
            self.phoneCode = "+61"
            self.phoneNumber = ""
        }else{
            let phone =  orgDetailsEdit.orgContact?.components(separatedBy: "-")
            self.phoneCode = phone?[0] ?? "+61"
            self.phoneNumber = phone?[1] ?? ""
            
        }
        
         dic_RegisterValue.updateValue(self.phoneCode, forKey:"Code")
        let org_Contact = self.phoneNumber
        let org_Email = orgDetailsEdit.orgEmail ?? "No Email"
        let org_url = orgDetailsEdit.orgURL ?? "No url"
        let org_reg = orgDetailsEdit.regNumber ?? "No regestration no"
        let org_Type = orgDetailsEdit.orgType ?? "No orgType"
        let org_details = orgDetailsEdit.description ?? "No description"
        
        if orgDetailsEdit.isPrivate == true{
            btn_Privateorg.isSelected = true
            self.isPrivate = true
            btn_Privateorg.setBackgroundImage(#imageLiteral(resourceName: "checkbox_iPhone"), for: .normal)
        }else{
            btn_Privateorg.isSelected = false
            self.isPrivate = false
            btn_Privateorg.setBackgroundImage(#imageLiteral(resourceName: "uncheckbox_iPhone"), for: .normal)
        }
        
        if orgDetailsEdit.donationAllowed == true{
            btn_Yes.backgroundColor = UIColor.init(hex: 0x0BC9E4)
            self.donationOrg = true
            btn_No.backgroundColor = UIColor.white
        }else{
            btn_No.backgroundColor = UIColor.init(hex: 0x0BC9E4)
            self.donationOrg = false
            btn_Yes.backgroundColor = UIColor.white
        }
        
        if orgDetailsEdit.taxDeductable == true{
            
            btn_txtDeductableYes.backgroundColor = UIColor.init(hex: 0x0BC9E4)
            btn_txtDeductableYes.setTitleColor(UIColor.white, for: .normal)
            self.txtDeductable = true
            btn_txtDeductableNo.backgroundColor = UIColor.white
            btn_txtDeductableNo.setTitleColor(UIColor.darkGray, for: .normal)
        }else{
            btn_txtDeductableNo.backgroundColor = UIColor.init(hex: 0x0BC9E4)
            self.txtDeductable = false
            btn_txtDeductableYes.backgroundColor = UIColor.white
            btn_txtDeductableNo.setTitleColor(UIColor.white, for: .normal)
            btn_txtDeductableYes.setTitleColor(UIColor.darkGray, for: .normal)
        }
        if orgDetailsEdit != nil{
            self.selectedServices = orgDetailsEdit.services!
        }
        self.array_OrgText = [org_name,org_abb,org_address,org_Contact,org_Email,org_url,org_Type,org_reg,org_details]
        
        for i in 0...8{
            dic_RegisterValue.updateValue(array_OrgText[i], forKey: array_Org[i])
        }
        if #available(iOS 13.0, *) {
            statusBarColorChange(color:UIColor.init(hex: 0x0DC9E4))
        } else {
            UIApplication.statusBarBackgroundColor = UIColor.init(hex: 0x0DC9E4)
        }
       
        lbl_HeaderTitle.text = "Onboard Registeration - MH".localized
        lbl_EnableDonationTitle.text = "Enable donation for ORG?".localized
        lbl_PrivateOrgTitle.text = "Private Org".localized
        lbl_TaxDeductableTitle.text = "Tax Deductable".localized
        lbl_ServiceProvidedTitle.text = "Services Provided".localized
        btn_AddServices.setTitle("Add Services".localized, for: .normal)
        self.txt_MerchantCode.placeholder = "Merchant Code".localized
        self.txt_AuthenticationCode.placeholder = "Authentication Code".localized
        
        self.btn_Save.setTitle("Save".localized, for: .normal)
         self.btn_Cancel.setTitle("Cancel".localized, for: .normal)
        self.btn_Yes.setTitle("Yes".localized, for: .normal)
        self.btn_No.setTitle("No".localized, for: .normal)
        
        self.btn_txtDeductableYes.setTitle("Yes".localized, for: .normal)
           self.btn_txtDeductableNo.setTitle("No".localized, for: .normal)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.cornerRadiusShadow()
        
        
    }
   
    func textViewDidBeginEditing(_ textView: UITextView) {
        guard let indexPath = textView.tableViewIndexPath(self.tableview_RegisterOrg) else { return }
        
        switch (indexPath.section, 8) {
            
        case (0,8):
            
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "MHDescViewController") as! MHDescViewController
            vc.delegate = self
                 vc.modalPresentationStyle = .fullScreen
            vc.textDesc = textView.text!
            textView.keyboardType = .default
            present(vc, animated: false, completion: nil)
        default:
            break
            
        }
        
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
           
           textView.textColor = UIColor.black
         
       }
       
    
    //MARK:TextField Delegate and DataSource Method--------------------
    func textFieldDidBeginEditing(_ textField: UITextField) {
        guard let indexPath = textField.tableViewIndexPath(self.tableview_RegisterOrg) else { return }
        
        switch (indexPath.section, indexPath.row) {
            
        case (0,2):
            
            let autocompleteController = GMSAutocompleteViewController()
            autocompleteController.delegate = self
            let filter = GMSAutocompleteFilter()
            filter.type = .noFilter  //suitable filter type
           // filter.country = "AUS"  //appropriate country code
            autocompleteController.autocompleteFilter = filter
            self.tag_forAddress = 2
            present(autocompleteController, animated: true, completion: nil)
            
            
        case (0,3):
            
            textField.keyboardType = .numberPad
            
        case (0, 6):

            let arr_OrgType  = ["mosque", "charity", "community","educational"]
//            AKMultiPicker().openPickerIn(textField, firstComponentArray: arr_OrgType) { (value1, _, index1, _) in
//                textField.text = value1
//              
//                self.dic_RegisterValue.updateValue(value1 as AnyObject, forKey: self.array_Org[6])
//
//            }
            
                        PickerViewController.openPickerView(viewC: self, dataArray: arr_OrgType) { (value, index) in
                            print(value)
            
                            textField.text = value
                           
                           self.dic_RegisterValue.updateValue(value as AnyObject, forKey: self.array_Org[6])
                    }
        case (0, 7):
            textField.keyboardType = .numberPad
        case (0,8):
            
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "MHDescViewController") as! MHDescViewController
            vc.delegate = self
                 vc.modalPresentationStyle = .fullScreen
            vc.textDesc = textField.text!
            present(vc, animated: false, completion: nil)
        case (0,11):
            
            textField.keyboardType = .numberPad
            
       default:
            break
        }
        
       
        
    }
    
    
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        
        guard let indexPath = textField.tableViewIndexPath(self.tableview_RegisterOrg) else { return }
        
        switch (indexPath.section, indexPath.row) {
            
        case (0,2):
            print(textField.text as Any)
        case (0,3):
            var Phnumber = String()
            Phnumber = textField.text ?? ""
            while (Phnumber.hasPrefix("0")) {
               Phnumber.remove(at: (Phnumber.startIndex))
                
            }
            
            if Phnumber.count > 10{
                AKAlertController.alert("Please Enter Valid Organization Contact")
            }
            print(textField.text as Any)
        case (0, 6):
            print("select Org Type")
            
            if let value = dic_RegisterValue["Org Type"] as? String  {
              
                textField.text = value
                
            }

        default:
            break
        }
        
        
        
        let index = IndexPath(row: textField.tag, section: 0)
        let cell: SuperAdminRegisterTableViewCell = self.tableview_RegisterOrg.cellForRow(at: index) as! SuperAdminRegisterTableViewCell
        
        let key = array_Org[textField.tag]
        
        dic_RegisterValue.updateValue(cell.txt_FillForm.text as AnyObject, forKey: key)
        
        //mark:Condition for appending mobile Number--------------
     
        if key == "Org Contact"{
            
           self.phoneCode = cell.lbl_countryCodeLabel.text ?? "+61"
            
            let mobileNumber = dic_RegisterValue[key] as? String
            let mobileNumberWithCode = mobileNumber!
            
            dic_RegisterValue.updateValue(mobileNumberWithCode, forKey: array_Org[textField.tag])
             self.array_OrgText[textField.tag] =  cell.txt_FillForm.text!
        }else{
            self.array_OrgText[textField.tag] = cell.txt_FillForm.text!
            
        }
       tableview_RegisterOrg.reloadData()
        
    }
    
    func placePicker(_ viewController: GMSPlacePickerViewController, didPick place: GMSPlace) {
        // Dismiss the place picker, as it cannot dismiss itself.
        let index = IndexPath(row: self.tag_forAddress, section: 0)
        let cell: SuperAdminRegisterTableViewCell = self.tableview_RegisterOrg.cellForRow(at: index) as! SuperAdminRegisterTableViewCell
        
        cell.txt_FillForm.text = "\(String(describing: place.formattedAddress ?? ""))"
        print("Place name \(String(describing: place.coordinate))")
        let location:CLLocationCoordinate2D = place.coordinate
        let latitude_str = String(format: "%.6f", location.latitude)
        let longituted_str = String(format: "%.6f", location.longitude)
        self.lat = Double(latitude_str) ?? 123.123
        self.long = Double(longituted_str) ?? 123.123
        print("Place address \(String(describing: place.formattedAddress))")
        viewController.dismiss(animated: true, completion: nil)
    }
    
    func placePickerDidCancel(_ viewController: GMSPlacePickerViewController) {
        // Dismiss the place picker, as it cannot dismiss itself.
        viewController.dismiss(animated: true, completion: nil)
        
        print("No place selected")
    }
    //MARK:IBACtion----------------------------------------
    @IBAction func btn_Back(sendr:UIButton){
        
        self.dismiss(animated:true, completion: nil)
    }
    @IBAction func btn_PrivateOrg(sender:UIButton){
        
        if btn_Privateorg.isSelected{
            btn_Privateorg.isSelected = false
            //  btn_Privateorg.backgroundColor = UIColor.white
            self.isPrivate = false
            btn_Privateorg.setBackgroundImage(#imageLiteral(resourceName: "uncheckbox_iPhone"), for: .normal)
        }else{
            btn_Privateorg.isSelected = true
            self.isPrivate = true
            // btn_Privateorg.backgroundColor = UIColor.init(hex: 0x0BC9E4)
            btn_Privateorg.setBackgroundImage(#imageLiteral(resourceName: "checkbox_iPhone"), for: .normal)
        }
    }
    
    @IBAction func btn_AddServices(sender:UIButton){
        
        if let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "AlertAddServicesViewController") as? AlertAddServicesViewController
        {
            vc.servicesDelegate = self
            vc.modalPresentationStyle = .overCurrentContext
            vc.addServiceTicked = self.selectedServices
            
            present(vc, animated: true, completion: nil)
        }
    }
    @IBAction func btn_EnableDonationOrg(sender:UIButton){
        
        if sender.tag == 0{
            
            btn_Yes.backgroundColor = UIColor.init(hex: 0x0BC9E4)
            self.donationOrg = true
            btn_No.backgroundColor = UIColor.white
            btn_Yes.setTitleColor(UIColor.white, for: .normal)
            btn_No.setTitleColor(UIColor.darkGray, for: .normal)
            self.txt_MerchantCode.isHidden = false
                       self.txt_AuthenticationCode.isHidden = false
        }else if sender.tag == 1{
            btn_No.backgroundColor = UIColor.init(hex: 0x0BC9E4)
            self.donationOrg = false
            btn_Yes.backgroundColor = UIColor.white
            btn_No.setTitleColor(UIColor.white, for: .normal)
            btn_Yes.setTitleColor(UIColor.darkGray, for: .normal)
            self.txt_MerchantCode.isHidden = true
                       self.txt_AuthenticationCode.isHidden = true
        }
    }
    @IBAction func btn_TextDeductale(sender:UIButton){
        
        if sender.tag == 2{
            
            btn_txtDeductableYes.backgroundColor = UIColor.init(hex: 0x0BC9E4)
            btn_txtDeductableYes.setTitleColor(UIColor.white, for: .normal)
            self.txtDeductable = true
            btn_txtDeductableNo.backgroundColor = UIColor.white
            btn_txtDeductableNo.setTitleColor(UIColor.darkGray, for: .normal)
        }else if sender.tag == 3{
            btn_txtDeductableNo.backgroundColor = UIColor.init(hex: 0x0BC9E4)
            self.txtDeductable = false
            btn_txtDeductableYes.backgroundColor = UIColor.white
            btn_txtDeductableNo.setTitleColor(UIColor.white, for: .normal)
            btn_txtDeductableYes.setTitleColor(UIColor.darkGray, for: .normal)
        }
    }
    
    @IBAction func btn_Save(sender:UIButton){
    self.orgEditApi()
    }
   
    @objc func showCountryPickerView(_ sender: UIButton) {
        
    
     
        let countryView = CountrySelectView.shared
        countryView.barTintColor = .red
        countryView.displayLanguage = .english
        countryView.show()
        countryView.selectedCountryCallBack = { (countryDic) -> Void in
            self.countrycode = "+\(countryDic["code"] as! NSNumber)"
            
            let index = IndexPath(row: sender.tag, section: 0)
            let cell: SuperAdminRegisterTableViewCell = self.tableview_RegisterOrg.cellForRow(at: index) as! SuperAdminRegisterTableViewCell
            cell.lbl_countryCodeLabel.text = self.countrycode
            self.phoneCode = self.countrycode
            // cell.img_CountryCode.image = countryDic["countryImage"] as? UIImage
            self.dic_RegisterValue.updateValue(self.phoneCode, forKey:"Code")
            self.tableview_RegisterOrg.reloadData()
        }
    }
    
    func addservices(service: [Int]) {
     
            self.selectedServices = service
        
       
    }
    
    func cornerRadiusShadow(){
        
        btn_Yes.clipsToBounds = true
        btn_Yes.layer.cornerRadius = 8
        btn_Yes.layer.borderColor = UIColor.init(hex: 0x0DC9E4).cgColor
        btn_Yes.layer.borderWidth = 1.0
        btn_Yes.layer.maskedCorners = [.layerMinXMaxYCorner, .layerMinXMinYCorner]
        
        btn_No.clipsToBounds = true
        btn_No.layer.cornerRadius = 8
        btn_No.layer.maskedCorners = [.layerMaxXMaxYCorner, .layerMaxXMinYCorner]
        btn_No.layer.borderColor = UIColor.init(hex: 0x0DC9E4).cgColor
        btn_No.layer.borderWidth = 1.0
        
        // corner radius
    
        
        
        // shadow
        view_Header.shadowBlackToHeader()
        
        
        btn_txtDeductableYes.clipsToBounds = true
        btn_txtDeductableYes.layer.cornerRadius = 8
        btn_txtDeductableYes.layer.borderColor = UIColor.init(hex: 0x0DC9E4).cgColor
        btn_txtDeductableYes.layer.borderWidth = 1.0
        btn_txtDeductableYes.layer.maskedCorners = [.layerMinXMaxYCorner, .layerMinXMinYCorner]
        
        btn_txtDeductableNo.clipsToBounds = true
        btn_txtDeductableNo.layer.cornerRadius = 8
        btn_txtDeductableNo.layer.maskedCorners = [.layerMaxXMaxYCorner, .layerMaxXMinYCorner]
        btn_txtDeductableNo.layer.borderColor = UIColor.init(hex: 0x0DC9E4).cgColor
        btn_txtDeductableNo.layer.borderWidth = 1.0
        
        
        view_tableViewUpperRadius.clipsToBounds = true
        view_tableViewUpperRadius.layer.cornerRadius = 8
        view_tableViewUpperRadius.layer.maskedCorners =
            [.layerMaxXMinYCorner, .layerMinXMinYCorner]
        
        
        
        view_tableViewLowerRadius.layer.cornerRadius = 8
        view_tableViewLowerRadius.layer.maskedCorners =  [.layerMaxXMaxYCorner, .layerMinXMaxYCorner]
     
        view_tableViewLowerRadius.layer.shadowColor = UIColor.black.cgColor
        view_tableViewLowerRadius.layer.shadowOffset = CGSize(width: 0, height: 3)
        view_tableViewLowerRadius.layer.shadowOpacity = 0.4
        view_tableViewLowerRadius.layer.shadowRadius = 2.0
    
        
        
        // corner radius
        btn_Save.layer.cornerRadius = 5
        // shadow
        btn_Save.shadowBlackToHeader()
        
        // corner radius
        btn_Cancel.layer.cornerRadius = 5
        // shadow
        btn_Cancel.shadowBlackToHeader()
        
        
    }
    
    
}
extension Edit_SuperAdminDetailsViewController: UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return array_OrgText.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "SuperAdminRegisterTableViewCell", for: indexPath) as! SuperAdminRegisterTableViewCell
        
        if indexPath.row == 8{
            cell.txtView_Description.isHidden = false
            cell.txt_FillForm.isHidden = true
            cell.txtView_Description.text = array_OrgText[8]
        }else{
            cell.txtView_Description.isHidden = true
            cell.txt_FillForm.isHidden = false
        }
        if indexPath.row == 8{
            cell.txtView_Description.text = self.dic_RegisterValue["Description"] as? String

            if cell.txtView_Description.text == "Description"{
                                           cell.txtView_Description.textColor = UIColor.lightGray
                                          
                                       }else{
                                           cell.txtView_Description.textColor = UIColor.black
                                         
                                                         
                                       }

        }
      
        if array_OrgText[indexPath.row] == ""{
             cell.txt_FillForm.placeholder = array_Org[indexPath.row]
        }else{
             cell.txt_FillForm.text = array_OrgText[indexPath.row]
        }
        
        cell.lbl_countryCodeLabel.text = self.phoneCode
      
     //   let array_Org = ["Organization Name","Organization abbreviation","Address","Org Contact","Org Email","Org URL","Org Type","Registration No","Description"]
        if indexPath.row == 6{
            cell.txt_FillForm.text = self.dic_RegisterValue["Org Type"] as? String
        }
        if indexPath.row == 7{
            cell.txt_FillForm.text = self.dic_RegisterValue["Registration No"] as? String
        }
        cell.txt_FillForm.delegate = self
        cell.txt_FillForm.tag = indexPath.row
        cell.btn_countryCodeLabel.tag = indexPath.row
        cell.btn_DropDown.isHidden = true
        if indexPath.row == 3 || indexPath.row == 11{
            cell.view_Contry.isHidden = false
            
        }else if indexPath.row == 6{
            cell.btn_DropDown.isHidden = false
            cell.view_Contry.isHidden = true
        }else{
            cell.view_Contry.isHidden = true
        }
        
        print(dic_RegisterValue)
        cell.btn_countryCodeLabel.addTarget(self, action: #selector(showCountryPickerView(_:)), for: .touchUpInside)
        
    
        cell.layoutIfNeeded()
        return cell
    }
    
    
    func orgEditApi(){
     
        var org_PhoneNumber:String = ""
        if dic_RegisterValue["Org Contact"] as! String == ""  {
            AKAlertController.alert("Please Enter Organization Contact")
           
        }else{
           
            let contact = dic_RegisterValue["Org Contact"] as! String
           
            org_PhoneNumber = ((self.phoneCode + "-" + contact))
            var phoneCode: String = ""
            var phoneNumber: String = ""
            let phone =  org_PhoneNumber.components(separatedBy: "-")
            phoneCode = phone[0]
            phoneNumber = phone[1]
            while (phoneNumber.hasPrefix("0")) {
                phoneNumber.remove(at: (phoneNumber.startIndex))
                
                // self.dic_RegisterValue.updateValue(phoneCode + "-" + phoneNumber, forKey: "Org Contact")
            }
             if self.donationOrg == true {
               
                if self.txt_MerchantCode.text == ""{
                    AKAlertController.alert("Please Enter Merchant Code")
                    return
                }else if self.txt_AuthenticationCode.text == ""{
                    AKAlertController.alert("Please Enter Authentication Code")
                    return
                }
                
            }
         let mobilePhone = (phoneCode + "-" + phoneNumber)
            
            KVNProgress.show()
            
            Network.shared.orgEdit(id: orgDetailsEdit.id ?? 0, image: "", name: dic_RegisterValue["Organization Name"] as! String, logo: "", abbreviation: dic_RegisterValue["Organization abbreviation"] as! String, org_type: dic_RegisterValue["Org Type"] as! String, description: dic_RegisterValue["Description"] as! String, address: dic_RegisterValue["Address"] as! String, latitude: self.lat, longitude: self.long, reg_number: dic_RegisterValue["Registration No"] as! String, admin: [], org_contact_name: "", org_contact: mobilePhone , org_email: dic_RegisterValue["Org Email"] as! String, org_url: dic_RegisterValue["Org URL"] as! String, comments: "", donation_allowed: self.donationOrg, tax_deductable: self.txtDeductable, poli_pay_id: "", is_private: self.isPrivate, services: selectedServices, auth_code: self.txt_AuthenticationCode.text ?? "", merchant_code: self.txt_MerchantCode.text ?? "" ) { (result) in
                if KVNProgress.isVisible(){
                    KVNProgress.dismiss()
                }
                
                
                guard let user = result else {
                    return
                }
                
                if user.success == true{
                    
                    self.showSingleAlertMessage(message: "", subMsg: "Organization updated successfully", sender: self, completion:
                        { (success) -> Void in
                            if success == true{
                                self.dismiss(animated: true, completion: nil)
                               self.isFirstTym = true
                            }
                    })
                    
                    
                }else{
                    
                    self.showSingleAlertMessage(message: "", subMsg: user.message ?? "", sender: self, completion:
                        { (success) -> Void in
                         self.isFirstTym = true
                    })
                }
                
            }
        }
        
     
        
      
    }
}
extension Edit_SuperAdminDetailsViewController: MHDescDelegate{
    func getDescription(desc: String) {
        self.dic_RegisterValue["Description"] = desc
        self.tableview_RegisterOrg.reloadRows(at: [IndexPath(row: 8, section: 0)], with: UITableView.RowAnimation.none)
    }
}
extension Edit_SuperAdminDetailsViewController: GMSAutocompleteViewControllerDelegate {
    
    // Handle the user's selection.
    func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {
        
        print("Place name: \(String(describing: place.name))")
        let index = IndexPath(row: self.tag_forAddress, section: 0)
        let cell: SuperAdminRegisterTableViewCell = self.tableview_RegisterOrg.cellForRow(at: index) as! SuperAdminRegisterTableViewCell
        let location:CLLocationCoordinate2D = place.coordinate
        let latitude_str = String(format: "%.6f", location.latitude)
        let longituted_str = String(format: "%.6f", location.longitude)
        self.lat = Double(latitude_str) ?? 123.123
        self.long = Double(longituted_str) ?? 123.123
        print("Place address \(String(describing: place.formattedAddress))")
        
        dic_RegisterValue.updateValue("\(String(describing: place.formattedAddress ?? ""))", forKey: "Address")
        
        cell.txt_FillForm.text = "\(String(describing:place.formattedAddress ?? ""))"
        
        
        print("Place address:",place.formattedAddress!)
        
        
        dismiss(animated: true, completion: nil)
    }
    
    func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
        
        // TODO: handle the error.
        print("Error: ", error.localizedDescription)
    }
    
    // User canceled the operation.
    func wasCancelled(_ viewController: GMSAutocompleteViewController) {
        //  addressTextView.resignFirstResponder()
        dismiss(animated: true, completion: nil)
    }
    
    // Turn the network activity indicator on and off again.
    func didRequestAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
    }
    
    func didUpdateAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
    }
    
}
