//
//  AlertAddServicesViewController.swift
//  Muslim Hub
//
//  Created by Sakshi Singh on 29/04/19.
//  Copyright © 2019 OnGraph. All rights reserved.
//

import UIKit
import  KVNProgress
import AlamofireImage

protocol addServiceDelegate {
    
    func addservices(service:[Int])
    
    
}

@available(iOS 11.0, *)
class AlertAddServicesViewController: UIViewController {
    
    @IBOutlet weak var btn_Save:UIButton!
    @IBOutlet weak var btn_Cancel:UIButton!
    @IBOutlet weak var lbl_Header:UILabel!
    @IBOutlet weak var collectionview_AlertAddServices:UICollectionView!
    
    let array_AddServices = ["Sisters prayer area","On-site Parking","Marriage Celebrant","accept Donations","Sporting Activities","Justice of Peace","New Muslim Programs"]
    let array_imgAddsrvices = [#imageLiteral(resourceName: "sisters_prayer_iPhone"),#imageLiteral(resourceName: "onsite_parking_iPhone"),#imageLiteral(resourceName: "marriage_celebrant_iPhone"),#imageLiteral(resourceName: "accept_donations_iPhone"),#imageLiteral(resourceName: "sporting_activities_iPhone"),#imageLiteral(resourceName: "justice_peace_iPhone"),#imageLiteral(resourceName: "sisters_prayer_iPhone")]
    var array_serviceSelected = [Int]()
    var servicesDelegate:addServiceDelegate!
    
    var arra_AddServices = [AddServiceData]()
    var FromOrgDetails:String!
    var addServiceTicked = [Int]()
    var orgId = Int()
    var isSuperAdmin = Bool()
    weak var weakSelf : ActivitiesViewController?
    var isorgAll:String?
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
        //Upper side cornerRadius---------------------------
        lbl_Header.clipsToBounds = true
        lbl_Header.layer.cornerRadius = 10
        lbl_Header.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner]
        self.cornerRadiusAndShadow()
        //mark:check for save button--------------------------------
        
        if (UserDefaults.standard.value(forKey: "Login_Token") as? String) != nil{
            
            if let superAdmin = UserDefaults.standard.value(forKey: "isSuperAdmin") as? Bool{
                self.isSuperAdmin = superAdmin
                if superAdmin == true{
                    btn_Save.isHidden = false
                }else{
                    if let orgOwn_Array = UserDefaults.standard.value(forKey: "OwnOrg_Array") as? [Int]{
                        
                        
                        if orgOwn_Array.contains(orgId){
                            if self.isorgAll == "All"{
                                btn_Save.isHidden = false
                            }else{
                                btn_Save.isHidden = true
                            }
                            
                        }else{
                            btn_Save.isHidden = true
                            
                        }
                    }
                }
            }
        }else{
            
            btn_Save.isHidden = true
        }
        
        self.lbl_Header.text = "Services".localized
        self.btn_Cancel.setTitle("Cancel".localized, for: .normal)
        self.btn_Save.setTitle("Save".localized, for: .normal)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.addServicesApi()
        
    }
    
    func cornerRadiusAndShadow(){
        
        // corner radius
        btn_Save.layer.cornerRadius = 5
        // shadow
        btn_Save.shadowBlackToHeader()
        
        // corner radius
        btn_Cancel.layer.cornerRadius = 5
        // shadow
        btn_Cancel.shadowBlackToHeader()
        
    }
    func AdminEditOnlyServicesApi(){
        
        KVNProgress.show()
        
        Network.shared.AdminEditOnlyServices(id: orgId ,services:array_serviceSelected) { (result) in
            if KVNProgress.isVisible(){
                KVNProgress.dismiss()
            }
            
            
            guard let user = result else {
                return
            }
            
            if user.success == true{
                self.dismiss(animated: true, completion: {
                    self.weakSelf?.viewWillAppear(true)
                })
            }
            
        }
        
        
    }
    
    func addServicesApi(){
        
        // KVNProgress.show()
        
        Network.shared.addServices{ (result) in
            // KVNProgress.dismiss()
            guard let user = result else {
                
                return
            }
            self.arra_AddServices = user
            
            self.collectionview_AlertAddServices.reloadData()
        }
        
        
    }
    
    @IBAction func btn_Back(sendr:UIButton){
        DispatchQueue.main.async {
            self.dismiss(animated: true, completion: {
                //  self.weakSelf?.viewWillAppear(true)
            })
            
        }
    }
    
    @objc func btn_tick(sender:UIButton){
        
        if sender.isSelected{
            sender.isSelected = false
            
            let unselectedService = arra_AddServices[sender.tag].id
            if array_serviceSelected.contains(unselectedService ?? 0){
                array_serviceSelected = array_serviceSelected.filter(){$0 != unselectedService}
            }
            
        }else{
            sender.isSelected = true
            
            array_serviceSelected.append(arra_AddServices[sender.tag].id ?? 0)
        }
    }
    
    
    @IBAction func btn_save(sender:UIButton){
        
        if FromOrgDetails == "FromOrgDetails"{
            self.AdminEditOnlyServicesApi()
            
        }else{
            servicesDelegate.addservices(service: array_serviceSelected)
            self.dismiss(animated: true, completion: nil)
        }
        
    }
    
}
extension AlertAddServicesViewController:UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arra_AddServices.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "AlertAddServicesCollectionViewCell", for: indexPath) as! AlertAddServicesCollectionViewCell
        cell.btn_Tick.tag = indexPath.row
        
        cell.btn_Tick.setBackgroundImage(#imageLiteral(resourceName: "checkbox_iPhone"), for: .selected)
        
        //MARK:case for btn_Tick tap-----------------------------------
        if  self.isSuperAdmin == false{
            if let orgOwn_Array = UserDefaults.standard.value(forKey: "OwnOrg_Array") as? [Int]{
                
                
                if orgOwn_Array.contains(orgId){
                    cell.btn_Tick.addTarget(self, action: #selector(btn_tick(sender:)), for: .touchUpInside)
                    
                    
                }else{
                    print("Button tick not clicked when coming from normal user or superAdmin")
                }}
        }else{
            cell.btn_Tick.addTarget(self, action: #selector(btn_tick(sender:)), for: .touchUpInside)
            
        }
        
        
        cell.lbl_AddServicesTitle.text = arra_AddServices[indexPath.row].serviceName
        if let photo = arra_AddServices[indexPath.item].icon, let url = URL(string: photo) {
            cell.img_AddServices.af_setImage(withURL: url)
        } else {
            cell.img_AddServices.image = UIImage(named: "name_icon_iPhone.png")
        }
        
        if addServiceTicked.contains(arra_AddServices[indexPath.row].id ?? 0){
            cell.btn_Tick.isSelected = true
            cell.btn_Tick.setBackgroundImage(#imageLiteral(resourceName: "checkbox_iPhone"), for: .selected)
            array_serviceSelected.append(arra_AddServices[indexPath.row].id ?? 0)
            
        }else{
            cell.btn_Tick.isSelected = false
            cell.btn_Tick.setBackgroundImage(#imageLiteral(resourceName: "uncheckbox_iPhone"), for: .normal)
        }
        
        
        return cell
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        return CGSize(width: collectionview_AlertAddServices.bounds.size.width/2 - 10 , height: 60)
        
        
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        insetForSectionAt section: Int) -> UIEdgeInsets{
        return  UIEdgeInsets( top: 5,  left: 10, bottom: 5,  right: 10)
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0.0
    }
    
    
    
}
