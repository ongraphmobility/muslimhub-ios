//
//  ProfileViewController.swift
//  Muslim Hub
//
//  Created by Sakshi Singh on 18/04/19.
//  Copyright © 2019 OnGraph. All rights reserved.
//

import UIKit
import KVNProgress
import  CountryPickerViewSwift
import DropDown
struct Constant {
    static let LANGUAGE  = "LANGUAGE"
}
enum App_FontSize:String {
    case Small
    case Medium
    case Large
    
}
class ProfileViewController: UIViewController,UITextFieldDelegate {
    
    
    @IBOutlet weak var txt_FullName:UITextField!
    @IBOutlet weak var txt_LastName:UITextField!
    @IBOutlet weak var txt_Email:UITextField!
    @IBOutlet weak var txt_Code:UITextField!
    @IBOutlet weak var txt_PhoneNumber:UITextField!
    @IBOutlet weak var txt_FontSize:UITextField!
    @IBOutlet weak var txt_Language:UITextField!
    @IBOutlet weak var btn_USerProfileEdit:UIButton!
    @IBOutlet weak var btn_chnagePassword:UIButton!
    @IBOutlet weak var btn_UserPreferences_Lang:UIButton!
    @IBOutlet weak var btn_UserPreferences_Font:UIButton!
    @IBOutlet weak var btn_UserPreferencesEdit:UIButton!
    @IBOutlet weak var btn_CreateUser:UIButton!
    @IBOutlet weak var lbl_UserPreferences_Title:UILabel!
    @IBOutlet weak var lbl_UserProfile_Title:UILabel!
    
    var fromHome:String!
    var isFontChanged = Bool()
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
        if #available(iOS 13.0, *) {
            statusBarColorChange(color:UIColor.init(hex:  0x05BDB9))
        } else {
            UIApplication.statusBarBackgroundColor = UIColor.init(hex: 0x05BDB9)
        }
        
        self.navigationController?.isNavigationBarHidden = true
        txt_FullName.isUserInteractionEnabled = false
        txt_LastName.isUserInteractionEnabled = false
        txt_Email.isUserInteractionEnabled = false
        txt_Code.isUserInteractionEnabled = false
        txt_PhoneNumber.isUserInteractionEnabled = false
        self.btn_chnagePassword.shadowWhiteToHeader()
        txt_FontSize.inputView = UIView()
        btn_UserPreferences_Lang.isUserInteractionEnabled = false
        btn_UserPreferences_Font.isUserInteractionEnabled = false
        
        //Langauage
        lbl_UserPreferences_Title.text = "User Preferences".localized
        lbl_UserProfile_Title.text = "User Profile".localized
        btn_chnagePassword.setTitle("Change Password?".localized, for: .normal)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        if let loginToken = UserDefaults.standard.value(forKey: "Login_Token") as? String{
            print(loginToken)
            self.btn_CreateUser.isHidden = true
            
            self.txt_FullName.text = UserDefaults.standard.value(forKey: "FirstName") as? String
            self.txt_LastName.text = UserDefaults.standard.value(forKey: "LastName") as? String
            self.txt_Email.text = UserDefaults.standard.value(forKey: "email") as? String
            let MobileNumber = UserDefaults.standard.value(forKey: "MobileNumber") as! String
            if MobileNumber == ""{
                self.txt_Code.text = "+61"
                self.txt_PhoneNumber.text = ""
            }else{
                let phone = MobileNumber.components(separatedBy: "-")
                self.txt_Code.text = phone[0]
                if phone[1] != ""{
                    self.txt_PhoneNumber.text = phone[1]
                }else{
                    self.txt_PhoneNumber.text = ""
                }
                
            }
        }else{
            self.btn_CreateUser.isHidden = false
            self.btn_chnagePassword.setTitle("Login", for: .normal)
        }
        if let fontSize = UserDefaults.standard.value(forKey: "FontSize") as? String{
            
            if fontSize == "Small"{
                self.txt_FontSize.text = "Small".localized
            }else if fontSize == "Medium"{
                self.txt_FontSize.text = "Medium".localized
            }else if fontSize == "Large"{
                self.txt_FontSize.text = "Large".localized
            }
        }
        
        if let langugae = UserDefaults.standard.value(forKey: Constant.LANGUAGE) as? String{
                
                if langugae == "ar"{
                    self.txt_Language.text = "Arabic".localized
                }else if langugae == "ms"{
                    self.txt_Language.text = "Malay".localized
                }else if langugae == "en"{
                    self.txt_Language.text = "English".localized
                }
            }
        
       
    }
    
    //MARK:TextField Delegate and DataSource------------------------------------------------------------------
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        if textField.text == txt_Code.text{
            let countryView = CountrySelectView.shared
            countryView.barTintColor = .red
            countryView.displayLanguage = .english
            countryView.show()  
            countryView.selectedCountryCallBack = { (countryDic) -> Void in
                //    self.countrycode = "+\(countryDic["code"] as! NSNumber)"
                
                
                self.txt_Code.text = "+\(countryDic["code"] as! NSNumber)"
                
            }
            
        }
    }
    
    
    //MARK:IBACtions------------------------------------------------------------------
    @IBAction func btn_Back(sendr:UIButton){
        
        if fromHome == "fromHome"{
            
            self.dismiss(animated: true, completion: nil)
        }else{
            
            if let controller = self.storyboard?.instantiateViewController(withIdentifier: "TabVC") {
                self.slideMenuController()?.changeMainViewController(controller, close: true)
                
            }
        }
        
        
    }
    
    
    //MARK:IBAction---------------------
    @IBAction func btn_UserPreference(sendr:UIButton){
        //        btn_UserPreferences_Lang.isUserInteractionEnabled = true
        //        btn_UserPreferences_Font.isUserInteractionEnabled = true
        
        let dropDown = DropDown()
        
        if sendr.tag == 2{
            
            dropDown.anchorView = self.txt_Language
            dropDown.bottomOffset = CGPoint(x: 50, y:(dropDown.anchorView?.plainView.bounds.height)!)
            
            // The list of items to display. Can be changed dynamically
            dropDown.dataSource = ["Arabic    ".localized, "Malay    ".localized,"English   ".localized]
            dropDown.textColor = UIColor.black
            dropDown.backgroundColor = UIColor.white
            dropDown.selectionAction = { [unowned self] (index: Int, item: String) in
                print("Selected item: \(item) at index: \(index)")
                
                if index == 0{
                    //mark:for editing value-------------------------------------------------
                                        UserDefaults.standard.set(["ar"], forKey: "AppleLanguages")
                    
                                        UserDefaults.standard.set("ar", forKey: Constant.LANGUAGE)
                                        UserDefaults.standard.synchronize()
                    
                                        self.showChangeLanguageAlert(language:"ar")
                                        self.txt_Language.text = "Arabic".localized
                    
                }else if index == 1{
                    //mark:for delete value-------------------------------------------------
                    
                                        UserDefaults.standard.set(["ms"], forKey: "AppleLanguages")
                    
                                        UserDefaults.standard.set("ms", forKey: Constant.LANGUAGE)
                                        UserDefaults.standard.synchronize()
                                        self.showChangeLanguageAlert(language:"ms")
                                        self.txt_Language.text = "Malay".localized
                    
                }else if index == 2{
                    //mark:for delete value-------------------------------------------------
                                        self.isFontChanged = true
                                        self.txt_Language.text = "English".localized
                    
                                        UserDefaults.standard.set(["en"], forKey: "AppleLanguages")
                    
                                        UserDefaults.standard.set("en", forKey: Constant.LANGUAGE)
                                        UserDefaults.standard.synchronize()
                                        self.showChangeLanguageAlert(language:"en")
                }
            }
            dropDown.show()
        }else if sendr.tag == 1{
            
            
            dropDown.anchorView = self.txt_FontSize
            dropDown.bottomOffset = CGPoint(x: 50, y:(dropDown.anchorView?.plainView.bounds.height)!)
            
            // The list of items to display. Can be changed dynamically
            dropDown.dataSource = ["Small    ".localized, "Medium    ".localized,"Large   ".localized]
            dropDown.textColor = UIColor.black
            dropDown.backgroundColor = UIColor.white
            dropDown.selectionAction = { [unowned self] (index: Int, item: String) in
                print("Selected item: \(item) at index: \(index)")
                if let newFontSize = UserDefaults.standard.value(forKey: "FontSize") as? String
                {
                    
                    UserDefaults.standard.set(newFontSize, forKey: "Current_FontSize")
                    
                }
                if index == 0{
                    //mark:for editing value-------------------------------------------------
                    
                    UserDefaults.standard.set("Small", forKey: "FontSize")
                    self.txt_FontSize.text = "Small".localized
                }else if index == 1{
                    //mark:for delete value-------------------------------------------------
                    self.isFontChanged = true
                    self.txt_FontSize.text = "Medium".localized
                    //  UserDefaults.standard.set(newFontSize, forKey: "Current_FontSize")
                    UserDefaults.standard.set(nil, forKey: "FontSize")
                }else if index == 2{
                    //mark:for delete value-------------------------------------------------
                    self.isFontChanged = true
                    self.txt_FontSize.text = "Large".localized
                    
                    UserDefaults.standard.set("Large", forKey: "FontSize")
                }
                
                
                
            }
            dropDown.show()
            
        }
        
    }
    
    
    @IBAction func btn_UserProfile(sender:UIButton){
        
        if sender.tag == 3{
            if sender.isSelected{
                sender.isSelected = false
                txt_FullName.isUserInteractionEnabled = false
                txt_LastName.isUserInteractionEnabled = false
                txt_Email.isUserInteractionEnabled = false
                txt_Code.isUserInteractionEnabled = false
                txt_PhoneNumber.isUserInteractionEnabled = false
                btn_USerProfileEdit.setImage(#imageLiteral(resourceName: "edit_profile_iPhone"), for: .normal)
                self.EditAdminApi()
            }else{
                sender.isSelected = true
                txt_FullName.isUserInteractionEnabled = true
                txt_LastName.isUserInteractionEnabled = true
                txt_Email.isUserInteractionEnabled = true
                txt_Code.isUserInteractionEnabled = true
                txt_PhoneNumber.isUserInteractionEnabled = true
                btn_USerProfileEdit.setImage(#imageLiteral(resourceName: "edit_profile_tick_iPhone"), for: .normal)
            }
        }else if sender.tag == 4{
            
            if sender.isSelected {
                sender.isSelected = false
                btn_UserPreferences_Lang.isUserInteractionEnabled = false
                btn_UserPreferences_Font.isUserInteractionEnabled = false
                btn_UserPreferencesEdit.setImage(#imageLiteral(resourceName: "edit_profile_iPhone"), for: .normal)
                AppDelegate.instance.sideMenu()
            }else{
                sender.isSelected = true
                btn_UserPreferences_Lang.isUserInteractionEnabled = true
                btn_UserPreferences_Font.isUserInteractionEnabled = true
                
                btn_UserPreferencesEdit.setImage(#imageLiteral(resourceName: "edit_profile_tick_iPhone"), for: .normal)
                
            }
        }
        
    }
    
    
    func showChangeLanguageAlert(language:String) {
        
        //    AKAlertController.alert("Alert", message: "You have selected a new language, press ok to continue".localized, buttons: ["Cancel".localized, "Ok".localized]) { (_, index) in
        //            guard index == 1 else { return }
        
        UserDefaults.standard.set([language], forKey: "AppleLanguages")
        
        UserDefaults.standard.set(language, forKey: Constant.LANGUAGE)
        UserDefaults.standard.synchronize()
        //            if language == "ar" {
        //                UIView.appearance().semanticContentAttribute = .forceLeftToRight
        //            }else{
        //                UIView.appearance().semanticContentAttribute = .forceLeftToRight
        //            }
        UIView.appearance().semanticContentAttribute = .forceLeftToRight
        
        AppDelegate.instance.sideMenu()
        //}
    }
    
    @IBAction func btn_ChangePassword(sender:UIButton){
        if btn_chnagePassword.titleLabel?.text == "Change Password?".localized{
            
            
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "ChangePasswordViewController") as! ChangePasswordViewController
            vc.modalPresentationStyle = .fullScreen
            self.present(vc, animated: true, completion: nil)
        }else{
            
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
            vc.normalUser = "normalUser"
            vc.modalPresentationStyle = .fullScreen
            self.present(vc, animated: true, completion: nil)
        }
        
        
    }
    @IBAction func btn_CreateUser(sender:UIButton){
        //
        //        let vc = self.storyboard?.instantiateViewController(withIdentifier: "OTPViewController") as! OTPViewController
        //        self.present(vc, animated: true, completion: nil)
    }
    //MARK: Functions And Methods-----------------------------------------------
    
    func EditAdminApi(){
        var fullPhoneNumber:String!
        var PhoneNumber:String!
        PhoneNumber =  txt_PhoneNumber.text ?? ""
        let countryCode = self.txt_Code.text ?? ""
        if txt_PhoneNumber.text == ""{
            AKAlertController.alert("Please Enter Mobile Number")
            fullPhoneNumber = ""
        }else{
            
            while (PhoneNumber.hasPrefix("0")) {
                PhoneNumber.remove(at: (PhoneNumber.startIndex))
            }
            fullPhoneNumber = (countryCode + "-" + (PhoneNumber))
            KVNProgress.show()
            Network.shared.ProfileUpdate(email: txt_Email.text ?? "",first_name:txt_FullName.text ?? "", last_name: txt_LastName.text ?? "", mobile_number_verified: true, mobile_number:fullPhoneNumber) { (result) in
                KVNProgress.dismiss()
                guard let user = result else {
                    
                    return
                }
                if user.success == true{
                    self.showSingleAlertMessage(message: "", subMsg:"Profile Updated Successfully.", sender: self, completion:
                        { (success) -> Void in
                            if success == true{
                                UserDefaults.standard.set(self.txt_FullName.text, forKey: "FirstName")
                                UserDefaults.standard.set(self.txt_LastName.text, forKey: "LastName")
                                UserDefaults.standard.set(self.txt_Email.text, forKey: "email")
                                
                                UserDefaults.standard.set(fullPhoneNumber, forKey: "MobileNumber")
                                UserDefaults.standard.synchronize()
                                self.dismiss(animated: true, completion: nil)
                                self.viewWillAppear(true)
                            }
                    })
                }else{
                    self.showSingleAlertMessage(message: "", subMsg:user.message ?? "", sender: self, completion:
                        { (success) -> Void in
                            
                            self.viewWillAppear(true)
                    })
                }
                
            }
        }
        
        
    }
}
extension UILabel{
    override open func awakeFromNib() {
        let old_fontSize = UserDefaults.standard.value(forKey: "Current_FontSize") as? String
        if (old_fontSize != nil){
            
            switch App_FontSize(rawValue: old_fontSize!) {
            case .Small?:
                if let fontSize = UserDefaults.standard.value(forKey: "FontSize") as? String
                {
                    if fontSize == "Small"{
                        self.font = UIFont(name:self.font.fontName,size:self.font.pointSize)
                        
                    }else if fontSize == "Large"{
                        self.font = UIFont(name:self.font.fontName,size:self.font.pointSize + 3)
                    }else if fontSize == "Medium"{
                        self.font = UIFont(name:self.font.fontName,size: self.font.pointSize + 1)
                    }
                    
                }
                break
            case .Large?:
                if let fontSize = UserDefaults.standard.value(forKey: "FontSize") as? String
                {
                    if fontSize == "Small"{
                        self.font = UIFont(name:self.font.fontName,size: self.font.pointSize - 2)
                    }else if fontSize == "Large"{
                        self.font = UIFont(name:self.font.fontName,size:self.font.pointSize + 3)
                    }else if fontSize == "Medium"{
                        //self.font = UIFont(name:self.font.fontName,size:self.font.pointSize - 1)
                    }
                    
                }
                break
            case .Medium?:
                if let fontSize = UserDefaults.standard.value(forKey: "FontSize") as? String
                {
                    if fontSize == "Small"{
                        self.font = UIFont(name:self.font.fontName,size:self.font.pointSize - 2)
                    }else if fontSize == "Large"{
                        self.font = UIFont(name:self.font.fontName,size: self.font.pointSize + 3)
                    }else if fontSize == "Medium"{
                        //self.font = UIFont.systemFont(ofSize: self.font.pointSize)
                    }
                    
                }
                break
            default:
                break
            }
            
        }else{
            if let fontSize = UserDefaults.standard.value(forKey: "FontSize") as? String
            {
                if fontSize == "Small"{
                    self.font = UIFont(name:self.font.fontName,size:self.font.pointSize - 2)
                }else if fontSize == "Large"{
                    self.font = UIFont(name:self.font.fontName,size: self.font.pointSize + 3)
                }else if fontSize == "Medium"{
                    // self.font = UIFont.systemFont(ofSize: self.font.pointSize)
                }
                
            }
        }
        
        
        
    }
}
extension UITextField{
    override open func awakeFromNib() {
        let old_fontSize = UserDefaults.standard.value(forKey: "Current_FontSize") as? String
        if (old_fontSize != nil){
            
            switch App_FontSize(rawValue: old_fontSize!) {
            case .Small?:
                if let fontSize = UserDefaults.standard.value(forKey: "FontSize") as? String
                {
                    if fontSize == "Small"{
                        self.font = UIFont(name:self.font!.fontName,size: self.font!.pointSize)
                    }else if fontSize == "Large"{
                        self.font = UIFont(name:self.font!.fontName,size: self.font!.pointSize + 3)
                    }else if fontSize == "Medium"{
                        self.font = UIFont(name:self.font!.fontName,size: self.font!.pointSize + 1)
                    }
                    
                }
                break
            case .Large?:
                if let fontSize = UserDefaults.standard.value(forKey: "FontSize") as? String
                {
                    if fontSize == "Small"{
                        self.font = UIFont(name:self.font!.fontName,size: self.font!.pointSize - 2)
                    }else if fontSize == "Large"{
                        self.font = UIFont(name:self.font!.fontName,size: self.font!.pointSize)
                    }else if fontSize == "Medium"{
                        self.font = UIFont(name:self.font!.fontName,size: self.font!.pointSize - 1)
                    }
                    
                }
                break
            case .Medium?:
                if let fontSize = UserDefaults.standard.value(forKey: "FontSize") as? String
                {
                    if fontSize == "Small"{
                        self.font = UIFont(name:self.font!.fontName,size: self.font!.pointSize - 2)
                    }else if fontSize == "Large"{
                        self.font = UIFont(name:self.font!.fontName,size: self.font!.pointSize + 3)
                    }else if fontSize == "Medium"{
                        //self.font = UIFont.systemFont(ofSize: self.font!.pointSize)
                    }
                    
                }
                break
            default:
                break
            }
            
        }else{
            if let fontSize = UserDefaults.standard.value(forKey: "FontSize") as? String
            {
                if fontSize == "Small"{
                    self.font = UIFont(name:self.font!.fontName,size: self.font!.pointSize )
                }else if fontSize == "Large"{
                    self.font = UIFont(name:self.font!.fontName,size: self.font!.pointSize + 3)
                }else if fontSize == "Medium"{
                    //self.font = UIFont.systemFont(ofSize: self.font!.pointSize )
                }
                
            }
        }
        
        
        
    }
}
extension UITextView{
    override open func awakeFromNib() {
        let old_fontSize = UserDefaults.standard.value(forKey: "Current_FontSize") as? String
        if (old_fontSize != nil){
            
            switch App_FontSize(rawValue: old_fontSize!) {
            case .Small?:
                if let fontSize = UserDefaults.standard.value(forKey: "FontSize") as? String
                {
                    if fontSize == "Small"{
                        self.font = UIFont(name:self.font!.fontName,size: self.font!.pointSize)
                    }else if fontSize == "Large"{
                        self.font = UIFont(name:self.font!.fontName,size: self.font!.pointSize + 3)
                    }else if fontSize == "Medium"{
                        self.font = UIFont(name:self.font!.fontName,size: self.font!.pointSize + 1)
                    }
                    
                }
                break
            case .Large?:
                if let fontSize = UserDefaults.standard.value(forKey: "FontSize") as? String
                {
                    if fontSize == "Small"{
                        self.font = UIFont(name:self.font!.fontName,size: self.font!.pointSize - 2)
                    }else if fontSize == "Large"{
                        self.font = UIFont(name:self.font!.fontName,size: self.font!.pointSize)
                    }else if fontSize == "Medium"{
                        self.font = UIFont(name:self.font!.fontName,size: self.font!.pointSize - 1)
                    }
                    
                }
                break
            case .Medium?:
                if let fontSize = UserDefaults.standard.value(forKey: "FontSize") as? String
                {
                    if fontSize == "Small"{
                        self.font = UIFont(name:self.font!.fontName,size: self.font!.pointSize - 2)
                    }else if fontSize == "Large"{
                        self.font = UIFont(name:self.font!.fontName,size: self.font!.pointSize + 3)
                    }else if fontSize == "Medium"{
                        //self.font = UIFont.systemFont(ofSize: self.font!.pointSize)
                    }
                    
                }
                break
            default:
                break
            }
            
        }else{
            if let fontSize = UserDefaults.standard.value(forKey: "FontSize") as? String
            {
                if fontSize == "Small"{
                    self.font = UIFont(name:self.font!.fontName,size: self.font!.pointSize )
                }else if fontSize == "Large"{
                    self.font = UIFont(name:self.font!.fontName,size: self.font!.pointSize + 3)
                }else if fontSize == "Medium"{
                    // self.font = UIFont.systemFont(ofSize: self.font!.pointSize)
                }
                
            }
        }
        
        
        
    }
}
extension UIButton{
    override open func awakeFromNib() {
        let old_fontSize = UserDefaults.standard.value(forKey: "Current_FontSize") as? String
        if (old_fontSize != nil){
            
            switch App_FontSize(rawValue: old_fontSize!) {
            case .Small?:
                if let fontSize = UserDefaults.standard.value(forKey: "FontSize") as? String
                {
                    if fontSize == "Small"{
                        titleLabel!.font = UIFont(name:self.titleLabel!.font.fontName,size: titleLabel!.font.pointSize)
                    }else if fontSize == "Large"{
                        titleLabel!.font = UIFont(name:self.titleLabel!.font.fontName,size: titleLabel!.font.pointSize + 3)
                    }else if fontSize == "Medium"{
                        titleLabel!.font = UIFont(name:self.titleLabel!.font.fontName,size: titleLabel!.font.pointSize + 2)
                    }
                    
                }
                break
            case .Large?:
                if let fontSize = UserDefaults.standard.value(forKey: "FontSize") as? String
                {
                    if fontSize == "Small"{
                        titleLabel!.font = UIFont(name:self.titleLabel!.font.fontName,size: titleLabel!.font.pointSize - 3)
                    }else if fontSize == "Large"{
                        titleLabel!.font = UIFont(name:self.titleLabel!.font.fontName,size: titleLabel!.font.pointSize + 3)
                    }else if fontSize == "Medium"{
                        // titleLabel!.font = UIFont(name:self.titleLabel!.font.fontName,size: titleLabel!.font.pointSize - 1)
                    }
                    
                }
                break
            case .Medium?:
                if let fontSize = UserDefaults.standard.value(forKey: "FontSize") as? String
                {
                    if fontSize == "Small"{
                        titleLabel!.font = UIFont(name:self.titleLabel!.font.fontName,size: titleLabel!.font.pointSize - 2)
                    }else if fontSize == "Large"{
                        titleLabel!.font = UIFont(name:self.titleLabel!.font.fontName,size: titleLabel!.font.pointSize + 3)
                    }else if fontSize == "Medium"{
                        // titleLabel!.font = UIFont.systemFont(ofSize: titleLabel!.font.pointSize)
                    }
                    
                }
                break
            default:
                break
            }
            
        }else{
            
            if let fontSize = UserDefaults.standard.value(forKey: "FontSize") as? String
            {
                if fontSize == "Small"{
                    titleLabel!.font = UIFont(name:self.titleLabel!.font.fontName,size: titleLabel!.font.pointSize )
                }else if fontSize == "Large"{
                    titleLabel!.font = UIFont(name:self.titleLabel!.font.fontName,size: titleLabel!.font.pointSize + 3)
                }else if fontSize == "Medium"{
                    //  titleLabel!.font = UIFont.systemFont(ofSize: titleLabel!.font.pointSize)
                }
                
            }
        }
        
        
        
    }
    
}

extension String {
    var localized: String {
        let language = UserDefaults.standard.array(forKey: "AppleLanguages")?.first as? String
        let bundlePath = Bundle.main.path(forResource: language, ofType: "lproj") ?? ""
        let bundle = Bundle(path: bundlePath) ?? Bundle.main
        return NSLocalizedString(self, tableName: nil, bundle: bundle, value: "", comment: "")
    }
}

