//
//  MenuViewController.swift
//  Muslim Hub
//
//  Created by Sakshi Singh on 12/04/19.
//  Copyright © 2019 OnGraph. All rights reserved.
//

import UIKit
import KVNProgress
import SlideMenuControllerSwift

class MenuViewController: UIViewController {
    
    @IBOutlet weak var tableView_Menu:UITableView!
    @IBOutlet weak var layout_HeaderSpacing:NSLayoutConstraint!
    
    let array_Tilte = ["MH Admin Announcement","Register Organization","Feedback","Eid Salah","Admin Login","User Profile","About MH"]
    
    var array_SuperAdmin = ["MH Admin Announcement","On Board Organization","Feedback","Eid Salah","Manage Admin","Admin Login","User Profile","About MH"]
    
    var array_BEforeLogin = ["MH Admin Announcement","Feedback","Eid Salah","Admin Login","Register","About MH"]
    var array_beforeLoginImg = [#imageLiteral(resourceName: "mh_annoucement_iPhone"),#imageLiteral(resourceName: "feedback_iPhone"),#imageLiteral(resourceName: "eid_salah_iPhone"),#imageLiteral(resourceName: "user_profile_iPhone"),#imageLiteral(resourceName: "user_profile_iPhone"),#imageLiteral(resourceName: "about_mh_iPhone")]
    
    var array_superAdminImg = [#imageLiteral(resourceName: "mh_annoucement_iPhone"),#imageLiteral(resourceName: "register_org_iPhone"),#imageLiteral(resourceName: "feedback_iPhone"),#imageLiteral(resourceName: "eid_salah_iPhone"),#imageLiteral(resourceName: "manage_admin_iPhone"),#imageLiteral(resourceName: "user_profile_iPhone"),#imageLiteral(resourceName: "manage_user_iPhone"),#imageLiteral(resourceName: "about_mh_iPhone")]
    var isSuper = Bool()
    var isStaff = Bool()
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        self.tabBarController?.tabBar.isHidden = false
        tableView_Menu.tableFooterView = UIView()
        
        if UIDevice().userInterfaceIdiom == .phone {
            switch UIScreen.main.nativeBounds.height {
                
                
            case 2436:
                print("iPhone X, XS")
                layout_HeaderSpacing.constant = 40.0
            default:
                print("Unknown")
                layout_HeaderSpacing.constant = 20.0
            }
        }
        
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        
        if let loginToken = UserDefaults.standard.value(forKey: "Login_Token") as? String{
            print(loginToken)
            
            if let superAdmin = UserDefaults.standard.value(forKey: "isSuperAdmin") as? Bool{
                isSuper = superAdmin
                if superAdmin == true{
                    self.array_SuperAdmin = ["MH Admin Announcement","On Board Organization","Feedback","Eid Salah","Manage Admin","User Profile","Manage User","About MH","Logout"]
                    self.array_superAdminImg = [#imageLiteral(resourceName: "mh_annoucement_iPhone"),#imageLiteral(resourceName: "register_org_iPhone"),#imageLiteral(resourceName: "feedback_iPhone"),#imageLiteral(resourceName: "eid_salah_iPhone"),#imageLiteral(resourceName: "manage_admin_iPhone"),#imageLiteral(resourceName: "user_profile_iPhone"),#imageLiteral(resourceName: "manage_user_iPhone"),#imageLiteral(resourceName: "about_mh_iPhone"),#imageLiteral(resourceName: "logout_iPhone")]
                }else{
                    self.array_SuperAdmin = ["MH Admin Announcement","Request Organisation","Feedback","Eid Salah","Manage Admin","User Profile","Manage Users","About MH","Logout"]
                    self.array_superAdminImg = [#imageLiteral(resourceName: "mh_annoucement_iPhone"),#imageLiteral(resourceName: "register_org_iPhone"),#imageLiteral(resourceName: "feedback_iPhone"),#imageLiteral(resourceName: "eid_salah_iPhone"),#imageLiteral(resourceName: "manage_admin_iPhone"),#imageLiteral(resourceName: "user_profile_iPhone"),#imageLiteral(resourceName: "manage_user_iPhone"),#imageLiteral(resourceName: "about_mh_iPhone"),#imageLiteral(resourceName: "logout_iPhone")]
                }
            }
            
            if let is_staff = UserDefaults.standard.value(forKey: "is_staff") as? Bool{
                isStaff = is_staff
                if is_staff == false{
                    self.array_SuperAdmin = ["MH Admin Announcement","Feedback","Eid Salah","Admin Login","About MH"]
                    self.array_superAdminImg  = [#imageLiteral(resourceName: "mh_annoucement_iPhone"),#imageLiteral(resourceName: "feedback_iPhone"),#imageLiteral(resourceName: "eid_salah_iPhone"),#imageLiteral(resourceName: "user_profile_iPhone"),#imageLiteral(resourceName: "about_mh_iPhone")]
                }
            }
            
            if isStaff == false && isSuper == false{
                // self.array_SuperAdmin = ["MH Admin Announcement","Feedback","Eid Salah","Admin Login","About MH","User Profile","Logout"]
                self.array_SuperAdmin = ["MH Admin Announcement","Feedback","Eid Salah","About MH","User Profile","Logout"]
                self.array_superAdminImg  = [#imageLiteral(resourceName: "mh_annoucement_iPhone"),#imageLiteral(resourceName: "feedback_iPhone"),#imageLiteral(resourceName: "eid_salah_iPhone"),#imageLiteral(resourceName: "about_mh_iPhone"),#imageLiteral(resourceName: "user_profile_iPhone"),#imageLiteral(resourceName: "logout_iPhone")]
            }
        }else{
            
            
            self.array_SuperAdmin = ["MH Admin Announcement","Request Organisation","Feedback","Eid Salah","Admin Login","About MH"]
            self.array_superAdminImg  = [#imageLiteral(resourceName: "mh_annoucement_iPhone"),#imageLiteral(resourceName: "register_org_iPhone"),#imageLiteral(resourceName: "feedback_iPhone"),#imageLiteral(resourceName: "eid_salah_iPhone"),#imageLiteral(resourceName: "user_profile_iPhone"),#imageLiteral(resourceName: "about_mh_iPhone")]
        }
        
        
        tableView_Menu.reloadData()
        
    }
    @IBAction func btn_Back(sender:UIButton){
        
        self.dismiss(animated: true, completion: nil)
    }
    
    
}
extension MenuViewController:UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return array_SuperAdmin.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "MenuTableViewCell", for: indexPath) as! MenuTableViewCell
        cell.lbl_Title.text = array_SuperAdmin[indexPath.row].localized
        cell.img_Title.image = array_superAdminImg[indexPath.row]
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        var viewController: UIViewController!
        
        switch indexPath.row {
        case 0:
            
            viewController = self.storyboard?.instantiateViewController(withIdentifier: "MHAnnouncementListsViewController") as! MHAnnouncementListsViewController
            
            self.slideMenuController()?.changeMainViewController(viewController, close: false)
        case 1:
            
            if let loginToken = UserDefaults.standard.value(forKey: "Login_Token") as? String{
                print(loginToken)
                
                if let isSuperAdmin = UserDefaults.standard.value(forKey: "isSuperAdmin") as? Bool{
                    
                    if isSuperAdmin == false{
                        if isStaff == false && isSuper == false{
                            viewController = self.storyboard?.instantiateViewController(withIdentifier: "FeedBackViewController") as! FeedBackViewController
                        }else{
                            viewController = self.storyboard?.instantiateViewController(withIdentifier: "RequestOrgnisationViewController") as! RequestOrgnisationViewController
                            
                        }
                        
                    }else{
                        viewController = self.storyboard?.instantiateViewController(withIdentifier: "SuperAdminRegisterViewController") as! SuperAdminRegisterViewController
                    }
                }
                
                
            }else{
                viewController = self.storyboard?.instantiateViewController(withIdentifier: "RequestOrgnisationViewController") as! RequestOrgnisationViewController
                
            }
            self.slideMenuController()?.changeMainViewController(viewController, close: false)
        case 2:
            if let loginToken = UserDefaults.standard.value(forKey: "Login_Token") as? String
            {   print(loginToken)
                
                if let isSuperAdmin = UserDefaults.standard.value(forKey: "isSuperAdmin") as? Bool{
                    
                    if isSuperAdmin == false{
                        if let isStaff = UserDefaults.standard.value(forKey: "is_staff") as? Bool{
                            if isStaff == true{
                                viewController = self.storyboard?.instantiateViewController(withIdentifier: "FeedBackViewController") as! FeedBackViewController
                            }else{
                                viewController = self.storyboard?.instantiateViewController(withIdentifier: "MHEidSalahVC") as! MHEidSalahVC
                            }
                        }
                        
                    }else{
                        viewController = self.storyboard?.instantiateViewController(withIdentifier: "FeedBackViewController") as! FeedBackViewController
                    }
                    
                }
                
            }else{
                
                viewController = self.storyboard?.instantiateViewController(withIdentifier: "FeedBackViewController") as! FeedBackViewController
                
            }
            
            self.slideMenuController()?.changeMainViewController(viewController, close: false)
            
            
        case 3:
            if let loginToken = UserDefaults.standard.value(forKey: "Login_Token") as? String{   print(loginToken)
                
                if let isSuperAdmin = UserDefaults.standard.value(forKey: "isSuperAdmin") as? Bool{
                    
                    if isSuperAdmin == false{
                        
                        if let isStaff = UserDefaults.standard.value(forKey: "is_staff") as? Bool{
                            if isStaff == true{
                                viewController = self.storyboard?.instantiateViewController(withIdentifier: "MHEidSalahVC") as! MHEidSalahVC
                            }
                            else{
                                
                                if isStaff == false && isSuper == false{
                                    viewController = self.storyboard?.instantiateViewController(withIdentifier: "AboutUsViewController") as! AboutUsViewController
                                }else{
                                    viewController = self.storyboard?.instantiateViewController(withIdentifier: "ManageAdminViewController") as! ManageAdminViewController
                                    UserDefaults.standard.set("", forKey: "ManageUser")
                                    
                                }
                                
                            }
                        }
                        
                        
                    }else{
                        viewController = self.storyboard?.instantiateViewController(withIdentifier: "MHEidSalahVC") as! MHEidSalahVC
                    }
                    
                }
                
            }else{
                viewController = self.storyboard?.instantiateViewController(withIdentifier: "MHEidSalahVC") as! MHEidSalahVC
                
            }
            self.slideMenuController()?.changeMainViewController(viewController, close: false)
        case 4:
            
            
            if let loginToken = UserDefaults.standard.value(forKey: "Login_Token") as? String{
                if let isSuperAdmin = UserDefaults.standard.value(forKey: "isSuperAdmin") as? Bool{
                    
                    if isSuperAdmin == false{
                        
                        if let isStaff = UserDefaults.standard.value(forKey: "is_staff") as? Bool{
                            if isStaff == true{
                                viewController = self.storyboard?.instantiateViewController(withIdentifier: "ManageAdminViewController") as! ManageAdminViewController
                                UserDefaults.standard.set("", forKey: "ManageUser")
                            }else{
                                viewController = self.storyboard?.instantiateViewController(withIdentifier: "ProfileViewController") as! ProfileViewController
                            }
                        }
                        
                        
                    }else{
                        viewController = self.storyboard?.instantiateViewController(withIdentifier: "ManageAdminViewController") as! ManageAdminViewController
                        print(loginToken)
                        UserDefaults.standard.set("", forKey: "ManageUser")
                    }
                    
                }
                
            }else{
                
                viewController = self.storyboard?.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
                
            }
            self.slideMenuController()?.changeMainViewController(viewController, close: false)
        case 5:
            if let loginToken = UserDefaults.standard.value(forKey: "Login_Token") as? String{
                print(loginToken)
                
                if isStaff == false && isSuper == false{
                    self.logoutApi()
                }else{
                    viewController = self.storyboard?.instantiateViewController(withIdentifier: "ProfileViewController") as! ProfileViewController
                    self.slideMenuController()?.changeMainViewController(viewController, close: false)
                }
                
                
            }else{
                viewController = self.storyboard?.instantiateViewController(withIdentifier: "AboutUsViewController") as! AboutUsViewController
                self.slideMenuController()?.changeMainViewController(viewController, close: false)
            }
            
        case 6:
            print("Mange User")
            if let loginToken = UserDefaults.standard.value(forKey: "Login_Token") as? String{
                print(loginToken)
                if let isSuperAdmin = UserDefaults.standard.value(forKey: "isSuperAdmin") as? Bool{
                    
                    if isSuperAdmin == false{
                        if let isStaff = UserDefaults.standard.value(forKey: "is_staff") as? Bool{
                            if isStaff == true{
                                //Mamage User
                                viewController = self.storyboard?.instantiateViewController(withIdentifier: "ManageAdminViewController") as! ManageAdminViewController
                                UserDefaults.standard.set("ManageUser", forKey: "ManageUser")
                                self.slideMenuController()?.changeMainViewController(viewController, close: false)
                                
                            }else{
                                
                                viewController = self.storyboard?.instantiateViewController(withIdentifier: "ManageAdminViewController") as! ManageAdminViewController
                                UserDefaults.standard.set("ManageUser", forKey: "ManageUser")
                                self.slideMenuController()?.changeMainViewController(viewController, close: false)
                            }
                            
                            
                        }
                    }
                        
                    else{
                        viewController = self.storyboard?.instantiateViewController(withIdentifier: "ManageAdminViewController") as! ManageAdminViewController
                        UserDefaults.standard.set("ManageUser", forKey: "ManageUser")
                        self.slideMenuController()?.changeMainViewController(viewController, close: false)
                        
                    }
                    
                    
                }else{
                    viewController = self.storyboard?.instantiateViewController(withIdentifier: "AboutUsViewController") as! AboutUsViewController
                    self.slideMenuController()?.changeMainViewController(viewController, close: false)
                }
            }
        case 7:
            
            if let isSuperAdmin = UserDefaults.standard.value(forKey: "isSuperAdmin") as? Bool{
                
                if isSuperAdmin == false{
                    AppDelegate.instance.isLogout = true
                    
                    if let isStaff = UserDefaults.standard.value(forKey: "is_staff") as? Bool{
                        if isStaff == true{
                            //Mamage User
                            viewController = self.storyboard?.instantiateViewController(withIdentifier: "AboutUsViewController") as! AboutUsViewController
                            self.slideMenuController()?.changeMainViewController(viewController, close: true)
                        }else{
                            if let loginToken = UserDefaults.standard.value(forKey: "Login_Token") as? String{
                                print(loginToken)
                                let device_id = UserDefaults.standard.value(forKey: "deviceToken") as! String
                                
                                AKAlertController.alert("", message: "Are you sure you want to logout from the app?".localized, buttons: ["Ok".localized,"Cancel".localized], tapBlock: { (_, _, index) in
                                    if index == 1 {return}
                                    
                                    KVNProgress.show()
                                    
                                    Network.shared.logOut(token:loginToken, device_id: device_id){ (result) in
                                        
                                        
                                        KVNProgress.dismiss()
                                        
                                        guard let user = result else {
                                            
                                            return
                                        }
                                        print(user)
                                        
                                        if user.success == true{
                                            
                                            UserDefaults.standard.set(nil, forKey: "Login_Token")
                                            UserDefaults.standard.set(nil, forKey: "isSuperAdmin")
                                            UserDefaults.standard.set(nil, forKey: "is_staff")
                                            
                                            
                                            AppDelegate.instance.AppAdminUser = nil
                                            UserDefaults.standard.synchronize()
                                            self.tableView_Menu.reloadData()
                                            AppDelegate.instance.sideMenu()
                                        }
                                        
                                    }
                                    
                                    
                                    
                                })
                                //
                                
                                
                            }
                            
                        }
                    }
                    
                    
                    
                }else{
                    
                    viewController = self.storyboard?.instantiateViewController(withIdentifier: "AboutUsViewController") as! AboutUsViewController
                    self.slideMenuController()?.changeMainViewController(viewController, close: false)
                }
            }else{
                viewController = self.storyboard?.instantiateViewController(withIdentifier: "AboutUsViewController") as! AboutUsViewController
                self.slideMenuController()?.changeMainViewController(viewController, close: false)
            }
        case 8:
            AppDelegate.instance.isLogout = true
            if let loginToken = UserDefaults.standard.value(forKey: "Login_Token") as? String{
                print(loginToken)
                let device_id = UserDefaults.standard.value(forKey: "deviceToken") as! String
                
                AKAlertController.alert("", message: "Are you sure you want to logout from the app?".localized, buttons: ["Ok".localized,"Cancel".localized], tapBlock: { (_, _, index) in
                    if index == 1 {return}
                    
                    KVNProgress.show()
                    
                    Network.shared.logOut(token:loginToken, device_id: device_id){ (result) in
                        
                        
                        KVNProgress.dismiss()
                        
                        guard let user = result else {
                            
                            return
                        }
                        print(user)
                        
                        if user.success == true{
                            
                            UserDefaults.standard.set(nil, forKey: "Login_Token")
                            UserDefaults.standard.set(nil, forKey: "isSuperAdmin")
                            UserDefaults.standard.set(nil, forKey: "is_staff")
                            UserDefaults.standard.synchronize()
                            AppDelegate.instance.AppAdminUser = nil
                            self.tableView_Menu.reloadData()
                            AppDelegate.instance.sideMenu()
                        }
                        
                    }
                    
                    
                    
                })
                //
                
                
            }
            else{
                viewController = self.storyboard?.instantiateViewController(withIdentifier: "AboutUsViewController") as! AboutUsViewController
                
                self.slideMenuController()?.changeMainViewController(viewController, close: false)
                
            }
            
            
        default:
            viewController = self.storyboard?.instantiateViewController(withIdentifier: "ProfileViewController") as! ProfileViewController
            self.slideMenuController()?.changeMainViewController(viewController, close: false)
        }
        
        if AppDelegate.instance.isLogout == true{
            
            print("Logout case")
        }else{
            self.toggleLeft()
            print("toggle Left")
            
        }
        
    }
    
    func logoutApi(){
        
        AppDelegate.instance.isLogout = true
        if let loginToken = UserDefaults.standard.value(forKey: "Login_Token") as? String{
            print(loginToken)
            
            
            AKAlertController.alert("", message: "Are you sure you want to logout from the app?".localized, buttons: ["Ok".localized,"Cancel".localized], tapBlock: { (_, _, index) in
                if index == 1 {return}
                
                KVNProgress.show()
                let device_id = UserDefaults.standard.value(forKey: "deviceToken") as! String
                
                Network.shared.logOut(token:loginToken, device_id: device_id){ (result) in
                    
                    
                    KVNProgress.dismiss()
                    
                    guard let user = result else {
                        
                        return
                    }
                    print(user)
                    
                    if user.success == true{
                        
                        UserDefaults.standard.set(nil, forKey: "Login_Token")
                        UserDefaults.standard.set(nil, forKey: "isSuperAdmin")
                        UserDefaults.standard.set(nil, forKey: "is_staff")
                        UserDefaults.standard.synchronize()
                        AppDelegate.instance.AppAdminUser = nil
                        self.tableView_Menu.reloadData()
                        AppDelegate.instance.sideMenu()
                    }
                    
                }
                
                
                
            })
            //
            
            
        }
    }
    
}
