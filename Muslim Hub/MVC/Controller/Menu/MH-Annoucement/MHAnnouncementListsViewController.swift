//
//  MHAnnouncementListsViewController.swift
//  Muslim Hub
//
//  Created by Rohit on 13/05/19.
//  Copyright © 2019 OnGraph. All rights reserved.
//

import UIKit
import KVNProgress
import SDWebImage
import SKPhotoBrowser
import Branch
import ActiveLabel
import TTTAttributedLabel


class MHAnnouncementListsViewController: UIViewController,ActiveLabelDelegate,UIGestureRecognizerDelegate {
    func didSelect(_ text: String, type: ActiveType) {
        
    }
    
    
    @IBOutlet weak var tableView_Announcement:UITableView!
    @IBOutlet weak var btn_add:UIButton!
    @IBOutlet weak var lbl_HeaderTitle:UILabel!
    var announcementLists = [AnnouncementData]()
    var imageSelecetd = 0
    var shareDate = ""
    
    var phoneNumber:String!
    var urlPhoneNumber:String!
    var array_String = [String]()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        if #available(iOS 13.0, *) {
            statusBarColorChange(color:UIColor.init(hex:  0x05BDB9))
        } else {
            UIApplication.statusBarBackgroundColor = UIColor.init(hex: 0x05BDB9)
        }
        if let loginToken = UserDefaults.standard.value(forKey: "Login_Token") as? String{
                       print(loginToken)
                       if let superAdmin = UserDefaults.standard.value(forKey: "isSuperAdmin") as? Bool{
                           
                           if superAdmin == true{
                               self.btn_add.isHidden = false
                              
                           }else{
                               
                               self.btn_add.isHidden = true
                              
                               
                           }
                       }
                   }else{
                       self.btn_add.isHidden = true
                      
                   }
        self.lbl_HeaderTitle.text = "MH Announcement".localized
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.announcementListApi()
        
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    //MARK: IBAction-----------------------------------------------
    @IBAction func btn_back(sender:UIButton){
        
        
        if let controller = self.storyboard?.instantiateViewController(withIdentifier: "TabVC") {
            self.slideMenuController()?.changeMainViewController(controller, close: true)
            
        }
        
        
        
    }
    @IBAction func btn_AddAnnouncement(sender:UIButton){
        
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "AddAnnoucementViewController") as! AddAnnoucementViewController
        vc.addNewAnnouncent = "addNewAnnouncent"
        vc.modalPresentationStyle = .fullScreen
        self.present(vc, animated: true, completion: nil)
    }
    @objc func btn_Delete(sender : UIButton!) {
        
        
        AKAlertController.alert("", message: "Do you really want to delete this announcement?".localized, buttons: ["Ok".localized,"Cancel".localized], tapBlock: { (_, _, index) in
            if index == 1 {return}
            let admin_id = self.announcementLists[sender.tag].id ?? 0
            let admin_id_str = String(admin_id)
            UserDefaults.standard.set(admin_id_str, forKey: "announcement_id")
            self.announcementLists.remove(at: sender.tag)
            self.adminDeleteApi()
            return
        })
        
        
        
    }
    @objc func btn_Edit(sender : UIButton) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "AddAnnoucementViewController") as! AddAnnoucementViewController
        vc.annoucementData = announcementLists[sender.tag]
        vc.modalPresentationStyle = .fullScreen
        self.present(vc, animated: true, completion: nil)
    }
    
    @objc func btn_shareTextButton(sender: UIButton) {
        
        var image = String()
        
      
        for (index, element) in (self.announcementLists[sender.tag].images?.enumerated())! {
                print("Item \(index): \(element)")
                
                
                image = image + "\n\n\(element)"
                
            }
       
      
        
        let buo = BranchUniversalObject.init(canonicalIdentifier: "content/12345")
        buo.title = self.announcementLists[sender.tag].title
        buo.contentDescription = "MH Announcement"
        //  buo.imageUrl = self.homeList[sender.tag].images[0]
        buo.publiclyIndex = true
        buo.locallyIndex = true
        buo.contentMetadata.customMetadata["MH_Announcement"] = "true"
        
        let lp: BranchLinkProperties = BranchLinkProperties()
        
        
        var brachioUrl = String()
        
        buo.getShortUrl(with: lp) { (url, error) in
            print(url ?? "")
            brachioUrl = url ?? ""
            
            
            if image != ""{
                let data = (self.announcementLists[sender.tag].description!) + "\n \n" + "Shared from MuslimHub. Download from Apple Store" + "\n" + "https://apps.apple.com/au/app/muslim-hub/id1014406101"
                
                
                let abbreviation = image + "\n\n" + self.announcementLists[sender.tag].title! + "\n \n" + "Take me to the event in MuslimHub \n" + brachioUrl + "\n\n" + data
                self.btn_ShareTapp(details: [abbreviation])
            }else{
                let data =   (self.announcementLists[sender.tag].description!) + "\n \n" + "Shared from MuslimHub. Download from Apple Store" + "\n"
                let abbreviation = self.announcementLists[sender.tag].title! + "\n \n" + "Take me to the event in MuslimHub \n"  + brachioUrl + "\n\n" + data + "https://apps.apple.com/au/app/muslim-hub/id1014406101"

                self.btn_ShareTapp(details: [abbreviation])
            }
            
        
    }
}
    
    //Mark:Function Api-----------------------------
    func adminDeleteApi(){
        KVNProgress.show()
        Network.shared.deletAnnouncementApi() { (result) in
            KVNProgress.dismiss()
            guard result != nil else {
                
                return
            }
            if result?.success == false{
                
                AKAlertController.alert("", message: result?.message ?? "Something went wrong")
            }else {
                AKAlertController.alert("", message: result?.message ?? "")
            }
            self.tableView_Announcement.reloadData()
        }
    }
    func announcementListApi(){
        KVNProgress.show()
        Network.shared.announcementLists() { (result) in
            KVNProgress.dismiss()
            guard let user = result else {
                return
            }
            self.announcementLists = user
            
            if self.announcementLists.count != 0{
                for i in 0...self.announcementLists.count - 1{
                    var arraySelectedImage: [Bool] = []
                    if self.announcementLists[i].images!.count > 0{
                        for j in 0...self.announcementLists[i].images!.count - 1{
                            if j == 0{
                                arraySelectedImage.append(true)
                            }else{
                                arraySelectedImage.append(false)
                            }
                        }
                    }
                    self.announcementLists[i].selectedImage_Array = arraySelectedImage
                }
            }
            
            
            
            self.tableView_Announcement.reloadData()
        }
    }
    
    //Get Date
    func GetDate(TimeStamp:Double,DateFormat:String) -> String
    {
        //        let date = NSDate(timeIntervalSinceReferenceDate: TimeStamp)
        let date = NSDate(timeIntervalSince1970:TimeStamp)
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = DateFormat//"MM-dd-yyyy"// HH:mm:ss"
        dateFormatter.locale = Locale(identifier: "en")
        dateFormatter.timeZone = TimeZone.current
        //                dateFormatter.locale = NSLocale.current
        return dateFormatter.string(from: date as Date)
    }
    
    @objc func imageTapped(tapGestureRecognizer: UITapGestureRecognizer)
    {
        _ = tapGestureRecognizer.view as! UIImageView
        
        
        //
        //        let vc: DetailProductVC = UIStoryboard(name: "Main", bundle:
        //            Bundle.main).instantiateViewController(withIdentifier:
        //                "DetailProductVC") as! DetailProductVC
        //        vc.modalPresentationStyle = .overCurrentContext
        //        vc.modalTransitionStyle = .crossDissolve
        //        let tappedImageView = tapGestureRecognizer.view!
        //        let imageView = tappedImageView as! UIImageView
        //        vc.imageSend =  imageView.image
        //        vc.indexTag =  self.imageSelecetd
        //        vc.allData = self.announcementLists[(tapGestureRecognizer.view?.tag)!].images
        //  isMethodCall = true
        SKPhotoBrowserOptions.displayAction = false
        var images = [SKPhoto]()
        for i in 0..<self.announcementLists[(tapGestureRecognizer.view?.tag)!].images!.count{
            
            let photo = SKPhoto.photoWithImageURL(self.announcementLists[(tapGestureRecognizer.view?.tag)!].images![i])
            photo.shouldCachePhotoURLImage = false // you can use image cache by true(NSCache)
            images.append(photo)
        }
        
        
        
        //let cell = collectionView.cellForItem(at: indexPath) as! DetailProductCell
        // let originImage = cell.img_view!.image
        
        let browser = SKPhotoBrowser(originImage: #imageLiteral(resourceName: "default_header_iPhone"), photos: images, animatedFromView: self.view)
        for i in 0..<self.announcementLists[(tapGestureRecognizer.view?.tag)!].images!.count{
            browser.initializePageIndex(self.imageSelecetd)
        }
        present(browser, animated: true, completion: {})
        //  self.present(vc, animated: true, completion: nil)
        
        
    }
    
}


//MARK:TableView-------------------------------------
extension MHAnnouncementListsViewController:UITableViewDelegate,UITableViewDataSource,ShowMoreCellDelegate,IndexImageSendCellDelegateAnnouncement  {
    func IndexImageSendCellDelegateAnnouncement(cell: MHAnnouncementListsTableViewCell, selected_imgIndex: Int) {
        
        self.imageSelecetd = selected_imgIndex
    }
    
    
    // MARK: - my cell delegate
    func moreTapped(cell: MHAnnouncementListsTableViewCell) {
        
        
        tableView_Announcement.beginUpdates()
        
        tableView_Announcement.endUpdates()
        
        
        
    }
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return announcementLists.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "MHAnnouncementListsTableViewCell", for: indexPath) as! MHAnnouncementListsTableViewCell
        
        cell.img_Viewannouncement.tag = indexPath.row
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(imageTapped(tapGestureRecognizer:)))
        cell.IndexImageSendCellDelegateAnnouncement = self
        cell.img_Viewannouncement.isUserInteractionEnabled = true
        cell.img_Viewannouncement.addGestureRecognizer(tapGestureRecognizer)
        
        if let loginToken = UserDefaults.standard.value(forKey: "Login_Token") as? String{
            print(loginToken)
            if let superAdmin = UserDefaults.standard.value(forKey: "isSuperAdmin") as? Bool{
                
                if superAdmin == true{
                    self.btn_add.isHidden = false
                    cell.btn_Edit.isHidden = false
                    cell.btn_Delete.isHidden = false
                }else{
                    
                    self.btn_add.isHidden = true
                    cell.btn_Edit.isHidden = true
                    cell.btn_Delete.isHidden = true
                    self.btn_add.isHidden = true
                    
                }
            }
        }else{
            self.btn_add.isHidden = true
            cell.btn_Edit.isHidden = true
            cell.btn_Delete.isHidden = true
        }
        cell.view_Announcment.layer.cornerRadius = 10.0
        cell.view_Announcment.shadowBlackToHeader()
        cell.delegate = self
        let data = announcementLists[indexPath.row]
        cell.setupImageCollection(data: data, index: indexPath.row, tableViewObject: self.tableView_Announcement)
        
        if (data.images?.count ?? 0) > 0 {
            cell.img_Viewannouncement.isHidden = false
            cell.collection_Images.isHidden = false
            cell.hConstImgView.constant = 170
            cell.hConstCollectionView.constant = 40
        } else {
            cell.img_Viewannouncement.isHidden = true
            cell.collection_Images.isHidden = true
            cell.hConstImgView.constant = 0
            cell.hConstCollectionView.constant = 0
        }
        
        cell.layoutIfNeeded()
        
        let containerView = cell.contentView
        containerView.layer.cornerRadius = 2
        containerView.clipsToBounds = true
        
        if let superAdmin = UserDefaults.standard.value(forKey: "isSuperAdmin") as? Bool{
            
            if superAdmin == true{
                cell.btn_Edit.isHidden = false
                cell.btn_Delete.isHidden = false
            }else{
                cell.btn_Edit.isHidden = true
                cell.btn_Delete.isHidden = true
            }
        }
        cell.tag = indexPath.row
        cell.btn_Edit.tag = indexPath.row
        cell.btn_Delete.tag = indexPath.row
        cell.btn_Delete.addTarget(self, action: #selector(btn_Delete(sender:)), for: .touchUpInside)
        cell.btn_Edit.addTarget(self, action: #selector(btn_Edit(sender:)), for: .touchUpInside)
        
        cell.lbl_Title.text = announcementLists[indexPath.row].title ?? ""
        
      
        
        
        cell.lbl_ReadMore.text = announcementLists[indexPath.row].description ?? ""
              
//      //mark:number deduction an d url
        cell.lbl_ReadMore.enabledTextCheckingTypes = NSTextCheckingResult.CheckingType.link.rawValue
        cell.lbl_ReadMore.delegate = self

        self.phoneNumberDetctor(input:  cell.lbl_ReadMore.text as! String)
        self.uRLDetctor(input:  cell.lbl_ReadMore.text as! String)

          let newText =  cell.lbl_ReadMore.text as! NSString

        if self.array_String.count != 0{
            for i in 0...self.array_String.count - 1{
                    let range:NSRange = newText.range(of: array_String[i])
                    cell.lbl_ReadMore.addLink(to: URL(string: array_String[i]), with: range)
                   }
        }
        
        
        //mark:- condition description count for show more show less
        let countLabl : Int = cell.lbl_ReadMore.calculateMaxLines()
        print("readMore:",indexPath.row,countLabl)
        if countLabl == 1{
            cell.buttonMore.setTitle(nil, for: .normal)
            cell.buttonMore.setImage(nil, for: .normal)
            cell.imgContraint_ButtonMoreHeight.constant = 0.0
            cell.buttonMore.isUserInteractionEnabled = false
        }else{
            cell.buttonMore.setTitle("Show more".localized, for: .normal)
            cell.buttonMore.setImage(#imageLiteral(resourceName: "down_black_arrow_iPhone"), for: .normal)
            cell.imgContraint_ButtonMoreHeight.constant = 30 //sakshi
            cell.buttonMore.isUserInteractionEnabled = true
        }
        
        let now = Date()
        let date = NSDate(timeIntervalSince1970: Double(announcementLists[indexPath.row].addedAt ?? 0))
        cell.lbl_Time.text = now.timeAgoDisplay(dateCreated:date as Date,timeStamp:announcementLists[indexPath.row].addedAt ?? 0 )
        
        let timeStamp = announcementLists[indexPath.row].addedAt ?? 0
        let unixTimeStamp: Double = Double(timeStamp) / 1000.0
        let exactDate = NSDate.init(timeIntervalSince1970: unixTimeStamp)
        let dateFormatt = DateFormatter();
        dateFormatt.dateFormat = "dd-MM-yyyy"
        cell.btn_Share.tag = indexPath.row
        self.shareDate = dateFormatt.string(from: exactDate as Date)
        cell.btn_Share.setTitle("Share".localized, for: .normal)
        cell.btn_Share.addTarget(self, action: #selector(btn_shareTextButton(sender:)), for: .touchUpInside)
        
        return cell
    }
  
    
  
   


}

extension MHAnnouncementListsViewController:TTTAttributedLabelDelegate{
    
    func phoneNumberDetctor(input:String){
                 
                 let detector = try! NSDataDetector(types: NSTextCheckingResult.CheckingType.phoneNumber.rawValue)
                           let matches = detector.matches(in: input, options: [], range: NSRange(location: 0, length: input.utf16.count))
                   
                           for match in matches {
                               guard let range = Range(match.range, in: input) else { continue }
                               let url = input[range]
                               print(url)
                            array_String.append(String(url))
                       
                         
                           
                           }
              
                 
             }
             func uRLDetctor(input:String){
                   
               
                   let detector = try! NSDataDetector(types: NSTextCheckingResult.CheckingType.link.rawValue)
                   let matches = detector.matches(in: input, options: [], range: NSRange(location: 0, length: input.utf16.count))

                   for match in matches {
                       guard let range = Range(match.range, in: input) else { continue }
                       let url = input[range]
                       print(url)
                 
                  array_String.append(String(url))
                   
                      
                        
                   }
              
            
               }
    
    
    func phoneAndUrlDetctor(tap_string:String){
        
        //        let tap_string = array_String[view.tag]
        //
        //
                let detector = try! NSDataDetector(types: NSTextCheckingResult.CheckingType.phoneNumber.rawValue)
                          let matches = detector.matches(in: tap_string, options: [], range: NSRange(location: 0, length: tap_string.utf16.count))
        
                          for match in matches {
                              guard let range = Range(match.range, in: tap_string) else { continue }
                              let url = tap_string[range]
                              print(url)
        
                            self.phoneNumber = "true"
        
                          }
        
              let detectorUrl = try! NSDataDetector(types: NSTextCheckingResult.CheckingType.link.rawValue)
                          let matchesurl = detectorUrl.matches(in: tap_string, options: [], range: NSRange(location: 0, length: tap_string.utf16.count))
        
                          for match in matchesurl {
                              guard let range = Range(match.range, in: tap_string) else { continue }
                              let url = tap_string[range]
                              print(url)
        
                          self.phoneNumber = "false"
        
        
        
                          }
        
                if self.phoneNumber == "true"{
        
                    if let url = URL(string:"tel://\(tap_string )"), UIApplication.shared.canOpenURL(url) {
                                        UIApplication.shared.openURL(url)
                                   }
                }else if self.phoneNumber == "false"{
        
                    let urlString = ("\(tap_string )")
                                   var urll = String()
        
        
                                   if urlString.contains("https://") {
                                       urll = ("\(tap_string)")
                                   }else if urlString.contains("http://") {
                                       urll = ("\(tap_string)")
                                   }else{
                                       urll = ("http://\(tap_string)")
                                   }
        
        
                                   guard let url = URL(string: urll) else { return }
                                   UIApplication.shared.openURL(url)
                }
    }
    

        func attributedLabel(_ label: TTTAttributedLabel!, didSelectLinkWith url: URL!) {
         debugPrint("label>", label)
                
                debugPrint("addressComponents>", url)
            
            self.phoneAndUrlDetctor(tap_string: String(describing: url!) )
        }
        
    
}
extension UILabel {
    
    func calculateMaxLines() -> Int {
        let maxSize = CGSize(width: frame.size.width, height: CGFloat(Float.infinity))
        let charSize = font.lineHeight
        let text = (self.text ?? "") as NSString
        let textSize = text.boundingRect(with: maxSize, options: .usesLineFragmentOrigin, attributes: [.font: font], context: nil)
        let linesRoundedUp = Int(ceil(textSize.height/charSize))
        return linesRoundedUp
    }
    

    
}
extension UITextView{
    
        func calculateMaxLines() -> Int {
          let maxSize = CGSize(width: frame.size.width, height: CGFloat(Float.infinity))
            let charSize = (font?.lineHeight)!
          let text = (self.text ?? "") as NSString
          let textSize = text.boundingRect(with: maxSize, options: .usesLineFragmentOrigin, attributes: [.font: font], context: nil)
          let linesRoundedUp = Int(ceil(textSize.height/charSize))
          return linesRoundedUp
      }

}
