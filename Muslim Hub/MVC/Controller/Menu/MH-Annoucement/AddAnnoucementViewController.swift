//
//  AddAnnoucementViewController.swift
//  Muslim Hub
//
//  Created by Rohit on 12/05/19.
//  Copyright © 2019 OnGraph. All rights reserved.
//

import UIKit
import KVNProgress
import AlamofireImage
import SDWebImage
import YangMingShan
import Messages
import MessageUI



class AddAnnoucementViewController: UIViewController, UITextViewDelegate {
    
    @IBOutlet weak var txt_Title            : UITextField!
    @IBOutlet weak var txt_Description      : UITextView!
    @IBOutlet weak var view_Header          : UIView!
    @IBOutlet weak var btn_Save             : UIButton!
    @IBOutlet weak var btn_Cancel           : UIButton!
    @IBOutlet weak var btn_Upload           : UIButton!
    @IBOutlet weak var collectionView_Images: UICollectionView!
    @IBOutlet weak var lbl_NoPhotoFound     : UILabel!
    @IBOutlet weak var lbl_HeaderTitle    : UILabel!
     @IBOutlet weak var lbl_UploadImagesTitle    : UILabel!
     @IBOutlet weak var lbl_6ImagesMax   : UILabel!
    var addNewAnnouncent                    : String!
    var annoucementData                     : AnnouncementData!
  
    var imageData                                               = [Data]()
    var arrayImages                                             = [UIImage]()
    var selectedImageArray                                      = [Data]()
    var orgId                               : Int               = 0
    
    var imageArray: [Any] = []
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        if #available(iOS 13.0, *) {
            statusBarColorChange(color:UIColor.init(hex:  0x05BDB9))
        } else {
            UIApplication.statusBarBackgroundColor = UIColor.init(hex: 0x05BDB9)
        }
        self.cornerRadiusAndShadow()
        self.txt_Description.delegate = self
       
        if annoucementData != nil{
            
            self.txt_Description.textColor = UIColor.lightGray
            self.txt_Title.text = annoucementData.title ?? ""
            self.txt_Description.text = annoucementData.description ?? ""
           
            if annoucementData.images?.count != 0{
                self.imageArray = annoucementData.images!
                print(self.imageData)
                collectionView_Images.isHidden = false
                self.lbl_NoPhotoFound.isHidden = true
                self.btn_Upload.isHidden = true
            }else{
                collectionView_Images.isHidden = true
                self.lbl_NoPhotoFound.isHidden = false
                 self.btn_Upload.isHidden = true
            }
        }else{
            txt_Description.text = "Description".localized
            txt_Title.placeholder = "Title".localized
            txt_Description.textColor = UIColor.lightGray
             collectionView_Images.isHidden = true
             self.lbl_NoPhotoFound.isHidden = true
             self.btn_Upload.isHidden = false
             
        }
        
        self.lbl_HeaderTitle.text =  "Add Announcement ".localized
        self.lbl_UploadImagesTitle.text = "Upload Images".localized
        self.lbl_6ImagesMax.text = "6 Images Maximum".localized
        self.btn_Save.setTitle("Save".localized, for: .normal)
        self.btn_Cancel.setTitle("Cancel".localized, for: .normal)
    }
    
    //MARK:UITextView Delegate and Datasources-------------------------------
    func textViewDidBeginEditing(_ textView: UITextView) {
        
        if textView == self.txt_Description{
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "MHDescViewController") as! MHDescViewController
            vc.delegate = self
            vc.textTitle = "Add Description"
            if txt_Description.text == "Description".localized{
                vc.textDesc = ""
                
            }else{
                vc.textDesc = self.txt_Description.text ?? ""
            }
     vc.modalPresentationStyle = .fullScreen
            present(vc, animated: false, completion: nil)
        }
    }
    
    
    func textViewDidEndEditing(_ textView: UITextView) {
        
        if txt_Description.text == "Description".localized{
            txt_Description.textColor = UIColor.lightGray
                  }else{
                      txt_Description.textColor = UIColor.black
                  }
    }
    
    
    
    func cornerRadiusAndShadow(){
        
        // corner radius
        btn_Save.layer.cornerRadius = 5
        // shadow
        btn_Save.shadowBlackToHeader()
        
        // corner radius
        btn_Cancel.layer.cornerRadius = 5
        // shadow
        btn_Cancel.shadowBlackToHeader()
        view_Header.shadowBlackToHeader()
    }
    
    //MARK:IBACTions------------------------------------------------
    @IBAction func btn_Back(sender:UIButton){
        // self.navigationController?.popViewController(animated: true)
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func btn_Add(sender:UIButton){
        print(imageData)
        print(selectedImageArray)
        if self.addNewAnnouncent == "addNewAnnouncent"{
            self.uploadImageApi(imageData  :imageData)
        }else if self.addNewAnnouncent == "addNewAnnouncentFromOrg"{
            self.addAnnouncementFromOrg(imageData: imageData, orgId: self.orgId)
        }else if self.addNewAnnouncent == "editNewAnnouncentFromOrg"{
            self.editAnnouncementFromOrg()
        }else{
            self.EditAnnouncementApi()
        }
    }
 
    @objc func btn_deltePhot(sender:UIButton){
        let buttonTag = sender.tag
      
        print("self.arrayImages:22",self.imageArray.count)
            self.arrayImages.remove(at: buttonTag)
        self.imageData.remove(at: buttonTag)

        print("self.arrayImages:33",self.imageArray.count)
            self.collectionView_Images.reloadData()
  
    }
    
    @objc func btn_AddPhotos(sender:UIButton){
        
        if self.arrayImages.count == 6{
            
            AKAlertController.alert("You can't select more than 6 images".localized)
        }
        
        self.view.endEditing(true)
        let pickerViewController = YMSPhotoPickerViewController.init()
        pickerViewController.numberOfPhotoToSelect = 6
        let customColor = UIColor.init(red: 3.0/255.0, green: 68.0/255.0, blue: 114.0/255.0, alpha: 1.0)
        pickerViewController.theme.titleLabelTextColor = UIColor.white
        pickerViewController.theme.navigationBarBackgroundColor = customColor
        pickerViewController.theme.tintColor = UIColor.white
        pickerViewController.theme.orderTintColor = customColor
        pickerViewController.theme.orderLabelTextColor = UIColor.white
        pickerViewController.theme.cameraVeilColor = customColor
        pickerViewController.theme.cameraIconColor = UIColor.white
        pickerViewController.theme.statusBarStyle = .default
        self.yms_presentCustomAlbumPhotoView(pickerViewController, delegate: self)
    }
    
    
    @IBAction func photoLibraryTapped(_ sender: Any) {
        self.view.endEditing(true)
        let pickerViewController = YMSPhotoPickerViewController.init()
        pickerViewController.numberOfPhotoToSelect = 6
        let customColor = UIColor.init(red: 3.0/255.0, green: 68.0/255.0, blue: 114.0/255.0, alpha: 1.0)
        pickerViewController.theme.titleLabelTextColor = UIColor.white
        pickerViewController.theme.navigationBarBackgroundColor = customColor
        pickerViewController.theme.tintColor = UIColor.white
        pickerViewController.theme.orderTintColor = customColor
        pickerViewController.theme.orderLabelTextColor = UIColor.white
        pickerViewController.theme.cameraVeilColor = customColor
        pickerViewController.theme.cameraIconColor = UIColor.white
        pickerViewController.theme.statusBarStyle = .default
        self.yms_presentCustomAlbumPhotoView(pickerViewController, delegate: self)
    }
    
    //MARK: Api functions------------------------------------------
    func registerApi(){
        
       
        if txt_Title.text == ""{
            
            AKAlertController.alert("Please enter title".localized)
        }else if txt_Description.text == "Description".localized{
            AKAlertController.alert("Please enter description".localized)
        }else if txt_Description.text == ""{
            AKAlertController.alert("Please enter description".localized)
        }else{
            KVNProgress.show()
            
            Network.shared.addAnnouncement(title:txt_Title.text ?? "", description: txt_Description.text ?? "", image1: "", image2: "",image3: "",image4: "",image5: "",image6: "" ) { (result) in
                KVNProgress.dismiss()
                guard let user = result else {
                    
                    return
                }
                if user.success == true{
                    
                    self.showSingleAlertMessage(message: "", subMsg: user.message ?? "Announcement Added SuccessFully".localized, sender: self, completion:
                        { (success) -> Void in
                            if success == true{
                                self.dismiss(animated: true, completion: nil)
                            }
                    })
                }
            }
        }
     
    }
    func uploadImageApi(imageData : [Data]){
        let para:[String:Any] = [ "title": txt_Title.text ?? "",
                                  "description": txt_Description.text ?? ""]
       
        
        if txt_Title.text == ""{
            
            AKAlertController.alert("Please enter title".localized)
        }else if txt_Description.text == "Description".localized{
            AKAlertController.alert("Please enter description".localized)
        }else if txt_Description.text == ""{
            AKAlertController.alert("Please enter description".localized)
        }else{
         KVNProgress.show()
        Network.shared.requestWithMultiple(imageData: imageData, image1: "", image2: "",image3: "",image4: "",image5: "",image6: "",parameters: para) { (result) in
            KVNProgress.dismiss()
            if result?.success == true{
                self.showSingleAlertMessage(message: "", subMsg: "uploaded successfully", sender: self, completion:
                    { (success) -> Void in
                        if success == true{
                            self.dismiss(animated: true, completion: nil)
                        }
                        
                }
                )
            }else{
                self.showSingleAlertMessage(message: "", subMsg: result?.message ?? "", sender: self, completion:
                    { (success) -> Void in
                        if success == true{
                           
                        }
                        
                })
            }
        }
        }
    }
    
    func addAnnouncementFromOrg(imageData : [Data], orgId: Int){
        print(orgId)
            let para:[String:Any] = [ "title": txt_Title.text ?? "",
                                      "description": txt_Description.text ?? "",
                                      "org" : orgId
            ]
        
        
        if txt_Title.text == ""{
            
            AKAlertController.alert("Please enter title".localized)
        }else if txt_Description.text == "Description".localized{
            AKAlertController.alert("Please enter description".localized)
        }else if txt_Description.text == ""{
            AKAlertController.alert("Please enter description".localized)
        }else{
              KVNProgress.show()
            Network.shared.addAnnouncementFromOrgMultiple(imageData: imageData, image1: "", image2: "",image3: "",image4: "",image5: "",image6: "",parameters: para) { (result) in
                KVNProgress.dismiss()
                if result?.success == true{
                    self.showSingleAlertMessage(message: "", subMsg: "uploaded successfully", sender: self, completion:
                        { (success) -> Void in
                            if success == true{
                                self.presentingViewController?.presentingViewController?.dismiss(animated: true, completion: nil)
                            }
                    })
                }else{
                    self.showSingleAlertMessage(message: "", subMsg: result?.message ?? "", sender: self, completion:
                        { (success) -> Void in
                            if success == true{
                                
                            }
                    })
                }
            }
            
        }
    }
    
    func editAnnouncementFromOrg(){
      
        if txt_Title.text == ""{
            
            AKAlertController.alert("Please enter title".localized)
        }else if txt_Description.text == "Description".localized{
            AKAlertController.alert("Please enter description".localized)
        }else if txt_Description.text == ""{
            AKAlertController.alert("Please enter description".localized)
        }else{
  KVNProgress.show()
        Network.shared.editAnnouncement(
                                        urlPath: "/api/v1/operations/announcement/",
                                        ordId: self.orgId,
                                        id: self.annoucementData.id ?? 0,
                                        title: self.txt_Title.text ?? "",
                                        description: self.txt_Description.text ?? "",
                                        image1: "",
                                        image2: "",
                                        image3: "",
                                        image4: "",
                                        image5: "",
                                        image6: "")
        { (result) in
            KVNProgress.dismiss()
            guard let user = result else {
                return
            }
            if user.success == true{
                self.showSingleAlertMessage(message: "", subMsg:"Announcement updated successfully", sender: self, completion:{ (success) -> Void in
                    if success == true{
                       self.dismiss(animated: true, completion: nil)
                    }
                })
            }
        }
        }
    }
    
    
    func EditAnnouncementApi(){
       
        if txt_Title.text == ""{
            
            AKAlertController.alert("Please enter title".localized)
        }else if txt_Description.text == ""{
            AKAlertController.alert("Please enter description".localized)
        }else{
             KVNProgress.show()
        Network.shared.editAnnouncement(urlPath: "/api/v1/info/announcements/change/", id: self.annoucementData.id ?? 0, title: self.txt_Title.text ?? "", description: self.txt_Description.text ?? "", image1: "", image2: "", image3: "", image4: "", image5: "", image6: ""){ (result) in
            KVNProgress.dismiss()
            guard let user = result else {
                return
            }
            if user.success == true{
                
                
                
                self.showSingleAlertMessage(message: "", subMsg:"Announcement updated successfully", sender: self, completion:
                    { (success) -> Void in
                        if success == true{
                            self.dismiss(animated: true, completion: nil)
                        }
                        
                }
            )
                

            }
        }
    }
    }
}

//MARK:CollectionView-----------------------------------
extension AddAnnoucementViewController: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout{
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 2
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if section == 0{
            if self.addNewAnnouncent != "addNewAnnouncent" && self.addNewAnnouncent != "addNewAnnouncentFromOrg"{
                return 0
            }else{
              return 1
            }
        }else{
            if arrayImages.count > 0{
                if self.arrayImages.count > 6{
                    return 6
                }else{
                    return self.arrayImages.count
                }
            }
            if self.addNewAnnouncent != "addNewAnnouncent" &&  self.addNewAnnouncent != "addNewAnnouncentFromOrg"{
                return annoucementData.images?.count ?? 0
            }
        }
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if indexPath.section == 0{
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Home_AddMoreCollectionViewCell", for: indexPath) as! Home_AddMoreCollectionViewCell
            cell.btn_Addmore.addTarget(self, action: #selector(btn_AddPhotos(sender:)), for: .touchUpInside)
            return cell
        }else{
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "AddimageAnnouncementCollectionViewCell", for: indexPath) as! AddimageAnnouncementCollectionViewCell
            if self.addNewAnnouncent != "addNewAnnouncent" && self.addNewAnnouncent != "addNewAnnouncentFromOrg"{
                if annoucementData.images != nil{
                    cell.btn_Delete.isHidden = true
                    cell.img_View.sd_setImage(with: URL(string:(self.annoucementData.images?[indexPath.item])!),placeholderImage: UIImage.init(named: "default_header_iPhone.png"), completed: nil)
                }else{
                    lbl_NoPhotoFound.isHidden = false
                }
            }else{
                lbl_NoPhotoFound.isHidden = true
                cell.btn_Delete.isHidden = false
                cell.img_View.image = arrayImages[indexPath.row]
                cell.btn_Delete.tag = indexPath.item
                cell.btn_Delete.addTarget(self, action: #selector(btn_deltePhot(sender:)), for: .touchUpInside)
            }
            return cell
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if indexPath.section == 0{
            return  CGSize(width: 100 , height: 100)
        }else{
            return CGSize(width: collectionView.bounds.size.width/2 - 20 , height: 150)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        insetForSectionAt section: Int) -> UIEdgeInsets{
        if section == 0{
            return  UIEdgeInsets( top: 0,  left: 0, bottom: 0,  right: 0)
        }else{
            return  UIEdgeInsets( top: 0,  left: 0, bottom: 0,  right: 0)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 5.0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 5.0
    }
}

extension AddAnnoucementViewController: MHDescDelegate{
    func getDescription(desc: String) {
        self.txt_Description.textColor = UIColor.darkGray
       self.txt_Description.text = desc
       
    }
}

extension AddAnnoucementViewController : YMSPhotoPickerViewControllerDelegate{
    
    // MARK: - YMSPhotoPickerViewControllerDelegate
    
    func photoPickerViewControllerDidReceivePhotoAlbumAccessDenied(_ picker: YMSPhotoPickerViewController!) {
        let alertController = UIAlertController.init(title: "Allow photo album access?".localized, message: "Need your permission to access photo albumbs".localized, preferredStyle: .alert)
        let dismissAction = UIAlertAction.init(title: "Cancel".localized, style: .cancel, handler: nil)
        let settingsAction = UIAlertAction.init(title: "Settings".localized, style: .default) { (action) in
            UIApplication.shared.openURL(URL.init(string: UIApplication.openSettingsURLString)!)
        }
        alertController.addAction(dismissAction)
        alertController.addAction(settingsAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
    func photoPickerViewControllerDidReceiveCameraAccessDenied(_ picker: YMSPhotoPickerViewController!) {
        let alertController = UIAlertController.init(title: "Allow camera album access?".localized, message: "Need your permission to take a photo".localized, preferredStyle: .alert)
        let dismissAction = UIAlertAction.init(title: "Cancel".localized, style: .cancel, handler: nil)
        let settingsAction = UIAlertAction.init(title: "Settings".localized, style: .default) { (action) in
            UIApplication.shared.openURL(URL.init(string: UIApplication.openSettingsURLString)!)
        }
        alertController.addAction(dismissAction)
        alertController.addAction(settingsAction)
        picker.present(alertController, animated: true, completion: nil)
    }
    func photoPickerViewController(_ picker: YMSPhotoPickerViewController!, didFinishPicking image: UIImage!) {
        picker.dismiss(animated: true) {
        }
    }
    
    func photoPickerViewController(_ picker: YMSPhotoPickerViewController!, didFinishPickingImages photoAssets: [PHAsset]!) {
        
        picker.dismiss(animated: true) {
            let imageManager = PHImageManager.init()
            let options = PHImageRequestOptions.init()
            options.deliveryMode = .highQualityFormat
            options.resizeMode = .none
            
            options.isSynchronous = true

            
            if photoAssets.count + self.arrayImages.count > 6{
                
                AKAlertController.alert("You can't select more than 6 images".localized)
            }
            
            else{
            for asset: PHAsset in photoAssets
            {
                let targetSize = CGSize(width: UIScreen.main.bounds.size.width, height: UIScreen.main.bounds.size.height)
              
                imageManager.requestImage(for: asset, targetSize: targetSize, contentMode: .default, options: options, resultHandler: { (image, info) in
                    
                  
                        self.arrayImages.append(image!)
                        self.collectionView_Images.isHidden = false
                        
                        
                        self.imageData.append(image!.jpegData(compressionQuality: 0.7)!)
                
             
              // self.imageData.append(image!.pngData()!)
             
                })
            }
        }
            self.collectionView_Images.reloadData()
        }
    }
    
    
}

