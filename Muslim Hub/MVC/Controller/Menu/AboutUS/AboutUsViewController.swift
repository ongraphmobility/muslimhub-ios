    //
    //  AboutUsViewController.swift
    //  Muslim Hub
    //
    //  Created by Sakshi Singh on 24/04/19.
    //  Copyright © 2019 OnGraph. All rights reserved.
    //
    
    import UIKit
    import KVNProgress
    
    
    class AboutUsViewController: UIViewController,UITextViewDelegate {
        
        @IBOutlet weak var tableiew_AboutUS:UITableView!
        @IBOutlet weak var btn_Save:UIButton!
     
        @IBOutlet weak var btn_PrivacyStatment:UIButton!
        
        var aboutUs:AboutData!
        var aboutUsText:String!
        var aboutUsId:Int!
        
        override func viewDidLoad() {
            super.viewDidLoad()
            
            // Do any additional setup after loading the view.
            if #available(iOS 13.0, *) {
                statusBarColorChange(color:UIColor.init(hex:  0x05BDB9))
            } else {
                UIApplication.statusBarBackgroundColor = UIColor.init(hex: 0x05BDB9)
            }
            
            btn_Save.layer.cornerRadius = 10
            // shadow
            btn_Save.layer.shadowColor = UIColor.darkGray.cgColor
            btn_Save.layer.shadowOffset = CGSize(width: 0, height: 3)
            btn_Save.layer.shadowOpacity = 0.7
            btn_Save.layer.shadowRadius = 4.0
            
         
            self.btn_Save.setTitle("Save".localized, for: .normal)
            
//            let yourAttributes: [NSAttributedString.Key: Any] = [
//            .font: UIFont.systemFont(ofSize: 14),
//            .foregroundColor: UIColor.blue,
//            .underlineStyle: NSUnderlineStyle.single.rawValue]
//               //.double.rawValue, .thick.rawValue
//            let attributeString = NSMutableAttributedString(string:"Privacy statement".localized,
//                                                            attributes: yourAttributes)
//            self.btn_PrivacyStatment.setAttributedTitle(attributeString, for: .normal)
            
        }
        func adjustUITextViewHeight(textview : UITextView){
            textview.translatesAutoresizingMaskIntoConstraints = true
            textview.sizeToFit()
            textview.isScrollEnabled = false
        }
        
        override func viewWillAppear(_ animated: Bool) {
            super.viewWillAppear(true)
            
            self.aboutUsApi()
            
        }
        //MARK:IBACtion--------------------------------------------------------
        @IBAction func btn_Back(sendr:UIButton){
            
            
            if let controller = self.storyboard?.instantiateViewController(withIdentifier: "TabVC") {
                self.slideMenuController()?.changeMainViewController(controller, close: true)
            }
            
            
        }
        @objc func btn_aboutUsEdit(sender:UIButton){
            let index = IndexPath(row: 0, section: 0)
            let cell: AboutUSTableViewCell = self.tableiew_AboutUS.cellForRow(at: index) as! AboutUSTableViewCell
            if sender.isSelected{
                sender.isSelected = false
                cell.txt_View.isScrollEnabled = false
                btn_Save.isHidden = true
                cell.txt_View.isEditable = false
            }else{
                sender.isSelected = true
                btn_Save.isHidden = false
                cell.txt_View.isEditable = true
                cell.txt_View.isScrollEnabled = true
                cell.tintColor = UIColor.blue
            }
            
        }
        
        @IBAction func btn_aboutUsEditApi(sender:UIButton){
            let index = IndexPath(row: 0, section: 0)
            let cell: AboutUSTableViewCell = self.tableiew_AboutUS.cellForRow(at: index) as! AboutUSTableViewCell
            
            
            KVNProgress.show()
            
            Network.shared.aboutUSEdit(id: aboutUsId,text: cell.txt_View.text ?? ""){ (result) in
                KVNProgress.dismiss()
                guard result != nil else {
                    
                    return
                }
                
                if result?.success == true{
                    self.showSingleAlertMessage(message: "", subMsg: "Updated Successfully".localized , sender: self, completion:
                        { (success) -> Void in
                            //  self.aboutUsApi()
                    })
                }
                
                
            }
            
            
        }
        
        func aboutUsApi(){
            
            KVNProgress.show()
            
            Network.shared.aboutUs{ (result) in
                KVNProgress.dismiss()
                guard let user = result else {
                    
                    return
                }
                self.aboutUs = user
                self.aboutUsText = self.aboutUs.text ?? ""
                self.aboutUsId = self.aboutUs.id ?? 0
                self.tableiew_AboutUS.reloadData()
                
            }
        }
    }
    
    //MARK:TableView DataSource Delegate--------------------------------------------------
    extension AboutUsViewController:UITableViewDelegate,UITableViewDataSource{
        func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
            return 1
        }
        
        func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "AboutUSTableViewCell", for: indexPath) as! AboutUSTableViewCell
            cell.lbl_Header.text = "About Us".localized
            cell.txt_View.isEditable = false
            let appVersion = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String
            cell.lbl_AppVersion.text = ("App Version  ".localized + (appVersion ?? "")) + " "
            if self.aboutUsText != nil{
                
                cell.txt_View.text =  self.aboutUsText
            }
            if let loginToken = UserDefaults.standard.value(forKey: "Login_Token") as? String{
                print(loginToken)
                if let superAdmin = UserDefaults.standard.value(forKey: "isSuperAdmin") as? Bool{
                    
                    if superAdmin == true{
                        
                        cell.btn_Edit.isHidden = false
                        self.aboutUsText = cell.txt_View.text
                        btn_Save.isHidden = true
                        
                    }else{
                        cell.btn_Edit.isHidden = true
                        btn_Save.isHidden = true
                        
                    }
                }
            }else{
                cell.btn_Edit.isHidden = true
                btn_Save.isHidden = true
            }
            cell.btn_EditPen.addTarget(self, action: #selector(btn_aboutUsEdit(sender:)), for: .touchUpInside)
           
            cell.btn_PrivacyStatement.setTitle("Privacy statement".localized, for: .normal)
            return cell
            
        }
        
        
    }
