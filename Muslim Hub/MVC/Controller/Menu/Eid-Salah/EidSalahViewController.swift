//
//  EidSalahViewController.swift
//  Muslim Hub
//
//  Created by Sakshi Singh on 10/05/19.
//  Copyright © 2019 OnGraph. All rights reserved.
//

import UIKit
import CountryPickerViewSwift
import GooglePlaces
import GooglePlacePicker

enum EidSalahDropDown{
    case held
    case attend
    case parking
    case language
}

enum EidSalahType{
    case add
    case edit
}

class EidSalahViewController: UIViewController,UITextFieldDelegate,UITextViewDelegate {
    
     @IBOutlet weak var lbl_Header            : UILabel!
    @IBOutlet weak var tableView_EidSalah            : UITableView!
    @IBOutlet weak var view_Header                   : UIView!
    @IBOutlet weak var view_tableViewUpperRadius     : UIView!
    @IBOutlet weak var view_tableViewLowerRadius     : UIView!
    @IBOutlet weak var btn_Save                      : UIButton!
    @IBOutlet weak var btn_Cancel                    : UIButton!
    @IBOutlet weak var txt_salah_time                : UITextField!
    @IBOutlet weak var txt_thakbeel_time             : UITextField!
    var dropDownType                                 : EidSalahDropDown!
    var type                                         : EidSalahType!
    var countryCode                                  : String!          = "+61"
    var dataDict                                     : [String: Any]    = [:]
    var lat                                                             = Double()
    var long                                                            = Double()
    var tag_forAddress                               : Int!
    var placesClient                                 : GMSPlacesClient!
    let arra_EisSalah                                                   = ["Org Name",
                                                                           "Org Abbreviation",
                                                                           "City/Subburb",
                                                                           "Location",
                                                                           "Date",
                                                                           "Salah time",
                                                                           "Held",
                                                                           "Who can attend",
                                                                           "Parking",
                                                                           "Khutbah Language",
                                                                           "Phone no",
                                                                           "info"]
    

    override func viewDidLoad() {
        super.viewDidLoad()
  statusBarColorChange(color:UIColor.init(hex: 0x05BDB9))
        // Do any additional setup after loading the view.
        self.cornerRadiuss()
        self.configureView()
        if type == EidSalahType.add{
            self.dataDict = ["orgName"  : "",
                             "orgAbbr"  : "",
                             "city"     : "",
                             "location" : "",
                             "date"     : "",
                             "salahTime": "",
                             "thakbeerTime": "",
                             "held"     : "",
                             "attend"   : "",
                             "parking"  : "",
                             "language" : "",
                             "phone"    : "",
                             "info"     : "info".localized
            ]
        }else{
            self.long = self.dataDict["longitude"] as! Double
            self.lat = self.dataDict["latitude"] as! Double
        }
        
        self.lbl_Header.text = "Eid - Salah".localized
        self.btn_Cancel.setTitle("Cancel".localized, for: .normal)
        self.btn_Save.setTitle("Save".localized, for: .normal)
    }
    
    func configureView(){
        let center = CLLocationCoordinate2D(latitude: -33.865143, longitude: 151.2099)
        let northEast = CLLocationCoordinate2D(latitude: center.latitude + 0.001,
                                               longitude: center.longitude + 0.001)
        let southWest = CLLocationCoordinate2D(latitude: center.latitude - 0.001,
                                               longitude: center.longitude - 0.001)
        let viewport = GMSCoordinateBounds(coordinate: northEast, coordinate: southWest)
        let config = GMSPlacePickerConfig(viewport: viewport)
    }
 

    func getDropDownData(type: EidSalahDropDown) -> [String]{
        if type == .held{
            return ["Indoor", "Outdoor"]
        }else if type == .attend{
            return ["Men Only", "Men, Women and Family"]
        }else if type == .parking{
            return ["Limited", "Plenty", "No Parking"]
        }else if type == .language{
            return ["English", "Urdu", "Arabic", "Malay"]
        }else{
            return []
        }
    }
    
    func getIndexOfDropDown(type: EidSalahDropDown, value: String? = "", index: Int? = nil) -> (Int, String){
        let array = self.getDropDownData(type: type)
        if value != ""{
            for i in 0..<array.count{
                if array[i] == value{
                    return (i + 1, array[i])
                }
            }
            return (0, value!)
        }else if index != nil{
            return (index! + 1, array[index!])
        }else{
            return (0, "")
        }
    }
    
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        guard let indexPath = textView.tableViewIndexPath(self.tableView_EidSalah) else { return }
        
        switch (indexPath.section, 11) {
            
        case (0,11):
            
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "MHDescViewController") as! MHDescViewController
            vc.delegate = self
            vc.textDesc = textView.text!
            textView.keyboardType = .default
                 vc.modalPresentationStyle = .fullScreen
            present(vc, animated: false, completion: nil)
        default:
            break
            
        }
        
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
              
              textView.textColor = UIColor.black
            
          }
    
    //MARK: textField Delegate and DataSource---------------------
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        guard let indexPath = textField.tableViewIndexPath(self.tableView_EidSalah) else { return }
        
        switch (indexPath.section, indexPath.row) {
    
        case (0,0):
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "SearchViewController") as! SearchViewController
            vc.delegate = self
            present(vc, animated: false, completion: nil)
        case (0,3):
            let autocompleteController = GMSAutocompleteViewController()
            autocompleteController.delegate = self
            let filter = GMSAutocompleteFilter()
            filter.type = .noFilter  //suitable filter type
         //   filter.country = "AUS"  //appropriate country code
            autocompleteController.autocompleteFilter = filter
            self.tag_forAddress = 3
            present(autocompleteController, animated: true, completion: nil)

        case (0, 4):
            AKDatePicker.openPicker(in: textField, currentDate: Date(), pickerMode: .date) { (value) in
                textField.text = self.convertToString(date: value, dateformat: "dd-MM-yyyy")
                self.dataDict["date"] = textField.text
            }
        case (0,5):
           var dateSelected = self.dataDict["date"] as? String
           
           if AppDelegate.instance.currentDate == dateSelected{
            
            if textField == self.txt_salah_time{
                AKTimePicker.openPicker(in: textField, currentDate: Date(),minimumDate: Date() ,pickerMode: .time) { (value) in
                    textField.text = self.convertToString(date: value, dateformat: "hh:mm a")
                    self.dataDict["salahTime"] = self.convertToString(date: value, dateformat: "HH:mm")
                }
            }else if textField == self.txt_thakbeel_time{
                AKTimePicker.openPicker(in: textField, currentDate: Date(), minimumDate: Date(), pickerMode: .time) { (value) in
                    textField.text = self.convertToString(date: value, dateformat: "hh:mm a")
                    self.dataDict["thakbeerTime"] = self.convertToString(date: value, dateformat: "HH:mm")
                }
            }
           }else{
            
            
            if textField == self.txt_salah_time{
                AKTimePicker.openPicker(in: textField, currentDate: Date(),pickerMode: .time) { (value) in
                    textField.text = self.convertToString(date: value, dateformat: "hh:mm a")
                    self.dataDict["salahTime"] = self.convertToString(date: value, dateformat: "HH:mm")
                }
            }else if textField == self.txt_thakbeel_time{
                AKTimePicker.openPicker(in: textField, currentDate: Date(),pickerMode: .time) { (value) in
                    textField.text = self.convertToString(date: value, dateformat: "hh:mm a")
                    self.dataDict["thakbeerTime"] = self.convertToString(date: value, dateformat: "HH:mm")
                }
            }
            }
    
        
    
        case (0,6):
            AKMultiPicker().openPickerIn(textField, firstComponentArray: self.getDropDownData(type: .held)) { (value, _, index1, _) in
                textField.text = value
                self.dataDict["held"] = value
            }

        case (0,7):
            AKMultiPicker().openPickerIn(textField, firstComponentArray: self.getDropDownData(type: .attend)) { (value, _, index1, _) in
                textField.text = value
                self.dataDict["attend"] = value
            }
            
        case (0,8):
            AKMultiPicker().openPickerIn(textField, firstComponentArray: self.getDropDownData(type: .parking)) { (value, _, index1, _) in
                textField.text = value
                self.dataDict["parking"] = value
            }
            
        case (0,9):
            AKMultiPicker().openPickerIn(textField, firstComponentArray: self.getDropDownData(type: .language)) { (value, _, index1, _) in
                textField.text = value
                self.dataDict["language"] = value
            }
        case (0,10):
            textField.keyboardType = .phonePad
        case (0,11):
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "MHDescViewController") as! MHDescViewController
            vc.delegate = self
            vc.descStr = textField.text!
                 vc.modalPresentationStyle = .fullScreen
            present(vc, animated: false, completion: nil)
        default:
            break
        }
            
    }

    func textFieldDidEndEditing(_ textField: UITextField) {
        guard let indexPath = textField.tableViewIndexPath(self.tableView_EidSalah) else { return }
        
        switch (indexPath.section, indexPath.row) {
            
        case (0,1):
            self.dataDict["orgAbbr"] = textField.text!
        case (0,2):
            self.dataDict["city"] = textField.text!
        case (0,10):
            self.dataDict["phone"] = textField.text!
        default:
            break
        }
    }
    
    func validate() -> Bool{
        var error : Bool = false
        if(self.dataDict["orgName"] as! String == ""){
            error = false
            AKAlertController.alert("", message: "Please select Organization Name")
        }else if(self.dataDict["orgAbbr"] as! String == ""){
            error = false
            AKAlertController.alert("", message: "Please enter your Organization Abbreviation")
        }else if(self.dataDict["city"] as! String == ""){
            error = false
            AKAlertController.alert("", message: "Please enter your city")
        }else if(self.dataDict["location"] as! String == ""){
            error = false
            AKAlertController.alert("", message: "Please enter your location")
        }else if(self.dataDict["date"] as! String == ""){
            error = false
              AKAlertController.alert("", message: "Please select date")
        }else if(self.dataDict["salahTime"] as! String == ""){
            error = false
              AKAlertController.alert("", message: "Please select salah time")
        }else if(self.dataDict["thakbeerTime"] as! String == ""){
            error = false
            AKAlertController.alert("", message: "Please select thakbir time")
        }else if(self.dataDict["held"] as! String == ""){
            error = false
            AKAlertController.alert("", message: "Please select held")
        }else if(self.dataDict["attend"] as! String == ""){
            error = false
            AKAlertController.alert("", message: "Please select attendees type")
        }else if(self.dataDict["parking"] as! String == ""){
            error = false
            AKAlertController.alert("", message: "Please select parking type")
        }else if(self.dataDict["language"] as! String == ""){
            error = false
            AKAlertController.alert("", message: "Please select language")
        }else if(self.dataDict["phone"] as! String != ""){
            var phone =  self.dataDict["phone"] as! String
          
            while (phone.hasPrefix("0")) {
                phone.remove(at: (phone.startIndex))
                
                self.dataDict.updateValue(phone, forKey: "phone")
            }
            if phone.count == 0{
                error = false
                AKAlertController.alert("", message: "Please enter mobile number")
            }else if phone.count > 10{
                error = false
                AKAlertController.alert("", message: "Please enter valid mobile number")
            }
                else{
                error = true
            }
        }
        else{
            error = true
        }
        return error
    }
    
    //MARK: IBAction------------------------------------------
    
    @objc func btn_CountryCode(Sender:UIButton){
        let countryView = CountrySelectView.shared
        countryView.barTintColor = .red
        countryView.displayLanguage = .english
        countryView.show()
        countryView.selectedCountryCallBack = { (countryDic) -> Void in
            self.countryCode = "+\(countryDic["code"] as! NSNumber)"
            self.tableView_EidSalah.reloadRows(at: [IndexPath(row: 10, section: 0)], with: UITableView.RowAnimation.none)
        }
    }
    
    @IBAction func btn_Back(sender:UIButton){
       
          self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func btnSaveTapped(_ sender: Any) {
        if self.validate(){
            let (heldIndex, _) = self.getIndexOfDropDown(type: .held, value: self.dataDict["held"] as? String)
            let (attendIndex, _) = self.getIndexOfDropDown(type: .attend, value: self.dataDict["attend"] as? String)
            let (parkingIndex, _) = self.getIndexOfDropDown(type: .parking, value: self.dataDict["parking"] as? String)
            let (languageIndex, _) = self.getIndexOfDropDown(type: .language, value: self.dataDict["language"] as? String)
            var phone: String = ""
            if self.dataDict["phone"] as! String != ""{
                phone = "\(String(describing: self.countryCode!))" + "-" + "\(self.dataDict["phone"] as! String)"
            }else{
                phone = ""
            }

            if self.type == EidSalahType.add{
                Network.shared.addEidSalah(latitude: self.lat,
                                           longitude: self.long,
                                           address: self.dataDict["location"] as! String,
                                           city: self.dataDict["city"] as! String,
                                           date: self.dataDict["date"] as! String,
                                           salah_time: self.dataDict["salahTime"] as! String,
                                           held: heldIndex,
                                           attendees: attendIndex,
                                           takbir_time: self.dataDict["thakbeerTime"] as! String,
                                           parking: parkingIndex,
                                           khutbah_lang: languageIndex,
                                           phone_num: phone,
                                           info: self.dataDict["info"] as! String,
                                           org: self.dataDict["orgNameId"] as! Int) { (response) in
                                            print(response!)
                                            if response!.success{
                                                AKAlertController.alert("", message: response!.message, buttons: ["Ok"], tapBlock: { (UIAlertController, UIAlertAction, Int) in
                                                     self.dismiss(animated: true, completion: nil)
                                                })
                                                
                                            }else{
                                                AKAlertController.alert("", message: response!.message)
                                            }
                        
                }
            }else{
                
                //self.lat = self.dataDict["latitude"] as! Double
                //self.long = self.dataDict["longitude"] as! Double
                
                Network.shared.editEidSalah(latitude: self.lat,
                                            longitude: self.long,
                                            address: self.dataDict["location"] as! String,
                                            city: self.dataDict["city"] as! String,
                                            date: self.dataDict["date"] as! String,
                                            salah_time: self.dataDict["salahTime"] as! String,
                                            held: heldIndex,
                                            attendees: attendIndex,
                                            takbir_time: self.dataDict["thakbeerTime"] as! String,
                                            parking: parkingIndex,
                                            khutbah_lang: languageIndex,
                                            phone_num: phone,
                                            info: self.dataDict["info"] as! String,
                                            org: self.dataDict["orgNameId"] as! Int,
                                            added_by: self.dataDict["added_by"] as! String) { (response) in
                                                if response!.success{
                                                    AKAlertController.alert("", message: response!.message, buttons: ["Ok"], tapBlock: { (UIAlertController, UIAlertAction, Int) in
                                                        self.dismiss(animated: true, completion: nil)
                                                       
                                                    })
                                                    
                                                }else{
                                                    AKAlertController.alert("", message: response!.message)
                                                }
                }
            }
            
        }else{
            
        }
    }
    
    //MARK:Functionc And Methods--------------------
    
    func cornerRadiuss(){
      view_Header.shadowBlackToHeader()
        
        view_tableViewUpperRadius.clipsToBounds = true
        view_tableViewUpperRadius.layer.cornerRadius = 8
        view_tableViewUpperRadius.layer.maskedCorners =
            [.layerMaxXMinYCorner, .layerMinXMinYCorner]
  
        view_tableViewLowerRadius.layer.cornerRadius = 8
        view_tableViewLowerRadius.layer.maskedCorners =  [.layerMaxXMaxYCorner, .layerMinXMaxYCorner]
        
        view_tableViewLowerRadius.layer.cornerRadius = 10
        view_tableViewLowerRadius.layer.shadowColor = UIColor.black.cgColor
        view_tableViewLowerRadius.layer.shadowOffset = CGSize(width: 0, height: 2)
        view_tableViewLowerRadius.layer.shadowOpacity = 0.3
        view_tableViewLowerRadius.layer.shadowRadius = 3.0
        
        // corner radius
        btn_Save.layer.cornerRadius = 5
        btn_Cancel.shadowBlackToHeader()
        
        // corner radius
        btn_Cancel.layer.cornerRadius = 5
        btn_Save.shadowBlackToHeader()
    }

}
extension EidSalahViewController:UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return arra_EisSalah.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "EidSalahaTableViewCell", for: indexPath) as! EidSalahaTableViewCell
        cell.txt_Title.placeholder = arra_EisSalah[indexPath.row].localized
        cell.view_2.isHidden = true
        cell.view_3.isHidden = true
        cell.view_4.isHidden = true
        cell.btn_side.isHidden = true
        cell.txt_Title.delegate = self
        cell.txt_Title.isUserInteractionEnabled = true
        cell.txt_Title.tag = indexPath.row
        
      
        if indexPath.row == 0{
            if self.type == EidSalahType.add{
                cell.txt_Title.isUserInteractionEnabled = true
            }else{
                cell.txt_Title.isUserInteractionEnabled = false
            }
            cell.txt_Title.text = self.dataDict["orgName"] as? String
        }else if indexPath.row == 1{
            cell.txt_Title.isUserInteractionEnabled = false
            cell.txt_Title.text = self.dataDict["orgAbbr"] as? String
        }else if indexPath.row == 2{
            cell.txt_Title.text = self.dataDict["city"] as? String
        }else if indexPath.row == 3{
            cell.txt_Title.text = self.dataDict["location"] as? String
        }else if indexPath.row == 4{
            cell.btn_side.isHidden = false
            cell.btn_side.setImage(UIImage(named: ""), for: .normal)
            cell.btn_side.setBackgroundImage(#imageLiteral(resourceName: "calendar_icon_iPhone"), for: .normal)
            //cell.btn_side.setImage(#imageLiteral(resourceName: "calendar_icon_iPhone"), for: .normal)
            cell.txt_Title.text = self.dataDict["date"] as? String
        }else if indexPath.row == 5 {
            cell.btn_side.isHidden = false
            cell.txt_Title2.delegate = self
            self.txt_salah_time = cell.txt_Title
            self.txt_thakbeel_time = cell.txt_Title2
            cell.view_2.isHidden = false
            cell.txt_Title2.placeholder = "Thakbeer Time".localized
            cell.btn_side.setImage(UIImage(named: ""), for: .normal)
            cell.btn_side.setBackgroundImage(UIImage(named: "event_Clock"), for: .normal)
            cell.btn_side2.setImage(UIImage(named: ""), for: .normal)
            cell.btn_side2.setBackgroundImage(UIImage(named: "event_Clock"), for: .normal)
            cell.txt_Title.text = self.dataDict["salahTime"] as? String
            cell.txt_Title2.text = self.dataDict["thakbeerTime"] as? String
          
            
        }else if indexPath.row == 6{
            cell.btn_side.isHidden = false
            cell.txt_Title.text = self.dataDict["held"] as? String
        }else if indexPath.row == 7{
            cell.btn_side.isHidden = false
            cell.txt_Title.text = self.dataDict["attend"] as? String
        }else if indexPath.row == 8{
            cell.btn_side.isHidden = false
            cell.txt_Title.text = self.dataDict["parking"] as? String
        }else if indexPath.row == 9{
            cell.btn_side.isHidden = false
            cell.txt_Title.text = self.dataDict["language"] as? String
        }else if indexPath.row == 10{
            cell.txt_Phone.placeholder = arra_EisSalah[indexPath.row].localized
            cell.txt_Phone.text = self.dataDict["phone"] as? String
            cell.txt_Phone.delegate = self
            cell.btnCountryCode.setTitle("\(String(describing: self.countryCode ?? "+61"))", for: .normal)
            cell.view_1.isHidden = true
            cell.view_2.isHidden = true
            cell.view_3.isHidden = false
            cell.view_4.isHidden = true
            cell.btnCountryCode.addTarget(self, action: #selector(btn_CountryCode(Sender:)), for: .touchUpInside)
        }else{
            cell.view_1.isHidden = true
            cell.view_2.isHidden = true
            cell.view_3.isHidden = true
            cell.view_4.isHidden = false
            cell.txtView_Description.text = self.dataDict["info"] as? String

            if cell.txtView_Description.text == "info".localized{
                cell.txtView_Description.textColor = UIColor.lightGray
                
            }else{
                cell.txtView_Description.textColor = UIColor.black
                
                
            }

        }

        return cell
    }
    
    
    
}

extension EidSalahViewController: SearchDelegate{
    func getSelected(string: String, id: Int, abbreviation: String) {
        self.dataDict["orgName"] = string
        self.dataDict["orgNameId"] = id
        self.dataDict["orgAbbr"] = abbreviation
        self.tableView_EidSalah.reloadRows(at: [IndexPath(row: 1, section: 0)], with: UITableView.RowAnimation.none)
        self.tableView_EidSalah.reloadRows(at: [IndexPath(row: 0, section: 0)], with: UITableView.RowAnimation.none)
    }

}

extension EidSalahViewController: MHDescDelegate{
    func getDescription(desc: String) {
        self.dataDict["info"] = desc
        self.tableView_EidSalah.reloadRows(at: [IndexPath(row: 11, section: 0)], with: UITableView.RowAnimation.none)
    }
}

//extension EidSalahViewController: GMSPlacePickerViewControllerDelegate{
//    func placePicker(_ viewController: GMSPlacePickerViewController, didPick place: GMSPlace) {
//        let location:CLLocationCoordinate2D = place.coordinate
//        let latitude_str = String(format: "%.6f", location.latitude)
//        let longituted_str = String(format: "%.6f", location.longitude)
//        self.lat = Double(latitude_str) ?? 123.123
//        self.long = Double(longituted_str) ?? 123.123
//        print("Place address \(String(describing: place.formattedAddress))")
//        self.dismiss(animated: true) {
//            self.dataDict["location"] = "\(String(describing: place.name ?? ""))"
//            self.tableView_EidSalah.reloadRows(at: [IndexPath(row: self.tag_forAddress, section: 0)], with: UITableView.RowAnimation.none)
//        }
//    }
//    
//    func placePickerDidCancel(_ viewController: GMSPlacePickerViewController) {
//        // Dismiss the place picker, as it cannot dismiss itself.
//        viewController.dismiss(animated: true, completion: nil)
//
//        print("No place selected")
//    }
//}
extension EidSalahViewController: GMSAutocompleteViewControllerDelegate {
    
    // Handle the user's selection.
    func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {
        
       // print("Place name: \(place.name)")
        let index = IndexPath(row: self.tag_forAddress, section: 0)
        let _: EidSalahaTableViewCell = self.tableView_EidSalah.cellForRow(at: index) as! EidSalahaTableViewCell
        let location:CLLocationCoordinate2D = place.coordinate
        let latitude_str = String(format: "%.6f", location.latitude)
        let longituted_str = String(format: "%.6f", location.longitude)
        self.lat = Double(latitude_str) ?? 123.123
        self.long = Double(longituted_str) ?? 123.123
        print("Place address \(String(describing: place.formattedAddress))")
        self.dismiss(animated: true) {
            self.dataDict["location"] = "\(String(describing: place.formattedAddress ?? ""))"
            self.tableView_EidSalah.reloadRows(at: [IndexPath(row: self.tag_forAddress, section: 0)], with: UITableView.RowAnimation.none)
        }
    }
    
    func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
        
        // TODO: handle the error.
        print("Error: ", error.localizedDescription)
    }
    
    // User canceled the operation.
    func wasCancelled(_ viewController: GMSAutocompleteViewController) {
        //  addressTextView.resignFirstResponder()
        dismiss(animated: true, completion: nil)
    }
    
    // Turn the network activity indicator on and off again.
    func didRequestAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
    }
    
    func didUpdateAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
    }
    
}
