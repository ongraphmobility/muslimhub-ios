//
//  DataTableViewCell.swift
//  Muslim Hub
//
//  Created by Ongraph on 5/13/19.
//  Copyright © 2019 OnGraph. All rights reserved.
//

import UIKit

class DataTableViewCell: UITableViewCell {

    @IBOutlet var lblTitle                 : UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
