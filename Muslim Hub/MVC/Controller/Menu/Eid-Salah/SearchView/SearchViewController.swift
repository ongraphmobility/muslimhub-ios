//
//  SearchViewController.swift
//  Muslim Hub
//
//  Created by Ongraph on 5/13/19.
//  Copyright © 2019 OnGraph. All rights reserved.
//

import UIKit

protocol SearchDelegate {
    func getSelected(string: String, id: Int, abbreviation: String)
}

class SearchViewController: UIViewController {

    @IBOutlet weak var lblNoRecordFound: UILabel!
    @IBOutlet var tableView                 : UITableView!
    @IBOutlet var searchBarTextField        : UITextField!
    @IBOutlet var btnClose                  : UIButton!
    var searchArray                         : [OrganizaionList]!     = []
    var txtAfterUpdate                                               = ""
    var delegate                            : SearchDelegate!
    
    override func viewDidLoad() {
        super.viewDidLoad()
      
        
        self.tableView.showsHorizontalScrollIndicator = false
        self.tableView.showsVerticalScrollIndicator = false
        self.tableView.register(UINib(nibName: "DataTableViewCell", bundle: nil), forCellReuseIdentifier: "DataTableViewCell")
        // Do any additional setup after loading the view.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    @IBAction func btnCloseTapped(_ sender: Any) {
        self.dismiss(animated: false, completion: nil)
    }
    
}

extension SearchViewController: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.searchArray.count > 0{
            return self.searchArray.count
        }else{
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "DataTableViewCell", for: indexPath) as! DataTableViewCell
        if self.searchArray.count > 0{
            let model = self.searchArray[indexPath.row]
            cell.lblTitle.text = model.name
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.searchBarTextField.text = ""
        self.view.endEditing(true)
        self.dismiss(animated: false) {
            self.delegate.getSelected(string: self.searchArray[indexPath.row].name , id: self.searchArray[indexPath.row].id, abbreviation: self.searchArray[indexPath.row].abbreviation)
        }
    }
}


extension SearchViewController: UITextFieldDelegate{
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        self.searchArray = []
        if let text = textField.text as NSString? {
            self.txtAfterUpdate = text.replacingCharacters(in: range, with: string)
//            if(self.txtAfterUpdate.count == 0){
//     AKAlertController.alert("Please Enter Text")
//            }else{
            
                    Network.shared.searchOrganization(searchString: trimString(str:self.txtAfterUpdate)) { (response) in
                       
                        self.searchArray = response!.data
                        self.tableView.reloadData()
                }
              
           // }
        }
        return true
    }
    
    
    func trimString(str: String) -> String{
        return str.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
    }
}
