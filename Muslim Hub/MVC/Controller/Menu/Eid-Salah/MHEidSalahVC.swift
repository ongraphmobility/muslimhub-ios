//
//  MHEidSalahVC.swift
//  Muslim Hub
//
//  Created by Ongraph2018 on 10/05/19.
//  Copyright © 2019 OnGraph. All rights reserved.
//

import UIKit
import DropDown
import KVNProgress
import GoogleMaps

class MHEidSalahVC: UIViewController,UISearchBarDelegate {

    @IBOutlet weak var lbl_Title         : UILabel!
    @IBOutlet weak var btn_sort         : UIButton!
    @IBOutlet weak var search_bar       : UISearchBar!
    @IBOutlet weak var table_view       : UITableView!
    var dataModel                       : [EidSalahListData]!
  
    var arr_Selected                    : [Bool]                    = []
    
    @IBOutlet weak var btn_Add         : UIButton!
    
    var locationManager = CLLocationManager()
    var currentLocation = CLLocation()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        //        self.table_view.rowHeight = UITableView.automaticDimension
        //        self.table_view.estimatedRowHeight = 350
        self.hideKeyboardWhenTappedAround()
        search_bar.isHidden = true
        // Do any additional setup after loading the view.
        //        if let loginToken = UserDefaults.standard.value(forKey: "Login_Token") as? String{
        //            print(loginToken)
        //            self.btn_Add.isHidden = false
        //        }else{
        //            self.btn_Add.isHidden = true
        //        }
        search_bar.delegate = self
        self.showCurrentLocation()
        table_view.keyboardDismissMode = .onDrag
        getPrivilegesDataApi()
        self.lbl_Title.text = "Eid Salah".localized
        self.btn_sort.setTitle("  sort".localized, for: .normal)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        
        //mark:SearchBar-------------------------------------
        var searchTextField:UITextField!
              
              if #available(iOS 13, *) {
                  searchTextField  = search_bar.subviews[0].subviews[2].subviews.last as? UITextField
              } else {
                  searchTextField = search_bar.subviews[0].subviews.last as? UITextField
              }
      
        searchTextField.layer.cornerRadius = 15
        searchTextField.textAlignment = NSTextAlignment.left
        searchTextField.leftView = nil
        searchTextField.placeholder = "Search".localized
        searchTextField.textColor = UIColor.white
        searchTextField.rightViewMode = UITextField.ViewMode.always
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
      self.getEidsalhaListApi()
    }
    
   

    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        // searchBar.showsCancelButton = true
           self.getEidsalhaListApi()
    }
    
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
        self.serachOrganizationListApi()
    }
    
    //MARK: Function Api----------------------
    
    
    func getEidsalhaListApi(){
        KVNProgress.show()
        Network.shared.getEidSalahList { (response) in
            KVNProgress.dismiss()
            self.dataModel = response!.data
            if response?.data != nil || response!.data.count > 0{
                for _ in 0..<response!.data.count{
                    self.arr_Selected.append(false)
                }
            }
            self.table_view.reloadData()
        }
    }
    
    func serachOrganizationListApi(){
      KVNProgress.show()
            Network.shared.getListOfSearchEidSalaha(searchKey: trimString(str:self.search_bar.text ?? "")) { (result) in
                KVNProgress.dismiss()
                guard let user = result else {
                    
                    return
                }
               self.dataModel = nil
            self.dataModel = user?.data
                  self.table_view.reloadData()
            }
    }
    
    
    func trimString(str: String) -> String{
        return str.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
    }
    
    //MARK:- Button Action
    @IBAction func btn_backAction(_ sender: Any) {
        if let controller = self.storyboard?.instantiateViewController(withIdentifier: "TabVC") {
            self.slideMenuController()?.changeMainViewController(controller, close: true)
            //  }
        }
    }
    @IBAction func btn_MapSearch(sender:UIButton){
        AppDelegate.instance.checkLocationPermission { (currentLat, currentLong) in
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "LocationOrgViewController" ) as! LocationOrgViewController
        vc.allLocationdataeid = self.dataModel
        vc.eidSalah = "EidSalah"
        vc.modalPresentationStyle = .fullScreen
        self.present(vc, animated: true, completion: nil)
    }
    }
    @IBAction func btn_searchTapped(_ sender: UIButton) {
        if sender.isSelected == true{
            sender.isSelected = false
            search_bar.isHidden = true
        }else{
            sender.isSelected = true
            search_bar.isHidden = false
        }
    }
    
    @IBAction func btn_addEidSalah_Tapped(_ sender: Any) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "EidSalahViewController") as! EidSalahViewController
        vc.type = .add
        vc.modalPresentationStyle = .fullScreen
        self.present(vc, animated: true, completion: nil)
    }
    
    @IBAction func btn_sortAction(_ sender: Any) {
        let dropDown = DropDown()
        
        dropDown.anchorView = self.btn_sort
        dropDown.bottomOffset = CGPoint(x: 80, y:(dropDown.anchorView?.plainView.bounds.height)!)
        
        dropDown.dataSource = ["date/time" , "nearest location"]
        dropDown.textColor = UIColor.black
        dropDown.backgroundColor = UIColor.white
        dropDown.selectionAction = { [unowned self] (index: Int, item: String) in
            print("Selected item: \(item) at index: \(index)")
            
            if index == 0{
                 self.getEidsalhaListApi()
            }else{
                
                self.closestLoc(userLocation: self.currentLocation)
            }
        }
        dropDown.show()
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}


//MARK:-TableView-------------------------------------
extension MHEidSalahVC:UITableViewDelegate,UITableViewDataSource,readmoreDelegate  {
    
    // MARK: - my cell delegate
    func moreTapped(cell: MHEidSalahTableCell, index: Int) {
        print(index)
        if self.arr_Selected[index] == true {
            arr_Selected[index] = false
        }else{
            arr_Selected[index] = true
        }
        table_view.layoutIfNeeded()
        table_view.beginUpdates()
        
        table_view.reloadRows(at: [IndexPath(row: index, section: 0)], with: UITableView.RowAnimation.automatic)
        table_view.endUpdates()
    }
    
    func deleteTapped(cell: MHEidSalahTableCell, index: Int)  {
        let dict = self.dataModel[index]
        AKAlertController.alert("", message: "Do you want to delete this eid salah?".localized, buttons: ["Ok".localized,"Cancel".localized], tapBlock: { (_, _, index) in
            if index == 1 {return}
            Network.shared.delateEidSalahListItem(id: "\(dict.id)") { (response) in
                if response!.success{
                    AKAlertController.alert("", message: response!.message, buttons: ["Ok".localized], tapBlock: { (UIAlertController, UIAlertAction, Int) in
                        self.viewWillAppear(true)
                    })
                }else{
                    AKAlertController.alert("", message: response!.message)
                }
            }
            return
        })
    }
    
    @objc func btn_Location(sender:UIButton){
        
        let lat = self.dataModel[sender.tag].latitude
        let long = self.dataModel[sender.tag].longitude
        self.location(lat: lat, long: long, title: self.dataModel[sender.tag].address )
       
    }
    func editTapped(cell: MHEidSalahTableCell, index: Int)  {
        let data = self.dataModel[index]
        
        let (_, held) = self.getIndexOfDropDown(type: .held, index: data.held - 1)
        let (_, attend) = self.getIndexOfDropDown(type: .attend, index: data.attendees - 1)
        let (_, parking) = self.getIndexOfDropDown(type: .parking, index: data.parking - 1)
        let (_, language) = self.getIndexOfDropDown(type: .language,index: data.khutbahLang - 1)
        var phoneCode: String = ""
        var phoneNumber: String = ""
        if data.phoneNum == "" || data.phoneNum == nil{
            phoneCode = "+61"
            phoneNumber = ""
        }else{
            let phone = data.phoneNum.components(separatedBy: "-")
            phoneCode = phone[0]
            phoneNumber = phone[1]
        }
        

        
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "EidSalahViewController") as! EidSalahViewController
        vc.type = .edit
        vc.countryCode = phoneCode
        let dict  = ["orgName"  : data.orgName,
                     "orgAbbr"  : data.orgAbb,
                     "orgNameId": data.id,
                     "city"     : data.city,
                     "location" : data.address ,
                     "latitude" : data.latitude,
                     "longitude" : data.longitude,
                     "date"     : data.date,
                     "salahTime": data.salahTime,
                     "thakbeerTime": data.takbirTime,
                     "held"     : held,
                     "attend"   : attend,
                     "parking"  : parking,
                     "language" : language,
                     "phone"    : phoneNumber,
                     "info"     : data.info,
                     "added_by" : data.addedBy
            ] as [String : Any]
        vc.dataDict = dict
        vc.modalPresentationStyle = .fullScreen
        self.present(vc, animated: true, completion: nil)
    }
    
    func getDropDownData(type: EidSalahDropDown) -> [String]{
        if type == .held{
            return ["Indoor", "Outdoor"]
        }else if type == .attend{
            return ["Men Only", "Men, Women and Family"]
        }else if type == .parking{
            return ["Limited", "Plenty", "No Parking"]
        }else if type == .language{
            return ["English", "Urdu", "Arabic", "Malay"]
        }else{
            return []
        }
    }
    
    func getIndexOfDropDown(type: EidSalahDropDown, value: String? = "", index: Int? = nil) -> (Int, String){
        let array = self.getDropDownData(type: type)
        if value != ""{
            for i in 0..<array.count{
                if array[i] == value{
                    return (i + 1, array[i])
                }
            }
            return (0, value!)
        }else if index != nil{
            return (index! + 1, array[index!])
        }else{
            return (0, "")
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.dataModel != nil{
            return self.dataModel.count
        }else{
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 1000.0

    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "MHEidSalahTableCell", for: indexPath) as! MHEidSalahTableCell
        cell.delegate = self
        
        
        //Check for Admin,super Admin,Normal User----------------------
        if (UserDefaults.standard.value(forKey: "Login_Token") as? String) != nil{
           // self.btn_Add.isHidden = false
            if let superAdmin = UserDefaults.standard.value(forKey: "isSuperAdmin") as? Bool{
                
                if superAdmin == true{
                    cell.btn_edit.isHidden = false
                    cell.btn_delete.isHidden = false
                  self.btn_Add.isHidden = false
                }else{
                    
                    if let is_staff = UserDefaults.standard.value(forKey: "is_staff") as? Bool{
                        
                        if is_staff == true{
                            //mark:check For own Organisation---------------------------
                            if let orgOwn_Array = UserDefaults.standard.value(forKey: "OwnOrg_Array") as? [Int]{
                                
                            if orgOwn_Array.contains(dataModel[indexPath.row].org){
                              
                                
                                var org_P = [PriviligesData]()
                                org_P = Global.shared.priviligesData
                                let filertedData = org_P.filter{$0.org_id == dataModel[indexPath.row].org}
                                filertedData.forEach {
                                    //   print("model data objects::",$0.org_perm as? String)
                                    
                                    if $0.org_perm == "iqama_only"{
                                        
                                        cell.btn_edit.isHidden = true
                                        cell.btn_delete.isHidden = true
                                        self.btn_Add.isHidden = true
                                        
                                    }else{
                                        cell.btn_edit.isHidden = false
                                        cell.btn_delete.isHidden = false
                                       self.btn_Add.isHidden = false
                                        
                                    }
                                }
                                    
                                }else{
                                    cell.btn_edit.isHidden = true
                                    cell.btn_delete.isHidden = true
                                    self.btn_Add.isHidden = true
                                }//orgOwn
                                
                            }
                        }//staff true
                    }//staff
                    
                }
            }//SuperAdmin
        }//Login
        else{
            self.btn_Add.isHidden = true
            cell.btn_edit.isHidden = true
            cell.btn_delete.isHidden = true
            
        }
        
  
//        let now = Date()
//        let date = NSDate(timeIntervalSince1970: Double(self.dataModel[indexPath.row].ti ?? 0))
//        cell.lbl_date.text = now.timeAgoDisplay(dateCreated:date as Date,timeStamp:self.dataModel[indexPath.row].logo ?? 0 )
        
        
        cell.btn_Location.tag = indexPath.row
        
        // cell.btn_Location.setAttributedTitle(attributeString, for: .normal)
        
        cell.btn_Location.addTarget(self, action: #selector(btn_Location(sender:)), for: .touchUpInside)
       //adds shadow to the layer of cell
        cell.view_salah.layer.cornerRadius = 10.0
        cell.view_salah.layer.masksToBounds = false
        cell.view_salah.layer.shadowColor = UIColor.black.cgColor
        cell.view_salah.layer.shadowOffset = CGSize(width: 0, height: 6)
        cell.view_salah.layer.shadowOpacity = 0.5

        //makes the cell round
        let containerView = cell.contentView
        containerView.layer.cornerRadius = 2
        containerView.clipsToBounds = true
        cell.btn_edit.tag = indexPath.row
        cell.btn_delete.tag = indexPath.row
        cell.btn_readMore.tag = indexPath.row

        cell.btn_image.contentMode = .scaleAspectFill
   
        if self.dataModel[indexPath.row].logo != nil && self.dataModel[indexPath.row].logo != ""{
            cell.btn_image.sd_setImage(with: URL(string:
                self.dataModel[indexPath.row].logo!),placeholderImage: UIImage.init(named: "default_header_iPhone.png"), completed: nil)
        }else{
            cell.btn_image.image = UIImage(named: "default_header_iPhone.png")
        }
        
        if self.arr_Selected[indexPath.row] == true {
            cell.btn_readMore.setTitle("Show less".localized, for: .normal)
            cell.imgShowDrop.image = UIImage(named: "up_arrow_iPhone")
        }else {
            cell.btn_readMore.setTitle("Show more".localized, for: .normal)
            cell.imgShowDrop.image = UIImage(named: "down_arrow_iPhone_blue")
        }
   
//        cell.lbl_name.preferredMaxLayoutWidth = cell.lbl_name.frame.width
//        cell.lbl_location.preferredMaxLayoutWidth = cell.lbl_location.frame.width
        cell.configureCell(data: self.dataModel[indexPath.row], isSelected: self.arr_Selected[indexPath.row])
        return cell
    }
    
    
}

extension MHEidSalahVC: CLLocationManagerDelegate{
    
    
    //MARK:current Location lat long----------------------------
    func showCurrentLocation() {
        // view_mapView.settings.myLocationButton = true
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestAlwaysAuthorization()
        locationManager.startUpdatingLocation()
        if CLLocationManager.locationServicesEnabled() {
            switch (CLLocationManager.authorizationStatus()) {
            case .notDetermined, .restricted, .denied:
                print("No access")
            case .authorizedAlways, .authorizedWhenInUse:
                print("Access")
            }
        } else {
            print("Location services are not enabled")
        }
     
       
        
    }
    //Location Manager delegates
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        let userLocation = locations.last
       _ = CLLocationCoordinate2D(latitude: userLocation!.coordinate.latitude, longitude: userLocation!.coordinate.longitude)
      
        
        print("Latitude :- \(userLocation!.coordinate.latitude)")
        print("Longitude :-\(userLocation!.coordinate.longitude)")
      
        self.currentLocation = CLLocation(latitude:userLocation!.coordinate.latitude , longitude: userLocation!.coordinate.longitude)
        locationManager.stopUpdatingLocation()
        
    }
    
    func closestLoc(userLocation:CLLocation){
        var distances = [CLLocationDistance]()
        var coord = CLLocation()
        for  i in 0..<dataModel.count{
            coord = CLLocation(latitude:  (dataModel?[i].latitude)!, longitude:  (dataModel?[i].longitude)!)
            distances.append(coord.distance(from: userLocation))
            print("distance = \(coord.distance(from: userLocation))")
            
           // let didsghjdfgshj = "\(distances[i])"
            dataModel[i].distance = Int(distances[i])
            
           
          }
        let myArraySorted = dataModel.sorted{$1.distance as! Int > $0.distance as! Int}

      
        self.dataModel = myArraySorted
       
        table_view.reloadData()
        if self.dataModel.count != 0{
            let closest = distances.min()//shortest distance
            let position = distances.index(of: closest!)//index of shortest distance
            // print("closest = \(closest!), index = \(String(describing: position))")
        }
       
        
       
       
    }

}
