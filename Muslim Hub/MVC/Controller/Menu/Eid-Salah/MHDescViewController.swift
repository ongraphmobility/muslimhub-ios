//
//  MHDescViewController.swift
//  Muslim Hub
//
//  Created by Ongraph on 5/14/19.
//  Copyright © 2019 OnGraph. All rights reserved.
//

import UIKit

protocol MHDescDelegate {
    func getDescription(desc: String)
}

class MHDescViewController: UIViewController,UITextViewDelegate {
    
    /**
     MARK: - Properties
     */
    @IBOutlet weak var btnSave      : UIButton!
    @IBOutlet weak var btnCancel      : UIButton!
    @IBOutlet weak var btn_back     : UIButton!
    @IBOutlet weak var lblHeader: UILabel!
    
    @IBOutlet weak var txtView      : UITextView!
    @IBOutlet weak var view_Header: UIView!
    var descStr                     : String                = ""
    var delegate                    : MHDescDelegate!
    var textDesc:String?
    var textTitle:String = ""
    
    //end
    
    /**
     MARK: - UIViewController Life Cycle
     */
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        btnSave.layer.cornerRadius = 10
        // shadow
        btnSave.layer.shadowColor = UIColor.darkGray.cgColor
        btnSave.layer.shadowOffset = CGSize(width: 0, height: 3)
        btnSave.layer.shadowOpacity = 0.7
        btnSave.layer.shadowRadius = 4.0
        
        btnCancel.layer.cornerRadius = 10
        // shadow
        btnCancel.layer.shadowColor = UIColor.darkGray.cgColor
        btnCancel.layer.shadowOffset = CGSize(width: 0, height: 3)
        btnCancel.layer.shadowOpacity = 0.7
        btnCancel.layer.shadowRadius = 4.0
        
        
        txtView.shadowBlackToHeader()
        self.txtView.delegate = self
        if textTitle == "Add Description".localized{
            lblHeader.text = textTitle
        }
        if textDesc != nil{
            self.txtView.text = textDesc
        }else if descStr != ""{
            self.txtView.text = descStr
        }else{
            txtView.text = "Description".localized
            
        }
        if txtView.text == "Description".localized{
            txtView.textColor = UIColor.lightGray
        }else{
            txtView.textColor = UIColor.black
        }
        
        
        self.txtView.textContainer.lineFragmentPadding = 10
        
        
        
        view_Header.shadowBlackToHeader()
        self.txtView.layer.cornerRadius = 8
        self.txtView.shadowBlackToHeader()
        // Do any additional setup after loading the view.
    }
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if txtView.textColor == UIColor.lightGray {
            txtView.text = nil
            txtView.textColor = UIColor.black
        }
    }
    func textViewDidEndEditing(_ textView: UITextView) {
        if txtView.text.isEmpty{
            txtView.text = "Description".localized
            txtView.textColor = UIColor.lightGray
        }
    }
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    @IBAction func btnBackTapped(_ sender: Any) {
        self.dismiss(animated: false, completion: nil)
    }
    @IBAction func btnSaveTapped(_ sender: Any) {
        if self.txtView.text.count > 0{
            self.delegate.getDescription(desc: self.txtView.text)
            self.dismiss(animated: false)
            /*self.dismiss(animated: false) {
             self.delegate.getDescription(desc: self.txtView.text)
             }*/
        }else{
            AKAlertController.alert("", message: "Please enter description".localized)
        }
    }
}

