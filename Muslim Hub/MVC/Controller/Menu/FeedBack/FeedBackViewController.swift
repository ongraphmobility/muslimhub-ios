//
//  FeedBackViewController.swift
//  Muslim Hub
//
//  Created by Sakshi Singh on 18/04/19.
//  Copyright © 2019 OnGraph. All rights reserved.
//

import UIKit
import KVNProgress

class FeedBackViewController: UIViewController {
    
    @IBOutlet weak var btn_Save:UIButton!
    @IBOutlet weak var lbl_feedBack:UILabel!
    @IBOutlet weak var txt_Subject:UITextField!
    @IBOutlet weak var txt_Message:UITextField!
    @IBOutlet weak var txt_Email:UITextField!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        if #available(iOS 13.0, *) {
            statusBarColorChange(color:UIColor.init(hex:  0x05BDB9))
        } else {
            UIApplication.statusBarBackgroundColor = UIColor.init(hex: 0x05BDB9)
        }
        self.navigationController?.isNavigationBarHidden = true
        
        btn_Save.layer.cornerRadius = 5
        // shadow
        btn_Save.layer.shadowColor = UIColor.darkGray.cgColor
        btn_Save.layer.shadowOffset = CGSize(width: 0, height: 2)
        btn_Save.layer.shadowOpacity = 0.7
        btn_Save.layer.shadowRadius = 4.0
        
        lbl_feedBack.layer.cornerRadius = 10.0
        lbl_feedBack.layer.maskedCorners =  [.layerMaxXMaxYCorner]
        
        self.lbl_feedBack.text = "Feedback".localized
        self.txt_Subject.placeholder = "Subject".localized
        self.txt_Email.placeholder = "Email Id".localized
        self.txt_Message.placeholder = "Message".localized
        self.btn_Save.setTitle("Send".localized, for: .normal)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
    }
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(true)
    }
    
    @IBAction func btn_Back(sendr:UIButton){
        
        if let controller = self.storyboard?.instantiateViewController(withIdentifier: "TabVC") {
            self.slideMenuController()?.changeMainViewController(controller, close: true)
            
        }
    }
    
    @IBAction func btn_save(sender:UIButton){
        if ((txt_Email.text?.validateEmail())!) == false{
            AKAlertController.alert("", message: "Please enter a valid email".localized)
        }else{
            
            self.feedbackApi()
        }
    }
    
    //MARK:Function Api's-----------------------------------------------------------
    
    func feedbackApi(){
        
        KVNProgress.show()
        Network.shared.feedBackAPi(subject: self.txt_Subject.text ?? "", email: self.txt_Email.text ?? "", message: self.txt_Message.text ?? ""){ (result) in
            KVNProgress.dismiss()
            
            guard let user = result else {
                
                return
            }
            print(user)
            
            if user.success == true{
                
                self.txt_Subject.text = ""
                self.txt_Email.text = ""
                self.txt_Message.text = ""
                self.showSingleAlertMessage(message: "", subMsg: user.message ?? "Thanks for your FeedBack", sender: self, completion:
                    { (success) -> Void in
                        if success == true{
                            
                            
                            self.dismiss(animated: true, completion: nil)
                            
                            
                        }
                })
            }else{
                
                self.showSingleAlertMessage(message: "", subMsg: user.message ?? "", sender: self, completion:
                    { (success) -> Void in
                        if success == true{
                            
                            
                            
                        }
                })
            }
        }
    }
    
}

