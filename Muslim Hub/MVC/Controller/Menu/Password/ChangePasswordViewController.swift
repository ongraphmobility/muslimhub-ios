//
//  ChangePasswordViewController.swift
//  Muslim Hub
//
//  Created by Sakshi Singh on 24/04/19.
//  Copyright © 2019 OnGraph. All rights reserved.
//

import UIKit
import KVNProgress

protocol FirstTimeLoginDelegate{
    
    func isLoginFirstTime(bool:Bool)
}

class ChangePasswordViewController: UIViewController {
    
    
    @IBOutlet weak var txt_Old:UITextField!
    @IBOutlet weak var txt_NewPassword:UITextField!
    @IBOutlet weak var txt_ConfirmPassword:UITextField!
    @IBOutlet weak var btn_Change:UIButton!
    @IBOutlet weak var view_Header:UIView!
    @IBOutlet weak var btn_Backk:UIButton!
    @IBOutlet weak var lbl_HaederTitle1:UILabel!
    @IBOutlet weak var lbl_HaederTitle2:UILabel!
    
    @IBOutlet weak var lbl_first_ChangePassword:UILabel!
    @IBOutlet weak var lbl_second_ChangePassword:UILabel!
    var isFirstLoginDelegate:FirstTimeLoginDelegate!
    var chnageFromLogin:String!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        // corner radius
        if #available(iOS 13.0, *) {
            statusBarColorChange(color:UIColor.init(hex:  0x05BDB9))
        } else {
            UIApplication.statusBarBackgroundColor = UIColor.init(hex: 0x05BDB9)
        }
        
        btn_Change.layer.cornerRadius = 10
        btn_Change.shadowBlackToHeader()
        // shadow
        view_Header.shadowBlackToHeader()
        
        if chnageFromLogin == "chnageFromLogin"{
            btn_Backk.isHidden = true
        }else{
            btn_Backk.isHidden = false
        }
        
        //Langauage
        self.txt_Old.placeholder = "Old Password".localized
        self.txt_ConfirmPassword.placeholder = "Confirm Password".localized
        self.txt_NewPassword.placeholder = "New Password".localized
        
        self.lbl_HaederTitle1.text = "Change Password".localized
        self.lbl_HaederTitle2.text = "Change Password".localized
        self.btn_Change.setTitle("Change Password".localized, for: .normal)
    }
    
    
    //MARK:IBACtion--------------------------------------------------------
    @IBAction func btn_Back(sender:UIButton){
        //self.navigationController?.popViewController(animated: true)
        self.dismiss(animated: true, completion: nil)
    }
    @IBAction func btn_ChangePassword(sender:UIButton){
        guard !txt_Old.text!.isEmpty else {
            AKAlertController.alert("", message: "Please enter your old password".localized)
            return
        }
        
        guard !txt_NewPassword.text!.isEmpty else {
            AKAlertController.alert("", message: "Please enter your new password".localized)
            return
        }
        if txt_NewPassword.text == txt_Old.text!{
            AKAlertController.alert("", message: "Old password and new password are same.".localized)
            return
        }
        guard !txt_ConfirmPassword.text!.isEmpty else {
            AKAlertController.alert("", message: "Please enter your confirm password".localized)
            return
        }
        if txt_NewPassword.text != txt_ConfirmPassword.text!{
            AKAlertController.alert("", message: "Please enter your same password".localized)
            return
        }
        
        self.ChangePasswordApis()
    }
    
    //MARK:Function Api's-----------------------------------------------------------
    func ChangePasswordApis(){
        
        KVNProgress.show()
        Network.shared.changePassword(old_password: txt_Old.text!, new_password1: txt_NewPassword.text!, new_password2: txt_ConfirmPassword.text!) { (result) in
            KVNProgress.dismiss()
            
            guard let user = result else {
                
                return
            }
            print(user)
            
            if user.success == true{
                
                self.showSingleAlertMessage(message: "", subMsg: user.message ?? "Password changed successfully".localized, sender: self, completion:
                    { (success) -> Void in
                        if success == true{
                            
                            self.isFirstLoginDelegate?.isLoginFirstTime(bool: true)
                            self.dismiss(animated: true, completion: nil)
                            
                        }
                })
            }else{
                
                self.showSingleAlertMessage(message: "", subMsg: user.message ?? "", sender: self, completion:
                    { (success) -> Void in
                        if success == true{
                            
                        }
                })
            }
        }
    }
    
}
