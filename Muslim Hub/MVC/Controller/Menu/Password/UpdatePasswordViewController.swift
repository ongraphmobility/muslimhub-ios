//
//  UpdatePasswordViewController.swift
//  Muslim Hub
//
//  Created by Ongraph on 02/09/19.
//  Copyright © 2019 OnGraph. All rights reserved.
//

import UIKit

class UpdatePasswordViewController: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        if #available(iOS 13.0, *) {
            statusBarColorChange(color:UIColor.init(hex:  0x05BDB9))
        } else {
            UIApplication.statusBarBackgroundColor = UIColor.init(hex: 0x05BDB9)
        }
        
    }
    
}
