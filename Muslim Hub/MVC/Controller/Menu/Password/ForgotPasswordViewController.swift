//
//  ForgotPasswordViewController.swift
//  Muslim Hub
//
//  Created by Sakshi Singh on 24/04/19.
//  Copyright © 2019 OnGraph. All rights reserved.
//

import UIKit
import KVNProgress

class ForgotPasswordViewController: UIViewController {
    
    @IBOutlet weak var txt_Email:UITextField!
    @IBOutlet weak var btn_Forgot:UIButton!
    @IBOutlet weak var view_Header:UIView!
    @IBOutlet weak var lbl_HaederTitle1:UILabel!
    @IBOutlet weak var lbl_HaederTitle2:UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        // corner radius
        if #available(iOS 13.0, *) {
            statusBarColorChange(color:UIColor.init(hex:  0x05BDB9))
        } else {
            UIApplication.statusBarBackgroundColor = UIColor.init(hex: 0x05BDB9)
        }
              
        btn_Forgot.layer.cornerRadius = 10
        // shadow
        btn_Forgot.layer.shadowColor = UIColor.darkGray.cgColor
        btn_Forgot.layer.shadowOffset = CGSize(width: 0, height: 2)
        btn_Forgot.layer.shadowOpacity = 0.7
        btn_Forgot.layer.shadowRadius = 4.0
        self.btn_Forgot.shadowWithCornerReadius(cornerRadius: 10)
        
        //Langauage
        self.btn_Forgot.setTitle("Forgot Password".localized, for: .normal)
        self.txt_Email.placeholder = "Email Id".localized
        self.lbl_HaederTitle1.text = "Forgot Password".localized
        self.lbl_HaederTitle2.text = "Forgot Password".localized
        
        // shadow
        view_Header.shadowBlackToHeader()
    }
    
    //MARK:IBACtion--------------------------------------------------------
    @IBAction func btn_Back(sender:UIButton){
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func btn_ForgotPassowrd(sender:UIButton){
        self.ForgotApis()
        
    }
    
    //MARK:ForgotApi------------------------------------------------------
    func ForgotApis(){
        
        KVNProgress.show()
        Network.shared.forgotPassword(email: txt_Email.text!) { (result) in
            if KVNProgress.isVisible(){
                KVNProgress.dismiss()
            }
            guard let user = result else {
                
                return
            }
            print(user)
            
            if user.success == true{
                
                self.showSingleAlertMessage(message: "", subMsg: result?.message ?? "", sender: self, completion:
                    { (success) -> Void in
                        if success == true{
                            self.dismiss(animated: true, completion: nil)
                            
                            
                        }
                })
                
          }else{
                
                self.showSingleAlertMessage(message: "", subMsg: result?.message ?? "", sender: self, completion:
                                { (success) -> Void in
                                 if success == true{
                                  
                                    }
                            })
          }
            
            
        }
    }
    
}
