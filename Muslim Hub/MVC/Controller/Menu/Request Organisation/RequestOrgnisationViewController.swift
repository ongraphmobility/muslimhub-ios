//
//  RequestOrgnisationViewController.swift
//  Muslim Hub
//
//  Created by Ongraph on 25/06/19.
//  Copyright © 2019 OnGraph. All rights reserved.
//

import UIKit
import CountryPickerViewSwift
import GooglePlaces
import GooglePlacePicker
import KVNProgress

class RequestOrgnisationViewController: UIViewController,UITextFieldDelegate,UITextViewDelegate {
    
    @IBOutlet weak var tableview_RequestOrg:UITableView!
    @IBOutlet weak var view_TableViewLastCell:UIView!
    @IBOutlet weak var btn_Request:UIButton!
    @IBOutlet weak var view_Header:UIView!
    
    @IBOutlet weak var lbl_Comment:UILabel!
    @IBOutlet weak var lbl_IsPrayerAvailabel:UILabel!
    @IBOutlet weak var lbl_AllFieldsMandatory:UILabel!
    @IBOutlet weak var lbl_Note:UILabel!
    
    @IBOutlet weak var lbl_Header:UILabel!
    @IBOutlet weak var view_UpperTableView:UIView!
    @IBOutlet weak var btn_Prayer:UIButton!
    @IBOutlet weak var txtView_Comment:UITextView!
    var dic_RegisterValue = [String:Any]()
    var lat = Double()
    var long = Double()
    var tag_forAddress:Int!
    var isprayer:String!
    var countrycode:String!
    let array_Org = ["Organization Name","Organization abbreviation","Organization Type","Description","Address","Org Register Number","Board Member","Requestor Name","Requestor Contact","Org Contact Name","Org Contact","Org Email","Org URL"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        if #available(iOS 13.0, *) {
            statusBarColorChange(color:UIColor.init(hex: 0x0DC9E4))
        } else {
            UIApplication.statusBarBackgroundColor = UIColor.init(hex: 0x0DC9E4)
        }
        self.cornerRadiusShadow()
        self.hideKeyboardWhenTappedAround()
        for i in 0...array_Org.count - 1{
            if dic_RegisterValue[array_Org[i]] == nil{
                
                self.dic_RegisterValue.updateValue("", forKey:array_Org[i])
            }
        }
        
        self.lbl_Comment.text = "Comments".localized
        self.lbl_IsPrayerAvailabel.text = "Is prayer available for this organization?".localized
        self.lbl_AllFieldsMandatory.text = "All fields are mandatory*".localized
        self.lbl_Note.text = "The app administrator will get in touch with you via an email after this request to confirm registration".localized
        self.lbl_Header.text = "Request Organisation".localized
        self.dic_RegisterValue.updateValue("YES", forKey: array_Org[6])
        self.dic_RegisterValue.updateValue("Description".localized, forKey:"Description")
        self.isprayer = "false"
        self.btn_Request.setTitle("SEND REQUEST".localized, for: .normal)
    }
    //MARK:TextView Delegate and DataSource method------------------------
    func textViewDidBeginEditing(_ textView: UITextView) {
        guard let indexPath = textView.tableViewIndexPath(self.tableview_RequestOrg) else { return }
        
        switch (indexPath.section, 3) {
            
        case (0,3):
            
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "MHDescViewController") as! MHDescViewController
            vc.delegate = self
            vc.textDesc = textView.text!
            textView.keyboardType = .default
            vc.modalPresentationStyle = .fullScreen
            present(vc, animated: false, completion: nil)
        default:
            break
            
        }
        
    }
    func textViewDidEndEditing(_ textView: UITextView) {
        
        textView.textColor = UIColor.black
        
    }
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        //300 chars restriction
        return txtView_Comment.text.count + (text.count - range.length) <= 250
    }
    
    
    
    //MARK:TextField Delegate and DataSource Method--------------------
    func textFieldDidBeginEditing(_ textField: UITextField) {
        guard let indexPath = textField.tableViewIndexPath(self.tableview_RequestOrg) else { return }
        switch (indexPath.section, indexPath.row) {
            
        case (0,0):
            textField.keyboardType = .default
            dic_RegisterValue.updateValue(textField.text ?? "", forKey: array_Org[0])
            
        case (0,1):
            textField.keyboardType = .default
            dic_RegisterValue.updateValue(textField.text ?? "", forKey: array_Org[1])
        case (0,2):
            
            let arr_OrgType  = ["mosque", "charity", "community","educational"]
            AKMultiPicker().openPickerIn(textField, firstComponentArray: arr_OrgType) { (value1, _, index1, _) in
                textField.text = value1
                
                self.dic_RegisterValue.updateValue(textField.text ?? "", forKey: self.array_Org[2])
                
            }
        case (0,3):
            
            
            print("")
            
            
        case (0,4):
            
            let autocompleteController = GMSAutocompleteViewController()
            autocompleteController.delegate = self
            let filter = GMSAutocompleteFilter()
            filter.type = .noFilter  //suitable filter type
            //   filter.country = "AUS"  //appropriate country code
            autocompleteController.autocompleteFilter = filter
            self.tag_forAddress = 4
            present(autocompleteController, animated: true, completion: nil)
            
        case (0,5):
            textField.keyboardType = .numberPad
            self.dic_RegisterValue.updateValue(textField.text as Any, forKey: array_Org[5])
            
            
            
        case (0,7):
            textField.keyboardType = .default
            self.dic_RegisterValue.updateValue(textField.text as Any, forKey: array_Org[7])
        case (0,8):
            textField.keyboardType = .numberPad
            self.dic_RegisterValue.updateValue(textField.text as Any, forKey: array_Org[8])
        case (0,9):
            textField.keyboardType = .default
            self.dic_RegisterValue.updateValue(textField.text as Any, forKey: array_Org[9])
            
        case (0,10):
            textField.keyboardType = .numberPad
            self.dic_RegisterValue.updateValue(textField.text as Any, forKey: array_Org[10])
        case (0,11):
            textField.keyboardType = .emailAddress
            self.dic_RegisterValue.updateValue(textField.text as Any, forKey: array_Org[11])
            
        case (0,12):
            textField.keyboardType = .URL
            self.dic_RegisterValue.updateValue(textField.text as Any, forKey: array_Org[12])
        default:
            break
        }
    }
    
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        guard let indexPath = textField.tableViewIndexPath(self.tableview_RequestOrg) else { return }
        switch (indexPath.section, indexPath.row) {
            
        case (0,0):
            
            dic_RegisterValue.updateValue(textField.text ?? "", forKey: array_Org[0])
            
        case (0,1):
            
            dic_RegisterValue.updateValue(textField.text ?? "", forKey: array_Org[1])
            
        case (0,3):
            
            print("")
            
        case (0,5):
            textField.keyboardType = .numberPad
            self.dic_RegisterValue.updateValue(textField.text as Any, forKey: array_Org[5])
        case (0,7):
            self.dic_RegisterValue.updateValue(textField.text as Any, forKey: array_Org[7])
        case (0,8):
            
            self.dic_RegisterValue.updateValue(textField.text as Any, forKey: array_Org[8])
        case (0,9):
            
            self.dic_RegisterValue.updateValue(textField.text as Any, forKey: array_Org[9])
            
        case (0,10):
            self.dic_RegisterValue.updateValue(textField.text as Any, forKey: array_Org[10])
        case (0,12):
            textField.keyboardType = .URL
            self.dic_RegisterValue.updateValue(textField.text as Any, forKey: array_Org[12])
            
            //            if (textField.text?.isValidURL)! == false {
            //                AKAlertController.alert("", message: "Please Enter Valid Url")
            //            }
            
        case (0,11):
            if ((textField.text?.validateEmail())!) == false{
                AKAlertController.alert("", message: "Please Enter Valid Email Id")
            }
            self.dic_RegisterValue.updateValue(textField.text as Any, forKey: array_Org[11])
        default:
            break
        }
        //mark:Condition for appending mobile Number--------------
        let index = IndexPath(row: textField.tag, section: 0)
        let cell: Request_Organisation_CellTableViewCell = self.tableview_RequestOrg.cellForRow(at: index) as! Request_Organisation_CellTableViewCell
        
        
        
        dic_RegisterValue.updateValue(cell.txt_FillForm.text as AnyObject, forKey: array_Org[textField.tag])
        let key = array_Org[textField.tag]
        if key == "Org Contact" || key == "Requestor Contact"{
            
            let countryCode = cell.lbl_countryCodeLabel.text ?? "+61"
            
            let mobileNumber = dic_RegisterValue[key] as? String
            let mobileNumberWithCode = (countryCode + "-" + mobileNumber!)
            
            
            dic_RegisterValue.updateValue(mobileNumberWithCode, forKey: array_Org[textField.tag])
            
        }
        self.tableview_RequestOrg.reloadData()
    }
    //MARK:IBACtion----------------------------------------
    @IBAction func btn_Back(sendr:UIButton){
        
        if let controller = self.storyboard?.instantiateViewController(withIdentifier: "TabVC") {
            self.slideMenuController()?.changeMainViewController(controller, close: true)
        }
    }
    @IBAction func btn_PrivateOrg(sender:UIButton){
        
        if btn_Prayer.isSelected{
            btn_Prayer.isSelected = false
            //  btn_Privateorg.backgroundColor = UIColor.white
            self.isprayer = "NO"
            btn_Prayer.setBackgroundImage(#imageLiteral(resourceName: "uncheckbox_iPhone"), for: .normal)
        }else{
            btn_Prayer.isSelected = true
            self.isprayer = "YES"
            // btn_Privateorg.backgroundColor = UIColor.init(hex: 0x0BC9E4)
            btn_Prayer.setBackgroundImage(#imageLiteral(resourceName: "checkbox_iPhone"), for: .normal)
        }
    }
    @IBAction func btn_RequestOrganisation(sendr:UIButton){
        
        requestApi()
        
        
    }
    @objc func showCountryPickerView(_ sender: UIButton) {
        let countryView = CountrySelectView.shared
        countryView.barTintColor = .red
        countryView.displayLanguage = .english
        countryView.show()
        countryView.selectedCountryCallBack = { (countryDic) -> Void in
            self.countrycode = "+\(countryDic["code"] as! NSNumber)"
            
            let index = IndexPath(row: sender.tag, section: 0)
            let cell: Request_Organisation_CellTableViewCell = self.tableview_RequestOrg.cellForRow(at: index) as! Request_Organisation_CellTableViewCell
            cell.lbl_countryCodeLabel.text = self.countrycode
            
            // cell.img_CountryCode.image = countryDic["countryImage"] as? UIImage
            self.tableview_RequestOrg.reloadData()
        }
    }
    @objc func btn_YesNoBM(sender:UIButton){
        
        let index = IndexPath(row: 6, section: 0)
        let cell: Request_Organisation_CellTableViewCell = self.tableview_RequestOrg.cellForRow(at: index) as! Request_Organisation_CellTableViewCell
        
        if sender.tag == 0{
            
            cell.btn_Yes_BM.backgroundColor = UIColor.init(hex: 0x0DC9E4)
            cell.btn_No_BM.backgroundColor = UIColor.white
            cell.btn_Yes_BM.setTitleColor(UIColor.white, for: .normal)
            cell.btn_No_BM.setTitleColor(UIColor.darkGray, for: .normal)
            
            self.dic_RegisterValue.updateValue("YES", forKey: array_Org[6])
        }else if sender.tag == 1{
            cell.btn_No_BM.backgroundColor = UIColor.init(hex: 0x0DC9E4)
            cell.btn_Yes_BM.backgroundColor = UIColor.white
            cell.btn_No_BM.setTitleColor(UIColor.white, for: .normal)
            cell.btn_Yes_BM.setTitleColor(UIColor.darkGray, for: .normal)
            self.dic_RegisterValue.updateValue("NO", forKey: array_Org[6])
            
        }
        
    }
    
    //MARK - Api Functions-------
    func requestApi(){
        let org_Conatct = dic_RegisterValue["Org Contact"] as? String
        
        var phoneCode: String = ""
        var phoneNumber: String = ""
        if phoneNumber != "" {
            let phone =  org_Conatct?.components(separatedBy: "-")
            phoneCode = phone?[0] ?? "+61"
            phoneNumber = phone?[1] ?? ""
            while (phoneNumber.hasPrefix("0")) {
                phoneNumber.remove(at: (phoneNumber.startIndex))
                
                self.dic_RegisterValue.updateValue(phoneCode + "-" + phoneNumber, forKey: "Org Contact")
            }
            
        }
        
        
        let Requestor_Contact = dic_RegisterValue["Requestor Contact"] as? String
        var Requestor_phoneCode: String = ""
        var Requestor_phoneNumber: String = ""
        if Requestor_phoneNumber != "" {
            
            let Requestor_phone = Requestor_Contact?.components(separatedBy: "-")
            Requestor_phoneCode = Requestor_phone?[0] ?? "+61"
            Requestor_phoneNumber = Requestor_phone?[1] ?? ""
            while (Requestor_phoneNumber.hasPrefix("0")) {
                Requestor_phoneNumber.remove(at: (Requestor_phoneNumber.startIndex))
                
                self.dic_RegisterValue.updateValue(Requestor_phoneCode + "-" + Requestor_phoneNumber, forKey: "Requestor Contact")
            }
        }
        
        if dic_RegisterValue.isEmpty == true{
            
            AKAlertController.alert("Please Fill Form")
            return
        }
        if dic_RegisterValue["Organization Name"] as! String == ""{
            
            AKAlertController.alert("Please Enter Organization Name")
            return
        }else if dic_RegisterValue["Organization abbreviation"] as! String == ""  {
            AKAlertController.alert("Please Enter Organization abbreviation")
            return
        } else if dic_RegisterValue["Organization Type"] as! String == ""{
            AKAlertController.alert("Please Enter Organization Type")
            return
        } else if dic_RegisterValue["Description"] as! String == ""  {
            AKAlertController.alert("Please Enter Description")
            return
        }
        else if dic_RegisterValue["Address"] as! String == ""  {
            AKAlertController.alert("Please Enter Address")
            return
        }else if dic_RegisterValue["Org Register Number"] as! String == ""  {
            AKAlertController.alert("Please Enter Organization Register Number")
            return
        }
        else if dic_RegisterValue["Board Member"] as! String == ""  {
            AKAlertController.alert("Please Select Board Member")
            return
        }
            
        else if dic_RegisterValue["Requestor Name"] as! String == ""  {
            AKAlertController.alert("Please Enter Requestor Name")
            return
        }else if dic_RegisterValue["Requestor Contact"] as! String == ""  {
            AKAlertController.alert("Please Enter Requestor Contact")
            return
        }
        else if dic_RegisterValue["Org Contact"] as! String == ""  {
            AKAlertController.alert("Please Enter Organization Contact")
            return
        }
        else if phoneNumber.count > 10 {
            AKAlertController.alert("Please Enter Valid Organization Contact")
            return
        }
        else if Requestor_phoneNumber.count > 10  {
            AKAlertController.alert("Please Enter Valid Requestor Contact")
            return
        }
        else if dic_RegisterValue["Org URL"] as! String == ""  {
            AKAlertController.alert("Please Enter Organization URL")
            return
        }else{
            
            
            
            KVNProgress.show()
            Network.shared.requestOrg(requester_name: dic_RegisterValue["Requestor Name"] as! String, requester_contact: dic_RegisterValue["Requestor Contact"] as! String,org_contact_name: dic_RegisterValue["Org Contact Name"] as! String, org_contact: dic_RegisterValue["Org Contact"] as! String, org_email: dic_RegisterValue["Org Email"] as! String, name: dic_RegisterValue["Organization Name"] as! String, abbreviation: dic_RegisterValue["Organization abbreviation"] as! String, org_type: dic_RegisterValue["Organization Type"] as! String, description: dic_RegisterValue["Description"] as! String, address: dic_RegisterValue["Address"] as! String, latitude: self.lat, longitude: self.long, reg_number: dic_RegisterValue["Org Register Number"] as! String, org_url: dic_RegisterValue["Org URL"] as! String, comments: self.txtView_Comment.text ?? "", board_member:dic_RegisterValue["Board Member"] as! String , is_prayer_available:self.isprayer ) { (result) in
                
                if KVNProgress.isVisible(){
                    KVNProgress.dismiss()
                }
                
                guard let user = result else {
                    
                    return
                }
                print(user)
                
                if user.success == true{
                    
                    self.showSingleAlertMessage(message: "", subMsg:  user.message!, sender: self, completion:
                        { (success) -> Void in
                            if success == true{
                                let vc = self.storyboard?.instantiateViewController(withIdentifier: "TabVC") as! TabVC
                                vc.tabRegister = "SuperAdminRegister"
                                self.tabBarController?.tabBar.isHidden = false
                                self.slideMenuController()?.changeMainViewController(vc, close: true)
                                vc.tabBarController?.selectedIndex = 1
                                
                            }
                    })
                    
                }else{
                    self.showSingleAlertMessage(message: "", subMsg:  user.message ?? "Something Went Wrong", sender: self, completion:
                        { (success) -> Void in
                            
                    })
                }
            }
        }
    }
    func cornerRadiusShadow(){
        // corner radius
        btn_Request.layer.cornerRadius = 5
        // shadow
        btn_Request.shadowBlackToHeader()
        
        
        // shadow
        view_Header.shadowBlackToHeader()
        
        view_UpperTableView.clipsToBounds = true
        view_UpperTableView.layer.cornerRadius = 5
        view_UpperTableView.layer.maskedCorners =
            [.layerMaxXMinYCorner, .layerMinXMinYCorner]
        
        
        
        view_TableViewLastCell.layer.cornerRadius = 8
        view_TableViewLastCell.layer.maskedCorners =  [.layerMaxXMaxYCorner, .layerMinXMaxYCorner]
        
        view_TableViewLastCell.layer.shadowColor = UIColor.black.cgColor
        view_TableViewLastCell.layer.shadowOffset = CGSize(width: 0, height: 3)
        view_TableViewLastCell.layer.shadowOpacity = 0.4
        view_TableViewLastCell.layer.shadowRadius = 2.0
        
        
    }
    
}
//MARK:TablView Delegate and DataSources----------------------------------
extension RequestOrgnisationViewController: UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return array_Org.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "Request_Organisation_CellTableViewCell", for: indexPath) as! Request_Organisation_CellTableViewCell
        
        print("Request organisation",dic_RegisterValue)
        cell.txt_FillForm.tag = indexPath.row
        cell.lbl_AreyouBoardMember.text = "Are you a board member?".localized
        if cell.txt_FillForm.text == ""{
            cell.txt_FillForm.placeholder = array_Org[indexPath.row].localized
        }
        
        if indexPath.row == 2{
            cell.btn_DropDown.isHidden = false
            cell.txtView_Description.isHidden = true
        }else{
            cell.btn_DropDown.isHidden = true
            cell.txtView_Description.isHidden = true
        }
        
        if indexPath.row == 3{
            cell.txtView_Description.isHidden = false
            cell.txt_FillForm.isHidden =  true
            cell.txtView_Description.text = self.dic_RegisterValue["Description"] as? String
            
            if cell.txtView_Description.text == "Description".localized{
                cell.txtView_Description.textColor = UIColor.lightGray
                
            }else{
                cell.txtView_Description.textColor = UIColor.black
                
                
            }
            
        }else{
            cell.txtView_Description.isHidden = true
            cell.txt_FillForm.isHidden =  false
        }
        if indexPath.row == 6{
            cell.txtView_Description.isHidden = true
            cell.view_BoardMember.isHidden = false
            cell.txt_FillForm.isHidden =  true
            
            
        }else{
            //  cell.txtView_Description.isHidden = true
            cell.view_BoardMember.isHidden = true
            //cell.txt_FillForm.isHidden =  false
        }
        cell.btn_Yes_BM.addTarget(self, action: #selector(btn_YesNoBM(sender:)), for: .touchUpInside)
        cell.btn_No_BM.addTarget(self, action: #selector(btn_YesNoBM(sender:)), for: .touchUpInside)
        
        if indexPath.row == 8 || indexPath.row == 10{
            cell.view_Contry.isHidden = false
        }else{
            cell.view_Contry.isHidden = true
        }
        cell.btn_countryCodeLabel.tag = indexPath.row
        
        cell.btn_countryCodeLabel.addTarget(self, action: #selector(showCountryPickerView(_:)), for: .touchUpInside)
        
        if indexPath.row == 0{
            
            cell.txt_FillForm.text = self.dic_RegisterValue["Organization Name"] as? String
        }else if indexPath.row == 1{
            cell.txt_FillForm.text = self.dic_RegisterValue["Organization abbreviation"] as? String
        }
        else if indexPath.row == 2{
            cell.txt_FillForm.text = self.dic_RegisterValue["Organization Type"] as? String
        }
        else if indexPath.row == 3{
            cell.txt_FillForm.text = self.dic_RegisterValue["Description"] as? String
        }
            
        else if indexPath.row == 4{
            
            cell.txt_FillForm.text = self.dic_RegisterValue["Address"] as? String
        }else if indexPath.row == 5{
            cell.txt_FillForm.text = self.dic_RegisterValue["Org Register Number"] as? String
        }else if indexPath.row == 7{
            
            cell.txt_FillForm.text = self.dic_RegisterValue["Requestor Name"] as? String
        }else if indexPath.row == 8{
            
            var phoneCode = ""
            var phoneNumber = ""
            let request_number = self.dic_RegisterValue["Requestor Contact"] as? String
            if request_number != "" {
                let phone =  request_number?.components(separatedBy: "-")
                phoneCode = phone?[0] ?? "+61"
                phoneNumber = phone?[1] ?? ""
            }
            cell.txt_FillForm.text = phoneNumber
        }
        else if indexPath.row == 9{
            
            cell.txt_FillForm.text = self.dic_RegisterValue["Org Contact Name"] as? String
        }
        else if indexPath.row == 10{
            var phoneCode = ""
            var phoneNumber = ""
            let request_number = self.dic_RegisterValue["Org Contact"] as? String
            if request_number != "" {
                let phone =  request_number?.components(separatedBy: "-")
                phoneCode = phone?[0] ?? "+61"
                phoneNumber = phone?[1] ?? ""
            }
            cell.txt_FillForm.text = phoneNumber
            
        }
        else if indexPath.row == 11{
            
            cell.txt_FillForm.text = self.dic_RegisterValue["Org Email"] as? String
        }else if indexPath.row == 12{
            
            cell.txt_FillForm.text = self.dic_RegisterValue["Org URL"] as? String
        }
        
        cell.layoutIfNeeded()
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    
}
//MARK:Address map
extension RequestOrgnisationViewController: GMSAutocompleteViewControllerDelegate {
    
    // Handle the user's selection.
    func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {
        
        print("Place name: \(String(describing: place.name))")
        let index = IndexPath(row: self.tag_forAddress, section: 0)
        let cell: Request_Organisation_CellTableViewCell = self.tableview_RequestOrg.cellForRow(at: index) as! Request_Organisation_CellTableViewCell
        let location:CLLocationCoordinate2D = place.coordinate
        let latitude_str = String(format: "%.6f", location.latitude)
        let longituted_str = String(format: "%.6f", location.longitude)
        self.lat = Double(latitude_str) ?? 123.123
        self.long = Double(longituted_str) ?? 123.123
        print("Place address \(String(describing: place.formattedAddress))")
        
        dic_RegisterValue.updateValue("\(String(describing: place.formattedAddress ?? ""))", forKey: "Address")
        
        cell.txt_FillForm.text = "\(String(describing: place.formattedAddress ?? ""))"
        
        
        print("Place address:",place.formattedAddress!)
        
        
        dismiss(animated: true, completion: nil)
    }
    
    func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
        
        // TODO: handle the error.
        print("Error: ", error.localizedDescription)
    }
    
    // User canceled the operation.
    func wasCancelled(_ viewController: GMSAutocompleteViewController) {
        //  addressTextView.resignFirstResponder()
        dismiss(animated: true, completion: nil)
    }
    
    // Turn the network activity indicator on and off again.
    func didRequestAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
    }
    
    func didUpdateAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
    }
    
}
extension RequestOrgnisationViewController: MHDescDelegate{
    func getDescription(desc: String) {
        self.dic_RegisterValue["Description"] = desc
        
        dic_RegisterValue.updateValue(desc, forKey: array_Org[3])
        self.tableview_RequestOrg.reloadRows(at: [IndexPath(row: 3, section: 0)], with: UITableView.RowAnimation.none)
        
    }
}
