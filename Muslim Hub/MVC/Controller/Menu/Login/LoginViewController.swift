//
//  LoginViewController.swift
//  Muslim Hub
//
//  Created by Sakshi Singh on 23/04/19.
//  Copyright © 2019 OnGraph. All rights reserved.
//

import UIKit
import KVNProgress


class LoginViewController: UIViewController,FirstTimeLoginDelegate {
    
    
    
    @IBOutlet weak var btn_Login:UIButton!
    @IBOutlet weak var btn_ForgotPassword:UIButton!
    @IBOutlet weak var txt_Email:UITextField!
    @IBOutlet weak var txt_Password:UITextField!
    @IBOutlet weak var view_Header:UIView!
    @IBOutlet weak var lbl_HaederTitle:UILabel!
    @IBOutlet weak var lbl_OneTimeTitle:UILabel!
    var LoginToken:String!
    var firstTimeLogin:Bool!
    var normalUser:String!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
         UIView.appearance().semanticContentAttribute = .forceLeftToRight
        if #available(iOS 13.0, *) {
                 statusBarColorChange(color:UIColor.init(hex: 0x0DC9E4))
             } else {
                 UIApplication.statusBarBackgroundColor = UIColor.init(hex: 0x0DC9E4)
             }
             
        btn_Login.shadowWithCornerReadius(cornerRadius: 10)
        

        
   // self.txt_Email.text = "sakshi.singh@ongraph.ca"//"we@gmail.com"//@gmail.com"//
      // self.txt_Password.text = "123"//"give_me_a_password"
        // shadow
        
        
        view_Header.shadowBlackToHeader()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        //Langauage
        self.btn_Login.setTitle("Login".localized, for: .normal)
        self.btn_ForgotPassword.setTitle("Forgot Password?".localized, for: .normal)
        self.txt_Email.placeholder = "EmailId".localized
        self.txt_Password.placeholder = "Password".localized
        self.lbl_HaederTitle.text = "Admin Login".localized
        self.lbl_OneTimeTitle.text = "One Time Login".localized
        
        if self.firstTimeLogin == true{
            if let controller = self.storyboard?.instantiateViewController(withIdentifier: "TabVC") {
                self.slideMenuController()?.changeMainViewController(controller, close: true)
            }
        }
    }
    func isLoginFirstTime(bool: Bool) {
        self.firstTimeLogin = bool
    }
    //MARK:IBAction-----------------------------------------------------------------------
    @IBAction func btn_Back(sender:UIButton){
        if normalUser == "normalUser"{
            self.dismiss(animated: true, completion: nil)
        }else{
            if let controller = self.storyboard?.instantiateViewController(withIdentifier: "TabVC") {
                self.slideMenuController()?.changeMainViewController(controller, close: true)
            }
        }
   }
    
    @IBAction func btn_Loginn(sender:UIButton){
        
    guard !txt_Email.text!.isEmpty else {
        AKAlertController.alert("", message: "Please enter your email".localized)
            return}
    guard !txt_Password.text!.isEmpty else {
            AKAlertController.alert("", message: "Please enter your password".localized)
            return
        }
        self.LoginApis()
        
    }
    
    @IBAction func btn_Forgot(sender:UIButton){
        
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "ForgotPasswordViewController") as! ForgotPasswordViewController
        vc.modalPresentationStyle = .fullScreen
        self.present(vc, animated: true, completion: nil)
    }
    
    //MARK:Function Api's-----------------------------------------------------------
    func LoginApis(){
        
        let device_id = UserDefaults.standard.value(forKey: "deviceToken") as! String
        guard let device_token =  UserDefaults.standard.value(forKey: "device_Id") as? String else {
            return
        }
        
        KVNProgress.show()
        Network.shared.loginUser(email: txt_Email.text!,password: txt_Password.text!, device_id: device_id, device_token: device_token,phone_type:1) { (result) in
            
            KVNProgress.dismiss()
            
            print(result?.success! as Any)
            if result!.success! == true{
                AppDelegate.instance.AppAdminUser = result?.data!
                
                self.showSingleAlertMessage(message: "Login Successfull", subMsg: "", sender: self, completion:
                    { (success) -> Void in
                        if success == true{
                            
                            
                            
                            let Token = (AppDelegate.instance.AppAdminUser?.Token)
                            UserDefaults.standard.set(Token, forKey: "Login_Token")
                            UserDefaults.standard.set(AppDelegate.instance.AppAdminUser?.firstName, forKey: "FirstName")
                            UserDefaults.standard.set(AppDelegate.instance.AppAdminUser?.lastName, forKey: "LastName")
                            UserDefaults.standard.set(AppDelegate.instance.AppAdminUser?.email, forKey: "email")
                            UserDefaults.standard.set(AppDelegate.instance.AppAdminUser?.id, forKey: "LoginUserId")
                            UserDefaults.standard.set(AppDelegate.instance.AppAdminUser?.mobileNumber, forKey: "MobileNumber")
                            UserDefaults.standard.set(AppDelegate.instance.AppAdminUser?.isSuperuser, forKey: "isSuperAdmin")
                            UserDefaults.standard.set(AppDelegate.instance.AppAdminUser?.is_staff, forKey: "is_staff")
                            var array_P = [PriviligesData]()
                            if AppDelegate.instance.AppAdminUser?.privileges != nil{
                                array_P = (AppDelegate.instance.AppAdminUser?.privileges)!
                                let array_privileges = array_P.map({$0.org_id })
                                print("org Id - ",array_privileges)
                                UserDefaults.standard.set(array_privileges, forKey: "OwnOrg_Array")
                                //mark:all Org_Permission save-------
                                //
                                
                                Global.shared.priviligesData = array_P
                                
                                
                            }
                            
                            UserDefaults.standard.synchronize()
                            
                            //mark:check for is Login Person is SUPERADMIN or ADMIN
                            if AppDelegate.instance.AppAdminUser?.isSuperuser == true{     //Super Admin
                                if let controller = self.storyboard?.instantiateViewController(withIdentifier: "TabVC") {
                                    
                                    self.slideMenuController()?.changeMainViewController(controller, close: true)
                                    
                                }
                            }else{
                                
                                if AppDelegate.instance.AppAdminUser?.first_login == true{     //Admin
                                    let vc = self.storyboard?.instantiateViewController(withIdentifier: "ChangePasswordViewController") as! ChangePasswordViewController
                                    vc.isFirstLoginDelegate = self
                                    vc.chnageFromLogin = "chnageFromLogin"
                                    vc.modalPresentationStyle = .fullScreen
                                    self.present(vc, animated: true, completion: nil)
                                }else{
                                    //Normal User
                                    if self.normalUser == "normalUser"{
                                        AppDelegate.instance.sideMenu()
                                    }else{
                                        if let controller = self.storyboard?.instantiateViewController(withIdentifier: "TabVC") {
                                            self.slideMenuController()?.changeMainViewController(controller, close: true)
                                        }
                                    }
                                    
                                }
                            }
                            
                        }
                })
                
            }
            else{
                
                self.showSingleAlertMessage(message: "", subMsg: result?.message ?? "", sender: self, completion:
                    { (success) -> Void in
                        if success == true{
                            
                        }
                })
                
            }
        }
        
    }
    
    
}

extension UIView{
    
    func shadowBlackToHeader(){
        
        self.layer.shadowColor = UIColor.black.cgColor
        self.layer.shadowOffset = CGSize(width: 0, height: 3)
        self.layer.shadowOpacity = 0.7
        self.layer.shadowRadius = 4.0
    }
    func shadowWhiteToHeader(){
        
        self.layer.shadowColor = UIColor.white.cgColor
        self.layer.shadowOffset = CGSize(width: 0, height: 3)
        self.layer.shadowOpacity = 0.7
        self.layer.shadowRadius = 4.0
    }
    
    func shadowWithCornerReadius(cornerRadius:CGFloat){
        
        self.layer.cornerRadius = cornerRadius
        // shadow
        self.layer.shadowColor = UIColor.darkGray.cgColor
        self.layer.shadowOffset = CGSize(width: 0, height: 3)
        self.layer.shadowOpacity = 0.7
        self.layer.shadowRadius = 4.0
    }
    
    func cornerRadiusUpper(cornerRadius:CGFloat){
        
        self.clipsToBounds = true
        self.layer.cornerRadius = cornerRadius
        self.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner]
    }
    
    func cornerRadiusLower(cornerRadius:CGFloat){
        // shadow
        self.layer.shadowColor = UIColor.darkGray.cgColor
        self.layer.shadowOffset = CGSize(width: 0, height: 3)
        self.layer.shadowOpacity = 0.7
        self.layer.shadowRadius = 4.0
        // self.clipsToBounds = true
        self.layer.cornerRadius = cornerRadius
        self.layer.maskedCorners = [.layerMinXMaxYCorner, .layerMaxXMaxYCorner]
    }
}

