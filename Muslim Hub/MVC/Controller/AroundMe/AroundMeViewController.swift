//
//  AroundMeViewController.swift
//  Muslim Hub
//
//  Created by Sakshi Singh on 03/04/19.
//  Copyright © 2019 OnGraph. All rights reserved.
//

import UIKit
import KVNProgress
import EventKit
import SKPhotoBrowser
import TTTAttributedLabel

class AroundMeViewController: UIViewController,UITextFieldDelegate,UISearchBarDelegate {
    
    //MARK:IBOutlets----------------------
    @IBOutlet weak var tableView_AroundMeIQama:UITableView!
    @IBOutlet weak var btn_IQAMA:UIButton!
    @IBOutlet weak var btn_Classes:UIButton!
    @IBOutlet weak var searchBar:UISearchBar!
    @IBOutlet weak var btn_Search:UIButton!
    @IBOutlet weak var lbl_Nodata:UILabel!
    var orgId = Int()
    var classesModelData:GetClassModel!
    var iqamaListsAround: AroundMeIQAMALists!
    var pastList:[AroundIaqma]?
    var futureList:[AroundIaqma]?
    
    var imageSelectedView = 0
    var favbtnSelectedtag:Int!
    var btnSelected:String!
    var isfavbtnSelectedt:String!
    var lat = Double()
    var long = Double()
    var phoneNumber:String!
    var urlPhoneNumber:String!
    var array_String = [String]()
    var FalseIndex = 0
    var firstPastFalse = false
    var dispatchGroup = DispatchGroup()
    
    var pastOrFuture = "Future"
    
    @IBOutlet weak var btn_Future:UIButton!
    @IBOutlet weak var btn_Past:UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        if #available(iOS 13.0, *) {
            statusBarColorChange(color:UIColor.init(hex: 0x05BDB9))
        } else {
            UIApplication.statusBarBackgroundColor = UIColor.init(hex: 0x05BDB9)
        }
        buttoncornerRadiusShadow()
        btn_IQAMA.backgroundColor = UIColor.init(hex: 0x05BDB9)
        btn_IQAMA.shadowBlackToHeader()
        btn_Classes.shadowBlackToHeader()
        self.searchBar.isHidden = true
        self.btn_Search.isHidden = true
        self.lbl_Nodata.isHidden = true
        self.hideKeyboardWhenTappedAround()
        self.btn_IQAMA.setTitle("IQAMAH".localized, for: .normal)
        self.btn_Classes.setTitle("CLASSES".localized, for: .normal)
        
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
      AppDelegate.instance.startUpdatingLocation()
        searchBar.delegate = self
        self.iqamaAroundLists()
    
        
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        //mark:SearchBar-------------------------------------
        var searchTextField:UITextField!
        
        if #available(iOS 13, *) {
            searchTextField  = searchBar.subviews[0].subviews[2].subviews.last as? UITextField
        } else {
            searchTextField = searchBar.subviews[0].subviews.last as? UITextField
        }
        
        searchTextField.layer.cornerRadius = 15
        searchTextField.textAlignment = NSTextAlignment.left
        searchTextField.textColor = UIColor.white
        searchTextField.leftView = nil
        // searchTextField.placeholder = "Search"
        
        searchTextField.rightViewMode = UITextField.ViewMode.always
        
        if searchTextField.responds(to: #selector(getter: UITextField.attributedPlaceholder)) {
            let attributeDict = [NSAttributedString.Key.foregroundColor: UIColor.white]
            searchTextField.attributedPlaceholder = NSAttributedString(string: "Search", attributes: attributeDict)
        }
    }
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        
        self.getClassesDataApi()
    }
    
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
        if searchBar.text == "" || searchBar.text == nil{
            
            
        }else{
            self.serachOrganizationListApi()
        }
        
    }
}
extension AroundMeViewController:UITableViewDelegate,UITableViewDataSource,NotSoGoodCellDelegateOrgClasess,IndexImageSendCellDelegateClass  {
    func IndexImageSendCellDelegateClass(cell: Org_Classes_TableViewCell, selected_imgIndex: Int) {
        
        self.imageSelectedView = selected_imgIndex
        
    }
    
    
    func moreTapped(index: Int, isSelected: Bool) {
        DispatchQueue.main.async {
            // self.tableView_Home.beginUpdates()
            print("SelectedClassesCell: ", isSelected)
            self.classesModelData.getClassData?[index].isMoreTapped = isSelected
            self.tableView_AroundMeIQama.reloadData()
        }
        
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.btnSelected == "Classes"{
            switch section{
            case 0:
                return 0
            case 1:
                return self.classesModelData?.getClassData?.count ?? 0
            default:
                break
            }
            
            
        }else{
            
            switch section{
            case 0:
                if self.pastOrFuture == "Past"{
                    return self.pastList?.count ?? 0
                }else{
                    return self.futureList?.count ?? 0
                }
            //return iqamaListsAround?.data?.count ?? 0
            case 1:
                return 0
            default:
                break
            }
            
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        switch indexPath.section {
        case 0:
            let cell = tableView.dequeueReusableCell(withIdentifier: "IQAMATableViewCell", for: indexPath) as! IQAMATableViewCell
            
            //  let rawData = iqamaListsAround?.data?[indexPath.row]
            let linkAttributes: [NSAttributedString.Key : Any] = [
                NSAttributedString.Key(rawValue: NSAttributedString.Key.foregroundColor.rawValue): UIColor.gray,
                NSAttributedString.Key(rawValue: NSAttributedString.Key.underlineColor.rawValue): UIColor.lightGray,
                NSAttributedString.Key(rawValue: NSAttributedString.Key.underlineStyle.rawValue): NSUnderlineStyle.single.rawValue
            ]
            if self.pastOrFuture == "Past"{
                
                let pastData = self.pastList?[indexPath.row]
                if pastData?.past == true{
                    
                    cell.view_AroundMeIQAMA.backgroundColor = UIColor.init(hex: 0xD1D1D1)
                    cell.lbl_IqamaIn.text =  pastData?.time_left ?? ""
                    
                }else{
                    if firstPastFalse == false{
                        firstPastFalse = true
                        FalseIndex = indexPath.row
                    }
                    cell.lbl_IqamaIn.text = pastData?.time_left ?? ""
                    cell.view_AroundMeIQAMA.backgroundColor = UIColor.white
                }
                if pastData?.is_fav == true{
                    cell.btn_Heart.isSelected = true
                    cell.btn_Heart.setBackgroundImage(#imageLiteral(resourceName: "heart_selected_iPhone"), for: .normal)
                    
                }else{
                    cell.btn_Heart.isSelected = false
                    cell.btn_Heart.setBackgroundImage(#imageLiteral(resourceName: "heart_unselected_iPhone"), for: .normal)
                }
                
                //cell.img_Logo.contentMode = .scaleToFill
                DispatchQueue.main.async {
                    cell.img_Logo.tintColor = UIColor.clear
                    if let photo = pastData?.logo, let url = URL(string: photo) {
                        
                        cell.img_Logo.sd_setImage(with: url, placeholderImage: UIImage.init(named: "default_icon_iPhone_Logo.png"))
                        
                    }else{
                        
                        cell.img_Logo.image = #imageLiteral(resourceName: "default_icon_iPhone_Logo")
                    }
                }
                
                let attributeString = NSMutableAttributedString(string: pastData?.address ?? "", attributes: linkAttributes)
                cell.btn_Location.attributedText = attributeString
                
                
                
                let gesture = UITapGestureRecognizer(target: self, action: #selector(btn_Location))
                // if labelView is not set userInteractionEnabled, you must do so
                cell.btn_Location.isUserInteractionEnabled = true
                cell.btn_Location.addGestureRecognizer(gesture)
                cell.btn_Title.setTitle(pastData?.abbreviation , for: .normal)
                cell.lbl_Description.text = pastData?.name ?? ""
                let distance = pastData?.distance ?? 0.0
                cell.lbl_Away.text = "\(distance)" + " km"
                if pastData?.time_left == "0 hr 5 min" || pastData?.time_left == "0 hr 4 min" || pastData?.time_left == "0 hr 3 min" || pastData?.time_left == "0 hr 2 min" ||
                    pastData?.time_left == "0 hr 1 min" || pastData?.time_left == "0 hr 0 min"{
                    cell.lbl_IqamaIn.textColor = UIColor.red
                }else{
                    cell.lbl_IqamaIn.textColor = UIColor.black
                }
                
                
                if pastData?.prayer_type == "jumma"{
                    cell.lbl_MainprayerType.text = "Jumma in".localized
                    cell.lbl_prayerType.text = "Jumma" + " \(pastData?.type ?? 0)"
                }else{
                      
                    cell.lbl_MainprayerType.text = "Iqamah ago".localized
                    if pastData?.prayerCode == "F"{
                        cell.lbl_prayerType.text = "FAJR"
                    }else if pastData?.prayerCode == "Z"{
                        cell.lbl_prayerType.text = "ZUHR"
                    } else if pastData?.prayerCode == "A"{
                        cell.lbl_prayerType.text = "ASR"
                    } else if pastData?.prayerCode == "M"{
                        cell.lbl_prayerType.text = "MAGHRIB"
                    }else if pastData?.prayerCode == "I"{
                        cell.lbl_prayerType.text = "ISHA"
                    }
                    
                }
                
                let timedateFormatter = DateFormatter()
                timedateFormatter.dateFormat = "HH:mm"
                let starttime = timedateFormatter.date(from: pastData?.time ?? "")
                let current = timedateFormatter.string(from: Date())
                let currentTime = timedateFormatter.date(from: current)
                
                
                let timedateFormatter1 = DateFormatter()
                timedateFormatter1.dateFormat = "hh:mm a"
                let startTime1 = timedateFormatter1.string(from: starttime!)
                
                cell.lbl_Time.text = startTime1
                cell.lbl_Time.backgroundColor = UIColor.white
                
                
            }else{
                
                let futureData = futureList?[indexPath.row]
                if  futureData?.past == true{
                    
                    cell.view_AroundMeIQAMA.backgroundColor = UIColor.init(hex: 0xD1D1D1)
                    cell.lbl_IqamaIn.text =  futureData?.time_left ?? ""
                }else{
                    if firstPastFalse == false{
                        firstPastFalse = true
                        FalseIndex = indexPath.row
                    }
                    cell.view_AroundMeIQAMA.backgroundColor = UIColor.white
                    cell.lbl_IqamaIn.text = futureData?.time_left ?? ""
                }
                
                if futureData?.is_fav == true{
                    cell.btn_Heart.isSelected = true
                    cell.btn_Heart.setBackgroundImage(#imageLiteral(resourceName: "heart_selected_iPhone"), for: .normal)
                    
                }else{
                    cell.btn_Heart.isSelected = false
                    cell.btn_Heart.setBackgroundImage(#imageLiteral(resourceName: "heart_unselected_iPhone"), for: .normal)
                }
                
                
                //cell.img_Logo.contentMode = .scaleToFill
                DispatchQueue.main.async {
                    cell.img_Logo.tintColor = UIColor.clear
                    if let photo = futureData?.logo, let url = URL(string: photo) {
                        
                        cell.img_Logo.sd_setImage(with: url, placeholderImage: UIImage.init(named: "default_icon_iPhone_Logo.png"))
                        
                    }else{
                        
                        cell.img_Logo.image = #imageLiteral(resourceName: "default_icon_iPhone_Logo")
                    }
                }
                let attributeString = NSMutableAttributedString(string: futureData?.address ?? "", attributes: linkAttributes)
                cell.btn_Location.attributedText = attributeString
                
                let gesture = UITapGestureRecognizer(target: self, action: #selector(btn_Location))
                // if labelView is not set userInteractionEnabled, you must do so
                cell.btn_Location.isUserInteractionEnabled = true
                cell.btn_Location.addGestureRecognizer(gesture)
                cell.btn_Title.setTitle(futureData?.abbreviation , for: .normal)
                cell.lbl_Description.text = futureData?.name ?? ""
                let distance = futureData?.distance ?? 0.0
                cell.lbl_Away.text = "\(distance)" + " km"
                if futureData?.time_left == "0 hr 5 min" || futureData?.time_left == "0 hr 4 min" || futureData?.time_left == "0 hr 3 min" || futureData?.time_left == "0 hr 2 min" ||
                    futureData?.time_left == "0 hr 1 min" || futureData?.time_left == "0 hr 0 min"{
                    cell.lbl_IqamaIn.textColor = UIColor.red
                }else{
                    cell.lbl_IqamaIn.textColor = UIColor.black
                }
                
                
                if futureData?.prayer_type == "jumma"{
                    cell.lbl_MainprayerType.text = "Jumma in".localized
                    cell.lbl_prayerType.text = "Jumma" + " \(futureData?.type ?? 0)"
                }else{
                    cell.lbl_MainprayerType.text = "Iqamah in".localized
                    if futureData?.prayerCode == "F"{
                        cell.lbl_prayerType.text = "FAJR"
                    }else if futureData?.prayerCode == "Z"{
                        cell.lbl_prayerType.text = "ZUHR"
                    } else if futureData?.prayerCode == "A"{
                        cell.lbl_prayerType.text = "ASR"
                    } else if futureData?.prayerCode == "M"{
                        cell.lbl_prayerType.text = "MAGHRIB"
                    }else if futureData?.prayerCode == "I"{
                        cell.lbl_prayerType.text = "ISHA"
                    }
                    
                }
                
                
                let timedateFormatter = DateFormatter()
                timedateFormatter.dateFormat = "HH:mm"
                let starttime = timedateFormatter.date(from: futureData?.time ?? "")
                let current = timedateFormatter.string(from: Date())
                let currentTime = timedateFormatter.date(from: current)
                
                
                let timedateFormatter1 = DateFormatter()
                timedateFormatter1.dateFormat = "hh:mm a"
                let startTime1 = timedateFormatter1.string(from: starttime!)
                
                cell.lbl_Time.text = startTime1
                cell.lbl_Time.backgroundColor = UIColor.white
                
                
            }
          
            cell.btn_Heart.addTarget(self, action: #selector(btn_favUnfav(sender:)), for: .touchUpInside)
            //adds shadow to the layer of cell
            cell.backgroundColor = UIColor.clear
            cell.view_Header.cornerRadiusUpper(cornerRadius: 4)
            cell.view_AroundMeIQAMA.cornerRadiusLower(cornerRadius: 8)
            cell.btn_Heart.tintColor = UIColor.white
            cell.btn_Location.tag = indexPath.row
            cell.btn_Heart.tag = indexPath.row
            
            cell.btn_Heart.addTarget(self, action: #selector(btn_favUnfav(sender:)), for: .touchUpInside)
             
            return cell
        case 1:
            let cell = tableView.dequeueReusableCell(withIdentifier: "Org_Classes_TableViewCell", for: indexPath) as! Org_Classes_TableViewCell
            cell.btn_Location.tag = indexPath.row
            cell.btnShare.isHidden = true
            cell.btnReminder.isHidden = true
            if let urlstr = self.classesModelData?.getClassData?[indexPath.row].classes_logo{
                cell.img_Org.sd_setImage(with: URL(string: urlstr), placeholderImage: #imageLiteral(resourceName: "default_icon_iPhone"))
            }else{
                cell.img_Org.image = #imageLiteral(resourceName: "default_icon_iPhone")
            }
            cell.configureCell(model: (self.classesModelData?.getClassData?[indexPath.row])!)
            cell.IndexImageSendCellDelegateClass = self
            cell.img_Viewannouncement.tag = indexPath.row
            let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(imageTapped(tapGestureRecognizer:)))
            
            
            let linkAttributes: [NSAttributedString.Key : Any] = [
                NSAttributedString.Key(rawValue: NSAttributedString.Key.foregroundColor.rawValue): UIColor.black,
                NSAttributedString.Key(rawValue: NSAttributedString.Key.underlineColor.rawValue): UIColor.black,
                NSAttributedString.Key(rawValue: NSAttributedString.Key.underlineStyle.rawValue): NSUnderlineStyle.single.rawValue
            ]
            
            let attributedString = NSMutableAttributedString(string: self.classesModelData?.getClassData?[indexPath.row].address ?? "", attributes: linkAttributes)
            
            attributedString.addAttribute(NSAttributedString.Key.underlineStyle, value: NSUnderlineStyle.single.rawValue, range: NSRange(location: 0, length: (self.classesModelData?.getClassData?[indexPath.row].address!.count)!))
            
            
            cell.btn_Location.setAttributedTitle(attributedString, for: .normal)
            
            cell.lblDesc.text = self.classesModelData?.getClassData?[indexPath.row].description ?? ""
            
            //      //mark:number deduction an d url
            cell.lblDesc.enabledTextCheckingTypes = NSTextCheckingResult.CheckingType.link.rawValue
            cell.lblDesc.delegate = self
            
            self.phoneNumberDetctor(input:  cell.lbl_ReadMore.text!)
            self.uRLDetctor(input:  cell.lblDesc.text as! String)
            
            let newText =  cell.lblDesc.text as! NSString
            
            if self.array_String.count != 0{
                for i in 0...self.array_String.count - 1{
                    let range:NSRange = newText.range(of: array_String[i])
                    cell.lblDesc.addLink(to: URL(string: array_String[i]), with: range)
                }
            }
            let value =  self.classesModelData?.getClassData?[indexPath.row].attendees_type ?? 0
            if value == 1{
                cell.lblGender.text = "Men Only"
            }else if value == 2 {
                cell.lblGender.text = "Women Only"
            }else if value == 3 {
                cell.lblGender.text = "Family"
            }
            
            
            let countLabl : Int = cell.lblDesc.calculateMaxLines()
            if countLabl == 1{
                cell.buttonMore.setTitle(nil, for: .normal)
                cell.buttonMore.setImage(nil, for: .normal)
                cell.imgContraint_ButtonMoreHeight.constant = 0.0
                cell.buttonMore.isUserInteractionEnabled = false
            }else{
                //            let model = self.arrayEventModel[indexPath.row]
                if (self.classesModelData?.getClassData?[indexPath.row])?.isMoreTapped == true{
                    cell.buttonMore.setTitle("Show less".localized, for: .normal)
                    cell.lblDesc.numberOfLines = 0
                }else{
                    cell.buttonMore.setTitle("Show more".localized, for: .normal)
                    cell.lblDesc.numberOfLines = 1
                }
                
                cell.buttonMore.setImage(#imageLiteral(resourceName: "down_black_arrow_iPhone"), for: .normal)
                cell.imgContraint_ButtonMoreHeight.constant = 39.5
                cell.buttonMore.isUserInteractionEnabled = true
            }
            cell.img_Viewannouncement.isUserInteractionEnabled = true
            cell.img_Viewannouncement.addGestureRecognizer(tapGestureRecognizer)
            cell.buttonMore.tag = indexPath.row
            
            
            
            cell.btn_Location.addTarget(self, action: #selector(btn_ClassLocation(sender:)), for: .touchUpInside)
            cell.lbl_OrgName.text = self.classesModelData?.getClassData?[indexPath.row].org_name
            
            cell.btnReminder.addTarget(self, action: #selector(btn_EventRemainderTapped(sender:)), for: .touchUpInside)
            
            
            //adds shadow to the layer of cell
            
            cell.view_Announcment.layer.cornerRadius = 10.0
            cell.view_Announcment.shadowBlackToHeader()
            
            //makes the cell round
            let containerView = cell.contentView
            containerView.layer.cornerRadius = 2
            containerView.clipsToBounds = true
            
            let now = Date()
            let date = NSDate(timeIntervalSince1970: Double(self.classesModelData?.getClassData?[indexPath.row].added_at ?? 0))
            cell.lblClassesDay.text = now.timeAgoDisplay(dateCreated:date as Date,timeStamp:self.classesModelData?.getClassData?[indexPath.row].added_at ?? 0)
            
            cell.lblAge.text = self.classesModelData?.getClassData?[indexPath.row].age_group ?? ""
            cell.btnShare.addTarget(self, action: #selector(btn_shareTextButton(sender:)), for: .touchUpInside)
            
            
            
            
            let cost = self.classesModelData?.getClassData?[indexPath.row].free ?? 0
            if cost == 1{
                cell.lbl_Cost.text = "Free"
            }else{
                cell.lbl_Cost.text = "Paid"
            }
            
            
            //   let select_Class = self.classesModelData?.getClassData?[indexPath.row].select_class ?? 0
            
            
            // let user_Input = self.classesModelData?.getClassData?[indexPath.row].user_input ?? ""
            //            if self.classesModelData?.getClassData?[indexPath.row].class_input_type == 1 {
            //                if self.classesModelData?.getClassData?[indexPath.row].select_class == 1{
            //                    cell.btnQuran.setTitle("Quran", for: .normal)
            //                }else{
            //                    cell.btnQuran.setTitle("Kids Quran", for: .normal)
            //                }
            //            }else{
            //                cell.btnQuran.setTitle(user_Input, for: .normal)
            //            }
            //
            
            if self.classesModelData?.getClassData?[indexPath.row].repeating_day ?? 0 == 1{
                cell.lblMonthType.text = "Weekly"
            }else if self.classesModelData?.getClassData?[indexPath.row].repeating_day ?? 0 == 2{
                cell.lblMonthType.text = "Fortnightly"
            }else if self.classesModelData?.getClassData?[indexPath.row].repeating_day ?? 0 == 3{
                cell.lblMonthType.text = "Monthly"
            }else{
                cell.lblMonthType.text = ""
            }
            
            
            cell.registerCell()
            cell.delegateClasses = self
            //cell.collection_Images.reloadData()
            
            
            cell.btnShare.addTarget(self, action: #selector(btn_shareTextButton(sender:)), for: .touchUpInside)
            
            
            return cell
            
            
            
        default:
            break
        }
        
        
        return UITableViewCell()
        
    }
    
    //MARK:IBACtion-----------------
    
    @IBAction func btn_IQAMA_Classes(sender:UIButton){
        
        if sender.tag == 0{
            self.btn_Search.isHidden = true
            self.searchBar.isHidden = true
            self.btn_Past.isHidden = false
            self.btn_Future.isHidden = false
            btn_IQAMA.backgroundColor = UIColor.init(hex: 0x0DC9E4)
            btn_Classes.backgroundColor = UIColor.white
            btn_IQAMA.setTitleColor(UIColor.white, for: .normal)
            btn_Classes.setTitleColor(UIColor.darkGray, for: .normal)
            self.btnSelected = "IQAMA"
            self.iqamaAroundLists()
            //            self.dispatchGroup.notify(queue: .main) {
            //                if self.firstPastFalse == true{
            //                    let indexPath = IndexPath(row: self.FalseIndex, section: 0)
            //                    self.tableView_AroundMeIQama.scrollToRow(at: indexPath, at: .top, animated: true)
            //
            //                }
            //            }
        }else{
            self.searchBar.isHidden = true
            self.btn_Search.isHidden = false
            self.btn_Past.isHidden = true
            self.btn_Future.isHidden = true
            btn_Classes.backgroundColor = UIColor.init(hex: 0x0DC9E4)
            btn_IQAMA.backgroundColor = UIColor.white
            btn_Classes.setTitleColor(UIColor.white, for: .normal)
            btn_IQAMA.setTitleColor(UIColor.darkGray, for: .normal)
            self.btnSelected = "Classes"
            self.getClassesDataApi()
        }
        
        UIView.transition(with: tableView_AroundMeIQama,
                          duration: 0.35,
                          options: .transitionCrossDissolve,
                          animations: { self.tableView_AroundMeIQama.reloadData() }) // left out the unnecessary syntax in the completion block and the optional completion parameter
    }
    @IBAction func btn_past(sender:UIButton){
        self.iqamaAroundLists()
        self.pastOrFuture = "Past"
        
        btn_Future.backgroundColor = UIColor.white
        btn_Past.backgroundColor = UIColor.init(hex: 0x0DC9E4)
        
        btn_Past.setTitleColor(UIColor.white, for: .normal)
        btn_Future.setTitleColor(UIColor.darkGray, for: .normal)
        
        tableView_AroundMeIQama.reloadData()
    }
    func buttoncornerRadiusShadow (){
        
        btn_Future.cornerRadiusLower(cornerRadius: 8)
        btn_Future.clipsToBounds = true
        btn_Future.layer.cornerRadius = 8
        btn_Future.layer.borderColor = UIColor.init(hex: 0x0DC9E4).cgColor
        btn_Future.layer.borderWidth = 1.0
        btn_Future.layer.maskedCorners = [.layerMinXMaxYCorner, .layerMinXMinYCorner]
        
        btn_Past.clipsToBounds = true
        btn_Past.layer.cornerRadius = 8
        btn_Past.layer.maskedCorners = [.layerMaxXMaxYCorner, .layerMaxXMinYCorner]
        btn_Past.layer.borderColor = UIColor.init(hex: 0x0DC9E4).cgColor
        btn_Past.layer.borderWidth = 1.0
        // shadow
        
    }
    @IBAction func btn_future(sender:UIButton){
        self.iqamaAroundLists()
        self.pastOrFuture = "Future"
        
        
        
        btn_Past.backgroundColor = UIColor.white
        btn_Future.backgroundColor = UIColor.init(hex: 0x0DC9E4)
        btn_Future.setTitleColor(UIColor.white, for: .normal)
        btn_Past.setTitleColor(UIColor.darkGray, for: .normal)
        
        tableView_AroundMeIQama.reloadData()
    }
    @objc func btn_favUnfav(sender:UIButton){
        if self.pastOrFuture == "Past"{
            if sender.isSelected {
                sender.isSelected = false
                self.pastList?[sender.tag].is_fav = false
                self.favbtnSelectedtag = self.pastList?[sender.tag].org
                isfavbtnSelectedt = "false"
            }else{
                sender.isSelected = true
                self.pastList?[sender.tag].is_fav = true
                self.favbtnSelectedtag = self.pastList?[sender.tag].org
                
                isfavbtnSelectedt = "true"
            }
        }else{
            if sender.isSelected {
                sender.isSelected = false
                self.futureList?[sender.tag].is_fav = false
                self.favbtnSelectedtag = self.futureList?[sender.tag].org
                isfavbtnSelectedt = "false"
            }else{
                sender.isSelected = true
                self.futureList?[sender.tag].is_fav = true
                self.favbtnSelectedtag = self.futureList?[sender.tag].org
                
                isfavbtnSelectedt = "true"
            }
        }
        
        
        DispatchQueue.global(qos: .background).async {
            self.favUnFavApi()
            DispatchQueue.main.async {
                let indexpath = IndexPath(row: sender.tag, section: 0)
                self.tableView_AroundMeIQama.reloadRows(at: [indexpath], with: .none)
                
            }
        }
    }
    
    //MARK:Function get Classes Data Api----------------------------------------------
    func getClassesDataApi(){
        
        
        AppDelegate.instance.checkLocationPermission { (currentLat, currentLong) in
            KVNProgress.show()
            Network.shared.getClassesAroundMeLists(lat:AppDelegate.instance.currentLat.rounded(toPlaces: 6),long:AppDelegate.instance.currentLong.rounded(toPlaces: 6)){ (result) in
                KVNProgress.dismiss()
                guard let data = result else {return}
                self.classesModelData = data
                
                if self.classesModelData.getClassData?.count ?? 0 > 0{
                    for i in 0...self.classesModelData.getClassData!.count - 1{
                        var arraySelectedImage: [Bool] = []
                        if self.classesModelData.getClassData![i].images!.count > 0{
                            for j in 0...self.classesModelData.getClassData![i].images!.count - 1{
                                if j == 0{
                                    arraySelectedImage.append(true)
                                }else{
                                    arraySelectedImage.append(false)
                                }
                            }
                        }
                        self.classesModelData.getClassData![i].selectedImage_Array = arraySelectedImage
                        self.classesModelData.getClassData![i].isMoreTapped = false
                    }
                    
                }else{
                    self.lbl_Nodata.isHidden = false
                    // AKAlertController.alert("No classes found near you!")
                }
                self.tableView_AroundMeIQama.reloadData()
                
            } }
        
    }
    func serachOrganizationListApi(){
        
        AppDelegate.instance.checkLocationPermission { (currentLat, currentLong) in
            KVNProgress.show()
            Network.shared.getListOfSearchAroundMe(searchKey: self.searchBar.text ?? "",lat:AppDelegate.instance.currentLat ,long:AppDelegate.instance.currentLong) { (result) in
                KVNProgress.dismiss()
                guard let user = result else {
                    
                    return
                }
                
                self.classesModelData  = user
                self.tableView_AroundMeIQama.reloadData()
            }
            
        }
        
        
    }
    
    //mark:IQAMALists---------------------------------------
    func iqamaAroundLists(){
           AppDelegate.instance.startUpdatingLocation()
        KVNProgress.show()
      //  self.dispatchGroup.enter()
          
        Network.shared.aroundIqamaListApi { (result) in
            KVNProgress.dismiss()
            guard let user = result else{
                
                return
            }
            
            self.iqamaListsAround = user
            
            if user.data?.count == 0{
                self.lbl_Nodata.isHidden = false
                //  AKAlertController.alert("No Iqama found near you!")
            }else{
                self.lbl_Nodata.isHidden = true
                
              //  self.dispatchGroup.leave()
              
                self.pastList = self.iqamaListsAround.data?.filter{($0.past!) == true}
                print("Pastlist:-",self.pastList!)
                self.futureList = self.iqamaListsAround.data?.filter{($0.past!) == false}
                print("Futurelist:-",self.futureList!)
                self.tableView_AroundMeIQama.reloadData()
            }
            
            
        }
        
    }
    
    
    func favUnFavApi(){
        
        Network.shared.favUnFav(org_id:  self.favbtnSelectedtag ?? 0, is_fav: isfavbtnSelectedt) { (result) in
            
            guard result != nil else {
                
                return
            }
            
            
        }
    }
    
    //MARK:IBACtions
    @objc func btn_ClassLocation(sender: UIButton){
        self.lat = self.classesModelData.getClassData?[sender.tag].latitude ?? 123.123
        self.long = self.classesModelData.getClassData?[sender.tag].longitude ?? 123.123
        
        self.location(lat: lat, long: long, title: "")
    }
    
    @objc func btn_Location(sender: UIGestureRecognizer){
        let lat = iqamaListsAround.data?[sender.view!.tag].latitude ?? 123.123
        let long = iqamaListsAround.data?[sender.view!.tag].longitude ?? 123.123
        
        self.location(lat: lat, long: long, title: "")
    }
    
    @IBAction func btn_Search(sender:UIButton){
        
        if sender.isSelected {
            sender.isSelected = false
            self.searchBar.isHidden = false
        }else{
            sender.isSelected = true
            self.searchBar.isHidden = true
        }
        
    }
    
    
    @objc func btn_EventRemainderTapped(sender: UIButton){
        
        self.eventMethod(indexPath:sender.tag)
    }
    
    @objc func btn_shareTextButton(sender: UIButton) {
        
        // text to share
        let text = "This is some text that I want to share."
        
        // set up activity view controller
        let textToShare = [ text ]
        let activityViewController = UIActivityViewController(activityItems: textToShare, applicationActivities: nil)
        activityViewController.popoverPresentationController?.sourceView = self.view // so that iPads won't crash
        
        // exclude some activity types from the list (optional)
        activityViewController.excludedActivityTypes = [ UIActivity.ActivityType.airDrop, UIActivity.ActivityType.postToFacebook ]
        
        // present the view controller
        self.present(activityViewController, animated: true, completion: nil)
        
    }
    
    @objc func imageTapped(tapGestureRecognizer: UITapGestureRecognizer)
    {
        _ = tapGestureRecognizer.view as! UIImageView
        
        SKPhotoBrowserOptions.displayAction = false
        var images = [SKPhoto]()
        
        let classData = self.classesModelData?.getClassData?[(tapGestureRecognizer.view?.tag)!].images
        for i in 0..<classData!.count{
            
            let photo = SKPhoto.photoWithImageURL(classData![i])
            photo.shouldCachePhotoURLImage = false // you can use image cache by true(NSCache)
            images.append(photo)
        }
        
        
        let browser = SKPhotoBrowser(originImage: #imageLiteral(resourceName: "default_header_iPhone"), photos: images, animatedFromView: self.view)
        for _ in 0..<classData!.count{
            browser.initializePageIndex(imageSelectedView)
        }
        present(browser, animated: true, completion: {})
        
    }
    
    
    //MARK:Set Reminder method-----------------------
    func eventMethod(indexPath:Int){
        let eventStore = EKEventStore()
        eventStore.requestAccess(
            to: EKEntityType.event, completion: {(granted, error) in
                if !granted {
                    DispatchQueue.main.sync {
                        AKAlertController.alert("The app is not permitted to access reminders, make sure to grant permission in the settings and try again")
                        return}
                    //print("Access to store not granted")
                    //   print(error?.localizedDescription as Any)
                } else {
                    print("Access granted")
                    self.createReminder(in: eventStore, reminderText: "",indexPath:indexPath)
                }
        })
    }
    func createReminder(in eventStore: EKEventStore, reminderText: String,indexPath:Int) {
        
        let reminder = EKEvent(eventStore: eventStore)
        
        let startTime = self.classesModelData?.getClassData?[indexPath].start_time ?? ""
        let dateOrg = ((self.classesModelData?.getClassData?[indexPath].start_date ?? "") + " " + startTime)
        let endDateOrg = self.classesModelData?.getClassData?[indexPath].end_date ?? ""
        
        reminder.title = (self.classesModelData?.getClassData?[indexPath].title)! + " \(self.classesModelData?.getClassData?[indexPath].start_date ?? "") To \(endDateOrg)"
        
        
        
        reminder.calendar =
            eventStore.defaultCalendarForNewEvents
        let date = Date()
        let alarm = EKAlarm(absoluteDate: date)
        reminder.addAlarm(alarm)
        
        let earlierDate = date.addingTimeInterval(-3600*24)
        let earlierAlarm = EKAlarm(absoluteDate: earlierDate)
        reminder.addAlarm(earlierAlarm)
        
        reminder.startDate = date.addingTimeInterval(-24 * 60 * 60)
        reminder.endDate = date.addingTimeInterval(-24 * 60 * 60)
        
        do {
            try eventStore.save(reminder, span: .thisEvent, commit: true)
        } catch let error  {
            print("Reminder failed with error \(error.localizedDescription)")
            return
        }
        
        
        DispatchQueue.main.sync {
            // Create the alert controller
            let alertController = UIAlertController(title: "Reminder Created Successfully", message: "", preferredStyle: .alert)
            
            // Create the actions
            let okAction = UIAlertAction(title: "OK", style: UIAlertAction.Style.default) {
                UIAlertAction in
                NSLog("OK Pressed")
                //            reminderText = ""
                //            self.activityIndicator.stopAnimating()
            }
            
            // Add the actions
            alertController.addAction(okAction)
            
            // Present the controller
            self.present(alertController, animated: true, completion: nil)
        }
        
        
    }
    
}
extension AroundMeViewController:TTTAttributedLabelDelegate{
    
    func phoneNumberDetctor(input:String){
        
        let detector = try! NSDataDetector(types: NSTextCheckingResult.CheckingType.phoneNumber.rawValue)
        let matches = detector.matches(in: input, options: [], range: NSRange(location: 0, length: input.utf16.count))
        
        for match in matches {
            guard let range = Range(match.range, in: input) else { continue }
            let url = input[range]
            print(url)
            array_String.append(String(url))
          
        }
        
        
    }
    func uRLDetctor(input:String){
        
        
        let detector = try! NSDataDetector(types: NSTextCheckingResult.CheckingType.link.rawValue)
        let matches = detector.matches(in: input, options: [], range: NSRange(location: 0, length: input.utf16.count))
        
        for match in matches {
            guard let range = Range(match.range, in: input) else { continue }
            let url = input[range]
            print(url)
            
            array_String.append(String(url))
            
            
            
        }
        
        
    }
    
    
    func phoneAndUrlDetctor(tap_string:String){
        
        //        let tap_string = array_String[view.tag]
        //
        //
        let detector = try! NSDataDetector(types: NSTextCheckingResult.CheckingType.phoneNumber.rawValue)
        let matches = detector.matches(in: tap_string, options: [], range: NSRange(location: 0, length: tap_string.utf16.count))
        
        for match in matches {
            guard let range = Range(match.range, in: tap_string) else { continue }
            let url = tap_string[range]
            print(url)
            
            self.phoneNumber = "true"
            
        }
        
        let detectorUrl = try! NSDataDetector(types: NSTextCheckingResult.CheckingType.link.rawValue)
        let matchesurl = detectorUrl.matches(in: tap_string, options: [], range: NSRange(location: 0, length: tap_string.utf16.count))
        
        for match in matchesurl {
            guard let range = Range(match.range, in: tap_string) else { continue }
            let url = tap_string[range]
            print(url)
            
            self.phoneNumber = "false"
            
            
            
        }
        
        if self.phoneNumber == "true"{
            
            if let url = URL(string:"tel://\(tap_string )"), UIApplication.shared.canOpenURL(url) {
                UIApplication.shared.openURL(url)
            }
        }else if self.phoneNumber == "false"{
            
            let urlString = ("\(tap_string )")
            var urll = String()
            
            
            if urlString.contains("https://") {
                urll = ("\(tap_string)")
            }else if urlString.contains("http://") {
                urll = ("\(tap_string)")
            }else{
                urll = ("http://\(tap_string)")
            }
            
            
            guard let url = URL(string: urll) else { return }
            UIApplication.shared.openURL(url)
        }
    }
    
    
    func attributedLabel(_ label: TTTAttributedLabel!, didSelectLinkWith url: URL!) {
        debugPrint("label>", label)
        
        debugPrint("addressComponents>", url)
        
        self.phoneAndUrlDetctor(tap_string: String(describing: url!) )
    }
    
    
}
