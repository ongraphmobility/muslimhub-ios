//
//  ManageAdminDeatilsTableViewCell.swift
//  Muslim Hub
//
//  Created by Sakshi Singh on 24/04/19.
//  Copyright © 2019 OnGraph. All rights reserved.
//

import UIKit

class ManageAdminDeatilsTableViewCell: UITableViewCell {
    @IBOutlet weak var lbl_NAme:UILabel!
    @IBOutlet weak var lbl_EmailId:UILabel!
    @IBOutlet weak var lbl_PhoneNo:UILabel!
    @IBOutlet weak var btn_Delete:UIButton!
    @IBOutlet weak var btn_Edit:UIButton!
    @IBOutlet weak var btn_Logo:UIImageView!
    @IBOutlet weak var lbl_AdminType:UILabel!
    
    @IBOutlet weak var lbl_NAmeTitle:UILabel!
    @IBOutlet weak var lbl_EmailIdTitle:UILabel!
    @IBOutlet weak var lbl_PhoneNoTitle:UILabel!
    @IBOutlet weak var lbl_AdminTypeTitle:UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
