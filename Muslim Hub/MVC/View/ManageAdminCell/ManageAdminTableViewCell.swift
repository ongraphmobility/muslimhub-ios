//
//  ManageAdminTableViewCell.swift
//  Muslim Hub
//
//  Created by Sakshi Singh on 24/04/19.
//  Copyright © 2019 OnGraph. All rights reserved.
//

import UIKit

class ManageAdminTableViewCell: UITableViewCell {

    @IBOutlet weak var lbl_Details:UILabel!
    @IBOutlet weak var lbl_Title:UILabel!
    @IBOutlet weak var view_Main:UIView!
    @IBOutlet weak var view_ThinLine:UIView!
    @IBOutlet weak var btn_Location:UIButton!
    @IBOutlet weak var img_Logo:UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
