//
//  MHAnnouncementListsTableViewCell.swift
//  Muslim Hub
//
//  Created by Rohit on 13/05/19.
//  Copyright © 2019 OnGraph. All rights reserved.
//



import UIKit
import KVNProgress
import SDWebImage
import TTTAttributedLabel


protocol ShowMoreCellDelegate {
    func moreTapped(cell: MHAnnouncementListsTableViewCell)
}
protocol IndexImageSendCellDelegateAnnouncement {
    func IndexImageSendCellDelegateAnnouncement(cell: MHAnnouncementListsTableViewCell,selected_imgIndex:Int)
}

class MHAnnouncementListsTableViewCell: UITableViewCell {
    //MARK:IBOutlets----------------------
    @IBOutlet weak var lbl_ReadMore:TTTAttributedLabel!
    @IBOutlet weak var view_Announcment:UIView!
    @IBOutlet weak var img_Viewannouncement:UIImageView!
    @IBOutlet weak var btn_Delete:UIButton!
    @IBOutlet weak var btn_Edit:UIButton!
    @IBOutlet weak var lbl_Title:UILabel!
    @IBOutlet weak var lbl_Time:UILabel!
    @IBOutlet weak var btn_Share:UIButton!
    @IBOutlet weak var collection_Images:UICollectionView!
    @IBOutlet weak var imgContraint_Height:NSLayoutConstraint!
    @IBOutlet weak var imgContraint_ButtonMoreHeight:NSLayoutConstraint!
    @IBOutlet var lblReadMore_Height:NSLayoutConstraint!
    var descheight = 20.0
    var selectImage: Int? = 0
    var isSelectedImg:Bool?
    @IBOutlet weak var buttonMore: UIButton!
    var allData:AnnouncementData?
    var delegate: ShowMoreCellDelegate?
    var IndexImageSendCellDelegateAnnouncement : IndexImageSendCellDelegateAnnouncement?
    var indexTable = Int()
    var indexTablecell = UITableView()
    
    var isExpanded: Bool = false
    var announcementLists = [AnnouncementData]()
    var imageData = [String]()
    var selectedIndexPath:Int!
    
    var img_array = [String?]()
    var selectedIndecx_Array : [Bool] = []
    
    @IBOutlet weak var hConstCollectionView: NSLayoutConstraint!
    @IBOutlet weak var hConstImgView: NSLayoutConstraint!
    
    func announcementListApi(){
        KVNProgress.show()
        Network.shared.announcementLists() { (result) in
            KVNProgress.dismiss()
            guard let user = result else {
                
                return
            }
            
            self.announcementLists = user
        
            self.collection_Images.reloadData()
        }
    }
    
    @IBAction func btnMoreTapped(_ sender: Any) {
        
        if sender is UIButton {
            isExpanded = !isExpanded
         
            lbl_ReadMore.numberOfLines = isExpanded ? 0 : 1
            

            if isExpanded == true{
                
                lblReadMore_Height.isActive = false
                
                lbl_ReadMore.sizeToFit()
            }else{
                 lblReadMore_Height.isActive = true
                lblReadMore_Height.constant = 20.0
               lbl_ReadMore.sizeToFit()
            }
            
            buttonMore.setTitle(isExpanded ? "Show less".localized : "Show more".localized, for: .normal)
     
            
            delegate?.moreTapped(cell: self)
        }
        
    }
    
    public func myInit(theTitle: String, theBody: String) {
     
        isExpanded = false
        
        // labelTitle.text = theTitle
        lbl_ReadMore.text = theBody
    
        
        lbl_ReadMore.numberOfLines = 1
        
    }
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    func setupImageCollection(data : AnnouncementData , index : Int,tableViewObject:UITableView) {
        
        self.indexTable = index
        self.indexTablecell = tableViewObject
        self.allData = data

    
      
        if let images = data.images{
            self.img_array = images
            self.collection_Images.reloadData()
        }
        
        self.selectedIndecx_Array = self.allData?.selectedImage_Array! ?? []
        
//
//        for i in 0...self.img_array.count - 1{
//            if i == 0{
//                self.selectedIndecx_Array.append(true)
//            }else{
//                self.selectedIndecx_Array.append(false)
//            }
//        }
        
        
    }
    
    func reloadTable(index: Int , table : UITableView){
        let index = IndexPath(row: index, section: 0)
        table.reloadRows(at: [index], with: .none)
    }
    
}
extension MHAnnouncementListsTableViewCell:UICollectionViewDelegate,UICollectionViewDataSource{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return img_array.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "AnnouncmentImageCollectionViewCell", for: indexPath) as! AnnouncmentImageCollectionViewCell
       
        cell.img_View.clipsToBounds = true
    
        if let imageUrl = img_array[indexPath.row]{
            cell.img_View.sd_setImage(with: URL(string: imageUrl), placeholderImage: #imageLiteral(resourceName: "default_header_iPhone"))
        }else{
            cell.img_View.image = #imageLiteral(resourceName: "default_header_iPhone")
        }
        
        
        if let urlstr = img_array[0]{
         //   imgContraint_Height.constant = 0.0
            img_Viewannouncement.sd_setImage(with: URL(string: urlstr), placeholderImage: #imageLiteral(resourceName: "default_header_iPhone"))
           
        }else{
            img_Viewannouncement.image = #imageLiteral(resourceName: "default_header_iPhone")
        }

     
        
        
        if indexPath.item == selectedIndexPath{

            if let urlstr = img_array[selectedIndexPath]{
                img_Viewannouncement.sd_setImage(with: URL(string: urlstr), placeholderImage: #imageLiteral(resourceName: "default_header_iPhone"))

            }else{
                img_Viewannouncement.image = #imageLiteral(resourceName: "default_header_iPhone")

            }

        }else{

        }
        
        
       print(indexPath.item,selectedIndexPath)
        
        if self.selectedIndecx_Array[indexPath.item] == true
        {
       //   let index = IndexPath(row: indexPath.item, section: 0)

          //  if let urlstr = img_array[selectedIndexPath]{
          //    self.collection_Images.reloadItems(at: [index])
              //  img_Viewannouncement.sd_setImage(with: URL(string: urlstr), placeholderImage: #imageLiteral(resourceName: "default_header_iPhone"))
                cell.img_View.layer.borderColor = UIColor.init(hex: 0x0DC9E4).cgColor
                cell.img_View.layer.borderWidth = 2.0
                cell.img_View.layer.cornerRadius = 2.0
                cell.img_View.clipsToBounds = false
               
            }
        else{
           // img_Viewannouncement.image = #imageLiteral(resourceName: "default_header_iPhone")
            cell.img_View.layer.borderColor = UIColor.clear.cgColor
            cell.img_View.layer.borderWidth = 2.0
            cell.img_View.layer.cornerRadius = 2.0
            cell.img_View.clipsToBounds = false
        }
        
        
        return cell
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
       selectedIndexPath = indexPath.row
        let index = IndexPath(row: indexPath.row, section: 0)

       DispatchQueue.main.async {
        
        var previousIndex : Int!
        for i in 0...self.selectedIndecx_Array.count - 1{
            if self.selectedIndecx_Array[i] == true{
                previousIndex = i
            }
        }
        
        if previousIndex != indexPath.item{
            self.selectedIndecx_Array[previousIndex] = false
            self.collection_Images.reloadItems(at: [IndexPath(item: previousIndex, section: 0)])
            if self.selectedIndecx_Array[indexPath.item] == true{
                self.selectedIndecx_Array[indexPath.item] = false
                
            }else{
                self.selectedIndecx_Array[indexPath.item] = true
            }
        self.IndexImageSendCellDelegateAnnouncement?.IndexImageSendCellDelegateAnnouncement(cell: self, selected_imgIndex: indexPath.row)
            self.collection_Images.reloadItems(at: [index])
        }
        
        
        }
        
        
    }
}

extension MHAnnouncementListsTableViewCell: UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        
        return 4
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        
        let totalCellWidth = 40 * collectionView.numberOfItems(inSection: 0)
        let totalSpacingWidth = 4 * (collectionView.numberOfItems(inSection: 0) - 1)
        
        let leftInset = (collectionView.layer.frame.size.width - CGFloat(totalCellWidth + totalSpacingWidth)) / 2
        let rightInset = leftInset
        
        return UIEdgeInsets(top: 0, left: leftInset, bottom: 0, right: rightInset)
        
    }
}
