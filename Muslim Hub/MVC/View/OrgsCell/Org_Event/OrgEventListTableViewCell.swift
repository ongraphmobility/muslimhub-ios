//
//  OrgEventListTableViewCell.swift
//  Muslim Hub
//
//  Created by Himanshu Goyal on 30/05/19.
//  Copyright © 2019 OnGraph. All rights reserved.
//

//HG
import UIKit
import ActiveLabel
import TTTAttributedLabel

protocol EventShowMoreDelegate {
    func showMoreTapped(index: Int, isSelected: Bool)
}
protocol IndexImageSendCellDelegateActivities {
    func IndexImageSendCellDelegateActivities(cell:OrgEventListTableViewCell ,selected_imgIndex:Int)
}

class OrgEventListTableViewCell: UITableViewCell {

    //MARK: - Properties
    @IBOutlet weak var imgBannerHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var collectionViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var btnLocation          : UIButton!
    @IBOutlet weak var btnShare             : UIButton!
    @IBOutlet weak var btnGoing             : UIButton!
    @IBOutlet weak var btnGoingCount             : UIButton!
    @IBOutlet weak var btnReminder          : UIButton!
    @IBOutlet weak var lblLocation          : UILabel!
    @IBOutlet weak var btnShowHide          : UIButton!
    @IBOutlet weak var lblDesc              : TTTAttributedLabel!
    @IBOutlet weak var lblAttendeeType      : UILabel!
    @IBOutlet weak var lblAttendeeTypeTitle      : UILabel!
    @IBOutlet weak var lblEventTitle        : UILabel!
    @IBOutlet weak var collectionView       : UICollectionView!
    @IBOutlet weak var imgEvent             : UIImageView!
    @IBOutlet weak var imgGoingCount        : UIImageView!
    @IBOutlet weak var lblEventDay          : UILabel!
    @IBOutlet weak var btnDelete            : UIButton!
    @IBOutlet weak var btnEdit              : UIButton!
    @IBOutlet weak var lblNumberOfAttendee  : UILabel!
    @IBOutlet weak var lblEventDate         : UILabel!
    @IBOutlet weak var lblEventTime         : UILabel!
    @IBOutlet weak var lblAttendesTitle        : UILabel!
    @IBOutlet weak var constrain_lblAttendesTitle_height        : NSLayoutConstraint!
    @IBOutlet weak var view_headerColor      : UIView!
    @IBOutlet weak var img_repeatingDay      : UIImageView!
    @IBOutlet weak var bg_View:UIView!
    @IBOutlet weak var imgContraint_ButtonMoreHeight:NSLayoutConstraint!
      @IBOutlet var lblDesc_Height:NSLayoutConstraint!
    
    var delegate                            : EventShowMoreDelegate!
    var model                               : EventData!
    var selectedIndexPath                   : Int!
    var img_array                           : [String?]                     = []
    var selectedIndecx_Array                : [Bool]                        = []
    var IndexImageSendCellDelegateActivities: IndexImageSendCellDelegateActivities?
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.collectionView.register(UINib(nibName: "ImageCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "ImageCollectionViewCell")
        
        
        // Initialization code
        self.layer.cornerRadius = 2
        self.layer.masksToBounds = true
        bg_View.layer.cornerRadius = 10.0
        bg_View.shadowBlackToHeader()
        bg_View.layer.maskedCorners =  [.layerMaxXMaxYCorner, .layerMinXMaxYCorner]

        self.btnShare.setTitle("Share".localized, for: .normal)
        self.btnGoing.setTitle("Going".localized, for: .normal)
        self.btnReminder.setTitle("Reminder".localized, for: .normal)
        
       
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    
    func configureCell(model: EventData){
  
        self.contentView.layer.cornerRadius = 2
        self.contentView.clipsToBounds = true
     
         self.model = model
        
        //Check for Admin,super Admin,Normal User----------------------
        if (UserDefaults.standard.value(forKey: "Login_Token") as? String) != nil{
            
            if let superAdmin = UserDefaults.standard.value(forKey: "isSuperAdmin") as? Bool{
                
                if superAdmin == true{
                   btnEdit.isHidden = false
                    btnDelete.isHidden = false
                 
                   self.lblNumberOfAttendee.textColor = UIColor.init(hex: 0x0DC9E4)
                 //  self.imgGoingCount.image = #imageLiteral(resourceName: "group_icon_blue_iPhone")
                }else{
                    
                    if let is_staff = UserDefaults.standard.value(forKey: "is_staff") as? Bool{
                        
                        if is_staff == true{
                            //mark:check For own Organisation---------------------------
                            if let orgOwn_Array = UserDefaults.standard.value(forKey: "OwnOrg_Array") as? [Int]{
                                
                                if orgOwn_Array.contains(self.model.org ?? 0){
                                    
                                    var org_P = [PriviligesData]()
                                    org_P = Global.shared.priviligesData
                                    let filertedData = org_P.filter{$0.org_id == self.model.org ?? 0}
                                    filertedData.forEach {
                                        //   print("model data objects::",$0.org_perm as? String)
                                        
                                        if $0.org_perm == "iqama_only"{
                                            btnEdit.isHidden = true
                                            btnDelete.isHidden = true
                                        }else{
                                            btnEdit.isHidden = false
                                            btnDelete.isHidden = false
                                            
                                        }
                                    }
                                    
                                  
                                 //   self.lblNumberOfAttendee.textColor = UIColor.init(hex: 0x0DC9E4)
                                 //   self.imgGoingCount.image = #imageLiteral(resourceName: "group_icon_blue_iPhone")
                                    
                                }else{
                                    btnEdit.isHidden = true
                                    btnDelete.isHidden = true
                                   
                                  //  self.lblNumberOfAttendee.textColor = UIColor.gray
                                   // self.imgGoingCount.image = #imageLiteral(resourceName: "group_icon_iPhone")
                                }//orgOwn
                                
                            }
                        }//staff true
                    }//staff
                    
                }
            }//SuperAdmin
        }//Login
        else{
           
            btnEdit.isHidden = true
            btnDelete.isHidden = true
           // self.lblNumberOfAttendee.textColor = UIColor.gray
         //   self.imgGoingCount.image = #imageLiteral(resourceName: "group_icon_iPhone")
        }
        
       
        
        self.lblEventTitle.text = model.title ?? ""

        
        if model.going == true{
            self.btnGoing.setImage(#imageLiteral(resourceName: "tick_icon_iPhone"), for: .normal)
        }else{
            self.btnGoing.setImage(#imageLiteral(resourceName: "event_Going"), for: .normal)
        }
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd-MM-yyyy"
        dateFormatter.locale = Locale(identifier: "en_US_POSIX")
        let date = dateFormatter.date(from: self.model.start_date ?? "")
        let dateFormatter1 = DateFormatter()
        dateFormatter1.dateFormat = "EEE, d MMM yyyy"
        dateFormatter1.locale = Locale(identifier: "en_US_POSIX")
        let date1 = dateFormatter1.string(from: date ?? Date())
        print(date1)
        self.lblEventDate.text = date1
        
        let timedateFormatter = DateFormatter()
        timedateFormatter.dateFormat = "HH:mm"
        timedateFormatter.locale = Locale(identifier: "en_US_POSIX")
        let starttime = timedateFormatter.date(from: self.model.start_time ?? "")
        let endTime = timedateFormatter.date(from: self.model.end_time ?? "")
        let timedateFormatter1 = DateFormatter()
        timedateFormatter1.dateFormat = "h:mm a"
        timedateFormatter1.locale = Locale(identifier: "en_US_POSIX")
        let startTime1 = timedateFormatter1.string(from: starttime ?? Date())
        let endTime1 = timedateFormatter1.string(from: endTime ?? Date())
        print(startTime1)
  

        self.lblEventTime.text = startTime1 + " - " + endTime1

        let linkAttributes: [NSAttributedString.Key : Any] = [
            NSAttributedString.Key(rawValue: NSAttributedString.Key.foregroundColor.rawValue): UIColor.black,
            NSAttributedString.Key(rawValue: NSAttributedString.Key.underlineColor.rawValue): UIColor.black,
            NSAttributedString.Key(rawValue: NSAttributedString.Key.underlineStyle.rawValue): NSUnderlineStyle.single.rawValue
        ]
        let attributeString = NSMutableAttributedString(string: model.address ?? "", attributes: linkAttributes)
        self.lblLocation.attributedText = attributeString
        
     
        
        self.lblNumberOfAttendee.text = "\(model.going_count!)" + " going".localized
        
        
        self.lblDesc.text = model.description ?? ""
        
        
        self.img_array = model.images!
        
        self.selectedIndecx_Array = model.selectedImage_Array ?? []

        if model.images!.count > 0{
            self.collectionViewHeightConstraint.constant = 40
            self.imgBannerHeightConstraint.constant = 170
        }else{
            self.collectionViewHeightConstraint.constant = 0
            self.imgBannerHeightConstraint.constant = 0
        }
        
        self.collectionView.reloadData()
        
    }
    
    @IBAction func btnShowMoreTapped(_ sender: Any) {
        if sender is UIButton {
            
            self.model.isMoreTapped! = !self.model.isMoreTapped!
            
            lblDesc.numberOfLines = self.model.isMoreTapped! ? 0 : 1
                    

                    if self.model.isMoreTapped! == true{
                        
                        lblDesc_Height.isActive = false
                        
                        lblDesc.sizeToFit()
                    }else{
                         lblDesc_Height.isActive = true
                        lblDesc_Height.constant = 20.0
                       lblDesc.sizeToFit()
                    }
                    
            if self.model.isMoreTapped!{
                
             
              
                delegate?.showMoreTapped(index: (sender as! UIButton).tag, isSelected: false)
              
            }else{
            
                delegate?.showMoreTapped(index: (sender as! UIButton).tag, isSelected: true)
              
            }
            
           
        }
    }
    
    
}


extension OrgEventListTableViewCell: UICollectionViewDelegate, UICollectionViewDataSource{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.img_array.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ImageCollectionViewCell", for: indexPath) as! ImageCollectionViewCell
        cell.img_View.clipsToBounds = true
        if let imageUrl = img_array[indexPath.row]{
            cell.img_View.sd_setImage(with: URL(string: imageUrl), placeholderImage: #imageLiteral(resourceName: "default_header_iPhone"))
        }else{
            cell.img_View.image = #imageLiteral(resourceName: "default_header_iPhone")
        }
        if let urlstr = img_array[0]{
            self.imgEvent.sd_setImage(with: URL(string: urlstr), placeholderImage: #imageLiteral(resourceName: "default_header_iPhone"))
        }else{
            self.imgEvent.image = #imageLiteral(resourceName: "default_header_iPhone")
        }
        if indexPath.item == selectedIndexPath{
            if let urlstr = img_array[selectedIndexPath]{
                self.imgEvent.sd_setImage(with: URL(string: urlstr), placeholderImage: #imageLiteral(resourceName: "default_header_iPhone"))
            }else{
                self.imgEvent.image = #imageLiteral(resourceName: "default_header_iPhone")

            }
        }else{

        }
        print(indexPath.item,selectedIndexPath)
        if self.selectedIndecx_Array[indexPath.item] == true
        {
            cell.img_View.layer.borderColor = UIColor.init(hex: 0x0DC9E4).cgColor
            cell.img_View.layer.borderWidth = 2.0
            cell.img_View.layer.cornerRadius = 2.0
            cell.img_View.clipsToBounds = false
        }else{
            cell.img_View.layer.borderColor = UIColor.clear.cgColor
            cell.img_View.layer.borderWidth = 2.0
            cell.img_View.layer.cornerRadius = 2.0
            cell.img_View.clipsToBounds = false
        }
        return cell
 
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        selectedIndexPath = indexPath.row
        let index = IndexPath(row: indexPath.row, section: 0)
        DispatchQueue.main.async {
            var previousIndex : Int!
            for i in 0...self.selectedIndecx_Array.count - 1{
                if self.selectedIndecx_Array[i] == true{
                    previousIndex = i
                }
            }
            if previousIndex != indexPath.item{
                self.selectedIndecx_Array[previousIndex] = false
                self.collectionView.reloadItems(at: [IndexPath(item: previousIndex, section: 0)])
                if self.selectedIndecx_Array[indexPath.item] == true{
                    self.selectedIndecx_Array[indexPath.item] = false
                }else{
                    self.selectedIndecx_Array[indexPath.item] = true
                }
                 self.IndexImageSendCellDelegateActivities?.IndexImageSendCellDelegateActivities(cell: self, selected_imgIndex: indexPath.row)
                self.collectionView.reloadItems(at: [index])
            }
        }
    }
}

extension OrgEventListTableViewCell: UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        
        return 4
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        
        let totalCellWidth = 40 * collectionView.numberOfItems(inSection: 0)
        let totalSpacingWidth = 4 * (collectionView.numberOfItems(inSection: 0) - 1)
        
        let leftInset = (collectionView.layer.frame.size.width - CGFloat(totalCellWidth + totalSpacingWidth)) / 2
        let rightInset = leftInset
        
        return UIEdgeInsets(top: 0, left: leftInset, bottom: 0, right: rightInset)
        
    }
}
