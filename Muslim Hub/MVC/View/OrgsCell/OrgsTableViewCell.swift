//
//  OrgsTableViewCell.swift
//  Muslim Hub
//
//  Created by Sakshi Singh on 04/04/19.
//  Copyright © 2019 OnGraph. All rights reserved.
//

import UIKit

class OrgsTableViewCell: UITableViewCell {
    
    @IBOutlet weak var lbl_Details:UILabel!
    @IBOutlet weak var lbl_Title:UILabel!
    @IBOutlet weak var view_Main:UIView!
    @IBOutlet weak var view_ThinLine:UIView!
    @IBOutlet weak var btn_Location:UILabel!
    @IBOutlet weak var btn_Heart:UIButton!
    @IBOutlet weak var img_Logo:UIImageView!
    

    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        // corner radius
        view_Main.layer.cornerRadius = 6
        // shadow
        view_Main.layer.shadowColor = UIColor.darkGray.cgColor
        view_Main.layer.shadowOffset = CGSize(width: 3, height: 1)
        view_Main.layer.shadowOpacity = 0.7
        view_Main.layer.shadowRadius = 4.0
        
        
        view_ThinLine.clipsToBounds = true
        view_ThinLine.layer.cornerRadius = 6
        view_ThinLine.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner]
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
//    override func setEditing(_ editing: Bool, animated: Bool) {
//        super.setEditing(editing, animated: animated)
//        
//        if editing {
//            for view in subviews where view.description.contains("Reorder") {
//                for case let subview as UIImageView in view.subviews {
//                    subview.image = UIImage(named: "selected_around_me_iPhone")
//                  
//                }
//            }
//        }
//    }

}
