//
//  OrgsAboutUsTableCell.swift
//  Muslim Hub
//
//  Created by Ongraph on 30/05/19.
//  Copyright © 2019 OnGraph. All rights reserved.
//

import UIKit

class OrgsAboutUsTableCell: UITableViewCell,UITextFieldDelegate {
    
    @IBOutlet weak var btn_Location: UILabel!
    @IBOutlet weak var lbl_DescriptionTitle: UILabel!
     @IBOutlet weak var lbl_AddressTitle: UILabel!
     @IBOutlet weak var lbl_ContactDetailsTitle: UILabel!
    
    @IBOutlet weak var lang_President:UITextField!
       @IBOutlet weak var lang_Imam:UITextField!
       @IBOutlet weak var lang_GeneralSecetry:UITextField!
    
    
    @IBOutlet weak var btn_Sms: UIButton!
    @IBOutlet weak var bg_View:UIView!
    @IBOutlet weak var txtDesc_View:UITextView!
    @IBOutlet weak var txtField_President:UITextField!
    @IBOutlet weak var txtField_Imam:UITextField!
    @IBOutlet weak var txtField_GeneralSecetry:UITextField!
    @IBOutlet weak var txtField_EmailID:UITextField!
    @IBOutlet weak var txtField_phoneNumber:UITextField!
    @IBOutlet weak var stackView_President:UIStackView!
    @IBOutlet weak var stackView_Imam:UIStackView!
    @IBOutlet weak var stackView_GeneralSecetry:UIStackView!
    @IBOutlet weak var layout_TopAdress:NSLayoutConstraint!
    @IBOutlet weak var view_LineAdress:UIView!
    @IBOutlet weak var btn_DescEdit: UIButton!
    @IBOutlet weak var btn_CountryCode: UIButton!
    @IBOutlet weak var btn_ContactEdit: UIButton!
    @IBOutlet weak var btn_AddressEdit: UIButton!
    @IBOutlet weak var btn_PresidentEdit: UIButton!
    var buttonDescPressed : (() -> ()) = {}
    var buttonContactPressed : (() -> ()) = {}
    var buttonAddressPressed : (() -> ()) = {}
    var buttonCountryCodePressed : (() -> ()) = {}
    var buttonPresidentPressed : (() -> ()) = {}
    var buttonSmsPressed : (() -> ()) = {}
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.layer.cornerRadius = 2
        self.layer.masksToBounds = true
        bg_View.layer.cornerRadius = 10.0
        bg_View.shadowBlackToHeader()
        self.lbl_DescriptionTitle.text = "Description".localized
        self.lbl_AddressTitle.text = "Address".localized
        self.lbl_ContactDetailsTitle.text = "Contact Details".localized
        
          self.lang_GeneralSecetry.text = "General Secretary :".localized
          self.lang_Imam.text = "Imam :".localized
          self.lang_President.text = "President :".localized
        
        self.txtField_GeneralSecetry.placeholder = "Enter Name".localized
        self.txtField_Imam.placeholder = "Enter Name".localized
        self.txtField_President.placeholder = "Enter Name".localized
        self.txtField_phoneNumber.placeholder = "Enter Phone Number".localized
         self.txtField_EmailID.placeholder = "Enter Email Id".localized
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
     @IBAction func buttonSendSmsAction(_ sender: UIButton) {
        buttonSmsPressed()
    }
    @IBAction func buttonDescAction(_ sender: UIButton) {
        if sender.isSelected{
            txtDesc_View.isEditable = false
            btn_DescEdit.setBackgroundImage(#imageLiteral(resourceName: "edit_pencil_iPhone"), for: .normal)
        }else{
            txtDesc_View.isEditable = true
            btn_DescEdit.setBackgroundImage(#imageLiteral(resourceName: "edit_pencil_iPhone"), for: .normal)
        }
        buttonDescPressed()
    }
    @IBAction func buttonContactAction(_ sender: UIButton) {
        if sender.isSelected{
           
            txtField_EmailID.isUserInteractionEnabled = false
            txtField_phoneNumber.isUserInteractionEnabled = false
            btn_CountryCode.isUserInteractionEnabled = false
            btn_ContactEdit.setBackgroundImage(#imageLiteral(resourceName: "edit_pencil_iPhone"), for: .normal)
        }else{
            txtField_EmailID.delegate = self
            txtField_phoneNumber.delegate = self
            txtField_EmailID.becomeFirstResponder()
            txtField_phoneNumber.becomeFirstResponder()
            
            txtField_EmailID.isUserInteractionEnabled = true
            txtField_phoneNumber.isUserInteractionEnabled = true
            btn_CountryCode.isUserInteractionEnabled = true
            btn_ContactEdit.setBackgroundImage(#imageLiteral(resourceName: "gray_edit_pencil_iPhone"), for: .normal)
        }
        buttonContactPressed()
    }
    @IBAction func buttonAddressAction(_ sender: UIButton) {
        if sender.isSelected{
            btn_Location.isUserInteractionEnabled = false
            btn_AddressEdit.setBackgroundImage(#imageLiteral(resourceName: "edit_pencil_iPhone"), for: .normal)
        }else{
            btn_Location.isUserInteractionEnabled = true
            btn_AddressEdit.setBackgroundImage(#imageLiteral(resourceName: "gray_edit_pencil_iPhone"), for: .normal)
        }
        buttonAddressPressed()
    }
     @IBAction func buttonCountryCodeAction(_ sender: UIButton) {
        buttonCountryCodePressed()
    }
    @IBAction func buttonPresidentAction(_ sender: UIButton) {
        if sender.isSelected{
            txtField_GeneralSecetry.isUserInteractionEnabled = false
            txtField_President.isUserInteractionEnabled = false
            txtField_Imam.isUserInteractionEnabled = false
            btn_PresidentEdit.setBackgroundImage(#imageLiteral(resourceName: "edit_pencil_iPhone"), for: .normal)
        }else{
               txtField_GeneralSecetry.isUserInteractionEnabled = true
             txtField_Imam.isUserInteractionEnabled = true
            txtField_President.isUserInteractionEnabled = true
            btn_PresidentEdit.setBackgroundImage(#imageLiteral(resourceName: "gray_edit_pencil_iPhone"), for: .normal)
        }
        buttonPresidentPressed()
    }
    
}
