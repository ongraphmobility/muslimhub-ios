//
//  Add_ActivitiesEventTableViewCell.swift
//  Muslim Hub
//
//  Created by Ongraph on 28/05/19.
//  Copyright © 2019 OnGraph. All rights reserved.
//

import UIKit

class Add_ActivitiesEventTableViewCell: UITableViewCell {
    
    @IBOutlet weak var txt_Title:UITextField!
    @IBOutlet weak var txt_RepeatingDay:UITextField!
    @IBOutlet weak var txtView_Description:UITextView!
    @IBOutlet weak var view_1:UIView!
    @IBOutlet weak var view_2:UIView!
    @IBOutlet weak var view_3:UIView!
    @IBOutlet weak var img_DropDown:UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
