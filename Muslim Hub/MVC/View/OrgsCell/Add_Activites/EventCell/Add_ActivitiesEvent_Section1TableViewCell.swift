//
//  Add_ActivitiesEvent_Section1TableViewCell.swift
//  Muslim Hub
//
//  Created by Ongraph on 28/05/19.
//  Copyright © 2019 OnGraph. All rights reserved.
//

import UIKit

class Add_ActivitiesEvent_Section1TableViewCell: UITableViewCell {
    
    
    @IBOutlet weak var txt_Date:UITextField!
    @IBOutlet weak var txt_Time:UITextField!
    @IBOutlet weak var img_Date:UIImageView!
    @IBOutlet weak var img_Time:UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
