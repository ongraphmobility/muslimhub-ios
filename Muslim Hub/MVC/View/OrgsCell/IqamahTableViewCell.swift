//
//  IqamahTableViewCell.swift
//  Muslim Hub
//
//  Created by Ongraph on 03/06/19.
//  Copyright © 2019 OnGraph. All rights reserved.
//

import UIKit

class IqamahTableViewCell: UITableViewCell {

    @IBOutlet weak var btn_Yes: UIButton!
    @IBOutlet weak var btn_No: UIButton!
    @IBOutlet weak var bg_View:UIView!
    @IBOutlet weak var lbl_PreUploaded: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.layer.cornerRadius = 2
        self.layer.masksToBounds = true
        bg_View.layer.cornerRadius = 10.0
        bg_View.shadowBlackToHeader()
        buttoncornerRadiusShadow ()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
        self.lbl_PreUploaded.text = "Preuploaded Iqamah Timing Availabel".localized
        
        self.btn_Yes.setTitle("Yes".localized, for: .normal)
        self.btn_No.setTitle("No".localized, for: .normal)
        
    }
 
     
    //MARK:Functions And Methods------------------------------
    func buttoncornerRadiusShadow (){
        
        btn_Yes.clipsToBounds = true
        btn_Yes.layer.cornerRadius = 8
        btn_Yes.layer.borderColor = UIColor.init(hex: 0x0DC9E4).cgColor
        btn_Yes.layer.borderWidth = 1.0
        btn_Yes.layer.maskedCorners = [.layerMinXMaxYCorner, .layerMinXMinYCorner]
        
        btn_No.clipsToBounds = true
        btn_No.layer.cornerRadius = 8
        btn_No.layer.maskedCorners = [.layerMaxXMaxYCorner, .layerMaxXMinYCorner]
        btn_No.layer.borderColor = UIColor.init(hex: 0x0DC9E4).cgColor
        btn_No.layer.borderWidth = 1.0
        
        self.btn_Yes.shadowBlackToHeader()
        self.btn_No.shadowBlackToHeader()
        
    }
    
}
