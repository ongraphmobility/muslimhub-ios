//
//  OrgActivityTableViewCell.swift
//  Muslim Hub
//
//  Created by Sakshi Singh on 15/04/19.
//  Copyright © 2019 OnGraph. All rights reserved.
//

import UIKit

protocol NotSoGoodCellDelegateOrg {
    func moreTapped(cell: OrgActivityTableViewCell)
}

class OrgActivityTableViewCell: UITableViewCell {
   
    
    @IBOutlet weak var lbl_ReadMore:UILabel!
    @IBOutlet weak var view_Announcment:UIView!
    @IBOutlet weak var img_Viewannouncement:UIImageView!
    
    
    @IBOutlet weak var collection_Images:UICollectionView!
    @IBOutlet weak var collection_height:NSLayoutConstraint!
    
    @IBOutlet weak var btn_Location: UIButton!
    @IBOutlet weak var buttonMore: UIButton!
    
    var delegate: NotSoGoodCellDelegateOrg?
    
    var isExpanded: Bool = false
    
    var selectedIndexPath:Int!
    
  var img_array = [#imageLiteral(resourceName: "sydney-opera-house"),#imageLiteral(resourceName: "screen6")]
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
         //registerCell()
      
    }
   
    @IBAction func btnMoreTapped(_ sender: Any) {
        
        if sender is UIButton {
            isExpanded = !isExpanded
            
            lbl_ReadMore.numberOfLines = isExpanded ? 0 : 2
            buttonMore.setTitle(isExpanded ? "Show less" : "Show more", for: .normal)
            
            delegate?.moreTapped(cell: self)
        }
        
    }
    
    public func myInit(theTitle: String, theBody: String) {
        
        isExpanded = false
        
        // labelTitle.text = theTitle
        lbl_ReadMore.text = theBody
        
        lbl_ReadMore.numberOfLines = 2
        
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
extension OrgActivityTableViewCell:UICollectionViewDelegate,UICollectionViewDataSource{
    
    func registerCell() {
        
        if img_array.count == 1 || img_array.count == 0{
            collection_height.constant = 0.0
            collection_Images.isHidden = true
        }
        else{
             collection_height.constant = 47.5
            collection_Images.isHidden = false
        }
    }
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return img_array.count
    }
    
  
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "AnnouncmentImageCollectionViewCell", for: indexPath) as! AnnouncmentImageCollectionViewCell
      
        cell.img_View.image = img_array[indexPath.item]
    
        if indexPath.row == selectedIndexPath{
            cell.layer.borderColor = UIColor.blue.cgColor
            cell.layer.borderWidth = 2.0
            cell.layer.cornerRadius = 2.0
            cell.clipsToBounds = false
            img_Viewannouncement.image = img_array[selectedIndexPath]
        }else{
            cell.layer.borderColor = UIColor.clear.cgColor
            cell.layer.borderWidth = 2.0
            cell.layer.cornerRadius = 2.0
            cell.clipsToBounds = false
            img_Viewannouncement.image = img_array[0]
        }
        
        return cell
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        selectedIndexPath = indexPath.item
     
       // delegateTableView?.tableViewReload(cell: self)
        collection_Images.reloadData()
        
    }
   
}
extension OrgActivityTableViewCell: UICollectionViewDelegateFlowLayout {
    
    private func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        
        return 4
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        
        let totalCellWidth = 40 * collectionView.numberOfItems(inSection: 0)
        let totalSpacingWidth = 10 * (collectionView.numberOfItems(inSection: 0) - 1)
        
        let leftInset = (collectionView.layer.frame.size.width - CGFloat(totalCellWidth + totalSpacingWidth)) / 2
        let rightInset = leftInset
        
        return UIEdgeInsets(top: 0, left: leftInset, bottom: 0, right: rightInset)
        
    }
}
