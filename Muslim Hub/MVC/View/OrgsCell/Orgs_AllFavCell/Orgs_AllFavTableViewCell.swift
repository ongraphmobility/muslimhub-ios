//
//  Orgs_AllFavTableViewCell.swift
//  Muslim Hub
//
//  Created by Sakshi Singh on 05/04/19.
//  Copyright © 2019 OnGraph. All rights reserved.
//

import UIKit

class Orgs_AllFavTableViewCell: UITableViewCell {
    
    @IBOutlet weak var lbl_Details:UILabel!
    @IBOutlet weak var lbl_Title:UILabel!
    @IBOutlet weak var view_Main:UIView!
    @IBOutlet weak var view_ThinLine:UIView!
    @IBOutlet weak var btn_Location:UILabel!
    @IBOutlet weak var btn_Heart:UIButton!
    @IBOutlet weak var img_Logo:UIImageView!
    
    

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        //mark: corner radius with shadow----------------
        view_Main.layer.cornerRadius = 6
        view_Main.layer.shadowColor = UIColor.darkGray.cgColor
        view_Main.layer.shadowOffset = CGSize(width: 3, height: 1)
        view_Main.layer.shadowOpacity = 0.7
        view_Main.layer.shadowRadius = 4.0
        
        
        //mark:Upper side conrner radius---------------
        view_ThinLine.clipsToBounds = true
        view_ThinLine.layer.cornerRadius = 6
        view_ThinLine.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner]
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
