//
//  Org_Classes_TableViewCell.swift
//  Muslim Hub
//
//  Created by Sakshi Singh on 17/04/19.
//  Copyright © 2019 OnGraph. All rights reserved.
//

import UIKit
import ActiveLabel
import TTTAttributedLabel

protocol NotSoGoodCellDelegateOrgClasess {
    func moreTapped(index: Int, isSelected: Bool)
}
protocol IndexImageSendCellDelegateClass {
    func IndexImageSendCellDelegateClass(cell: Org_Classes_TableViewCell,selected_imgIndex:Int)
}

class Org_Classes_TableViewCell: UITableViewCell {
    
    @IBOutlet weak var lbl_ReadMore:UILabel!
    @IBOutlet weak var view_Announcment:UIView!
    @IBOutlet weak var img_Viewannouncement:UIImageView!
    
    @IBOutlet weak var collection_Images:UICollectionView!
    @IBOutlet weak var imageView_topConstraint: NSLayoutConstraint!
    @IBOutlet weak var btn_Location: UIButton!
    @IBOutlet weak var buttonMore: UIButton!
    @IBOutlet weak var imgContraint_ButtonMoreHeight: NSLayoutConstraint!
    @IBOutlet  var lblDesc_Height: NSLayoutConstraint!
    @IBOutlet weak var imgBannerHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var collectionViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var lblClassesTitle        : UILabel!
    //    @IBOutlet weak var btnProfile             : UIButton!
    @IBOutlet weak var btnQuran             : UIButton!
    @IBOutlet weak var lblClassesDay          : UILabel!
    @IBOutlet weak var lblClassesDate         : UILabel!
    @IBOutlet weak var lblClassesTime         : UILabel!
    @IBOutlet weak var lblMonthType         : UILabel!
    @IBOutlet weak var lblAge               : UILabel!
    @IBOutlet weak var lblAgeTitle              : UILabel!
    @IBOutlet weak var lblGender               : UILabel!
    @IBOutlet weak var lblTeacher               : UILabel!
      @IBOutlet weak var lblTeacherTitle              : UILabel!
    @IBOutlet weak var lblDesc              : TTTAttributedLabel!
    @IBOutlet weak var btnShare             : UIButton!
    @IBOutlet weak var btnReminder          : UIButton!
    @IBOutlet weak var lbl_OrgName        : UILabel!
    @IBOutlet weak var img_Org     : UIImageView!
    @IBOutlet weak var lblNumberOfAttendee  : UIButton!
    @IBOutlet weak var btnDelete            : UIButton!
    @IBOutlet weak var btnEdit              : UIButton!
    @IBOutlet weak var lbl_Cost              : UILabel!
    @IBOutlet weak var btn_CostTitle              : UIButton!
    
    var delegateClasses: NotSoGoodCellDelegateOrgClasess?
    var IndexImageSendCellDelegateClass: IndexImageSendCellDelegateClass?
    
    var isExpanded: Bool = false
    var selectedIndexPath                   :Int!
    var model                               : GetClassData!
    var img_array                           : [String?]                     = []
    var selectedIndecx_Array                : [Bool]                        = []
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        self.btnShare.setTitle("Share".localized, for: .normal)
        
        self.btnReminder.setTitle("Reminder".localized, for: .normal)
        self.btn_CostTitle.setTitle("Cost:".localized, for: .normal)
        self.lblAgeTitle.text = "Age Group:".localized
        self.lblTeacherTitle.text = "Teacher:".localized
    }
    @IBAction func btnMoreTapped(_ sender: Any) {
        
        
        if sender is UIButton {
            isExpanded = !isExpanded
            if isExpanded{
                lblDesc_Height.isActive = false
                
                lblDesc.sizeToFit()
                delegateClasses?.moreTapped(index: (sender as! UIButton).tag, isSelected: true)
                lblDesc.numberOfLines = 0
            }else{
                lblDesc_Height.isActive = true
                lblDesc_Height.constant = 20.0
                lblDesc.sizeToFit()
                delegateClasses?.moreTapped(index: (sender as! UIButton).tag, isSelected: false)
                lblDesc.numberOfLines = 1
            }
        }
        
        
    }
    
    public func myInit(theTitle: String, theBody: String) {
        
        isExpanded = false
        
        // labelTitle.text = theTitle
        lbl_ReadMore.text = theBody
        
        lbl_ReadMore.numberOfLines = 2
        
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    func configureCell(model: GetClassData){
        
        
        self.lblClassesTitle.text = model.title
        self.model = model
        
        
        btnQuran.layer.cornerRadius = 4.0
        btnQuran.layer.borderWidth = 1.0
        btnQuran.layer.borderColor = UIColor.lightGray.cgColor
        btnQuran.layer.maskedCorners =  [.layerMinXMaxYCorner , .layerMinXMinYCorner] //.layerMinXMaxYCorner]
        
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd-MM-yyyy"
        dateFormatter.locale = Locale(identifier: "en_US_POSIX")
        let date = dateFormatter.date(from: model.start_date ?? "")
        let dateFormatter1 = DateFormatter()
        dateFormatter1.dateFormat = "EEE, d MMM yyyy"
        dateFormatter1.locale = Locale(identifier: "en_US_POSIX")
        let date1 = dateFormatter1.string(from: date ?? Date())
        print(date1)
        self.lblClassesDate.text = date1
        
        let timedateFormatter = DateFormatter()
        timedateFormatter.dateFormat = "HH:mm"
        timedateFormatter.locale = Locale(identifier: "en_US_POSIX")
        let starttime = timedateFormatter.date(from: model.start_time ?? "")
        let endTime = timedateFormatter.date(from: model.end_time ?? "")
        let timedateFormatter1 = DateFormatter()
        timedateFormatter1.dateFormat = "h:mm a"
        timedateFormatter1.locale = Locale(identifier: "en_US_POSIX")
        let startTime1 = timedateFormatter1.string(from: starttime ?? Date())
        let endTime1 = timedateFormatter1.string(from: endTime ?? Date())
        print(startTime1)
        
        
        self.lblClassesTime.text = startTime1 + " - " + endTime1
        
        
        
        //  self.btn_Location.setTitle(model.address ?? "", for: .normal)
        
        //  self.lblNumberOfAttendee.setTitle("\(22)" + " going", for: .normal)
        //       lblDesc.numberOfLines = 0
        //       lblDesc.enabledTypes = [.mention, .hashtag, .url]
        //       lblDesc.text = model.description ?? ""
        //        //        cell.lbl_ReadMore.handleHashtagTap { hashtag in
        //        //            print("Success. You just tapped the \(hashtag) hashtag")
        //        //        }
        //        lblDesc.handleURLTap { url in
        //            
        //            let urlString = ("\(url)")
        //            var urll = String()
        //           
        //            if urlString.contains("https://") {
        //                urll = ("\(url)")
        //            }else if urlString.contains("http://") {
        //                urll = ("\(url)")
        //            }else{
        //                urll = ("http://\(url)")
        //            }
        //            
        //            
        //            guard let url = URL(string: urll) else { return }
        //            UIApplication.shared.open(url)
        //        }
        //        
        //        var string = ""
        //            let input = model.description ?? ""
        //            let detector = try! NSDataDetector(types: NSTextCheckingResult.CheckingType.phoneNumber.rawValue)
        //            let matches = detector.matches(in: input, options: [], range: NSRange(location: 0, length: input.utf16.count))
        //
        //            for match in matches {
        //                guard let range = Range(match.range, in: input) else { continue }
        //                let url = input[range]
        //                print(url)
        //                string = String(url)
        //            }
        //          
        //            let customType = ActiveType.custom(pattern: "\\s\(string)\\b") //Regex that looks for "with"
        //             lblDesc.enabledTypes.append(customType)
        //            lblDesc.enabledTypes = [.mention, .hashtag, .url, customType]
        //           
        //            lblDesc.text = model.description ?? ""
        //            lblDesc.customColor[customType] = UIColor.blue
        //            lblDesc.customSelectedColor[customType] = UIColor.blue
        //
        //           lblDesc.handleCustomTap(for: customType) { element in
        //               
        //                if let url = URL(string:"tel://\(element)"), UIApplication.shared.canOpenURL(url) {
        //                     UIApplication.shared.open(url)
        //                }
        //            }
        
        // self.lblDesc.text = model.description ?? ""
        self.img_array = model.images!
        self.lblTeacher.text = " \(model.teacher_name ?? " ")"
        
        self.selectedIndecx_Array = model.selectedImage_Array ?? []
        
        
        
        
        if model.images!.count > 0{
            if let urlstr = model.images?[0]{
                self.img_Viewannouncement.sd_setImage(with: URL(string: urlstr), placeholderImage: #imageLiteral(resourceName: "default_header_iPhone"))
            }else{
                self.img_Viewannouncement.image = #imageLiteral(resourceName: "default_header_iPhone")
            }
            if model.images!.count > 1{
                
                self.collectionViewHeightConstraint.constant = 40
                self.imageView_topConstraint.constant = 5
                
            }else{
                
                self.collectionViewHeightConstraint.constant = 0
                self.imageView_topConstraint.constant = 0
                
            }
            
            self.imgBannerHeightConstraint.constant = 165
        }else{
            
            self.collectionViewHeightConstraint.constant = 0
            self.imgBannerHeightConstraint.constant = 0
            self.imageView_topConstraint.constant = 0
            
        }
        
        self.collection_Images.reloadData()
        
    }
    
}
extension Org_Classes_TableViewCell:UICollectionViewDelegate,UICollectionViewDataSource{
    
    func registerCell() {
        
        if img_array.count == 1 || img_array.count == 0{
            //  collection_height.constant = 0.0
            collection_Images.isHidden = true
        }
        else{
            // collection_height.constant = 47.5
            collection_Images.isHidden = false
        }
    }
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return img_array.count
    }
    
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "AnnouncmentImageCollectionViewCell", for: indexPath) as! AnnouncmentImageCollectionViewCell
        //    cell.img_View.image = nil
        cell.img_View.clipsToBounds = true
        if let imageUrl = img_array[indexPath.row]{
            cell.img_View.sd_setImage(with: URL(string: imageUrl), placeholderImage: #imageLiteral(resourceName: "default_header_iPhone"))
        }else{
            cell.img_View.image = #imageLiteral(resourceName: "default_header_iPhone")
        }
        if let urlstr = img_array[0]{
            self.img_Viewannouncement.sd_setImage(with: URL(string: urlstr), placeholderImage: #imageLiteral(resourceName: "default_header_iPhone"))
        }else{
            self.img_Viewannouncement.image = #imageLiteral(resourceName: "default_header_iPhone")
        }
        if indexPath.item == selectedIndexPath{
            if let urlstr = img_array[selectedIndexPath]{
                self.img_Viewannouncement.sd_setImage(with: URL(string: urlstr), placeholderImage: #imageLiteral(resourceName: "default_header_iPhone"))
            }else{
                self.img_Viewannouncement.image = #imageLiteral(resourceName: "default_header_iPhone")
                
            }
        }else{
            
        }
        print(indexPath.item,selectedIndexPath)
        if selectedIndexPath != nil{
            if self.selectedIndecx_Array[indexPath.item] == true
            {
                cell.img_View.layer.borderColor = UIColor.init(hex: 0x0DC9E4).cgColor
                cell.img_View.layer.borderWidth = 2.0
                cell.img_View.layer.cornerRadius = 2.0
                cell.img_View.clipsToBounds = false
            }else{
                cell.img_View.layer.borderColor = UIColor.clear.cgColor
                cell.img_View.layer.borderWidth = 2.0
                cell.img_View.layer.cornerRadius = 2.0
                cell.img_View.clipsToBounds = false
            }
        }
        
        
        return cell
        
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        selectedIndexPath = indexPath.row
        let index = IndexPath(row: indexPath.row, section: 0)
        DispatchQueue.main.async {
            var previousIndex : Int!
            for i in 0...self.selectedIndecx_Array.count - 1{
                if self.selectedIndecx_Array[i] == true{
                    previousIndex = i
                }
            }
            if previousIndex != indexPath.item{
                self.selectedIndecx_Array[previousIndex] = false
                self.collection_Images.reloadItems(at: [IndexPath(item: previousIndex, section: 0)])
                if self.selectedIndecx_Array[indexPath.item] == true{
                    self.selectedIndecx_Array[indexPath.item] = false
                }else{
                    self.selectedIndecx_Array[indexPath.item] = true
                }
                
                self.IndexImageSendCellDelegateClass?.IndexImageSendCellDelegateClass(cell: self, selected_imgIndex: indexPath.row)
                self.collection_Images.reloadItems(at: [index])
            }
        }
    }
    //    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
    //
    //        selectedIndexPath = indexPath.item
    //        delegateClasses?.moreTapped(cell: self)
    //        collection_Images.reloadData()
    //
    //    }
    
}
extension Org_Classes_TableViewCell: UICollectionViewDelegateFlowLayout {
    
    private func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        
        return 4
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        
        let totalCellWidth = 40 * collectionView.numberOfItems(inSection: 0)
        let totalSpacingWidth = 10 * (collectionView.numberOfItems(inSection: 0) - 1)
        
        let leftInset = (collectionView.layer.frame.size.width - CGFloat(totalCellWidth + totalSpacingWidth)) / 2
        let rightInset = leftInset
        
        return UIEdgeInsets(top: 0, left: leftInset, bottom: 0, right: rightInset)
        
    }
}

