//
//  Manage_UserListCellTableViewCell.swift
//  Muslim Hub
//
//  Created by Ongraph on 28/06/19.
//  Copyright © 2019 OnGraph. All rights reserved.
//

import UIKit
import MobileCoreServices

class Manage_UserListCellTableViewCell: UITableViewCell {

    @IBOutlet weak var lbl_NAme:UILabel!
    @IBOutlet weak var lbl_EmailId:UILabel!
    @IBOutlet weak var lbl_PhoneNo:UILabel!
    @IBOutlet weak var btn_Delete:UIButton!
    @IBOutlet weak var btn_Edit:UIButton!
    
    @IBOutlet weak var lbl_NAmeTitle:UILabel!
    @IBOutlet weak var lbl_EmailIdTitle:UILabel!
    @IBOutlet weak var lbl_PhoneNoTitle:UILabel!
 
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

   
}
