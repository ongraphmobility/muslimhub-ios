//
//  EidSalahaTableViewCell.swift
//  Muslim Hub
//
//  Created by Sakshi Singh on 10/05/19.
//  Copyright © 2019 OnGraph. All rights reserved.
//

import UIKit

class EidSalahaTableViewCell: UITableViewCell {
    
    @IBOutlet weak var btnCountryCode: UIButton!
    @IBOutlet weak var txt_Title:UITextField!
    @IBOutlet weak var txt_Title2:UITextField!
    @IBOutlet weak var txt_Phone:UITextField!
    @IBOutlet weak var view_1:UIView!
    @IBOutlet weak var view_2:UIView!
    @IBOutlet weak var view_3:UIView!
      @IBOutlet weak var view_4:UIView!
      @IBOutlet weak var txtView_Description:UITextView!
    @IBOutlet weak var btn_side:UIButton!
    @IBOutlet weak var btn_side2:UIButton!
   
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}
