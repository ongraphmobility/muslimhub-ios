//
//  AboutUSTableViewCell.swift
//  Muslim Hub
//
//  Created by Sakshi Singh on 24/04/19.
//  Copyright © 2019 OnGraph. All rights reserved.
//

import UIKit

class AboutUSTableViewCell: UITableViewCell {
    
    @IBOutlet weak var btn_Edit:UIButton!
    @IBOutlet weak var txt_View:UITextView!
    @IBOutlet weak var btn_EditPen:UIButton!
     @IBOutlet weak var lbl_AppVersion:UILabel!
       @IBOutlet weak var lbl_Header:UILabel!
      @IBOutlet weak var btn_PrivacyStatement:UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    @IBAction func btn_Editt(sender:UIButton){
    
    txt_View.isEditable = true
    
    }
}
