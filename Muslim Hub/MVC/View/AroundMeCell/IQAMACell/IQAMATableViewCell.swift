//
//  IQAMATableViewCell.swift
//  Muslim Hub
//
//  Created by Sakshi Singh on 03/04/19.
//  Copyright © 2019 OnGraph. All rights reserved.
//

import UIKit

class IQAMATableViewCell: UITableViewCell {
    
    //MARK:IBOutlets----------------------
    @IBOutlet weak var view_AroundMeIQAMA:UIView!
    @IBOutlet weak var btn_Heart:UIButton!
    @IBOutlet weak var view_Header:UIView!
    @IBOutlet weak var btn_Location:UILabel!
    @IBOutlet weak var btn_Title:UIButton!
    @IBOutlet weak var lbl_Description:UILabel!
    @IBOutlet weak var lbl_Away:UILabel!
    @IBOutlet weak var lbl_IqamaIn:UILabel!
    @IBOutlet weak var lbl_prayerType:UILabel!
    @IBOutlet weak var lbl_MainprayerType:UILabel!
    @IBOutlet weak var lbl_Time:UILabel!
  
     @IBOutlet weak var img_Logo:UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
