//
//  MHEidSalahTableCell.swift
//  Muslim Hub
//
//  Created by Ongraph2018 on 10/05/19.
//  Copyright © 2019 OnGraph. All rights reserved.
//

import UIKit

protocol readmoreDelegate {
    func moreTapped(cell: MHEidSalahTableCell, index: Int)
    func deleteTapped(cell: MHEidSalahTableCell, index: Int)
    func editTapped(cell: MHEidSalahTableCell, index: Int)
}

class MHEidSalahTableCell: UITableViewCell {
 @IBOutlet weak var btn_Location:UIButton!
    @IBOutlet weak var btn_edit         : UIButton!
    @IBOutlet weak var btn_delete       : UIButton!
    @IBOutlet weak var btn_image        : UIImageView!
    @IBOutlet weak var lbl_name         : UILabel!
    @IBOutlet weak var lbl_date         : UILabel!
    @IBOutlet weak var lbl_salahTime    : UILabel!
    @IBOutlet weak var lbl_thakbeerTime : UILabel!
    @IBOutlet weak var lbl_held         : UILabel!
    @IBOutlet weak var lbl_parking      : UILabel!
    @IBOutlet weak var lbl_attend       : UILabel!
    
    
    @IBOutlet weak var lbl_salahTimeTitle    : UILabel!
    @IBOutlet weak var lbl_thakbeerTimeTitle : UILabel!
    @IBOutlet weak var lbl_heldTitle         : UILabel!
    @IBOutlet weak var lbl_parkingTitle      : UILabel!
    @IBOutlet weak var lbl_attendTitle       : UILabel!
    @IBOutlet weak var lbl_addedByTitle       : UILabel!
    
    @IBOutlet weak var lbl_language     : UILabel!
    @IBOutlet weak var lbl_phoneNo      : UILabel!
    @IBOutlet weak var lbl_notes        : UILabel!
    @IBOutlet weak var lbl_addedBy      : UILabel!
    @IBOutlet weak var btn_readMore     : UIButton!
    @IBOutlet weak var view_salah       : UIView!
    @IBOutlet weak var lbl_location     : UILabel!
    @IBOutlet weak var lbl_Static_Lang  : UILabel!
    @IBOutlet weak var lbl_Static_Phone : UILabel!
    @IBOutlet weak var imgShowDrop      : UIImageView!
    var delegate                        : readmoreDelegate?
    var isExpanded                      : Bool                  = false
    @IBOutlet weak var lblTempLocation: UILabel!
    
    
    @IBOutlet weak var stackView: UIStackView!
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func getDropDownData(type: EidSalahDropDown) -> [String]{
        if type == .held{
            return ["Indoor", "Outdoor"]
        }else if type == .attend{
            return ["Men Only", "Men, Women and Family"]
        }else if type == .parking{
            return ["Limited", "Plenty", "No Parking"]
        }else if type == .language{
            return ["English", "Urdu", "Arabic", "Malay"]
        }else{
            return []
        }
    }
    
    func getIndexOfDropDown(type: EidSalahDropDown, value: String? = "", index: Int? = nil) -> (Int, String){
        let array = self.getDropDownData(type: type)
        if value != ""{
            for i in 0..<array.count{
                if array[i] == value{
                    return (i + 1, array[i])
                }
            }
            return (0, value!)
        }else if index != nil{
            return (index! + 1, array[index!])
        }else{
            return (0, "")
        }
    }
    
    func configureCell(data: EidSalahListData, isSelected: Bool){
        
        lbl_salahTimeTitle.text = "Salah Time:".localized
        lbl_thakbeerTimeTitle.text = "Thakbeer Time:".localized
        lbl_heldTitle.text = "Held:".localized
        lbl_parkingTitle.text = "Parking:".localized
        lbl_attendTitle.text = "Who can attend:".localized
        lbl_addedByTitle.text = "Added By:".localized
        
        
        let (_, held) = self.getIndexOfDropDown(type: .held, index: data.held - 1)
        let (_, attend) = self.getIndexOfDropDown(type: .attend, index: data.attendees - 1)
        let (_, parking) = self.getIndexOfDropDown(type: .parking, index: data.parking - 1)
        let (_, language) = self.getIndexOfDropDown(type: .language,index: data.khutbahLang - 1)
        
        self.lbl_name.text = data.orgAbb + " - " + data.city
//        self.btn_title.text = data.orgAbb
        self.lbl_date.text = data.date
        
        let attributedString = NSMutableAttributedString ()
        self.lblTempLocation.text = data.address
        attributedString.append(NSAttributedString(string: data.address ?? "",
                                                   attributes: [.font: UIFont(name: "Avenir Book", size: 14.0)!, .foregroundColor: UIColor.lightGray, NSAttributedString.Key.underlineStyle : NSUnderlineStyle.single.rawValue]))
        
        self.lbl_location.attributedText = attributedString
        self.lbl_salahTime.text = data.salahTime
        self.lbl_thakbeerTime.text = data.takbirTime

        self.lbl_held.text = held
        self.lbl_attend.text = attend
        self.lbl_language.text = language
        self.lbl_parking.text = parking
        self.lbl_phoneNo.text = data.phoneNum
        self.lbl_notes.text = data.info
        self.lbl_addedBy.text = data.addedBy
        
        if isSelected ==  false {
            self.lbl_Static_Lang.text = ""
            self.lbl_Static_Phone.text = ""
            self.lbl_phoneNo.text = ""
            self.lbl_language.text = ""
            self.lbl_notes.text = ""
            self.lbl_notes.isHidden = true
            self.stackView.isHidden = true
        }else{
            self.lbl_Static_Lang.text = "Khutbah Language:"
            self.lbl_Static_Phone.text = "Contact:"
            self.lbl_phoneNo.text = data.phoneNum
            self.lbl_language.text = language
            self.lbl_notes.text = data.info
            self.lbl_notes.isHidden = false
            self.stackView.isHidden = false
        }
        
    }

    @IBAction func btn_deleteTapped(_ sender: Any) {
        delegate?.deleteTapped(cell: self, index: (sender as! UIButton).tag)
    }
    
    @IBAction func btn_editTapped(_ sender: Any) {
        print((sender as! UIButton).tag)
        delegate?.editTapped(cell: self, index: (sender as! UIButton).tag)
    }
    
    @IBAction func btn_readMore(_ sender: Any) {
        if sender is UIButton {
//            isExpanded = !isExpanded
//            lbl_notes.numberOfLines = isExpanded ? 0 : 2
//            btn_readMore.setTitle(isExpanded ? "Show less..." : "Show more...", for: .normal)
            delegate?.moreTapped(cell: self, index: (sender as! UIButton).tag)
        }
    }
}
