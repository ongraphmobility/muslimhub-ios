//
//  DonationPaymentAmountCollectionViewCell.swift
//  Muslim Hub
//
//  Created by Rohit on 05/10/19.
//  Copyright © 2019 OnGraph. All rights reserved.
//

import UIKit

class DonationPaymentAmountCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var view_Main:UIView!
    @IBOutlet weak var lbl_Amount:UILabel!
    @IBOutlet weak var lbl_Description:UILabel!
}
