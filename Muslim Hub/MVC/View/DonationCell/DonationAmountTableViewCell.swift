//
//  DonationAmountTableViewCell.swift
//  Muslim Hub
//
//  Created by Ongraph on 31/07/19.
//  Copyright © 2019 OnGraph. All rights reserved.
//

import UIKit

class DonationAmountTableViewCell: UITableViewCell,UITextFieldDelegate {

    @IBOutlet weak var btn_Delete:UIButton!
    @IBOutlet weak var btn_DonationAmount:UITextField!
    @IBOutlet weak var btn_DonationDescription:UITextField!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        btn_DonationDescription.delegate = self
         btn_DonationAmount.delegate = self
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
