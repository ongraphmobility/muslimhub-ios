//
//  DonationTableViewCell.swift
//  Muslim Hub
//
//  Created by Ongraph on 31/07/19.
//  Copyright © 2019 OnGraph. All rights reserved.
//

import UIKit

class Add_DonationTableViewCell: UITableViewCell {
    
    @IBOutlet weak var txt_Title:UITextField!
    @IBOutlet weak var txt_RepeatingDay:UITextField!
    @IBOutlet weak var txtView_Description:UITextView!
    @IBOutlet weak var view_1:UIView!
    @IBOutlet weak var view_2:UIView!
    @IBOutlet weak var view_3:UIView!
    @IBOutlet weak var view_4:UIView!
    @IBOutlet weak var view_5:UIView!
    @IBOutlet weak var view_6:UIView!
    @IBOutlet weak var img_DropDown:UIImageView!
    @IBOutlet weak var btn_Yes:UIButton!
    @IBOutlet weak var btn_No:UIButton!
    @IBOutlet weak var txt_StartDate:UITextField!
    @IBOutlet weak var txt_EndDate:UITextField!
    @IBOutlet weak var txt_Code:UITextField!
    @IBOutlet weak var btn_PreDefined:UIButton!
    @IBOutlet weak var btn_UserDefined:UIButton!
    override func awakeFromNib() {
        
        super.awakeFromNib()
        // Initialization code
        btn_Yes.clipsToBounds = true
        btn_Yes.layer.cornerRadius = 8
        btn_Yes.layer.borderColor = UIColor.init(hex: 0x0DC9E4).cgColor
        btn_Yes.layer.borderWidth = 1.0
        btn_Yes.layer.maskedCorners = [.layerMinXMaxYCorner, .layerMinXMinYCorner]
        
        btn_No.clipsToBounds = true
        btn_No.layer.cornerRadius = 8
        btn_No.layer.maskedCorners = [.layerMaxXMaxYCorner, .layerMaxXMinYCorner]
        btn_No.layer.borderColor = UIColor.init(hex: 0x0DC9E4).cgColor
        btn_No.layer.borderWidth = 1.0
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}
