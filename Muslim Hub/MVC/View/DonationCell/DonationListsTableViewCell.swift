//
//  Org_Classes_TableViewCell.swift
//  Muslim Hub
//
//  Created by Sakshi Singh on 17/04/19.
//  Copyright © 2019 OnGraph. All rights reserved.
//

import UIKit
import ActiveLabel
import KVNProgress
import TTTAttributedLabel


protocol NotSoGoodCellDelegateDonation {
    func moreTapped(cell:DonationListsTableViewCell)
}
protocol IndexImageSendCellDelegateDonation {
    func IndexImageSendCellDelegateClass(cell: DonationListsTableViewCell,selected_imgIndex:Int)
}

class DonationListsTableViewCell: UITableViewCell {
    
    @IBOutlet weak var lbl_ReadMore:TTTAttributedLabel!
    @IBOutlet weak var view_Announcment:UIView!
    @IBOutlet weak var img_Viewannouncement:UIImageView!
    
    @IBOutlet weak var collection_Images:UICollectionView!
    @IBOutlet weak var collection_Donation:UICollectionView!
    
    @IBOutlet weak var lbl_WebsiteUrl: ActiveLabel!
    @IBOutlet weak var buttonMore: UIButton!
    @IBOutlet weak var imgContraint_ButtonMoreHeight: NSLayoutConstraint!
    @IBOutlet weak var imgBannerHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var collectionViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var lblClassesTitle        : UILabel!
    //    @IBOutlet weak var btnProfile             : UIButton!
    @IBOutlet weak var btnQuran             : UIButton!
    @IBOutlet weak var lblClassesDay          : UILabel!
    @IBOutlet weak var lblClassesDate         : UILabel!
    @IBOutlet weak var lblClassesTime         : UILabel!
    @IBOutlet weak var lbl_DonateBelowTitle         : UILabel!
    @IBOutlet weak var lbl_DonateAmountTitle         : UILabel!
   
    @IBOutlet weak var lbl_OrgName        : UILabel!
    @IBOutlet weak var img_Org     : UIImageView!
    @IBOutlet weak var btnDelete            : UIButton!
    @IBOutlet weak var btnEdit              : UIButton!
    @IBOutlet weak var btn_DonateUserDefined           : UIButton!
    @IBOutlet weak var view_UserDefinedView              : UIView!
    @IBOutlet var view_UserDefinedView_bottomConstraint: NSLayoutConstraint!
    @IBOutlet var collectionDonation_bottomConstraint: NSLayoutConstraint!
    @IBOutlet var lblReadMoreTop_bottomConstraint: NSLayoutConstraint!
      @IBOutlet var lblReadmore_HeightContraint: NSLayoutConstraint!
    
    
    @IBOutlet weak var txt_Userdefined       : UITextField!
    @IBOutlet weak var lbl_TaxDeductable          : UILabel!
    @IBOutlet weak var img_TaxDeductable         : UIImageView!
    @IBOutlet weak var lbl_StartDateEndDate_Donation         : UILabel!
    var delegateDonation: NotSoGoodCellDelegateDonation?
    var delegateImageSendCellDelegateDonation: IndexImageSendCellDelegateDonation?
    
    var isExpanded: Bool = false
    var selectedIndexPath                   :Int!
    var model                               : DonationData!
    var img_array                           : [String?]                     = []
    var selectedIndecx_Array                : [Bool]                        = []
    
    
    var phoneNumber:String!
    var urlPhoneNumber:String!
      var array_String = [String]()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        self.collection_Donation.delegate = self
        self.collection_Donation.dataSource = self
        self.lbl_DonateBelowTitle.text = "Donate Below".localized
        self.lbl_DonateAmountTitle.text = "Amount:-".localized
        self.btn_DonateUserDefined.setTitle("Donate".localized, for: .normal)
    }
    
    //Mark:IBAction---------------------------
    @IBAction func btnMoreTapped(_ sender: Any) {
        
        isExpanded = !isExpanded
        
        lbl_ReadMore.numberOfLines = isExpanded ? 0 : 1
        buttonMore.setTitle(isExpanded ? "Show less".localized : "Show more".localized, for: .normal)
       
        
        if isExpanded == true{
            
            lblReadmore_HeightContraint.isActive = false
            buttonMore.setImage(#imageLiteral(resourceName: "down_arrow_iPhone_blackup"), for: .normal)
            lbl_ReadMore.sizeToFit()
        }else{
            lblReadmore_HeightContraint.isActive = true
            lblReadmore_HeightContraint.constant = 20.0
            buttonMore.setImage(#imageLiteral(resourceName: "down_black_arrow_iPhone"), for: .normal)
            lbl_ReadMore.sizeToFit()
        }
        
        delegateDonation?.moreTapped(cell: self)
        
    }
    
    @objc func btnDonate(sender:UIButton) {
        
        self.donationPaymentApi(indexPath:sender.tag)
    }
    
    public func myInit(theTitle: String, theBody: String) {
        
        isExpanded = false
        
        // labelTitle.text = theTitle
        lbl_ReadMore.text = theBody
        
        lbl_ReadMore.numberOfLines = 1
        
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    func configureCell(model: DonationData,indexPath:Int){
        self.model = model
        self.btn_DonateUserDefined.tag = indexPath
        self.btn_DonateUserDefined.addTarget(self, action: #selector(btnDonate(sender:)), for: .touchUpInside)
        
        self.lblClassesTitle.text = model.org_abbr
        self.lbl_OrgName.text = model.title
     //   self.lbl_ReadMore.enabledTypes = [.mention, .hashtag, .url]
       // self.lbl_ReadMore.text = model.datumDescription
//        self.lbl_ReadMore.numberOfLines = 1
//        self.lbl_ReadMore.handleURLTap { url in
//
//
//
//            self.buttonMore.imageEdgeInsets = UIEdgeInsets(top: 0, left:0, bottom: -2, right: -172)
//            let urlString = ("\(url)")
//            var urll = String()
//
//            if urlString.contains("https://") {
//                urll = ("\(url)")
//            }else if urlString.contains("http://") {
//                urll = ("\(url)")
//            }else{
//                urll = ("http://\(url)")
//            }
//
//
//            guard let url = URL(string: urll) else { return }
//            UIApplication.shared.open(url)
//        }
//
        self.lbl_WebsiteUrl.handleURLTap { url in
            
            let urlString = ("\(url)")
            var urll = String()
            
            if urlString.contains("https://") {
                urll = ("\(url)")
            }else if urlString.contains("http://") {
                urll = ("\(url)")
            }else{
                urll = ("http://\(url)")
            }
            
            
            guard let url = URL(string: urll) else { return }
            UIApplication.shared.open(url)
        }
     
          
        lbl_ReadMore.text = model.datumDescription
                      
        //      //mark:number deduction an d url
                lbl_ReadMore.enabledTextCheckingTypes = NSTextCheckingResult.CheckingType.link.rawValue
                lbl_ReadMore.delegate = self

                self.phoneNumberDetctor(input: lbl_ReadMore.text as! String)
                self.uRLDetctor(input: lbl_ReadMore.text as! String)

                  let newText =  lbl_ReadMore.text as! NSString

                if self.array_String.count != 0{
                    for i in 0...self.array_String.count - 1{
                            let range:NSRange = newText.range(of: array_String[i])
                            lbl_ReadMore.addLink(to: URL(string: array_String[i]), with: range)
                           }
                }
                
     
        let countLabl : Int = self.lbl_ReadMore.calculateMaxLines()
        
        if countLabl == 1{
            self.buttonMore.setTitle(nil, for: .normal)
            self.buttonMore.setImage(nil, for: .normal)
            self.imgContraint_ButtonMoreHeight.constant = 0.0
            self.buttonMore.isUserInteractionEnabled = false
        }else{
            self.buttonMore.setTitle("Show more".localized, for: .normal)
            self.buttonMore.setImage(#imageLiteral(resourceName: "down_black_arrow_iPhone"), for: .normal)
            self.imgContraint_ButtonMoreHeight.constant = 30 //sakshi
            self.buttonMore.isUserInteractionEnabled = true
        }
        
        btnQuran.layer.cornerRadius = 4.0
        btnQuran.layer.borderWidth = 1.0
        btnQuran.layer.borderColor = UIColor.lightGray.cgColor
        btnQuran.layer.maskedCorners =  [.layerMinXMaxYCorner , .layerMinXMinYCorner]
        
        if   model.donationType == 1{
            btnQuran.setTitle("Medical", for: .normal)
            
        }else if   model.donationType == 2{
            btnQuran.setTitle("Education", for: .normal)
            
        }else if   model.donationType == 3{
            btnQuran.setTitle("Religious", for: .normal)
            
        }
        
        
        DispatchQueue.main.async {
            if let photo = self.model.donationLogo, let url = URL(string: photo) {
                
                
                self.img_Org.sd_setImage(with: url, placeholderImage: UIImage.init(named: "default_icon_iPhone_Logo.png"))
                
            } else {
                
                self.img_Org.image = UIImage(named: "default_icon_iPhone_Logo.png")
            }
            
        }
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd-MM-yyyy"
        dateFormatter.locale = Locale(identifier: "en_US_POSIX")
        //        let date = dateFormatter.date(from: model.startDate ?? "")
        //        let dateFormatter1 = DateFormatter()
        //        dateFormatter1.dateFormat = "d MMM yyyy"
        //        let date1 = dateFormatter1.string(from: date ?? Date())
        //        print(date1)
        //
        
        let enDatedate = dateFormatter.date(from: self.model.endDate ?? "")
        let enDatedatedateFormatter1 = DateFormatter()
        enDatedatedateFormatter1.dateFormat = "d MMM yyyy"
        enDatedatedateFormatter1.locale = Locale(identifier: "en_US_POSIX")
        let enDatedatedate1 = enDatedatedateFormatter1.string(from: enDatedate ?? Date())
        
        
        
        self.lblClassesDate.text = "Expires on - ".localized + enDatedatedate1
        
        
        self.img_array = model.images!
        
        
        self.selectedIndecx_Array = model.selectedImage_Array ?? []
        
        
        if model.images!.count > 0{
            if let urlstr = model.images?[0]{
                self.collectionViewHeightConstraint.constant = 40
                self.imgBannerHeightConstraint.constant = 180
                self.img_Viewannouncement.sd_setImage(with: URL(string: urlstr), placeholderImage: #imageLiteral(resourceName: "default_header_iPhone"))
            }else{
                self.img_Viewannouncement.image = #imageLiteral(resourceName: "default_header_iPhone")
            }
            
        }else{
            
            
            self.collectionViewHeightConstraint.constant = 0
            self.imgBannerHeightConstraint.constant = 0
            
            
        }
        
        
        if model.taxDeductable == true{
            
            self.img_TaxDeductable.isHidden = false
            self.lbl_TaxDeductable.text = "Tax Deduction"
        }else{
            self.img_TaxDeductable.isHidden = true
            self.lbl_TaxDeductable.text = ""
        }
        
        
        //mark: payment gateway selection
        if model.paymentGateway == 1{
            self.lbl_WebsiteUrl.isHidden = true
            
            //mark: payment according to amountType
            if model.amountType == 1{
                self.lbl_WebsiteUrl.isHidden = true
                
                self.collectionDonation_bottomConstraint.isActive = true
                self.view_UserDefinedView.isHidden = true
                self.view_UserDefinedView_bottomConstraint.isActive = false
                self.collection_Donation.isHidden = false
            }else{
                self.lbl_WebsiteUrl.isHidden = true
                
                self.collectionDonation_bottomConstraint.isActive = false
                self.view_UserDefinedView_bottomConstraint.constant = 10.0
                self.view_UserDefinedView_bottomConstraint.isActive = true
                self.view_UserDefinedView.isHidden = false
                self.collection_Donation.isHidden = true
                
            }
            self.lblReadMoreTop_bottomConstraint.isActive = false
        }else{
            self.lblReadMoreTop_bottomConstraint.isActive = true
            self.lblReadMoreTop_bottomConstraint.constant = 10.0
            
            self.lbl_WebsiteUrl.isHidden = false
            self.view_UserDefinedView.isHidden = true
            self.collection_Donation.isHidden = true
            self.collectionDonation_bottomConstraint.isActive = false
            self.view_UserDefinedView_bottomConstraint.isActive = false
            
        }
        
        self.collection_Images.reloadData()
        self.collection_Donation.reloadData()
        
    }
    
}
extension DonationListsTableViewCell:UICollectionViewDelegate,UICollectionViewDataSource{
    
    func registerCell() {
        
        if img_array.count == 1 || img_array.count == 0{
            //  collection_height.constant = 0.0
            collection_Images.isHidden = true
        }
        else{
            // collection_height.constant = 47.5
            collection_Images.isHidden = false
        }
    }
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        if collectionView != self.collection_Donation{
            
            return img_array.count
        }else{
            return model.preDIFAmountList?.count ?? 0
        }
    }
    
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if collectionView != self.collection_Donation{
            
            
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "AnnouncmentImageCollectionViewCell", for: indexPath) as! AnnouncmentImageCollectionViewCell
            //    cell.img_View.image = nil
            cell.img_View.clipsToBounds = true
            if let imageUrl = img_array[indexPath.row]{
                cell.img_View.sd_setImage(with: URL(string: imageUrl), placeholderImage: #imageLiteral(resourceName: "default_header_iPhone"))
            }else{
                cell.img_View.image = #imageLiteral(resourceName: "default_header_iPhone")
            }
            if let urlstr = img_array[0]{
                self.img_Viewannouncement.sd_setImage(with: URL(string: urlstr), placeholderImage: #imageLiteral(resourceName: "default_header_iPhone"))
            }else{
                self.img_Viewannouncement.image = #imageLiteral(resourceName: "default_header_iPhone")
            }
            if indexPath.item == selectedIndexPath{
                if let urlstr = img_array[selectedIndexPath]{
                    self.img_Viewannouncement.sd_setImage(with: URL(string: urlstr), placeholderImage: #imageLiteral(resourceName: "default_header_iPhone"))
                }else{
                    self.img_Viewannouncement.image = #imageLiteral(resourceName: "default_header_iPhone")
                    
                }
            }else{
                
            }
            print(indexPath.item,selectedIndexPath)
            if selectedIndexPath != nil{
                if self.selectedIndecx_Array[indexPath.item] == true
                {
                    cell.img_View.layer.borderColor = UIColor.init(hex: 0x0DC9E4).cgColor
                    cell.img_View.layer.borderWidth = 2.0
                    cell.img_View.layer.cornerRadius = 2.0
                    cell.img_View.clipsToBounds = false
                }else{
                    cell.img_View.layer.borderColor = UIColor.clear.cgColor
                    cell.img_View.layer.borderWidth = 2.0
                    cell.img_View.layer.cornerRadius = 2.0
                    cell.img_View.clipsToBounds = false
                }
            }
            
            
            return cell
        }
        else{
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "DonationPaymentAmountCollectionViewCell", for: indexPath) as! DonationPaymentAmountCollectionViewCell
            
            cell.shadowWithCornerReadius(cornerRadius: 5.0)
            
            cell.lbl_Amount.text = "$" + String(self.model.preDIFAmountList?[indexPath.item].amount ?? 0)
            cell.lbl_Description.text = String(self.model.preDIFAmountList?[indexPath.item].preDIFAmountListDescription ?? "")
            //  NotificationCenter.default.post(name: Notification.Name("DonationReload"), object: nil)
            return cell
        }
        
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        if collectionView != self.collection_Donation{
            
            
            selectedIndexPath = indexPath.row
            let index = IndexPath(row: indexPath.row, section: 0)
            DispatchQueue.main.async {
                var previousIndex : Int!
                for i in 0...self.selectedIndecx_Array.count - 1{
                    if self.selectedIndecx_Array[i] == true{
                        previousIndex = i
                    }
                }
                if previousIndex != indexPath.item{
                    self.selectedIndecx_Array[previousIndex] = false
                    self.collection_Images.reloadItems(at: [IndexPath(item: previousIndex, section: 0)])
                    if self.selectedIndecx_Array[indexPath.item] == true{
                        self.selectedIndecx_Array[indexPath.item] = false
                    }else{
                        self.selectedIndecx_Array[indexPath.item] = true
                    }
                    
                    self.delegateImageSendCellDelegateDonation?.IndexImageSendCellDelegateClass(cell: self, selected_imgIndex: indexPath.row)
                    self.collection_Images.reloadItems(at: [index])
                }
            }}else{
            
            self.donationPaymentApi(indexPath:indexPath.item)
            
        }
        
    }
    
    
}
extension DonationListsTableViewCell: UICollectionViewDelegateFlowLayout {
    
    private func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        if collectionView != self.collection_Donation{
            return 4
            
        }else{
            return 0
            
        }
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        if collectionView != self.collection_Donation{
            
            let totalCellWidth = 40 * collectionView.numberOfItems(inSection: 0)
            let totalSpacingWidth = 10 * (collectionView.numberOfItems(inSection: 0) - 1)
            
            let leftInset = (collectionView.layer.frame.size.width - CGFloat(totalCellWidth + totalSpacingWidth)) / 2
            let rightInset = leftInset
            
            return UIEdgeInsets(top: 0, left: leftInset, bottom: 0, right: rightInset)
        }else{
            return UIEdgeInsets()
        }
    }
    
    func donationPaymentApi(indexPath:Int){
        
        if (UserDefaults.standard.value(forKey: "Login_Token") as? String) != nil{
            
            var amount = ""
            if txt_Userdefined.text != "" {
                if (Int(txt_Userdefined.text ?? "0")! < 1){
                    
                    AKAlertController.alert("amount should be greater or equal to $1".localized)
                    return
                }else{
                    amount = txt_Userdefined.text!
                }
                
            }else{
                
                if model.amountType == 2{
                    if amount == ""{
                        AKAlertController.alert("Please enter amount".localized)
                        return
                    }else{
                        amount = "\(self.model.preDIFAmountList?[indexPath].amount ?? 0)"
                    }
                }else{
                    amount = "\(self.model.preDIFAmountList?[indexPath].amount ?? 0)"
                }
                
                
            }
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "E, d MMM yyyy HH:mm:ss Z"
              dateFormatter.locale = Locale(identifier: "en_US_POSIX")
            let dateFormatter1 = DateFormatter()
            dateFormatter1.dateFormat = "E, d MMM yyyy HH:mm:ss Z"
              dateFormatter1.locale = Locale(identifier: "en_US_POSIX")
            let expiry_Date = dateFormatter1.string(from:Calendar.current.date(byAdding: .day, value: 3, to: Date())!)
            
            
            
            Network.shared.poliPaymentApi(LinkType: "0", Amount:amount , CurrencyCode: "AUD", MerchantData: model.title ?? "", MerchantReference: model.title ?? "", ConfirmationEmail: "true", AllowCustomerReference: "false", ViaEmail: "false", RecipientName: "false", LinkExpiry: expiry_Date, RecipientEmail: "false", auth_Code: model.auth_code!, merchant_Code: model.merchant_code!) { (result) in
                KVNProgress.dismiss()
                guard var user = result else {
                    
                    return
                }
                
             if user != ""{
                                  _ = user.removeFirst()
                                  _ = user.removeLast()
                              
                AKAlertController.alert("", message: "You are being directed out of MuslimHub to Australia Post POLi Online Payment Option. This payment will be safely transacted from your account to the organisation account directly without going through MuslimHub", buttons: ["Ok","Cancel"], tapBlock: { (_, _, index) in
                    if index == 1 {return}
                    
                    guard let url = URL(string: user) else { return }
                    UIApplication.shared.open(url)
                    
                })
                }
            }
            
        }
        else{
            
            let registerUser = UserDefaults.standard.value(forKey: "hasProfile") as? Bool
            
            
            if registerUser != true{
                AKAlertController.alert("Please login/create profile for donation".localized)
                return
            }else{
                var amount = ""
                if txt_Userdefined.text != ""{
                    if (Int(txt_Userdefined.text ?? "0")! < 1){
                        
                        AKAlertController.alert("amount should be greater or equal to $1".localized)
                        return
                    }else{
                        amount = txt_Userdefined.text!
                    }
                    
                }else{
                    if model.amountType == 2{
                        if amount == ""{
                            AKAlertController.alert("Please enter amount".localized)
                            return
                        }else{
                            amount = "\(self.model.preDIFAmountList?[indexPath].amount ?? 0)"
                        }
                    }else{
                        amount = "\(self.model.preDIFAmountList?[indexPath].amount ?? 0)"
                    }
                    
                }
                
                
                
                
                
                let dateFormatter = DateFormatter()
                dateFormatter.dateFormat = "E, d MMM yyyy HH:mm:ss Z"
                 dateFormatter.locale = Locale(identifier: "en_US_POSIX")
                let dateFormatter1 = DateFormatter()
                dateFormatter1.dateFormat = "E, d MMM yyyy HH:mm:ss Z"
                 dateFormatter1.locale = Locale(identifier: "en_US_POSIX")
                let expiry_Date = dateFormatter1.string(from:Calendar.current.date(byAdding: .day, value: 3, to: Date())!)
                
                
                
                let userName = UserDefaults.standard.value(forKey: "userName") as? String ?? ""
                let userEmail = UserDefaults.standard.value(forKey: "userEmail") as? String ?? ""
                
                let MerchantData = (userName + " - " + userEmail)
                
                Network.shared.poliPaymentApi(LinkType: "0", Amount:amount , CurrencyCode: "AUD", MerchantData: MerchantData , MerchantReference: model.title! , ConfirmationEmail: "true", AllowCustomerReference: "false", ViaEmail: "false", RecipientName: "false", LinkExpiry: expiry_Date, RecipientEmail: "false", auth_Code: model.auth_code!, merchant_Code: model.merchant_code!) { (result) in
                    KVNProgress.dismiss()
                    guard var user = result else {
                        
                        return
                    }
                    
                    if user != ""{
                                      _ = user.removeFirst()
                                      _ = user.removeLast()
                                  
                        AKAlertController.alert("", message: "You are being directed out of MuslimHub to Australia Post POLi Online Payment Option. This payment will be safely transacted from your account to the organisation account directly without going through MuslimHub".localized, buttons: ["Ok".localized,"Cancel".localized], tapBlock: { (_, _, index) in
                        if index == 1 {return}
                        
                        guard let url = URL(string: user) else { return }
                        UIApplication.shared.open(url)
                        
                    })
                    }
                }
                
            }
            
        }
        
    }
    
    
}


extension DonationListsTableViewCell:TTTAttributedLabelDelegate{
    
    func phoneNumberDetctor(input:String){
                 
                 let detector = try! NSDataDetector(types: NSTextCheckingResult.CheckingType.phoneNumber.rawValue)
                           let matches = detector.matches(in: input, options: [], range: NSRange(location: 0, length: input.utf16.count))
                   
                           for match in matches {
                               guard let range = Range(match.range, in: input) else { continue }
                               let url = input[range]
                               print(url)
                            array_String.append(String(url))
                       
                         
                           
                           }
              
                 
             }
             func uRLDetctor(input:String){
                   
               
                   let detector = try! NSDataDetector(types: NSTextCheckingResult.CheckingType.link.rawValue)
                   let matches = detector.matches(in: input, options: [], range: NSRange(location: 0, length: input.utf16.count))

                   for match in matches {
                       guard let range = Range(match.range, in: input) else { continue }
                       let url = input[range]
                       print(url)
                 
                
                   
                      
                        
                   }
              
            
               }
    
    
    func phoneAndUrlDetctor(tap_string:String){
        
        //        let tap_string = array_String[view.tag]
        //
        //
                let detector = try! NSDataDetector(types: NSTextCheckingResult.CheckingType.phoneNumber.rawValue)
                          let matches = detector.matches(in: tap_string, options: [], range: NSRange(location: 0, length: tap_string.utf16.count))
        
                          for match in matches {
                              guard let range = Range(match.range, in: tap_string) else { continue }
                              let url = tap_string[range]
                              print(url)
        
                            self.phoneNumber = "true"
        
                          }
        
              let detectorUrl = try! NSDataDetector(types: NSTextCheckingResult.CheckingType.link.rawValue)
                          let matchesurl = detectorUrl.matches(in: tap_string, options: [], range: NSRange(location: 0, length: tap_string.utf16.count))
        
                          for match in matchesurl {
                              guard let range = Range(match.range, in: tap_string) else { continue }
                              let url = tap_string[range]
                              print(url)
        
                          self.phoneNumber = "false"
        
        
        
                          }
        
                if self.phoneNumber == "true"{
        
                    if let url = URL(string:"tel://\(tap_string )"), UIApplication.shared.canOpenURL(url) {
                                        UIApplication.shared.openURL(url)
                                   }
                }else if self.phoneNumber == "false"{
        
                    let urlString = ("\(tap_string )")
                                   var urll = String()
        
        
                                   if urlString.contains("https://") {
                                       urll = ("\(tap_string)")
                                   }else if urlString.contains("http://") {
                                       urll = ("\(tap_string)")
                                   }else{
                                       urll = ("http://\(tap_string)")
                                   }
        
        
                                   guard let url = URL(string: urll) else { return }
                                   UIApplication.shared.openURL(url)
                }
    }
    

        func attributedLabel(_ label: TTTAttributedLabel!, didSelectLinkWith url: URL!) {
         debugPrint("label>", label)
                
                debugPrint("addressComponents>", url)
            
            self.phoneAndUrlDetctor(tap_string: String(describing: url!) )
        }
        
    
}



