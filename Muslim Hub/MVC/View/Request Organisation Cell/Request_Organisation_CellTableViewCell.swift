//
//  Request_Organisation_CellTableViewCell.swift
//  Muslim Hub
//
//  Created by Ongraph on 25/06/19.
//  Copyright © 2019 OnGraph. All rights reserved.
//

import UIKit

class Request_Organisation_CellTableViewCell: UITableViewCell {
    
    @IBOutlet weak var txt_FillForm:UITextField!
    @IBOutlet weak var view_Main:UIView!
    @IBOutlet weak var view_Contry: UIView!
    @IBOutlet weak var btn_DropDown: UIButton!
    @IBOutlet weak var txtView_Description: UITextView!
    
    @IBOutlet weak var lbl_countryCodeLabel: UILabel!
    @IBOutlet weak var lbl_AreyouBoardMember: UILabel!
  
    @IBOutlet weak var uibutton_PickerOpen: UIButton!
    @IBOutlet weak var view_TextField: UIView!
    @IBOutlet weak var view_BoardMember:UIStackView!
    @IBOutlet weak var btn_Yes_BM:UIButton!
    @IBOutlet weak var btn_No_BM:UIButton!
    @IBOutlet weak var btn_countryCodeLabel: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        btn_Yes_BM.clipsToBounds = true
        btn_Yes_BM.layer.cornerRadius = 8
        btn_Yes_BM.layer.borderColor = UIColor.init(hex: 0x0DC9E4).cgColor
        btn_Yes_BM.layer.borderWidth = 1.0
        btn_Yes_BM.layer.maskedCorners = [.layerMinXMaxYCorner, .layerMinXMinYCorner]
        
        btn_No_BM.clipsToBounds = true
        btn_No_BM.layer.cornerRadius = 8
        btn_No_BM.layer.maskedCorners = [.layerMaxXMaxYCorner, .layerMaxXMinYCorner]
        btn_No_BM.layer.borderColor = UIColor.init(hex: 0x0DC9E4).cgColor
        btn_No_BM.layer.borderWidth = 1.0
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
