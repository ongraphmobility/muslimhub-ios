//
//  MenuTableViewCell.swift
//  Muslim Hub
//
//  Created by Sakshi Singh on 12/04/19.
//  Copyright © 2019 OnGraph. All rights reserved.
//

import UIKit

class MenuTableViewCell: UITableViewCell {
    
    @IBOutlet weak var img_Title:UIImageView!
    @IBOutlet weak var lbl_Title:UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
