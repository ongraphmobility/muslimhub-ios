//
//  AnnouncementTableViewCell.swift
//  Muslim Hub
//
//  Created by Sakshi Singh on 01/04/19.
//  Copyright © 2019 OnGraph. All rights reserved.
//

import UIKit
import ActiveLabel
import KVNProgress
import TTTAttributedLabel

protocol NotSoGoodCellDelegate {
    func moreTapped(cell: AnnouncementTableViewCell)
}
protocol NewsFeedCellDelegate {
    func moreNewsFeedTapped(cell: AnnouncementTableViewCell)
}
protocol IndexImageSendCellDelegate {
    func IndexImageSendCellDelegate(cell: AnnouncementTableViewCell,selected_imgIndex:Int)
}
protocol shareDataDelegateHome {
    func shareDataDelegateHome(cell: AnnouncementTableViewCell,date:String,time:String)
}
class AnnouncementTableViewCell: UITableViewCell {
    //MARK:IBOutlets----------------------
    @IBOutlet weak var btnDelete: UIButton!
    @IBOutlet weak var btnEdit: UIButton!
    @IBOutlet weak var lbl_ReadMore:TTTAttributedLabel!
    @IBOutlet weak var lbl_Title1:UIButton!
    @IBOutlet weak var lbl_Title2:UILabel!
    @IBOutlet weak var lbl_Title3:UILabel!
    @IBOutlet weak var lbl_Title4:UILabel!
    @IBOutlet weak var lbl_Title5:UILabel!
    @IBOutlet weak var view_Announcment:UIView!
    @IBOutlet weak var img_Viewannouncement:UIImageView!
    @IBOutlet weak var img_Logo:UIImageView!
    @IBOutlet weak var imgBannerHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var stackViewTimeTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var collectionViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var View_CategoryColor: UIView!
    @IBOutlet weak var collection_Images:UICollectionView!
    @IBOutlet weak var lblEventDate         : UILabel!
    @IBOutlet weak var lblEventTime         : UILabel!
    @IBOutlet weak var lblLocation          : UIButton!
    @IBOutlet weak var view_Location          :UIView!
    @IBOutlet weak var btn_ClassType          : UIButton!
    @IBOutlet weak var btn_GoingCount:UIButton!
    @IBOutlet weak var height_stackViewTypes:NSLayoutConstraint!
    @IBOutlet weak var view_stackViewTypes:UIStackView!
    @IBOutlet weak var view_stackViewTypes_Date:UIStackView!
    @IBOutlet weak var view_stackViewTypes_Location:UIStackView!
    @IBOutlet weak var buttonMore: UIButton!
    @IBOutlet weak var btn_ViewCategory: UIButton!
    @IBOutlet weak var view_ClassCost: UIView!
    @IBOutlet weak var lbl_ClassCost: UILabel!
    @IBOutlet weak var lbl_TeacherNAme: UILabel!
    @IBOutlet weak var lbl_TeacherNAmeTitle: UILabel!
    @IBOutlet weak var view_AgeGroup: UIView!
    @IBOutlet weak var lbl_AgeGroup: UILabel!
    @IBOutlet weak var lbl_AgeGroupTitle: UILabel!
    @IBOutlet weak var lbl_Gender: UILabel!
    @IBOutlet weak var view_Gender: UIView!
    @IBOutlet weak var view_TeacherType: UIView!
    @IBOutlet weak var view_AttendesType: UIView!
    @IBOutlet weak var lbl_AttendesType: UILabel!
     @IBOutlet weak var lbl_AttendesTypeTtile: UILabel!
    @IBOutlet weak var lbl_MonthType: UILabel!
    @IBOutlet weak var view_MonthType: UIView!
    @IBOutlet weak var view_Divider: UIView!
       
    @IBOutlet weak var view_ContainAGGA: UIStackView! //containing age group,attendees,gender
    @IBOutlet weak var view_Share: UIStackView!
    @IBOutlet weak var layout_ShareHeight: NSLayoutConstraint!
    
    @IBOutlet weak var btnShowHide          : UIButton!
    @IBOutlet weak var btnImageLogo         : UIButton!
    @IBOutlet weak var imgContraint_ButtonMoreHeight:NSLayoutConstraint!
    @IBOutlet var lblReadMore_Height:NSLayoutConstraint!
    
    var delegate: NotSoGoodCellDelegate?
    var newsFeedDelegate : NewsFeedCellDelegate?
    @IBOutlet weak var btnShare             : UIButton!
    @IBOutlet weak var btnGoing             : UIButton!
    @IBOutlet weak var btnReminder          : UIButton!
     @IBOutlet weak var lblCostTitle         : UIButton!
    var isExpanded: Bool = false
    
    var imageSelectedSendDelegate: IndexImageSendCellDelegate?
    var shareDataDelegatehome: shareDataDelegateHome?
    
    var model                               : HomeFeedData!
    var selectedIndexPath                   : Int!
    var img_array                           : [String?]                     = []
    var selectedIndecx_Array                : [Bool]                        = []
    @IBOutlet weak var lbl_TaxDeductable          : UILabel!
    @IBOutlet weak var img_TaxDeductable         : UIImageView!
    @IBOutlet weak var lbl_StartDateEndDate_Donation         : UILabel!
    @IBOutlet weak var collection_Donation:UICollectionView!
    @IBOutlet weak var lbl_WebsiteUrl: ActiveLabel!
    @IBOutlet weak var btn_DonateUserDefined : UIButton!
    @IBOutlet weak var view_UserDefinedView : UIView!
    @IBOutlet var view_UserDefinedView_bottom:NSLayoutConstraint!
    @IBOutlet weak var collection_DonationHeight:NSLayoutConstraint!
    @IBOutlet var collection_Donation_bottom:NSLayoutConstraint!
    @IBOutlet weak var view_donationHeight:NSLayoutConstraint!
    @IBOutlet weak var txt_Userdefined       : UITextField!
    @IBOutlet weak var view_stackViewDonation:UIStackView!
    @IBOutlet weak var lbl_DonateBelowTitle       : UILabel!
    @IBOutlet weak var lbl_DonateAmountTitle       : UILabel!
    @IBOutlet var lbl_DescriptionTop:NSLayoutConstraint!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        
        if #available(iOS 13.0, *) {
            self.collection_Donation.automaticallyAdjustsScrollIndicatorInsets = false
        } else {
            // Fallback on earlier versions
        }
        self.collection_Images.register(UINib(nibName: "ImageCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "ImageCollectionViewCell")
        
        self.btnShare.setTitle("Share".localized, for: .normal)
        self.btnGoing.setTitle("Going".localized, for: .normal)
        self.btnReminder.setTitle("Reminder".localized, for: .normal)
        self.lblCostTitle.setTitle("Cost:".localized, for: .normal)
        self.lbl_DonateBelowTitle.text = "Donate Below".localized
        self.lbl_DonateAmountTitle.text = "Amount:-".localized
        self.btn_DonateUserDefined.setTitle("Donate".localized, for: .normal)
        self.buttonMore.setTitle("Show more".localized, for: .normal)
        self.lbl_AgeGroupTitle.text = "Age Group:".localized
        self.lbl_TeacherNAmeTitle.text = "Teacher:".localized
        self.lbl_AttendesTypeTtile.text = "Attendees : ".localized
        

    }
    
    
    @objc func btnDonate(sender:UIButton) {
        
        self.donationPaymentApi(indexPath:sender.tag)
    }
    
    @IBAction func btnMoreTapped(_ sender: Any) {
        
        if sender is UIButton {
            isExpanded = !isExpanded
            
            lbl_ReadMore.numberOfLines = isExpanded ? 0 : 1
            buttonMore.setTitle(isExpanded ? "Show less".localized : "Show more".localized, for: .normal)
            
            if isExpanded == true{
                        
                        lblReadMore_Height.isActive = false
                        
                        lbl_ReadMore.sizeToFit()
                    }else{
                         lblReadMore_Height.isActive = true
                        lblReadMore_Height.constant = 20.0
                       lbl_ReadMore.sizeToFit()
                    }
            if isExpanded == true{
                buttonMore.setImage(#imageLiteral(resourceName: "down_arrow_iPhone_blackup"), for: .normal)
                btnShowHide.setImage(#imageLiteral(resourceName: "down_arrow_iPhone_blackup"), for: .normal)
            }else{
                buttonMore.setImage(#imageLiteral(resourceName: "down_black_arrow_iPhone"), for: .normal)
                btnShowHide.setImage(#imageLiteral(resourceName: "down_black_arrow_iPhone"), for: .normal)
            }
            
            
            delegate?.moreTapped(cell: self)
            newsFeedDelegate?.moreNewsFeedTapped(cell: self)
        }
        
    }
    
    public func myInit(theTitle: String, theBody: String) {
        
        isExpanded = false
        
        // labelTitle.text = theTitle
        lbl_ReadMore.text = theBody
        
        lbl_ReadMore.numberOfLines = 1
        
    }
    
    
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    //Mark: configure cell-----------------------------
    func configureCell(model: HomeFeedData,indexPath:Int){
        
        
        
        self.model = model
        
        self.img_array = model.images
        
        
       // buttonMore.imageEdgeInsets = UIEdgeInsets(top: 0, left:0, bottom: -2, right: -162)
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd-MM-yyyy"
        let date = dateFormatter.date(from: self.model.startDate ?? "")
        let dateFormatter1 = DateFormatter()
        dateFormatter1.dateFormat = "EEE, d MMM yyyy"
        dateFormatter1.locale = Locale(identifier: "en_US_POSIX")
        let date1 = dateFormatter1.string(from: date ?? Date())
        print(date1)
        self.lblEventDate.text = date1
        
        let timedateFormatter = DateFormatter()
        timedateFormatter.dateFormat = "HH:mm"
        timedateFormatter.locale = Locale(identifier: "en_US_POSIX")
        let starttime = timedateFormatter.date(from: self.model.startTime ?? "")
        let endTime = timedateFormatter.date(from: self.model.endTime ?? "")
        let timedateFormatter1 = DateFormatter()
        timedateFormatter1.dateFormat = "h:mm a"
        timedateFormatter1.locale = Locale(identifier: "en_US_POSIX")
        let startTime1 = timedateFormatter1.string(from: starttime ?? Date())
        let endTime1 = timedateFormatter1.string(from: endTime ?? Date())
        print(startTime1)
        
        
        self.lblEventTime.text = startTime1 + " - " + endTime1
        
        self.shareDataDelegatehome?.shareDataDelegateHome(cell: self, date: date1, time: self.lblEventTime.text ?? "")
        
        let linkAttributes: [NSAttributedString.Key : Any] = [
            NSAttributedString.Key(rawValue: NSAttributedString.Key.foregroundColor.rawValue): UIColor.black,
            NSAttributedString.Key(rawValue: NSAttributedString.Key.underlineColor.rawValue): UIColor.black,
            NSAttributedString.Key(rawValue: NSAttributedString.Key.underlineStyle.rawValue): NSUnderlineStyle.single.rawValue
        ]
        let attributeString = NSMutableAttributedString(string: model.address ?? "", attributes: linkAttributes)
        self.lblLocation.setAttributedTitle(attributeString, for: .normal)
        // self.lblLocation.attributedText = attributeString
        
        self.selectedIndecx_Array = model.selectedImage_Array ?? []
        
        if model.images.count > 0{
            self.collectionViewHeightConstraint.constant = 40
            self.stackViewTimeTopConstraint.constant = 5
            self.imgBannerHeightConstraint.constant = 180
        }else{
            self.collectionViewHeightConstraint.constant = 0
            self.stackViewTimeTopConstraint.constant = -10
            
            self.imgBannerHeightConstraint.constant = 0
        }
        
        //Mark:- Donation-------------------------------------------------------
        if model.category == "donation"{
            self.txt_Userdefined.isUserInteractionEnabled = true
            self.btn_DonateUserDefined.tag = indexPath
            self.btn_DonateUserDefined.addTarget(self, action: #selector(btnDonate(sender:)), for: .touchUpInside)
            
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "dd-MM-yyyy"
             dateFormatter.locale = Locale(identifier: "en_US_POSIX")
            let date = dateFormatter.date(from: self.model.startDate ?? "")
            let dateFormatter1 = DateFormatter()
            dateFormatter1.dateFormat = "d MMM yyyy"
            dateFormatter1.locale = Locale(identifier: "en_US_POSIX")
            let date1 = dateFormatter1.string(from: date ?? Date())
            print(date1)
            
            let enDatedate = dateFormatter.date(from: self.model.endDate ?? "")
            let enDatedatedateFormatter1 = DateFormatter()
            enDatedatedateFormatter1.dateFormat = "d MMM yyyy"
            enDatedatedateFormatter1.locale = Locale(identifier: "en_US_POSIX")
            let enDatedatedate1 = dateFormatter1.string(from: enDatedate ?? Date())
            
            
            
            self.lbl_StartDateEndDate_Donation.text =  "Expires on - ".localized + enDatedatedate1
            
            if model.tax_deductable == true{
                
                self.img_TaxDeductable.isHidden = false
                self.lbl_TaxDeductable.text = "Tax Deduction"
            }else{
                self.img_TaxDeductable.isHidden = true
                self.lbl_TaxDeductable.text = ""
            }
            self.lbl_WebsiteUrl.isHidden = true
            lbl_DescriptionTop.isActive = false
            if model.paymentGateway == 1{
                
                //mark: payment according to amountType
                if model.amountType == 1{
                    self.lbl_WebsiteUrl.isHidden = true
                    
                    self.collection_Donation_bottom.isActive = true
                    collection_Donation_bottom.constant = 10.0
                    self.view_UserDefinedView_bottom.isActive = false
                    
                    self.view_donationHeight.constant = 45.0
                    self.view_UserDefinedView.isHidden = true
                    self.collection_Donation.isHidden = false
                    collection_DonationHeight.constant = 88
                    
                    
                }else{
                    
                    self.lbl_WebsiteUrl.isHidden = true
                    
                    self.view_donationHeight.constant = 45.0
                    
                    self.view_UserDefinedView_bottom.isActive = true
                    
                    
                    self.collection_Donation_bottom.isActive = false
                    
                    self.view_UserDefinedView_bottom.constant = 10.0
                    self.view_UserDefinedView.isHidden = false
                    self.collection_Donation.isHidden = true
                    collection_DonationHeight.constant = 0
                }
                
            }else{
                self.view_donationHeight.constant = 65.0
                self.lbl_WebsiteUrl.isHidden = false
                self.view_UserDefinedView.isHidden = true
                self.collection_Donation.isHidden = true
                collection_DonationHeight.constant = 0
                if self.view_UserDefinedView_bottom != nil  {
                    self.view_UserDefinedView_bottom.isActive = false
                }
                
                self.collection_Donation_bottom.isActive = true
                
                collection_Donation_bottom.constant = 0.0
            }
            lbl_WebsiteUrl.text = model.website
            self.lbl_WebsiteUrl.handleURLTap { url in
                
                let urlString = ("\(url)")
                var urll = String()
                
                if urlString.contains("https://") {
                    urll = ("\(url)")
                }else if urlString.contains("http://") {
                    urll = ("\(url)")
                }else{
                    urll = ("http://\(url)")
                }
                
                
                guard let url = URL(string: urll) else { return }
                UIApplication.shared.open(url)
            }
            
            
            
        }else{
            lbl_DescriptionTop.isActive = true
            self.lbl_DescriptionTop.constant = 5.0
            self.view_UserDefinedView.isHidden = true
            self.collection_Donation.isHidden = true
            collection_DonationHeight.constant = 0
            self.view_donationHeight.constant = 0.0
            
        }
        
        self.collection_Images.reloadData()
        
        
        
        self.collection_Donation.reloadData()
        self.collection_Donation.collectionViewLayout.invalidateLayout()
    }
    
    
}
extension AnnouncementTableViewCell: UICollectionViewDelegate, UICollectionViewDataSource{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        if collectionView == self.collection_Donation{
            
            return model.pre_dif_amount_list?.count ?? 0
            
        }else{
            return self.img_array.count
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if collectionView == self.collection_Images{
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ImageCollectionViewCell", for: indexPath) as! ImageCollectionViewCell
            cell.img_View.clipsToBounds = true
            if let imageUrl = img_array[indexPath.row]{
                cell.img_View.sd_setImage(with: URL(string: imageUrl), placeholderImage: #imageLiteral(resourceName: "default_header_iPhone"))
            }else{
                cell.img_View.image = #imageLiteral(resourceName: "default_header_iPhone")
            }
            if let urlstr = img_array[0]{
                self.img_Viewannouncement.sd_setImage(with: URL(string: urlstr), placeholderImage: #imageLiteral(resourceName: "default_header_iPhone"))
            }else{
                self.img_Viewannouncement.image = #imageLiteral(resourceName: "default_header_iPhone")
            }
            if indexPath.item == selectedIndexPath{
                if let urlstr = img_array[selectedIndexPath]{
                    self.img_Viewannouncement.sd_setImage(with: URL(string: urlstr), placeholderImage: #imageLiteral(resourceName: "default_header_iPhone"))
                }else{
                    self.img_Viewannouncement.image = #imageLiteral(resourceName: "default_header_iPhone")
                    
                }
            }else{
                
            }
            print(indexPath.item,selectedIndexPath)
            if selectedIndexPath != nil{
                if self.selectedIndecx_Array[indexPath.item] == true
                {
                    cell.img_View.layer.borderColor = UIColor.init(hex: 0x0DC9E4).cgColor
                    cell.img_View.layer.borderWidth = 2.0
                    cell.img_View.layer.cornerRadius = 2.0
                    cell.img_View.clipsToBounds = false
                }else{
                    cell.img_View.layer.borderColor = UIColor.clear.cgColor
                    cell.img_View.layer.borderWidth = 2.0
                    cell.img_View.layer.cornerRadius = 2.0
                    cell.img_View.clipsToBounds = false
                }
            }
            
            return cell
        }else{
            
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "DonationPaymentAmountCollectionViewCell", for: indexPath) as! DonationPaymentAmountCollectionViewCell
            
            cell.shadowWithCornerReadius(cornerRadius: 5.0)
            
            cell.lbl_Amount.text = "$" + String(self.model.pre_dif_amount_list?[indexPath.item].amount ?? 0)
            cell.lbl_Description.text = String(self.model.pre_dif_amount_list?[indexPath.item].preDIFAmountListDescription ?? "")
            
            return cell
        }
        
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if collectionView != self.collection_Donation{
            selectedIndexPath = indexPath.row
            let index = IndexPath(row: indexPath.row, section: 0)
            
            DispatchQueue.main.async {
                var previousIndex : Int!
                
                for i in 0...self.selectedIndecx_Array.count - 1{
                    if self.selectedIndecx_Array[i] == true{
                        previousIndex = i
                    }
                }
                
                
                if previousIndex != indexPath.item{
                    self.selectedIndecx_Array[previousIndex] = false
                    self.collection_Images.reloadItems(at: [IndexPath(item: previousIndex, section: 0)])
                    if self.selectedIndecx_Array[indexPath.item] == true{
                        self.selectedIndecx_Array[indexPath.item] = false
                    }else{
                        self.selectedIndecx_Array[indexPath.item] = true
                    }
                    self.imageSelectedSendDelegate?.IndexImageSendCellDelegate(cell: self, selected_imgIndex: indexPath.row)
                    self.collection_Images.reloadItems(at: [index])
                }
            }
        }else{
            
            
            self.donationPaymentApi(indexPath:indexPath.item)
            
            
        }}
}

extension AnnouncementTableViewCell: UICollectionViewDelegateFlowLayout {
    
    
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        
        if collectionView != self.collection_Donation{
            return 4
            
        }else{
            return 0
            
        }
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        
        if collectionView != self.collection_Donation{
            let totalCellWidth = 40 * collectionView.numberOfItems(inSection: 0)
            let totalSpacingWidth = 4 * (collectionView.numberOfItems(inSection: 0) - 1)
            
            let leftInset = (collectionView.layer.frame.size.width - CGFloat(totalCellWidth + totalSpacingWidth)) / 2
            let rightInset = leftInset
            
            return UIEdgeInsets(top: 0, left: leftInset, bottom: 0, right: rightInset)
        }
        return UIEdgeInsets()
    }
    
    func donationPaymentApi(indexPath:Int){
        
        if (UserDefaults.standard.value(forKey: "Login_Token") as? String) != nil{
            var amount = ""
            if txt_Userdefined.text != "" {
                if (Int(txt_Userdefined.text!)! < 1){
                    
                    AKAlertController.alert("amount should be greater or equal to $1")
                    return
                }else{
                    amount = txt_Userdefined.text!
                }
            }else{
                
                
                if model.amountType == 2{
                    if amount == ""{
                        AKAlertController.alert("Please enter amount")
                        return
                    }else{
                        amount = "\(self.model.pre_dif_amount_list?[indexPath].amount ?? 0)"
                    }
                }else{
                    
                    amount = "\(self.model.pre_dif_amount_list?[indexPath].amount ?? 0)"
                }
                
                
            }
            
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "E, d MMM yyyy HH:mm:ss Z"
            let dateFormatter1 = DateFormatter()
            dateFormatter1.dateFormat = "E, d MMM yyyy HH:mm:ss Z"
            let expiry_Date = dateFormatter1.string(from:Calendar.current.date(byAdding: .day, value: 3, to: Date())!)
            
            
            
            Network.shared.poliPaymentApi(LinkType: "0", Amount:amount , CurrencyCode: "AUD", MerchantData: model.title , MerchantReference: model.title , ConfirmationEmail: "true", AllowCustomerReference: "false", ViaEmail: "false", RecipientName: "false", LinkExpiry: expiry_Date, RecipientEmail: "false", auth_Code: model.auth_code!, merchant_Code: model.merchant_code!) { (result) in
                
                guard var user = result else {
                    
                    return
                }
                if user != ""{
                    _ = user.removeFirst()
                    _ = user.removeLast()
                    
                    
                    
                    AKAlertController.alert("", message: "You are being directed out of MuslimHub to Australia Post POLi Online Payment Option. This payment will be safely transacted from your account to the organisation account directly without going through MuslimHub", buttons: ["Ok","Cancel"], tapBlock: { (_, _, index) in
                        if index == 1 {return}
                        
                        guard let url = URL(string: user) else { return }
                        UIApplication.shared.open(url)
                        
                    })
                }
                
            }}else{
            
            let registerUser = UserDefaults.standard.value(forKey: "hasProfile") as? Bool
            
            
            if registerUser != true{
                AKAlertController.alert("Please login/create profile for donation")
                return
            }else{
                var amount = ""
                if txt_Userdefined.text != "" {
                    if (Int(txt_Userdefined.text!)! < 1){
                        
                        AKAlertController.alert("amount should be greater or equal to $1")
                        return
                    }else{
                        amount = txt_Userdefined.text!
                    }
                    
                }else{
                    if model.amountType == 2{
                        if amount == ""{
                            AKAlertController.alert("Please enter amount")
                            return
                        }else{
                            amount = "\(self.model.pre_dif_amount_list?[indexPath].amount ?? 0)"
                        }
                    }else{
                        
                        amount = "\(self.model.pre_dif_amount_list?[indexPath].amount ?? 0)"
                    }
                }
                
                let dateFormatter = DateFormatter()
                dateFormatter.dateFormat = "E, d MMM yyyy HH:mm:ss Z"
                let dateFormatter1 = DateFormatter()
                dateFormatter1.dateFormat = "E, d MMM yyyy HH:mm:ss Z"
                let expiry_Date = dateFormatter1.string(from:Calendar.current.date(byAdding: .day, value: 3, to: Date())!)
                
                let userName = UserDefaults.standard.value(forKey: "userName") as! String
                let userEmail = UserDefaults.standard.value(forKey: "userEmail") as! String
                
                let MerchantData = (userName + " - " + userEmail)
                
                Network.shared.poliPaymentApi(LinkType: "0", Amount:amount , CurrencyCode: "AUD", MerchantData: MerchantData , MerchantReference: model.title , ConfirmationEmail: "true", AllowCustomerReference: "false", ViaEmail: "false", RecipientName: "false", LinkExpiry: expiry_Date, RecipientEmail: "false", auth_Code: model.auth_code!, merchant_Code: model.merchant_code!) { (result) in
                    
                    guard var user = result else {
                        
                        return
                    }
                    
                    if user != ""{
                        _ = user.removeFirst()
                        _ = user.removeLast()
                        
                        AKAlertController.alert("", message: "You are being directed out of MuslimHub to Australia Post POLi Online Payment Option. This payment will be safely transacted from your account to the organisation account directly without going through MuslimHub", buttons: ["Ok","Cancel"], tapBlock: { (_, _, index) in
                            if index == 1 {return}
                            
                            guard let url = URL(string: user) else { return }
                            UIApplication.shared.open(url)
                            
                        })
                        
                    }}
                
                
            }
            
        }
        
    }
}
