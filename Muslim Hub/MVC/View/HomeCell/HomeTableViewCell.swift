//
//  HomeTableViewCell.swift
//  Muslim Hub
//
//  Created by Sakshi Singh on 01/04/19.
//  Copyright © 2019 OnGraph. All rights reserved.
//

import UIKit

class HomeTableViewCell: UITableViewCell {
    
    //MARK:IBOutlets----------------------
    @IBOutlet weak var view_Home:UIView!


    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
