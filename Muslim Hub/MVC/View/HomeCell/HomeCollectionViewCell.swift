//
//  HomeCollectionViewCell.swift
//  Muslim Hub
//
//  Created by Sakshi Singh on 02/04/19.
//  Copyright © 2019 OnGraph. All rights reserved.
//

import UIKit

class HomeCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var bg_View:UIView!
    @IBOutlet weak var img_View:UIImageView!
    @IBOutlet weak var lbl_Title:UILabel!
     @IBOutlet weak var img_Notification:UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        bg_View.clipsToBounds = false
        bg_View.layer.shadowColor = UIColor.white.cgColor
        bg_View.layer.shadowOpacity = 1.0
        bg_View.layer.shadowOffset = CGSize.zero
        bg_View.layer.shadowRadius = 6
        bg_View.layer.cornerRadius = 32.5
        
        
        img_View.clipsToBounds = true
        img_View.layer.cornerRadius = 25
        img_View.layer.borderColor = UIColor.init(hex: 0xD09B31).cgColor
        img_View.layer.borderWidth = 2.0
    }
}






