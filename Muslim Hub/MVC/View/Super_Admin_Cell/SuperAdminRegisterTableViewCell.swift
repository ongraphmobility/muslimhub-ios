//
//  SuperAdminRegisterTableViewCell.swift
//  Muslim Hub
//
//  Created by Sakshi Singh on 22/04/19.
//  Copyright © 2019 OnGraph. All rights reserved.
//

import UIKit

class SuperAdminRegisterTableViewCell: UITableViewCell {
    
    @IBOutlet weak var txt_FillForm:UITextField!
    @IBOutlet weak var view_Main:UIView!
    @IBOutlet weak var view_Contry: UIView!
    @IBOutlet weak var btn_DropDown: UIButton!
    @IBOutlet weak var txtView_Description: UITextView!
    
    @IBOutlet weak var lbl_countryCodeLabel: UILabel!
    @IBOutlet weak var btn_countryCodeLabel: UIButton!
    @IBOutlet weak var uibutton_PickerOpen: UIButton!
      @IBOutlet weak var view_TextField: UIView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
      
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

  
}
