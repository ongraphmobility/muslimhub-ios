//
//  AlertAddServicesCollectionViewCell.swift
//  Muslim Hub
//
//  Created by Sakshi Singh on 29/04/19.
//  Copyright © 2019 OnGraph. All rights reserved.
//

import UIKit

class AlertAddServicesCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var btn_Tick:UIButton!
    @IBOutlet weak var lbl_AddServicesTitle:UILabel!
    @IBOutlet weak var img_AddServices:UIImageView!
}
